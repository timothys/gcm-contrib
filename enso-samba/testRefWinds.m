function [] = testRefWinds()
% Script to test if the wind stress in reference experiment
% matches ECCOv4r2 well enough to simply add perturbation 
% to field for reconstruction
%
% Conclusion: See differences bigger than the perturbation...
%             can't use them interchangeably.
% 
% -------------------------------------------------------

mygrid = establish_mygrid('llc90');

% --- Load up ECCO fields
matfile = '../samoc/mat/exf/eccov4r2/short-atm/exf_tauu.mat';
load(matfile,'exf_fld');
tauu_ecco = exf_fld; clear exf_fld;

% --- Load reference from en97 perturbation experiment
matfile = 'mat/en97.p005/exf_tau.mat';
load(matfile,'tauu');
tauu_en97 = tauu; clear tauu;


% --- Grab Jan 1997 - June 1998 from ecco
inds_en97 = [2:17];
inds_ecco = 5*12+inds_en97;

tauu_en97 = tauu_en97(:,:,inds_en97);
tauu_ecco = tauu_ecco(:,:,inds_ecco);


% --- Plot sum of absolute value difference
tauu_diff = abs(tauu_en97 - tauu_ecco);

figureW;
m_map_atl(nansum(tauu_diff,3),8);

keyboard
end
