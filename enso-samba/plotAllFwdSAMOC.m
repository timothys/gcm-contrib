function [] = plotAllFwdSAMOC(pYear,pMonth,windFreq,recString)
% Plotting script to compare the different forward runs 
%
% Optional Parameters
% -------------------
%
%   pYear   ::  defines the year of the perturbation experiment
%               '92' (default) or '11'
%
%   pMonth  ::  defines the month over which the perturbation 
%               is applied 
%               'feb' (default) or 'mar', 'jtm', 'ftm', 'fta'
%   windFreq
%           ::  frequency of wind stress perturbation to plot 
%               'daily' (default) or 'monthly'
%   
%   recString
%           ::  which month or months to use in reconstruction
%               '' (default) = use 2011 sensitivities from 
%                   each individual month
%               'dec.92' = use December objective function sens.
%                       from 1992 
%               'dec.11' = same, use Dec. from  2011
%               
%
% -----------------------------------------------------
if nargin < 1
    pYear   = '11';
end
if nargin < 2
    pMonth  = 'fta';
end
if nargin < 3 
    windFreq= 'daily';
end
if nargin < 4
    recString='';
end

pType       = {'xx','hk'};
pStrength   = {'p001','p005','p05','p05.spy','p1'};
pStrengthStr= {'p001','p005','p05','p05_spy','p1'};
pStrengthDbl= [0.001,0.005,0.05,0.05,0.1];
scaleFact   = [100,20,2,4,1];


% --- Total number of simulations
Ntype       = length(pType);
Nstrength   = length(pStrength);
Nts         = Ntype*Nstrength;

% --- Make a data structure to hold all the results and labels
%   For now, assume all during feb and 1992
samoc_diff  = struct();
samoc_diff_norm = struct();
samoc_str   = struct();
samoc_adj   = struct();
dtauu_avg   = struct();
absErr      = struct();
relErr      = struct();

for i = 1:Ntype
    for j = 1:Nstrength

        % --- Compute dSAMOC
        tmp1 = calcSAMOCDiff(pType{i},pYear,pStrength{j},pMonth);
        eval(sprintf('samoc_diff.%s.%s = tmp1;',pType{i},pStrengthStr{j}));

        % --- Normalize 
        tmp2 = tmp1*scaleFact(j);
        eval(sprintf('samoc_diff_norm.%s.%s = tmp2;',pType{i},pStrengthStr{j}));

        % --- String
        tmp3 = sprintf('Fwd from %s = %0.3f, scaled by %2d',...
                        '$\delta\tau_x$',pStrengthDbl(j),scaleFact(j));
        eval(sprintf('samoc_str.%s.%s = tmp3;',pType{i},pStrengthStr{j}));
    end
end

% --- Time vector
Nt = length(samoc_diff.xx.p05);
t = [1:Nt] + 0.5;

% --- Load average tauu perturbation 
%   Assume same pStrength for each type
%   value is from 0.05 experiment, because all cases are run with this strength 
%   but then scale by 2
tauStr = 'p05';
tauFact= 2;
if ~isempty(strfind(pMonth,'jtm')) || strcmp(pYear,'11')
    tauStr = 'p005';
    tauFact= 20;
end
for i = 1:Ntype
    refStr  = sprintf('no%s.ref',pYear);
    pStr    = sprintf('%s%s.%s.%s',pType{i},pYear,tauStr,pMonth);
    [~,~,tmp] = calcDeltaZonalWind(refStr,pStr,windFreq);
    tmp = tmp*tauFact;
    eval(sprintf('dtauu_avg.%s = tmp;',pType{i}));

    % --- Make time vector for winds
    Nt_wind = length(tmp);
    t_wind = [1:Nt_wind]+0.5;
end

% --- Load adjoint version
for i = 1:Ntype
    for j = 1:Nstrength
        eval(sprintf('samoc_adj.%s.%s = calcAdjSAMOCDiff(pType{i},pYear,pStrength{j},pMonth,Nt,recString);',pType{i},pStrengthStr{j}));
    end
end

% --- Because p05 is done for all cases, make sure p1 for adj exists
%   because linear, can scale by a factor here...
if sum(isnan(samoc_adj.xx.p1))>0
    if sum(isnan(samoc_adj.xx.p05))==0
        samoc_adj.xx.p1 = samoc_adj.xx.p05*2;
    elseif sum(isnan(samoc_adj.xx.p005))==0
        samoc_adj.xx.p1 = samoc_adj.xx.p005*20;
    else
        error('Pick a new adjoint reconstruction ...')
    end
end
if sum(isnan(samoc_adj.hk.p1))>0
    if sum(isnan(samoc_adj.hk.p05))==0
        samoc_adj.hk.p1 = samoc_adj.hk.p05*2;
    elseif sum(isnan(samoc_adj.hk.p005))==0
        samoc_adj.hk.p1 = samoc_adj.hk.p005*20;
    else
        error('Pick a new adjoint reconstruction ...')
    end
end

% --- Compute absolute and relative error
for i = 1:Ntype
    for j = 1:Nstrength

        % --- Absolute error
        eval(sprintf('absErr.%s.%s = abs(samoc_diff.%s.%s - samoc_adj.%s.%s);', ...
            pType{i},pStrengthStr{j},...
            pType{i},pStrengthStr{j},...
            pType{i},pStrengthStr{j}));
        
        % --- Relative error
        eval(sprintf('relErr.%s.%s = absErr.%s.%s ./ abs(samoc_diff.%s.%s) * 100;', ...
            pType{i},pStrengthStr{j},...
            pType{i},pStrengthStr{j},...
            pType{i},pStrengthStr{j}));
    end
end


% --- Figure settings
cmap    = cbrewer2('Dark2',Nstrength+1);
ms      = 20;
fs      = 22;
fsl     = 16;
xlbl    = sprintf('Months during %s',pYear);
ylbl    = 'Normalized $\delta$SAMOC [Sv]';
ylbltau = 'Avg. $\delta\tau_x$ [N/m$^2$]';
relErrLbl= 'Relative Error [\%]';

% --- Show all xx type perturbations
figureL;
subplot(2,1,1)
[ax,h1,h2] = plotyy(...
    t,[samoc_diff_norm.xx.p001;...
       samoc_diff_norm.xx.p005;...
       samoc_diff_norm.xx.p05;...
       samoc_diff_norm.xx.p05_spy;...
       samoc_diff_norm.xx.p1;...
       samoc_adj.xx.p1],...
    t_wind,dtauu_avg.xx);
doublePlotSettings(ax,h1,h2,fs,ms,cmap,windFreq);
ylabel(ax(1),ylbl);
ylabel(ax(2),ylbltau);
hl=legend(samoc_str.xx.p001,...
       samoc_str.xx.p005,...
       samoc_str.xx.p05 ,...
       samoc_str.xx.p05_spy ,...
       samoc_str.xx.p1  ,...
       'Adjoint reconstruction',...
       'Avg. $\delta\tau_x$');
set(hl,'fontsize',fsl);

subplot(2,1,2),
[h1] = semilogy(...
    t,[relErr.xx.p001;...
       relErr.xx.p005;...
       relErr.xx.p05;...
       relErr.xx.p05_spy;...
       relErr.xx.p1]);
singlePlotSettings(gca,h1,fs,ms,cmap(1:Nstrength,:));
ylabel(relErrLbl)


% --- Show all hk type perturbations
figureL;
subplot(2,1,1)
[ax,h1,h2] = plotyy(...
    t,[samoc_diff_norm.hk.p001;...
       samoc_diff_norm.hk.p005;...
       samoc_diff_norm.hk.p05;...
       samoc_diff_norm.hk.p05_spy;...
       samoc_diff_norm.hk.p1;...
       samoc_adj.hk.p1],...
    t_wind,dtauu_avg.hk);
doublePlotSettings(ax,h1,h2,fs,ms,cmap,windFreq);
ylabel(ax(1),ylbl);
ylabel(ax(2),ylbltau);
hl = legend(samoc_str.hk.p001,...
       samoc_str.hk.p005,...
       samoc_str.hk.p05 ,...
       samoc_str.hk.p05_spy ,...
       samoc_str.hk.p1  ,...
       'Adjoint reconstruction',...
       'Avg. $\delta\tau_x$');
set(hl,'fontsize',fsl);

subplot(2,1,2),
[h1] = semilogy(...
    t,[relErr.hk.p001;...
       relErr.hk.p005;...
       relErr.hk.p05;...
       relErr.hk.p05_spy;...
       relErr.hk.p1]);
singlePlotSettings(gca,h1,fs,ms,cmap(1:Nstrength,:));
ylabel(relErrLbl)

keyboard

end

% -----------------------------------------------------

function [] = doublePlotSettings(ax,h1,h2,fs,ms,cmap,windFreq)

% --- Set monthly labels
xlbl_clim(ax(1));
set(ax(1),'xlim',[0.5 13]);

% --- Set xlim based on wind output frequency
%   this is ad hoc for now, but match xlim of [.5 12.5] 
%   for monthly output
if strcmp(windFreq,'daily')
    set(ax(2),'xlim',[-17 365]);
elseif strcmp(windFreq,'monthly')
    set(ax(2),'xlim',[0.5 13])
else
    fprintf('Warning: windFreq option not recognized, not changing xlim\n');
end


for n=1:2
    
    % --- Set y-axes black
    ax(n).YColor = [0,0,0];

    % --- Fontsize
    ax(n).FontSize = fs;
end

% --- Colormap
set(h1,{'Color'},num2cell(cmap,2));
set(h2,{'Color'},num2cell([0.2 0.2 0.2],2));

for n=1:length(h1)
    h1(n).Marker='.';
    h1(n).MarkerSize=ms;
end
if strcmp(windFreq,'monthly')
    for n=1:length(h2)
        h2(n).Marker='.';
        h2(n).MarkerSize=ms;
    end
end

% --- Set nice ylim for dSAMOC
ymin = -0.02;
ymax =  0.08;
set(ax(1),'ylim',[ymin ymax],...
          'ytick',ymin:.02:ymax);

grid on
end

% -----------------------------------------------------

function [] = singlePlotSettings(ax,h1,fs,ms,cmap)

% --- Set monthly labels
xlbl_clim(ax);
set(ax,'xlim',[0.5 13]);

% --- Fontsize
ax.FontSize = fs;

% --- Colormap
set(h1,{'Color'},num2cell(cmap,2));

for n=1:length(h1)
    h1(n).Marker='.';
    h1(n).MarkerSize=ms;
end

% --- Set y limit for relative error
set(ax,'ylim',[1 10^3]);

grid on
end

% -----------------------------------------------------

function [samoc_diff] = calcSAMOCDiff(pType,pYear,pStrength,pMonth)
% compute the difference between perturbed and reference SAMOC values

refStr  = sprintf('no%s.ref',pYear);
pStr    = sprintf('%s%s.%s.%s',pType,pYear,pStrength,pMonth);

results_dir = sprintf('../../results/enso-samba/%s',pStr);

% --- Check for files, if not exist, return NaNs
if exist(results_dir,'dir')
    
    [samoc_r] = calcForwardSAMOC(refStr,'monthly');
    [samoc_p] = calcForwardSAMOC(pStr,'monthly');
    
    samoc_diff = samoc_p - samoc_r;
else
    if strcmp(pYear,'92')
        Nmo = 12;
    elseif strcmp(pYear,'11')
        Nmo = 11;
    else
        error('Need to set up other simulation years')
    end

    samoc_diff = NaN*ones(1,Nmo);
end

end

% -----------------------------------------------------

function [samoc_adj] = calcAdjSAMOCDiff(pType,pYear,pStrength,pMonth,Nt,recString)

refStr  = sprintf('no%s.ref',pYear);
pStr    = sprintf('%s%s.%s.%s',pType,pYear,pStrength,pMonth);

results_dir = sprintf('../../results/enso-samba/%s',pStr);

% --- Check for files, if not exist, return NaNs
if exist(results_dir,'dir')

    samoc_diff_rec = calcPerturbationReconstruction(refStr,pStr,recString);

    % --- Index the reconstruction memory and monthly records
    tau_mem = 228;
    if strcmp(pYear,'92')
        time_ind = [1:Nt];
    elseif strcmp(pYear,'11')
        time_ind = 19*12 + [1:Nt];
    else
        error('Need to set up other years...')
    end

    samoc_adj = samoc_diff_rec(tau_mem,time_ind);

else
    samoc_adj = NaN*ones(1,Nt);
end
end
