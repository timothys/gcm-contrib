function [] = plotPerturbEta()

mygrid=establish_mygrid('llc90');

ref_results_dir = '../../results/enso-samba/ref/diags/';
per_results_dir = '../../results/enso-samba/en97.p05/diags/';

% Grab iterations for time stamp in plot
iters = grabAllIters(ref_results_dir,'eta_mxl_7day');

start_iter = 297;
end_iter = 500;
dates = ts2dte(iters,3600,1992,1,1,12,1);
dates = dates(start_iter:end_iter,:);
dates = mat2cell(dates,ones(1,size(dates,1)),size(dates,2));
for n=1:length(dates)
    dates{n} = num2str(dates{n});
end

% Load mxl depth from each
% 'ETAN   ','MXLDEPTH'
mxl_ref = rdmds2gcmfaces([ref_results_dir 'eta_mxl_7day'],iters(start_iter:end_iter),'rec',2);
mxl_per = rdmds2gcmfaces([per_results_dir 'eta_mxl_7day'],iters(start_iter:end_iter),'rec',2);

mxl_diff = mxl_per-mxl_ref;



% Make some flicks
%makeMovie(mxl_diff,dates)
saveMovie(mxl_diff,dates,'figures/mxl_diff_en97_p05');

keyboard
end

function [] = makeMovie(fld,timeStamp)
% fld = 3D gcmfaces object, 2D field, 3rd dim gives time records
% timeStamp = cell aray of time stamps for each record of field

Nt = size(fld.f1,3);
cmap = cbrewer2('RdBu',11);
cmap = flipdim(cmap,1);

figureW;
for n=1:Nt
    m_map_atl(fld(:,:,n),5);
    title(['MLD$_{pert}$ - MLD$_{ref}$, ' timeStamp{n}])
    hc=niceColorbar;
    ylabel(hc,'$\delta$MLD, m')
    colormap(cmap);
    pause(.5)
end

end

function [] = saveMovie(fld,timeStamp,filename)


Nt = size(fld.f1,3);
ncolors = 9;
cmap = cbrewer2('RdBu',ncolors);
cmap = flipdim(cmap,1);


% Get log of data
fact = 10^5;
log_fld = binByLog(fld,fact);

cmax=nanmax(abs(log_fld(:)));
c_lim = 10^round(cmax) / fact;


% Open video object for saving
vidObj = VideoWriter(filename,'MPEG-4');
set(vidObj,'FrameRate',2);
open(vidObj);

figureW;
c=gcf();
for n=1:Nt
    m_map_atl(log_fld(:,:,n),5);
    xlabel(['MLD$_{pert}$ - MLD$_{ref}$, ' timeStamp{n}])
    hc=niceColorbar;
    caxis([-cmax cmax]);
    hc.Ticks = linspace(-cmax,cmax,ncolors);
    nn=floor(ncolors/2)-1;
    hc.TickLabels = [-c_lim*10.^[0:-1:-nn],0,c_lim*10.^[-nn:0]];    
    ylabel(hc,'$\delta$MLD, m')
    colormap(cmap);
    currFrame=getframe(c);
    writeVideo(vidObj,currFrame);
end

close(vidObj);
end

function [bin_fld] = binByLog(fld,fact)
% min_bin = smallest bin, everything smaller than 10^{min_bin} goes in last bin

fld = fld.*fact;
fld_pos = log10(fld.*(fld>0));
fld_pos(isinf(fld_pos))=0;
fld_pos(isnan(fld_pos))=0;
fld_pos(fld_pos<4) = 0;
fld_neg = log10(abs(fld.*(fld<=0)));
fld_neg(isinf(fld_neg))=0;
fld_neg(isnan(fld_neg))=0;
fld_neg(fld_neg<4) = 0;

bin_fld = fld_pos - fld_neg;

end
