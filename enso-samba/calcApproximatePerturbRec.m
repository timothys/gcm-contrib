function [dJ,dJdtau] = calcApproximatePerturbRec(refStr,perturbStr)
% Compute the expected response in the AMOC from sensitivities
% This is "equal" to calcDiffOfReconstructions.m and 
% calcPerturbationReconstruction.m, 
% but is supposed to be a more "intuitive" 
% approach because the reconstruction script is hardly readable
%
% Parameters
% ----------
%
%   refStr      ::  String specifying reference experiment
%                   e.g. no92.ref 
%   perturbStr  ::  String specifying perturbation experiment 
%                   e.g. xx92.p05.feb
%
% Output
% ------
%
%   dJ          ::  matrix giving SAMOC reconstruction from 
%                   zonal wind perturbation
%
% ----------------------------------------------------------------- 

% --- Load delta wind stress
[dtau] = calcDeltaZonalWind(refStr,perturbStr);

% --- Recreate perturbation
mskW = createPerturbMask('small');
mskW = convert2gcmfaces(mskW);
mskW(isnan(mskW))=0;
xx = 0.005 * convert2gcmfaces(mskW);

sensFrac = calcSamocSensFrac;

if ~isempty(strfind(perturbStr,'97'))
    % --- Create vector from Oct. to June.
    months = {'oct','nov','dec','jan','feb','march','april','may','june'};
    cost_ind=[ 238 , 239 , 240 , 229 , 230 ,   231 ,   232 , 233 ,  234 ];
    tau_lag =[   0 ,   1 ,   2 ,   3 ,   4 ,     5 ,     6 ,   7 ,    8 ];
    sf = circshift(sensFrac,3);
    sf = sf(1:length(months));
    msk = createPerturbMask('reg');
elseif ~isempty(strfind(perturbStr,'92')) || ~isempty(strfind(perturbStr,'11'))
    % --- Create vector from Oct. to June.
    months = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
    cost_ind= [229:240];
    tau_lag = [-1:10];
    sf = sensFrac(1:length(months));
    msk = createPerturbMask('small');
end

Nxy = nansum(msk(:));
dJ = zeros(1,length(months));
dJdtau = zeros(1,length(months));

% --- Compute average response 
for n = 1:length(months)

    % Load sensitivity
    matfile = sprintf('../samoc/mat/%s.240mo/adj_tauu.mat',months{n});
    load(matfile,'adxx');

    % Grab average sensitivity associated with specific time lag
    % Ignore months before perturbation has started with negative tau_lag
    if tau_lag(n) < 0
        dJ(n) = 0;
    else
        adj = adxx(:,:,cost_ind(n)-tau_lag(n));
        if tau_lag(n) == 0
            adj = sf(n)*adj;
        end

        dJ(n) = nansum(nansum(adj .* xx));
        dJdtau(n) = nansum(nansum(adj .* msk)) / Nxy;
    end
end

end
