function [mskW] = createPerturbMask(varargin)
maskType='reg';
for n=1:length(varargin)
    if ischar(varargin{n})
        maskType = varargin{n};
    end
end

mygrid = establish_mygrid('llc90');

if strcmp(maskType,'reg')
    % Grab thin latitude band +/- 10 deg of equator
    msks = createRegionMasks('tropPac10');
    
    % Mimic the Oct. 1997 El Nino wind with a box
    xCond = mygrid.XC<180 & mygrid.XC>160 | mygrid.XC<-160;
    mskW = msks.tropPac10.*xCond;

elseif strcmp(maskType,'small')
    msks = createRegionMasks('tropPac2_5');

    % Mimic the Oct. 1997 El Nino wind with a box
    xCond = mygrid.XC<180 & mygrid.XC>170;
    mskW = msks.tropPac2_5.*xCond;

elseif strcmp(maskType,'small_pyramid')

    % Call this function to get same small box
    mskW = createPerturbMask('small');

    % Now make it a big triangular prism in space
    % accumulate in x, both directions, to make two "ramp" functions
    % dimensions for cumsum are determined for this particular gcmface...
    x_cum_east = cumsum(mskW,2,'forward');
    x_cum_west = cumsum(mskW,2,'reverse');

    mele = x_cum_east <= x_cum_west;
    megt = x_cum_east >  x_cum_west;

    ramp_x = mele.*x_cum_east + megt.*x_cum_west;

    % same in y direction
    y_cum_north = cumsum(mskW,1,'reverse');
    y_cum_south = cumsum(mskW,1,'forward');

    mnle = y_cum_north <= y_cum_south;
    mngt = y_cum_north >  y_cum_south;

    ramp_y = mnle.*y_cum_north + mngt.*y_cum_south;

    % now make it a pyramid
    mskY = (ramp_y <= ramp_x).*ramp_y;
    mskX = (ramp_y >  ramp_x).*ramp_x;

    mskW = mskY+mskX;

    % Normalize so in [0,1]
    mskW = mskW/nanmax(mskW(:));

end

end
