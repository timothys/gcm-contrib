function [] = calcRegressedWindsOnENSO(varargin)
% Regress zonal wind anomalies on ENSO 3.4 index. 
% Specifically, consider regression of zonal wind anomalies in Pacific ocean 
% averaged over July-Feb on DJF ENSO3.4.
%
% Zonal wind and ENSO 3.4 diagnosed as monthly values from ECCOv4r2.
% 
% --------------------------------------------------------------------------

% --- input arguments for averaging type ... tbd
avgType = 'JulyThruFeb';
ensoVar = 'eccov4r2';
modelVar = 'eccov4r2';
atmVar = 'short-atm';
for n=1:length(varargin)
    if ischar(varargin{n}) 
        avgType=varargin{n};
    end
end


% --- mygrid, directories etc.
mygrid = establish_mygrid('llc90');


matDir = 'mat/reg/';
if ~exist(matDir,'dir'), mkdir(matDir); end;
matDir = sprintf('%s%s/',matDir,avgType);
if ~exist(matDir,'dir'), mkdir(matDir); end;
saveMatFile_u = sprintf('%stauu_regression_%s.mat',matDir,ensoVar);
saveMatFile_v = sprintf('%stauv_regression_%s.mat',matDir,ensoVar);

% --- Load it all up
[enso_ind,sst_anom,tauu_anom,tauv_anom] = loadEnsoAndWinds(ensoVar,modelVar,atmVar);

% --- Compute averages of winds and enso 3.4 (and SST anom for viewing pleasure)
[enso_avg, sst_avg, tauu_avg, tauv_avg] = calcAveragedIndexAndWinds(enso_ind, sst_anom, tauu_anom, tauv_anom, avgType);

% --- Now do the linear regression
[tauu_fit,tauv_fit,tauu_coeffs,tauv_coeffs] = calcLinearRegression(enso_avg,tauu_avg,tauv_avg);

% --- Compute relevant points for 95%, 99%, 99.9% confidence interval
%     where confidence interval considers either u or v correlation
Navg = size(tauu_avg.f1,3);
[cl_msk_95_u, cl_msk_95_u_slope,cl_msk_95_u_int] = calcConfidenceLevelMask(0.95 ,enso_avg,tauu_avg,tauu_fit,tauu_coeffs);
[cl_msk_95_v, cl_msk_95_v_slope,cl_msk_95_v_int] = calcConfidenceLevelMask(0.95 ,enso_avg,tauv_avg,tauv_fit,tauv_coeffs);

save(saveMatFile_u,'enso_avg','sst_avg','tauu_avg','tauu_fit','tauu_coeffs',...
                            'cl_msk_95_u','cl_msk_95_u_slope','cl_msk_95_u_int');
save(saveMatFile_v,'enso_avg','sst_avg','tauv_avg','tauv_fit','tauv_coeffs',...
                            'cl_msk_95_v','cl_msk_95_v_slope','cl_msk_95_v_int');

end

% --------------------------------------------------------------------------

function [enso_ind, sst_anom, tauu_anom, tauv_anom] = loadEnsoAndWinds(ensoVar,modelVar,atmVar)

% --- Load ENSO 3.4 index
ensoMatFile = sprintf('mat/%s/sst_enso_34.mat',ensoVar);
if ~isempty(strfind(ensoVar,'ecco'))
    load(ensoMatFile,'enso_ecco','sst_anom');
    enso_ind = enso_ecco; clear enso_ecco;
else
    error('Need to copy other enso indices from Ekman');
end

% --- Load winds
tauuMatFile = sprintf('../samoc/mat/exf/%s/%s/exf_tauu.mat',modelVar,atmVar);
tauvMatFile = sprintf('../samoc/mat/exf/%s/%s/exf_tauv.mat',modelVar,atmVar);
load(tauuMatFile,'exf_fld'); tauu=exf_fld;
load(tauvMatFile,'exf_fld'); tauv=exf_fld;

Nmo = size(exf_fld.f1,3);
Nyrs=Nmo/12;

% --- compute tauu anomaly
tauu = convert2gcmfaces(tauu);
tauu = convert2gcmfaces( tauu - repmat(nanmean(tauu,3),[1 1 Nmo]) );
clim = convert2gcmfaces(calcClimatology(tauu,Nyrs));
tauu_anom = tauu - convert2gcmfaces(repmat(clim,[1 1 Nyrs]));

% --- compute tauv anomaly
tauv = convert2gcmfaces(tauv);
tauv = convert2gcmfaces( tauv - repmat(nanmean(tauv,3),[1 1 Nmo]) );
clim = convert2gcmfaces(calcClimatology(tauv,Nyrs));
tauv_anom = tauv - convert2gcmfaces(repmat(clim,[1 1 Nyrs]));

end

% --------------------------------------------------------------------------

function [enso_avg, sst_avg, tauu_avg, tauv_avg] = calcAveragedIndexAndWinds(enso_ind, sst_anom, tauu_anom, tauv_anom, avgType)

yrs = 1992:2011;
Nyrs = length(yrs);
Nmo = Nyrs*12;

% --- Only look at 1993-2010, forget edge cases
Navg = Nyrs-1;
enso_avg = zeros(1,Navg);
sst_avg = 0*sst_anom(:,:,1:Navg);
tauu_avg = 0*tauu_anom(:,:,1:Navg);
tauv_avg = 0*tauv_anom(:,:,1:Navg);

% --- First ENSO 3.4: for year N grab avg(Dec N-1, Jan N, Feb N)
for n=1:Navg

    ind_beg = n*12;
    ind_end = n*12+2;
    enso_avg(n) = mean(enso_ind( ind_beg : ind_end ));
    sst_avg(:,:,n) = nanmean(sst_anom(:,:,ind_beg:ind_end),3 );
end
enso_avg = enso_avg - mean(enso_avg);
sigma_enso_avg = std(enso_avg);

% --- Avg July-Feb wind anomaly
if strcmp(lower(avgType),'julythrufeb')
    % Have Nyrs-1 records: for year N, avg(July N-1, ..., Dec N-1, Jan N, Feb N)
    ind_offset_1=7;
    ind_offset_2=14;
    fprintf('1. Averaging July-Feb wind stress field ...\n');

% --- Look at three month averages
elseif strcmp(lower(avgType),'marchthrumay')
    % Have Nyrs-1 records: for year N, avg(March N-1, April N-1, May N-1)
    ind_offset_1=3;
    ind_offset_2=5;
    fprintf('1. Averaging March-May wind stress field ...\n');
elseif strcmp(lower(avgType),'maythrujuly')
    % Have Nyrs-1 records: for year N, avg(May N-1, June N-1, July N-1)
    ind_offset_1=5;
    ind_offset_2=7;
    fprintf('1. Averaging May-July wind stress field ...\n');
elseif strcmp(lower(avgType),'julythrusept')
    % Have Nyrs-1 records: for year N, avg(July N-1, Aug N-1, Sept N-1)
    ind_offset_1=7;
    ind_offset_2=9;
    fprintf('1. Averaging July-Sept wind stress field ...\n');
elseif strcmp(lower(avgType),'septthrunov')
    % Have Nyrs-1 records: for year N, avg(Sept N-1, Oct N-1, Nov N-1)
    ind_offset_1=9;
    ind_offset_2=11;
    fprintf('1. Averaging Sept-Nov wind stress field ...\n');
elseif strcmp(lower(avgType),'octthrudec')
    % Have Nyrs-1 records: for year N, avg(Oct N-1, Nov N-1, Dec N-1)
    ind_offset_1=10;
    ind_offset_2=12;
    fprintf('1. Averaging Oct-Dec wind stress field ...\n');
elseif strcmp(lower(avgType),'novthrujan')
    % Have Nyrs-1 records: for year N, avg(Nov N-1, Dec N-1, Jan N)
    ind_offset_1=11;
    ind_offset_2=13;
    fprintf('1. Averaging Nov-Jan wind stress field ...\n');
elseif strcmp(lower(avgType),'decthrufeb')
    % Have Nyrs-1 records: for year N, avg(Dec N-1, Jan N, Feb N)
    ind_offset_1=12;
    ind_offset_2=14;
    fprintf('1. Averaging Dec-Feb wind stress field ...\n');


% --- Look at individual months
elseif strcmp(lower(avgType),'march')
    ind_offset_1 = 3;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging March wind stress field ...\n');
elseif strcmp(lower(avgType),'april')
    ind_offset_1 = 4;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging April wind stress field ...\n');
elseif strcmp(lower(avgType),'may')
    ind_offset_1 = 5;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging May wind stress field ...\n');
elseif strcmp(lower(avgType),'june')
    ind_offset_1 = 6;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging june wind stress field ...\n');
elseif strcmp(lower(avgType),'july')
    ind_offset_1 = 7;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging July wind stress field ...\n');
elseif strcmp(lower(avgType),'aug')
    ind_offset_1 = 8;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging August wind stress field ...\n');
elseif strcmp(lower(avgType),'sept')
    ind_offset_1 = 9;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging September wind stress field ...\n');
elseif strcmp(lower(avgType),'oct')
    ind_offset_1 = 10;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging October wind stress field ...\n');
elseif strcmp(lower(avgType),'nov')
    ind_offset_1 = 11;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging November wind stress field ...\n');
elseif strcmp(lower(avgType),'dec')
    ind_offset_1 = 12;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging December wind stress field ...\n');
elseif strcmp(lower(avgType),'jan')
    ind_offset_1 = 13;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging January wind stress field ...\n');
elseif strcmp(lower(avgType),'feb')
    ind_offset_1 = 14;
    ind_offset_2=ind_offset_1;
    fprintf('1. Averaging February wind stress field ...\n');
end

% --- Do the averaging
for n=1:Navg
    ind_beg = (n-1)*12+ind_offset_1;
    ind_end = (n-1)*12+ind_offset_2;
    tmp_u = nanmean( tauu_anom(:,:,ind_beg:ind_end), 3 );
    tmp_v = nanmean( tauv_anom(:,:,ind_beg:ind_end), 3 );
    tauu_avg(:,:,n) = tmp_u;
    tauv_avg(:,:,n) = tmp_v;
    if ceil(n/10)-n/10 < eps
       fprintf(' --- Done %d / %d --- \n',n,Navg);
    end
end

end

% --------------------------------------------------------------------------

function [tauu_fit, tauv_fit, tauu_coeffs, tauv_coeffs] = calcLinearRegression(enso_avg,tauu_avg,tauv_avg)


% --- Convert to matrix/array format for polyfit
tauu_avg = convert2array(tauu_avg);
tauv_avg = convert2array(tauv_avg);
tauu_fit = 0*tauu_avg;
tauv_fit = 0*tauv_avg;
tauu_coeffs = 0*tauu_avg(:,:,1:2);
tauv_coeffs = 0*tauv_avg(:,:,1:2);

Nx=size(tauu_avg,1);
Ny=size(tauu_avg,2);
Navg=size(tauu_avg,3);

fprintf('2. Computing linear regression ...\n');
for i=1:Nx
    for j=1:Ny

        % --- Compute zonal fit
        windSeries = reshape(tauu_avg(i,j,:),[1 Navg]);
        tmp_coeffs = polyfit( enso_avg, windSeries, 1);
        tmp_fit = polyval(tmp_coeffs, enso_avg);

        tauu_fit(i,j,:) = tmp_fit;
        tauu_coeffs(i,j,:) = tmp_coeffs;

        % --- Compute meridional fit
        windSeries = reshape(tauv_avg(i,j,:),[1 Navg]);
        tmp_coeffs = polyfit( enso_avg, windSeries, 1);
        tmp_fit = polyval(tmp_coeffs, enso_avg);

        tauv_fit(i,j,:) = tmp_fit;
        tauv_coeffs(i,j,:) = tmp_coeffs;
    end

    if ceil((i*Ny)/10^3)-(i*Ny)/10^3 < eps
        fprintf(' --- Done %d / %d --- \n',i*Ny,Nx*Ny);
    end
end

% --- Convert back to gcmfaces
tauu_avg = convert2array(tauu_avg);
tauu_coeffs=convert2array(tauu_coeffs);
tauu_fit = convert2array(tauu_fit);

tauv_avg = convert2array(tauv_avg);
tauv_coeffs=convert2array(tauv_coeffs);
tauv_fit = convert2array(tauv_fit);

end

% --------------------------------------------------------------------------

function [cl_msk,cl_msk_slope,cl_msk_int] = calcConfidenceLevelMask(cl,x,y,y_fit,coeffs)
% Compute confidence interval for fit y_fit of y to the index x_i  
% Here y & y_fit are 3D gcmfaces objects, [ spatial_dim_1, spatial_dim_2, i ]
% where i covers the index. 
% At each point in space, we have a series over the index x_i and compute 
% the confidence interval as
% 
%   cl_interval = t(alpha/2, n-2) * sqrt( MSE ) * 
%                   sqrt ( 1/n (x_i-x_bar) ./ sum_i(x_i-x_bar) )
%
% where
%
%   cl              = confidence interval, e.g 95%, = (1-alpha)*100%
%   t(alpha/2,n-2)  = two tailed the critical value at alpha/2, 
%                     n-2 degrees of freedom t-distribution
%   n-2             = degrees of freedom for error
%   MSE             = sum( y-y_fit) / (n-2), mean squared error
%   x_bar           = mean(x_i), avg of index
%
% For slope and intercept parameters we have
%
%   cl_interval_slope = t(alpha/2,n-2)*sqrt(MSE)/sqrt(sum(ssi))
%
%   cl_interval_slope = t(alpha/2,n-2)*sqrt(MSE)*(1/sqrt(n) + mean(x)/sum(ssi) )
%
% and for slope, assume the null hypothesis: no relationship between x & y, so
% that slope is zero. cl_msk returns points where null hypothesis is rejected to 
% specified confidence interval (i.e. 0 not in the confidence interval).
%
% similar for intercept, null hypothesis: intercept is zero.
% This is relatively meaningless for most cases.
%
% Returns:
%   cl_msk          = gcmfaces mask same dimensions as y_fit giving 
%                     values in the confidence interval
%   cl_msk_slope    = same, but for the slope parameter of the fit
%   cl_msk_int      = same, for intercept parameter
% --------------------------------------------------------------------------

% Compute degrees of freedom
n = length(x);

% get two sided confidence level prob. and positive answer from matlab
cl = 1-(1-cl)/2;
t_cl = tinv(cl,n-2);

% Compute mean squared error, repeat as gcmfaces object over series data
mse = convert2gcmfaces( nansum( (y-y_fit).^2, 3 ) / (n-2) );
mse = convert2gcmfaces( repmat(mse,[1 1 n]) );

% Compute the final sqrt() term, repeat at each spatial point as a gcmfaces object
ssi = (x - mean(x) ).^2;
num = sqrt( (1/n) + ( ssi./sum(ssi) ) );
num_g = 0*y;
for i=1:n
    num_g(:,:,i) = num_g(:,:,i)+num(i);
end


% Compute the interval
cl_interval = t_cl * sqrt(mse) .* num_g;

% Turn it into a mask
cl_msk = y >= (y_fit-cl_interval) & y <= (y_fit + cl_interval);

% Do a similar computation for the coefficients
slope_interval = t_cl*sqrt(mse(:,:,1))/sqrt(sum(ssi));
cl_msk_slope = (coeffs(:,:,1)-slope_interval).*(coeffs(:,:,1)+slope_interval) > 0;

int_interval   = t_cl*sqrt(mse(:,:,1))*(1/sqrt(n)+mean(x)/sum(ssi));
cl_msk_int = (coeffs(:,:,2)-int_interval).*(coeffs(:,:,2)+int_interval) > 0;

end
