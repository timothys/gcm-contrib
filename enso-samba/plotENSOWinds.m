function [] = plotENSOWinds()
% Plot zonal wind stress anomalies for Nino, Nina, 
% and regular December months
%
% ----------------------------------------------

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');

% parameters
modelVar='eccov4r2';
atmVar='short-atm';

tauu_file = sprintf('../samoc/mat/exf/%s/%s/exf_tauu.mat',modelVar,atmVar);
load(tauu_file);

% --- Remove mean
exf_fld = convert2gcmfaces(  ...
        convert2gcmfaces(exf_fld) -  ...
        repmat(nanmean(convert2gcmfaces(exf_fld),3),[1 1 size(exf_fld.f1,3)]) ...
        );
% --- Remove clim
Nyrs=20;
clim = convert2gcmfaces(calcClimatology(exf_fld,Nyrs));
exf_fld = exf_fld - convert2gcmfaces(repmat(clim,[1 1 Nyrs]));

plotWWB(exf_fld);

% --- Plot 1997
inds_1997 = [1:12]+5*12;
lbls_1997 = {'Jan. 1997','Feb. 1997','Mar. 1997','Apr. 1997','May 1997',...
             'Jun. 1997','Jul. 1997','Aug. 1997',...
             'Sep. 1997','Oct. 1997','Nov. 1997','Dec. 1997'};
for i=1:length(inds_1997)
    ensoPlotter(exf_fld,inds_1997(i),lbls_1997{i});
end
keyboard

% --- Plot 2006
inds_2006 = [1:12]+14*12;
lbls_2006 = {'Jan. 2006','Feb. 2006','Mar. 2006','Apr. 2006','May 2006',...
             'Jun. 2006','Jul. 2006','Aug. 2006',...
             'Sep. 2006','Oct. 2006','Nov. 2006','Dec. 2006'};
for i=1:length(inds_2006)
    ensoPlotter(exf_fld,inds_2006(i),lbls_2006{i});
end
keyboard

% --- Plot 2008
inds_2008 = [1:5]+16*12;
lbls_2008 = {'Jan. 2008','Feb. 2008','Mar. 2008','Apr. 2008','May 2008'};%,...
             %'Jun. 2008','Jul. 2008','Aug. 2008',...
             %'Sep. 2008','Oct. 2008','Nov. 2008','Dec. 2008'};
for i=[] %1:length(inds_2008)
    ensoPlotter(exf_fld,inds_2008(i),lbls_2008{i});
end
keyboard

% --- Plot 2011
inds_2011 = [1:5]+19*12;
lbls_2011 = {'Jan. 2011','Feb. 2011','Mar. 2011','Apr. 2011',...
             'May 2011','Jun. 2011','Jul. 2011','Aug. 2011',...
             'Sep. 2011','Oct. 2011','Nov. 2011','Dec. 2011'};
for i=[] %1:length(inds_2011)
    ensoPlotter(exf_fld,inds_2011(i),lbls_2011{i});
end
keyboard

% --- Grab "off" correlation months
off_inds = [[15,18,19]*12-2,16*12+8,20*12-7] ;
off_lbls = {'Oct. 2006','Oct. 2009','Oct. 2010','Jul. 2008',...
            'May 2011'};

for i=[] %1:length(off_inds)
    ensoPlotter(exf_fld,off_inds(i),off_lbls{i});
end
keyboard

% --- Plot Nino winds
nino_inds = [3,6,11,13,15,18]*12-2;
nino_lbls = {'Oct. 1994','Oct. 1997','Oct. 2002','Oct. 2004','Oct. 2006','Oct. 2009'};

for i=1:length(nino_inds)
    ensoPlotter(exf_fld,nino_inds(i),nino_lbls{i});
end
keyboard

% Plot Nina winds
nina_inds = [4,7,8,9,14,16,17,19]*12-2;
nina_lbls = {'Oct. 1996','Oct. 1999','Oct. 2000','Oct. 2001','Oct. 2006','Oct. 2008','Oct. 2009','Oct. 2011'};

for i=1:length(nina_inds)
    ensoPlotter(exf_fld,nina_inds(i),nina_lbls{i});
end
keyboard

end

% ----------------------------------------------


function [] = ensoPlotter(exf_fld,ind,lbl)

cmax = .09; % nanmax(abs(exf_fld));
cmap = cbrewer2('RdBu',9);
cmap=flipdim(cmap,1);
proj = 4.5;

figureW;
m_map_atl(exf_fld(:,:,ind),proj)
cm = nanmax(abs(exf_fld(:,:,ind)));
caxis([-cmax cmax]);
colormap(cmap);
niceColorbar;
title(lbl);


end

% ----------------------------------------------

function [] = plotWWB(exf_fld)
% Make plot similar to McPhaden 2015 paper which shows WWB in 1975
% 

global mygrid;

% Grab latitude band +/- 10 deg of equator
msks = createRegionMasks('tropPac10');

% Apply mask to exf_fld
% and create longitude -vs- time matrix
exf_fld=convert2gcmfaces(exf_fld);
tropPacMask=convert2gcmfaces(msks.tropPac10);

% Mask out the tropical band
trop_pac = convert2gcmfaces(exf_fld .* repmat(tropPacMask,[1 1 size(exf_fld,3)]));

% Compute mean in meridional direction (wwb is lon vs time)
lon_bounds = [130 -80];
[wwb, lon] = calcMeridionalMean( trop_pac, [120 -79], msks.tropPac10, 2 );

%% Convert 80E to 280W
%wwb = [wwb(lon>=80,:); wwb(lon<80,:)];
%lon = [lon(lon>=80); lon(lon<80)];
%lon(lon>=80) = lon(lon>=80)-360;

% Grab indices related to 230W (130E to 79W
%pac_ind = lon>-230 & lon<-79;
%lon=lon(pac_ind);
%wwb=wwb(pac_ind,:);

% Flip dims for plotting similar to McPhaden
wwb = wwb';
Ncolors = 9;
t=[1/12:1/12:20]+1992;
%cmax=nanmax(abs(wwb(:)));
cmax=0.09;
cmap=cbrewer2('RdBu',Ncolors);
cmap=flipdim(cmap,1);

% Do the plotting
figureL;
%contourf(lon,t,wwb,Ncolors)
pcolor(lon,t,wwb)
shading interp
set(gca,'ydir','reverse')
set(gca,'xtick',[lon(1):30:lon(end)]);
set(gca,'xticklabel',{'$120^\circ$ E','$150^\circ$ E','$180^\circ$ W','$150^\circ$ W','$120^\circ$ W','$90^\circ$ W'});
colormap(cmap)
caxis([-cmax cmax]);
niceColorbar;

keyboard



end

