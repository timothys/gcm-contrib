function [] = plotBaroclinicWaves(varargin)
% Diagnose 20C isotherm from temperature diagnostics 
% visualize baroclinic mode resulting from perturbed wind field
%
% Input: perturbStr : indicate which perturbation to look at
%           'en97.m05' , 'en97.p05' (default)
%           'no93.m05' , 'no93.p05'
%
% NOTE: This turned out to show the same thing as ETA but is much more noisy...
%
% --------------------------------------------------------------------
perturbStr='en97.p05';
for n=1:length(varargin)
    if iscell(varargin{n})
        perturbStr=varargin{n};
    end
end

mygrid=establish_mygrid('llc90');

% Set up load and save dirs
pert_mat_dir = sprintf('mat/%s/',perturbStr);
if ~exist(pert_mat_dir),mkdir(pert_mat_dir); end;
pert_matfile = [pert_mat_dir 'twentyC_isotherm_depth.mat'];

if ~exist(pert_matfile,'file')
    [twentyC_depth_pert,twentyC_depth_ref] = calcTwentyCisotherm(perturbStr,pert_matfile);
else
    load(pert_matfile,'twentyC_depth_pert','twentyC_depth_ref');
end



keyboard



end

% --------------------------------------------------------------------

function [twentyC_depth_pert,twentyC_depth_ref] = calcTwentyCisotherm(perturbStr,pert_matfile)

global mygrid;

% --- Set up results dirs
ref_results_dir = '../../results/enso-samba/ref/diags/';
pert_results_dir = sprintf('../../results/enso-samba/%s/diags/',perturbStr);

diagsName='ts_monthly';
iters=grabAllIters(ref_results_dir,diagsName);
Nmo = length(iters);
chunkSize=50;

Nr = length(mygrid.RC);
depth_mygrid = mk3D(mygrid.RC,mygrid.mskC);
depth_mygrid = convert2gcmfaces( ...
                repmat(convert2gcmfaces(depth_mygrid),[1 1 1 chunkSize]));
twentyC_depth_pert = 0*convert2gcmfaces( ...
                       repmat(convert2gcmfaces(mygrid.XC),[1 1 Nmo]));
twentyC_depth_ref  = twentyC_depth_pert;

fprintf('Computing 20C isotherm ...\n');

for n=1:chunkSize:Nmo

    % --- Load up some of the diagnostics
    ind = n:min(n+chunkSize-1,Nmo);
    indL = length(ind);
    theta_ref = rdmds2gcmfaces([ref_results_dir diagsName],iters(ind),'rec',1);
    theta_pert= rdmds2gcmfaces([pert_results_dir diagsName],iters(ind),'rec',1);

    % --- Find temperature closest to 20C
    theta_twenty = 0*theta_ref + 20;

    theta_diff_ref = abs(theta_ref - theta_twenty);
    theta_diff_pert= abs(theta_pert- theta_twenty);

    theta_min_ref = nanmin(theta_diff_ref,[],3);
    theta_min_pert= nanmin(theta_diff_pert,[],3);

    % --- Define a mask showing where in 3D (and time) space this close to 20C point is
    theta_twenty_ref_msk = abs(convert2gcmfaces( ...
                            repmat(convert2gcmfaces(theta_min_ref),[1 1 Nr 1]))  - ...
                            theta_diff_ref) < eps;
    theta_twenty_pert_msk = abs(convert2gcmfaces( ...
                            repmat(convert2gcmfaces(theta_min_pert),[1 1 Nr 1]))  - ...
                            theta_diff_pert) < eps;

    % --- Omit parts in the domain which don't get to 20C
    omit_points_ref = convert2gcmfaces(...
                        repmat(convert2gcmfaces(nansum(theta_twenty_ref_msk,3) > 1),[1 1 Nr 1]));
    omit_points_pert = convert2gcmfaces(...
                        repmat(convert2gcmfaces(nansum(theta_twenty_pert_msk,3) > 1),[1 1 Nr 1]));

    twentyC_depth_ref(:,:,ind) = squeeze(nansum(depth_mygrid(:,:,:,1:indL).*theta_twenty_ref_msk.*~omit_points_ref,3));
    twentyC_depth_pert(:,:,ind)= squeeze(nansum(depth_mygrid(:,:,:,1:indL).*theta_twenty_pert_msk.*~omit_points_pert,3));

    fprintf(' --- Done %d / %d ---\n',min(n+chunkSize-1,Nmo),Nmo);

end


save(pert_matfile,'twentyC_depth_pert','twentyC_depth_ref');

end



    
