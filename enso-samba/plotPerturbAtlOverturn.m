function [] = plotPerturbAtlOverturn(varargin)
% Show samoc as a result of perturbed wind field during el nino
%
% Input: perturbCell : indicate which perturbation to look at
%           {'en97.m05' , 'en97.p05'} (default)
%           {'no93.m05' , 'no93.p05'}
%
% --------------------------------------------------------------------
perturbCell={'en97.m05','en97.p05'};
for n=1:length(varargin)
    if iscell(varargin{n})
        perturbCell=varargin{n};
    end
end

% Add samoc dir to path for calcAMOC functions
addpath(genpath('../samoc'));

% Set up load and save dirs
ref_results_dir = '../../results/enso-samba/ref/diags/';
ref_mat_dir = 'mat/ref/';
if ~exist(ref_mat_dir),mkdir(ref_mat_dir); end;

pert_results_dir_m = sprintf('../../results/enso-samba/%s/diags/',perturbCell{1});
pert_mat_dir_m = sprintf('mat/%s/',perturbCell{1});
pert_results_dir_p = sprintf('../../results/enso-samba/%s/diags/',perturbCell{2});
pert_mat_dir_p = sprintf('mat/%s/',perturbCell{2});
if ~exist(pert_mat_dir_p),mkdir(pert_mat_dir_p); end;
if ~exist(pert_mat_dir_m),mkdir(pert_mat_dir_m); end;

diagsName='trsp_monthly';

% Didn't output diags at final time...
Nt=239;
t = [1:Nt]./12 + 1992;

% Compute AMOC at various latitudes
lats = [-34:25]';
Nlats = length(lats);
ref_amoc = zeros(Nlats,Nt);
per_amoc_m=zeros(Nlats,Nt);
per_amoc_p=zeros(Nlats,Nt);
legendStr_m = cell(Nlats,1);
legendStr_p = cell(Nlats,1);

for l=1:Nlats
    [ref_amoc(l,:)] = calcAMOC(lats(l),'eulerian',ref_mat_dir,ref_results_dir,diagsName);
    [per_amoc_m(l,:)] = calcAMOC(lats(l),'eulerian',pert_mat_dir_m,pert_results_dir_m,diagsName);
    [per_amoc_p(l,:)] = calcAMOC(lats(l),'eulerian',pert_mat_dir_p,pert_results_dir_p,diagsName);
    legendStr_m{l} = sprintf('%d, (-.05)',lats(l));
    legendStr_p{l} = sprintf('%d, (+.05)',lats(l));
end


% Plot perturbed AMOC diff vs latitude

% First plot maximum amplitude vs latitude
figureW; 
cmap1=cbrewer2('Set1',2);
ph1=plot(lats,nanmax(per_amoc_p-ref_amoc,[],2),lats,nanmin(per_amoc_m-ref_amoc,[],2));
set(ph1,{'color'},num2cell(cmap1,2));
ylabel('$\max|\delta$AMOC$|$')
xlabel('Latitude')
legend('$\max|\delta$AMOC$_+|$)','$\max|\delta$AMOC$_-|$');
grid on;
savePlot(sprintf('figures/max_deltaAmocVsLat_%s',perturbCell{1}(1:4)),'pdf');

% Now show most latitudes up to North
figureW;
latInd = 1:5:Nlats;
cmap2=cbrewer2('Paired',length(latInd));
ph2=plot(t,per_amoc_p(latInd,:)-ref_amoc(latInd,:));
set(ph2,{'color'},num2cell(cmap2,2));
ylabel('$\delta$AMOC')
if ~isempty(strfind(perturbCell{1},'en97'))
    xlim([1997.5 2001]);
else
    xlim([1993.5 1997]);
end
legend(legendStr_p{latInd})
grid on;
savePlot(sprintf('figures/deltaAmocVsTime_all_%s',perturbCell{1}(1:4)),'pdf');

% Now zoom in on South
figureW;
latInd = 1:10;
cmap3=cbrewer2('Paired',length(latInd));
ph3=plot(t,per_amoc_p(latInd,:)-ref_amoc(latInd,:));
set(ph3,{'color'},num2cell(cmap3,2));
ylabel('$\delta$AMOC')
if ~isempty(strfind(perturbCell{1},'en97'))
    xlim([1997.5 2001]);
else
    xlim([1993.5 1997]);
end

legend(legendStr_p{latInd})
grid on;
savePlot(sprintf('figures/deltaAmocVsTime_sh_%s',perturbCell{1}(1:4)),'pdf');

keyboard

end
