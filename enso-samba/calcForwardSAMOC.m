function [samoc] = calcForwardSAMOC(runStr,diagFreq)
% Compute the SAMOC from simulation denoted by runStr at frequency diagFreq
% Uses m_trVol_monthly if diagFreq = 'monthly' and files exist
%
% Parameters
% ----------
%
%   runStr      ::  string denoting simulation, 
%                   e.g. 'no92.ref' or 'xx92.p05.feb'
%   diagFreq    ::  Frequency of diagnostics to use
%                   'monthly' or 'daily'
%
% Output
% ------
%
%   samoc       ::  vector with amoc values over records in simulation
%
% ---------------------------------------------------------------

% --- Add samoc dir to path for calcAMOC and calcBarfileAmoc functions
addpath(genpath('../samoc'));

% --- Set up results dirs
results_dir = sprintf('../../results/enso-samba/%s',runStr);

% --- First check for results directory ...
if ~exist(results_dir,'dir')
    error(sprintf('Could not find dir: %s',results_dir))
end

% --- Next, check for barfiles (m_trVol) and try to use these for computation 
%   only use barfiles if diagFreq is monthly, wouldn't use this for cost function
if ~isempty(dir(sprintf('%s/m_trVol*',results_dir))) && strcmp(diagFreq,'monthly')
    doBarfileCalc = 1;
else
    doBarfileCalc = 0;
    fprintf('calcForwardSAMOC: Could not find barfile m_trVol or diagFreq is not set to monthly.\n -> Computing SAMOC from diags\n');
end

% --- Calculate AMOC
if doBarfileCalc
    mat_dir = sprintf('mat/%s/',runStr);;
    if ~exist(mat_dir),mkdir(mat_dir); end;
    mat_dir = sprintf('%s/monthly-barfiles/',mat_dir);;
    if ~exist(mat_dir),mkdir(mat_dir); end;
    mat_file = sprintf('%s/fwd_samoc.mat',mat_dir);
    
    if ~exist(mat_file,'file')
        [samoc] = calcBarfileAmoc(results_dir,'month');
        save(mat_file,'samoc');
    else
        load(mat_file,'samoc');
    end
else
    % --- For calcAMOC, set up directories
    mat_dir = sprintf('mat/%s/',runStr);;
    if ~exist(mat_dir),mkdir(mat_dir); end;
    mat_dir = sprintf('%s/%s/',mat_dir,diagFreq);;
    if ~exist(mat_dir),mkdir(mat_dir); end;

    % --- Grab diagnostics and compute
    lat         = -34;
    velocity    = 'eulerian';
    diagsName   = sprintf('trsp_%s',diagFreq);
    diags_dir   = sprintf('%s/diags/',results_dir);

    % --- If already computed, calcAMOC grabs matfile
    [samoc] = calcAMOC(lat,velocity,mat_dir,diags_dir,diagsName);
end

end

