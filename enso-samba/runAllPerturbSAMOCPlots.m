function [] = runAllPerturbSAMOCPlots()
% Driver script to process all perturbation results 

% --- For now, only for 1992
pYear = '92';

% --- First all xx, monthly results
pType       = 'xx';
diagFreq    = 'monthly';

plotPerturbSAMOC(pType,pYear,'p05','feb',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p05','mar',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p005','feb',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p1','feb',diagFreq);
close all;

% --- Now all hacked, monthly results
pType       = 'hk';

plotPerturbSAMOC(pType,pYear,'p05','feb',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p05','mar',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p005','feb',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p1','feb',diagFreq);
close all;

% --- Now get those daily plots... just because
pType       = 'xx';
diagFreq    = 'daily';

plotPerturbSAMOC(pType,pYear,'p05','feb',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p05','mar',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p005','feb',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p1','feb',diagFreq);
close all;

pType       = 'hk';

plotPerturbSAMOC(pType,pYear,'p05','feb',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p05','mar',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p005','feb',diagFreq);
close all;
plotPerturbSAMOC(pType,pYear,'p1','feb',diagFreq);
close all;

end
