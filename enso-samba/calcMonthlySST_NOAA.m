function [] = calcMonthlySST_NOAA()
% Take daily 1/4 degree SST data and compute monthly avg values
% This uses the daily mean and anomaly data from:
%   NOAA OI SST V2 High Resolution Dataset from AVHRR data
%   https://www.esrl.noaa.gov/psd/data/gridded/data.noaa.oisst.v2.highres.html
%
% Notes:
%   - each SST file is ~480MB
%   - -9.96921e+36 implies missing data, I convert this to NaN
%   - Reference:
%     Reynolds, Richard W., (et al) 2007: Daily High-Resolution-Blended 
%     Analyses for Sea Surface Temperature. J. Climate, 20, 5473-5496. 
%
% ----------------------------------------------------------------------------


% Data information
data_dir = '../../data/noaa_sst/';
mean_file_prefix = 'sst.day.mean.';
anom_file_prefix = 'sst.day.anom.';
suffix = '.nc';

% Save location
mat_dir = 'mat/noaa_high_res/';
if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_file = [mat_dir 'sst_monthly.mat'];

yrs = 1992:2011;
Nyrs=length(yrs);
Nmo = 12*Nyrs;
day_per_month = [0 31 28 31 30 31 30 31 31 30 31 30 31];
day_in_year = cumsum(day_per_month);
day_per_month_leap = [0 31 29 31 30 31 30 31 31 30 31 30 31];
day_in_year_leap = cumsum(day_per_month_leap);
leapYrs = 1992:4:2011;

% Knowing the dimensions required
% meta=ncinfo( *.nc )
% meta.Dimensions.Name, meta.Dimensions.Length
% lat = 720, lon = 1440
sst_monthly = zeros(1440,720,Nmo);


for i=1:Nyrs

    % Load mean and anomaly
    sst_mean = ncread(sprintf('%s%s%d%s',data_dir,mean_file_prefix,yrs(i),suffix),'sst');
    sst_anom = ncread(sprintf('%s%s%d%s',data_dir,anom_file_prefix,yrs(i),suffix),'anom');

    % Remove missing values
    sst_mean(abs(sst_mean)>10^10)=NaN;
    sst_anom(abs(sst_anom)>10^10)=NaN;

    % Test for leap year
    if nansum(yrs(i) == leapYrs) == 1
        diy = day_in_year_leap;
    else
        diy = day_in_year;
    end

    for j=1:12

        % Compute mean over each month
        mo_ind = (i-1)*12 + j;
        day_ind_st = diy(j) + 1;
        day_ind_ed = diy(j+1);

        sst_days = sst_mean(:,:,day_ind_st:day_ind_ed) + ...
                   sst_anom(:,:,day_ind_st:day_ind_ed);

        sst_monthly(:,:,mo_ind) = nanmean(sst_days,3);
    end

    fprintf(' --- Done with %d ---\n',yrs(i));

end

% Prep for saving, grab lat and longitude
sst = sst_monthly;
lat = ncread(sprintf('%s%s%d%s',data_dir,mean_file_prefix,1992,suffix),'lat');
lon = ncread(sprintf('%s%s%d%s',data_dir,mean_file_prefix,1992,suffix),'lon');

save(mat_file,'sst','lat','lon');

end


