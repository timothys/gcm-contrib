function [enso,sst_anom,lat,lon] = calcENSOFromObs(obs_name)
% Compute ENSO 3.4 index and SST's for famous EN years
% From various SST products
% Note: this function works both with SST data on ECCO grid structure
%       or in lat/lon form as from netcdf file, hence lots of code.
%
% Input:
%
%   obs_name: string denoting obs product to use
%       'reynolds' - reynolds sst product used in ECCO
%                    monthly SST averages
%
%
% ----------------------------------------------

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');

saveDir = sprintf('mat/%s/',obs_name);
if ~exist(saveDir,'dir'),mkdir(saveDir); end;
enso_sst_file = [saveDir 'sst_enso.mat'];

if ~exist(enso_sst_file,'file')
    % Load monthly averaged SST anomalies
    % For now: Assume 1992-2011 
    [sst,lat,lon] = loadSST(obs_name);
    
    
    % Compute sst anomaly (from climatology)
    [sst_anom] = calcAnomaly(sst,lat);
    [enso] = calcENSOIndices(sst_anom,lat,lon);

    save(enso_sst_file,'sst_anom','enso','lat','lon');
else
    load(enso_sst_file,'sst_anom','enso','lat','lon');
end

end

% ------------------------------------------------------


function [sst_anom] = calcAnomaly(sst,lat)
% Compute full anomaly, not just for ENSO 3.4 region

Nyrs=20;

if isempty(lat)
    % Data are on mygrid
    Nt = size(sst.f1,3);
    sst = sst - ...
            convert2gcmfaces( ...
            repmat(convert2gcmfaces(nanmean(sst,3)),[1 1 Nt]));
    clim=calcClimatology(sst,Nyrs);
    sst_anom=sst - ...
               convert2gcmfaces( ...
               repmat(convert2gcmfaces(clim),[1 1 Nyrs]) );
else
    % Data are on lat/lon specified by vectors
    Nt = size(sst,3);
    sst = sst - repmat(nanmean(sst,3),[1 1 Nt]);
    clim = calcClimatology(sst,Nyrs);
    sst_anom = sst - repmat(clim,[1 1 Nyrs]);
end

end

% ------------------------------------------------------

function [sst,lat,lon] = loadSST(obs_name)

global mygrid;

% For now, assume monthly fields 1992-2011
yrs = 1992:2011;
Nt = 240;

% Need these for obs data not on mygrid
lat=[];
lon=[];

if strcmp(obs_name,'reynolds')
    data_dir = '../../data/ecco/reynolds_sst/';

    sst = NaN*convert2gcmfaces( ...
        repmat( convert2gcmfaces(mygrid.XC), [1 1 Nt] ) );

    for n=1:length(yrs)
        sst(:,:,(n-1)*12+1:n*12) = read_bin(...
            sprintf('%sreynolds_oiv2_r1_sea_ice_masked_%d',data_dir,yrs(n)));
    end
    sst(sst<-1000)=NaN;

elseif strcmp(obs_name,'noaa_high_res')

    % NOAA 1/4 resolution SST data, preprocessed to monthly averages via
    % calcMonthlySST_NOAA

    mat_file = 'mat/noaa_high_res/sst_monthly.mat';
    load(mat_file,'sst','lat','lon');

elseif strcmp(obs_name,'noaa_low_res')

    % NOAA SST data from Dec. 1981 -> April 2018
    data_dir = '../../data/noaa_sst/';

    % Find Jan 1992
    ind92 = 122;

    sst = ncread([data_dir 'sst.mnmean.nc'],'sst');
    sst = sst(:,:,ind92:ind92+Nt-1);
    lat = ncread([data_dir 'sst.mnmean.nc'],'lat');
    lon = ncread([data_dir 'sst.mnmean.nc'],'lon');

else
    error(sprintf('Obs type %s not recognized ...',obs_name));
end
end

