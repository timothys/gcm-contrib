function [dtau,exf_fld,dtau_avg] = calcDeltaZonalWind(refStr,perturbStr,diagFreq,atmVar);
% Compute the perturbed wind stress field
%   
%   tauu_p - tauu_r
%
% Save in a specific format for the reconstruction script...
%
% Parameters
% ----------
%
%   refStr      ::  String specifying reference experiment
%                   e.g. no92.ref 
%   perturbStr  ::  String specifying perturbation experiment 
%                   e.g. xx92.p05.feb
%   diagFreq    ::  Frequency of diagnostics to load
%                   'monthly' (default) or 'daily'
%                   if daily specified, exf_fld = dtau
%   atmVar      ::  String specifying forcing to generate for reconstruction
%                   'short-atm' (default) or 'long-atm'
% 
% Output
% ------
%
%   dtau        ::  difference tauu_p - rauu_r
%   exf_fld     ::  difference placed in "the right spot" within 
%                   records for ECCO type reconstruction
%   dtau_avg    ::  vector giving average perturbation strength over 
%                   nonzero area
%
% ---------------------------------------------------------------

if nargin<3
    diagFreq='monthly';
end
if nargin<4
    atmVar='short-atm';
end


% --- matfile with zonal wind delta
matfile = sprintf('mat/%s/%s/%s/tauu_diff.mat',perturbStr,diagFreq,atmVar);

if ~exist(matfile,'file')

    % --- Make sure directories exist
    %   First two subdirs made by loadZonalWind
    matdir = sprintf('mat/%s/%s/%s',perturbStr,diagFreq,atmVar);
    if ~exist(matdir,'dir'), mkdir(matdir); end;

    mygrid = establish_mygrid('llc90');

    % --- Load zonal wind from each run
    [tauu_r] = loadZonalWind(sprintf('%s',refStr),diagFreq);
    [tauu_p] = loadZonalWind(sprintf('%s',perturbStr),diagFreq);
    
    % --- Compute difference
    dtau = tauu_p - tauu_r;
    dtau(isnan(dtau)) = 0;
    
    % --- Save as exf_fld with 20yr length for reconstruction
    exf_fld = convert2gcmfaces(dtau);
    
    % --- For reconstruction, make vector same size as ECCO, i.e. 240mo
    %   but with zeros everywhere but during perturb simulation months
    %   Only do this if monthly fieldss desired
    if strcmp(diagFreq,'monthly')
        if strcmp(atmVar,'short-atm')
            Nt = 240;
        elseif strcmp(atmVar,'long-atm')
            Nt_ecco = 240;
            Nt_full = 396;
        else
            error(sprintf('calcDeltaZonalWind: do not recognize atm-var %s',atmVar))
        end
        
        if ~isempty(strfind(perturbStr,'92')) 
        
            if strcmp(atmVar,'short-atm')
        
                N_extend = Nt - size(dtau.f1,3);
                extend_mat = zeros([mygrid.faces2gcmSize N_extend]);
                exf_fld = cat(3,exf_fld,extend_mat);
        
            elseif strcmp(atmVar,'long-atm')
        
                N_prepend = Nt_full - Nt_ecco;
                N_append = Nt_ecco - size(dtau.f1,3);
                prepend_mat = zeros([mygrid.faces2gcmSize N_prepend]);
                append_mat = zeros([mygrid.faces2gcmSize N_append]);
                exf_fld = cat(3,prepend_mat,exf_fld);

                % Add on last record
                exf_fld = cat(3,exf_fld,append_mat);
        
            end

        elseif ~isempty(strfind(perturbStr,'11'))

            if strcmp(atmVar,'short-atm')
        
                % exf_fld is only 11 months long
                N_extend = Nt - 12;
                extend_mat = zeros([mygrid.faces2gcmSize N_extend]);
                exf_fld = cat(3,extend_mat,exf_fld);
                exf_fld = cat(3,exf_fld,extend_mat(:,:,1));
        
            elseif strcmp(atmVar,'long-atm')
        
                N_prepend = Nt_full - Nt_ecco;
                N_prepend2 = Nt_ecco - 12;
                prepend_mat = zeros([mygrid.faces2gcmSize N_prepend]);
                prepend2_mat = zeros([mygrid.faces2gcmSize N_prepend2]);

                prepend_mat = cat(3,prepend_mat,prepend2_mat);
                exf_fld = cat(3,prepend_mat,exf_fld);

                % Add on last record
                exf_fld = cat(3,exf_fld,prepend_mat(:,:,1));
        
            end
        
        else
            error('Need to extend dtau properly for other years')
        end
    end
    
    % --- Convert exf_fld back to gcmfaces object
    exf_fld = convert2gcmfaces(exf_fld);
    
    % --- Compute dtau_avg
    Nt = size(dtau.f1,3);
    dtau_avg = zeros(1,Nt);
    for n = 1:Nt
        numer   = nansum(nansum( dtau(:,:,n)    ));
        denom   = nansum(nansum( dtau(:,:,n)~=0 ));
        if denom ~=0
            dtau_avg(n) = numer / denom;
        else
            dtau_avg(n) = 0;
        end
    end

    % --- Save
    save(matfile,'dtau','exf_fld','dtau_avg');
else
    load(matfile,'dtau','exf_fld','dtau_avg');
end

end
