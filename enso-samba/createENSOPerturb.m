function [] = createENSOPerturb()
% Function to create xx_tauu zonal wind perturbations

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');

% Create directories w/ +/- perturbation
% Noting that sign is flipped because of the exf package :(
% a negative tauu in control package creates a positive perturbation
% through the exf package
% hak = my hack to add only during the exact month, sign is intuitive
perturb_dir = 'enso-perturb/';
if ~exist(perturb_dir,'dir'), mkdir(perturb_dir); end;
exf_positive_dir = sprintf('%sexf-positive/',perturb_dir);
exf_negative_dir = sprintf('%sexf-negative/',perturb_dir);
hak_positive_dir = sprintf('%shak-positive/',perturb_dir);
hak_negative_dir = sprintf('%shak-negative/',perturb_dir);
if ~exist(exf_positive_dir,'dir'), mkdir(exf_positive_dir); end;
if ~exist(exf_negative_dir,'dir'), mkdir(exf_negative_dir); end;
if ~exist(hak_positive_dir,'dir'), mkdir(hak_positive_dir); end;
if ~exist(hak_negative_dir,'dir'), mkdir(hak_negative_dir); end;
prefix_exf_positive = [exf_positive_dir 'xx_'];
prefix_exf_negative = [exf_negative_dir 'xx_'];
prefix_hak_positive = [hak_positive_dir 'xx_'];
prefix_hak_negative = [hak_negative_dir 'xx_'];
suffix = '.0000000012.data';

% --- Making perturbation over smaller area
mskW = createPerturbMask('small');
mskW=convert2gcmfaces(mskW);
mskW(isnan(mskW))=0;

% Now repeat Nt times
Nt = 241;
xx=0*repmat(mskW,[1 1 Nt]);

% Set perturbation amplitude
tauu_0 = 0.005;
%tauu_0 = 0.1;
%tauu_0 = 0.001;

% Perturb on Oct. 1 1997 for super nino
% ts2dte(69,2635200,1992,1,1,6,0) = '05-Oct-1997 18:00:00'
% ts2dte(9,2635200,1997,1,1,0,0)  = '02-Oct-1997 12:00:00'
%  ... although calendar package should do this more elegantly?
%xx(:,:,70) = tauu_0 * mskW;

% Perturb Feb 1992
%xx(:,:,2) = tauu_0*mskW;

% Perturb Feb 2011
xx(:,:,230) = tauu_0*mskW;

%% Perturb during Feb-April 1992 ... make sure first controls don't get zero'd out
%xx(:,:,2) = 0.5 * tauu_0 * mskW;
%xx(:,:,3) = tauu_0 * mskW;
%xx(:,:,4) = 0.5 * tauu_0 * mskW;

% Perturb during Feb-April 2011 ... make sure first controls don't get zero'd out
%xx(:,:,230) = 0.5 * tauu_0 * mskW;
%xx(:,:,231) = tauu_0 * mskW;
%xx(:,:,232) = 0.5 * tauu_0 * mskW;

% Make both positive and negative perturbations
xx_exf_positive = -xx;
xx_exf_negative = xx;
xx_hak_positive = xx;
xx_hak_negative = -xx;

% Write to file
write2file([prefix_exf_positive 'tauu' suffix],xx_exf_positive);
write2meta([prefix_exf_positive 'tauu' suffix],size(xx_exf_positive));
write2file([prefix_exf_negative 'tauu' suffix],xx_exf_negative);
write2meta([prefix_exf_negative 'tauu' suffix],size(xx_exf_negative));
write2file([prefix_hak_positive 'tauu' suffix],xx_hak_positive);
write2meta([prefix_hak_positive 'tauu' suffix],size(xx_hak_positive));
write2file([prefix_hak_negative 'tauu' suffix],xx_hak_negative);
write2meta([prefix_hak_negative 'tauu' suffix],size(xx_hak_negative));

%% For reproducibility and such ... copy this file as well so we know what made the masks
copyfile('createENSOPerturb.m',exf_positive_dir);
copyfile('createENSOPerturb.m',exf_negative_dir);
copyfile('createENSOPerturb.m',hak_positive_dir);
copyfile('createENSOPerturb.m',hak_negative_dir);

% Zip it up for sending to a supercomputer 
tar('enso-perturb.tar.gz',perturb_dir);

end
