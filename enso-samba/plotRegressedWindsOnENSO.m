function [] = plotRegressedWindsOnENSO()
% See calc'' version. 
% ----------------------------------------------

% input arguments for averaging type ... tbd
avgType = 'JulyThruFeb';
ensoVar = 'eccov4r2';
modelVar = 'eccov4r2';
atmVar = 'short-atm';

% --- mygrid, directories etc.
mygrid = establish_mygrid('llc90');

matDir = sprintf('mat/reg/%s/',avgType);
if ~exist(matDir,'dir'), mkdir(matDir); end;
loadMatFile_u = sprintf('%stauu_regression_%s.mat',matDir,ensoVar);
loadMatFile_v = sprintf('%stauv_regression_%s.mat',matDir,ensoVar);
load(loadMatFile_u,'enso_avg','sst_avg','tauu_avg','tauu_fit','tauu_coeffs',...
                            'cl_msk_95_u','cl_msk_99_u','cl_msk_999_u');
load(loadMatFile_v,'enso_avg','sst_avg','tauv_avg','tauv_fit','tauv_coeffs',...
                            'cl_msk_95_v','cl_msk_99_v','cl_msk_999_v');

cl_msk_95 = cl_msk_95_u | cl_msk_95_v;
cl_msk_99 = cl_msk_99_u | cl_msk_99_v;
cl_msk_999 = cl_msk_999_u | cl_msk_999_v;

plotMaskWindsAndSSTs( cl_msk_99,tauu_fit,tauv_fit,sst_avg);




end

function [] = plotMaskWindsAndSSTs(cl_msk,tauu,tauv,sst)

yrs=1992:2012;
Nyrs=length(yrs);

% --- Plot CL mask, 


cmap = flipdim(cbrewer2('RdBu',9),1);
for i=1:Nyrs

    figureL;

    % --- set up u/v fields
    fld_u = tauu(:,:,i).*cl_msk(:,:,i);
    fld_v = tauv(:,:,i).*cl_msk(:,:,i);

    subplot(2,1,1) 
        m_map_atl(cl_msk(:,:,i),5);
        niceColorbar;
        colormap(cmap);
        caxis([-1 1]);
        xlabel('CL Mask');


    subplot(2,1,2)
        m_map_atl(sst(:,:,i),4.5,{'doHold',1},{'doLabl',0})
        m_map_gcmfaces_uv(fld_u,fld_v,4.5,2.5,3)
        niceColorbar;

        colormap(cmap);
        sstMax = nanmax(abs(sst(:,:,i)));
        caxis([-sstMax sstMax]);
        hold off
        xlabel(sprintf('Year %d',yrs(i)));

    keyboard
    close
end


end
