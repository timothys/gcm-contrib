function [] = plotPerturbEta(varargin)
perturbStr = 'en97';
for n=1:length(varargin)
    if ischar(varargin{n})
        perturbStr = varargin{n};
    end
end

mygrid=establish_mygrid('llc90');

ref_results_dir = sprintf('../../results/enso-samba/%s.ref/diags/',perturbStr);
per_results_dir = sprintf('../../results/enso-samba/%s.p1/diags/',perturbStr);

% Grab iterations for time stamp in plot
iters = grabAllIters(ref_results_dir,'eta_daily');

% Here specifying the iters as in MITgcm iteration numbers

if strcmp(perturbStr,'en97')
    start_iter = 50016; % 15 Sep 1997
    %end_iter = 53016;   % 18 Jan 1998
    end_iter = 54024;   % 01 Mar 1998
elseif strcmp(perturbStr,'no92')
    start_iter  = 24;
    end_iter    = 8760;
end

% Grab iters in directory in this range
select_ind = iters >= start_iter & iters < end_iter;
select_iters = iters(select_ind);
dates = ts2dte(select_iters,3600,1992,1,1,12,1);
dates = mat2cell(dates,ones(1,size(dates,1)),size(dates,2));
for n=1:length(dates)
    dates{n} = num2str(dates{n});
end

% Load eta from each
% 'ETAN   ','MXLDEPTH'
eta_ref = rdmds2gcmfaces([ref_results_dir 'eta_daily'],select_iters,'rec',1);
eta_per = rdmds2gcmfaces([per_results_dir 'eta_daily'],select_iters,'rec',1);

eta_diff = eta_per-eta_ref;


% Make some flicks
etaMovie(eta_diff,dates,perturbStr);



keyboard
end

function [] = etaMovie(fld,timeStamp,perturbStr)
% fld = 3D gcmfaces object, 2D field, 3rd dim gives time records
% timeStamp = cell aray of time stamps for each record of field

global mygrid;

Ncolors = 7;
cmap = cbrewer2('RdBu',Ncolors);
cmap = flipdim(cmap,1);

if iscell(timeStamp)
    xlbl = cell(size(timeStamp));
    for n = 1:length(timeStamp)
        xlbl{n} = sprintf('%s - %s, %s','$\eta_{pert}$','$\eta_{ref}$',timeStamp{n});
    end
else
    xlbl = sprintf('%s - %s, %s','$\eta_{pert}$','$\eta_{ref}$',timeStamp);
end

savename = sprintf('figures/eta_diff_%s_p1',perturbStr);

strs = struct(...
    'xlbl',xlbl,...
    'clbl',sprintf('%s [m]','$\delta\eta$'),...
    'vidName',savename);

opts = struct(...
    'saveVideo',1,...
    'logFld',1,...
    'mmapOpt',8,...
    'cmap',cmap);

plotVideo(fld,strs,opts,mygrid);
end
