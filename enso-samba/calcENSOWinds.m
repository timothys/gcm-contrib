function [enso] = calcENSOWinds(tauu)
% Compute various ENSO Indices from input wind stress field
% as if they were SST's
% Just using ECCOv4r2 wind stres...
% NOTE: This computes the anomaly from monthly climatology, 
%      assuming full field as input
%
% Inputs:
%
%   tauu: 3D field (horizontal space x time) 
%         zonal wind stress field
%
% Output:
%   
%   enso: data structure containing multiple ENSO indices as 
%         1D vectors in time  
%   
% --------------------------------------------------------------

global mygrid;

% Find length of time dimension and get lat/lon depending on data type 
Nt = size(tauu.f1,3);
lat = mygrid.YC;
lon = mygrid.XG;
% Note: tauu field is already masked for land points
%       However, leave this here in case use another grid
land_mask = 1;

% Check for matfile, otherwise do computation
matfile = 'mat/eccov4r2/enso_winds.mat';

if exist(matfile,'file')

    load(matfile,'enso');

else

    % Compute monthly anomaly
    tauu_anom = calcAnomaly(tauu);
    
    % Create empty container to be filled with the following metrics
    enso = struct;
    %mskNames = {'enso34','enso12','enso3','enso4','ensoModoki','tropPac',};
    %structNames={'ind_34','ind_12','ind_3','ind_4','modoki','tropPac'};
    mskNames = {'enso34','tropPac','tropPac10','tropPac5'};
    structNames={'enso34','tropPac','tropPac10','tropPac5'};
    
    for i = 1:length(mskNames)
    
        % Get mask        
        mskU = calcENSOWindMasks(lat,lon,mskNames{i});
        mskU_orig = mskU;
        mskU = mskU .* land_mask;
    
        % Compute index
        enso_ind = zeros(1,Nt);
    
        for n=1:Nt
            numer = nansum(tauu(:,:,n).*mskU);
            denom = nansum(mskU);
            enso_ind(n) = numer / denom;
        end
    
        % Add field to enso structure
        eval(sprintf('enso.%s = deal(%s);',structNames{i},'enso_ind'));
    
    end
    save(matfile,'enso','mskU','lat','lon');
end
end

% --------------------------------------------------------------

function [tauu_anom] = calcAnomaly(tauu)
% Compute full anomaly, not just for ENSO 3.4 region

Nt = size(tauu.f1,3);
Nyrs = Nt / 12;
tauu = tauu - ...
        convert2gcmfaces( ...
        repmat(convert2gcmfaces(nanmean(tauu,3)),[1 1 Nt]));
clim=calcClimatology(tauu,Nyrs);
tauu_anom=tauu - ...
           convert2gcmfaces( ...
           repmat(convert2gcmfaces(clim),[1 1 Nyrs]) );

end
