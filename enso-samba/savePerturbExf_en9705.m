function [] = savePerturbExf_en9705(varargin)
% Save the perturbed exf diagnostics to a mat file for viewing/reconstruction
% 
% Note: en97.05, did not output at final time so missing last record. For this,
%       I will append the files with the eccov4r2 forcing ... 
%       no93.05 ... same thing
%
% Input: perturbStr : indicate which perturbation to look at
%           'en97.p05' (default) / 'en97.m05'
%           'no93.p05' / 'no93.m05'
%
% --------------------------------------------------------------------
perturbStr='en97.p05';
for n=1:length(varargin)
    if ischar(varargin{n})
        perturbStr=varargin{n};
    end
end

mygrid = establish_mygrid('llc90');

resultsDir = sprintf('../../results/enso-samba/%s/diags/',perturbStr);

atmVar = 'short-atm';
saveDir = sprintf('mat/exf/%s/%s/',perturbStr,atmVar);

if ~exist('mat/','dir'), mkdir('mat/'); end;
if ~exist('mat/exf/','dir'), mkdir('mat/exf/'); end;
if ~exist(sprintf('mat/exf/%s/',perturbStr)), mkdir(sprintf('mat/exf/%s/',perturbStr)); end;
if ~exist(saveDir), mkdir(saveDir); end; 

exfStr = {'tauu','tauv','atemp','aqh','lwdown','swdown','precip','runoff'};
recId =  [     1,     2,      5,    6,       7,       8,      10,      11];


for i=1:length(exfStr)

    saveStr = [saveDir 'exf_' exfStr{i} '.mat'];
    rdStr = [resultsDir 'exf_monthly'];
    if ~exist(saveStr,'file')
        loadForcingFile(saveStr, rdStr, recId(i));
    end
end

end

% -----------------------------------------------------------------------------

function [exf_fld] = loadForcingFile(saveFile,rdStr,recID)

%% Load forcing files
if ~isempty(strfind(saveFile,'tauu')) || ~isempty(strfind(saveFile,'tauv'))

  exf_taux = rdmds2gcmfaces(rdStr,NaN,'rec',recID);
  exf_tauy = rdmds2gcmfaces(rdStr,NaN,'rec',recID+1);

  Nt=size(exf_taux.f1,3);
  
  for n=1:Nt
    [exf_taux(:,:,n),exf_tauy(:,:,n)]=calc_UEVNfromUXVY(exf_taux(:,:,n),exf_tauy(:,:,n));
  end

  % Append ecco forcing 
  load('../samoc/mat/exf/eccov4r2/short-atm/exf_tauu.mat','exf_fld');
  ecco_tauu=exf_fld;
  load('../samoc/mat/exf/eccov4r2/short-atm/exf_tauv.mat','exf_fld');
  ecco_tauv=exf_fld;

  Nt=240;
  exf_taux = cat(3,exf_taux,ecco_tauu(:,:,Nt));
  exf_tauy = cat(3,exf_tauy,ecco_tauv(:,:,Nt));

  if size(exf_taux.f1,3)~=Nt, error('exf_taux not the right size!'); end;
  if size(exf_tauy.f1,3)~=Nt, error('exf_tauy not the right size!'); end;
  
  if ~isempty(strfind(saveFile,'tauu'))
    exf_fld=exf_taux;
    fprintf('Saving file: %s \n ',saveFile);
    save(saveFile,'exf_fld');

    % Kill 2 birds, save tauy as well
    exf_fld=exf_tauy;
    saveFile=strrep(saveFile,'tauu','tauv');
    fprintf('Saving file: %s \n ',saveFile);
    save(saveFile,'exf_fld');

  else
    exf_fld=exf_tauy;
    fprintf('Saving file: %s \n ',saveFile);
    save(saveFile,'exf_fld');

    % Save taux as well
    exf_fld=exf_taux;
    saveFile=strrep(saveFile,'tauv','tauu');
    fprintf('Saving file: %s \n ',saveFile);
    save(saveFile,'exf_fld');

  end

else

  if ~isempty(strfind(saveFile,'atemp')) 
  
    % Append ecco forcing 
    load('../samoc/mat/exf/eccov4r2/short-atm/exf_atemp.mat','exf_fld');
    ecco_fld=exf_fld;
  
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID);
      
  elseif ~isempty(strfind(saveFile,'aqh')) 
  
    % Append ecco forcing
    load('../samoc/mat/exf/eccov4r2/short-atm/exf_aqh.mat','exf_fld');
    ecco_fld=exf_fld;
  
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID);
  
  elseif ~isempty(strfind(saveFile,'lwdown')) 
  
    % Append ecco forcing
    load('../samoc/mat/exf/eccov4r2/short-atm/exf_lwdown.mat','exf_fld');
    ecco_fld=exf_fld;
  
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID);
      
  elseif ~isempty(strfind(saveFile,'swdown')) 
  
    % Append ecco forcing
    load('../samoc/mat/exf/eccov4r2/short-atm/exf_swdown.mat','exf_fld');
    ecco_fld=exf_fld;
  
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID);
      
  elseif ~isempty(strfind(saveFile,'precip')) 
  
    % Append ecco forcing
    load('../samoc/mat/exf/eccov4r2/short-atm/exf_precip.mat','exf_fld');
    ecco_fld=exf_fld;
  
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID);
      
  elseif ~isempty(strfind(saveFile,'runoff')) 
   
    % Append ecco forcing
    load('../samoc/mat/exf/eccov4r2/short-atm/exf_runoff.mat','exf_fld');
    ecco_fld=exf_fld;
  
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID);
  
  end

  Nt=240;
  exf_fld = cat(3,exf_fld,ecco_fld(:,:,Nt));

  if size(exf_fld.f1,3)~=Nt
    error(['exf_fld for file ' saveFile ' not the right size!']); 
  end

  fprintf('Saving file: %s \n ',saveFile);
  save(saveFile,'exf_fld');
end

end
