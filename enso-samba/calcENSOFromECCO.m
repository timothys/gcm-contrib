function [enso,sst_anom] = calcENSOFromECCO()
% Compute ENSO indices and SST's for famous EN years
% diagnosed from ECCOv4r2
%
% ----------------------------------------------

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');

% parameters
modelVar='eccov4r2';
atmVar='short-atm';

% Load ECCO SST's
saveDir = sprintf('mat/%s/',modelVar);
if ~exist(saveDir,'dir'),mkdir(saveDir); end;
enso_sst_file = [saveDir 'sst_enso.mat'];

if ~exist(enso_sst_file,'file')

    sst_file = sprintf('../samoc/mat/%s/%s/sst.mat',modelVar,atmVar);
    if ~exist(sst_file,'file')
        prepThetaFile(modelVar,atmVar);
    end
    load(sst_file,'sst');
    
    % Compute sst anomaly (from climatology)
    [sst_anom] = calcAnomaly(sst);

    % Compute all ENSO metrics from sst_anom and put into data structure
    [enso] = calcENSOIndices(sst_anom,[],[]);

    save(enso_sst_file,'sst_anom','enso');
else
    load(enso_sst_file,'sst_anom','enso');
end

end

% ------------------------------------------------------


function [sst_anom] = calcAnomaly(sst)
% Compute full anomaly, not just for ENSO 3.4 region

Nyrs=20;
Nt = size(sst.f1,3);
sst = sst - ...
        convert2gcmfaces( ...
        repmat(convert2gcmfaces(nanmean(sst,3)),[1 1 Nt]));
clim=calcClimatology(sst,Nyrs);
sst_anom=sst - ...
           convert2gcmfaces( ...
           repmat(convert2gcmfaces(clim),[1 1 Nyrs]) );

end

% ------------------------------------------------------

function [] = prepThetaFile(modelVar,atmVar)

global mygrid;

if strcmp(atmVar,'biweekly')
    diags_dir = '/workspace/results/enso-samba/ref/diags/';
    
    % theta (50 vertical levels, 521 recs in time) takes ~22 GB total, load in chunks
    chunkSize=100;
    iters = grabAllIters(diags_dir,'state_3d');
    
    Nr=length(mygrid.RC);
    Nt=length(iters);
    theta = 0*convert2gcmfaces(repmat(convert2gcmfaces(mygrid.mskC(:,:,1)),[1 1 Nr chunkSize]));
    sst = 0*convert2gcmfaces(repmat(convert2gcmfaces(mygrid.mskC(:,:,1)),[1 1 Nt]));
    
    for i=1:chunkSize:Nt
        beg=i;
        ed =min(i+chunkSize-1,Nt);
        n=ed-beg +1;
        theta(:,:,:,1:n) = rdmds2gcmfaces([diags_dir 'state_3d'],iters(beg:ed),'rec',4);
        sst(:,:,beg:ed)=squeeze(theta(:,:,1,1:n));
    end
    %sst=squeeze(theta(:,:,1,:));
    
    if ~exist(sprintf('../samoc/mat/%s/',modelVar)), mkdir(sprintf('mat/%s/',modelVar)); end;
    if ~exist(sprintf('../samoc/mat/%s/%s/',modelVar,atmVar)), mkdir(sprintf('mat/%s/%s/',modelVar,atmVar)); end;
    
    %theta_file = sprintf('mat/%s/%s/theta.mat');
    %save(theta_file,'theta');
    
    sst_file = sprintf('../samoc/mat/%s/%s/sst.mat');
    save(sst_file,'sst');

elseif strcmp(atmVar,'short-atm')

    % theta (50 vertical levels, 240 recs in time) ... should be good on ekman
    theta_file = '/workspace/release2/nctiles_monthly/THETA/THETA';
    theta = read_nctiles(theta_file);
    sst=squeeze(theta(:,:,1,:));
    
    sst_file = sprintf('../samoc/mat/%s/%s/sst.mat',modelVar,atmVar);
    if ~exist(sprintf('../samoc/mat/%s/',modelVar)), mkdir(sprintf('mat/%s/',modelVar)); end;
    if ~exist(sprintf('../samoc/mat/%s/%s/',modelVar,atmVar)), mkdir(sprintf('mat/%s/%s/',modelVar,atmVar)); end;

    save(sst_file,'sst');
end

end
