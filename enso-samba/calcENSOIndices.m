function [enso] = calcENSOIndices(sst,lat,lon)
% Compute various ENSO Indices from input sst field
% Note: if data are on mygrid, lat,lon as empty arrays
%
% Inputs:
%
%   sst: 3D field (horizontal space x time) sst anomaly field
%   lat/lon: latitude and longitude, same spatial dim as sst
%
% Output:
%   
%   enso: data structure containing multiple ENSO indices as 
%         1D vectors in time  
%   
% --------------------------------------------------------------

global mygrid;

% Find length of time dimension and get lat/lon depending on data type 
if isempty(lat)
    Nt = size(sst.f1,3);
    lat = mygrid.YC;
    lon = mygrid.XC;
    land_mask = mygrid.mskC(:,:,1);
else
    Nt = size(sst,3);
    % Set lon to vary (-180,180)
    [lon,sst] = setLon_nc(lon,sst);
    [lon,lat] = meshgrid(lon,lat);
    land_mask = ones(size(lat));
end

% Create empty container to be filled with the following metrics
enso = struct;
mskNames = {'enso34','enso12','enso3','enso4','ensoModoki'};
structNames={'ind_34','ind_12','ind_3','ind_4','modoki'};

for i = 1:length(mskNames)

    % Get mask        
    mskC = calcENSOMasks(lat,lon,mskNames{i});
    mskC_orig = mskC;
    mskC = mskC .* land_mask;

    % Compute index
    enso_ind = zeros(1,Nt);

    for n=1:Nt
        numer = nansum(sst(:,:,n).*mskC);
        denom = nansum(mskC);
        enso_ind(n) = numer / denom;
    end

    % Add field to enso structure
    eval(sprintf('enso.%s = deal(%s);',structNames{i},'enso_ind'));

end
end
