function [] = plotENSOTheta()
% Plot ENSO 3.4 index and SST's for famous EN years
%
% ----------------------------------------------

% Load obs ENSO index from NOAA Website 
% This gives monthly SSTs from 1981-2011
matfile = ['../samoc/mat/index-data/enso_34.mat'];
if exist(matfile,'file')
  load(matfile,'ensoInd');
else
  fprintf('Need to prep enso data\n');
  return;
end

% Compute ENSO from NOAA data
[enso_noaa, noaa_sst, noaa_lat, noaa_lon] = calcENSOFromObs('noaa_high_res');
fprintf('Loaded NOAA SST data ...\n');

% Use NOAA low res data for kicks
[enso_noaa_lr, noaa_sst_lr, noaa_lat_lr, noaa_lon_lr] = calcENSOFromObs('noaa_low_res');
fprintf('Loaded NOAA 1 degree resolution SST data ...\n');

% Compute ENSO from Reynolds SST data
[enso_reynolds, reynolds_sst] = calcENSOFromObs('reynolds');
fprintf('Loaded Reynolds SST data ...\n');

% Compute ENSO from ECCO
[enso_ecco, ecco_sst] = calcENSOFromECCO();
fprintf('Loaded ECCO SST fields ...\n');


% time vector
t= [1/12:1/12:20]+1992;
t_92 = 145;
ensoInd=ensoInd(t_92:end);


% Plot ENSO3.4
mskNames = {'enso34','enso12','enso3','enso4','ensoModoki'};
structNames={'ind_34','ind_12','ind_3','ind_4','modoki'};
legendStr={'ENSO 3.4 Index','ENSO 1+2 Index','ENSO 3 Index','ENSO 4 Index','ENSO Modoki Index'};
cmap=cbrewer2('Dark2',4);

for i = 1:length(mskNames)

    figureW;
    ph=eval(sprintf('plot(t,enso_ecco.%s,t,enso_reynolds.%s,t,enso_noaa.%s,t,enso_noaa_lr.%s)',structNames{i},structNames{i},structNames{i},structNames{i}));
    set(ph,{'color'},num2cell(cmap,2));
    ylabel(legendStr{i});
    legend('ECCOv4r2','Reynolds SST','NOAA SST 1/4$^\circ$','NOAA SST 1$^\circ$');
    grid on;
    savePlot(sprintf('figures/%s_eccov4r2_reynolds_noaa',mskNames{i}),'pdf');

end



% Get a nice look at that warm tongue
nino_inds = [3,6,11,13,15,18]*12;
nino_lbls = {'$\theta$ Anomaly, Dec. 1994','$\theta$ Anomaly, Dec. 1997','$\theta$ Anomaly, Dec. 2002','$\theta$ Anomaly, Dec. 2004','$\theta$ Anomaly, Dec. 2006','$\theta$ Anomaly, Dec. 2009'};

for n=1:length(nino_inds)
    yr = (nino_inds(n)/12 - 1)+1992;
    yr_lbl = num2str(yr);
    plotWarmTongue(ecco_sst(:,:,nino_inds(n)),nino_lbls{n},...
                  'ECCOv4r2');
    savePlot(sprintf('figures/sst_anom_dec%s_eccov4r2',yr_lbl(3:4)),'png')

    plotWarmTongue(reynolds_sst(:,:,nino_inds(n)),nino_lbls{n},...
                  'Reynolds SST');
    savePlot(sprintf('figures/sst_anom_dec%s_reynolds',yr_lbl(3:4)),'png')

    plotWarmTongue_obs(noaa_lat,noaa_lon,noaa_sst(:,:,nino_inds(n))',nino_lbls{n},...
                      'NOAA SST 1/4$^\circ$');
    savePlot(sprintf('figures/sst_anom_dec%s_noaa_highres',yr_lbl(3:4)),'png')
    plotWarmTongue_obs(noaa_lat_lr,noaa_lon_lr,noaa_sst_lr(:,:,nino_inds(n))',nino_lbls{n},...
                      'NOAA SST 1$^\circ$');
    savePlot(sprintf('figures/sst_anom_dec%s_noaa_lowres',yr_lbl(3:4)),'png')
end

end

% ------------------------------------------------------


function [] = plotWarmTongue(sst,lbl,data_lbl)
% Nice 2D map of the tropical pacific

mygrid = establish_mygrid('llc90');

% Trying to limit cbar by region being plotted
mskX = ~(mygrid.XC<-115 & mygrid.XC > 65); 
mskY = mygrid.YC>-32 & mygrid.YC<32;
sst = sst.*mskX.*mskY;

figureW;
m_map_atl(sst,4.5);
cmap=cbrewer2('BrBG',9);
cmap=flipdim(cmap,1);
colormap(cmap);
hh=niceColorbar;
title([lbl ' ' data_lbl])
ylabel(hh,'$^\circ$C')

cmax=round(nanmax(abs(sst(:))));
caxis([-cmax cmax]);

end

% ------------------------------------------------------

function [] = plotWarmTongue_obs(lat,lon,sst,lbl,data_lbl)
% Nice 2D map of the tropical pacific
% using obs data...

% Trying to limit cbar by region being plotted
[lon_mat,lat_mat] = meshgrid(lon,lat);
mskX = ~(lon_mat<-115 & lon_mat > 65); 
mskY = lat_mat>-32 & lat_mat<32;
sst = sst.*mskX.*mskY;

figureW;
m_map_nc(lat,lon,sst,4.5);
cmap=cbrewer2('BrBG',9);
cmap=flipdim(cmap,1);
colormap(cmap);
hh=niceColorbar;
title([lbl ' ' data_lbl])
ylabel(hh,'$^\circ$C')

cmax=round(nanmax(abs(sst(:))));
caxis([-cmax cmax]);

end
