function [dJ] = calcDiffOfReconstructions(refStr,perturbStr)
% Compute the reconstructed monthly SAMOC perturbation as the
% difference between perturbed and reference reconstructions
%
%   dJ = dJ^perturbation - dJ^reference
%
% Note: this should (and does!) compute the same reconstruction as
%       calcPerturbationReconstruction.m
%
% Parameters
% ----------
%
%   refStr      ::  String specifying reference experiment
%                   e.g. no92.ref 
%   perturbStr  ::  String specifying perturbation experiment 
%                   e.g. xx92.p05.feb
%
% Output
% ------
%
%   dJ          ::  matrix giving SAMOC reconstruction from 
%                   zonal wind perturbation
%                   [N_memory, N_time]
%
% ---------------------------------------------------------------

% --- Pass perturbStr as modelVar  
%   giving entire directory to samoc reconstruction script 
modelVar    = sprintf('%s',perturbStr);
atmVar      = 'long-atm'; % This doesn't matter so much...
lat         = -34;

% --- Compute and save perturbed zonal wind field for reconstruction
calcPerturbedECCOZonalWind(refStr,perturbStr,atmVar);

% --- Do the reconstruction or load
matfile = sprintf('mat/reconstruct.34S/%s/%s/full/fullReconstruction.mat',modelVar,atmVar);

if ~exist(matfile,'file')
    addpath(genpath('../samoc'))
    sensFrac = calcSamocSensFrac;
    samoc_adjFields = samocReconstructionByMonth( ...
        modelVar,atmVar,lat,sensFrac);
else
    load(matfile,'samoc_adjFields');
end

% --- Now compute difference between reconstructions
dJ_p = samoc_adjFields{1};

matfile = sprintf('../samoc/mat/reconstruct.34S/eccov4r2/%s/full/fullReconstruction.mat',atmVar);
load(matfile,'samoc_adjFields');
dJ_r = samoc_adjFields{1};

dJ = dJ_p - dJ_r;

end

% ---------------------------------------------------------------

function[exf_fld] = calcPerturbedECCOZonalWind(refStr,perturbStr,atmVar)
% Recreate the ECCOv4r2 fields, but with the same perturbation
% as in the experiment as 
%
%   tauu_p = tauu_ECCO + dtau
% 
% This is to use the 20 year record in the reconstruction while only.
% running a 1 yr perturbation experiment
% ---------------------------------------------------------------

if nargin<3
    atmVar='short-atm';
end

% --- mat dirs created by calcDeltaZonalWind
matfile = sprintf('mat/%s/%s/%s/exf_tauu.mat',perturbStr,'monthly',atmVar);

if ~exist(matfile,'file')
    % --- Load dtau as full length, equivalent to 
    %   ECCO field specified by atmVar
    [~,dtau_atmVar_length] = calcDeltaZonalWind(refStr,perturbStr,'monthly',atmVar);
    
    % --- Now load eccov4r2 forcing used in reconstruction
    load(sprintf('../samoc/mat/exf/eccov4r2/%s/exf_tauu.mat',atmVar),'exf_fld')
    
    % --- Add dtau to exf_fld
    exf_fld = exf_fld + dtau_atmVar_length;
    
    % --- Save perturbed 
    save(matfile,'exf_fld');
else
    load(matfile,'exf_fld');
end

end
