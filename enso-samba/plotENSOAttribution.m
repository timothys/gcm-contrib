function [] = plotENSOAttribution(varargin)
% Show SAMOC attributed to zonal wind stress anomaly in ECCO correlated to ENSO indices 
%
% Input: 
%
% Optional inputs:
%
%       avgSelect : 'annual' or 'anomaly' (default) for 
%                    annual avg or anomaly from climatology
%       plotGlobe : 'false' (default) or 'globe'
%                    plots globe showing reconstruction area
%
% -----------------------------------------------------------------------------

% Parameters
modelVar='eccov4r2';
atmVar='long-atm';
Nt = 240;
tMem=228;
t=[1:Nt]./12 + 1992;
tYrs=1992:2011;
Nyrs=length(tYrs);
avgSelect = 'anomaly';
plotGlobe='false';
ensoInd = 'enso34';

for n=1:length(varargin)
    if strcmp(varargin{n},'annual')||strcmp(varargin{n},'anomaly')
        avgSelect=varargin{n};
    elseif ~isempty(strfind(varargin{n},'globe'))
        plotGlobe='globe';
    elseif ~isempty(strfind(varargin{n},'enso')) | ~isempty(strfind(varargin{n},'tropPac')) | ~isempty(strfind(varargin{n},'soi'))
        ensoInd = varargin{n};
    else
        error(sprintf('variable arg. number %d not recognized ...',n));
    end
end

%% --- Set up directories
plotDir = 'figures/attribution/';
if ~exist(plotDir,'dir'), mkdir(plotDir); end;

%% --- Load reconstruction
ensoRegion = 'westCentTropPac';
load(sprintf('../samoc/mat/reconstruct.34S/%s/%s/%s/region_rec.mat',modelVar,atmVar,ensoRegion));

%% --- Load ENSO index
if strcmp(ensoInd,'soi')

    % Compute SOI
    cd('../samoc');
    soi = calcEquatorialSOI;
    cd('../enso-samba');

    % Do some filtering ...
    filterWindow = 6;
    w = gausswin(filterWindow);
    w = w/sum(w);
    soi = filter(w,1,soi);

    tau_lag = 0;

    soi = -soi;

    % Fill necessary vars for plotting
    enso_obs = soi;
    ensoStr = 'Southern Oscillation Index';
    yax = [-3 3];
    dy = 1.5;
else
    matfile = 'mat/eccov4r2/sst_enso.mat';
    %matfile = '../samoc/mat/exf/eccov4r2/short-atm/exf_tauu.mat';
    %load(matfile,'exf_fld');
    %enso = calcENSOWinds(exf_fld);
    if exist(matfile,'file')
        load(matfile,'enso');
    
        % Set ENSO Index and legend string
        if ~isempty(strfind(ensoInd,'34'))
            structFieldName = 'ind_34';
            ensoStr = 'ENSO 3.4 Index';
            yax = [-3.5 3.5];
            dy = 1.75;
        elseif ~isempty(strfind(ensoInd,'3'))
            structFieldName = 'ind_3';
            ensoStr = 'ENSO 3 Index';
            yax = [-2 4];
            dy = 1.5;
        elseif ~isempty(strfind(ensoInd,'4'))
            structFieldName = 'ind_4';
            ensoStr = 'ENSO 4 Index';
            yax = [-2 2];
            dy = 1;
        elseif ~isempty(strfind(ensoInd,'12'))
            structFieldName = 'ind_12';
            ensoStr = 'ENSO 1+2 Index';
            yax = [-2.5 4.5];
            dy = 2.25;
        elseif ~isempty(strfind(lower(ensoInd),'modoki'))
            structFieldName = 'modoki';
            ensoStr = 'ENSO Modoki Index';
            yax = [-3.5 2.5];
            dy = 1.5;
        elseif strcmp(ensoInd,'tropPac')
            structFieldName = 'tropPac';
            ensoStr = 'Tropical Pacific $\pm15^\circ$';
            yax = [-3.5 2.5];
            dy = 1.5;
        elseif ~isempty(strfind(ensoInd,'tropPac10'))
            structFieldName = 'tropPac10';
            ensoStr = 'Tropical Pacific $\pm10^\circ$';
            yax = [-3.5 2.5];
            dy = 1.5;
        elseif ~isempty(strfind(ensoInd,'tropPac5'))
            structFieldName = 'tropPac5';
            ensoStr = 'Tropical Pacific $\pm5^\circ$';
            yax = [-3.5 2.5];
            dy = 1.5;
        else
            fprintf('ENSO Index "%s" unknown, switching to 3.4\n',ensoInd);
            structFieldName = 'ind_34';
            ensoStr = 'ENSO 3.4 Index';
            yax = [-2.5 3.5];
            dy = 1.5;
        end
        enso_obs = getfield(enso,structFieldName);
    else
        error('Need to prep enso data: %s ...\n',matfile);
    end
end

%% --- Grab Tropical Pacific reconstructions
dJ_enso=regionRec.pac.Ntrop{1}(tMem,:) + regionRec.pac.Strop{1}(tMem,:);
%dJ_enso=dJ_enso+regionRec.pac.NStrop{1}(tMem,:) + regionRec.pac.SStrop{1}(tMem,:);
eval(sprintf('westTropPac = regionRec.%s{1}(tMem,:);',ensoRegion));
%dJ_enso = [dJ_enso; westTropPac];

%% --- Do averaging
if strcmp(avgSelect,'annual')
    dJ_enso=calcAnnualMean(dJ_enso,20);
    enso_obs=calcAnnualMean(enso_obs,20);
else
    clim = calcClimatology(dJ_enso,Nyrs);
    dJ_enso=dJ_enso - repmat(clim,[1 Nyrs]);
end

%% --- Account for time lag
if exist('tau_lag','var')
    enso_obs = enso_obs(1+tau_lag:end);
    dJ_enso = dJ_enso(:,1:end-tau_lag);
    t = t(1:end-tau_lag);
end

%% --- Compute correlation
ensoFit = zeros(size(enso_obs));
r_squared = zeros(1,size(dJ_enso,1));
for i=1:size(dJ_enso,1)
    pp=polyfit(enso_obs,dJ_enso(i,:),1);
    ensoFit(i,:)=polyval(pp,enso_obs);
    rr=corrcoef(enso_obs,dJ_enso(i,:));
    r_squared(i) = rr(1,2);
end

%% --- Set strings
ylbl = ensoStr;
%legendStr = {'$\delta J_{\tau_x, Trop Pac}$',ensoRegion,ensoStr};
legendStr = {'$\delta J_{\tau_x, Trop Pac}$',ensoStr};
savename=sprintf('%s%s_%s',plotDir,ensoInd,avgSelect);

%% --- Do the plotting
if strcmp(avgSelect,'annual')

    ax=indexPlotter(tYrs, dJ_enso, enso_obs, ensoFit, r_squared, ylbl, legendStr, savename );
else
    ax=indexPlotter(t, dJ_enso, enso_obs, ensoFit, r_squared, ylbl, legendStr, savename );
    set(ax(1),'ylim',[-1 1],'ytick',[-1:.5:1])
    set(ax(2),'ylim',yax,'ytick',[yax(1):dy:yax(2)])
end

%% --- Save 
savePlot(savename,'pdf',[1 1 1],'nocrop')
end

% -----------------------------------------------------------------------------

function [ax,h1,h2] = indexPlotter( t, dJ, ind, indTrend, r_squared, ylbl, legendStr, savename )

% Basic plot options
fontsize=22;

% Set up colormap
Ncolors = 2;%size(dJ,1)+1;
cmap=cbrewer2('Dark2',Ncolors+1);
cmap=cmap([1,2],:);
Ncolors = 2;

% Big plot showing reconstruction(s) and index
figureW;
subplot(3,2,1:4),
[ax,h1,h2]=plotyy(t,dJ,t,ind);

set(gca,'xtick',[1992:2:2012])
xlim([1992 2012])
grid on

% Make y-axes black
set(ax(1),'YColor',[0 0 0]);
set(ax(2),'YColor',[0 0 0]);

% Set y-labels and string color, fontsize
set(get(ax(1),'ylabel'),'string','$\delta J$ [Sv]','color',cmap(1,:),'fontsize',fontsize);
set(get(ax(2),'ylabel'),'string',ylbl,'color',cmap(Ncolors,:),'fontsize',fontsize)

% Main legend
hl=legend(legendStr,'Location','Best');
hl.FontSize=fontsize;

% Set color for lines
set(h1,{'color'},num2cell(cmap(1:Ncolors-1,:),2));
set(h2,{'color'},num2cell(cmap(Ncolors,:),2));


% Show the correlation in small, bottom left panel
subplot(3,2,5),
for n = 1:Ncolors-1
    plot(ind,dJ(n,:),'.','Color',cmap(n,:));
    hold on
end
plot(ind,indTrend,'k-');
hold off

% Label correlation axes
xlabel(legendStr{Ncolors})
ylabel(legendStr{1})

% Set legend entries for correlation
fitStr = cell(1,Ncolors-1);
for n=1:Ncolors-1
    fitStr{n} = sprintf('Linear trend, $r^2 = %1.2f$',r_squared(n));
end
legend(fitStr,'Location','Best');

%% --- Make a large legend for 
%sh = subplot(3,2,6);
%sp = get(sh,'position');
%set(hl,'position',sp);
%delete(sh);



% --- Make a globe plot with tropical pacific
%figName=sprintf('%senso_globe',plotDir);
tropPacInd = 4; % see grabSamocGroups.m
separator='southek';
tauuInd = 1;
msks=createRegionMasks('ekman');

addpath(genpath('../samoc'))
[~,ensoSection]=grabSamocGroups(separator,tauuInd,msks);
fullLegend=grabSamocGroupLegend(separator);
cmap=grabSamocGroupColors(length(fullLegend));
plotSelectSamocMasks(tropPacInd,separator,[],cmap(tropPacInd,:));


%saveas(gcf,savename,'pdf');

end

% -----------------------------------------------------------------------------

