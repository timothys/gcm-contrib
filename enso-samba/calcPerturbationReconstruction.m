function [dJ] = calcPerturbationReconstruction(refStr,perturbStr,recString)
% Compute the reconstructed monthly SAMOC from the 
% zonal wind stress perturbation 
%
%   dtauu = tauu_p - tauu_reference
%
% Note: this should (and does!) compute the same reconstruction as
%       calcDiffOfReconstructions.m
%
% Parameters
% ----------
%
%   refStr      ::  String specifying reference experiment
%                   e.g. no92.ref 
%   perturbStr  ::  String specifying perturbation experiment 
%                   e.g. xx92.p05.feb
%
% Optional Parameters
% -------------------
%
%   recString
%           ::  which month or months to use in reconstruction
%               '' (default) = use 2011 sensitivities from 
%                   each individual month
%               'dec.92' = use December objective function sens.
%                       from 1992 
%               'dec.11' = same, use Dec. from  2011
%
% Output
% ------
%
%   dJ          ::  matrix giving SAMOC reconstruction from 
%                   zonal wind perturbation
%                   [N_memory, N_time]
%
% ---------------------------------------------------------------

if nargin < 3
    recString = '';
end

% --- Pass perturbStr as modelVar  
%   giving entire directory to samoc reconstruction script 
%   add '.diff' for reconstruction to recognize this is reconstructing
%   from a perturbation, not full field
modelVar    = sprintf('%s.diff',perturbStr);
atmVar      = 'short-atm'; 
lat         = -34;

% --- Optional: reconstruct only using December based sensitivity
if strcmp(recString,'')
    dirString = 'full';
else
    dirString = recString;
end


% --- Compute and save the mat file for deltaZonalWind
calcDeltaZonalWind(refStr,perturbStr,'monthly',atmVar);

% --- Do the reconstruction or load
matfile = sprintf('mat/reconstruct.34S/%s/%s/%s/fullReconstruction.mat',modelVar,atmVar,dirString);

if ~exist(matfile,'file')
    addpath(genpath('../samoc'))
    sensFrac = calcSamocSensFrac;
    samoc_adjFields = samocReconstructionByMonth( ...
        modelVar,atmVar,lat,sensFrac,dirString);
else
    load(matfile,'samoc_adjFields');
end

% --- Remove cell structure from reconstruction script
dJ = samoc_adjFields{1};

end
