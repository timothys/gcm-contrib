function [] = plotPerturbSAMOC(pType,pYear,pStrength,pMonth,diagFreq)
% Show samoc as a result of perturbed wind field during el nino
%
% Parameters 
% ----------
%
%   pType   : 2 characters indicating perturbation type
%             1st two indicate how perturbation applied
%               xx = through control package, with interpolation
%               hk = through my hack, only applied during a single month
%               (not an option) no = reserved for reference run
%           'xx' (default)
%           'hk'
%
%   pYear   : 2 characters indicating year
%           '92' (default)
%           '97' 
%
%   pStrength : string indicating sign and avg strength of perturbation
%           'p05' (default)
%           'p005','p1','m05' (for some...)
%   
%   pMonth : month of perturbation
%           'feb' (default)
%           'mar','oct'
%
%   diagFreq : frequency for plotting perturbed SAMOC
%
%           'monthly' (default)
%           'daily'
%
% --------------------------------------------------------------------
if nargin<1
    pType='xx';
end
if nargin<2
    pYear='92';
end
if nargin<3
    pStrength='p05';
end
if nargin<4
    pMonth='feb';
end
if nargin<5
    diagFreq='monthly';
end

% --- Set up commonly used strings to access simulation results
perturbStr  = sprintf('%s%s.%s.%s',pType,pYear,pStrength,pMonth);
refStr      = sprintf('no%s.ref',pYear);

% --- Compute the reference and perturbed AMOC
[samoc_r] = calcForwardSAMOC(refStr,diagFreq);
[samoc_p] = calcForwardSAMOC(perturbStr,diagFreq);
samoc_diff = samoc_p - samoc_r;

% --- Compute perturbed wind stress field
[dtau,~,dtau_avg] = calcDeltaZonalWind(refStr,perturbStr,diagFreq);
if length(dtau_avg)<length(samoc_diff)
    dtau_avg = cat(2,dtau_avg,NaN);
end

% --- Time length
Nt = length(samoc_diff);
t = [1:Nt];
if strcmp(pYear,'97')
    samoc_diff = samoc_diff(10:18);
end

% --- Figure settings
cmap    = cbrewer2('Dark2',4);
ms      = 20;
fs      = 22;
if strcmp(diagFreq,'monthly')

    xlbl = sprintf('Months during %s',pYear);

elseif strcmp(diagFreq,'daily')

    xlbl = sprintf('Days during %s',pYear);

end
samocLegendStr = sprintf('%sSAMOC FWD %s','$\delta$',pStrength);
tauLegendStr = sprintf('%s %s','$\delta\tau_x$',pStrength);

% --- Plot the SAMOC perturbation and one plot with dtau too
figureW;
plot(t,samoc_diff,'k-')
ylabel('$\delta$SAMOC [Sv]')
xlabel(xlbl);
legend(samocLegendStr);
if strcmp(diagFreq,'monthly')
    xlbl_clim;
end
grid on;

figureW;
ax = plotyy(t,samoc_diff,t,dtau_avg);
hl = legend(samocLegendStr,tauLegendStr);
xlabel(xlbl);
if strcmp(diagFreq,'monthly')
    xlbl_clim(ax(1));
    xlbl_clim(ax(2));
end
grid on;

% --- Only compute adjoint reconstructions if diagFreq = 'monthly'
if strcmp(diagFreq,'monthly')

    % --- Reconstruct the perturbation
    [samoc_diff_rec] = calcPerturbationReconstruction(refStr,perturbStr);
    tau_mem = 228;
    dJ = samoc_diff_rec(tau_mem,:);
    
    % --- Compute difference of reconstructed perturbed and reference runs
    % NOTE: this has been verified to be equivalent to the above form
    [samoc_diff_rec_2] = calcDiffOfReconstructions(refStr,perturbStr);
    dJ2= samoc_diff_rec_2(tau_mem,:);
    
    % --- Compute average sensitivity in perturb region from adjoint
    [dJ3,samoc_sens] = calcApproximatePerturbRec(refStr,perturbStr);

    % --- Grab year from reconstruction (which is now ECCO records long)
    if strcmp(pYear,'92')
        dJ  = dJ(1:Nt);
        dJ2 = dJ2(1:Nt);
        dJ3 = dJ3(1:Nt);
    elseif strcmp(pYear,'11')
        t_ind = 19*12 + [1:Nt];
        dJ  = dJ(t_ind);
        dJ2 = dJ2(t_ind);
        dJ3 = dJ3(1:Nt);
    else
        error('Grab the right records for other perturb years');
    end

    % --- Make the plot at last
    figureW;
    set(gca,'colororder',cmap,'nextplot','replacechildren')
    plot(t,dJ,'.-',t,dJ2,'.-',t,dJ3,'.-',t,samoc_diff,'.-','markersize',ms);
    legend('dJ: adjoint','dJ: adjoint 2','dJ: adjoint approx','dJ: fwd perturbation')
    ylabel('$\delta$SAMOC [Sv]')
    xlbl_clim;
    grid on;

end

keyboard


end
