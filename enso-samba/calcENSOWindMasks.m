function[mskC] = calcENSOWindMasks(lat,lon,maskString)
% Compute masks for various ENSO metrics
% Inputs (both lat/lon same dimension as field to be masked):
%   
%   lat: 2D field containing latitude
%   lon: 2D field containing longitude
%        Note: assume lon \in [-180,180]
%   maskString: string denoting mask to be made
%       Options include:
%       'enso34', 'enso12', 'enso3', 'enso4', 'ensoModoki'
%   
% Output: 
%
%   mskC: mask same size as lat and lon for masking SST anomaly field
%
% -------------------------------------------------------------------

pac = v4_basin('pacExt');

if strcmp(maskString,'enso34')
    %%% ENSO 3.4 mask
    yCond = lat >= -10 & lat <= 10;
    xCond = lon >= -170 & lon <= -120;
    mskC = yCond.*xCond;

elseif strcmp(maskString,'enso12')
    %%% ENSO 1+2 mask
    yCond = lat >= -10 & lat <= 0;
    xCond = lon >= -90 & lon <= -80;
    mskC = yCond.*xCond;

elseif strcmp(maskString,'enso3')
    %%% ENSO 3 mask
    yCond = lat >= -10 & lat <= 10;
    xCond = lon >= -150 & lon <= -90;
    mskC = yCond.*xCond;

elseif strcmp(maskString,'enso4')
    %%% ENSO 4 mask
    yCond = lat >= -10 & lat <= 10;
    xCond = (lon >= 160  & lon <= 180) | (lon >= -180 & lon <= -150);
    mskC = yCond.*xCond;

elseif strcmp(maskString,'ensoModoki')

    %% See Ashok et al, 2007, El Nino Modoki and its teleconnections
    %% ENSO Modoki = center avg - 1/2(East + West)

    yCondC = lat >= -10 & lat <= 10;
    xCondC = (lon >= 165  & lon <= 180) | (lon >= -180 & lon <= -140);

    yCondW = lat >= -10 & lat <= 20;
    xCondW = lon >= 125 & lon <= 145;

    yCondE = lat >= -15 & lat <= 5;
    xCondE = lon <= -70 & lon >= -110;

    mskC = yCondC.*xCondC - .5*(yCondE.*xCondE + yCondW.*xCondW);

elseif strcmp(maskString,'tropPac')
    yCond = lat>=-15 & lat<=15;
    mskC = yCond;

elseif strcmp(maskString,'tropPac10')
    yCond = lat>=-10 & lat<=10;
    mskC = yCond;

elseif strcmp(maskString,'tropPac5')
    yCond = lat>=-5 & lat<=5;
    mskC = yCond;

else 
    error(sprintf('maskString = %s not recognized\n',maskString))
end

mskC = mskC.*pac;

end
