function [] = calcExtended1992Sensitivity()
% Goal: test the impact of reconstructing 1992 perturbation using
% 1992 generated sensitivity maps.      
% These are found in 
%
%   ../../results/samoc/dec.12mo/adxx_*
%   ../../samoc/mat/dec.12mo/adj_* 
%
% And have 13 records because of ctrl package. However, the period lines up with
% months much better than in 2011 case. Here, modelEndDate = 31 Dec 1992 13:00:00
% and the last ctrl rec ends at 1 Jan 1993 06:00:00
%
% ../samoc/samocReconstructionByMonth.m requires sensitivities to be 230-240mo
% and rather than rewriting this, I'll just prepend these 12mo sensitivities 
%
% ------------------------------------------------------------------------------


global mygrid;

loaddir = '../samoc/mat/dec.12mo';
savedir = 'mat/dec.92.extend_ad';
if ~exist(savedir,'dir'), mkdir(savedir); end;

addpath('../samoc');
adjField = set_adjField('atm');

Nmo_ecco = 241;

for i = 1:length(adjField)
    
    % Load sensitivity with 13 records from 1992
    loadfile = sprintf('%s/adj_%s.mat',loaddir,adjField{i});
    load(loadfile,'adxx');

    % Make gcmfaces object with lots of zeros
    N_prepend = Nmo_ecco - size(adxx.f1,3);
    prepend_mat = 0 * adxx(:,:,1);
    prepend_mat = repmat(prepend_mat,[1 1 N_prepend]);

    % Prepend this thing to 12month sensitivity
    adxx = cat(3,prepend_mat,adxx);

    % be like jesus
    savefile = sprintf('%s/adj_%s.mat',savedir,adjField{i});
    save(savefile,'adxx');

end
end
