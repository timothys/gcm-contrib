function [] = plotPerturbExf(varargin)
% Plot exf fields (tauu, mainly) between perturbed and reference run...
%
% Input: perturbStr : indicate which perturbation to look at
%           'en97.p05' (default) / 'en97.m05'
%           'no93.p05' / 'no93.m05'
%
% --------------------------------------------------------------------
refStr = 'en97.ref';
perturbStr='en97.p005';
for n=1:length(varargin)
    if ischar(varargin{n})
        if ~isempty(strfind(varargin{n},'ref'))
            refStr = varargin{n};
        else
            perturbStr = varargin{n};
        end
    end
end

mygrid = establish_mygrid('llc90');

% Found maximum difference to be 10^-9... going to assume this is fine...
%checkRefTauEqualsEccoTau;

% --- Load files
[tauu_r] = loadZonalWind(refStr);
[tauu_p] = loadZonalWind(perturbStr);

exf_diff = tauu_p - tauu_r;

for i=1:size(exf_diff.f1,3)
    plotExf(exf_diff(:,:,i))
end


end


% ----------------------------------------------------

function [] = plotExf(fld)


figureW;
m_map_atl(fld,4.5,{'symmetricCbar',1})
ylabel('N/m$^2$');
legend('$\tau_x^{pert}$ - $\tau_x^{ref}$')

keyboard

end

% ----------------------------------------------------

function [tauu] = loadZonalWind(runStr)

matdir = sprintf('mat/%s',runStr);
if ~exist(matdir,'dir'), mkdir(matdir); end;
matfile = sprintf('%s/exf_tau.mat',matdir);

if ~exist(matfile,'file')
    results_dir = sprintf('../../results/enso-samba/%s/diags',runStr);
    taux = rdmds2gcmfaces(sprintf('%s/exf_monthly',results_dir),NaN,'rec',1);
    tauy = rdmds2gcmfaces(sprintf('%s/exf_monthly',results_dir),NaN,'rec',2);

    [tauu,tauv] = calc_UEVNfromUXVY(taux,tauy);

    save(matfile,'tauu','tauv');
else
    load(matfile,'tauu')
end

end

% ----------------------------------------------------

function [] = checkRefTauEqualsEccoTau()


rdStr = '../../results/enso-samba/ref/diags/exf_monthly';

exf_taux = rdmds2gcmfaces(rdStr,NaN,'rec',1);
exf_tauy = rdmds2gcmfaces(rdStr,NaN,'rec',2);

Nt=size(exf_taux.f1,3);

for n=1:Nt
  [exf_taux(:,:,n),exf_tauy(:,:,n)]=calc_UEVNfromUXVY(exf_taux(:,:,n),exf_tauy(:,:,n));
end

load('../samoc/mat/exf/eccov4r2/short-atm/exf_tauu.mat','exf_fld');
ecco_tauu = exf_fld;
load('../samoc/mat/exf/eccov4r2/short-atm/exf_tauv.mat','exf_fld');
ecco_tauv = exf_fld;

taux_diff = exf_taux - ecco_tauu(:,:,1:Nt);
tauy_diff = exf_tauy - ecco_tauv(:,:,1:Nt);

nansum(taux_diff(:))
nansum(tauy_diff(:))

keyboard


end
