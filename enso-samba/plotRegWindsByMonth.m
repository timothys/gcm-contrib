function [] = plotRegWindsByMonth()
% Looking for the month with the largest wind anomaly associated with ENSO
% --------------------------------------------------------------------------

avgType = {'JulyThruFeb',...
           'MarchThruMay','MayThruJuly','JulyThruSept','SeptThruNov','OctThruDec','NovThruJan','DecThruFeb',...
           'march','april','may','june','july','aug','sept','oct','nov','dec','jan','feb'};
Navg = length(avgType);

% --- mygrid, directories etc.
mygrid = establish_mygrid('llc90');

fig_dir = 'figures/regression';
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;

% --- Fit slopes and CI masks into cell array
tauu_slope = cell(1,Navg);
cl_msk = cell(1,Navg);
%c_lim=.04;
c_lim=[];
CIstr = '(only showing points for which 0 slope does not lie in the 95\% CI)';
for i=9:Navg

    % --- Load just tauu and (tauu,tauv) fields masked by 95 CI 
    % difference is that tauu_slope_95u is masked only with confidence
    % interval from tauu, tauu_slope_95uv is masked with CI from tauu or tauv
    [tauu_slope_95u{i},cl_msk{i}] = loadZonalSlope(avgType{i});
    [tauu_slope_95uv{i},tauv_slope_95uv{i},sst_avg]=loadBothSlopes(avgType{i});

    % --- Plot just tauu, then (tauu,tauv) on top of tauu pcolor 

    xlbl = sprintf('Slope coefficient for regression\n%s\nbased on %s avg. %s and x = avg. DJF ENSO3.4\n%s',...
    '$\tau_u=\beta x + \alpha$',avgType{i},'$\tau_u$',CIstr); 
    figname = sprintf('%s/tauu_%s_slope_95_u',fig_dir,avgType{i});
    plotFld(tauu_slope_95u{i},xlbl,c_lim,figname);

    xlbl = sprintf('Slope coefficients for regression\n%s\nbased on %s avg. %s and x = avg. DJF ENSO3.4\n%s',...
    '$(\tau_u,\tau_v)=(\beta_u,\beta_v) x + (\alpha_u,\alpha_v)$',avgType{i},'$\tau$',CIstr); 
    figname = sprintf('%s/tauuv_%s_slope_95_uv',fig_dir,avgType{i});
    plotFldWithWinds(tauu_slope_95uv{i},tauu_slope_95uv{i},tauv_slope_95uv{i},xlbl,c_lim,figname);
    %plotFldWithWinds(tauv_slope_95{i},tauu_slope_95{i},tauv_slope_95{i},avgType{i},c_lim);
end

keyboard

% --- Look at Tauu field before 97-98 El Nino 
%     based on regression tauu -vs- DJF ENSO3.4
tauu_enso = cell(1,Navg);
cl_msk_enso=cell(1,Navg);
enso_ind = 6; % 97/98 enso
for i=1:Navg

    [tauu_enso{i},cl_msk_enso{i}]=loadZonalEnso(avgType{i},enso_ind);

    xlbl = sprintf('Predicted %s for %s of 97/98 ENSO based on regression\n%s\n%s',...
    '$\tau_u$',avgType{i},'$\tau_u=\beta x + \alpha$',CIstr); 
    figname = sprintf('%s/tauu_%s_ENSO_9798_fit_95_u',fig_dir,avgType{i});
    plotFld(tauu_enso{i},xlbl,c_lim,figname);
end

keyboard


end

% --------------------------------------------------------------------------

function[tauu_fit, cl_msk_95_u_slope ] = loadZonalEnso(avgType,enso_ind)

ensoVar = 'eccov4r2';
matDir = sprintf('mat/reg/%s/',avgType);
loadMatFile_u = sprintf('%stauu_regression_%s.mat',matDir,ensoVar);
load(loadMatFile_u,'tauu_fit','cl_msk_95_u_slope');

tauu_fit = tauu_fit(:,:,enso_ind);

tauu_fit = tauu_fit.*cl_msk_95_u_slope;

end

% --------------------------------------------------------------------------

function[tauu_slope, cl_msk_95_u_slope ] = loadZonalSlope(avgType)

ensoVar = 'eccov4r2';
matDir = sprintf('mat/reg/%s/',avgType);
loadMatFile_u = sprintf('%stauu_regression_%s.mat',matDir,ensoVar);
load(loadMatFile_u,'tauu_coeffs','cl_msk_95_u_slope');

tauu_slope = tauu_coeffs(:,:,1);

tauu_slope = tauu_slope.*cl_msk_95_u_slope;

end

% --------------------------------------------------------------------------

function[tauu_slope_cl, tauv_slope_cl,sst_avg] = loadBothSlopes(avgType)

ensoVar = 'eccov4r2';
matDir = sprintf('mat/reg/%s/',avgType);
loadMatFile_u = sprintf('%stauu_regression_%s.mat',matDir,ensoVar);
load(loadMatFile_u,'tauu_coeffs','cl_msk_95_u_slope');

loadMatFile_v = sprintf('%stauv_regression_%s.mat',matDir,ensoVar);
load(loadMatFile_v,'tauv_coeffs','cl_msk_95_v_slope','sst_avg');

tauu_slope = tauu_coeffs(:,:,1);
tauv_slope = tauv_coeffs(:,:,1);
cl_msk = cl_msk_95_u_slope | cl_msk_95_v_slope;

tauu_slope_cl = tauu_slope .* cl_msk;
tauv_slope_cl = tauv_slope .* cl_msk;

end

% --------------------------------------------------------------------------

function [] = plotFld(tauu_fld,lblStr,c_lim,figname)

% --- If necessary, compute max for colorbar
msks=createRegionMasks;
msk=msks.pac.Ntrop+msks.pac.Strop+msks.pac.NStrop+msks.pac.SStrop;
cmax = nanmax(abs(tauu_fld.*msk));
if ~exist('c_lim','var') 
    c_lim = cmax;
elseif exist('c_lim','var') && isempty(c_lim)
    c_lim = cmax;
end

cmap = flipdim(cbrewer2('RdBu',9),1);
figureW; 
m_map_atl(tauu_fld,4.5);
colormap(cmap)
caxis([-c_lim c_lim]);
hc=niceColorbar;
%ylabel(hc,'N/m$^2$/$^\circ$C')
xlabel(lblStr);

% --- Save 
if exist('figname','var')
    savePlot(figname,'png',[1 1 1]);
end

end

% --------------------------------------------------------------------------

function [] = plotFldWithWinds(fld,uFld,vFld,lblStr,c_lim,figname)

% --- If necessary, compute max for colorbar
msks=createRegionMasks;
msk=msks.pac.Ntrop+msks.pac.Strop+msks.pac.NStrop+msks.pac.SStrop;
cmax = nanmax(abs(fld.*msk));
if ~exist('c_lim','var') 
    c_lim = cmax;
elseif exist('c_lim','var') && isempty(c_lim)
    c_lim = cmax;
end

cmap = flipdim(cbrewer2('RdBu',9),1);
figureW; 
m_map_atl(fld,4.5,{'doHold',1},{'doLabl',0});

% --- m_map_gcmfaces_uv seems to be rotated ... this seems to work
[uFld,vFld]=calc_UEVNfromUXVY(uFld,vFld);
uFld.f4=-uFld.f4; vFld.f4=-vFld.f4;
uFld.f5=-uFld.f5; vFld.f5=-vFld.f5;

m_map_gcmfaces_uv(uFld,vFld,4.5,2.5,10);
colormap(cmap)
caxis([-c_lim c_lim]);

niceColorbar;
xlabel(lblStr);

% --- Save 
if exist('figname','var')
    savePlot(figname,'png',[1 1 1]);
end

end
