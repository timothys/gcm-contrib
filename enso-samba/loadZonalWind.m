function [tauu] = loadZonalWind(runStr,diagFreq)
% Load all time records for tauu field for a single simulation
%
% Parameters
% ----------
%
%   runStr      ::  string denoting the simulation 
%                   e.g. 'no92.ref', 'xx92.p05.feb'
%                   Looks for results in ../../results/enso-samba/runStr/diags
%   diagFreq    ::  string denoting frequency of diagnostics to load
%                   'monthly' (default) or 'daily'
%
% Output
% ------
%
%   tauu        ::  gcmfaces object with records in time denoted by diagFreq
%   
% ---------------------------------------------------------------

if nargin<2
    diagFreq = 'monthly';
end

% --- Set up mat directory, file
matdir = sprintf('mat/%s',runStr);
if ~exist(matdir,'dir'), mkdir(matdir); end;
matdir = sprintf('%s/%s',matdir,diagFreq);
if ~exist(matdir,'dir'), mkdir(matdir); end;
matfile = sprintf('%s/exf_tau.mat',matdir);

if ~exist(matfile,'file')

    results_dir = sprintf('../../results/enso-samba/%s/diags',runStr);
    
    if strcmp(diagFreq,'monthly')
        results_file = sprintf('%s/exf_%s',results_dir,diagFreq);
    elseif strcmp(diagFreq,'daily')
        results_file = sprintf('%s/tau_%s',results_dir,diagFreq);
    else
        error('loadZonalWind: diagFreq not recognized')
    end

    taux = rdmds2gcmfaces(results_file,NaN,'rec',1);
    tauy = rdmds2gcmfaces(results_file,NaN,'rec',2);

    tauu = 0*taux;
    tauv = 0*tauy;

    Nt = size(taux.f1,3);

    for n = 1:Nt
        [tauu(:,:,n),tauv(:,:,n)] = calc_UEVNfromUXVY(taux(:,:,n),tauy(:,:,n));
        if mod(n,10) == 0
            fprintf(' --- calc_UEVNfromUXVY %d / %d ---\n',n,Nt);
        end
    end

    save(matfile,'tauu','tauv');
else
    load(matfile,'tauu')
end

end
