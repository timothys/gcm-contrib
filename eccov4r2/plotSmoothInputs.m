function [] = plotSmoothInputs()
% Just want a look at smooth input fields for ecco 
% to figure out how to replicate for another application...
% ---------------------------------------------------------


mygrid = establish_mygrid('llc90');

% read in the mds files
smooth2Dnorm = rdmds2gcmfaces('input_smooth/smooth2Dnorm001');
smooth3Dnorm = rdmds2gcmfaces('input_smooth/smooth3Dnorm001');

% read in binaries
smooth2Dscale = read_bin('input_smooth/smooth2Dscales001');
smooth3DscaleH = read_bin('input_smooth/smooth3DscalesH001');
smooth3DscaleZ = read_bin('input_smooth/smooth3DscalesZ001');


% Plot up 2d fields
figureW;
m_map_atl(smooth2Dnorm,8,{'myCmap','parula'})
title('smooth2Dnorm001')

figureW;
m_map_atl(smooth2Dscale(:,:,1),8,{'myCmap','parula'})
title(sprintf('smooth2Dscales001, rec 1, Lx = 3 * DXC \n equivalent to smooth3DscalesH001 recs 1-50'))

figureW;
m_map_atl(smooth2Dscale(:,:,2),8,{'myCmap','parula'})
title(sprintf('smooth2Dscales001, rec 2, Ly = 3 * DYC \n equivalent to smooth3DscalesH001 recs 51-100'))

% Plot k_lev for 3D
k_lev =10;

figureW;
m_map_atl(smooth3Dnorm(:,:,k_lev),8,{'myCmap','parula'})
title(sprintf('smooth3Dnorm001 k=%d ',k_lev))

figureW;
m_map_atl(smooth3DscaleZ(:,:,k_lev),8,{'myCmap','parula'})
title(sprintf('smooth3DscalesZ001 k=%d ',k_lev))

figureW;
m_map_atl(smooth3DscaleH(:,:,1),8,{'myCmap','parula'})
title(sprintf('smooth3DscalesH001 x'))

figureL;
plot(squeeze(smooth3DscaleZ{1}(1,1,:))./mygrid.DRC)
title(sprintf('smooth3DscaleZ001 at some random point / DRC'))


keyboard
end
