%% Plotting adj-1x1 files

expName = 'adj-tau';
if ~exist('figures/','dir'), mkdir('figures/');end

if exist(sprintf('%s/XC.data',expName),'file')
	xc=rdmds(sprintf('%s/XC*',expName));
end
if exist(sprintf('%s/YC.data',expName),'file')
	yc=rdmds(sprintf('%s/YC*',expName));
end

adtauu=rdmds(sprintf('%s/ADJtaux*',expName),NaN);

figure
st = max(size(adtauu,3)-3,1);
for i=size(adtauu,3):-1:st

	contourf(xc,yc,adtauu(:,:,i))
	colorbar

	title(sprintf('ADJtaux, lag= %d time steps',i-4-1))
	
	set(gcf,'paperorientation','landscape')
	set(gcf,'paperunits','normalized')
	set(gcf,'paperposition',[0 0 1 1])

	saveas(gcf,sprintf('figures/adtaux_%d',i),'pdf')
end

