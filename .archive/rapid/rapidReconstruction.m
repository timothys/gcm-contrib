function [ ] = rapidReconstruction(mygrid)
% Want to reconstruct AMOC at 25N from adjoint sensitivities
%
% -------------------------------------------------------------------------

%% Set up dirs
dirs = establish_rapidDirs;
if nargin<1,establish_mygrid;end
saveDir = [dirs.mat 'reconstruct.25N/'];
if ~exist(saveDir,'dir'), mkdir(saveDir);end;

%% First, grab cost function from forward model
slat = 25; 
[amoc] = calcAMOC(slat,'eulerian',mygrid);

% Forward cost function, variability
amoc_for = amoc - mean(amoc); 

%% Now go through forcing files and sensitivities
Nt = 240;
adjField = {'tauu','tauv','aqh','atemp','swdown','lwdown','precip'};
monthFields = {'jan','feb','march','april','may','jun','july','aug','sept',...
               'oct','nov','dec'};
Nadj = length(adjField); 
Nmo = length(monthFields);

amoc_adjFields = cell(Nadj,1);
amoc_adjTot = zeros(Nadj,Nt);

for i = 1:Nadj
    % Initialize fields for saving
    amoc_adjFields{i} = zeros(Nt,Nt);
    
    for m = 1:Nmo
        runStr = sprintf('%s.240mo/',monthFields{m});
        if exist(sprintf('%s/%s',dirs.mat,runStr),'dir')
            monthSaveDir=sprintf('%s%s',saveDir,runStr);
            if ~exist(monthSaveDir,'dir'),mkdir(monthSaveDir);end;
            reconstructFile = sprintf('%samoc_adj_%s.mat',monthSaveDir,adjField{i});
            if ~exist(reconstructFile,'file')
            % Load the files
            adjFile = sprintf('%s%sadj_%s.mat',dirs.mat,runStr,adjField{i});
            forceFile = sprintf('../calc/xx_%s.mat',adjField{i});
            load(adjFile,'adxx');
            load(forceFile,'xx_fld');
            
            % just a prediction on tau_x,y sign ...
            if strcmp(adjField{i},'tauu') || strcmp(adjField{i},'tauv')
                xx_fld = -1*xx_fld;
            end
            
            % Prep vector: Compute mean and add space to front for fast
            % convolution
            adxx = adxx(:,:,2:Nt+1);
            xx_fldMean = nanmean(xx_fld,3);
            xx_fld = xx_fld - repmat(xx_fldMean,[1 1 Nt]);
            xx_fld = cat(3,NaN*xx_fld,xx_fld); % now 3rd dim is 480 long
            
            amoc_adj = zeros(Nt,Nt);
            
            % Perform reconstruction for particular forcing
            for n = m:12:Nt
                tmp1 = xx_fld(:,:,n+[0:Nt-1]).*adxx;
                amoc_adj(:,n) = squeeze(nansum(nansum(convert2gcmfaces(tmp1),1),2));
%                 for k = 0:Nt-1
%                     amoc_adj(k+1,n) = nansum( xx_fld(:,:,n+k).*adxx(:,:,k+1) );
%                 end
            end
            
            % Save
            save(reconstructFile,'amoc_adj','xx_fld');
            else
            load(recondstructFile,'amoc_adj');
            end
            
            % Add to "global" container fields
            amoc_adjFields{i} = amoc_adjFields{i} + amoc_adj;
            amoc_adjTot(i,:) = amoc_adjTot(i,:) + sum(amoc_adj,1);
        end
    end
    
    % Plot reconstruction from particular variable
    figure
    plot(1:Nt,amoc_adj',1:Nt,amoc_adjTot(i,:),1:Nt,amoc_for);
    xlabel('months')
    ylabel('J''(t)')
    title(sprintf('Variability attribution to %s',adjField{i}))
end

reconstructFile = sprintf('%sfullReconstruction.mat',saveDir);
save(reconstructFile,'amoc_adjFields','amoc_adjTot','amoc_for','amoc')

keyboard
figure
plot(1:Nt,amoc_for,1:Nt,amoc_adjTot,1:Nt,sum(amoc_adjTot,1),'k')
xlabel('Months')
ylabel('J''(t)')
title(sprintf('AMOC Variability Reconstruction'))

keyboard
end