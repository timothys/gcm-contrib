if ~exist('mygrid','var')
    establish_mygrid;
end

readdir='/workspace/gcmpack/results/rapid/pfe_6mo/';

Nt = 7; 
Nr = length(mygrid.DRF);

%% Compute transport per depth and integrated transport
if ~exist('Vcc','var')
Vcc = zeros(Nr,Nt);

totsum = 0;

for n = 1:Nt
    m_trVol=read_bin([readdir 'm_trVol_month.0000000012.data'], n);
    for k = 1:Nr
        totsum=totsum+sum(sum(convert2gcmfaces(m_trVol(:,:,k))));
        Vcc(k,n) = sum(sum(convert2gcmfaces(m_trVol(:,:,k))));
    end
end
clear m_trVol
Vcc = Vcc*10^-6;      %[Sv]
end

%% Compute AMOC in Sv
% Trying the flip thing
Vcc = Vcc(end:-1:1,:);
amoc = cumsum(-Vcc,1); 
amoc = amoc(end:-1:1,:);
Vcc = Vcc(end:-1:1,:);


%% Summary stats
avgvcc = mean(Vcc,2);
stdvcc = std(Vcc,0,2);
avgamoc = mean(amoc,2);
stdamoc = std(amoc,0,2);

%% AMOC is integrated transport at maximized depth
kmax = zeros(1,Nt);
maxamoc = zeros(1,Nt);
depthmax = zeros(1,Nt);
for n = 1:Nt
    [~,kmax(n)] = max(amoc(:,n));
    maxamoc(n) = amoc(kmax(n),n);
    depthmax(n) = mygrid.RC(kmax(n));
end

figure
% plot(avgtransp,depth,avgtransp+stdtransp,depth,'k--',avgtransp-stdtransp,depth,'k--')
plot(Vcc,mygrid.RC);
    ylabel('Depth (m)')
    xlabel('Vcc (Sv)')
    
figure
% plot(avgamoc,depth,avgamoc+stdamoc,depth,'k--',avgamoc-stdamoc,depth,'k--')
plot(amoc,mygrid.RC)
    ylabel('Depth (m)')
    xlabel('AMOC (Sv)')
    
figure
subplot(2,1,1),plot(depthmax)
    xlabel('Month (1992-1996)')
    ylabel('z_{max} - AMOC maximizing depth')
subplot(2,1,2),plot(maxamoc)
    xlabel('Month (1992-1996)')
    ylabel('AMOC (Sv)')
    