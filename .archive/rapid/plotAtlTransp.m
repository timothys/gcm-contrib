if ~exist('mygrid','var')
    establish_mygrid;
end

readdir='/workspace/results/rapid/pfe_48mo/';

%% Vertical grid spacing
delR = [10*ones(1,7), 10.01, ...
        10.03, 10.11, 10.32, 10.80, 11.76, 13.42, 16.04, 19.82, 24.85, ...
        31.10, 38.42, 46.50, 55.00, 63.50, 71.58, 78.90, 85.15, 90.18, ...
        93.96, 96.58, 98.25, 99.25, 100.01,101.33,104.56,111.33,122.83,...
        139.09,158.94,180.83,203.55,226.50,249.50,272.50,295.50,318.50,...
        341.50,364.50,387.50,410.50,433.50,456.50]; 
Nr=length(delR);
depth=zeros(Nr,1);
depth(1) = delR(1);
for k=2:Nr, depth(k) = depth(k-1) + delR(k); end
Nt = 48; %size(m_trVol.f1,3)/50;

%% Latitudes
y = [-40:80];
Ny = length(y);
latoffset = y(1)-mygrid.LATS_MASKS(1).lat;

%% Compute transport per depth and integrated transport
if ~exist('Vcc','var')
Vcc = zeros(Ny,Nr,Nt);

totsum = 0;

for n = 1:Nt
    m_trVol=read_bin([readdir 'm_trVol_month.0000000012.data'], n);
    for i = 1:Ny
        latind = latoffset+i;
        msk=mygrid.LATS_MASKS(latind).mskSedge;
        for k = 1:Nr
            Vcc(i,k,n) = sum(sum(convert2gcmfaces(msk.*m_trVol(:,:,k))));
        end
    end
    fprintf('# -- Done with time step: %d/%d -- #\n',n,Nt);
end
clear m_trVol
Vcc = Vcc*10^-6;      %[Sv]
end

%% Compute AMOC in Sv
amoc = cumsum(Vcc,2); 
kmax = zeros(Ny,Nt);
maxamoc = zeros(Ny,Nt);
depthmax = zeros(Ny,Nt);
depthMatTime = repmat(depth,Nt);
for i = 1:Ny
    for n = 1:Nt
        [~,kmax(i,n)] = max(amoc(i,:,n));
        maxamoc(i,n) = amoc(i,kmax(i,n),n);
        depthmax(i,n) = depthMatTime(kmax(i,n),n);
    end
end

%% Summary Stats
avgvcc = mean(Vcc,3);
stdvcc = std(Vcc,0,3);
avgdepth = mean(depthmax,2);
stddepth = std(depthmax,0,2);
avgamoc = mean(maxamoc,2);
stdamoc = std(maxamoc,0,2);


figure
dzMatLat = repmat(delR,1,Ny);
contourf(y,depth,avgvcc'./dzMatLat);
    set(gca,'ydir','reverse')
    ylabel('Depth (m)')
    xlabel('Vcc (Sv)')
    
figure
plot(y,avgdepth,y,avgdepth+stddepth,'k--',y,avgdepth-stddepth,'k--')
    set(gca,'ydir','reverse')
    ylabel('Maximizing Depth (m)')
    xlabel('latitude')
    legend('48 month mean','+/- 1 \sigma')
    
figure
plot(y,avgamoc,y,avgamoc+stdamoc,'k--',y,avgamoc-stdamoc,'k--')
    ylabel('AMOC (Sv)')
    xlabel('latitude')
    legend('48 month mean','+/- 1 \sigma')
    
figure
subplot(2,1,1),plot(y,depthmax)
    set(gca,'ydir','reverse')
    xlabel('Latitude')
    ylabel('z_{max} - AMOC maximizing depth')
subplot(2,1,2),plot(y,maxamoc)
    xlabel('Latitude')
    ylabel('AMOC (Sv)')
    