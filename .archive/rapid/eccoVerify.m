% Want to verify what I'm getting with ECCOv4r2 product

%% Load grid
establish_mygrid;

%% Load U/V fields
if ismac && ~exist('vvel','var')
    readdir = '../../release1/nctiles_climatology';
end
if ~ismac && ~exist('vvel','var')
    readdir = '../../release2_climatology/nctiles_climatology';
end

uvel=read_nctiles([readdir '/UVELMASS/UVELMASS']);
vvel=read_nctiles([readdir '/VVELMASS/VVELMASS']);

%% Get basin mask
amocBasin = v4_basin({'atlExt'});
amocBasin = mk3D(amocBasin,vvel(:,:,:,1));

%% Preliminaries
Nt = size(vvel.f1,4);

for i = 1:Nt
    uvel(:,:,:,i) = uvel(:,:,:,i).*mygrid.mskW.*amocBasin;
    vvel(:,:,:,i) = vvel(:,:,:,i).*mygrid.mskS.*amocBasin;
end
uvelMean = nanmean(uvel,4);
vvelMean = nanmean(vvel,4);

%% Compute 20yr mean ATL Overturn streamfunction
%  Compare to the ECCO v4 supplement (Standard analysis)
fldOV = calc_overturn(uvelMean,vvelMean);
kk=find(mygrid.LATS<-35|mygrid.LATS>70);
fldOV(kk,:) = NaN;
fldOV(fldOV==0)=NaN;

%% Plotting, stolen from gael
X=mygrid.LATS*ones(1,length(mygrid.RF)); Y=ones(length(mygrid.LATS),1)*(mygrid.RF');
cc=[[-50:10:-30] [-24:3:24] [30:10:50]]; title0=sprintf('1992-2011 Mean\nAtlantic Meridional Stream Function');

figureL; set(gcf,'Renderer','zbuffer'); %set(gcf,'Units','Normalized','Position',[0.05 0.1 0.4 0.8]);
depthStretchPlot('pcolor',{X,Y,fldOV}); shading interp; %cbar=gcmfaces_cmap_cbar(cc); 
title(title0);
colorbar

%% 20 yr mean at 25N, 34S
figureL;
kind1=find(mygrid.LATS==25);
kind2=find(mygrid.LATS==-34);
plot(fldOV([kind1,kind2],:),mygrid.RF)
    xlabel('Streamfunction (Sv)')
    ylabel('Depth (m)')
    legend('25N','34S')
    title(sprintf('1992-2011 Mean\nAtlantic Meridional Stream Function'))

%% Monthly climatologies
fldOV = calc_overturn(uvel,vvel);
kk=find(mygrid.LATS<-35|mygrid.LATS>70);
fldOV(kk,:,:) = NaN;
fldOV(fldOV==0)=NaN;

%% Plot January
X=mygrid.LATS*ones(1,length(mygrid.RF)); Y=ones(length(mygrid.LATS),1)*(mygrid.RF');
cc=[[-50:10:-30] [-24:3:24] [30:10:50]]; title0=sprintf('1992-2011 Jan. Climatology\nAtlantic Meridional Stream Function');

figureL; set(gcf,'Renderer','zbuffer'); %set(gcf,'Units','Normalized','Position',[0.05 0.1 0.4 0.8]);
depthStretchPlot('pcolor',{X,Y,fldOV(:,:,1)}); shading interp; %cbar=gcmfaces_cmap_cbar(cc); 
title(title0);
colorbar

%% Plot July
X=mygrid.LATS*ones(1,length(mygrid.RF)); Y=ones(length(mygrid.LATS),1)*(mygrid.RF');
cc=[[-50:10:-30] [-24:3:24] [30:10:50]]; title0=sprintf('1992-2011 July Climatology\nAtlantic Meridional Stream Function');

figureL; set(gcf,'Renderer','zbuffer'); %set(gcf,'Units','Normalized','Position',[0.05 0.1 0.4 0.8]);
depthStretchPlot('pcolor',{X,Y,fldOV(:,:,7)}); shading interp; %cbar=gcmfaces_cmap_cbar(cc); 
title(title0);
colorbar