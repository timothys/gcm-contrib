function [] = createRelaxationMasks()

global mygrid;


nordic=v4_basin({'north','gin','barents'});
gib=v4_basin({'med'});

yCond=mygrid.YC<= -62;
xCond=mygrid.XC >=-60 & mygrid.XC <=0;
weddell=mygrid.mskC(:,:,1).*xCond.*yCond;
weddell(isnan(weddell))=0;

%% Put all together
rbcsMsk = nordic+gib+weddell;

%% Make 3D
rbcsMsk=convert2gcmfaces(rbcsMsk);
rbcsMsk=repmat(rbcsMsk(:,:,1),[1 1 50]) .* convert2gcmfaces(mygrid.mskC);

%% Relax the whole surface
rbcsMsk(:,:,1) = convert2gcmfaces(mygrid.mskC(:,:,1));

rbcsMsk(isnan(rbcsMsk))=0;

%% Make masks
mskDir='relax';
if ~exist(mskDir,'dir'), mkdir(mskDir); end;
write2file('relax/relax_mask.bin',rbcsMsk);
copyfile('createRelaxationMasks.m','relax/');
tar('relax.tar.gz','relax');
keyboard
end
