function [] = calcExfMonthClimMean( mygrid )
% Want to compute monthly climatology of external forcing files
% Since ../calc/xx_<> already contains each month's mean: 
%
%       xx_fld = mean( F(x,y,t) ) .. over e/month
%
%       Fclim_k = mean( xx_fld_i ) 
% 
% for i=1,2,...,20 corresponding to e/ month k = jan, feb, ...
% 
% -----------------------------------------------------------------

dirs=establish_longSensDirs();
saveDir = sprintf('forcing-monthClim');
if ~exist(saveDir,'dir'), mkdir(saveDir); end;

adjFields = {'lwdown','swdown','precip','aqh','atemp','tauu','tauv'};
forceFields = {'dlw','dsw','rain','spfh2m','tmp2m_degC','ustr','vstr'};
monthFlds = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
Nadj = length(adjFields);

for i = 1:Nadj
    
    % Load 20 years of monthly means
    loadFile = sprintf('../calc/xx_%s.mat',adjFields{i});    
    load(loadFile,'xx_fld');
    
    for mo = 1:12
        
        % Index months and compute clim
        n = mo:12:240;
        Fclim = nanmean(xx_fld(:,:,n),3);
        Fclim = convert2gcmfaces(Fclim);
        
        % be like jesus ... 
        saveFile = sprintf('%s/twenty_year_clim_%s_%s.data',saveDir,forceFields{i},monthFlds{mo});
        write2file(saveFile,Fclim);
        write2meta(saveFile,size(Fclim));
        
    end
    
    fprintf('# -- Done with %s -- #\n',adjFields{i});


end