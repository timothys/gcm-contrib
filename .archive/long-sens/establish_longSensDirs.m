function [dirs] = establish_longSensDirs()
% Establish directory tree for long-sens computations

matDir = 'mat/';
figDir = 'figures/';
nctilesDir = '../../release2/nctiles_monthly/';
eccoDiagsDir = '../../ecco_diags/state1/';
resultsDir = '../../results/long-sens/';

if ~exist(matDir,'dir'), mkdir(matDir);end
if ~exist(figDir,'dir'), mkdir(matDir);end
if ~exist(nctilesDir,'dir'), fprintf('ERROR: Can''t find nctiles dir\n');end
if ~exist(eccoDiagsDir,'dir'), fprintf('ERROR: Can''t find ecco diags dir\n');end
if ~exist(resultsDir,'dir'), fprintf('ERROR: Can''t find results dir\n');end

dirs = struct('mat',matDir,'figs',figDir,'nctiles',nctilesDir,...
	      'eccoDiags',eccoDiagsDir,'results',resultsDir);
end
