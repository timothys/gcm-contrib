%% Load TPSLA results. Want everything in global memory.

tpsla_mask;
landMask=mygrid.mskC(:,:,1);
clear mskK mskT myenv pacMsk yCond 

rmsPlot=1;
costFuncPlot=0;
transMaps=0;
showVideo=1;
saveVideo=0;
saveFigs=0;

%% Load sensitivities and cost function
suffix='5z_36mo';
resultsdir=['/workspace/gcmpack/tpsla/results_' suffix '/'];
filadTauu='adxx_tauu.0000000012.data';
filadTauv='adxx_tauv.0000000012.data';
filboxmean='m_boxmean_eta';
if ~exist('adTauu','var')
  adTauu=-2*1e-7*read_bin([resultsdir filadTauu]);
  adTauv=-2*1e-7*read_bin([resultsdir filadTauv]);
end
if ~exist('bmEta','var')
  bmEta=rdmds([resultsdir filboxmean],NaN); 
end


Nt=size(adTauu,3);
rmsadTauu=zeros(size(Nt));
rmsadTauv=rmsadTauu;
%minadTauu=rmsadTauu;

Neta=size(bmEta,3);
% etaCost=zeros(Neta,1);

%% My attempt at plotting 
if rmsPlot

  a=figure;
  for i=1:Nt
      %rmsadTauu(i)=rms(rms(sqrt(adTauu(:,:,i).^2 + adTauv(:,:,i).^2)));
      rmsadTauu(i) = sqrt(1/(90*1170)*sum(sum(adTauu(:,:,i).^2 + adTauv(:,:,i).^2)));
      rmsadTauv(i) = rms(rms(sqrt(adTauu(:,:,i).^2 + adTauv(:,:,i).^2)));
      
      %minadTauu(i)=min(min(adTauu(:,:,i)));
  end
  t=1:Nt;
  plot(t,rmsadTauu,t,rmsadTauv)
  xlabel('month')
  ylabel('rms(ADJ Sensitivity to \tau_x)')
  if saveFigs, saveas(a,sprintf('rms_sens_%s',suffix),'pdf');end
   
  keyboard
end

%% Sensitivity dJ/dTau maps
% First show a few time stamps using fancy plots
b=figure;
if transMaps

  % Convert sensitivities to transport values
  for i=1:10:Nt
      adTauug=convert2gcmfaces(adTauu(:,:,i)).*mygrid.mskW(:,:,1);
      adTauvg=convert2gcmfaces(adTauv(:,:,i)).*mygrid.mskS(:,:,1);
      [U,V]=calc_UEVNfromUXVY(adTauug,adTauvg);
      figure(b),m_map_gcmfaces_uv(U,V);
  
      colorbar;
      title(sprintf('Sensitivity to %s_x\nTime: 1995 - (%d months)','\tau',Nt-i+1))
      if saveFigs,saveas(b,sprintf('transport-maps/%s/U_%dmo',suffix,Nt-i+1),'pdf');end
  end
end

if showVideo

  if saveVideo
      vidObj = VideoWriter(sprintf('dJdTau_%s',suffix));
      set(vidObj,'FrameRate',2)
      open(vidObj);
  end
  c=figure;
  for i=Nt:Nt
  
      adTauug=convert2gcmfaces(adTauu(:,:,i)).*mygrid.mskW(:,:,1);
      adTauvg=convert2gcmfaces(adTauv(:,:,i)).*mygrid.mskS(:,:,1);
      [U,V]=calc_UEVNfromUXVY(adTauug,adTauvg);
      figure(c),qwckplot(U.*mygrid.mskW(:,:,1));
      colorbar;
      mm=max(max(abs(U)));
      caxis([-mm mm])
      title(sprintf('Sensitivity to Wind Stress %s_x\nTime: 1995 - (%d months)','\tau',Nt-i+1))
      if ~saveVideo
          pause(1)
      end
      if saveVideo
          currFrame=getframe(c);
          writeVideo(vidObj,currFrame);
      end
  end
  if saveVideo
      close(vidObj);
  end
end

keyboard

if costFuncPlot
  for i=1:Neta
      etaCost(i)=squeeze(sum(sum(bmEta(:,:,i))));
  end
  figure
  plot(etaCost)
  legend('Boxmean Cost: Eta')
end
keyboard

%% Grab full ECCO SLA
% grab and mask etaN from state diagnostics
diagsdir='/workspace/gcmpack/tpsla/ecco_diags/state1/';
fil='state_2d_set1';
if ~exist('eta','var')
eta=rdmds([diagsdir fil],NaN,'rec',1);
taux=rdmds([diagsdir fil],NaN,'rec',14);
tauy=rdmds([diagsdir fil],NaN,'rec',15);
end

Necco=size(eta,3);
etaSum=zeros(Necco,1);
etaRMS=etaSum;
posSum=etaSum;
negSum=etaSum;
% convTau=zeros(Necco,1);
convTable=zeros(Necco,Necco+Nt);

posMsk = convert2gcmfaces(posMsk);
negMsk = convert2gcmfaces(negMsk);

totWt=1/(sum(sum(posMsk))+sum(sum(abs(negMsk))));
posWt=1/sum(sum(posMsk));
negWt=1/sum(sum(abs(negMsk)));
for i=1:Necco
% Use these for plotting maps   
%     etag=convert2gcmfaces(mskC.*eta(:,:,i));
%     qwckplot(etag)
%     pause(1)
%       gg=convert2gcmfaces(mskC.*eta(:,:,i));
%       qwckplot(gg)
%       keyboard
    posSum(i) = posWt*squeeze(sum(sum(posMsk.*eta(:,:,i))));
    negSum(i) = negWt*squeeze(sum(sum(negMsk.*eta(:,:,i))));
    etaSum(i) = posSum(i) + negSum(i);

    for j=1:Nt
        convTable(i,i+j-1) = totWt*squeeze(sum(sum(adTauu(:,:,j).*taux(:,:,i) + adTauv(:,:,j).*tauy(:,:,i))));
    end
end

temp=sum(convTable,1);
convTau=temp(Nt+1:end);
tt=1:Necco;

d=figure;
figure(d),plot(tt,etaSum,tt,convTau);
xlabel('months')
ylabel('\eta')
hl=legend({'ECCO Cost func','dJ/d$\tau$(t-T) * $\tau$''(t)'},'Interpreter','Latex');
if saveFigs, saveas(d,sprintf('CostFunc&ConvSens_%s',suffix),'pdf'); end
mskcg=convert2gcmfaces(mskC);


% Load eta from tpsla experiment
stateDir =[resultsdir 'state/'];
expFile = 'state_2d_set1';
if ~exist('tauxExp','var')
tauxExp = rdmds([stateDir expFile],NaN,'rec',14);
tauyExp = rdmds([stateDir expFile],NaN,'rec',15);
end
dJ = zeros(size(adTauu));
rmsGT = zeros(size(adTauu(:,:,1)));

for i=1:Nt
    dJ(:,:,i) = adTauu(:,:,i).*tauxExp(:,:,i) + adTauv(:,:,i).*tauyExp(:,:,i);
end

rmsGT = convert2gcmfaces(rms(dJ,3));

ee=figure;
figure(ee),qwckplot(rmsGT.*landMask)
colorbar
if saveFigs, saveas(ee,sprintf('rmsGT_%s',suffix),'pdf');end



%figure,qwckplot(mskcg.*landMask),colorbar
% figure
% plot(tt,etaSum)
% xlabel('Time')
% ylabel('ECCO Cost func')

% clear mygrid eta taux tauy
