
tpsla_mask; % Establishes masks 
clearvars -except etp_msk wtp_msk mygrid % Clear up a bit of memory

saveVideo = 0; % Change this to save a video of adj. sensitivity plots

if 0;
  % Load from *.data files, save to *.mat for quick reload
  doLoad=1; doReLoad=0; doComp=0; doDisp=0;
else;
  % Load from *.mat, do computation and plots
  doLoad=0; doReLoad=1; doComp=1; doDisp=1;
end;

suffix = 'engaging/2masks'; % Change for directory to data
if doReLoad;
  dirAdj=['/workspace/gcmpack/tpsla/results_' suffix '/'];
  %
  dirIn=[dirAdj 'mat/']; 
  if ~exist(dirIn,'dir'), mkdir(dirIn), end
  dirOut=[dirAdj 'plots/'];
  if ~exist(dirOut,'dir'), mkdir(dirOut), end
  %
  load([dirIn 'ETAN.mat']);
  load([dirIn 'oceTAUX.mat']);
  load([dirIn 'oceTAUY.mat']);
  load([dirIn 'adjTAUX.mat']);
  load([dirIn 'adjTAUY.mat']);
  %
  etp_msk = convert2gcmfaces(abs(etp_msk));
  wtp_msk = convert2gcmfaces(wtp_msk);
  RAC=mygrid.RAC;
end;

if doLoad;
  doOutput=1;
  dirAdj=['/workspace/gcmpack/tpsla/results_' suffix '/'];
  dirDiags=['/workspace/gcmpack/tpsla/ecco_diags/state1/'];
  dirOut=[dirAdj 'mat/'];
  if ~exist(dirOut,'dir'), mkdir(dirOut), end

%'ETAN    ' 'SIarea  ' 'SIheff  ' 'SIhsnow ' 'DETADT2 '
%'PHIBOT  ' 'sIceLoad' 'MXLDEPTH' 'oceSPDep' 'SIatmQnt'
%'SIatmFW ' 'oceQnet ' 'oceFWflx' 'oceTAUX ' 'oceTAUY '
%'ADVxHEFF' 'ADVyHEFF' 'DFxEHEFF' 'DFyEHEFF' 'ADVxSNOW'
%'ADVySNOW' 'DFxESNOW' 'DFyESNOW' 'SIuice  ' 'SIvice  '


  %1) load eta: 
  tic;
  ETAN=rdmds2gcmfaces([dirDiags 'state_2d_set1*'],NaN,'rec',1);
  sIceLoad=rdmds2gcmfaces([dirDiags 'state_2d_set1*'],NaN,'rec',7);
  
  ETAN=ETAN+1/1029*sIceLoad;
  ETAN=ETAN.*repmat(mygrid.mskC(:,:,1),[1 1 240]);
  clear sIceLoad;
  toc;

  %2) load wind stres:
  tic;
  oceTAUX=rdmds2gcmfaces([dirDiags 'state_2d_set1*'],NaN,'rec',14);
  oceTAUY=rdmds2gcmfaces([dirDiags 'state_2d_set1*'],NaN,'rec',15);
  
  oceTAUX=oceTAUX.*repmat(mygrid.mskW(:,:,1),[1 1 240]);
  oceTAUY=oceTAUY.*repmat(mygrid.mskS(:,:,1),[1 1 240]);
  for tt=1:240;
    [oceTAUX(:,:,tt),oceTAUY(:,:,tt)]=calc_UEVNfromUXVY(oceTAUX(:,:,tt),oceTAUY(:,:,tt));
  end;

  oceTAUXmean=mean(oceTAUX,3);
  oceTAUX=oceTAUX-repmat(oceTAUXmean,[1 1 240]);
  oceTAUX=cat(3,NaN*oceTAUX(:,:,1:35),oceTAUX);
  
  oceTAUYmean=mean(oceTAUY,3);
  oceTAUY=oceTAUY-repmat(oceTAUYmean,[1 1 240]);
  oceTAUY=cat(3,NaN*oceTAUY(:,:,1:35),oceTAUY);
  toc;

  %3) load adjoint:
  tic;
  % Note: the coefficient for adjTAU*: (-2) will need to change based on the number
  % of mask files you have. With 2 masks (e.g. 1 East, 1 West) then a -2 is
  % appropriate. If 1 mask=>(-1), 4 masks => (-4). I'm not sure why.

  % 2nd Note: the coefficient 1e-7 is from data.ecco, which has the multiplier
  % of 1e7. This is to amplify the cost function so it doesn't get mixed up 
  % in nonlinear noise in the adjoint. Gael could explain this better. 

  adjTAUX=-2*1e-7*read_bin([dirAdj 'adxx_tauu.effective.0000000012.data']);
  adjTAUY=-2*1e-7*read_bin([dirAdj 'adxx_tauv.effective.0000000012.data']);
  adjTAUX=gcmfaces_interp_1d(3,(7+[1:80]*14)/366,adjTAUX,[0.5:35.5]/12);
  adjTAUY=gcmfaces_interp_1d(3,(7+[1:80]*14)/366,adjTAUY,[0.5:35.5]/12);
  toc;

  %save result to disk:
  if doOutput;
    save([dirOut 'ETAN.mat'],'ETAN');
    save([dirOut 'oceTAUX.mat'],'oceTAUX');
    save([dirOut 'oceTAUY.mat'],'oceTAUY');
    save([dirOut 'adjTAUX.mat'],'adjTAUX');
    save([dirOut 'adjTAUY.mat'],'adjTAUY');
  end;
end;

if doComp;
  %1) time series of ETAN over the two boxes:
  tic;
  etp_rac=RAC.*etp_msk; 
  wtp_rac=RAC.*wtp_msk;
  for tt=1:240;
    etp_ETAN(tt)=nansum(ETAN(:,:,tt).*etp_rac)/nansum(etp_rac);
    wtp_ETAN(tt)=nansum(ETAN(:,:,tt).*wtp_rac)/nansum(wtp_rac);
  end;
  toc;

  %1.5) time series of adjoint sensitivity rms
  tic;
  adjTAUrms=zeros(1,36);
  for tt=1:36;
    tmp1=mygrid.mskC(:,:,1).*(adjTAUX(:,:,tt).^2+adjTAUY(:,:,tt).^2);
    adjTAUrms(tt)=sqrt(nanmean(tmp1));
  end;
  toc;

  %2) reconstruction
  wtpMetp_target=wtp_ETAN-etp_ETAN;
  wtpMetp_adjtau=NaN*zeros(1,240);
  wtpMetp_adjtaux_meansq=0*mygrid.mskC(:,:,1);
  wtpMetp_adjtauy_meansq=0*mygrid.mskC(:,:,1);
  for tt=1:240;
    tmp1=oceTAUX(:,:,tt+[0:35]).*adjTAUX;
    tmp2=oceTAUY(:,:,tt+[0:35]).*adjTAUY;
    wtpMetp_adjtau(tt)=nansum(tmp1)+nansum(tmp2);
    wtpMetp_adjtaux_meansq=wtpMetp_adjtaux_meansq+nansum(tmp1.^2,3)/240;
    wtpMetp_adjtauy_meansq=wtpMetp_adjtauy_meansq+nansum(tmp2.^2,3)/240;
  end;

  %3) display
  if doDisp;
    figureL; 
    set(gca,'FontSize',16);
    plot(wtpMetp_target-median(wtpMetp_target),'LineWidth',2);
    hold on; plot(wtpMetp_adjtau,'r','LineWidth',2);
    legend('full ecco','adj x tau'); grid on;
	saveas(gcf,sprintf('%sfig1',dirOut),'pdf')
	close;
    
    figureL; 
	 m_map_gcmfaces(sqrt(wtpMetp_adjtaux_meansq),1.2,{'myCaxis',[0:0.1:1]*6e-5});
	saveas(gcf,sprintf('%sfig2',dirOut),'pdf')
	close;

    figureL;
	m_map_gcmfaces(log10(wtpMetp_adjtaux_meansq),1.2,{'myCaxis',[-16:0.5:-8]});
	saveas(gcf,sprintf('%sfig3',dirOut),'pdf')
	close;

    figureL; m_map_gcmfaces(sqrt(wtpMetp_adjtauy_meansq),1.2,{'myCaxis',[0:0.1:1]*6e-5});
	saveas(gcf,sprintf('%sfig4',dirOut),'pdf')
	close;

    figureL; m_map_gcmfaces(log10(wtpMetp_adjtauy_meansq),1.2,{'myCaxis',[-16:0.5:-8]});
	saveas(gcf,sprintf('%sfig5',dirOut),'pdf')
	close;

    figureL; m_map_gcmfaces(wtp_msk-etp_msk,1.2,{'myCaxis',[-2 2]});
	saveas(gcf,sprintf('%sfig6',dirOut),'pdf')
	close;

    figureL; set(gca,'FontSize',16); plot(adjTAUrms,'LineWidth',2);
    xlabel('month'); ylabel('rms(adj)'); grid on;
	saveas(gcf,sprintf('%sfig7',dirOut),'pdf')
	close;

    figureL; m_map_gcmfaces(adjTAUX(:,:,35),1.2,{'myCaxis',[-1:0.2:1]*1e-3});
	saveas(gcf,sprintf('%sfig8',dirOut),'pdf')
	close;

    figureL; m_map_gcmfaces(adjTAUX(:,:,29),1.2,{'myCaxis',[-1:0.2:1]*5e-4});
	saveas(gcf,sprintf('%sfig9',dirOut),'pdf')
	close;

    figureL; m_map_gcmfaces(adjTAUX(:,:,23),1.2,{'myCaxis',[-1:0.2:1]*3e-4});
	saveas(gcf,sprintf('%sfig10',dirOut),'pdf')
	close;

    if saveVideo
      Nt=size(adjTAUX.f1,3);
      vidObj = VideoWriter(sprintf('%sdJdTau_%s',dirOut,suffix));
      set(vidObj,'FrameRate',2)
      open(vidObj);
      c=figure;
      for i=1:Nt
        figure(c),qwckplot(adjTAUX(:,:,i).*mygrid.mskC(:,:,1));
        colorbar;
        caxis([nanmin(adjTAUX(:,:,i)) nanmax(adjTAUX(:,:,i))])
        title(sprintf('Sensitivity to Wind Stress %s_x\nTime: 1995 - (%d months)','\tau',Nt-i+1))
        currFrame=getframe(c);
        writeVideo(vidObj,currFrame);
      end
      close(vidObj);
      close;
    end;
  end;
end;


