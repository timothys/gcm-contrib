% Goal is to recreate Gael's Tropical Pacific Sea Level Anomaly Plot

if ~exist('mygrid','var')
  runDir='/workspace/gcmpack/GRID/';
  grid_load(runDir,5, 'compact');
  gcmfaces_global;
end

%% Create directory for masks
if ~exist('masks/','dir'), mkdir('masks'),end

%% 2D tracer field mask
% Masking a specific part of the pacific basin
    pacMsk=v4_basin('pac');
    yCond=mygrid.YC<20 & mygrid.YC>-20;
    eastCond = mygrid.XC > -130 & mygrid.XC < -70; 
    westCond = mygrid.XC < 180 & mygrid.XC > 120;

    etp_msk = -1*pacMsk .*yCond .* eastCond .* mygrid.mskC(:,:,1);
    wtp_msk = 1*pacMsk .* yCond .* westCond .* mygrid.mskC(:,:,1);

    tpsla_msk = etp_msk + wtp_msk;

% Convert all from gcmfaces
    etp_msk = convert2gcmfaces( etp_msk );
    wtp_msk = convert2gcmfaces( wtp_msk );
    tpsla_msk = convert2gcmfaces( tpsla_msk );
    
    etp_msk( isnan(etp_msk) ) = 0;
    wtp_msk( isnan(wtp_msk) ) = 0;
    tpsla_msk( isnan(tpsla_msk) ) = 0;


%% Depth level mask
% Not necessary for 2d field eta
% Grab selected elements of the water column 
   mskK=zeros(1,50); mskK(1) = 1;

    
%% Temporal mask
% Only want to compute cost function over last month
%   after three years of simulation
    mskT=zeros(1,240); mskT(36)=1;

%% Write all to file
%     write2file('masks/etp_mskC',etp_msk);
%     write2file('masks/wtp_mskC',wtp_msk);
    write2file('masks/tpsla_mskC',tpsla_msk);
    write2file('masks/tpsla_maskK',mskK);
%     write2file('masks/etp_mskT',mskT);
%     write2file('masks/wtp_mskT',mskT);
    write2file('masks/tpsla_mskT',mskT);
    
%% For reproducibility and such ... 
%  copy this file as well so we know what made the masks
copyfile('tpsla_mask.m','masks/');

% Zip it up for sending to a supercomputer 
tar('masks.tar.gz','masks/');

% qwckplot(convert2gcmfaces(tpsla_msk).*mygrid.mskC(:,:,1))
