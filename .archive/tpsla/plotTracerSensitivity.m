
%% Load TPSLA results. Want everything in global memory.
% Just playing around here

tpsla_mask;
clear mskK mskT myenv pacMsk yCond 

showVideo=1;
saveVideo=1;

%% Load sensitivities and cost function
resultsdir='/workspace/gcmpack/tpsla/results_5z_36mo/';
filadAtemp='ADJatemp';
filadSST='ADJclimsst';
if ~exist('adSST','var')
  adSST=rdmds([resultsdir filadSST],NaN);
  adAtemp=rdmds([resultsdir filadAtemp],NaN);
end

Nt=size(adSST,3);

if showVideo

  if saveVideo
      vidObj = VideoWriter('dJdsst_5z_36mo');
      set(vidObj,'FrameRate',2)
      open(vidObj);
  end
  a=figure;
  for i=1:Nt
  
      adAtempg=convert2gcmfaces(adAtemp(:,:,i)).*mygrid.mskC(:,:,1);
      figure(a),qwckplot(adAtempg);
      colorbar;
      mm=max(max(abs(adAtemp(:,:,i))));
      caxis([-mm mm]);
      title(sprintf('Sensitivity to Air Temp.\nTime: 1995 - (%d months)',Nt-i+1));
      if ~saveVideo
          pause(1)
      end
      if saveVideo
          currFrame=getframe(a);
          writeVideo(vidObj,currFrame);
      end
  end
  if saveVideo
      close(vidObj);
  end
end

