function [ ] = projectCtrls(climOpt,mygrid) 
% Goal is to produce ctrl vectors based on 2 week and monthly climatologies 
% over the full 24 years, and a method for producing a 2016 ctrl vector based on
% 2 week climatologies from the previous 2 years.
%
% ------------------------------------------------------------------

%% 1. Establish grid and directories
if ~exist('climOpt','var'), climOpt='biweekly'; end;
if ~exist('mygrid','var'), establish_mygrid; end;

ctrlDir = '/workspace/gcmpack/ctrls-r3/';
mainSaveDir = 'projected-ctrls/';
if strcmp(climOpt,'biweekly')
  subDir = [mainSaveDir 'full/'];
elseif strcmp(climOpt,'2yr')
  subDir = [mainSaveDir '2yr/'];
else
  fprintf('Climatology option not recognized ...\n');
  return;
end

ctrlSaveDir = [subDir 'ctrls/'];
varSaveDir = [subDir 'var/'];

if ~exist(ctrlDir,'dir'), fprintf('Error: Can''t find controls ... exiting ...\n'); return; end;
if ~exist(mainSaveDir,'dir'), mkdir(mainSaveDir); end;
if ~exist(subDir,'dir'), mkdir(subDir); end;
if ~exist(ctrlSaveDir,'dir'), mkdir(ctrlSaveDir); end;
if ~exist(varSaveDir,'dir'), mkdir(varSaveDir); end;
% ------------------------------------------------------------------

%% 2. Set variables based on climOpt
if strcmp(climOpt,'biweekly')
	recSt = 1;
	recEnd=627-1;

elseif strcmp(climOpt,'2yr')
	recSt=627-52+1;
	recEnd=627-1;
else
	fprintf('Climatology option not recognized ...\n');
end 
fldNames = {'tauu','tauv','atemp','lwdown','swdown','aqh','precip'};

Nproj = 652-627+1;
xx_proj = repmat(0.0*mygrid.mskC(:,:,1),[1 1 Nproj]);
var_proj= xx_proj;
% ------------------------------------------------------------------

%% 3. Loop thru e/ var and take climatology 
for i=1:length(fldNames)

  xx_tmp = rdmds2gcmfaces([ctrlDir 'xx_' fldNames{i}],59);
  Nrec = size(xx_tmp.f1,3);
  recip = 1/24;
  projInd = zeros(1,Nrec);
  
  saveStr = [ctrlSaveDir 'xx_' fldNames{i} '.0000000059.data'];
  if ~exist(saveStr,'file')
    % First compute the mean
    for j=recSt:recEnd
    
      % Compute interpolated record and find index for xx_proj
      [ xx, k ] = interpRecs( xx_tmp(:,:,j), xx_tmp(:,:,j+1), j);
      
      % Accumulate to kth record for the mean
      xx_proj(:,:,k) = xx_proj(:,:,k) + xx*recip;
    end
    
    % Now save xx_proj appended to control vector
    xx_tmp = cat(3, xx_tmp(:,:,1:Nrec-1), xx_proj);
    xx_tmp=convert2gcmfaces(xx_tmp); xx_tmp(isnan(xx_tmp))=0;
    write2file(saveStr,xx_tmp);
    write2meta(saveStr,size(xx_tmp));
    xx_tmp = convert2gcmfaces(xx_tmp);
    fprintf('Written climatology for %s ...\n',fldNames{i});

  else
    if ~exist([varSaveDir fldNames{i} '.data'])
      xx_tmp=rdmds2gcmfaces([ctrlSaveDir 'xx_' fldNames{i}],59);
      xx_proj=xx_tmp(:,:,628:653);
      fprintf('Loaded climatology for %s ...\n',fldNames{i});
    end
  end

saveStr = [varSaveDir fldNames{i} '.data'];
if ~exist(saveStr,'file')
  % Now compute the variance
  for j=recSt:recEnd
  
  	% Compute interpolated record and find index for variance
  	[ xx, k ] = interpRecs( xx_tmp(:,:,j), xx_tmp(:,:,j+1), j);
  	var_proj(:,:,k) = var_proj(:,:,k) + ((xx-xx_proj(:,:,k)).^2)*recip;
  end
  
  % And save variance
  var_proj = convert2gcmfaces(var_proj); var_proj(isnan(var_proj))=0;
  write2file(saveStr,var_proj);
  write2meta(saveStr,size(var_proj));
  var_proj = convert2gcmfaces(var_proj);
  fprintf('Written variance for %s ...\n',fldNames{i});
end

end %for i=1:fldNames
end
% ------------------------------------------------------------------

function [ xx, projInd ] = interpRecs(xx0, xx1, j) 
% Interpolate between records xx0 & xx1 where xx0 sits at record j

% Records for projecting 
Nproj = 652-627+1; 
projDate = rec2date([627:652]);

% Dates for interpolation 
date0 = rec2date(j);
date1 = rec2date(j+1); 

foundRefDate=0;
k=0;

% Find the reference date (i.e. the date associated with a particular 
% element of xx_proj) that sits between records j & j+1
while ~foundRefDate && k<Nproj
  k=k+1;
  lb = projDate(k)-14; 
  if lb <= 0 && date0>351, lb = 366+lb; end;
  
  ub = projDate(k)+14;
  if ub >= 366 && date1<15, ub = ub-366; end;

  foundRefDate = lb<=date0 && ub>=date1;
end

if k==Nproj && ~foundRefDate
	fprintf('ERROR: Couldn''t find a bound between records j=%d & j=%d\n',j,j+1);
	keyboard
end

if date0>351 && projDate(k)<15
  date0=date0-366;
end 

% Linearly interpolate between the two records 
% to get controls at the particular day of xx_proj(k)
alpha = (projDate(k) - date0)/14;
xx = (1-alpha)*xx0 + alpha*xx1;
projInd = k;
end
