function [] = enso_mask(mygrid)
% Want a mask for computing ENSO3.4 as an objective function (except not a 3month avg)

if ~exist('mygrid','var'), establish_mygrid; end;

%% Create directory for masks
if ~exist('masks/','dir'), mkdir('masks'),end

%% 2D tracer field mask
% Masking a specific part of the pacific basin
pacMsk=v4_basin('pac');
yCond=mygrid.YC<5 & mygrid.YC>-5;
xCond = mygrid.XC < -120 & mygrid.XC > -170;

mskC = pacMsk.*yCond.*xCond;
mskC = convert2gcmfaces(mskC);


%% Depth level mask
mskK=zeros(1,50); mskK(1) = 1;
    
%% Temporal mask
mskT=ones(1,300); 

%% Write all to file
write2file('masks/enso_mskC',mskC);
write2file('masks/enso_mskK',mskK);
write2file('masks/enso_mskT',mskT);

%% For reproducibility and such ... 
%  copy this file as well so we know what made the masks
copyfile('enso_mask.m','masks/');

% Zip it up for sending to a supercomputer 
tar('masks.tar.gz','masks/');
end
