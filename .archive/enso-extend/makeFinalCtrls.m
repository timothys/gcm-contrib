function [] = makeFinalCtrls()
%% Preliminaries
run('~/gcm-contrib/matlab/startup.m')

addpath(genpath('~/gcmfaces'));
addpath(genpath('~/MITprof'));
addpath(genpath('~/MITgcm_c65q/utils/matlab/'))
addpath(genpath('~/gcm-contrib/matlab/'))
addpath(genpath('~/gcm-contrib/plots/'))
addpath(genpath('~/gcm-contrib/calc/'));

runDir = '/scratch/tsmith/enso2/run.eig_a/';
grid_load(runDir,5,'compact');
gcmfaces_global;
% ---


%% Grab iterations in results dir
% resultsDir = '/work/03754/tsmith/lonestar/enso/run.eig_a/diags/';
resultsDir = [runDir 'diags/'];
exfName = 'exf';
%iters = grabAllIters(resultsDir,exfName);

% 'EXFtaux ' 'EXFtauy ' 'EXFqnet ' 'EXFempmr' 'EXFatemp' 'EXFaqh  ' 
% 'EXFlwdn ' 'EXFswdn ' 'EXFevap ' 'EXFpreci' 'EXFroff '
% --- 

%% Loop through diagnostic variables, then grab and save as control vectors
% saveDir = '/work/03754/tsmith/ecco-data/control-vectors-2015';
saveDir = 'ctrl-vectors-2015-final/';
loadDir = 'ctrl-vectors-2015/';
loadDir0 = 'ctrl-vectors-2015-b0/';
loadDir15 = '/scratch/tsmith/data/input_v4_rls2.025l.iter59/ADXXfiles_it59/';

if ~exist(saveDir), mkdir(saveDir); end

prefix = [saveDir 'xx_'];
suffix = '.0000000059.data';
fldNames = {'tauu','tauv','atemp','aqh','lwdown','swdown','precip'};
recNum = [1 5 6 7 8 10];

for i = 1:length(fldNames)

	xx_tmp = rdmds2gcmfaces([loadDir 'xx_' fldNames{i}],59);
	xx_0 = rdmds2gcmfaces([loadDir0 'xx_' fldNames{i}],59);
	xx_N = rdmds2gcmfaces([loadDir15 'xx_' fldNames{i}],59);

	xx_tmp(:,:,1) = xx_0(:,:,1);
	xx_tmp(:,:,628)=xx_N(:,:,628);

	xx_tmp=convert2gcmfaces(xx_tmp);
	xx_tmp(isnan(xx_tmp))=0;
	write2file([prefix fldNames{i} suffix],xx_tmp);
	write2meta([prefix fldNames{i} suffix],size(xx_tmp));
	fprintf('File %s written ... \n',[prefix fldNames{i} suffix]);

end
end
