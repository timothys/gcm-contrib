function [] = diag2ctrl()
%
% On Lonestar, want to take diagnostic output of EXF fields and 
% convert to control vector format
%
% -------------------------------------------------------------

%% Preliminaries
run('~/gcm-contrib/matlab/startup.m')

addpath(genpath('~/gcmfaces'));
addpath(genpath('~/MITprof'));
addpath(genpath('~/MITgcm_c65q/utils/matlab/'))
addpath(genpath('~/gcm-contrib/matlab/'))

%runDir = '/scratch/tsmith/enso2/run.eig_a/';
runDir = '/scratch/tsmith/enso2/run.eig_b/';
grid_load(runDir,5,'compact');
gcmfaces_global;
% ---


%% Grab iterations in results dir
% resultsDir = '/work/03754/tsmith/lonestar/enso/run.eig_a/diags/';
resultsDir = [runDir 'diags/'];
exfName = 'exf';
%iters = grabAllIters(resultsDir,exfName);

% 'EXFtaux ' 'EXFtauy ' 'EXFqnet ' 'EXFempmr' 'EXFatemp' 'EXFaqh  ' 
% 'EXFlwdn ' 'EXFswdn ' 'EXFevap ' 'EXFpreci' 'EXFroff '
% --- 

%% Loop through diagnostic variables, then grab and save as control vectors
saveDir = 'ctrl-vectors-2015/';
%saveDir = 'forcing-plus-ctrls-2015/';
if ~exist(saveDir), mkdir(saveDir); end

prefix = [saveDir 'xx_'];
suffix = '.0000000059.data';
fldNames = {'tau','atemp','aqh','lwdown','swdown','precip'};
recNum = [1 5 6 7 8 10];

for i = 1:length(fldNames)

	%% Special case for wind stress, need to flip UX,VY
	if ~isempty(strfind(fldNames{i},'tau'))
		tmpu = rdmds2gcmfaces([resultsDir exfName],NaN,'rec',recNum(i));
		tmpv = rdmds2gcmfaces([resultsDir exfName],NaN,'rec',recNum(i)+1);

		Nt = size(tmpu.f1,3);
		for n=1:Nt
			[tmpu(:,:,n),tmpv(:,:,n)] = calc_UEVNfromUXVY(tmpu(:,:,n),tmpv(:,:,n));
		end

		tmpu=convert2gcmfaces(tmpu); tmpu(isnan(tmpu))=0;
		tmpv=convert2gcmfaces(tmpv); tmpv(isnan(tmpv))=0;

		if size(tmpu,3)~=626
			fprintf('tmpu dim3 not = 626 ... \n')
			keyboard
		end

		%% Add extra spot for time 0
		tmpu = cat(3,tmpu(:,:,1),tmpu);
		tmpv = cat(3,tmpv(:,:,1),tmpv);

		%% Add extra spot for time N
		tmpu = cat(3,tmpu,tmpu(:,:,end));
		tmpv = cat(3,tmpv,tmpv(:,:,end));

		% %% Switch sign from step b
		% tmpu=-tmpu;
		% tmpv=-tmpv;
		
		write2file([prefix 'tauu' suffix],tmpu);
		write2meta([prefix 'tauu' suffix],size(tmpu));
		fprintf('File %s written ... \n',[prefix 'tauu' suffix]);

		write2file([prefix 'tauv' suffix],tmpv);
		write2meta([prefix 'tauv' suffix],size(tmpv));
		fprintf('File %s written ... \n',[prefix 'tauv' suffix]);

		clear tmpu tmpv;
	
	%% Otherwise super straight forward
	else

		xx_tmp = rdmds2gcmfaces([resultsDir exfName],NaN,'rec',recNum(i));	

		% %% Switch sign from step b
		% xx_tmp=-xx_tmp;
		
		xx_tmp=convert2gcmfaces(xx_tmp);
		xx_tmp(isnan(xx_tmp))=0;
		
		if size(xx_tmp,3)~=626
			fprintf('xx_tmp dim3 not = 626 ... \n')
			keyboard
		end

		%% Add extra spot for time 0 
		xx_tmp = cat(3,xx_tmp(:,:,1),xx_tmp);
		xx_tmp = cat(3,xx_tmp,xx_tmp(:,:,end));
	
		write2file([prefix fldNames{i} suffix],xx_tmp);
		write2meta([prefix fldNames{i} suffix],size(xx_tmp));
		fprintf('File %s written ... \n',[prefix fldNames{i} suffix]);
	end
end
end
