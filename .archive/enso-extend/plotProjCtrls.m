function [] = plotProjCtrls( climOpt, mygrid )
% Want to plot projected control climatologies and variance
% -----------------------------------------------------------

if ~exist('climOpt','var'), climOpt='biweekly'; end;
if ~exist('mygrid','var'), establish_mygrid; end;

mainSaveDir = 'projected-ctrls/';
if strcmp(climOpt,'biweekly')
  subDir = [mainSaveDir 'full/'];
elseif strcmp(climOpt,'2yr')
  subDir = [mainSaveDir '2yr/'];
else
  fprintf('Climatology option not recognized ...\n');
  return;
end

loadDir = [subDir 'proj/'];
saveDir = [subDir 'plots/'];

if ~exist(loadDir,'dir'), fprintf('Can''t find controls, exiting ...\n'); return; end;
if ~exist(saveDir,'dir'), mkdir(saveDir); end;

fldNames = {'tauu','tauv','atemp','lwdown','swdown','aqh','precip'};

% -----------------------------------------------------------
%% Plot climatology
for i=1:length(fldNames)

	xx_proj = rdmds2gcmfaces([loadDir 'xx_' fldNames{i}],59);
	if strcmp(climOpt,'biweekly')
	  xlbl=sprintf('%s 1992-2015 climatology of biweekly ctrls',fldNames{i});
	else 
	  xlbl=sprintf('%s 2014-2015 climatology of biweekly ctrls',fldNames{i});
	end 
	strs=struct( ...
		'xlbl', xlbl,...
		'time', 'rec',...
		'vidName',[saveDir 'xx_' fldNames{i}]);

	opts=struct('saveVideo',1);

	if ~exist(sprintf('%s.avi',strs.vidName),'file')
	  plotVideo(xx_proj,strs,opts,mygrid);
	end

	fprintf('Done with %s climatology ...\n',fldNames{i});
end



% -----------------------------------------------------------
%% Plot variance
loadDir=[subDir 'var/'];
for i=1:length(fldNames)

	xx_proj = rdmds2gcmfaces([loadDir fldNames{i}]);
	if strcmp(climOpt,'biweekly')
	  xlbl=sprintf('%s 1992-2015 variance of biweekly ctrls',fldNames{i});
	else 
	  xlbl=sprintf('%s 2014-2015 variance of biweekly ctrls',fldNames{i});
	end 
	strs=struct( ...
		'xlbl', xlbl,...
		'time', 'rec',...
		'vidName',[saveDir 'var' fldNames{i}]);

	opts=struct('saveVideo',1);

	if ~exist(sprintf('%s.avi',strs.vidName),'file')
	  plotVideo(xx_proj,strs,opts,mygrid);
	end

	fprintf('Done with %s variance ...\n',fldNames{i});
end
