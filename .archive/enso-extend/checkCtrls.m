function [] = checkCtrls()
%
% On Lonestar, want to take diagnostic output of EXF fields and 
% convert to control vector format
%
% -------------------------------------------------------------

%% Preliminaries
run('~/gcm-contrib/matlab/startup.m')

addpath(genpath('~/gcmfaces'));
addpath(genpath('~/MITprof'));
addpath(genpath('~/MITgcm_c65q/utils/matlab/'))
addpath(genpath('~/gcm-contrib/matlab/'))
addpath(genpath('~/gcm-contrib/plots/'))
addpath(genpath('~/gcm-contrib/calc/'));

runDir = '/scratch/tsmith/enso2/run.eig_c/';
grid_load(runDir,5,'compact');
gcmfaces_global;
% ---


%% Grab iterations in results dir
resultsDir = [runDir 'diags/'];
exfName = 'exf';

% exf.*
% 'EXFtaux ' 'EXFtauy ' 'EXFqnet ' 'EXFempmr' 'EXFatemp' 'EXFaqh  ' 
% 'EXFlwdn ' 'EXFswdn ' 'EXFevap ' 'EXFpreci' 'EXFroff '
% --- 

% state_3d.*
% 'UVELMASS' 'VVELMASS' 'THETA' 'SALT'
% ---

% state_2d.*
% 'ETAN'
% ---

%% Loop through state variables, compare to run_ref
resultsDir_ref = '/scratch/tsmith/enso2/run.eig_ref/diags/';
saveDir = 'verification/';
if ~exist(saveDir), mkdir(saveDir); end

fileNames = {'state_2d','state_3d'};
fldNames = {'ETAN',{'UVELMASS';'VVELMASS';'THETA';'SALT'}};

for i=1:length(fileNames)
  for j=1:size(fldNames{i},1)

	fld_c=rdmds2gcmfaces([resultsDir fileNames{i}],NaN,'rec',j);
	fld_a=rdmds2gcmfaces([resultsDir_ref fileNames{i}],NaN,'rec',j);

	fld_c=convert2gcmfaces(fld_c); fld_c(isnan(fld_c))=0;
	fld_a=convert2gcmfaces(fld_a); fld_a(isnan(fld_a))=0;

	if i>1
	  Nt = min(size(fld_c,4),size(fld_a,4));
	  N = size(fld_a,1)*size(fld_a,2)*size(fld_a,3);
       	  for n=1:Nt
       	    err(n) = sqrt(1/N*squeeze(nansum(nansum(nansum((fld_c(:,:,:,n)-fld_a(:,:,:,n)).^2,1),2),3)));
       	  end
	else 
	  Nt = min(size(fld_c,3),size(fld_a,3));
	  N = size(fld_a,1)*size(fld_a,2);
       	  for n=1:Nt
       	    err(n) = sqrt(1/N*squeeze(nansum(nansum(nansum((fld_c(:,:,n)-fld_a(:,:,n)).^2,1),2),3)));
       	  end
	end 

	figureW;
	plot(1:Nt,err)
	xlabel('Months')
	ylabel('Error x_c-x_a')
        grid on
        set(gcf,'paperorientation','landscape')
        set(gcf,'paperunits','normalized')
        set(gcf,'paperposition',[0 0 1 1])
	if iscell(fldNames{i})
	  saveas(gcf,[saveDir fldNames{i}{j}],'pdf')
	else 
	  saveas(gcf,[saveDir fldNames{i}],'pdf')
	end
	
  end
end

end
