function [ ] = grabProjCtrls( climOpt, mygrid, beg, stp )
% Now that projected ctrls are generated, want to grab subset 
% that runs from 
%	iter0=201612
%	nTimeSteps=17543
% ---------------------------------------------------------------

%% 1. Set directories and grid
if ~exist('climOpt','var'), climOpt='biweekly'; end;
if ~exist('mygrid','var'), establish_mygrid; end;
if ~exist('beg','var'), beg=628; end;
if ~exist('stp','var'), stp=653; end;

mainSaveDir = 'projected-ctrls/';
if strcmp(climOpt,'biweekly')
  subDir = [mainSaveDir 'full/'];
elseif strcmp(climOpt,'2yr')
  subDir = [mainSaveDir '2yr/'];
else
  fprintf('Climatology option not recognized ...\n');
  return;
end

loadDir = [subDir 'ctrls/'];
saveDir = [subDir 'proj/'];

if ~exist(loadDir,'dir'), fprintf('Can''t find controls, exiting ...\n'); return; end;
if ~exist(saveDir,'dir'), mkdir(saveDir); end;

fldNames = {'tauu','tauv','atemp','lwdown','swdown','aqh','precip'};
% ---------------------------------------------------------------

%% 2. Read in full controls
for i = 1:length(fldNames)

	xx_tmp=rdmds2gcmfaces([loadDir 'xx_' fldNames{i}],59);

	% Should start at record num 600, lets see what happens
	xx_tmp=xx_tmp(:,:,beg:stp);
	xx_tmp=convert2gcmfaces(xx_tmp); xx_tmp(isnan(xx_tmp))=0;

	write2file([saveDir 'xx_' fldNames{i} '.0000000059.data'],xx_tmp);
	write2meta([saveDir 'xx_' fldNames{i} '.0000000059.data'],size(xx_tmp));
	fprintf('Done with %s ...\n',fldNames{i});

end

