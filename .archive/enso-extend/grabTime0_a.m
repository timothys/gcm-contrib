function [] = grabTime0_a()
%
% On Lonestar, want to take diagnostic output of EXF fields and 
% convert to control vector format
%
% -------------------------------------------------------------

%% Preliminaries
run('~/gcm-contrib/matlab/startup.m')

addpath(genpath('~/gcmfaces'));
addpath(genpath('~/MITprof'));
addpath(genpath('~/MITgcm_c65q/utils/matlab/'))
addpath(genpath('~/gcm-contrib/matlab/'))

runDir = '/scratch/tsmith/enso2/run.eig_a0/';
grid_load(runDir,5,'compact');
gcmfaces_global;
% ---


%% Grab iterations in results dir
% resultsDir = '/work/03754/tsmith/lonestar/enso/run.eig_a/diags/';
resultsDir = [runDir 'diags/'];
exfName = 'exf';


% 'EXFtaux ' 'EXFtauy ' 'EXFqnet ' 'EXFempmr' 'EXFatemp' 'EXFaqh  ' 
% 'EXFlwdn ' 'EXFswdn ' 'EXFevap ' 'EXFpreci' 'EXFroff '
% --- 

%% Loop through diagnostic variables, then grab and save as control vectors
% saveDir = '/work/03754/tsmith/ecco-data/control-vectors-2015';

loadDir = 'ctrl-vectors-2015/';
saveDir = 'ctrl-vectors-2015-a0/';
if ~exist(saveDir), mkdir(saveDir); end

prefix = [saveDir 'xx_'];
suffix = '.0000000059.data';
suff2 = '.0000000059';
fldNames = {'tau','atemp','aqh','lwdown','swdown','precip'};
recNum = [1 5 6 7 8 10];

alpha = 0.5;

for i = 1:length(fldNames)

	%% Special case for wind stress, need to flip UX,VY
	if ~isempty(strfind(fldNames{i},'tau'))

		% Grab exf snapshot, fully packaged
		tmpu = rdmds2gcmfaces([resultsDir exfName],168,'rec',recNum(i));
		tmpv = rdmds2gcmfaces([resultsDir exfName],168,'rec',recNum(i)+1);

		Nt = size(tmpu.f1,3);
		for n=1:Nt
			[tmpu(:,:,n),tmpv(:,:,n)] = calc_UEVNfromUXVY(tmpu(:,:,n),tmpv(:,:,n));
		end
		
		% Grab control vector at ts 1 (not 0)
		dx1_u = rdmds2gcmfaces([loadDir 'xx_tauu' suff2]);
		dx1_v = rdmds2gcmfaces([loadDir 'xx_tauv' suff2]);

	
		% Subtract out 
		tmpu = tmpu - (1-alpha)*dx1_u(:,:,2);
		tmpv = tmpv - (1-alpha)*dx1_v(:,:,2);

		tmpu=convert2gcmfaces(tmpu); tmpu(isnan(tmpu))=0;
		tmpv=convert2gcmfaces(tmpv); tmpv(isnan(tmpv))=0;

		tmpu=cat(3,tmpu,0*repmat(tmpu,[1,1,2]));
		tmpv=cat(3,tmpv,0*repmat(tmpu,[1,1,2]));
		
		write2file([prefix 'tauu' suffix],tmpu);
		write2meta([prefix 'tauu' suffix],size(tmpu));

		fprintf('File %s written ... \n',[prefix 'tauu' suffix]);

		write2file([prefix 'tauv' suffix],tmpv);
		write2meta([prefix 'tauv' suffix],size(tmpv));
		fprintf('File %s written ... \n',[prefix 'tauv' suffix]);

		clear tmpu tmpv dx1_u dx1_v;
	
	%% Otherwise super straight forward
	else

		% Grab exf snapshot fully packaged
		xx_tmp = rdmds2gcmfaces([resultsDir exfName],168,'rec',recNum(i));	

		% Grab control vector at ts 1
		dx1 = rdmds2gcmfaces([loadDir 'xx_' fldNames{i} suff2]);

		% Subtract out
		xx_tmp = xx_tmp - (1-alpha)*dx1(:,:,2);

		xx_tmp=convert2gcmfaces(xx_tmp);
		xx_tmp(isnan(xx_tmp))=0;

		xx_tmp=cat(3,xx_tmp,0*repmat(xx_tmp,[1,1,2]));

		write2file([prefix fldNames{i} suffix],xx_tmp);
		write2meta([prefix fldNames{i} suffix],size(xx_tmp));
		fprintf('File %s written ... \n',[prefix fldNames{i} suffix]);
	end
end
end
