function [mytest]=testreport_ctrls(mytest,nameRef,listTests,dirRun,useGcmfaces);

% Modified version of the testreport_ecco file to verify results between: 
%  -> reference r3 run from ph setup
%  -> run with ctrls separated from raw forcing (../run.eig_c)
% 
% Testing: 
%  -> SSH (without sea ice load)
%  -> T, S
%  -> UVELMASS, VVELMASS (2008-2010 as in ECCOv4r2)
% -------------------------------------------------------------------------

%% Preliminaries
run('~/gcm-contrib/matlab/startup.m')

addpath(genpath('~/gcmfaces'));
addpath(genpath('~/MITprof'));
addpath(genpath('~/MITgcm_c65q/utils/matlab/'))
addpath(genpath('~/gcm-contrib/matlab/'))
addpath(genpath('~/gcm-contrib/plots/'))
addpath(genpath('~/gcm-contrib/calc/'));

runDir = '/scratch/tsmith/enso2/run.eig_c_smooth/';
grid_load(runDir,5,'compact');
gcmfaces_global;
% ---

%% Results dir 
resultsDir_ctrl = [runDir 'diags/'];
resultsDir_ref = '/scratch/tsmith/enso2/run.eig_ref/diags/';

%% Modify this for N months
[iters_ctrl]=grabAllIters( resultsDir_ctrl, 'state_2d' );
[iters_ref]=grabAllIters( resultsDir_ref, 'state_2d' );

Nt = min(length(iters_ctrl),length(iters_ref));
iters=iters_ctrl(1:Nt);

% -------------------------------------------------------------------------

fprintf('-> monthly free surface height test started \n');
%
rac=mygrid.RAC.*(mygrid.hFacC(:,:,1)>0);
racsum=nansum(rac(:));
%
mH_ctrl=NaN*zeros(1,Nt);
mH_ref=NaN*zeros(1,Nt);

ETAN_ctrl = rdmds2gcmfaces([resultsDir_ctrl 'state_2d'],iters(1:Nt),'rec',1);
ETAN_ref = rdmds2gcmfaces([resultsDir_ref 'state_2d'],iters(1:Nt),'rec',1);

for tt=1:Nt;
    mH_ctrl(tt)=nansum(rac.*ETAN_ctrl(:,:,tt))/racsum;
    mH_ref(tt)=nansum(rac.*ETAN_ref(:,:,tt))/racsum;
end;

clear ETAN_ctrl ETAN_ref;

%
fprintf('-> monthly free surface height test completed \n\n');
mH_test=testreport_metric(mH_ctrl,mH_ref);
fprintf('\n mH agreement: %0.5f \n\n',mH_test);

% -------------------------------------------------------------------------

fprintf('-> monthly T test started \n');
%
Nk=length(mygrid.DRF);
vol=repmat(mygrid.RAC,[1 1 Nk]).*mygrid.hFacC;
for kk=1:Nk; vol(:,:,kk)=mygrid.DRF(kk)*vol(:,:,kk); end;
volsum=nansum(vol(:));
%
mT_ctrl=NaN*zeros(1,Nt);
mT_ref=NaN*zeros(1,Nt);

theta_ctrl = rdmds2gcmfaces([resultsDir_ctrl 'state_3d'],iters(1:Nt),'rec',3);
theta_ref = rdmds2gcmfaces([resultsDir_ref 'state_3d'],iters(1:Nt),'rec',3);

for tt=1:Nt;

    mT_ctrl(tt)=nansum(vol.*theta_ctrl(:,:,:,tt))/volsum;
    mT_ref(tt)=nansum(vol.*theta_ref(:,:,:,tt))/volsum;

end;
%

clear theta_ctrl theta_ref;

fprintf('-> monthly T test completed \n\n');

% -------------------------------------------------------------------------

fprintf('-> monthly S test started \n');
mS_ctrl=NaN*zeros(1,Nt);
mS_ref=NaN*zeros(1,Nt);

salt_ctrl = rdmds2gcmfaces([resultsDir_ctrl 'state_3d'],iters(1:Nt),'rec',4);
salt_ref = rdmds2gcmfaces([resultsDir_ref 'state_3d'],iters(1:Nt),'rec',4);

for tt=1:Nt;

    mS_ctrl(tt)=nansum(vol.*salt_ctrl(:,:,:,tt))/volsum;
    mS_ref(tt)=nansum(vol.*salt_ref(:,:,:,tt))/volsum;

end;
%

clear salt_ctrl salt_ref;

fprintf('-> monthly S test completed \n\n');

mT_test=testreport_metric(mT_ctrl,mT_ref);
%
mS_test=testreport_metric(mS_ctrl,mS_ref);
fprintf('\n mT agreement: %0.5f \n\n',mT_test);
fprintf('\n mS agreement: %0.5f \n\n',mS_test);
% -------------------------------------------------------------------------

fprintf('-> tV test started \n');
%
UVELMASS_ctrl=0*mygrid.hFacC;
UVELMASS_ref=0*mygrid.hFacC;
VVELMASS_ctrl=0*mygrid.hFacC;
VVELMASS_ref=0*mygrid.hFacC;

if Nt<193
    fprintf('Skipping tV test, not enough records finished ...\n');
elseif Nt>=193 && Nt<228
    TT=[193:Nt]; 
else
    TT=[193:228];
end

if exist('TT','var')
nt = length(TT);
%% Do U and V separate for memory concerns
u_ctrl = rdmds2gcmfaces([resultsDir_ctrl 'state_3d'],iters(TT),'rec',1);
u_ref = rdmds2gcmfaces([resultsDir_ref 'state_3d'],iters(TT),'rec',1);

for tt=TT;
    UVELMASS_ctrl=UVELMASS_ctrl + 1/nt*u_ctrl(:,:,:,tt);
    UVELMASS_ref=UVELMASS_ref + 1/nt*u_ref(:,:,:,tt);
end;

%
clear u_ctrl u_ref;
%

v_ctrl = rdmds2gcmfaces([resultsDir_ctrl 'state_3d'],iters(TT),'rec',2);
v_ref = rdmds2gcmfaces([resultsDir_ref 'state_3d'],iters(TT),'rec',2);

for tt=TT;
    VVELMASS_ctrl=VVELMASS_ctrl + 1/nt*v_ctrl(:,:,tt);
    VVELMASS_ref=VVELMASS_ref + 1/nt*v_ref(:,:,tt);
end;
%
clear v_ctrl v_ref;
%

U_ctrl=UVELMASS_ctrl.*mygrid.mskW; V_ctrl=VVELMASS_ctrl.*mygrid.mskS;
tV_ctrl=1e-6*calc_MeridionalTransport(UVELMASS_ctrl,VVELMASS_ctrl,1);
%
U_ref=UVELMASS_ref.*mygrid.mskW; V_ref=VVELMASS_ref.*mygrid.mskS;
tV_ref=1e-6*calc_MeridionalTransport(UVELMASS_ref,VVELMASS_ref,1);
%
fprintf('-> tV test completed \n\n');
else
    tV_ctrl=NaN*ones(1,Nt);
    tV_ref=NaN*ones(1,Nt);
end

% -------------------------------------------------------------------------

mH_test=testreport_metric(mH_ctrl,mH_ref);
%
mT_test=testreport_metric(mT_ctrl,mT_ref);
%
mS_test=testreport_metric(mS_ctrl,mS_ref);
%
tV_test=testreport_metric(tV_ctrl,tV_ref);

% -------------------------------------------------------------------------

fprintf('\n mH agreement: %0.5f \n\n',mH_test);
fprintf('\n mT agreement: %0.5f \n\n',mT_test);
fprintf('\n mS agreement: %0.5f \n\n',mS_test);
fprintf('\n tV agreement: %0.5f \n\n',tV_test);

fprintf('\n\n=> testreport_ctrls completed\n\n'); 

keyboard

end

function [d]=testreport_metric(cur,ref,nrm);

if isempty(whos('nrm'))&(length(ref(:))==1);
  nrm=abs(ref);
elseif isempty(whos('nrm'));
  nrm=nanstd(ref(:));
end;
d=sqrt(nanmean((cur(:)-ref(:)).^2))/nrm;

end

