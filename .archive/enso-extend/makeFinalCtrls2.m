function [] = makeFinalCtrls2()
%
% On Lonestar, want to take diagnostic output of EXF fields and 
% convert to control vector format
%
% -------------------------------------------------------------

%% Preliminaries
run('~/gcm-contrib/matlab/startup.m')

addpath(genpath('~/gcmfaces'));
addpath(genpath('~/MITprof'));
addpath(genpath('~/MITgcm_c65q/utils/matlab/'))
addpath(genpath('~/gcm-contrib/matlab/'))
addpath(genpath('~/gcm-contrib/plots/'))
addpath(genpath('~/gcm-contrib/calc/'));

runDir = '/scratch/tsmith/enso2/run.eig_a_inv/';
grid_load(runDir,5,'compact');
gcmfaces_global;
% ---


%% Grab iterations in results dir
resultsDir_full = [runDir 'diags/'];
resultsDir_a0 = '/scratch/tsmith/enso2/run.eig_a0_inv/diags/';
resultsDir_raw = '/scratch/tsmith/enso2/run.eig_raw/diags/';
resultsDir_raw_a0 = '/scratch/tsmith/enso2/run.eig_raw_a0/diags/';
exfName = 'exf';

% 'EXFtaux ' 'EXFtauy ' 'EXFqnet ' 'EXFempmr' 'EXFatemp' 'EXFaqh  ' 
% 'EXFlwdn ' 'EXFswdn ' 'EXFevap ' 'EXFpreci' 'EXFroff '
% --- 

%% Loop through diagnostic variables, then grab and save as control vectors
mainSaveDir = 'ctrl-vectors-2015-final-inv/';
if ~exist(mainSaveDir), mkdir(mainSaveDir); end


suffix = '.0000000059.data';
fldNames = {'tau','atemp','aqh','lwdown','swdown','precip'};
recNum = [1 5 6 7 8 10];

%% First loop through and make controls as xx_<> files
for j=1:2
  if j==1
    resultsDir = resultsDir_full;
    saveDir = [mainSaveDir 'full/'];
    if ~exist(saveDir), mkdir(saveDir); end
  elseif j==2
    resultsDir = resultsDir_a0;
    saveDir = [mainSaveDir 'a0/'];
    if ~exist(saveDir), mkdir(saveDir); end
  elseif j==3
    resultsDir = resultsDir_raw;
    saveDir = [mainSaveDir 'raw/'];
    if ~exist(saveDir), mkdir(saveDir); end
  else
    resultsDir = resultsDir_raw_a0;
    saveDir = [mainSaveDir 'raw-a0/'];
    if ~exist(saveDir), mkdir(saveDir); end
  end

  prefix = [saveDir 'xx_'];

  for i = 1:length(fldNames)
  
  	%% Special case for wind stress, need to flip UX,VY
  	if ~isempty(strfind(fldNames{i},'tau'))
  		tmpu = rdmds2gcmfaces([resultsDir exfName],NaN,'rec',recNum(i));
  		tmpv = rdmds2gcmfaces([resultsDir exfName],NaN,'rec',recNum(i)+1);
  
  		Nt = size(tmpu.f1,3);
  		for n=1:Nt
  			[tmpu(:,:,n),tmpv(:,:,n)] = calc_UEVNfromUXVY(tmpu(:,:,n),tmpv(:,:,n));
  		end
  
  		tmpu=convert2gcmfaces(tmpu); tmpu(isnan(tmpu))=0;
  		tmpv=convert2gcmfaces(tmpv); tmpv(isnan(tmpv))=0;
  
  		write2file([prefix 'tauu' suffix],tmpu);
  		write2meta([prefix 'tauu' suffix],size(tmpu));
  		fprintf('File %s written ... \n',[prefix 'tauu' suffix]);
  
  		write2file([prefix 'tauv' suffix],tmpv);
  		write2meta([prefix 'tauv' suffix],size(tmpv));
  		fprintf('File %s written ... \n',[prefix 'tauv' suffix]);
  
  		clear tmpu tmpv;
  	
  	%% Otherwise super straight forward
  	else
  
  		xx_tmp = rdmds2gcmfaces([resultsDir exfName],NaN,'rec',recNum(i));	
  
  		xx_tmp=convert2gcmfaces(xx_tmp);
  		xx_tmp(isnan(xx_tmp))=0;
  
  		write2file([prefix fldNames{i} suffix],xx_tmp);
  		write2meta([prefix fldNames{i} suffix],size(xx_tmp));
  		fprintf('File %s written ... \n',[prefix fldNames{i} suffix]);

		clear xx_tmp
  	end


  end % for i=1:length(fldNames)
	fprintf('\n*********\n Done with %s files \n*********\n',saveDir);
end % for j=1:4

keyboard

%% Now loop through and make final controls
%% For all but first record
saveDir = [mainSaveDir 'final/'];
if ~exist(saveDir), mkdir(saveDir); end
prefix = [saveDir 'xx_'];
fldNames = {'tauu','tauv','atemp','aqh','lwdown','swdown','precip'};
 
for i=1:length(fldNames)

	xx_full=rdmds2gcmfaces([mainSaveDir 'full/xx_' fldNames{i}],59);
	xx_raw=rdmds2gcmfaces([mainSaveDir 'raw/xx_' fldNames{i}],59);
	xx_a0=rdmds2gcmfaces([mainSaveDir 'a0/xx_' fldNames{i}],59);
	xx_raw_a0=rdmds2gcmfaces([mainSaveDir 'raw-a0/xx_' fldNames{i}],59);
	xx_r3=rdmds2gcmfaces(['/scratch/tsmith/data/input_v4_rls2.025l.iter59/ADXXfiles_it59/xx_' fldNames{i}],59);
	xx_r3=xx_r3(:,:,628);

	% For all but rec0
	xx_final = xx_full-xx_raw;

	% Grab initial rec0
	alpha=0.5;
	xx_0 = (xx_a0 - xx_raw_a0 - (1-alpha)*xx_final(:,:,1))./alpha;

	xx_final=convert2gcmfaces(xx_final);
	xx_final(isnan(xx_final))=0;
	xx_0=convert2gcmfaces(xx_0);
	xx_0(isnan(xx_0))=0;
	xx_r3=convert2gcmfaces(xx_r3);
	xx_r3(isnan(xx_r3))=0;

	% Stick it together
	xx_final=cat(3,xx_0,xx_final);
	xx_final=cat(3,xx_final,xx_r3);

	write2file([prefix fldNames{i} suffix],xx_final);
	write2meta([prefix fldNames{i} suffix],size(xx_final));
	fprintf('File %s written ... \n',[prefix fldNames{i} suffix]);
end
end
