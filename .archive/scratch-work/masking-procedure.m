
%% Set up dirs
establish_mygrid;

%% Set up mask for SAMOC
read_dir = '../../release2/nctiles_monthly/';

Ny = length(mygrid.LATS_MASKS);
Nr = length(mygrid.DRF);
Nt = 12;
trVolZon = zeros(Nr,Nt); % Zonally integrated volume transport

%% Loop through each month of 20 yr period
for n = 1:Nt
    uvel = read_nctiles([read_dir 'UVELMASS/UVELMASS'], 'UVELMASS', n);
    vvel = read_nctiles([read_dir 'VVELMASS/VVELMASS'], 'VVELMASS', n);
    
%     facW = mygrid.mskW; facS = mygrid.mskS;
    uvel = uvel.*mygrid.mskW; vvel = vvel.*mygrid.mskS; 
        
    amocBasin = v4_basin('atlExt');
    amocBasin = mk3D(amocBasin,uvel);
    
    uvel = uvel.*amocBasin; vvel = vvel.*amocBasin; 
%     mskS = mygrid.LATS_MASKS(ind).mskSedge.*amocBasin;
%     mskW = mygrid.LATS_MASKS(ind).mskWedge.*amocBasin;
    
%     facS = mk3D(mskS,uvel); facW = mk3D(mskW,uvel);
    
    
    % Grab vals for volume transport
%     uvel(isnan(uvel))=0; vvel(isnan(vvel))=0;
    
    dxg = mk3D(mygrid.DXG,uvel);
    dyg = mk3D(mygrid.DYG,uvel);
    drf = mk3D(mygrid.DRF,uvel);
    
    % Make volume transport masks
    facS = dxg; 
    facS(:) = 1;
    facW = facS;
    
    facS = dxg.*drf.*facS; facW = dyg.*drf.*facW;
    
    uvel=uvel.*facW; vvel=vvel.*facS;
    
    ind = find(mygrid.LATS==25);
    mskS = mygrid.LATS_MASKS(ind).mskSedge; mskS=mk3D(mskS,uvel);
    mskW = mygrid.LATS_MASKS(ind).mskWedge; mskW=mk3D(mskW,uvel);
    
    vecW = mskW.*uvel; vecS = mskS.*vvel; 
    
    trVolZon(:,n) = squeeze(nansum(nansum(convert2gcmfaces(vecW + vecS),1),2));
    fprintf('step; %d / %d\n',n,Nt)
end

clear uvel;
clear vvel;

