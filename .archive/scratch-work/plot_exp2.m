function [bt] = plot_exp2()


wkdir='/Users/tim/MITgcm/verification/exp2/input/';
% cd(wkdir)

u = rdmds(strcat(wkdir,'U'),NaN);
v = rdmds(strcat(wkdir,'V'),NaN);
x = rdmds(strcat(wkdir,'XC'));
y = rdmds(strcat(wkdir,'YC'));
T = rdmds(strcat(wkdir,'T'),NaN);
S = rdmds(strcat(wkdir,'S'),NaN);
H = rdmds(strcat(wkdir,'Depth'));
eta = rdmds(strcat(wkdir,'eta'),NaN);
ph = rdmds(strcat(wkdir,'PH'),NaN);
phl = rdmds(strcat(wkdir,'PHL'),NaN);

bt = struct('x',x,'y',y,'u',u,'v',v,'eta',eta,'H',H,...
            'ph',ph,'phl',phl,'T',T,'S',S);

        
for i=1:3        
figure
quiver(x,y,u(:,:,2,i),v(:,:,2,i),3)
%     xlim([0,1200]),ylim([0,1200])
    title('Horizontal velocity (m/s)')

figure 
contourf(x,y,eta(:,:,i));
%     xlim([0,1200]),ylim([0,1200])
    cb=colorbar;
    title('SSH \eta')
    
figure
contourf(x,y,T(:,:,2,i));
    cb=colorbar;
    ylabel(cb,'T')
    title('Temperature')

figure
contourf(x,y,S(:,:,2,i));
    caxis([34 36])
    cb=colorbar; 
    title('Salinity')
    
figure
contourf(x,y,ph(:,:,2,i));
    title('\phi_{hyd}')
    colorbar;
% 
% figure, 
% contourf(bt.x/1000,bt.y/1000,bt.phl(:,:,i));
%     title('\phi_{hyd,low}')
%     colorbar;
    
keyboard
end
end