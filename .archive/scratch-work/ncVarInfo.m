function [names,xtypes,dimids,atts] = t_ncVarInfo(fileName)

ncid = netcdf.open(fileName);
[ndim,nvar] = netcdf.inq(ncid);

names = cell(ndim,1);
xtypes = cell(ndim,1);
dimids = cell(ndim,1);
atts = cell(ndim,1);
ndim
for i=1:nvar
    [names{i},xtypes{i},dimids{i},atts{i}] = netcdf.inqVar(ncid,i-1);
    
end

end