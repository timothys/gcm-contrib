
C:----------------------------------------------
C RESET LOCAL ADJOINT VARIABLES NO ALLOC
C----------------------------------------------
      dummyrl_ad = 0.d0
      do ip5 = 1, nsy
        do ip4 = 1, nsx
          do ip3 = 1, nr
            do ip2 = 1-oly, sny+oly
              do ip1 = 1-olx, snx+olx
                mybar_ad(ip1,ip2,ip3,ip4,ip5) = 0.d0
              end do
            end do
          end do
        end do
      end do
      mytempmax_ad = 0.d0
      do ip1 = 1, nr
        tmpcumsumglo_ad(ip1) = 0.d0
      end do
      do ip3 = 1, nsy
        do ip2 = 1, nsx
          do ip1 = 1, nr
            tmpcumsumtile_ad(ip1,ip2,ip3) = 0.d0
          end do
        end do
      end do

      jtlo = mybylo(mythid)
      jthi = mybyhi(mythid)
      itlo = mybxlo(mythid)
      ithi = mybxhi(mythid)
      imin = 1
      imax = snx
      jmin = 1
      jmax = sny
      do kgen = ngencost, 1, -1
        if (gencost_name(kgen)(1:4) .eq. 'amoc' .and. using_gencost(
     $kgen)) then
          doglobalread =  .false. 
          ladinit =  .false. 
          dummyrl = gencost_dummy(kgen)
          nrecloc = gencost_nrec(kgen)
          il = ilnblnk(gencost_barfile(kgen))
          write(unit=mybarfile,fmt='(2a,i10.10)') gencost_barfile(kgen)(
     $1:il),'.',eccoiter
          do irec = nrecloc, 1, -1
C$taf INCOMPLETE tmpcumsumtile
            call active_read_xyz( mybarfile,mybar,irec,doglobalread,
     $ladinit,eccoiter,mythid,dummyrl )
            il = ilnblnk(gencost_errfile(kgen))
            write(unit=fname0(1:128),fmt='(2A)') gencost_errfile(kgen)(
     $1:il),'T'
            inquire(file=fname0(1:il+1),exist=exst)
            if (( .not. exst) .or. gencost_errfile(kgen) .eq. ' ') then
              gencost_msktemporal = nrecloc
              gencost_msktemporal = 1.d0/gencost_msktemporal
            else
              iounit = 0
              call mds_readvec_loc( fname0,cost_iprec,iounit,'RL',1,
     $gencost_msktemporal,dummyrs,0,0,irec,mythid )
            endif
C$taf INCOMPLETE tmpcumsumtile
            if (gencost_msktemporal .ne. 0) then
C$taf INCOMPLETE tmpcumsumtile
              do bj = jtlo, jthi
                do bi = itlo, ithi
                  do k = 1, nr
                    tmpcumsumtile(k,bi,bj) = 0.d0
                  end do
                end do
              end do
              do bj = jtlo, jthi
                do bi = itlo, ithi
                  do j = jmin, jmax
                    do i = imin, imax
      tmpcumsumtile(nr,bi,bj) = tmpcumsumtile(nr,bi,bj)-mybar(i,j,nr,bi,
     $bj)*gencost_msktemporal
                    end do
                  end do
                end do
              end do
              do bj = jtlo, jthi
                do bi = itlo, ithi
                  do k = nr-1, 1, -1
                    do j = jmin, jmax
      do i = imin, imax
        tmpcumsumtile(k,bi,bj) = tmpcumsumtile(k,bi,bj)-mybar(i,j,k,bi,
     $bj)*gencost_msktemporal
      end do
                    end do
                    tmpcumsumtile(k,bi,bj) = tmpcumsumtile(k,bi,bj)+
     $tmpcumsumtile(k+1,bi,bj)
                  end do
                end do
              end do
              do k = nr, 1, -1
                call global_sum_tile_rl( tmpcumsumtile(k,1,1),
     $tmpcumsumglo(k),mythid )
              end do
              mytempmax = tmpcumsumglo(1)
              if (myprocid .eq. 0) then
                mytempmax_ad = mytempmax_ad+objf_gencost_ad(1,1,kgen)
              endif
              do k = nr, 2, -1
                mytempmax = tmpcumsumglo(1)
                do k1 = 2, k-1
                  if (mytempmax .lt. tmpcumsumglo(k1)) then
                    mytempmax = tmpcumsumglo(k1)
                  endif
                end do
                    end do
                    tmpcumsumtile(k,bi,bj) = tmpcumsumtile(k,bi,bj)+
     $tmpcumsumtile(k+1,bi,bj)
                  end do
                end do
              end do
              do k = nr, 1, -1
                call global_sum_tile_rl( tmpcumsumtile(k,1,1),
     $tmpcumsumglo(k),mythid )
              end do
              mytempmax = tmpcumsumglo(1)
              if (myprocid .eq. 0) then
                mytempmax_ad = mytempmax_ad+objf_gencost_ad(1,1,kgen)
              endif
              do k = nr, 2, -1
                mytempmax = tmpcumsumglo(1)
                do k1 = 2, k-1
                  if (mytempmax .lt. tmpcumsumglo(k1)) then
                    mytempmax = tmpcumsumglo(k1)
                  endif
                end do
                if (mytempmax .lt. tmpcumsumglo(k)) then
                  tmpcumsumglo_ad(k) = tmpcumsumglo_ad(k)+mytempmax_ad
                  mytempmax_ad = 0.d0
                endif
              end do
              tmpcumsumglo_ad(1) = tmpcumsumglo_ad(1)+mytempmax_ad
              mytempmax_ad = 0.d0
              do k = 1, nr
                call global_adsum_tile_rl( tmpcumsumtile_ad(k,1,1),
     $tmpcumsumglo_ad(k),mythid )
              end do
              do bj = jthi, jtlo, -1
                do bi = ithi, itlo, -1
                  do k = 1, nr-1
                    tmpcumsumtile_ad(k+1,bi,bj) = tmpcumsumtile_ad(k+1,
     $bi,bj)+tmpcumsumtile_ad(k,bi,bj)
                    do j = jmin, jmax
      do i = imin, imax
        mybar_ad(i,j,k,bi,bj) = mybar_ad(i,j,k,bi,bj)-tmpcumsumtile_ad(
     $k,bi,bj)*gencost_msktemporal
      end do
                    end do
                  end do
                end do
              end do
              do bj = jthi, jtlo, -1
                do bi = ithi, itlo, -1
                  do j = jmin, jmax
                    do i = imin, imax
      mybar_ad(i,j,nr,bi,bj) = mybar_ad(i,j,nr,bi,bj)-tmpcumsumtile_ad(
     $nr,bi,bj)*gencost_msktemporal
                    end do
                  end do
                end do
              end do
              do bj = jtlo, jthi
                do bi = itlo, ithi
                  do k = 1, nr
                    tmpcumsumtile_ad(k,bi,bj) = 0.d0
                  end do
                end do
              end do
            endif
            call adactive_read_xyz( mybarfile,mybar_ad,irec,
     $doglobalread,ladinit,eccoiter,mythid,dummyrl,dummyrl_ad )
          end do
          gencost_dummy_ad(kgen) = gencost_dummy_ad(kgen)+dummyrl_ad
          dummyrl_ad = 0.d0
        endif
      end do

      end
