function [bt] = quickplot()


wkdir='/Users/tim/MITgcm/verification/tutorial_barotropic_gyre/build/';
% cd(wkdir)

u = rdmds(strcat(wkdir,'U'),NaN);
v = rdmds(strcat(wkdir,'V'),NaN);
x = rdmds(strcat(wkdir,'XC'));
y = rdmds(strcat(wkdir,'YC'));
T = rdmds(strcat(wkdir,'T'),NaN);
S = rdmds(strcat(wkdir,'S'),NaN);
H = rdmds(strcat(wkdir,'Depth'));
eta = rdmds(strcat(wkdir,'eta'),NaN);
ph = rdmds(strcat(wkdir,'PH'),NaN);
phl = rdmds(strcat(wkdir,'PHL'),NaN);

bt = struct('x',x,'y',y,'u',u,'v',v,'eta',eta,'H',H,...
            'ph',ph,'phl',phl,'T',T,'S',S);

figure,
quiver(bt.x/1000,bt.y/1000,bt.u(:,:,2),bt.v(:,:,2))
    xlim([0,1200]),ylim([0,1200])
    title('Horizontal velocity (m/s)')

figure, 
contourf(bt.x/1000,bt.y/1000,bt.eta(:,:,2)./bt.H);
    xlim([0,1200]),ylim([0,1200])
    cb=colorbar;
    ylabel(cb,'\eta/H')
    title('Relative SSH \eta/H')
    
figure, 
contourf(bt.x/1000,bt.y/1000,bt.ph);
    title('\phi_{hyd}')
    colorbar;

figure, 
contourf(bt.x/1000,bt.y/1000,bt.phl);
    title('\phi_{hyd,low}')
    colorbar;
    

end