function [S,phi,phiLow] = quickplot_netcdf()

wkdir = '/Users/tim/MITgcm/verification/tutorial_barotropic_gyre/build/mnc_test_0001';

% S = rdmnc('state.*','U','V','W','X','Y','Z','iter');
S = struct([]);
S(1).name = 'iter';
S(2).name = 'X';
% S(3).name = 'Xp1';
S(3).name = 'Y';
% S(5).name = 'Yp1';
S(4).name = 'U';
S(5).name = 'V';
S(6).name = 'T';
S(7).name = 'Temp';
S(8).name = 'S';

fpat = strcat(wkdir,'/state.0000000000.t%03d.nc')
[nt,nf] = mnc_assembly(fpat,S);

keyboard

figure,quiver(S.X/1000,S.Y/1000,S.U(2:end,:,1,2),S.V(:,2:end,1,2))
    xlabel('km'),xlim([0 1200])
    ylabel('km'),ylim([0 1200])
    title('t001')

phi = rdmnc('phiHyd.*','X','Y','Z','T','iter','phiHyd');

phiLow = rdmnc('phiHydLow.*','X','Y','T','iter','phiHydLow');

% 
% S = rdmnc('state.0000000000.t002.nc','U','V','W','X','Y','Z','iter');
% figure,quiver(S.X/1000,S.Y/1000,S.U(2:end,:,1,2),S.V(:,2:end,1,2))
%     xlabel('km')
%     ylabel('km')
%     title('t002')
% 
% S = rdmnc('state.0000000000.t003.nc','U','V','W','X','Y','Z','iter');
% figure,quiver(S.X/1000,S.Y/1000,S.U(2:end,:,1,2),S.V(:,2:end,1,2))
%     xlabel('km')
%     ylabel('km')
%     title('t003')
% 
% S = rdmnc('state.0000000000.t004.nc','U','V','W','X','Y','Z','iter');
% figure,quiver(S.X/1000,S.Y/1000,S.U(2:end,:,1,2),S.V(:,2:end,1,2))
%     xlabel('km')
%     ylabel('km')
%     title('t004')
    
end