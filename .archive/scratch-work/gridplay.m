% function [] = gridplay()

% addpath(gcmfaces...)
if ~exist('mygrid','var')
runDir='~/gcmpack/GRID/';%'/net/confucius/raid13/home/timothys/MITgcm/verification/global_oce_llc90/run';
gcmfaces_init;
grid_load(runDir,5, 'compact');
end

% --- Just showing how plotting and structures work
% pcolor(mygrid.Depth.f1)
% shading interp
% qwckplot(mygrid.XC)

natlones = mygrid.YC>0&mygrid.XC<0&mygrid.XC>-90&mygrid.Depth~=0;
natlones(~natlones==1) = 0;

qwckplot(natlones)
% qwckplot(mygrid.Depth)

write2file('natl_1deg_obs.data',convert2gcmfaces(natlones),32);

%keyboard
% end