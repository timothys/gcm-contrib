
C==============================================
C declare arguments
C==============================================
      integer mythid

C==============================================
C declare local variables
C==============================================
      integer bi
      integer bj
      logical doglobalread
      double precision dummyrs(1)
      logical exst
      double precision gencost_msktemporal
      integer i
      integer il
      integer iounit
      integer ip1
      integer ip2
      integer ip3
      integer ip4
      integer irec
      integer j
      integer kgen
      logical ladinit
      double precision mybar_ad(1-olx:snx+olx,1-oly:sny+oly,nsx,nsy)
      double precision mydummy
      double precision mydummy_ad
      character*(80) myfname
      double precision mysumgloh_ad
      integer nrecloc
      character*(128) tempfile
      double precision tmpsumgloh_ad
      double precision tmpsumtileh_ad(nsx,nsy)

C==============================================
C declare external procedures and functions
C==============================================
      integer ilnblnk
      external ilnblnk

C----------------------------------------------
C RESET LOCAL ADJOINT VARIABLES NO ALLOC
C----------------------------------------------
      do ip4 = 1, nsy
        do ip3 = 1, nsx
          do ip2 = 1-oly, sny+oly
            do ip1 = 1-olx, snx+olx
              mybar_ad(ip1,ip2,ip3,ip4) = 0.d0
            end do
          end do
        end do
      end do


      mydummy_ad = 0.d0
      mysumgloh_ad = 0.d0
      tmpsumgloh_ad = 0.d0
      do ip2 = 1, nsy
        do ip1 = 1, nsx
          tmpsumtileh_ad(ip1,ip2) = 0.d0
        end do
      end do



      do kgen = ngencost, 1, -1

        if (gencost_name(kgen)(1:7) .eq. 'boxmean' .and. using_gencost(
     $kgen)) then

          doglobalread =  .false. 
          ladinit =  .false. 
          mydummy = gencost_dummy(kgen)

c-- barfile name
          il = ilnblnk(gencost_barfile(kgen))
          write(unit=myfname(1:80),fmt='(2a,i10.10)') gencost_barfile(
     $kgen)(1:il),'.',eccoiter
          nrecloc = gencost_nrec(kgen)

c-- temporal mask
          do irec = 1, nrecloc
            il = ilnblnk(gencost_errfile(kgen))
            write(unit=tempfile(1:128),fmt='(2A)') gencost_errfile(kgen)
     $(1:il),'T'
            inquire(file=tempfile(1:il+1),exist=exst)
            if (( .not. exst) .or. gencost_errfile(kgen) .eq. ' ') then
            else
              iounit = 0
              call mds_readvec_loc( tempfile,cost_iprec,iounit,'RL',1,
     $gencost_msktemporal,dummyrs,0,0,irec,mythid )
            endif
          end do

c-- ad sum tile
          call global_adsum_tile_rl( objf_gencost_ad(1,1,kgen),
     $mysumgloh_ad,mythid )

c-- temporal mask backward
          do irec = nrecloc, 1, -1
            il = ilnblnk(gencost_errfile(kgen))
            write(unit=tempfile(1:128),fmt='(2A)') gencost_errfile(kgen)
     $(1:il),'T'
            inquire(file=tempfile(1:il+1),exist=exst)
            if (( .not. exst) .or. gencost_errfile(kgen) .eq. ' ') then
              gencost_msktemporal = nrecloc
              gencost_msktemporal = 1.d0/gencost_msktemporal
            else
              iounit = 0
              call mds_readvec_loc( tempfile,cost_iprec,iounit,'RL',1,
     $gencost_msktemporal,dummyrs,0,0,irec,mythid )
            endif

c-- ad sum tile 
            call global_adsum_tile_rl( tmpsumtileh_ad,tmpsumgloh_ad,
     $mythid )
            do bj = mybyhi(mythid), mybylo(mythid), -1
              do bi = mybxhi(mythid), mybxlo(mythid), -1
                do j = 1, sny
                  do i = 1, snx
                    mybar_ad(i,j,bi,bj) = mybar_ad(i,j,bi,bj)+
     $objf_gencost_ad(bi,bj,kgen)*gencost_msktemporal
                  end do
                end do
              end do
            end do

c-- ad adctive read
            call adactive_read_xy( myfname,mybar_ad,irec,doglobalread,
     $ladinit,eccoiter,mythid,mydummy,mydummy_ad )

          end do
          gencost_dummy_ad(kgen) = gencost_dummy_ad(kgen)+mydummy_ad
          mydummy_ad = 0.d0
        endif
      end do

      end
