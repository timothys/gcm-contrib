function [] = calc_rho_moc(uvel,vvel,maskW,maskS,theta,salt,maskC)
% Compute AMOC in density space along lines defined by maskW/S
%
% ------------------------------------------------------------

mygrid = establish_mygrid('llc90');


% First compute density
rho = densjmd95
