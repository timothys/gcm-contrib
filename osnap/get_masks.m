function [osnap_east, osnap_west] = get_masks()
% Get mask files denoting the east and west sections
% 
% ----------------------------------------------------

mygrid = establish_mygrid('llc90');

% --- OSNAP West
lons_west = [-52 -46];
lats_west = [52.5 60];
osnap_west = gcmfaces_lines_transp(lons_west,lats_west,{'OSNAP West'});

% --- OSNAP East 
lons_east = [-44 -5];
lats_east = [60 56];
osnap_east = gcmfaces_lines_transp(lons_east,lats_east,{'OSNAP East'});


end
