function [osnap_east, osnap_west] = calc_osnap()
% Compute OSNAP transport East and West values


load_dir = '../../results/eccov4r2/ref/diags';
trsp_file = sprintf('%s/trsp_3d_set1',load_dir);
ts_file = sprintf('%s/state_3d_set1',load_dir);

save_dir = 'mat';
if ~exist(save_dir,'dir'), mkdir(save_dir); end;
save_file = sprintf('%s/osnap_trsp.mat',save_dir);

% First compute masks defining sections
[mask_east, mask_west] = get_masks;

% Time length...
Nmo = 240;
Nr = length(mygrid.RC);

if ~exist(save_file,'file')

    osnap_east = zeros(1,Nmo);
    osnap_east_stf = zeros(Nr,Nmo);
    osnap_west = zeros(1,Nmo);
    osnap_west_stf = zeros(Nr,Nmo);

    % Find time steps
    iters = grabAllIters(load_dir,'trsp_3d_set1');
    if Nmo ~= length(iters)
        error(sprintf('Hard coded Nmo not equal to number of timesteps found in dir %s', load_dir);
    end

    for n = 1:Nmo

        % Load velocity fields
        uvel = rdmds2gcmfaces(trsp_file,iters(n),'rec',1);
        vvel = rdmds2gcmfaces(trsp_file,iters(n),'rec',2);

        % Load T/S fields
        theta = rdmds2gcmfaces(ts_file,iters(n),'rec',1);
        salt = rdmds2gcmfaces(ts_file,iters(n),'rec',2);

        % Compute OSNAP
        [osnap_east(n), osnap_stf_east(:,n)] = calc_rho_moc(theta,salt,uvel,vvel,mask_east.mskWedge,mask_east.mskSedge);
        [osnap_west(n), osnap_stf_west(:,n)] = calc_rho_moc(theta,salt,uvel,vvel,mask_east.mskWedge,mask_east.mskSedge);
    end

else
    load(save_file,'osnap_east','osnap_west');

end

end
