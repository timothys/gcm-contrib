C--   These common blocks are extracted from the
C--   automatically created tangent linear code.
C--   You need to make sure that they are up-to-date
C--   (i.e. in right order), and customize them accordingly.
C--
C--   heimbach@mit.edu 11-Jan-2001

#ifdef ALLOW_AUTODIFF_MONITOR

      common /dynvars_r_ad/
     &                     etan_ad,
     &                     uvel_ad, vvel_ad, wvel_ad,
     &                     theta_ad, salt_ad,
     &                     gu_ad, gv_ad,
#ifdef ALLOW_ADAMSBASHFORTH_3
     &                     gunm_ad, gvnm_ad, gtnm_ad, gsnm_ad
#else
     &                     gunm1_ad, gvnm1_ad, gtnm1_ad, gsnm1_ad
#endif
      _RL etan_ad(1-olx:snx+olx,1-oly:sny+oly,nsx,nsy)
      _RL gu_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL gv_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL salt_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL theta_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL uvel_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL vvel_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL wvel_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
#ifdef ALLOW_ADAMSBASHFORTH_3
      _RL gtnm_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy,2)
      _RL gsnm_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy,2)
      _RL gunm_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy,2)
      _RL gvnm_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy,2)
#else
      _RL gtnm1_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL gsnm1_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL gunm1_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
      _RL gvnm1_ad(1-olx:snx+olx,1-oly:sny+oly,nr,nsx,nsy)
#endif

      common /dynvars_r_2_ad/
     &                     etah_ad
      _RL etah_ad(1-olx:snx+olx,1-oly:sny+oly,nsx,nsy)


#endif /* ALLOW_AUTODIFF_MONITOR */
