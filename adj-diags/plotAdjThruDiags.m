function [] = plotAdjThruDiags(gridRes)
% (optional) Input: 
%       
%       gridRes: scalar
%                represents grid resolution, options are
%                8 (default), 16, 32
% 
% ------------------------------------------------------
prec=64;

if nargin<1, gridRes=8; end;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));

% --- Grab diagnostics
%resultsDir='/workspace/ase-adjoint/pig/generic/run_ad_08';
resultsDir='/workspace/ase-adjoint/pig/no-ecco/run_ad_08_nodepth';
diagsDir = sprintf('%s/diags/',resultsDir);

% ADJetan is broken...
%eta_snaps = rdmds([diagsDir 'state_2d_snaps'],NaN,'rec',1);
%eta_ad_avg = rdmds([diagsDir 'adjState_2d_avg'],NaN,'rec',1);
%eta_ad_snaps = rdmds([diagsDir 'adjState_2d_snaps'],NaN,'rec',1);
eta_adj= rdmds([diagsDir '../ADJetan'],NaN);
eta_adxx=rdmds([resultsDir '/adxx_etan'],0);

%theta_ad_avg = rdmds([diagsDir 'adjState_3d_avg'],NaN,'rec',1);
%theta_ad_snaps=rdmds([diagsDir 'adjState_3d_snaps'],NaN,'rec',1);
%theta_adj= rdmds([diagsDir '../ADJtheta'],NaN);
%
%salt_ad_avg = rdmds([diagsDir 'adjState_3d_avg'],NaN,'rec',2);
%salt_ad_snaps=rdmds([diagsDir 'adjState_3d_snaps'],NaN,'rec',2);
%salt_adj= rdmds([diagsDir '../ADJsalt'],NaN);
%
%uvel_ad_avg = rdmds([diagsDir 'adjState_3d_avg'],NaN,'rec',3);
%uvel_ad_snaps=rdmds([diagsDir 'adjState_3d_snaps'],NaN,'rec',3);
%uvel_adj= rdmds([diagsDir '../ADJuvel'],NaN);
%
%vvel_ad_avg = rdmds([diagsDir 'adjState_3d_avg'],NaN,'rec',4);
%vvel_ad_snaps=rdmds([diagsDir 'adjState_3d_snaps'],NaN,'rec',4);
%vvel_adj= rdmds([diagsDir '../ADJvvel'],NaN);
%
%wvel_ad_avg = rdmds([diagsDir 'adjState_3d_avg'],NaN,'rec',5);
%wvel_ad_snaps=rdmds([diagsDir 'adjState_3d_snaps'],NaN,'rec',5);
%wvel_adj= rdmds([diagsDir '../ADJwvel'],NaN);

% Load netCDF diags...
%mnc_dir = '/workspace/results/pig/mnc_adj_test/diags/';
%mnc_suffix = '.0000000000.t001.nc';
%theta_ad_snaps_nc = ncread([mnc_dir 'adjState_3d_snaps' mnc_suffix],'ADJtheta');
%salt_ad_snaps_nc = ncread([mnc_dir 'adjState_3d_snaps' mnc_suffix],'ADJsalt');
%uvel_ad_snaps_nc = ncread([mnc_dir 'adjState_3d_snaps' mnc_suffix],'ADJuvel');
%vvel_ad_snaps_nc = ncread([mnc_dir 'adjState_3d_snaps' mnc_suffix],'ADJvvel');
%wvel_ad_snaps_nc = ncread([mnc_dir 'adjState_3d_snaps' mnc_suffix],'ADJwvel');
%theta_ad_avg_nc = ncread([mnc_dir 'adjState_3d_avg' mnc_suffix],'ADJtheta');
%salt_ad_avg_nc = ncread([mnc_dir 'adjState_3d_avg' mnc_suffix],'ADJsalt');
%uvel_ad_avg_nc = ncread([mnc_dir 'adjState_3d_avg' mnc_suffix],'ADJuvel');
%vvel_ad_avg_nc = ncread([mnc_dir 'adjState_3d_avg' mnc_suffix],'ADJvvel');
%wvel_ad_avg_nc = ncread([mnc_dir 'adjState_3d_avg' mnc_suffix],'ADJwvel');

%eta_snaps = rdmds([diagsDir 'state_2d_snaps'],NaN,'rec',1);
%eta_avg = rdmds([diagsDir 'state_2d_avg'],NaN,'rec',1);
%theta_snaps_nc = ncread([mnc_dir 'state_3d_snaps' mnc_suffix],'THETA');
%theta_avg_nc = ncread([mnc_dir 'state_3d_avg' mnc_suffix],'THETA');
%t_nc_fwd=ncread([mnc_dir 'state_3d_snaps' mnc_suffix],'T');
%t_nc_fwda=ncread([mnc_dir 'state_3d_avg' mnc_suffix],'T');
%t_nc = ncread([mnc_dir 'adjState_3d_snaps' mnc_suffix],'T');
%t_nca = ncread([mnc_dir 'adjState_3d_avg' mnc_suffix],'T');
%Nt_nc=length(t_nc);
%Nt_nca=length(t_nca);


%theta_ad_snaps_nc = flipdim(theta_ad_snaps_nc(:,:,:,Nt_nc-5:Nt_nc),4);
%salt_ad_snaps_nc = flipdim(salt_ad_snaps_nc(:,:,:,Nt_nc-5:Nt_nc),4);
%uvel_ad_snaps_nc = flipdim(uvel_ad_snaps_nc(:,:,:,Nt_nc-5:Nt_nc),4);
%vvel_ad_snaps_nc = flipdim(vvel_ad_snaps_nc(:,:,:,Nt_nc-5:Nt_nc),4);
%wvel_ad_snaps_nc = flipdim(wvel_ad_snaps_nc(:,:,:,Nt_nc-5:Nt_nc),4);
%theta_ad_avg_nc = flipdim(theta_ad_avg_nc(:,:,:,Nt_nca-2:Nt_nca),4);
%salt_ad_avg_nc = flipdim(salt_ad_avg_nc(:,:,:,Nt_nca-2:Nt_nca),4);
%uvel_ad_avg_nc = flipdim(uvel_ad_avg_nc(:,:,:,Nt_nca-2:Nt_nca),4);
%vvel_ad_avg_nc = flipdim(vvel_ad_avg_nc(:,:,:,Nt_nca-2:Nt_nca),4);
%wvel_ad_avg_nc = flipdim(wvel_ad_avg_nc(:,:,:,Nt_nca-2:Nt_nca),4);
%
%[theta_nc_diff,max_theta_nc_diff] = calcQuickDiff(theta_ad_snaps,theta_ad_snaps_nc);
%[salt_nc_diff,max_salt_nc_diff] = calcQuickDiff(salt_ad_snaps,salt_ad_snaps_nc);
%[uvel_nc_diff,max_uvel_nc_diff] = calcQuickDiff(uvel_ad_snaps,uvel_ad_snaps_nc(1:end-1,:,:,:));
%[vvel_nc_diff,max_vvel_nc_diff] = calcQuickDiff(vvel_ad_snaps,vvel_ad_snaps_nc(:,1:end-1,:,:));
%[wvel_nc_diff,max_wvel_nc_diff] = calcQuickDiff(wvel_ad_snaps,wvel_ad_snaps_nc);
%
%[fwd_nc_diff,max_fwd_diff]=calcQuickDiff(theta_snaps,theta_snaps_nc);
%[afwd_nc_diff,amax_fwd_diff]=calcQuickDiff(theta_avg,theta_avg_nc);
%
%
%[atheta_nc_diff,amax_theta_nc_diff] = calcQuickDiff(theta_ad_avg,theta_ad_avg_nc);
%[asalt_nc_diff,amax_salt_nc_diff] = calcQuickDiff(salt_ad_avg,salt_ad_avg_nc);
%[auvel_nc_diff,amax_uvel_nc_diff] = calcQuickDiff(uvel_ad_avg,uvel_ad_avg_nc(1:end-1,:,:,:));
%[avvel_nc_diff,amax_vvel_nc_diff] = calcQuickDiff(vvel_ad_avg,vvel_ad_avg_nc(:,1:end-1,:,:));
%[awvel_nc_diff,amax_wvel_nc_diff] = calcQuickDiff(wvel_ad_avg,wvel_ad_avg_nc);


% --- plot diags movie
zlev=20;
%Nt_avg =size(eta_ad_avg,3);%size(theta,4);
Nt_snap =size(eta_ad_snaps,3);%size(theta,4);

% Plot difference between eta_adxx and first snapshot 
shelficeMsk = mygrid.hFacCsurf~=0;
diff_adj = (eta_adxx - eta_adj(:,:,1)).*shelficeMsk;
%  only doing this when avg over time step...
%diff_diag  = (eta_adxx - eta_ad_snaps(:,:,1)).*shelficeMsk;
figure
contourf(mygrid.XC,mygrid.YC,diff_adj)
    niceColorbar;
    title('adxx - adj dump (t=t$_0$)')
%figure
%contourf(mygrid.XC,mygrid.YC,diff_diag)
%    niceColorbar;
%    title('adxx - diag adj (t=t$_0$)')

keyboard
%[eta_diff,max_eta_diff] = calcQuickDiff(eta_ad_snaps,eta_adj);
%[theta_diff,max_theta_diff] = calcQuickDiff(theta_ad_snaps,theta_adj);
%[salt_diff,max_salt_diff] = calcQuickDiff(salt_ad_snaps,salt_adj);
%[uvel_diff,max_uvel_diff] = calcQuickDiff(uvel_ad_snaps,uvel_adj);
%[vvel_diff,max_vvel_diff] = calcQuickDiff(vvel_ad_snaps,vvel_adj);
%[wvel_diff,max_wvel_diff] = calcQuickDiff(wvel_ad_snaps,wvel_adj);


%diagMovie(eta_ad_avg,zlev,Nt_avg)
%title('eta ad avg')
%figure
%contourf(mygrid.XC,mygrid.YC,eta_adxx)
%title('eta adxx')
%
%keyboard
%diagMovie(eta_ad_snaps,zlev,Nt_snap);
%
%keyboard
diagMovie(eta_adj,zlev,size(eta_adj,3));

keyboard

end

% ------------------------------------------------------

function [] = diagMovie(fld,zlev,Nt)

global mygrid;

mskC=mygrid.mskC;
mskC_2d = nanmax(mygrid.mskC,[],3);
mskC_2d(mskC_2d==0)=NaN;
mskC(mskC==0)=NaN;

figure;
for n=1:Nt
  %contourf(mygrid.XC,mygrid.YC,theta(:,:,zlev,n));
  if length(size(fld))<4
    contourf(mygrid.XC,mygrid.YC,fld(:,:,n).*mskC_2d);
  else
    contourf(mygrid.XC,mygrid.YC,fld(:,:,zlev,n).*mskC(:,:,zlev));
  end
  %hold on;
  %quiver(mygrid.XC,mygrid.YC,uvel(:,:,zlev,n),vvel(:,:,zlev,n),'k');
  %caxis([-1 1]);
  niceColorbar;
  pause(0.5)
  %hold off
end
end

% ------------------------------------------------------

function [] = diagMovieDepthDive(fld,n_time)
% Plot movie going down in depth at a point in time: n_time
global mygrid;

mskC=mygrid.mskC;
mskC_2d = nanmax(mygrid.mskC,[],3);
mskC_2d(mskC_2d==0)=NaN;
mskC(mskC==0)=NaN;
Nr = length(mygrid.DRF);
figure;
for k=1:Nr
  %contourf(mygrid.XC,mygrid.YC,theta(:,:,zlev,n));
  contourf(mygrid.XC,mygrid.YC,fld(:,:,k,n_time).*mskC(:,:,k));
  %hold on;
  %quiver(mygrid.XC,mygrid.YC,uvel(:,:,zlev,n),vvel(:,:,zlev,n),'k');
  %caxis([-1 1]);
  niceColorbar;
  pause(0.5)
  %hold off
end
end

% ------------------------------------------------------

function [fldDiff, maxFldDiff] = calcQuickDiff(fld1,fld2)

fldDiff = abs(fld1-fld2);

maxFldDiff=nanmax(fldDiff(:));
end

% ------------------------------------------------------

function [] = plotDomainMean(fld,fldStr)

% --- Plot domain mean quantities from spinup
global mygrid;
Nr = size(fld,3);
Nt = size(fld,4);


% Scale by grid cell volume
drf = repmat( reshape(mygrid.DRF,[1 1 length(mygrid.DRF)]), size(mygrid.DXG) );
fld = fld.*repmat(mygrid.DXG,[1 1 Nr Nt]).*...
                 repmat(mygrid.DYG,[1 1 Nr Nt]).*...
                 repmat(drf,[1 1 1 Nt]); 

global_fld = squeeze(nansum(nansum(nansum(fld,1),2),3));
totalVolume = nansum(nansum(nansum(repmat(mygrid.DXG, [1 1 Nr]).*...
                                   repmat(mygrid.DYG, [1 1 Nr]).*...
                                   drf)));

global_fld = global_fld/totalVolume;

% --- Meaningful time axis, 10 year spinup
diagnosticFreq = 1209600 / 3600 / 24 / 365 ; % [yrs]
t = [1:Nt]*diagnosticFreq;

figure;
plot(t,global_fld)
  title(sprintf('Domain Volume Avg %s vs Time',fldStr))
  xlabel('Years')
end
