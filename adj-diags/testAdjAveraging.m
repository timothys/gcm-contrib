function [] = testAdjAveraging()
% Test what the adjoint state variables are actually printing out
% compared to the adxx files...
% 
% ------------------------------------------------------------------

mygrid = establish_mygrid('pig_08');

% Set dirs
resultsDir = '/workspace/ase-adjoint/pig/generic/run_ad_08/';
diagsDir = [resultsDir 'diags/'];


% Load snapshots and full dJ/dx
eta_ad_snaps = rdmds([diagsDir 'adjState_2d_snaps'],NaN,'rec',1);
theta_ad_snaps=rdmds([diagsDir 'adjState_3d_snaps'],NaN,'rec',1);

eta_ad = rdmds([resultsDir 'adxx_etan'],NaN);
theta_ad = rdmds([resultsDir 'adxx_theta'],NaN);


% Accumulate snaps
sum_eta_snaps = nansum(eta_ad_snaps,3);
sum_theta_snaps = nansum(theta_ad_snaps,4);

% Test diff
[eta_diff,max_eta_diff] = calcQuickDiff(sum_eta_snaps,eta_ad)
[theta_diff,max_theta_diff]=calcQuickDiff(sum_theta_snaps,theta_ad)

% Test final snapshot
[eta_diff0,max_eta_diff0] = calcQuickDiff(eta_ad_snaps(:,:,1),eta_ad)
[theta_diff0,max_theta_diff0] = calcQuickDiff(theta_ad_snaps(:,:,:,1),theta_ad)

zlev=10;

figureH;
subplot(2,3,1)
    contourf(mygrid.XC,mygrid.YC,theta_ad_snaps(:,:,zlev,1))
    title('ADJtheta snapshot at time 0')
subplot(2,3,2)
    contourf(mygrid.XC,mygrid.YC,theta_ad(:,:,zlev))
    title(sprintf('adxx theta'))
subplot(2,3,3)
    contourf(mygrid.XC,mygrid.YC,sum_theta_snaps(:,:,zlev))
    title('sum( ADJtheta snapshots )')
subplot(2,3,4)
    contourf(mygrid.XC,mygrid.YC,theta_diff0(:,:,zlev))
    title(sprintf('abs( ADJtheta0 - adxxtheta )'))
    niceColorbar;
subplot(2,3,6)
    contourf(mygrid.XC,mygrid.YC,theta_diff(:,:,zlev))
    title(sprintf('abs( sum(ADJtheta snaps) - adxxtheta )'))
    niceColorbar;

keyboard

end

% ------------------------------------------------------------------

function [fldDiff, maxFldDiff] = calcQuickDiff(fld1,fld2)

fldDiff = abs(fld1-fld2);

maxFldDiff=nanmax(fldDiff(:));
end
