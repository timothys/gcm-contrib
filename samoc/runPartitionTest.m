function[] = runPartitionTest(modelVar,atmVar )
% Want to compute samoc reconstruction and partition the hflux contribution
% to pin down sources of error and accumulation in the reconstruction

msks=createRegionMasks('ekman'); 
regionRec = msks;
mskNames=fieldnames(msks);

% --- Adding last bit of sensitivity
sensFrac=calcSamocSensFrac();

for i=1:length(mskNames)

        subNames = fieldnames(getfield(msks,mskNames{i}));
        if strcmp(subNames{1},'nFaces')
          
          matFile=sprintf('mat/reconstruct.34S/%s/%s/%s/fullReconstruction.mat',modelVar,atmVar,mskNames{i});

          if ~exist(matFile,'file')          
            tmp=samocReconstructionByMonth(modelVar,atmVar,-34,getfield(msks,mskNames{i}),mskNames{i},sensFrac);
          else
            load(matFile,'samoc_adjFields');
            tmp=samoc_adjFields;
            clear samoc_adjFields;
          end

          regionRec = setfield(regionRec,mskNames{i},tmp);
          fprintf('-- Done with %s --\n',mskNames{i});

        else
          for j=1:length(subNames)
            saveName = sprintf('%s/%s',mskNames{i},subNames{j});
            saveDir = sprintf('mat/reconstruct.34S/%s/%s/%s',modelVar,atmVar,mskNames{i});
            if ~exist(saveDir,'dir'), mkdir(saveDir); end;
            
            matFile=sprintf('mat/reconstruct.34S/%s/%s/%s/fullReconstruction.mat',modelVar,atmVar,saveName);

            if ~exist(matFile,'file')
              tmp=samocReconstructionByMonth(modelVar,atmVar,-34,getfield(getfield(msks,mskNames{i}),subNames{j}),saveName,sensFrac);
            else
              load(matFile,'samoc_adjFields');
              tmp=samoc_adjFields;
              clear samoc_adjFields;
            end
       
            eval(sprintf('regionRec.%s.%s=tmp;',mskNames{i},subNames{j}));
            fprintf('-- Done with %s %s --\n',mskNames{i},subNames{j});
          end
        end
end

% Save data structure containing regional reconstruction
matfile=sprintf('mat/reconstruct.34S/%s/%s/region_rec.mat',modelVar,atmVar);
save(matfile,'msks','regionRec');
end
