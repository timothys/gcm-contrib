function [] = verifyAmoc(mygrid)
% Want to compare AMOC computed by m_trVol_month in simulation vs ECCO
% fields


Nt = 240; 
Nz = 50;

%% First, grab ECCO product 
slat = -34; 
[samoc] = calcAMOC(slat,'eulerian',mygrid);

%% Loop through and load trvol field
trvol = zeros(Nz,Nt);

for n = 1:Nt
    cf = read_bin('~/Downloads/m_trVol_month.0000000012.data',n);
    trvol(:,n) = squeeze(nansum(nansum(convert2gcmfaces(cf),1),2));
end

ovStf = -flipdim(cumsum(flipdim(trvol,1),1),1);
kmax = zeros(1,Nt);
amoc = zeros(1,Nt);
depthmax = zeros(1,Nt);
for n = 1:Nt
    [~,kmax(n)] = max(ovStf(:,n));
    amoc(n) = ovStf(kmax(n),n);
    depthmax(n) = abs(mygrid.RF(kmax(n)));
end

amoc = amoc*10^-6;

figure
plot(1:Nt,samoc,1:Nt,amoc)
    xlabel('months')
    ylabel('AMOC (Sv)')
    
    
keyboard
end
