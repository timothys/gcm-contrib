function [eqsoi,years,months] = calcEquatorialSOI()
% A script to convert equatorial SOI index from csv form to vector
% Finally I'm doing this repeatably...
% ----------------------------------------------

% --- Read in the data
filename = 'index-data/equatorial_soi.data';
fid = fopen(filename);
mat = fscanf(fid,'%f',[13 20]);
mat = mat';
fclose(fid);

% --- Convert second column to vector with the index
eqsoi = mat(:,2:end)';
eqsoi = eqsoi(:);

% --- Create year vector starting with first year given
Nt = length(eqsoi);
months = 1:Nt;
years = months/12 + mat(1,1);

% --- Convert all to row vectors b/c I like time to go with this dimension
% already done for years and months
eqsoi   = eqsoi';

end
