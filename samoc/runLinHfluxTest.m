function [] = runLinHfluxTest( mygrid )
% Does removing ACC and N. Subpolar gyre regions from reconstruction
% result in linear hflux calculation? 
%
% ------------------------------------------------------------------

msks=createRegionMasks('ekman');

% Mask out ACC and N. Subpolar gyre
linMsk = msks.globe - msks.acc - msks.atl.subPolar;

modelVar='eccov4r2';
atmVar='flux';
sensFrac = calcSamocSensFrac();

dirName = sprintf('linearHflux');
if ~exist(sprintf('mat/reconstruct.34S/%s/%s/%s/fullReconstruction.mat',modelVar,atmVar,dirName),'file')
  samocReconstructionByMonth(modelVar,atmVar,-34,mygrid,linMsk,dirName,sensFrac);
end

plotSamocReconstructionByMonth(modelVar,atmVar,-34,mygrid,dirName);
keyboard

for i=0:2

        % n = number of months of barotropic response to remove
        n=int8(i);
        
        % create dir name
        dirName = sprintf('linearHflux_rm%dmo',n+1);

        if ~exist(sprintf('mat/reconstruct.34S/%s/%s/%s/fullReconstruction.mat',modelVar,atmVar,dirName),'file')
          samocReconstructionByMonth(modelVar,atmVar,-34,mygrid,n,linMsk,dirName,sensFrac);
        end

        plotSamocReconstructionByMonth(modelVar,atmVar,-34,mygrid,dirName);

        keyboard
        close all
        
end
