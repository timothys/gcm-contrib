% Function should return a mask over 34S for evaluating a cost function
% along the rapid array

mygrid = establish_mygrid('llc90');

if ~exist('masks/','dir'), mkdir('masks'),end

%% 2D masks at 34S
ind = find(mygrid.LATS==-34);
mskS=mygrid.LATS_MASKS(ind).mskSedge;
mskW=mygrid.LATS_MASKS(ind).mskWedge;
atlBasin = v4_basin('atlExt');

mskS = convert2gcmfaces(mskS.*atlBasin);
mskW = convert2gcmfaces(mskW.*atlBasin);

mskS(isnan(mskS))=0;
mskW(isnan(mskW))=0;

%% Depth mask
NR=length(mygrid.DRF);
mskK = ones(1,NR);

%% Temporal mask
%mskT = [zeros(1,228) 0 0 0 0 0 0 0 0 0 0 0 1];
%mskT = [zeros(1,11) 1];
%mskT = [0 0 1];
%mskT = [zeros(1,7291) 1/14*ones(1,14) 0 0 0 0 0 0];
mskT = [zeros(1,8) 1];

%% Write to file
write2file('masks/maskS',mskS);
write2file('masks/maskW',mskW);
write2file('masks/maskK',mskK);
write2file('masks/maskT',mskT);

%% For reproducibility and such ... copy this file as well so we know what made the masks
copyfile('create_masks.m','masks/');

% Zip it up for sending to a supercomputer 
tar('masks.tar.gz','masks/');
