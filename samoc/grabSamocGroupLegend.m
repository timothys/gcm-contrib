function [legendCell] = grabSamocGroupLegend(separator)
% Want to consistently make a legend based on how the regions are separated out. 
%
% Input:
%   
%   separator: 'south' - splits up southern ocean to ind,pac,atl basins
%              'ek' - splits up Atl South Subtropical to local "ekman" component and others
%              'southek' - does both ... this is used in Smith&Heimbach 201?
%              'none' ... nothing
%
% Output: 
%   
%   legendCell: cell structure with appropriate legend fields
%
% -------------------------------------------------------------------------------

fullLegend={'Arctic','Pacific Subpolar',...
            'Pacific N. Subtropics','Pacific Tropics','Pacific S. Subtropics',...
            'Atlantic Subpolar','Atlantic N. Subtropics','Atlantic Tropics'}; ...

if strfind(separator,'ek')
  fullLegend=cat(2,fullLegend,{'Atlantic S. Subtropics','$30-40^{\circ}$S'});
else
  fullLegend=cat(2,fullLegend,{'Atlantic S. Subtropics'});
end

fullLegend=cat(2,fullLegend,...
           {'Indian Tropics','Indian S. Subtropics'});

if strfind(separator,'south')
  fullLegend=cat(2,fullLegend,{'Pacific South','Atlantic South','Indian South','ACC ($<60^\circ$S)'});
else
  fullLegend=cat(2,fullLegend,{'Southern Ocean'});
end

legendCell = fullLegend;

end
