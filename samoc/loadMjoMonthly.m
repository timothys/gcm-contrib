function [] = loadMjoMonthly()
% Read in the daily MJO index data and convert to monthly
%

fid = fopen('index-data/mjo_daily.data','r');

%iyear | imonth | iday | ihour | PC1 coefficient | PC2 coefficient | PC1+PC2 amplitud
ncols = 7;
nrows = 23*365+6; %23 years, 6 leap years
mjo_daily = fscanf(fid,'%f',[ncols, nrows]);
fclose(fid);

% Do the processing
mjo_daily=mjo_daily'; % Now it looks like the .data file

yrs=1990:2012;
Nyrs=length(yrs);
Nmo=12*Nyrs;
day_per_month = [0 31 28 31 30 31 30 31 31 30 31 30 31];
day_in_year = cumsum(day_per_month);
day_per_month_leap = [0 31 29 31 30 31 30 31 31 30 31 30 31];
day_in_year_leap = cumsum(day_per_month_leap);
leapYrs = 1992:4:2011;

pc1_col = 5;
pc2_col = 6;
pc12_col= 7;
mjo_pc1_monthly = zeros(1,Nmo);
mjo_pc2_monthly = zeros(1,Nmo);
mjo_pc12_monthly = zeros(1,Nmo);

for i=1:Nyrs

    % Test for leap year
    if nansum(yrs(i) == leapYrs) == 1
        diy = day_in_year_leap;
    else
        diy = day_in_year;
    end

    for j=1:12

        % Compute mean over each month
        mo_ind = (i-1)*12 + j;
        day_ind_st = diy(j) + 1;
        day_ind_ed = diy(j+1);

        mjo_pc1_monthly(mo_ind) = nanmean(mjo_daily(day_ind_st:day_ind_ed,pc1_col));
        mjo_pc2_monthly(mo_ind) = nanmean(mjo_daily(day_ind_st:day_ind_ed,pc2_col));
        mjo_pc12_monthly(mo_ind) = nanmean(mjo_daily(day_ind_st:day_ind_ed,pc12_col));
    end

    fprintf(' --- Done with %d ---\n',yrs(i));

end

% Save it up
mjo_pc1 = mjo_pc1_monthly;
mjo_pc2 = mjo_pc2_monthly;
mjo_pc12= mjo_pc12_monthly;
ind92 = 24+1;
save('mat/index-data/mjo_monthly.mat','mjo_pc1','mjo_pc2','mjo_pc12','ind92');
keyboard
end

