function [] = plotPerturbVelocity()
% Show perturbed velocity vs depth as a result of 
%      perturbed wind field during near Indonesian thruflow
%
% --------------------------------------------------------------------
expStr = 'barotropic.mo';
refStr='ref';
perturbStr='pert';
freqStr = 'daily';
%for n=1:length(varargin)
%    if iscell(varargin{n})
%        perturbStr=varargin{n};
%    end
%end

mygrid = establish_mygrid('llc90');

% Set up fig dir for saving plots
fig_dir = sprintf('figures/%s',expStr);
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;

% Set up load and save dirs
ref_results_dir = sprintf('../../results/samoc/%s/%s/diags/',expStr,refStr);
pert_results_dir = sprintf('../../results/samoc/%s/%s/diags/',expStr,perturbStr);
exp_mat_dir = sprintf('mat/%s',expStr);
diagsName=sprintf('trsp_%s',freqStr);

% Check for mat_file first
mat_file = sprintf('%s/velocity_vs_depth_%s.mat',exp_mat_dir,freqStr);

% Load the velocity fields
iters = grabAllIters(ref_results_dir,diagsName);
Nt=length(iters);


if ~exist(mat_file,'file')
    
    % Do this for 1st iter to get latXV, latR, Nx, Nr

    % Read results
    uvel_ref = rdmds2gcmfaces([ref_results_dir diagsName],iters(1),'rec',1);
    vvel_ref = rdmds2gcmfaces([ref_results_dir diagsName],iters(1),'rec',2);

    uvel_per = rdmds2gcmfaces([pert_results_dir diagsName],iters(1),'rec',1);
    vvel_per = rdmds2gcmfaces([pert_results_dir diagsName],iters(1),'rec',2);

    % Mask out bathy
    uvel_ref = uvel_ref.*mygrid.mskW; vvel_ref = vvel_ref.*mygrid.mskS;
    uvel_per = uvel_per.*mygrid.mskW; vvel_per = vvel_per.*mygrid.mskS;

    % Grab lat band
    [~,latV_ref,~,latXV,latR] = grabAtlLatBand_uv(-34,uvel_ref,vvel_ref);
    [~,latV_per,~,latXV,latR] = grabAtlLatBand_uv(-34,uvel_per,vvel_per);
    Nx = length(latXV);
    Nr = length(latR);

    % Initialize container
    diff_vel = zeros(Nr,Nx,Nt);
    diff_vel(:,:,1) = latV_per-latV_ref;

    % Read and fill
    for n=2:Nt

        % Read results
        uvel_ref = rdmds2gcmfaces([ref_results_dir diagsName],iters(n),'rec',1);
        vvel_ref = rdmds2gcmfaces([ref_results_dir diagsName],iters(n),'rec',2);

        uvel_per = rdmds2gcmfaces([pert_results_dir diagsName],iters(n),'rec',1);
        vvel_per = rdmds2gcmfaces([pert_results_dir diagsName],iters(n),'rec',2);

        % Mask out bathy
        uvel_ref = uvel_ref.*mygrid.mskW; vvel_ref = vvel_ref.*mygrid.mskS;
        uvel_per = uvel_per.*mygrid.mskW; vvel_per = vvel_per.*mygrid.mskS;

        % Grab lat band
        [~,latV_ref] = grabAtlLatBand_uv(-34,uvel_ref,vvel_ref);
        [~,latV_per] = grabAtlLatBand_uv(-34,uvel_per,vvel_per);
    
        diff_vel(:,:,n)=latV_per-latV_ref;
    
        if mod(n,10)==0
            fprintf(' --- Done %d / %d --- \n',n,Nt);
        end
    end
    save(mat_file,'diff_vel','latXV','latR');
else
    load(mat_file,'diff_vel','latXV','latR');
    Nx = size(diff_vel,2);
    Nr = size(diff_vel,1);
    Nt = size(diff_vel,3);
end

Nbwks = 9;          % Num. of biweeks in simulation
Ndays = 121;        % Num. days in simulation

t_bwks = [1:Nbwks]*2-1; % weeks
t_days = [1:Ndays]/7; % in weeks

if strcmp(freqStr,'biweekly')
    assert(Nt==Nbwks,sprintf('Num. records in %s != %d biweeks hard coded\n',pert_results_dir,Nbwks));
    t=t_bwks;
    selectVect = [3:4];
    meanVect = [3:4];
    t0 = 2;
elseif strcmp(freqStr,'daily')
    assert(Nt==Ndays,sprintf('Num. records in %s != %d days hard coded\n',pert_results_dir,Ndays));
    t=t_days;
    if ~isempty(strfind(expStr,'mo'))
        selectVect = [21,35];
        meanVect   = [32:60];
        t0         = 0;
        cmax = 5.5*10.^-4;
        cticks = [-5:5]*10^-4;
    elseif ~isempty(strfind(expStr,'bwk'))
        selectVect = [32,45];
        meanVect = [29:56];
        t0 = 28;
        cmax = 8*10.^[-5, -4];
        fprintf('Warning: may need to work out colorbar ticks ...\n');
    else
        error('expStr not recognized ...');
    end
end

% Plot it up
ncolors = 11;
cmap = flipdim(cbrewer2('RdBu',ncolors),1);
fs = 22;

for n=1:length(selectVect)

    % make the labels depending on period
    if strcmp(freqStr,'biweekly')
        xlbl = sprintf('%s, t = %d weeks since start of perturbation',...
                        '$v_{pert}-v_{ref}$',(selectVect(n)-t0)*2);
        filename = sprintf('%s/vel_diff_depth_%02d_wks',fig_dir,(selectVect(n)-t0)*2);
    elseif strcmp(freqStr,'daily')
        xlbl = sprintf('%s, t = %02d days since start of perturbation',...
                        '$v_{pert}-v_{ref}$',selectVect(n)-t0);
        filename = sprintf('%s/vel_diff_depth_%02d_days',fig_dir,selectVect(n)-t0);
    end

    % Make the plot
    figureW; 
    pcolor(latXV,latR,diff_vel(:,:,selectVect(n)));
        shading interp
        %xlabel(xlbl);
        ylabel('Depth (m)');
        set(gca,'fontsize',fs,...
            'ytick',[-5000:1000:0],'yticklabel',num2cell([-5000:1000:0]'),...
            'xtick',[-50:10:20],'xticklabel',...
            {'50$^{\circ}$W','40$^{\circ}$W','30$^{\circ}$W','20$^{\circ}$W','10$^{\circ}$W','0$^{\circ}$E','10$^{\circ}$E'});
        colormap(cmap);
        caxis([-cmax cmax]);
        hc=niceColorbar;
        hc.Ticks = cticks;
        set(hc,'fontsize',fs);
        ylabel(hc,'m/s','rotation',0,'fontsize',fs,...
            'units','normalized','position',[3.33 0.533 0]);
        set(gca,'color',[1 1 1]*.7); % set NaN color to gray
        savePlot(filename);
end

% Plot the mean over perturbation period
xlbl = sprintf('%s, time mean over perturbation period',...
                '$v_{pert}-v_{ref}$');
filename = sprintf('%s/vel_diff_depth_mean_%s',fig_dir,freqStr);
meanVel = nanmean(diff_vel(:,:,meanVect),3);
figureW; 
pcolor(latXV,latR,meanVel);
    shading interp
    %xlabel(xlbl);
    ylabel('Depth (m)')
    set(gca,'fontsize',fs,...
        'ytick',[-5000:1000:0],'yticklabel',num2cell([-5000:1000:0]'),...
        'xtick',[-50:10:20],'xticklabel',...
        {'50$^{\circ}$W','40$^{\circ}$W','30$^{\circ}$W','20$^{\circ}$W','10$^{\circ}$W','0$^{\circ}$E','10$^{\circ}$E'});
    colormap(cmap);
    caxis([-cmax cmax]);
    hc=niceColorbar;
    hc.Ticks = cticks;
    set(hc,'fontsize',fs);
    ylabel(hc,'m/s','rotation',0,'fontsize',fs,...
            'units','normalized','position',[3.33 0.533 0]);
    set(gca,'color',[1 1 1]*.7); % set NaN color to gray
    savePlot(filename);

% Verify that perturbation mostly impacts western bdy current
%   Indonesian throughflow->equatorial pacific->coastal Kelvin wave around SAmerica

% Grab zonal length of grid cells along 34S
[~,dxg]=grabAtlLatBand_uv(-34,mygrid.DYG,mygrid.DXG);
dxg=abs(dxg);

% Grab velocity during weeks 6-8 of simulation, maximum samoc anomaly
% Convert to Sverdrups
vel = 10^-6 * diff_vel(:,:,selectVect(2)); % [Sv/m^2]

% Western boundary current is around 1-8 zonal cells, 
%   9->end is contribution that passes through Indian basin
%   integrate to approximate samoc depth of ~1400m at 33rd cell
k_amoc = 33;
west_trsp = nansum( vel(:,1:8).*repmat(dxg(1:8),[Nr,1]), 2); %[Sv/m]
east_trsp = nansum( vel(:,9:end).*repmat(dxg(9:end),[Nr,1]), 2); %[Sv/m]

west_samoc = -flipdim( nansum( flipdim( ...
                west_trsp(k_amoc:end).*mygrid.DRF(k_amoc:end),1),1),1);
east_samoc = -flipdim( nansum( flipdim( ...
                east_trsp(k_amoc:end).*mygrid.DRF(k_amoc:end),1),1),1);


                keyboard
end

