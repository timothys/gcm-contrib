function [] = plotRegionRecErr()
% Function to show how SAMOC reconstruction improves as tau_mem increases
% when attributed to various regions
%
% Plot summed relative error
% this shows:
%
%   sum_t | dJ_i(t) - dJ_tauMem=19yrs(t) | / sum_t| dJ_tauMem=19yrs(t) | -vs- tauMem
%
%
% -------------------------------------------------------------------------

modelVar='eccov4r2';
atmVar='long-atm';
separator='southek';
numTopRegions = 5;

%[adjField] = set_adjField(atmVar);
% --- Want to condense thermal (radiation+atemp) as in paper
%     'warm' does this
%     'texas' would combine this with humidity ... 
adjField = {'tauu','tauv','warm','aqh','precip','runoff'};

climOrAnnual = {'clim','annual'};

for i=1:length(adjField)

  for j = 1:2

    % --- Result is mat that is [tauMemMax Ngroups]
    [absErr,relErr,topRegionIndex] = calcRegionRecErr(adjField{i},modelVar,atmVar,separator,climOrAnnual{j},numTopRegions);

    % --- Get legend stuff
    Ngroups = size(relErr,1);
    tauMemMax = size(relErr,2);
    titleStr = grabForceVarLegend(adjField{i});
    if strcmp(climOrAnnual{j},'clim')
      titleStr = sprintf('%s, Climatology',titleStr);
    else
      titleStr = sprintf('%s, Interannual',titleStr);
    end

    fullLegendStr = grabSamocGroupLegend(separator);
    legendStr = cell(1,numTopRegions+1);
    legendStr{end} = 'Total';
    for k = 1:numTopRegions
      legendStr{k} = fullLegendStr{topRegionIndex(k)};
    end

    % --- Plot it up
    tau_mem = [1:tauMemMax]./12;
    figureW;
    plot(tau_mem,relErr(1:numTopRegions,:))
    hold on
    plot(tau_mem,relErr(end,:),'k')
    hold off
      xlabel('\tau_{mem} (Years)')
      ylabel('Relative Error')
      title(titleStr);
      legend(legendStr);

  end
end

keyboard
end
