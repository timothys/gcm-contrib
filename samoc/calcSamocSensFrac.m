function [sensFrac] = calcSamocSensFrac( )

finalts = 175295;
dayPerMo = [31 28 31 30 31 30 31 31 30 31 30 31];
offset = cumsum(dayPerMo);
mts=  finalts - (365-offset)*24;

mat_file = 'mat/sensFrac.mat';

if ~exist(mat_file,'file')

    sensFrac = zeros(size(mts));
    
    for i=1:length(mts)
        sensFrac(i) = calcSensFrac( 240-12 + i-1, '../../results/samoc/dec.240mo/STDOUT.0000',mts(i) );
    end

else
    load(mat_file,'sensFrac');
end

end
