function [] = plotSamocSensMovies(varargin)
% Make the samoc sensitivity movies with cbrewer2
%
% Optional Inputs
%       
%       runStr : string denoting run type 
%               'dec.240mo' (default)
%
%       atmVar : atm vars or flux vars
%               'flux' (default) or 'atm'
%
%       areaWeighted: normalize sensitivities by area or not
%               'noNormByArea' (default) or 'normByArea'
%
% Colorbar notes
%
%       tauu: 
%           non-area-normalized:
%               +/- 10^-1 Sv / (N/m^2) 
%
%           area-normalized:
%               +/- 10^-11 Sv / N 
%
%       tauv: 
%           non-area-normalized:
%               +/- 10^-1 Sv / (N/m^2) 
%
%           area-normalized:
%               +/- 10^-11 Sv / N 
%
%       oceHeat: 
%           non-area-normalized:
%               +/- 10^6 Sv / (W/m^2)
%
%           area-normalized:
%               +/- 10^-16 Sv / W
%               from tf-1mo -> tf-6mo there are 18, 52, 48, 36, 22, 6 grid cells where 
%               floor(log10(abs(adxx))) = -15
%               They are close to 1-2 * 10^-15 and saturating these points does not remove features
%               (shows path of heat flux sensitivity moving through Drake Passage, still visible
%               with slightly saturated colorbar)
%
%       fwf: 
%           non-area-normalized:
%               +/- 10^9 Sv / (Sv/m^2)
%               Max amplitude is 10116 * 10^6 Sv / (Sv/m^2) and occurs at 1 point on the globe
%               at time step tf - 3 months ONLY. Removing this point by setting colorbar
%               to +/- 10^9 is no problem ...
%
%           area-normalized:
%               +/- 10^0 Sv / Sv  
%
% ----------------------------------
runStr = 'dec.240mo';
atmVar = 'flux';
areaWeighted='noNormByArea';
for n=1:length(varargin)
    if ischar(varargin{n})
        if ~isempty(strfind(varargin{n},'atm')) || ~isempty(strfind(varargin{n},'flux'))
            atmVar = varargin{n};
        elseif ~isempty(strfind(lower(varargin{n}),'area'))
            areaWeighted=varargin{n};
        else
            runStr = varargin{n};
        end
    end
end

% --- Load grid and setup data
mygrid=establish_mygrid('llc90');
matDir = sprintf('mat/%s/',runStr);
if ~exist(matDir,'dir'), error(sprintf('Cannot find matDir: %s, exiting ...',matDir)); end;
figDir = sprintf('figures/%s/',runStr);
if ~exist(figDir,'dir'), mkdir(figDir); end;


% --- Set the fields to plot
adjField = set_adjField(atmVar);
Nadj = length(adjField);
if ~isempty(strfind(atmVar,'atm'))
    if strcmp(areaWeighted,'normByArea')
        cunits = {sprintf('Sv/N'),sprintf('Sv/N'),sprintf('Sv/\n[kg/kg m^2]'),...
              sprintf('Sv/\n[K m$^2$]'),sprintf('Sv/W'), sprintf('Sv/W'),
              sprintf('Sv/\n[m^3/s]'),sprintf('Sv/\n[m^3/s]')};       
    else
        cunits = {sprintf('Sv/\n[N/m$^2$]'),sprintf('Sv/\n[N/$m^2$]'),sprintf('Sv/\n[kg/kg]'),...
              sprintf('Sv/\nK'),sprintf('Sv/\n[W/m$^2$]'), sprintf('Sv/\n[W/m$^2$]'),
              sprintf('Sv/\n[m/s]'),sprintf('Sv/\n[m/s]')};       
    end
else
    if strcmp(areaWeighted,'normByArea')
        cunits = {sprintf('Sv/N'),sprintf('Sv/N'),...
              sprintf('Sv/W'), sprintf('Sv/\nSv')};       
    else    
        cunits = {sprintf('Sv/\n[N/m$^2$]'),sprintf('Sv/\n[N/$m^2$]'),...
              sprintf('Sv/\n[W/m$^2$]'), sprintf('Sv/\n[Sv/m$^2$]')};       
    end
    adjField{3}='oceHeat';
    adjField{4}='fwf';
end

% --- Set the time limits
if ~isempty(strfind(runStr,'240mo'))
        tLims = {[205 241], [205 241], [1 241],  [1 241], [1 241], [1 241], [1 241], [1 241], [1 241],[1 241]};
elseif ~isempty(strfind(runStr,'480bwk'))
        tLims={[408 480], [408 480], [1 480], [1 480],  [1 480], [1 480], [1 480], [1 480], [1 480], [1 480], [1 480]};
elseif ~isempty(strfind(runStr,'ad5day')) && timeDir==1
        tLims={[32 74], [32 74], [1 74], [1 74],  [1 74], [1 74], [1 74], [1 74], [1 74], [1 74], [1 74]};
end
% -------------------------------------------------------------------------


% --- 3. Plot and save at various time steps
for i = 1:Nadj
    adjFile = sprintf('%sadj_%s.mat',matDir,adjField{i});
    if exist(adjFile,'file')

        load(adjFile,'adxx');

        % --- Set the time units
        if ~isempty(strfind(runStr,'366day'))
            adxx = gcmfaces_interp_1d(3,[.5:365.5]/366,adxx,[.5:51.5]/52);
        end
        if ~isempty(strfind(runStr,'mo'))
		    if ~isempty(strfind(runStr,'ad5day')) 
                tt='5days';
		    else 
                tt='months';
		    end
        elseif ~isempty(strfind(runStr,'flux'))
            tt='months';
        elseif ~isempty(strfind(runStr,'366day'))
            tt='days';
        elseif ~isempty(strfind(runStr,'five-day'))
            tt='hrs';
        elseif ~isempty(strfind(runStr,'bwk'))
            tt='2 weeks';
        end

        % --- Set the sensitivity 
        if strcmp(adjField{i},'tauu')
          xlbl='$\frac{\partial J}{\partial\tau_x}$';
        elseif strcmp(adjField{i},'tauv')
          xlbl='$\frac{\partial J}{\partial\tau_y}$';
        elseif strcmp(adjField{i},'hflux')
          xlbl='$\frac{\partial J}{\partial Q_{Net}}$, Q$_{Net}$>0 Cools Ocean';
        elseif strcmp(adjField{i},'oceHeat')
          xlbl='$\frac{\partial J}{\partial Q_{Net}}$, Q$_{Net}>0$ Heats Ocean';
        elseif strcmp(adjField{i},'sflux')
          xlbl='$\frac{\partial J}{\partial S}$';
        elseif strcmp(adjField{i},'fwf')
          xlbl='$\frac{\partial J}{\partial FWF}$';
        else
          xlbl=sprintf('$J$() = time mean of final month AMOC\nSensitivity to %s ',adjField{i});
        end

        % --- Save name
        vidName=sprintf('%sadj_%s',figDir,adjField{i});

        % --- Do the area weighting if desired
        if strcmp(areaWeighted,'normByArea')

            fprintf('normalizing by area ...\n');
            xlbl = [xlbl ' (area normalized)'];
            vidName = [vidName '_normedByArea'];

            if ~isempty(strfind(adjField{i},'tauu'))
                adxx = adxx ./ mk3D(mygrid.RAW,adxx);
            elseif ~isempty(strfind(adjField{i},'tauv'))
                adxx = adxx ./ mk3D(mygrid.RAS,adxx);
            else
                adxx = adxx ./ mk3D(mygrid.RAC,adxx);
            end
        end
       
        % --- Set FWF to be Sv / Sv units
        if strcmp(adjField{i},'fwf')
            adxx = adxx.*10^6;
            if strcmp(areaWeighted,'normByArea')
                fprintf(' --- Factoring FWF sensitivity by %s to be Sv/Sv --- \n','10$^6$');
            else
                fprintf(' --- Factoring FWF sensitivity by %s to be Sv/(Sv/m^2) --- \n','10$^6$');
            end
        end

        % --- Colorbrewer2 colormap
        ncolors=7;
        cmap = cbrewer2('RdBu',ncolors);
        cmap = flipdim(cmap,1);

        % --- Ridiculous options structures...
        timeDir = -1;
        strs = struct(...
            'xlbl',xlbl, ...
            'time',tt,...
            'clbl',cunits{i},...
            'vidName',vidName);
        opts = struct(...
            'saveVideo',1,...
            'logFld',1,...
            'mmapOpt',5,...
            'cmap',cmap);
	    if exist('tLims','var'), opts.tLims=deal(tLims{i}); end;
        if ismac, opts.fileFormat=deal('MPEG-4');
                  checkVid= [vidName '.mp4'];
        else,     opts.fileFormat=deal('Motion JPEG 2000');
                  checkVid= [vidName '.avi'];
        end

        % --- Saturate colorbar, removing ~10 points from plots ...
        if strcmp(adjField{i},'oceHeat') && strcmp(areaWeighted,'normByArea')
            c_lim=10^-16;
            opts.c_lim=deal(c_lim);
        elseif strcmp(adjField{i},'fwf') 
            if strcmp(areaWeighted,'normByArea')
                c_lim=10^-1;
                opts.c_lim=deal(c_lim);
            else
                c_lim=10^9;
                opts.c_lim=deal(c_lim);
            end
        end

        %if ~exist(checkVid,'file')
          plotVideo(adxx,strs,opts,mygrid,timeDir);
        %end

        %if makeVidStills
        %  if ~isempty(strfind(runStr,'ad5day'))
        %    ind=[73 69 64 60 50 30];
        %  end
        %  opts.spdim=deal([3 2]);
        %  opts.plotInd = deal(ind);
        %  opts.saveFig = deal(1);
        %  plotManyMaps(adxx,strs,opts,mygrid,timeDir);
        %end

    else
        fprintf('* Skipping %s file ... \n',adjField{i})
    end
end
end
