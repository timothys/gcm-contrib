function [] = plotAmocBolusMht(plotType)
% Plot SAMOC, bolus AMOC, and MHT at -34S
%
% Input:
%
%   plotType: 'full' (default)  = 20 years of AMOC, MHT, and AMOC+bolus
%             'clim' = seasonal cycle of above  
%             'annual' = annually averaged of above  
%             'anomaly' = anomaly to climatology: monthly signal - (detrended) seasonal cycle
%
% ---------------------------------------------------------------------------

slat=-34;
loadDir='mat/eccov4r2/state/';
saveDir = 'figures/eccov4r2/';


[amoc, kmax] = calcAMOC(slat,'eulerian',loadDir);
[amocBolus, kmax_bolus] = calcAMOC(slat,'residual',loadDir);
[mht,kmax_mht] = calcAMOC(slat,'mht',loadDir);

fprintf(' --- Maximizing depths --- \n')
fprintf('amoc: %1.2f\n',mean(kmax));
fprintf('amoc+bolus: %1.2f\n',mean(kmax_bolus));

t=1992+[1:length(amoc)]./12;


% --- Plot AMOC, MHT, AMOC+bolus together
if strcmp(plotType,'full')
  triplePlot(t,amoc,amocBolus,mht,'full',[saveDir 'amoc_bolus_mht_34S']);
  %triplePlotWithDepth(t,amoc,amocBolus,mht,-kmax,'full',[saveDir 'amoc_bolus_mht_34S_withDepth']);
end
printStats(amoc,amocBolus,mht);

% --- Compute seasonal cycle and plot
amoc_anom = amoc - mean(amoc)*ones(1,length(amoc));
bolus_anom = amocBolus - mean(amocBolus)*ones(1,length(amocBolus));
mht_anom = mht - mean(mht)*ones(1,length(mht));

if strcmp(plotType,'clim')
  amoc_clim = calcClimatology(amoc_anom,20);
  bolus_clim = calcClimatology(bolus_anom,20);
  mht_clim = calcClimatology(mht_anom,20);
  triplePlot(1:12,amoc_clim,bolus_clim,mht_clim,'clim',[saveDir 'amoc_bolus_mht_34S_clim']);
end

% --- Compute annual averages and plot
if strcmp(plotType,'annual')
  amoc_annual = calcAnnualMean(amoc_anom,20);
  bolus_annual = calcAnnualMean(bolus_anom,20);
  mht_annual = calcAnnualMean(mht_anom,20);
  triplePlot([1992:2011],amoc_annual,bolus_annual,mht_annual,'annual',[saveDir 'amoc_bolus_mht_34S_annual']);
end

% --- Compute anomaly from seasonal cycle
if strcmp(plotType,'anomaly')
  amoc_clim = calcClimatology(amoc_anom,20);
  bolus_clim = calcClimatology(bolus_anom,20);
  mht_clim = calcClimatology(mht_anom,20);

  amoc_anom = amoc_anom - repmat(amoc_clim,[1 20]);
  bolus_anom = bolus_anom - repmat(bolus_clim,[1 20]);
  mht_anom = mht_anom - repmat(mht_clim,[1 20]);
  
  % Moving to double plot because bolus doesn't look any different here
  doublePlot(t,amoc_anom,mht_anom,'anomaly',[saveDir 'amoc_mht_34S_anomaly']);
  %triplePlot(t,amoc_anom,bolus_anom,mht_anom,'anomaly',[saveDir 'amoc_bolus_mht_34S_anomaly']);
  %triplePlotStdNorm(t,amoc_anom,bolus_anom,mht_anom,'anomaly',[saveDir 'amoc_bolus_mht_34S_anomalyStd']);
  keyboard
end

end

% ---------------------------------------------------------------------------

function [] = doublePlot(t,amoc,mht,plotType,filename)
% plotType is a string and either
%  'full' = with mean flow, monthly values
%  'clim' = seasonal cycle
%  'annual' = annual average
%  'anomaly' = monthly values with mean and seasonal cycle removed

fs = 18;

figure;
set(gcf,'units','normalized','position',[0 0.2 1.2 0.25])
% red and black ... a very particular red
cc=cbrewer2('RdGy',5);
cc=[[0 0 0]; cc(1,:)]; 
axColor = [0.15 0.15 0.15];
[ax,h1,h2]=plotyy(t,amoc,t,mht);


if strcmp(plotType,'full') || strcmp(plotType,'anomaly')
  set(gca,'xtick',[1992:2:2012])
  xlim([1992 2012])
elseif strcmp(plotType,'clim')
    for ii=1:length(ax)
        set(ax(ii),'xlim',[0.5 12.5])
        set(ax(ii),'xtick',[1:12],...
            'xticklabel',{'Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'});
    end
else 
  xlim([1992 2012])
end

% --- Left y axis
set(ax(1),'fontsize',fs);
set(get(ax(1),'ylabel'),'string','Sv','rotation',0,...
    'units','normalized','position',[-.028 0.396 0])
set(ax(1),'YColor',axColor);
set(get(ax(1),'ylabel'),'color',cc(1,:))
set(h1,{'color'},num2cell(cc(1,:),2));

if strcmp(plotType,'full')
    set(ax(1),'ylim',[5 30])
    set(ax(1),'ytick',[5:5:30]);
elseif strcmp(plotType,'anomaly')
    set(ax(1),'ylim',[-6 7])
    set(ax(1),'ytick',[-6:2:6]);
end

% --- Right y axis
set(ax(2),'fontsize',fs);
set(get(ax(2),'ylabel'),'string','PW','rotation',0,...
        'units','normalized','position',[1.035 0.527 0])
set(ax(2),'YColor',axColor);
set(get(ax(2),'ylabel'),'color',cc(2,:))
set(h2,{'color'},num2cell(cc(2,:),2));

if strcmp(plotType,'full')
    set(ax(2),'ylim',[-.25 1])
    set(ax(2),'ytick',[-.25:0.25:1]);
elseif strcmp(plotType,'anomaly')
    set(ax(2),'ylim',[-.45 .45+(1/6)*.45]); %upper ylim to match left axis scale
    set(ax(2),'ytick',[-.45:0.15:.45]);
end

hl=legend('AMOC','MHT','Location','Best');
if strcmp(plotType,'anomaly')
    set(hl,'location','north')
end

% --- saving...
grid on;

savePlot(filename,'pdf',[1 1 1]);
keyboard

end

% ---------------------------------------------------------------------------

function [] = triplePlot(t,amoc,amocBolus,mht,plotType,filename)
% plotType is a string and either
%  'full' = with mean flow, monthly values
%  'clim' = seasonal cycle
%  'annual' = annual average
%  'anomaly' = monthly values with mean and seasonal cycle removed


figureW;
cc=cbrewer2('Dark2',3);
set(gca,'colororder',cc,'nextplot','replacechildren');
[ax,h1,h2]=plotyy(t,amoc,t,mht);
hold on
%cc=lines(7);
plot(t,amocBolus,'Color',cc(3,:))
hold off



if strcmp(plotType,'full') || strcmp(plotType,'anomaly')
  set(gca,'xtick',[1992:2:2012])
  xlim([1992 2012])
elseif strcmp(plotType,'clim')
    for ii=1:length(ax)
        set(ax(ii),'xlim',[0.5 12.5])
        set(ax(ii),'xtick',[1:12],...
            'xticklabel',{'Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'});
    end
else 
  xlim([1992 2012])
end

% --- Left y axis
set(get(ax(1),'ylabel'),'string','Sv','rotation',0)
set(ax(1),'YColor',[0 0 0]);
set(get(ax(1),'ylabel'),'color',cc(1,:))

if strcmp(plotType,'full')
    set(ax(1),'ylim',[5 30])
    set(ax(1),'ytick',[5:5:30]);
elseif strcmp(plotType,'anomaly')
    set(ax(1),'ylim',[-6 7])
    set(ax(1),'ytick',[-6:2:6]);
end

% --- Right y axis
set(get(ax(2),'ylabel'),'string','PW','rotation',0)
set(ax(2),'YColor',[0 0 0]);
set(get(ax(2),'ylabel'),'color',cc(2,:))

if strcmp(plotType,'full')
    set(ax(2),'ylim',[-.25 1])
    set(ax(2),'ytick',[-.25:0.25:1]);
elseif strcmp(plotType,'anomaly')
    set(ax(2),'ylim',[-.45 .45+(1/6)*.45]); %upper ylim to match left axis scale
    set(ax(2),'ytick',[-.45:0.15:.45]);
end

hl=legend('AMOC','AMOC with Bolus transport','MHT','Location','Best');
if strcmp(plotType,'anomaly')
    set(hl,'location','north')
end

% --- saving...
grid on;
saveSlimWidePlot(filename,'pdf');

end

% ---------------------------------------------------------------------------

function [] = triplePlotStdNorm(t,amoc,amocBolus,mht,plotType,filename)
% plotType is a string and either
%  'full' = with mean flow, monthly values
%  'clim' = seasonal cycle
%  'annual' = annual average
%  'anomaly' = monthly values with mean and seasonal cycle removed


figureW;
cc=cbrewer2('Dark2',3);
cmap = [cc(1,:); cc(3,:); cc(2,:)];
set(gca,'colororder',cmap,'nextplot','replacechildren');
plot(t,amoc./std(amoc),t,amocBolus./std(amocBolus),t,mht./std(mht));

if strcmp(plotType,'full') || strcmp(plotType,'anomaly')
  set(gca,'xtick',[1992:2:2012])
  xlim([1992 2012])
elseif strcmp(plotType,'clim')
  xlbl_clim;
else
  xlim([1992 2012])
end

% --- Left y axis
ylabel('Normalized')

hl=legend('AMOC','AMOC with Bolus transport','MHT','Location','Best');
if strcmp(plotType,'anomaly')
    set(hl,'location','north')
end

% --- saving...
grid on;
saveSlimWidePlot(filename,'pdf');

end

% ---------------------------------------------------------------------------

function [] = triplePlotWithDepth(t,amoc,amocBolus,mht,depth,plotType,filename)
% plotType is a string and either
%  'full' = with mean flow, monthly values
%  'clim' = seasonal cycle
%  'annual' = annual average
%  'anomaly' = monthly values with mean and seasonal cycle removed


figureH;
subplot(3,1,1:2)
cc=cbrewer2('Dark2',3);
set(gca,'colororder',cc,'nextplot','replacechildren');
[ax,h1,h2]=plotyy(t,amoc,t,mht);
hold on
%cc=lines(7);
plot(t,amocBolus,'Color',cc(3,:))
hold off

legend('AMOC','AMOC with Bolus transport','MHT','Location','Best')
%legend('AMOC','MHT') %,'Location','Best')

if strcmp(plotType,'full') || strcmp(plotType,'anomaly')
  set(gca,'xtick',[1992:2:2012])
  xlim([1992 2012])
elseif strcmp(plotType,'clim')
  xlbl_clim;
else
  xlim([1992 2012])
end

% --- Left y axis
set(get(ax(1),'ylabel'),'string','Sv')
set(ax(1),'YColor',[0 0 0]);

if strcmp(plotType,'full')
  set(ax(1),'ylim',[5 30])
  set(ax(1),'ytick',[5:5:30]);
end

% --- Right y axis
set(get(ax(2),'ylabel'),'string','PW')
set(ax(2),'YColor',[0 0 0]);

if strcmp(plotType,'full')
  set(ax(2),'ylim',[-.25 1])
  set(ax(2),'ytick',[-.25:0.25:1]);
end
grid on;

% --- depth plot
subplot(3,1,3)
set(gca,'colororder',cc,'nextplot','replacechildren');
plot(t,depth,t,median(depth)*ones(size(t)),'k--');%,'Color',cc(1,:))
ylabel('Maximizing Depth (m)');

legend('AMOC Maximizing Depth','Location','Southeast');

if strcmp(plotType,'full') || strcmp(plotType,'anomaly')
  set(gca,'xtick',[1992:2:2012])
  xlim([1992 2012])
elseif strcmp(plotType,'clim')
  xlbl_clim;
else
  xlim([1992 2012])
end
 

% --- saving...
grid on;
savePlot(filename);

end

% ---------------------------------------------------------------------------

function [] = printStats(amoc,amocBolus,mht)
% Print some stats
r_squared=corrcoef(amoc,mht);
r_squared_bolus = corrcoef(amocBolus,mht);

fprintf('r^2 between amoc & mht: %.4f \n',r_squared(2));
fprintf('r^2 between amoc + bolus & mht: %.4f \n',r_squared_bolus(2));
fprintf('mean +/- std amoc: %.4f +/- %.4f\n',mean(amoc),std(amoc));
fprintf('mean +/- std amoc+bolus: %.4f +/- %.4f\n',mean(amocBolus),std(amocBolus));
fprintf('mean +/- std mht: %.4f +/- %.4f\n',mean(mht),std(mht));
end


