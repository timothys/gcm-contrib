function [] = plotSamocPaper()
% Purpose is to generate all plots for the SAMOC paper ...


modelVar = 'eccov4r2';
atmVar = 'long-atm';
lat=-34;
condenseVars=1;
tau_mem=229; %months
separator='southek';


% This is out of date...
plotAmocBolusMht('full');

% For this plot manually edit:
%  ../matlab/plots/plotVideo.m so that
%    1. colorbar fontsize for horizontal colorbar (colorbarFlag==2)
%       hc.FontSize=22;
%    2. Set ppi to 600 in savePlot
%plotBigSensFig();
plotExpVar(modelVar,atmVar,lat,'full',condenseVars);
plotExpVar(modelVar,atmVar,lat,'clim',condenseVars);
plotStackRec({'tauu','tauv','precip','runoff','warm','aqh'},...
                modelVar,atmVar,tau_mem,separator,'clim');
plotExpVar(modelVar,atmVar,lat,'annual',condenseVars);
plotStackRec({'tauu','tauv','precip'},modelVar,atmVar,tau_mem,separator,'annual');
plotHfluxDivergence(modelVar,atmVar);

% --- copy to master folder
samocDir = 'figures/samoc-manuscript';
if ~exist(samocDir,'dir'), mkdir(samocDir); end;

fig=cell(14,1);

fig{1}=sprintf('figures/%s/amoc_bolus_mht_34S.png',modelVar);
%figs 2 & 3 handled via latex ...
fig{2}=sprintf('figures/reconstruct.34S/%s/%s/full/exp_var_full.png',modelVar,atmVar);
fig{3}=sprintf('figures/reconstruct.34S/%s/%s/full/exp_var_clim.png',modelVar,atmVar);
fig{4}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/tauu/tauu_clim_condensed.png',modelVar,atmVar);
fig{5}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/tauv/tauv_clim_condensed.png',modelVar,atmVar);
fig{6}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/precip/precip_clim_condensed.png',modelVar,atmVar);
fig{7}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/runoff/runoff_clim_condensed.png',modelVar,atmVar);
fig{8}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/warm/warm_clim_condensed.png',modelVar,atmVar);
fig{9}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/aqh/aqh_clim_condensed.png',modelVar,atmVar);
fig{10}=sprintf('figures/reconstruct.34S/%s/%s/full/exp_var_annual.png',modelVar,atmVar);
fig{11}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/tauu/tauu_annual_condensed.png',modelVar,atmVar);
fig{12}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/tauv/tauv_annual_condensed.png',modelVar,atmVar);
fig{13}=sprintf('figures/reconstruct.34S/%s/%s/region-rec/precip/precip_annual_condensed.png',modelVar,atmVar);
fig{14}=sprintf('figures/reconstruct.34S/%s/%s/full/hflux_divergence.png',modelVar,atmVar);


for i=1:length(fig)
  copyfile(fig{i},samocDir);
end

end
