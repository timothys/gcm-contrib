function [] = plotTauuClim()

modelVar = 'eccov4r2';
atmVar = 'short-atm';

mygrid=establish_mygrid('llc90');

matFile = sprintf('mat/exf/%s/%s/exf_tauu.mat',modelVar,atmVar);
load(matFile,'exf_fld');

plotDir = 'figures/exf';
if ~exist(plotDir,'dir'), mkdir(plotDir); end;
plotDir=sprintf('%s/%s',plotDir, modelVar);
if ~exist(plotDir,'dir'), mkdir(plotDir); end;
plotDir=sprintf('%s/%s',plotDir, atmVar);
if ~exist(plotDir,'dir'), mkdir(plotDir); end;


% --- compute climatology and 3mo avg
Nmo = size(exf_fld.f1,3);
Nyrs = Nmo/12;
exf_fld=convert2gcmfaces(exf_fld);
exf_fld=exf_fld - repmat(nanmean(exf_fld,3),[1 1 Nmo]);
exf_fld=convert2gcmfaces(exf_fld);
tauu_clim = calcClimatology(exf_fld,Nyrs);
tauu_3mo = 0*tauu_clim(:,:,1:4);
for i=1:4
    tauu_3mo(:,:,i) = nanmean(tauu_clim(:,:,(i-1)*3+1:i*3),3);
end


% --- Plot up 3mo avg
fs=22;
lblStr = {'DJF','MAM','JJA','SON'};
cmap=flipdim(cbrewer2('BrBG',9),1);
%cmax=nanmax(abs(tauu_3mo));
cmax = 0.135;
dc = 0.03;
c_lim = cmax - dc/2;
figureW;
for i=1:4
    m_map_atl(tauu_3mo(:,:,i),5,{'fontSize',fs},{'myCmap',cmap});
    caxis([-cmax cmax]);
    hl=niceColorbar;
    hl.Ticks=[-c_lim:dc:c_lim];
    ylabel(hl,'N/m$^2$','rotation',0,'fontsize',fs);
    set(get(hl,'ylabel'),'position',[4, 0.54 0]);
    xlabel(sprintf('%s Climatology\n%s Avg.','$\delta\tau_x$',lblStr{i}),'fontsize',fs);
    savePlot(sprintf('%s/tauu_%s_clim',plotDir,lblStr{i}),'png',[1 1 1]);
end

% --- Plot up clim 
lblStr = {'Jan','Feb','March','April','May','June','July','Aug','Sept','Oct','Nov','Dec'};
cmap=flipdim(cbrewer2('BrBG',9),1);
%cmax=nanmax(abs(tauu_clim));
figureW;
for i=1:12
    m_map_atl(tauu_clim(:,:,i),5,{'fontSize',fs},{'myCmap',cmap});
    caxis([-cmax cmax]);
    hl=niceColorbar;
    hl.Ticks=[-c_lim:dc:c_lim];
    ylabel(hl,'N/m$^2$','rotation',0,'fontsize',fs);
    set(get(hl,'ylabel'),'position',[4 0.54 0]);
    xlabel(sprintf('%s Climatology\n%s Avg.','$\delta\tau_x$',lblStr{i}),'fontsize',fs);
    savePlot(sprintf('%s/tauu_%s_clim',plotDir,lower(lblStr{i})),'png',[1 1 1]);
end

end
