function [] = runBaroTest(mygrid)
% Want to perform samoc reconstruction with different indices on omitting barotropic response

modelVar='eccov4r2';
atmVar='flux';

for i=1:2
	n=int8(i);
        dirString = sprintf('rm%dmo',n);
	samocReconstructionByMonth(modelVar,atmVar,-34,mygrid,n,dirString);
	plotSamocReconstructionByMonth(modelVar,atmVar,-34,mygrid,dirString);
	
	close all
end

end
