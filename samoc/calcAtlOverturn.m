function [atlOvStf,atlBolusOv,atlResOv,atlTrVolZon,atlMht] = calcAtlOverturn(saveDir,loadDir,diagsName);
% Compute Atlantic overturning stream function 
% from [U,V]VELMASS diagnostic output.
%
%  Inputs: 
% 	
%       saveDir (optional) : where to save atlOverturn.mat from
%                            default: mat/eccov4r2/state/
%       loadDir (optional) : where to pull UVELMASS, VVELMASS from
%                               default: ../../results/eccov4r2/ref/diags
%       diagsName (optional) : name of diagnostics file where [U,V]VELMASS recs exist
%                               default: trsp_3d_set1
%
%  Outputs:
%
%	atlOvStf : [Nlat x Nr x Nt] vector 
%              overturning stream function at each 
%              latitude, depth, time record of U/V data
%
%   atlBolusOv: same, include bolus transport
%
%   atlResOv: atlOvStf+atlBolusOv
%
%   atlTrVolZon: same structure, giving zonal transport at each depth level
%
%   atlMht: same structure, giving meridional heat transport at latitude
%
% ---------------------------------------------------------------------------------

global mygrid;
if nargin<1, saveDir = 'mat/eccov4r2/state/'; end;
if nargin<2, loadDir = '../../results/eccov4r2/ref/diags/'; end;
if nargin<3, diagsName = 'trsp_3d_set1'; end;

Ny = length(mygrid.LATS_MASKS);
Nr = length(mygrid.DRF);

% --- For diagnostic output, grab iter numbers of meta/data files
[iters] = grabAllIters( loadDir, diagsName );
Nt = length(iters);

%% Loop through each month of 20 yr period
% Atlantic basin and prep fields
amocBasin = v4_basin('atlExt');
amocBasin = mk3D(amocBasin,mygrid.mskC);

atlOvStf = zeros(Ny,Nr+1,Nt);
atlBolusOv = zeros(Ny,Nr+1,Nt);
atlResOv = zeros(Ny,Nr+1,Nt);
atlTrVolZon = zeros(Ny,Nr+1,Nt);
atlMht = zeros(Ny,Nt);

fprintf('Note: For now, calcAtlOverturn ignores bolus velocity and MHT...\n');

for n = 1:Nt
    % Load velocities
    %  from ekman:/workspace/results/eccov4r2/ref/diags/state_3d_set1*
    % 'UVELMASS' 'VVELMASS' 'WVELMASS' 'GM_PsiX ' 'GM_PsiY ' 
    uVel = rdmds2gcmfaces([loadDir diagsName],iters(n),'rec',1);
    vVel = rdmds2gcmfaces([loadDir diagsName],iters(n),'rec',2);

    %GM_PsiX = rdmds2gcmfaces([loadDir diagsName],iters(n),'rec',4);
    %GM_PsiY = rdmds2gcmfaces([loadDir diagsName],iters(n),'rec',5);

    % Load advection of temperature and salinity
    %  from ekman:/workspace/results/eccov4r2/ref/diags/state_3d_set2*
    % 'DFxE_TH ' 'DFyE_TH ' 'ADVx_TH ' 'ADVy_TH ' 'DFxE_SLT' 'DFyE_SLT' 'ADVx_SLT' 'ADVy_SLT'   
    %DFxE_TH = rdmds2gcmfaces([loadDir 'trsp_3d_set2'],iters(n),'rec',1);
    %DFyN_TH = rdmds2gcmfaces([loadDir 'trsp_3d_set2'],iters(n),'rec',2);
    %ADVx_TH = rdmds2gcmfaces([loadDir 'trsp_3d_set2'],iters(n),'rec',3);
    %ADVy_TH = rdmds2gcmfaces([loadDir 'trsp_3d_set2'],iters(n),'rec',4);
    %uTh = DFxE_TH + ADVx_TH;
    %vTh = DFyN_TH + ADVy_TH;

    %% Compute bolus velocity
    %[uBolus,vBolus,~] = calc_bolus(GM_PsiX,GM_PsiY);
    
    % Mask and compute residual
    uVel = uVel.*mygrid.mskW.*amocBasin; vVel = vVel.*mygrid.mskS.*amocBasin;
    % uBolus=uBolus.*mygrid.mskW.*amocBasin; vBolus=vBolus.*mygrid.mskS.*amocBasin;
    % uRes = uVel + uBolus; vRes = vVel + vBolus;
    % uTh = uTh.*mygrid.mskW.*amocBasin; 
    % vTh = vTh.*mygrid.mskS.*amocBasin;
    
    % Compute overturning stream functions
    atlOvStf(:,:,n) = calc_overturn(uVel,vVel,1);
    %atlBolusOv(:,:,n)=calc_overturn(uBolus,vBolus);
    %atlResOv(:,:,n)=calc_overturn(uRes,vRes);

    % Compute meridional heat transport
    wattsToPetaWatts=1e-15;
    densityTimesHeatCapacity = 4e6; % mimicking gael's standard analysis
    fact = wattsToPetaWatts*densityTimesHeatCapacity;
    %atlMht(:,n)=fact*calc_MeridionalTransport(uTh,vTh,0);

    if mod(n,10) == 0
      fprintf(' --- calcAtlOverturn: done %d / %d --- \n',n,Nt);
    end
end

% Compute volumetric transport at depth
atlTrVolZon(:,Nr+1,:) = -atlOvStf(:,Nr,:);
for k = Nr:-1:1
    atlTrVolZon(:,k,:) = -(atlOvStf(:,k,:) - atlOvStf(:,k+1,:));
end

% Remove latitudes that don't make sense
iy=find(mygrid.LATS<-35|mygrid.LATS>70);
atlOvStf(iy,:,:) = NaN;
atlBolusOv(iy,:,:) = NaN;
atlResOv(iy,:,:) = NaN;
atlTrVolZon(iy,:,:) = NaN;
atlMht(iy,:) = NaN;

atlOvStf(atlOvStf==0)=NaN;
atlBolusOv(atlBolusOv==0)=NaN;
atlResOv(atlResOv==0)=NaN;
atlTrVolZon(atlTrVolZon==0)=NaN;
atlMht(atlMht==0)=NaN;

save(sprintf('%satlOverturn.mat',saveDir),'atlOvStf','atlBolusOv','atlResOv','atlTrVolZon','atlMht')  
end
