function [] = verifyPartitionTest(mygrid)
% My plots aren't working or something ... 

adjFields=set_adjField( 'flux' );
load('mat/reconstruct.34S/flux_region_rec.mat');
load('mat/reconstruct.34S/basin_rec.mat');
load('mat/reconstruct.34S/monthly_flux/fullReconstruction.mat');

msks=createRegionMasks('ekman');
Nt=240;
tMem=229;
t=[1992+1/12:1/12:2012];

for i=1:length(adjFields)

  sumBasin=tropicRec{i}(tMem,:) + subTropicRec{i}(tMem,:) + regionRec.atl.ek{i}(tMem,:) + southRec{i}(tMem,:) + ...
           regionRec.acc{i}(tMem,:) + regionRec.arc{i}(tMem,:) + ... 
           regionRec.pac.subPolar{i}(tMem,:) + regionRec.atl.subPolar{i}(tMem,:);

  sumAtl = regionRec.atl.subPolar{i}(tMem,:) +regionRec.atl.NStrop{i}(tMem,:) +regionRec.atl.Ntrop{i}(tMem,:) + ...
           regionRec.atl.Strop{i}(tMem,:) +regionRec.atl.SStrop{i}(tMem,:) +regionRec.atl.ek{i}(tMem,:) +...
           regionRec.atl.south{i}(tMem,:); 

  sumPac = regionRec.pac.subPolar{i}(tMem,:) +regionRec.pac.NStrop{i}(tMem,:) +regionRec.pac.Ntrop{i}(tMem,:) +...
           regionRec.pac.Strop{i}(tMem,:) +regionRec.pac.SStrop{i}(tMem,:) +...
           regionRec.pac.south{i}(tMem,:); 

  sumInd = regionRec.ind.NStrop{i}(tMem,:) +regionRec.ind.Ntrop{i}(tMem,:) +...
           regionRec.ind.Strop{i}(tMem,:) +regionRec.ind.SStrop{i}(tMem,:) +...
           regionRec.ind.south{i}(tMem,:); 

  sumRegion=regionRec.acc{i}(tMem,:) + regionRec.arc{i}(tMem,:) + ...
            sumAtl + sumPac + sumInd;
            
  figureW
  subplot(3,1,1),plot(t,samoc_adjFields{i}(tMem,:),...
       t, sumBasin,...
       t, sumRegion)

  subplot(3,1,2),plot(t,samoc_adjFields{i}(tMem,:)-sumBasin,...
       t, samoc_adjFields{i}(tMem,:)-sumRegion)

  subplot(3,1,2),plot(t,samoc_adjFields{i}(tMem,:)-sumBasin-regionRec.bar{i}(tMem,:),...
       t, samoc_adjFields{i}(tMem,:)-sumRegion-regionRec.bar{i}(tMem,:))

end


allMsks=msks.acc+msks.arc+...
        msks.atl.subPolar+msks.atl.NStrop+msks.atl.Ntrop+...
        msks.atl.Strop+msks.atl.SStrop+msks.atl.ek+msks.atl.south+...
        msks.pac.subPolar+msks.pac.NStrop+msks.pac.Ntrop+...
        msks.pac.Strop+msks.pac.SStrop+msks.pac.south+...
        msks.ind.NStrop+msks.ind.Ntrop+...
        msks.ind.Strop+msks.ind.SStrop+msks.ind.south;

barents=v4_basin('barents');
figureW
subplot(2,2,1),m_map_atl(mygrid.mskC(:,:,1)-allMsks-barents,2)
subplot(2,2,2),m_map_atl(mygrid.mskC(:,:,1)-allMsks,3)
subplot(2,2,3),m_map_atl(mygrid.mskC(:,:,1)-allMsks-barents,5);

keyboard

end
