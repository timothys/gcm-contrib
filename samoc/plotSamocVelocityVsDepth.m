function [] = plotSamocVelocityVsDepth()
% Plot some quantities showing the velocity at 34S to depth
%
% --------------------------------------------------------

mygrid=establish_mygrid('llc90');
read_dir = '../../release2/nctiles_monthly/';
mat_dir = 'mat/eccov4r2/'; if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_dir = [mat_dir 'state/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_file = [mat_dir 'samoc_vel_vs_depth.mat']; 
plot_dir = 'figures/eccov4r2/'; if ~exist(plot_dir,'dir'), mkdir(plot_dir); end;
plot_dir = [plot_dir 'state/']; if ~exist(plot_dir,'dir'), mkdir(plot_dir); end;

Nt = 240;

if ~exist(mat_file,'file')
    
    % Do this to get number of grid points on latitude band
    uvel = read_nctiles([read_dir 'UVELMASS/UVELMASS'],'UVELMASS',1);
    vvel = read_nctiles([read_dir 'VVELMASS/VVELMASS'],'VVELMASS',1);
    [~,latV,~,latXV,latR] = grabAtlLatBand_uv(-34,uvel,vvel);
    Nx = length(latXV);
    Nr = length(latR);

    % Initialize container
    samoc_vel = zeros(Nr,Nx,Nt);
    samoc_vel(:,:,1) = latV;

    % Read and fill
    for n=2:Nt
        uvel = read_nctiles([read_dir 'UVELMASS/UVELMASS'],'UVELMASS',n);
        vvel = read_nctiles([read_dir 'VVELMASS/VVELMASS'],'VVELMASS',n);
    
        [~,latV] = grabAtlLatBand_uv(-34,uvel,vvel);
        samoc_vel(:,:,n)=latV;
    
        if mod(n,10)==0
            fprintf(' --- Done %d / %d --- \n',n,Nt);
        end
    end
    mean_samoc_vel=nanmean(samoc_vel,3);
    std_samoc_vel=nanstd(samoc_vel,0,3);
    save(mat_file,'samoc_vel','mean_samoc_vel','std_samoc_vel','latXV','latR');
else
    load(mat_file,'samoc_vel','mean_samoc_vel','std_samoc_vel','latXV','latR');
    Nx = size(samoc_vel,2);
    Nr = size(samoc_vel,1);
end

% --- Plot it up
cmap = flipdim(cbrewer2('RdBu',9),1);

figureW; 
contourf(latXV,latR,mean_samoc_vel);
xlabel('Avg Meridional Velocity at 34$^\circ$S, 1992-2011');
colormap(cmap);
cmax = nanmax(nanmax(abs(mean_samoc_vel),[],1),[],2);
caxis([-cmax cmax])
ch=niceColorbar;
ylabel(ch,'m/s','rotation',0);
savePlot([plot_dir 'avg_samoc_vel_vs_depth'],'png');

cmap = cbrewer2('YlGnBu',9);

figureW;
contourf(latXV,latR,log10(std_samoc_vel));
xlabel('$\log_{10}$ Standard Deviation of Meridional Velocity at 34$^\circ$S, 1992-2011');
colormap(cmap);
ch=niceColorbar;
ylabel(ch,'$\log_{10}($m/s)','rotation',0);
savePlot([plot_dir 'std_samoc_vel_vs_depth'],'png');
keyboard
end
