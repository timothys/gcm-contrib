function [ ] = samocReconstruction(lat,mygrid)
% Want to reconstruct AMOC at 34S from adjoint sensitivities
%
% -------------------------------------------------------------------------

%% Set up dirs
% lat = -34;
dirs = establish_samocDirs;
if nargin<1,establish_mygrid;end
if lat < 0, dirName = sprintf('%dS',abs(lat)); else dirName=sprintf('%dN',lat);end;
saveDir = sprintf('%sreconstruct.%s/',dirs.mat,dirName); 
plotSaveDir = sprintf('%sreconstruct.%s/',dirs.figs,dirName); 
if ~exist(saveDir,'dir'), mkdir(saveDir);end;
if ~exist(plotSaveDir,'dir'), mkdir(plotSaveDir);end;

%% Step 0: Pre processing 
Nt = 240;
adjField = {'tauu','tauv','hflux','sflux'}; %'aqh','atemp','swdown','lwdown','precip','runoff','hflux','sflux'};
Nadj = length(adjField);

if lat==-34, runStr='dec.240mo/'; else runStr=sprintf('dec.240mo.%s/',dirName);end;

if exist(sprintf('%s%s',dirs.results,runStr),'dir')
    postProcess_adxx(adjField, 10^-6,1,runStr,dirs,mygrid);
end


%% First, grab cost function from forward model
[samoc] = calcAMOC(lat,'eulerian',mygrid);

% Forward cost function, variability
samoc_for_mean= samoc - mean(samoc);

%% Now go through forcing files and sensitivities
Nmem = Nt;
samoc_rec  = zeros(Nmem,Nt);
samoc_adjFields = cell(Nadj,1);
samoc_tmp = zeros(Nt,Nt);

for i = 1:Nadj
    %% Initialize fields for saving
    samoc_adjFields{i} = zeros(Nt,Nt);
    
    %% Load sensitivity
    adjFile = sprintf('%s%sadj_%s.mat',dirs.mat,runStr,adjField{i});
    forceFile = sprintf('%sxx_%s.mat',saveDir,adjField{i});

    if exist(adjFile,'file')
        load(adjFile,'adxx');
        
        if exist(forceFile,'file')
            load(forceFile,'xx_fld');
        else
            xx_fld=prepForcingFile(forceFile,mygrid);
        end        
        
        %% Prep forcing files
        xx_fldMean = mean(xx_fld,3);
        xx_fld = xx_fld - repmat(xx_fldMean,[1 1 Nt]);
        xx_fld = cat(3,0*xx_fld(:,:,1:Nt-1),xx_fld); % now 3rd dim is 480 long
        
        %% Prep sensitivity file
        adxx=adxx(:,:,1:Nt);
        
        % Perform reconstruction for particular forcing
        for n = 1:Nt
            tmp1 = xx_fld(:,:,n+[0:Nt-1]).*adxx;
            samoc_tmp(1:Nt,n) = squeeze(nansum(nansum(convert2gcmfaces(tmp1),1),2));
            % Flip direction for sum going backward in time
            samoc_tmp(1:Nt,n) = samoc_tmp(Nt:-1:1,n);
        end
        
        % Keep track of sensitivity to particular field
        samoc_adjFields{i} = cumsum(samoc_tmp,1);
        
        % Depending on adj field, add to total reconstruction
        samoc_rec = samoc_rec + samoc_adjFields{i};
        
        fprintf('* Done with %s sensitivity ... \n',adjField{i});
    else
        fprintf('File: %s doesn''t exist, skipping ...\n',adjFile);
        
    end
end

save(sprintf('%sfullReconstruction.mat',saveDir),'samoc_adjFields','samoc_rec','samoc_for_mean'); 

plotSamocReconstruction(lat,mygrid);
end
