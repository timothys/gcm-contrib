function [] = plotSamocReconstruction(lat,mygrid)

dirs = establish_samocDirs;
if lat < 0, dirName = sprintf('%dS',abs(lat)); else dirName=sprintf('%dN',lat);end;
saveDir = sprintf('%sreconstruct.%s/',dirs.mat,dirName); 
plotSaveDir = sprintf('%sreconstruct.%s/',dirs.figs,dirName); 
if ~exist(plotSaveDir,'dir'), mkdir(plotSaveDir); end;
adjField = {'tauu','tauv','hflux','sflux'}; %,'aqh','atemp','swdown','lwdown','precip','runoff','hflux','sflux'};
Nadj = length(adjField);
% mem = 12*[.5 1 5 10 15 20];
Nmem = 240; %length(mem);
Nt=240;

fullFile = sprintf('%sfullReconstruction.mat',saveDir);
load(fullFile);

ind = 12; 


 for i = 1:Nadj
     
     % Plot reconstruction from particular variable
%      figureW;
%      plot(1:Nt,samoc_for_mean,'k',1:Nt,samoc_adjFields{i}(41,:));
%      xlabel('Months')
%      ylabel('\deltaJ(t)')
%      title(sprintf('Variability attribution to %s',adjField{i}))
%      set(gcf,'paperorientation','landscape')
%      set(gcf,'paperunits','normalized')
%      set(gcf,'paperposition',[0 0 1 1])
%      saveas(gcf,sprintf('%s%s_reconstruct',plotSaveDir,adjField{i}),'pdf')
%     
%     normErr = zeros(1,Nt);
% %     if i==1
%     for n=1:Nt, normErr(n) = norm(samoc_for_mean - samoc_adjFields{i}(n,:)); end;
% %     else
% %     tmpRec = zeros(1,Nt);
% %     for j=1:i-1, tmpRec = tmpRec + samoc_adjFields{j}(16,:); end;
% %     for n=1:Nt, normErr(n) = norm(tmpRec - samoc_adjFields{i}(n,:)); end;
% %     end
%     figureW;
%     plot(1:Nt,normErr)
%     xlabel('Memory in months')
%     ylabel('||\deltaJ(t)_{forward} - \deltaJ(t)_{reconstruct}||_2')
%     title(sprintf('Norm of Error forward model vs. %s',adjField{i}));
%     set(gcf,'paperorientation','landscape')
%     set(gcf,'paperunits','normalized')
%     set(gcf,'paperposition',[0 0 1 1])
% %     saveas(gcf,sprintf('%snorm_error_%s',plotSaveDir,adjField{i}),'pdf')
% %     keyboard
%     
 end

%% Full reconstruction
figureW
plot(1:Nt,samoc_for_mean,1:Nt,samoc_rec(ind,:));
xlabel('Months')
ylabel('\deltaJ(t)')
title(sprintf('AMOC Variability Reconstruction'))
legend('Forward Model Output','Adjoint Reconstruction')
set(gca,'xtick',[12:12:240])
xlim([0 240])
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
 saveas(gcf,sprintf('%sfull_reconstruct_mean_flux',plotSaveDir),'pdf')
 
%% Wind stress reconstruction
figureW;
windRec = samoc_adjFields{1}+samoc_adjFields{2};
plot(1:Nt,samoc_for_mean,1:Nt,windRec(ind,:));
    xlabel('Months')
    ylabel('\deltaJ(t)')
    legend('Forward Model Output','Wind stress reconstruction')
set(gca,'xtick',[12:12:240])
xlim([0 240])
grid on
% subplot(2,1,2),plot(1:Nt,abs(samoc_for_mean - windRec(41,:)),1:Nt,abs(samoc_for_mean-samoc_rec(41,:)))
%     xlabel('Months')
%     ylabel('|\deltaJ(t) - \deltaJ(t)_{rec}|')
%     legend('Abs. Error, wind only','Abs. Error, all fluxes')
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%swind_reconstruct',plotSaveDir),'pdf')
    

normErr_flux = zeros(Nmem,1);
normErr_wind = zeros(Nmem,1);
for n=1:Nmem
    normErr_flux(n) = norm(samoc_for_mean - samoc_rec(n,:));
    normErr_wind(n) = norm(samoc_for_mean - windRec(n,:));
end

figureW;
plot(1:Nt,normErr_flux,1:Nt,normErr_wind)
ylabel('||J''(t)_{forward} - J''(t)_{reconstruct}||_2')
xlabel('Memory in Months')
set(gca,'xtick',[12:12:240])
xlim([0 240])
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%snorm_error',plotSaveDir),'pdf')


% compute 1 sided FT of windRec error
windErr = samoc_for_mean-windRec(ind,:);
freq = (0:Nt/2-1)'/240 * 12; % Frequency in 1/yr
ff = fft(windErr,Nt);
freq_windErr = ff(1:Nt/2)/Nt;
freq_windErr(2:end) = 2*freq_windErr(2:end);

% Remove higher frequencies 
freq_windErr_low = ff;
freq_windErr_low(41:Nt-39)=zeros(size(freq_windErr_low(41:Nt-39)));
freq_windErr_low(21) = 0;
freq_windErr_low(Nt-19)=0;
windErr_low = ifft(freq_windErr_low);

% Plot wind reconstruction error and low pass filtered version
figureW;
semilogx(freq,abs(freq_windErr));
xlabel('Frequency [1/yr]')
ylabel('Amplitude');
title('Frequency domain of wind reconstruction error')
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%sfreq_fwd_minus_wind',plotSaveDir),'pdf');

figureW;
plot(1:Nt,windErr,1:Nt,real(windErr_low))
xlabel('Months')
ylabel('\deltaJ(t) - \deltaJ(t)_{Wind}')
title('Forward model - wind reconstruction')
legend('Full error','Low pass & season filtered')
set(gca,'xtick',[12:12:240])
xlim([0 240])
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%sfwd_minus_wind',plotSaveDir),'pdf');

keyboard
end
