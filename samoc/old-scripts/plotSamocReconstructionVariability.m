function [] = plotSamocReconstructionVariability(lat,mygrid)

dirs = establish_samocDirs;
if lat < 0, dirName = sprintf('%dS',abs(lat)); else dirName=sprintf('%dN',lat);end;
saveDir = sprintf('%sreconstruct.%s/variability/',dirs.mat,dirName); 
plotSaveDir = sprintf('%sreconstruct.%s/variability/',dirs.figs,dirName); 
if ~exist(plotSaveDir,'dir'),mkdir(plotSaveDir);end;
adjField = {'tauu','tauv','atemp','aqh','lwdown','swdown','precip','runoff'}; %,'hflux','sflux'};
monthVect = {'Jan','Feb','March','April','May','June','July','Aug','Sept','Oct','Nov','Dec'};
Nadj = length(adjField);

Nt=240;
Nmem = 240;

% Load the goods
fullFile = sprintf('%sfullReconstruction.mat',saveDir);
load(fullFile); %,'samoc_adjFields','samoc_rec_atemp','samoc_rec_flux','samoc_for_mean');

memInd=12;

samocrec2=cell(1,3);
for k=1:3
    samocrec2{k}=zeros(Nt,Nt);
end

 
% Now look at contribution from each forcing individually 
for i = 1:Nadj
    
%     adjFile = sprintf('%s%s_reconstruction.mat',saveDir,adjField{i});
%     load(adjFile,'samoc_adjFields','xx_fld','cumsamoc_adj','samoc_for_mean');
    
    % Plot reconstruction from particular variable
    for k=1:3
        samoc_adjFields{i,k}(samoc_adjFields{i,k}==0)=NaN;
    end
    figureW;
    plot(1:Nt,samoc_for_mean,'k',1:Nt,samoc_adjFields{i,2}(memInd,:),'b-',1:Nt,samoc_adjFields{i,1}(memInd,:),'b:',1:Nt,samoc_adjFields{i,3}(memInd,:),'b:');
    xlabel('Months')
    ylabel('J''(t)')
    title(sprintf('Variability attribution to %s',adjField{i}))
%     legend('J''_{Forward}','\tau_{mem} = 6mo','\tau_{mem} = 1yr',...
%         '\tau_{mem} = 5yr','\tau_{mem} = 10yr','\tau_{mem} = 15yr','\tau_{mem} = 20yr',...
%         'location','bestoutside')
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])
    saveas(gcf,sprintf('%s%s_reconstruct',plotSaveDir,adjField{i}),'pdf')
    
    for k=1:3
        samoc_adjFields{i,k}(isnan(samoc_adjFields{i,k}))=0;
        samocrec2{k}=samocrec2{k}+samoc_adjFields{i,k};
    end
%     figureW;
%     plot(1:Nt,abs(samoc_adjFields{i,2}(memInd,:) - samoc_adjFields{i,1}(memInd,:))./abs(samoc_adjFields{i,2}(memInd,:)))
%     xlabel('Months')
%     ylabel('|dJ_{clim+std} - dJ_{clim}|/|dJ_{clim}|')
%     set(gcf,'paperorientation','landscape')
%     set(gcf,'paperunits','normalized')
%     set(gcf,'paperposition',[0 0 1 1])
%     saveas(gcf,sprintf('%s%s_clim_relErr',plotSaveDir,adjField{i}),'pdf')
    
end


figureW
plot(1:Nt,samoc_for_mean,'k',1:Nt,samocrec2{2}(memInd,:),'b-',1:Nt,samocrec2{1}(memInd,:),'b:',1:Nt,samocrec2{3}(memInd,:),'b:');
xlabel('Months')
ylabel('J''(t)')
title(sprintf('AMOC Variability Reconstruction'))
legend('Forward Model Output','Adjoint Reconstruction')
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%sfull_reconstruct_mean_flux',plotSaveDir),'pdf')

% normErr_atemp = zeros(Nmem,1);
% normErr_flux = zeros(Nmem,1);
% 
% for n=1:Nmem
%     normErr_atemp(n) = norm(samoc_for_mean - samoc_rec_atemp(n,:));
%     normErr_flux(n) = norm(samoc_for_mean - samoc_rec_flux(n,:));
% end
% 
% figure
% bar(normErr_atemp)
% ylabel('||J''(t)_{forward} - J''(t)_{reconstruct}||_2')
% set(gca,'XTickLabel',{'\tau = 20yr','\tau = 15yr','\tau = 10yr','\tau = 5yr',...
%     '\tau = 1yr','\tau = 6mo'});
% set(gcf,'paperorientation','landscape')
% set(gcf,'paperunits','normalized')
% set(gcf,'paperposition',[0 0 1 1])
% saveas(gcf,sprintf('%snorm_error_atemp',plotSaveDir),'pdf')
% 
% figure
% bar(normErr_flux)
% ylabel('||J''(t)_{forward} - J''(t)_{reconstruct}||_2')
% set(gca,'XTickLabel',{'\tau = 20yr','\tau = 15yr','\tau = 10yr','\tau = 5yr',...
%         '\tau = 1yr','\tau = 6mo'});
% set(gcf,'paperorientation','landscape')
% set(gcf,'paperunits','normalized')
% set(gcf,'paperposition',[0 0 1 1])
% saveas(gcf,sprintf('%snorm_error_atemp',plotSaveDir),'pdf')
    
% adjFile = sprintf('%s%s_reconstruction.mat',saveDir,adjField{1});
% load(adjFile,'samoc_adjFields','xx_fld','cumsamoc_adj','samoc_for_mean');
% 
% figureW
% plot(1:Nt,abs(samoc_rec_flux(1,:) - samoc_adjFields(1,:)))
% xlabel('Months')
% ylabel('|J''(t)_{reconstruction} - J''(t)_{Ekman}|')
% title('Absolute Error: Full Reconstruction vs Ekman')
% set(gcf,'paperorientation','landscape')
% set(gcf,'paperunits','normalized')
% set(gcf,'paperposition',[0 0 1 1])
% saveas(gcf,sprintf('%sekman_abs_error',plotSaveDir),'pdf')

% First lets get a look at the standard deviation & mean in each of the
% climatologies
%for i = 1:Nadj
%    for m = 1:12
%        figureW;
%        m_map_atl(xx_clim{i}(:,:,m),5);
%        xlabel(sprintf('%s Climatology, %s',monthVect{m},adjField{i}))
%        
%        keyboard
%        m_map_atl(sig_anom{i}(:,:,m),5);
%        xlabel(sprintf('%s Standard Deviation, %s',monthVect{m},adjField{i}))
%        keyboard
%    end
%end
end
