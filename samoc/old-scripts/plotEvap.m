function [] = plotHfluxPerturb(mygrid)

expName = '';
expNameSave = strrep(expName,'.','_');
%resultsDir = '/workspace/gcmpack/results/hflux/';
resultsDir = '~/Downloads/hflux/';
resultsP10 = [resultsDir expName 'p1.20yr/diags/'];
if ~exist(resultsP10,'dir'), pcheck=0; else pcheck=1; end;
resultsM10 = [resultsDir expName 'm1.20yr/diags/'];
resultsRef = [resultsDir 'ref/diags/'];
matDir = 'mat/hf-perturb/';
plotDir = 'figures/hf-perturb/';
if ~exist(matDir,'dir'), mkdir(matDir); end
if ~exist(plotDir,'dir'), mkdir(plotDir); end
fName = 'external_forcing';

%% External forcing
% 'oceTAUX ' 'oceTAUY ' 'oceQnet ' 'oceFWflx' 'EXFtaux ' 'EXFtauy ' 'EXFqnet ' 'EXFempmr' 
% 'EXFatemp' 'EXFaqh  ' 'EXFlwdn ' 'EXFswdn ' 'EXFevap ' 'EXFpreci' 'EXFroff '

%% ----------------------------------------------------------------------------------------
%% 1. Load evap response to see how response differs perturb-ref 
tind=120;
recInd=10;
evap_m1 = rdmds2gcmfaces([resultsM10 fName],NaN,'rec',recInd);
evap_p1 = rdmds2gcmfaces([resultsP10 fName],NaN,'rec',recInd);
evap_ref = rdmds2gcmfaces([resultsRef fName],NaN,'rec',recInd);

%% ----------------------------------------------------------------------------------------
%% 2. Split response by region 
msks=createRegionMasks(mygrid);
Nt = size(evap_m1.f1,3);
evapDiff_m1 = zeros(1,Nt);
evapDiff_p1 = zeros(1,Nt);
evapDiff_acc=evapDiff_m1;
evapDiff_arc=evapDiff_m1;
evapDiff_ind=evapDiff_m1;
evapDiff_pac=evapDiff_m1;
evapDiff_atl=evapDiff_m1;
evapDiff_north=evapDiff_m1;
for n=1:Nt
	evapDiff_m1(n) = nansum(evap_m1(:,:,n)-evap_ref(:,:,n));
	evapDiff_acc(n) = nansum(msks.acc.*(evap_m1(:,:,n)-evap_ref(:,:,n)));
	evapDiff_arc(n) = nansum(msks.arc.*(evap_m1(:,:,n)-evap_ref(:,:,n)));
	evapDiff_ind(n) = nansum(msks.ind.*(evap_m1(:,:,n)-evap_ref(:,:,n)));
	evapDiff_pac(n) = nansum(msks.pac.*(evap_m1(:,:,n)-evap_ref(:,:,n)));
	evapDiff_atl(n) = nansum(msks.atl.*(evap_m1(:,:,n)-evap_ref(:,:,n)));
	evapDiff_north(n) = nansum(msks.north.*(evap_m1(:,:,n)-evap_ref(:,:,n)));
end

%% ----------------------------------------------------------------------------------------
%% 3. Plot Heat Flux perturbation

figureW;
t=[1:Nt]./12;
plot(t,evapDiff_acc,t,evapDiff_arc,t,evapDiff_ind,...
	t,evapDiff_pac,t,evapDiff_atl,t,evapDiff_north)
%hold on; 
%if pcheck, plot(t,evapDiff_p1,'b-'),end;
%hold off
xlabel('Years')
if recInd==13 
	ylabel(sprintf('%sEvap_{perturb} - %sEvap_{ref} (Sv)','\Sigma','\Sigma'))
elseif recInd==10
	ylabel(sprintf('%saqh_{perturb} - %saqh_{ref} (Sv)','\Sigma','\Sigma'))
else
	ylabel(sprintf('%svar_{perturb} - %svar_{ref} (Sv)','\Sigma','\Sigma'))
end
legend('acc','arc','ind','pac','atl','north',...
	'Location','Best')
set(gca,'xtick',[1:20])
xlim([0 20])
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
if recInd==13
  plotName=sprintf('%sevapDiff_%s',plotDir,expNameSave);
elseif recInd==10
  plotName=sprintf('%saqhDiff_%s',plotDir,expNameSave);
else
  plotName=sprintf('%svarDiff_%s',plotDir,expNameSave);
end
saveas(gcf,plotName,'pdf')

keyboard
end

