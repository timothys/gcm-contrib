function [] = plotSamocReconstructionBwk(atmVars,lat,mygrid,varargin)

%% 0. Get directories etc ready
if strcmp(atmVars,'atm')
	adjField = {'tauu','tauv','aqh','atemp','swdown','lwdown','precip','runoff'};
elseif strcmp(atmVars,'flux')
	adjField = {'tauu','tauv','hflux','sflux'};
else 
	error('** Error: atmVars option %s not recognized, later dude ...\n',atmVars)
end

monthStr = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
Nt = 480;
Nmem=457;
Nadj = length(adjField);
Nmo = length(monthStr);
t=[1:Nt]./24;
tMem=[1:Nmem]./24;

%% Sift through varargs
for n=1:length(varargin)
	if isa(varargin{n},'char'), dirString=varargin{n};
	else fprintf('** Don''t recognize the %dth varargin, type help samocReconstructionByMonth for options and how to assign\n Except only pay attention to dirString...\n',n);
	     return;
	end

end

%% Set up dirs
if lat < 0, dirName = sprintf('%dS',abs(lat)); else dirName=sprintf('%dN',lat);end;
mainSaveDir=['mat/reconstruct.' dirName '/'];
mainPlotDir = ['figures/reconstruct.' dirName '/'];
if exist('dirString','var')
    saveDir = [mainSaveDir 'monthly_' dirString '_' atmVars '/']; 
    plotSaveDir = [mainPlotDir 'monthly_' dirString '_' atmVars '/'];
else
    saveDir = [mainSaveDir 'monthly_' atmVars '/']; 
    plotSaveDir = [mainPlotDir 'monthly_' atmVars '/'];
end

if ~exist(saveDir,'dir'), fprintf('Can''t find mat files, exiting ...\n');return;end;
if ~exist(mainPlotDir,'dir'), mkdir(mainPlotDir); end;
if ~exist(plotSaveDir,'dir'), mkdir(plotSaveDir);end;

%% Load results
fullFile = sprintf('%sfullReconstruction.mat',saveDir);
load(fullFile);

%% This is just a guess on optimal memory time
ind = 456; 
% -------------------------------------------------------------------------

%% 1. Plot reconstruction for each variable individually 
for i = 1:4
     
    % Plot reconstruction from particular variable
    figureW;
    samoc_adjFields{i}(samoc_adjFields{i}==0) = NaN;
      plot(t,samoc_adjFields{i}(2*[12,60,120,180,229],:),'*');
    xlabel('Years')
    ylabel('\deltaJ(t)')
	ylim([-9,9])
    legend('\tau_{mem}=1 yr','\tau_{mem}=5 yrs','\tau_{mem}=10 yrs',...
	   '\tau_{mem}=15 yr','\tau_{mem}=20 yrs','Location','Best')
    title(sprintf('Variability attribution to %s',adjField{i}))
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])
    set(gca,'xtick',[1:20])
    xlim([0 20])
    grid on
    saveas(gcf,sprintf('%s%s_reconstruct',plotSaveDir,adjField{i}),'pdf')

    samoc_adjFields{i}(isnan(samoc_adjFields{i})) = 0;
 end
% -------------------------------------------------------------------------

%% 2. Full reconstruction
samoc_rec(samoc_rec==0)=NaN;
 
figureW;
windRec = samoc_adjFields{1}+samoc_adjFields{2};
mhfRec = windRec + samoc_adjFields{4};
fullRec= mhfRec+samoc_adjFields{3};
plot(t,samoc_for_mean,t,windRec(ind,:),t,mhfRec(ind,:),t,fullRec(ind,:));
    xlabel('Years')
    ylabel('\deltaJ(t)')
    legend('Forward Model','Wind Stress Reconstruction','Wind + FWF Reconstruction','Full','Location','Best')
set(gca,'xtick',[1:20])
xlim([0 20])
grid on
%subplot(2,1,2),plot(1:Nt,abs(samoc_for_mean - windRec(ind,:)),1:Nt,abs(samoc_for_mean-samoc_rec(ind,:)))
%    xlabel('Months')
%    ylabel('|\deltaJ(t) - \deltaJ(t)_{rec}|')
%    legend('Abs. Error, wind only','Abs. Error, all fluxes')
%set(gca,'xtick',[12:12:240])
%xlim([0 240])
%grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%sfull_reconstruct',plotSaveDir),'pdf')
% -------------------------------------------------------------------------

%% 3. Error in reconstructions
normErr = zeros(Nmem,1);
normErr_wind = zeros(Nmem,1);
normErr_mhf = zeros(Nmem,1);
for n=1:Nmem
    normErr(n) = norm(samoc_for_mean - fullRec(n,:));
    normErr_wind(n) = norm(samoc_for_mean - windRec(n,:));
    normErr_mhf(n) = norm(samoc_for_mean - mhfRec(n,:));
end

figureW;
plot(tMem,normErr,tMem,normErr_wind,tMem,normErr_mhf)
ylabel('||\deltaJ(t)_{Fwd} - \deltaJ(t)_{R}||_2')
xlabel('Memory in Years')
legend('All components','Wind','Wind + FWF')
set(gca,'xtick',[1:20])
xlim([0 20])
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%snorm_error',plotSaveDir),'pdf')

% compute 1 sided FT of windRec error
windErr = samoc_for_mean-windRec(ind,:);
mhfErr =  samoc_for_mean-mhfRec(ind,:);
fullErr = samoc_for_mean-fullRec(ind,:);

freq = (0:Nt/2-1)'/Nt * 24; % Frequency in 1/yr
ffw = fft(windErr,Nt);
ffmhf = fft(mhfErr,Nt);
fffull = fft(fullErr,Nt);
freq_windErr = ffw(1:Nt/2)/Nt;
freq_windErr(2:end) = 2*freq_windErr(2:end);
freq_mhfErr = ffmhf(1:Nt/2)/Nt;
freq_mhfErr(2:end) = 2*freq_mhfErr(2:end);
freq_fullErr = fffull(1:Nt/2)/Nt;
freq_fullErr(2:end) = 2*freq_fullErr(2:end);

% Plot wind reconstruction error and low pass filtered version
figureW;
semilogx(freq,abs(freq_windErr),freq,abs(freq_mhfErr),freq,abs(freq_fullErr));
xlabel('Frequency [1/yr]')
ylabel('Amplitude');
legend('Wind','Wind + FWF','full')
%title('Frequency domain of wind reconstruction error')
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%sfreq_err',plotSaveDir),'pdf');

%% Remove higher frequencies 
%freq_windErr_low = ffw;
%freq_windErr_low(41:Nt-39)=zeros(size(freq_windErr_low(41:Nt-39)));
%freq_windErr_low(21) = 0;
%freq_windErr_low(Nt-19)=0;
%windErr_low = ifft(freq_windErr_low);

%figureW;
%plot(t,windErr,t,real(windErr_low))
%xlabel('Years')
%ylabel('\deltaJ(t) - \deltaJ(t)_{Wind}')
%title('Forward model - wind reconstruction')
%legend('Full error','Low pass & season filtered')
%set(gca,'xtick',[1:20])
%xlim([0 20])
%grid on
%set(gcf,'paperorientation','landscape')
%set(gcf,'paperunits','normalized')
%set(gcf,'paperposition',[0 0 1 1])
%saveas(gcf,sprintf('%sfwd_minus_wind',plotSaveDir),'pdf');

end
