function [] = warmVsColdPlot( mygrid )
% Plot the contribution to variability from warm and cold water paths
% Warm path: agulhas leakage / benguela current, reconstructed
%            from forcing over Indian ocean N of 45S
% Cold path: ACC/Drake Passage/Malvinas Confluence reconstructed from
%            forcing over Pacific south of 45S

fullrecfile='mat/reconstruct.34S/monthly_flux/fullReconstruction.mat';
matfile = 'mat/reconstruct.34S/flux_region_rec.mat';
matfile2= 'mat/reconstruct.34S/basin_rec.mat';
mainPlotDir = 'figures/reconstruct.34S/region-rec/warmVsCold/';
if ~exist(mainPlotDir,'dir'), mkdir(mainPlotDir); end;
plotFile = [mainPlotDir 'warmVsColdPlot'];

load(matfile,'regionRec')
msks=createSamocMasks(mygrid);
if ~exist(matfile2,'file'), calcPartitionTest(mygrid); end;
load(matfile2)
load(fullrecfile,'samoc_adjFields');

mskNames = fieldnames(msks);
Nt=240;
%tMem = [1,2,3,4,6,12,24,60,120,180,229];
%tMem=[1,3,5,12,60,72,84,96,108,120,132,144,156,168,180,192,204,216,229];
tMem=[4,229];
t=[1992+1/12:1/12:2012];


%figureL;
%[ha,pos]=tight_subplot(9,1,[.02 .03],[.05 .02],[.1 .1]);

warmPath=0*regionRec.pac.SStrop{1}(tMem,:);
coldPath=warmPath;

for i=1:4
  if i~=3
  warmPath = warmPath + regionRec.ind.SStrop{i}(tMem,:) + ...
             0*regionRec.ind.Strop{i}(tMem,:);
  coldPath = coldPath + regionRec.acc{i}(tMem,:) + ...
             regionRec.pac.south{i}(tMem,:);
  end
end

figureW;
plot(t,warmPath,t,coldPath)
  legend('Warm Path, \tau_{mem} = 4 months',...
         'Warm Path, \tau_{mem} = 20 years',...
         'Cold Path, \tau_{mem} = 4 months',...
         'Cold Path, \tau_{mem} = 20 years',...
         'Location','Best')


  ylabel('Sv')
  xlim([1992 2012])
  grid on



set(gcf,'paperorientation','portrait')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])

saveas(gcf,plotFile,'pdf')

end
