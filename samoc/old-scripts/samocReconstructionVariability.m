function [ ] = samocReconstructionVariability(lat,mygrid)
% Want to reconstruct AMOC at 34S from adjoint sensitivities
%
% -------------------------------------------------------------------------

%% Set up dirs
dirs = establish_samocDirs;
if nargin<1,establish_mygrid;end
if lat < 0, dirName = sprintf('%dS',abs(lat)); else dirName=sprintf('%dN',lat);end;
saveDir = sprintf('%sreconstruct.%s/variability/',dirs.mat,dirName); 
plotSaveDir = sprintf('%sreconstruct.%s/variability/',dirs.figs,dirName); 
if ~exist(saveDir,'dir'), mkdir(saveDir);end;
if ~exist(plotSaveDir,'dir'), mkdir(plotSaveDir);end;

%% Step 0: Pre processing 
Nt = 240;
monthStr = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
adjField = {'tauu','tauv','atemp','aqh','lwdown','swdown','precip','runoff'}; %,'hflux','sflux'};
Nadj = length(adjField);

for m = 1:12
    if lat==-34, runStr=sprintf('%s.240mo/',monthStr{m});
    else runStr=sprintf('%s.240mo.%s/',monthStr{m},dirName);
    end;
    if exist(sprintf('%s%s',dirs.results,runStr),'dir')
        postProcess_adxx(adjField, 10^-6,1,runStr,dirs,mygrid);
    end
end

%% First, grab cost function from forward model
[samoc] = calcAMOC(lat,'eulerian',mygrid);

% Forward cost function, variability
samoc_for_mean= samoc - mean(samoc);

%% Now go through forcing files and sensitivities
Nmem = Nt;
samoc_rec = cell(1,3); %zeros(Nmem,Nt);
samoc_rec_adj = cell(1,3);
samoc_adjFields = cell(Nadj,3);
samoc_tmp = zeros(Nt,Nt);

for k = 1:3
    samoc_rec_adj{k} = zeros(Nmem,Nt);
    samoc_rec{k} = zeros(Nmem,Nt);
end
 
for i = 1:Nadj
    %% Load sensitivity
    forceFile = sprintf('%s../xx_%s.mat',saveDir,adjField{i});
    
    if exist(forceFile,'file')
        load(forceFile,'xx_fld');
    else
        xx_fld=prepForcingFile(forceFile,mygrid);
    end

    %% Prep forcing files
    xx_fldMean = mean(xx_fld,3);
    xx_fld = xx_fld - repmat(xx_fldMean,[1 1 Nt]);

    % Compute monthly climatology on variability
    xx_clim = repmat(0*xx_fldMean,[1 1 12]);
    sig_anom = 0*xx_clim;
    for m=1:12
        xx_clim(:,:,m) = nanmean(xx_fld(:,:,[m:12:240]),3);
        sig_anom(:,:,m) = nanstd(xx_fld(:,:,[m:12:240]),[],3);
    end
        
    %% Prep sensitivity file
    for m=1:12
        %% Determine relevant time period for sensitivity file
        Ntad = Nt + m - 12;
        runStr = sprintf('%s.240mo/',monthStr{m});
        adjFile = sprintf('%s%sadj_%s.mat',dirs.mat,runStr,adjField{i});
        if exist(adjFile,'file')
            load(adjFile,'adxx');
            adxx=adxx(:,:,1:Ntad);
    
            ss = [ -1 0 1];
            
            for k = 1:3
                %% Initialize fields for saving only in first month, otherwise already initialized
                if m==1, samoc_adjFields{i,k} = zeros(Nt,Nt); end;
                
                xx_fld = xx_clim + ss(k)*sig_anom; % xx_fld now 12 months long
                xx_fld = repmat(xx_fld, [1 1 20]); % xx_fld now 240 months long
                xx_fld = xx_fld(:,:,1:Ntad); % Remove excess months dep. on end
                xx_fld = cat(3,0*xx_fld(:,:,1:Ntad-1),xx_fld); % now 3rd dim is 2*Ntad long
                
                % Perform reconstruction for particular forcing
                for n = m:12:Nt
                    tmp1 = xx_fld(:,:,n+[0:Ntad-1]).*adxx;
                    samoc_tmp(1:Ntad,n) = squeeze(nansum(nansum(convert2gcmfaces(tmp1),1),2));
                    % Flip direction for sum going backward in time
                    samoc_tmp(1:Ntad,n) = samoc_tmp(Ntad:-1:1,n);
                end
                % Keep track of sensitivity to particular field
                % calculate accumulating sum
                samoc_adjFields{i,k}(1:Ntad,m:12:Nt) = cumsum(samoc_tmp(1:Ntad,m:12:Nt),1);
                

                
            end % k=1:3
        else
            fprintf('File: %s doesn''t exist, skipping ...\n',adjFile);
            
        end

    end % m=1:12

    % Depending on adj field, add to total reconstruction
    samoc_rec{k} = samoc_rec{k} + samoc_adjFields{i,k};
            
    fprintf('* Done with %s sensitivity ... \n',adjField{i});
        
        save(sprintf('%s%s_reconstruction.mat',saveDir,adjField{i}),...
            'samoc_adjFields','xx_clim','sig_anom','samoc_for_mean')


end
% end

save(sprintf('%sfullReconstruction.mat',saveDir),'samoc_adjFields','samoc_rec','samoc_for_mean','sig_anom','xx_clim');

plotSamocReconstructionVariability(lat,mygrid);
end
