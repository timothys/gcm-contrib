function [samoc_hf] = loadReconstructedAmoc(matFile,hfFile,mygrid)

if ~exist(matFile)
%% Step 0: Pre processing
dirs=establish_samocDirs;
adjField = {'tauu','tauv','hflux','sflux'};
monthStr = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
Nt = 240;
Nadj = length(adjField);
Nmo = length(monthStr);

%% Make sure sensitivity files are post processed to mat files
for m = 1:12
    runStr=sprintf('%s.240mo/',monthStr{m});
    if ~exist(['mat/' runStr 'adj_hflux.mat'])
	fprintf('Need to postprocess adxx files, exiting ...\n');
	return;
    end
end

%% Now go through forcing files and sensitivities
samoc_tmp = zeros(Nt,Nt);
samoc_hf = zeros(Nt,Nt);
%samoc_adjFields = cell(Nadj,1);

for i = 3
    samoc_adjFields{i} = zeros(Nt,Nt);

    for m = 1:Nmo

        %% Load sensitivity
        runStr = sprintf('%s.240mo/',monthStr{m});
        adjFile = sprintf('%s%sadj_%s.mat',dirs.mat,runStr,adjField{i});
        forceFile = sprintf('%s/reconstruct.34S/xx_%s.mat',dirs.mat,adjField{i});
        
        if exist(adjFile,'file')
            load(adjFile,'adxx');

	    if strcmp(adjField{i},'hflux')
              xx_fld = rdmds2gcmfaces(hfFile,NaN,'rec',7);
	    elseif exist(forceFile,'file')
	      load(forceFile,'xx_fld');
	    else
	       xx_fld = prepForcingFile(forceFile,mygrid);
	    end
            
            %% Determine relevant time period for sensitivity file
            Ntad = Nt + m - 12;
            
            %% Prep forcing files
            xx_fldMean = mean(xx_fld,3);
            xx_fld = xx_fld(:,:,1:Ntad) - repmat(xx_fldMean,[1 1 Ntad]);
            xx_fld = cat(3,0*xx_fld(:,:,1:Ntad-1),xx_fld); % now 3rd dim is 480 long
            
            %% Prep sensitivity file
            adxx=adxx(:,:,1:Ntad);
            
            % Perform reconstruction for particular forcing
            for n = m:12:Nt
                tmp1 = xx_fld(:,:,n+[0:Ntad-1]).*adxx;
                samoc_tmp(1:Ntad,n) = squeeze(nansum(nansum(convert2gcmfaces(tmp1),1),2));
                % Flip direction for sum going backward in time
                samoc_tmp(1:Ntad,n) = samoc_tmp(Ntad:-1:1,n);
            end
            
            % Keep track of sensitivity to particular field
	    samoc_adjFields{i}(1:Ntad,m:12:Nt) = cumsum(samoc_tmp(1:Ntad,m:12:Nt),1);
           % samoc_hf(1:Ntad,m:12:Nt) = cumsum(samoc_tmp(1:Ntad,m:12:Nt),1);
            
        else
            fprintf('File: %s doesn''t exist, skipping ...\n',adjFile);
        end % if adxx file exists
    end % for m=1:Nmo

    samoc_hf = samoc_hf + samoc_adjFields{i};
    fprintf('* Done with %s sensitivity ... \n',adjField{i});
    

end % for 1:Nadj

%  % Now load the rest of the field reconstructions
%  load('mat/reconstruct.34S/monthly_atm/fullReconstruction.mat','samoc_rec','samoc_adjFields');
%  
%  
%  % Now add to full reconstruction
%  samoc_rec = samoc_rec - samoc_adjFields{3}+; 


save(matFile,'samoc_hf','samoc_adjFields');
else
load(matFile,'samoc_hf','samoc_adjFields');
end % if matFile doesn't exist

end % end of subroutine
