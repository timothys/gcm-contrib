function [ ] = samocReconstructionBwk(atmVars,lat,mygrid,varargin)
% Want to reconstruct AMOC at 34S from adjoint sensitivities
%
%	atmVars: ['atm' or 'flux']select to reconstruct with atmospheric state or 
%		 computed fluxes. 
%	lat: denotes latitude for computing reconstruction
%	mygrid : yup
%
%	varargin:
%	  hfluxMsk: mask for partitioning hflux reconstruction
%		gcmfaces type
%
%	  baroInd: must be an integer! Denotes removing tf-n -> tf from reconst.
%	 	assign with n=int8(n);
%
%	  dirString: name for specifying save location of experiment, beyond monthly/flux/atm
%		e.g. monthly_"nobaro"_flux denotes no barotropic response ... no quotes though
%
% -------------------------------------------------------------------------


%% Step 0: Pre processing
if strcmp(atmVars,'atm')
	adjField = {'tauu','tauv','aqh','atemp','swdown','lwdown','precip','runoff'};
elseif strcmp(atmVars,'flux')
	adjField = {'tauu','tauv','hflux','sflux'};
else 
	error('** Error: atmVars option %s not recognized, later dude ...\n',atmVars)
end



%% Sift through varargs
for n=1:length(varargin)
	if isa(varargin{n},'gcmfaces'), hfluxMsk=varargin{n};
	elseif isa(varargin{n},'integer'), baroInd=varargin{n};
	elseif isa(varargin{n},'char'), dirString=varargin{n};
	else fprintf('** Don''t recognize the %dth varargin, type help samocReconstructionByMonth for options and how to assign\n',n);
	     return;
	end
end

monthStr = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
Nt = 480;
recPerYr = 24;
Nadj = length(adjField);
Nmo = length(monthStr);

%% Set up dirs
if lat < 0, dirName = sprintf('%dS',abs(lat)); else dirName=sprintf('%dN',lat);end;
mainSaveDir=['mat/reconstruct.' dirName '/'];
if exist('dirString','var')
    saveDir = [mainSaveDir 'monthly_' dirString '_' atmVars '/']; 
else
    saveDir = [mainSaveDir 'monthly_' atmVars '/']; 
end

if ~exist(mainSaveDir,'dir'), mkdir(mainSaveDir); end;
if ~exist(saveDir,'dir'), mkdir(saveDir);end;

%% 2. Prep for reconstruction
%% First, grab cost function from forward model
[samoc] = calcAMOC(lat,'eulerian',mygrid);

% Forward cost function, variability
samoc_for_mean= samoc - mean(samoc);

%% Now go through forcing files and sensitivities
doReconstruction = 1;
samoc_rec = zeros(Nt,Nt);
samoc_adjFields = cell(Nadj,1);
fld_rec = zeros(Nt,Nt);
samoc_tmp = zeros(Nt,Nt);
% -------------------------------------------------------------------------

%% 3. Perform reconstruction
for i = 1:Nadj
    %% Initialize fields for saving
    samoc_adjFields{i} = zeros(Nt,Nt);

    for m = 1:Nmo
        %% Load sensitivity
        adjFile = sprintf('mat/%s.480bwk/adj_%s.mat',monthStr{m},adjField{i});
	forceFile = sprintf('%s../hf/xx_%s.mat',saveDir,adjField{i});

        if exist(adjFile,'file')
            
            for bwkOffset=-1:0

              load(adjFile,'adxx');
              load(forceFile,'xx_fld');

              %% Determine relevant time period for sensitivity file
              Ntad = Nt + 2*m+bwkOffset - recPerYr;

              if Ntad ~= 480
              %% Prep forcing files
              xx_fldMean = mean(xx_fld,3);
              xx_fld = xx_fld(:,:,1:Ntad) - repmat(xx_fldMean,[1 1 Ntad]);
              xx_fld = cat(3,0*xx_fld(:,:,1:Ntad-1),xx_fld); % now 3rd dim is 2*Ntad-1 long

              %% Prep sensitivity file
              adxx=adxx(:,:,1:Ntad);

	      %% Optional: remove barotropic component
	      if strcmp(adjField{i},'hflux') && exist('baroInd','var')
	          baroInd=double(baroInd);
	          adxx(:,:,Ntad-baroInd:Ntad) = 0*adxx(:,:,Ntad-baroInd:Ntad);
	          %xx_fld(:,:,2*Ntad-baroInd-1:2*Ntad-1)=0*xx_fld(:,:,2*Ntad-baroInd-1:2*Ntad-1);
	      end

	      %% Optional: remove greenland area
	      if strcmp(adjField{i},'hflux') && exist('hfluxMsk','var')
	          adxx=adxx.*repmat(hfluxMsk,[1 1 Ntad]);
	      end
              
              % Perform reconstruction for particular forcing
              st=2*m+bwkOffset;
              stp=Nt-1;
              for n = st:recPerYr:stp
                try
                  tmp1 = xx_fld(:,:,n+[0:Ntad-1]).*adxx;
                catch me
                        keyboard
                end
                  samoc_tmp(1:Ntad,n) = squeeze(nansum(nansum(convert2gcmfaces(tmp1),1),2));
                  % Flip direction for sum going backward in time
                  samoc_tmp(1:Ntad,n) = samoc_tmp(Ntad:-1:1,n);
              end
              
              % Keep track of sensitivity to particular field
              samoc_adjFields{i}(1:Ntad,st:recPerYr:stp) = cumsum(samoc_tmp(1:Ntad,st:recPerYr:stp),1);

              end % if Ntad~=480
            end % bwkoffset
	
        else
            fprintf('File: %s doesn''t exist, skipping ...\n',adjFile);
        end
    end %for 1:Nmo

    % Now add to full reconstruction
    samoc_rec = samoc_rec + samoc_adjFields{i}; 
    fld_rec = samoc_adjFields{i};
    fprintf('* Done with %s sensitivity ... \n',adjField{i});

    save([saveDir adjField{i} '.mat'],'fld_rec');
end %for 1:Nadj

save(sprintf('%sfullReconstruction.mat',saveDir),'samoc_adjFields','samoc_for_mean','samoc_rec');
end
