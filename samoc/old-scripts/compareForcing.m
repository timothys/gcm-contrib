function [] = compareForcing(mygrid)
% Want to test the difference between using oceTAUX & exf_tauu field

%% Set up dirs
dirs = establish_samocDirs;
if nargin<1,establish_mygrid;end
saveDir = [dirs.mat 'reconstruct.34S/'];
if ~exist(saveDir,'dir'), mkdir(saveDir);end;

%% First, grab cost function from forward model
slat = -34; 
[samoc] = calcAMOC(slat,'eulerian',mygrid);

% Forward cost function, variability
samoc_for = samoc - mean(samoc); 

%% Now go through forcing files and sensitivities
Nt = 240;
adjField = {'tauu','tauv'};
monthFields = {'jan','feb','march','april','may','june','july','aug','sept',...
               'oct','nov','dec'};
Nadj = length(adjField); 
Nmo = length(monthFields);

samoc_adjFields = cell(Nadj,1);
samoc_adjTot = zeros(Nadj,Nt);
samoc_adjTot2=samoc_adjTot;

for i = 1:Nadj
    % Initialize fields for saving
    samoc_adjFields{i} = zeros(Nt,Nt);
    
    for m = 1:Nmo
        runStr = sprintf('%s.240mo/',monthFields{m});
        if exist(sprintf('%s/%s',dirs.mat,runStr),'dir')
            % Load the files
            adjFile = sprintf('%s%sadj_%s.mat',dirs.mat,runStr,adjField{i});
            forceFile = sprintf('../calc/xx_%s.mat',adjField{i});
            
            xx_taux = read_nctiles('../../release2/nctiles_monthly/oceTAUX/oceTAUX');
            xx_tauy = read_nctiles('../../release2/nctiles_monthly/oceTAUY/oceTAUY');
            
            for n=1:Nt
                [xx_taux(:,:,n),xx_tauy(:,:,n)]=calc_UEVNfromUXVY(xx_taux(:,:,n),xx_tauy(:,:,n));
            end
            
            if strcmp('tauu',adjField{i})
                xx2 = xx_taux;
            elseif strcmp('tauv',adjField{i})
                xx2 = xx_tauy;
            end
            load(adjFile,'adxx');
            load(forceFile,'xx_fld');
            
            if m==12
                % Compare forcing fields
                absErr = abs(xx2 - xx_fld);
                relErr = absErr./abs(xx2);
                meanRelErr = nanmean(relErr,3);
                logRelErr= convert2gcmfaces(log(convert2gcmfaces(meanRelErr)));
                
                figureW;
                m_map_atl(logRelErr,5);
                title('log of rel error in 20 yr mean of exf vs diag')
                xlabel(sprintf('Forcing: %s',adjField{i}))
            end
            
            % Prep vector: Compute mean and add space to front for fast
            % convolution
            Ntad = 20*12; 
            stp = Nt;
            beg = stp - Ntad+1;
            adxx = adxx(:,:,beg:stp);
            xx_fldMean = nanmean(xx_fld,3);
            xx_fld = xx_fld - repmat(xx_fldMean,[1 1 Nt]);
            xx_fld = cat(3,NaN*xx_fld(:,:,1:Ntad),xx_fld); % now 3rd dim is 480 long
            
            % -- comparing to diagnostics output
            xx2Mean = nanmean(xx2,3);
            xx2 = xx2 - repmat(xx2Mean,[1 1 Nt]);
            xx2 = cat(3,NaN*xx2(:,:,1:Ntad),xx2);
            xx2=-xx2;
            % -------
            
            samoc_adj = zeros(Nt,Nt);
            samoc2 = samoc_adj;
            
            % Perform reconstruction for particular forcing
            for n = m:12:Nt
                tmp1 = xx_fld(:,:,n+[0:Ntad-1]).*adxx;
                tmp2 = xx2(:,:,n+[0:Ntad-1]).*adxx;
                samoc_adj(1:Ntad,n) = squeeze(nansum(nansum(convert2gcmfaces(tmp1),1),2));
                samoc2(1:Ntad,n) = squeeze(nansum(nansum(convert2gcmfaces(tmp2),1),2));
            end
            
            samoc_adjTot(i,:) = samoc_adjTot(i,:) + sum(samoc_adj,1);
            samoc_adjTot2(i,:) = samoc_adjTot(i,:) + sum(samoc2,1);
        end
    end
    
    
    figure
plot(1:Nt,samoc_adjTot,1:Nt,samoc_adjTot2,'--')
xlabel('Months')
ylabel('J''(t)')
title(sprintf('AMOC Variability Reconstruction'))

recAbsErr = abs(samoc_adjTot - samoc_adjTot2);
recRelErr = recAbsErr./abs(samoc_adjTot2);
figure
semilogy(1:Nt,recRelErr);
    xlabel('months')
    ylabel('rel error due to exf vs diag')
    
end
                