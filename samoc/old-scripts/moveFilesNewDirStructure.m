function [] = moveFilesNewDirStructure()

% Moving files to exf_<> ... 


mainDir = 'mat/exf';
models = {'eccov4r2','eig'};
atmVars = {'long-atm','short-atm','flux'};

adjField = {'tauu','tauv','atemp','aqh','lwdown','swdown','precip','runoff','hflux','sflux'};

for i=1:length(models)
  for j=1:length(atmVars)

    matDir = sprintf('%s/%s/%s',mainDir,models{i},atmVars{j});

    for k=1:length(adjField)
      old_matfile = sprintf('%s/xx_%s.mat',matDir,adjField{k});
      new_matfile = sprintf('%s/exf_%s.mat',matDir,adjField{k});

      if exist(old_matfile,'file')
    %    load(old_matfile,'xx_fld');
    %    exf_fld=xx_fld;
    %    fprintf('Saving file: %s ...\n',new_matfile);
    %    save(new_matfile,'exf_fld');
        delete(old_matfile)
      end
    end
  end
end
end
