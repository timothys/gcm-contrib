function [] = bigTauuPlot( mygrid )
% Make reconstruction plot for attributing variability to 
% zonal windstress only.

fullrecfile='mat/reconstruct.34S/monthly_flux/fullReconstruction.mat';
matfile = 'mat/reconstruct.34S/long-atm-ecco_region_rec.mat';
matfile2= 'mat/reconstruct.34S/basin_rec.mat';
mainPlotDir = 'figures/reconstruct.34S/region-rec/tauu/';
if ~exist(mainPlotDir,'dir'), mkdir(mainPlotDir); end;
plotFile = [mainPlotDir 'bigTauuPlot'];

load(matfile,'regionRec','msks');
if ~exist(matfile2,'file'), calcPartitionTest(mygrid); end;
load(matfile2)
load(fullrecfile,'samoc_adjFields');

mskNames = fieldnames(msks);
Nt=240;
%tMem = [1,2,3,4,6,12,24,60,120,180,229];
%tMem=[1,3,5,12,60,72,84,96,108,120,132,144,156,168,180,192,204,216,229];
tMem=[3*12,19*12];
t=[1992+1/12:1/12:2012];


plotDir=[mainPlotDir 'tauu/']; if ~exist(plotDir,'dir'),mkdir(plotDir);end;

figureL;
[ha,pos]=tight_subplot(9,1,[.02 .03],[.05 .02],[.1 .1]);

% --- Total tauu
axes(ha(1)); plot(t,samoc_adjFields{1}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',[-6:6:6],'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([-6 6])
  grid on

% --- Ekman
axes(ha(2)); plot(t,regionRec.atl.ek{1}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',[-6:6:6],'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([-6 6])
  grid on

lo=-1.5;
hi=1.5;
tt=1.5;
v=[lo:tt:hi];

% --- Tropical Pacific
axes(ha(3)); plot(t,diff(regionRec.pac.Ntrop{1}(tMem,:)+regionRec.pac.Strop{1}(tMem,:),1,1))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([lo hi])
  grid on

% --- Tropical Atlantic
axes(ha(4)); plot(t,regionRec.atl.Ntrop{1}(tMem,:)+regionRec.atl.Strop{1}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([lo hi])
  grid on

% --- Tropical Indian
axes(ha(5)); plot(t,regionRec.ind.Ntrop{1}(tMem,:)+regionRec.ind.Strop{1}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([lo hi])
  grid on

ylabel('Sv','position',[1990.75 0 -1])

% --- North Subtropical Atlantic
axes(ha(6)); plot(t,regionRec.acc{1}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([lo hi])
  grid on

hl=legend('\tau_{mem} = 4 months','\tau_{mem} = 20 years','Location','none');
set(hl,'Position',[0.6054 0.4238 0.2929 0.0710]);

% --- South Subtropical Indian
axes(ha(7)); plot(t,regionRec.ind.SStrop{1}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([lo hi])
  grid on

% --- Southern Ocean
axes(ha(8)); plot(t,southRec{1}(tMem,:)+regionRec.acc{1}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([lo hi])
  grid on

% --- All others
axes(ha(9)); plot(t,regionRec.arc{1}(tMem,:)+regionRec.bar{1}(tMem,:)+...
                  regionRec.pac.subPolar{1}(tMem,:)+regionRec.pac.NStrop{1}(tMem,:)+...
                  regionRec.pac.SStrop{1}(tMem,:)+regionRec.atl.subPolar{1}(tMem,:)+...
                  regionRec.atl.SStrop{1}(tMem,:)+regionRec.ind.NStrop{1}(tMem,:))
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on')
  xlim([1992 2012])
  ylim([lo hi])
  grid on


set(gcf,'paperorientation','portrait')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])

saveas(gcf,plotFile,'pdf')

