function [] = bigHfluxPlot( mygrid )
% Make reconstruction plot for attributing variability to 
% hflux only.


fullrecfile='mat/reconstruct.34S/monthly_flux/fullReconstruction.mat';
matfile = 'mat/reconstruct.34S/region_rec.mat';
matfile2= 'mat/reconstruct.34S/basin_rec.mat';
mainPlotDir = 'figures/reconstruct.34S/region-rec/hflux/';
if ~exist(mainPlotDir,'dir'), mkdir(mainPlotDir); end;
plotFile = [mainPlotDir 'bigHfluxPlot'];

load(matfile,'regionRec','msks');
if ~exist(matfile2,'file'), calcPartitionTest(mygrid); end;
load(matfile2)
load(fullrecfile,'samoc_adjFields');

mskNames = fieldnames(msks);
Nt=240;
%tMem = [1,2,3,4,6,12,24,60,120,180,229];
%tMem=[1,3,5,12,60,72,84,96,108,120,132,144,156,168,180,192,204,216,229];
tMem=[4,229];
t=[1992+1/12:1/12:2012];


plotDir=[mainPlotDir 'sflux/']; if ~exist(plotDir,'dir'),mkdir(plotDir);end;

figureL;
[ha,pos]=tight_subplot(7,1,[.02 .03],[.05 .02],[.1 .1]);

lo=-1;
hi=1;
tt=1;
v=[lo:tt:hi];

ttm=.5;
vm=[lo:ttm:hi];

% --- Full hflux
axes(ha(1)); plot(t,samoc_adjFields{3}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on'); 
  ax=gca; ax.YAxis.MinorTickValues=vm;
  xlim([1992 2012])
  ylim([lo hi])
  grid on

% --- Arctic + Barents sea
axes(ha(2)); plot(t,regionRec.arc{3}(tMem,:)+0*regionRec.bar{3}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on'); 
  ax=gca; ax.YAxis.MinorTickValues=vm;
  xlim([1992 2012])
  ylim([lo hi])
  grid on

% --- Atlantic Subpolar
axes(ha(3)); plot(t,regionRec.pac.subPolar{3}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on'); 
  ax=gca; ax.YAxis.MinorTickValues=vm;
  xlim([1992 2012])
  ylim([lo hi])
  grid on

% --- South + ACC
axes(ha(4)); plot(t,southRec{3}(tMem,:),t,regionRec.acc{3}(tMem,:))
  set(gca,'xticklabel','')
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on'); 
  ax=gca; ax.YAxis.MinorTickValues=vm;
  xlim([1992 2012])
  ylim([lo hi])
  grid on

ylabel('Sv','Position',[1990.45 -1.65 -1])
hl=legend('\tau_{mem} = 4 months','\tau_{mem} = 20 years');
set(hl,'Position',[0.6089 0.3851 0.2891 0.0708]);

% --- All other
axes(ha(5)); plot(t,regionRec.ind.NStrop{3}(tMem,:))
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on'); 
  ax=gca; ax.YAxis.MinorTickValues=vm;
  xlim([1992 2012])
  ylim([lo hi])
  grid on

axes(ha(6)); plot(t,regionRec.ind.Ntrop{3}(tMem,:)+regionRec.ind.Strop{3}(tMem,:))
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on'); 
  ax=gca; ax.YAxis.MinorTickValues=vm;
  xlim([1992 2012])
  ylim([lo hi])
  grid on


axes(ha(7)); plot(t,0*regionRec.atl.ek{3}(tMem,:)+regionRec.ind.SStrop{3}(tMem,:))
  set(gca,'ytick',v,'xtick',[1992:2:2012])
  set(gca,'yminorgrid','on','yminortick','on'); 
  ax=gca; ax.YAxis.MinorTickValues=vm;
  xlim([1992 2012])
  ylim([lo hi])
  grid on


%axes(ha(5)); plot(t,regionRec.atl.NStrop{3}(tMem,:))
%  set(gca,'ytick',v,'xtick',[1992:2:2012])
%  set(gca,'yminorgrid','on','yminortick','on'); 
%  ax=gca; ax.YAxis.MinorTickValues=vm;
%  xlim([1992 2012])
%  ylim([lo hi])
%  grid on
%
%axes(ha(5)); plot(t,regionRec.atl.NStrop{3}(tMem,:))
%  set(gca,'ytick',v,'xtick',[1992:2:2012])
%  set(gca,'yminorgrid','on','yminortick','on'); 
%  ax=gca; ax.YAxis.MinorTickValues=vm;
%  xlim([1992 2012])
%  ylim([lo hi])
%  grid on

set(gcf,'paperorientation','portrait')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])


saveas(gcf,plotFile,'pdf')

