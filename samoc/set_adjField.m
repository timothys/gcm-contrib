function [adjField,flux_ind] = set_adjField( atmVar, varargin )

if length(varargin)>0
  forceVar=varargin{1}; 
end


%% First set string
if ~isempty(strfind(atmVar,'atm'))
  adjField = {'tauu','tauv','aqh','atemp','swdown','lwdown','precip','runoff'};
elseif ~isempty(strfind(atmVar,'flux'))
  adjField = {'tauu','tauv','hflux','sflux'};
else 
  error('** Error: atmVar option %s not recognized, later dude ...\n',atmVar)
end

if exist('forceVar','var')
% --- Based on forceVar, create vars and loop limits
if strcmp(forceVar,'tauu')
  flux_ind=1;
elseif strcmp(forceVar,'tauv')
  flux_ind=2;
elseif strcmp(forceVar,'wind')
  flux_ind=1:2;
elseif strcmp(forceVar,'hflux')
  flux_ind=3;
elseif strcmp(forceVar,'sflux') || strcmp(forceVar,'fwf')
  flux_ind=4;
elseif strcmp(forceVar,'buoyancy')
  if ~isempty(strfind(atmVar,'flux'))
    flux_ind=3:4;
  else
    flux_ind=3:8;
  end
elseif strcmp(forceVar,'all')
  flux_ind=1:4;
elseif strcmp(forceVar,'aqh') || strcmp(forceVar,'humidity')
  flux_ind=3;
elseif strcmp(forceVar,'atemp')
  flux_ind=4;
elseif strcmp(forceVar,'swdn') || strcmp(forceVar,'swdown')
  flux_ind=5;
elseif strcmp(forceVar,'lwdn') || strcmp(forceVar,'lwdown')
  flux_ind=6;
elseif ~isempty(strfind(forceVar,'rad')) 
  flux_ind=5:6;
elseif ~isempty(strfind(forceVar,'warm'))
  flux_ind=4:6;
elseif ~isempty(strfind(forceVar,'texas'))
  flux_ind=3:6;
elseif strcmp(forceVar,'evap')
  error('No evaporation in adxx fields ...')
elseif strcmp(forceVar,'preci') || strcmp(forceVar,'precip')
  flux_ind=7;
elseif strcmp(forceVar,'roff') || strcmp(forceVar,'runoff')
  flux_ind=8;
elseif strcmp(forceVar,'clim') && strcmp(atmVar,'atm')
  flux_ind = [1:7]; 
elseif strcmp(forceVar,'annual') && strcmp(atmVar,'atm')
  flux_ind = [1:2 7 8];
else
  error('forceVar option not recognized, exiting ... ')
  return;
end
end % end if exist(forceVar)
end
