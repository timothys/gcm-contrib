function [soi,years,months] = calcSOIIndex()
% A script to convert SOI index from csv form to vector
% Finally I'm doing this repeatably...
% ----------------------------------------------

% --- Read in the data
filename = 'index-data/soi_index.data';
csvmat = csvread(filename);


% --- Convert second column to vector with the index
soi = csvmat(:,2);

% --- Convert 1st column to sensible values
% (given as number with YYYYMM)
a = csvmat(:,1);
b = num2str(a);

yy = str2num(b(:,1:4));
mm = str2num(b(:,5:6));

years = yy + mm / 12;
months = 1:length(years);

% --- Convert all to row vectors b/c I like time to go with this dimension
soi     = soi';
years   = years';
months  = months';

end
