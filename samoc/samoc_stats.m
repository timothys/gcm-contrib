% Want some statistics on AMOC at 34S 

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');

slat = -34; 
[amoc, depthmax, kmax, ovStf] = calcAMOC(slat,'eulerian');
[amocBolus, dmBolus, kmBolus, ovStfBolus] = calcAMOC(slat,'bolus');
[amocRes, dmRes, kmRes, ovStfRes] = calcAMOC(slat,'residual');
[~,~,~,trVolZon] = calcAMOC(slat,'trVolZon');

%% Plot for SAMOC
ylbl = 'Depth (m)';
xlbl = 'Zonally Integrated Transport (Sv)';
titleStr = sprintf('Zonally Integrated Transport at %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
depthStatsPlot(mygrid.RF,trVolZon,{xlbl,ylbl,titleStr})
    
xlbl = 'Stream Function (Sv)';
titleStr = sprintf('Meridional Overturning Stream Function at %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
depthStatsPlot(mygrid.RF,ovStf,{xlbl,ylbl,titleStr})
    
titleStr = sprintf('Meridional Bolus Overturning Stream Function at %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
depthStatsPlot(mygrid.RF,ovStfBolus,{xlbl,ylbl,titleStr})

titleStr = sprintf('Meridional Residual Overturning Stream Function at %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
depthStatsPlot(mygrid.RF,ovStfRes,{xlbl,ylbl,titleStr})

Nt = size(amoc,2);
xlbl = 'Month (1992-2011)';
ylbl1 = 'AMOC (Sv)';
ylbl2 = 'z_{max} (m)';
titleStr = 'AMOC Characteristics from ECCOv4r2 Fields'; 
figureL
timeStatsPlot(1:Nt, [amoc;depthmax], {xlbl,ylbl1,ylbl2,titleStr},2,'median');

figureW
ylbl1='AMOC (Sv)';
timeStatsPlot(1:Nt, amoc, {xlbl,ylbl1,''},0);
saveas(gcf,'figures/samocStats','pdf');
%% Grab seasonality 
% ind_djf = [1:12:Nt, 2:12:Nt, 12:12:Nt]; ind_djf = unique(ind_djf);
% ind_mam = [3:12:Nt, 4:12:Nt, 5:12:Nt]; ind_mam=unique(ind_mam);
% ind_jja = [6:12:Nt, 7:12:Nt, 8:12:Nt]; ind_jja=unique(ind_jja);
% ind_son = [9:12:Nt, 10:12:Nt, 11:12:Nt]; ind_son=unique(ind_son);
% 
% amoc_djf = amoc(ind_djf); amoc_mam=amoc(ind_mam); amoc_jja=amoc(ind_jja); amoc_son=amoc(ind_son);
% % amoc_djf = depthmax(ind_djf); amoc_mam=depthmax(ind_mam); amoc_jja=depthmax(ind_jja); dm_son=depthmax(ind_son);

% t=1:length(amoc(ind_djf));
% titleStr = 'Dec. Jan. Feb. AMOC characteristics';
% figureL;
% timeStatsPlot(t, [amoc(ind_djf);depthmax(ind_djf)], {xlbl,ylbl1,ylbl2,titleStr},2,'median');
% 
% titleStr = 'March April May AMOC characteristics';
% figureL;
% timeStatsPlot(t, [amoc(ind_mam);depthmax(ind_mam)], {xlbl,ylbl1,ylbl2,titleStr},2,'median');
% 
% titleStr = 'June July Aug. AMOC characteristics';
% figureL;
% timeStatsPlot(t, [amoc(ind_jja);depthmax(ind_jja)], {xlbl,ylbl1,ylbl2,titleStr},2,'median');
% 
% titleStr = 'Sept. Oct. Nov AMOC characteristics';
% figureL;
% timeStatsPlot(t, [amoc(ind_son);depthmax(ind_son)], {xlbl,ylbl1,ylbl2,titleStr},2,'median');
