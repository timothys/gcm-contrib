function [] = plotseaice(mygrid)
%% Plot seaice diagnostics
% -------------------------------------------------------------------------


dataDir = '../../results/samoc/ref/diags/';
mainMatDir  = 'mat/eccov4r2/';
mainPlotDir = sprintf('figures/eccov4r2/'); 
matDir = [mainMatDir 'seaice/'];
plotDir = [mainPlotDir 'seaice/'];

if ~exist(mainMatDir,'dir'), mkdir(mainMatDir); end;
if ~exist(mainPlotDir,'dir'), mkdir(mainPlotDir); end;
if ~exist(matDir,'dir'), mkdir(matDir); end;
if ~exist(plotDir,'dir'), mkdir(plotDir); end;

exfField = {'SIarea','SIheff','SIempmr','SIatmFW','SIsnPrcp','SIacSubl','oceSPflx','oceSPtnd'};
Nexf = length(exfField);
cunits = {'m^2/m^2','m','kg/m^2/s','kg/m^2/s','kg/m^2/s','kg/m^2/s','kg/m^2/s','g/m^2/s','g/m^2/s'};
caxLim = {[-.5 .5],[-1.5 1.5],[-1e-4 1e-4],[-1e-4 1e-4],[-1e-5 1e-5],[-1e-6 1e-6],[-1e-3,1e-3],[-1e-4,1e-4]};
      
%% Plot and save at various time steps
for i = 1:Nexf

  matfile = [matDir exfField{i} '.mat'];
  
  % Make sure mat file is made
  if ~exist(matfile,'file')
    xx_fld=rdmds2gcmfaces([dataDir 'seaice*'],NaN,'rec',i);
    save(matfile,'xx_fld');
  else
    load(matfile,'xx_fld');
  end

  xx_fld=xx_fld-repmat(nanmean(xx_fld,3),[1 1 240]);

  vidFile = sprintf('%s%s_clim',plotDir,exfField{i});

  if ~exist([vidFile '.mp4'],'file')

    xx_clim = calcClimatology(xx_fld,20);

    %% Climatology video
    strs = struct(...
        'xlbl',sprintf('%s climatology',exfField{i}), ...
        'time','months',...
        'clbl',cunits{i},...
        'vidName',vidFile);
    opts = struct('saveVideo',1,...
                  'caxLim',caxLim{i},...
                  'mmapOpt',2.1,...
                  'colorScheme','parula',...
                  'noaxLims',1,...
                  'figType','square');
    plotVideo(xx_clim,strs,opts,mygrid);

  end

  vidFile=strrep(vidFile,'clim','annual');
  if ~exist([vidFile '.mp4'],'file')

    xx_annual = calcAnnualMean(xx_fld,20);

    %% Annual average
    strs = struct(...
        'xlbl',sprintf('%s annual avg',exfField{i}), ...
        'time','years',...
        'clbl',cunits{i},...
        'vidName',vidFile);
    opts = struct('saveVideo',1,...
                  'caxLim',caxLim{i},...
                  'mmapOpt',2.1,...
                  'colorScheme','parula',...
                  'noaxLims',1,...
                  'figType','square');
    plotVideo(xx_annual,strs,opts,mygrid);

  end
end
end
