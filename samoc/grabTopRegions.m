function [condensed_list, indSeparate, indEtc] = grabTopRegions(regionFld,numSeparate)
% grab numSeparate regions from full regionFld matrix
%
% Inputs:
%
%       regionFld = Ngroups x Ntime matrix
%                   with reconstruction from particular regions (groups)
%
% -----------------------------------------------------------------

% --- Grab sum of squares along 'time' dimension (columns) and sort
sosFld = sum(abs(regionFld),2);
%sosFld = max(abs(regionFld),[],2);

[sorted_sosFld, ind] = sort(sosFld);

% want max to min,
ind = ind(end:-1:1);

condensed_list = zeros(numSeparate+1,size(regionFld,2));

for i=1:numSeparate
  condensed_list(i,:) = regionFld(ind(i),:);
end

for i=numSeparate+1:size(regionFld,1)
  condensed_list(numSeparate+1,:) = condensed_list(numSeparate+1,:)+regionFld(ind(i),:);
end

indSeparate=ind(1:numSeparate);
indEtc = ind(numSeparate+1:end);
end
