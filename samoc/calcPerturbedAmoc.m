function [ amoc ] = calcPerturbedAmoc( uvel, vvel, mygrid )
% Given uvel and vvel, compute amoc for a given perturbation experiment
%   Returns 
%
%       amoc : [Ny x Nt] , Nt time length, 4th dim of uvel&vvel
%                          Ny latitude, NaNs for <35S, >70N
% 
% ----------------------------------------------------------------------

%% Instantiate
Ny = length(mygrid.LATS_MASKS);
Nr = length(mygrid.DRF);
Nt = size(uvel.f1,4);

%%  Atlantic basin masking
amocBasin = v4_basin('atlExt');
amocBasin = mk3D(amocBasin,mygrid.mskC);

%% Compute stream functions
atlOvStf=zeros(Ny,Nr+1,Nt);
for n =1:Nt
    uvel(:,:,:,n) = uvel(:,:,:,n).*mygrid.mskW.*amocBasin; 
    vvel(:,:,:,n) = vvel(:,:,:,n).*mygrid.mskS.*amocBasin;
    atlOvStf(:,:,n) = calc_overturn(uvel(:,:,:,n),vvel(:,:,:,n));
end
    
%% Compute volumetric transport at depth
atlTrVolZon = zeros(Ny,Nr+1,Nt);
atlTrVolZon(:,Nr+1,:) = -atlOvStf(:,Nr,:);
for k = Nr:-1:1
    atlTrVolZon(:,k,:) = -(atlOvStf(:,k,:) - atlOvStf(:,k+1,:));
end

%% Remove latitudes that don't make sense
iy=find(mygrid.LATS<-35|mygrid.LATS>70);
atlOvStf(iy,:,:) = NaN;
atlTrVolZon(iy,:,:) = NaN;

atlOvStf(atlOvStf==0)=NaN;
atlTrVolZon(atlTrVolZon==0)=NaN;

%% Compute AMOC 
kmax = zeros(Ny,Nt);
amoc = zeros(Ny,Nt);
depthmax = zeros(Ny,Nt);
iyRange = find(mygrid.LATS>=-35 & mygrid.LATS<=70);
for n = 1:Nt
    for iy=iyRange(1):iyRange(end)
        [amoc(iy,n),kmax(iy,n)] = max(atlOvStf(iy,:,n));
        if isnan(amoc(iy,n))
            depthmax(iy,n)=NaN;
        else
            depthmax(iy,n) = abs(mygrid.RF(kmax(iy,n)));
        end
    end
end
end