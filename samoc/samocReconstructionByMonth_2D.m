function [ samoc_adjFields ] = samocReconstructionByMonth_2D(modelVar,atmVar,lat,tau_mem,varargin)
% Want to reconstruct AMOC at 34S from adjoint sensitivities
% Note: This reconstruction skips the integral in space, so reconstruction is 
%       2D in space and varies with time
%       For this reason, have to decide tau_mem a priori and directories are saved    
%       with _2D suffix.
%
%   modelVar: e.g. 'eccov4r2', decides which forcing to use in reconstruction
%	atmVar: ['long' or 'short'] - ['atm' or 'flux']select to reconstruct with atmospheric state or 
%		 computed fluxes. 
%	lat: denotes latitude for computing reconstruction
%
%   tau_mem: memory over which to reconstruct
%
%	varargin:
%     sensFrac: fraction of final adxx_<> record to use
%               must be a double
%
%	  regionMsk: mask for partitioning hflux reconstruction
%		gcmfaces type
%
%	  baroInd: must be an integer! Denotes removing tf-n -> tf from reconst.
%	 	assign with n=int8(n);
%
%	  dirString: name for specifying save location of experiment, beyond monthly/flux/atm
%		e.g. monthly_"nobaro"_flux denotes no barotropic response ... no quotes though
%
%     integralString options: 
%           string indicating other variables to integrate reconstruction against
%           'none' (default)
%           'timescale' - compute reconstruction as 
%               int_t0^tf{ int_t-tau^t{ (t-s) | adxx(x,y,s-t) .* dx(x,y,s) | ds } dt}/ 
%               int_t0^tf{ int_t-tau^t{       | adxx(x,y,s-t) .* dx(x,y,s) | ds } dt}
%                           
%
% -------------------------------------------------------------------------

%% --- Load global grid variable
mygrid = establish_mygrid('llc90');

%% Step 0: Pre processing
[adjField] = set_adjField( atmVar );

%% Sift through varargs
dirString='full_2D';
integralString='none';
for n=1:length(varargin)
	if isa(varargin{n},'gcmfaces'), 
        regionMsk=varargin{n};
        fprintf('Found region msk in var argument %d \n',n);
	elseif isa(varargin{n},'integer') 
        baroInd=varargin{n};
        fprintf('Found barotropic index in var argument %d \n Index is: %d\n',n,baroInd);
	elseif isa(varargin{n},'char')
        if strcmp(varargin{n},'timescale')
            integralString = 'timescale';
            fprintf('Found integralString index in var argument %d \n integralString is: %s\n',n,integralString);
            dirString = 'timescale_2D';
            fprintf('Setting dirString to %s\n',dirString);
        else
            dirString=sprintf('%s_2D',varargin{n});
            fprintf('Found dirString index in var argument %d \n dirString is: %s\n',n,dirString);
        end
    elseif isa(varargin{n},'double')
        sensFrac=varargin{n};
        fprintf('Found sensFrac index in var argument %d \n sensFrac(1) is: %f\n',n,sensFrac(1));
	else 
        fprintf('** Don''t recognize the %dth varargin, type help samocReconstructionByMonth for options and how to assign\n',n);
	    return;
	end
end

if ~isempty(strfind(modelVar,'gpcp'))
    Nt = 180;
else
    Nt = 240;
end
recPerYr = 12;
monthStr = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
if strcmp(dirString,'hf')
   Nt = 479;
   recPerYr = 24;
end
Nadj = length(adjField);
Nmo = length(monthStr);

%% Set up dirs
[saveDir]=samoc_dirs('reconstruct-mat',modelVar,atmVar,dirString);

%% Make sure sensitivity files are post processed to mat files
for m = 1:Nmo
    if lat==-34, runStr=sprintf('%s.240mo',monthStr{m});
    else runStr=sprintf('%s.240mo.%s',monthStr{m},dirName);
    end;
    if ~exist(sprintf('mat/%s/adj_hflux.mat',runStr))
	fprintf('Need to postprocess adxx files, exiting ...\n');
	return;
    end
end
% -------------------------------------------------------------------------

%% 2. Prep for reconstruction
%% First, grab cost function from forward model
[samoc] = calcAMOC(lat,'eulerian');

% Forward cost function, variability
samoc_for_mean= samoc - mean(samoc);

%% Now go through forcing files and sensitivities
% make a 2D zero vector
zero_gcmface = convert2gcmfaces(0*mygrid.XC(:,:,1));
zero_gcmface = squeeze( ...
                    repmat(zero_gcmface,[1 1 Nt]));
zero_gcmface = convert2gcmfaces(zero_gcmface);

samoc_rec = zero_gcmface;
if strcmp(integralString,'timescale')
    samoc_rec = samoc_rec(:,:,1);
end
samoc_adjFields = cell(Nadj,1);

%% Make sensFrac recPerYear long
if exist('sensFrac','var')
  if(length(sensFrac)==1)
    sensFrac=repmat(sensFrac,recPerYr,1);
  elseif(length(sensFrac) ~= recPerYr)
    fprintf('Unrecognized length of sensFrac variable, exiting ...')
    return;
  end
end

% -------------------------------------------------------------------------

%% 3. Perform reconstruction
for i = 1:Nadj
    %% Initialize fields for saving
    fld_rec             = zero_gcmface;
    samoc_adjFields{i}  = zero_gcmface;

    %% Find force file
    if ~isempty(strfind(modelVar,'en97')) || ~isempty(strfind(modelVar,'no93'))
        forceDir=sprintf('../enso-samba/mat/exf/%s/%s',modelVar,atmVar);
    else
        forceDir=sprintf('mat/exf/%s/%s',modelVar,atmVar);
    end
    forceFile = sprintf('%s/exf_%s.mat',forceDir,adjField{i});
    if strcmp(dirString,'hf') 
      error( 'Need to retool forcing for high frequency ' )
      forceFile = sprintf('%s../hf/exf_%s.mat',saveDir,adjField{i});
    end

    %% Load Forcing
    if exist(forceFile,'file')
        load(forceFile,'exf_fld');
    else
        fprintf('Preparing file: %s\n',forceFile);
        exf_fld=prepForcingFile(modelVar,atmVar,adjField{i},mygrid);
    end

    for m = 1:recPerYr
        %% Load sensitivity
        adjFile = sprintf('mat/%s.240mo/adj_%s.mat',monthStr{m},adjField{i});
	    if strcmp(dirString,'hf') 
            adjFile = sprintf('mat/%s.480bwk/adj_%s.mat',monthStr{m},adjField{i});
        end
        
        if exist(adjFile,'file')

            load(adjFile,'adxx');
            
            %% Determine relevant time period for sensitivity file
            %% Note: This is different from Nt because the start date is 
            %%       the same no matter when the objective function is
            %%       evaluated.
            %%       Practically, means that reconstructions are only
            %%       reliable for (Nyrs-1) years. 
            %%       Consider a 5 year simulation. Compute Jan. & Dec. 
            %%       objective functions. We have monthly sensitivities going back
            %%       48 and 48+11=59 months respectively. This means a 50+ month 
            %%       reconstruction is not well defined in the former case
            %%       so for consistency this is eliminated.
            %%       NOTE here: this is valid 50 and beyond, not 49 because in this case
            %%       there is a portion of the sensitivity which is computed for the 
            %%       month during objective function evaluation which is incorporated
            %%       in the reconstruction.
            Ntad = Nt + m - recPerYr;
            lv=m:recPerYr:Nt;
            Nyrs=length(lv);

            %% Prep forcing files
            Nexf=size(exf_fld.f1,3);
            exf_tmp = exf_fld - repmat(mean(exf_fld,3),[1 1 Nexf]);

            % nExcess is num records for which Nexf > Nt (e.g. using EIG start in 1979)
            % Grab only exf variables up until where objective function is evaluated 
            % given by nExcess+Ntad
            nExcess=Nexf-Nt;
            exf_tmp=exf_tmp(:,:,1:(nExcess+Ntad));

            % Now for convolution, want to pad exf_tmp with zeros prior to where we have 
            % data. nAppend gives how many zeros to pad with
            % if Nt=Nexf, then the appending is Ntad-1 long. Otherwise it's less.
            nAppend=Ntad-nExcess;
            exf_tmp = cat(3,0*exf_tmp(:,:,1:nAppend-1),exf_tmp); % now 3rd dim is 2*Ntad-1 long

            %% Optional: if sensFrac exists, add on to last bit of adxx
            if exist('sensFrac','var')
	            adxx(:,:,Ntad)=adxx(:,:,Ntad)+sensFrac(m)*adxx(:,:,Ntad+1);
            end
            adxx=adxx(:,:,1:Ntad);

	        %% Optional: remove barotropic component
	        if strcmp(adjField{i},'hflux') && exist('baroInd','var')
		        baroInd=double(baroInd);
		        adxx(:,:,Ntad-baroInd:Ntad) = 0*adxx(:,:,Ntad-baroInd:Ntad);
	        end

	        %% Optional: grab subdomain for reconstruction
	        if exist('regionMsk','var')
		        adxx=adxx.*repmat(regionMsk,[1 1 Ntad]);
	        end
            
            % Perform reconstruction for particular forcing
            exf_tmp = convert2gcmfaces(exf_tmp);
            adxx = convert2gcmfaces(adxx);
            if strcmp(integralString,'none')
                for n = 1:Nyrs

                    % Inner product, summing only in time
                    % samoc_tmp is just a matrix
                    samoc_tmp = exf_tmp(:,:,lv(n)+[0:Ntad-1]).*adxx;
                    samoc_tmp = convert2gcmfaces(flipdim(samoc_tmp,3));
                    samoc_tmp = samoc_tmp(:,:,1:tau_mem);

                    % Keep track of sensitivity to particular field
                    samoc_adjFields{i}(:,:,lv(n)) = nansum(samoc_tmp,3);
                end
            elseif strcmp(integralString,'timescale')

                % Here computing the ratio shown in the header of this file
                % because I'm hijacking this code to do a different computation
                % the following variables are used
                % (this is done because have to compute ratio after all time steps
                %  are reconstructed)
                %
                %   T = dJ_numer / dJ_denom
                %
                %   samoc_adjFields{i} := dJ_numer{i}
                %   fld_rec            := dJ_denom
                %
                % then rewrite for
                %
                %   samoc_adjFields{i} := T

                s = [Ntad-0.5:-1:0.5];
                s_mat = repmat(reshape(s,[1 1 Ntad]),[size(adxx(:,:,1)) 1]);
                for n = 1:Nyrs

                    % Inner product, summing only in time
                    % samoc_tmp is just a matrix, not gcmfaces
                    abs_inner_prod = abs(exf_tmp(:,:,lv(n)+[0:Ntad-1]).*adxx);

                    numer = flipdim(s_mat.*abs_inner_prod,3);
                    numer = nansum(numer(:,:,1:tau_mem),3);

                    denom = flipdim(abs_inner_prod,3);
                    denom = nansum(denom(:,:,1:tau_mem),3);

                    % Use available fields for storing until done for all t
                    samoc_adjFields{i}(:,:,lv(n)) = convert2gcmfaces(numer);
                    fld_rec(:,:,lv(n)) = convert2gcmfaces(denom);
                end
            end% if strcmp(integralString...)
        else
            fprintf('File: %s doesn''t exist, skipping ...\n',adjFile);
        end
    end %for 1:Nmo

    % Now add to full reconstruction
    if strcmp(integralString,'none')
        samoc_rec = samoc_rec + samoc_adjFields{i}; 
        fld_rec = samoc_adjFields{i};
    elseif strcmp(integralString,'timescale')
        samoc_adjFields{i} = nansum(samoc_adjFields{i},3) ./ nansum(fld_rec,3);
        samoc_rec = samoc_rec + samoc_adjFields{i};
        fld_rec = samoc_adjFields{i};
    end
    fprintf('* Done with %s sensitivity ... \n',adjField{i});

    save(sprintf('%s/%s.mat',saveDir, adjField{i}),'fld_rec');
end %for 1:Nadj

save(sprintf('%s/fullReconstruction.mat',saveDir),'samoc_adjFields','samoc_for_mean','samoc_rec');
end
