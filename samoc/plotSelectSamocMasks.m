function [] = plotSelectSamocMasks(ind,separator,saveName,cc)
% Plot a selection of masks
%
% Inputs: 
%       ind: index associated with msks to grab. Computed inside stackRecPlot for now
%       separator: indicator for regions to grab. See grabSamocGroups.m for details

%
% --------------------------------------------------------------

mygrid=establish_mygrid('llc90');

if nargin<4
  cc=cbrewer2('Paired',length(ind)+1);
end

% --- Grab masks 
if ~isempty(strfind(separator,'ek'))
    % separate out ekman band around 34S
    msks=createRegionMasks('ekman');
else
    msks=createRegionMasks();
end

% --- Grab masks for each specified index with the samoc groups
[~,sections]=grabSamocGroups(separator,msks,mygrid);
samocTopTracks=flipdim(sections(:,:,ind),3);
mskFld=0*samocTopTracks(:,:,1);
for i=1:size(samocTopTracks.f1,3)
    mskFld=mskFld+i*samocTopTracks(:,:,i);
end
mskFld(mskFld==0)=NaN;

% --- If no save name assume adding to subplot for now
if ~exist('saveName','var') || isempty(saveName)
  ax3=subplot(3,2,6);
else
  figureWhalf;
end

% --- Could add depth, TBD
%m_map_atl({'contour',mygrid.Depth,[5e1 5e2 1.5e3],'k'},5,{'doHold',1});

% --- Do the plotting
m_map_atl(mskFld,5.1);
colormap(cc);
ch=findall(gcf,'tag','Colorbar');
delete(ch);

if exist('saveName','var') && ~isempty(saveName)
  savePlot(saveName,'png');
end

end
