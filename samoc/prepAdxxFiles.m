function [] = prepAdxxFiles( mygrid )
% This could be a lot fancier, but I want to prep adxx files
% really just need for hflux to verify signage

months={'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};

for m=1:length(months)
  runStr=sprintf('%s.240mo/',months{m});
  resultsDir=sprintf('../../results/samoc/%s.240mo/',months{m});
  postProcess_adxx(runStr,resultsDir,'hflux',mygrid,10^-6);
end

end
