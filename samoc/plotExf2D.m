function [] = plotExf2D( exfVar, regionVar, plotType)
% Plot precipitation average over N Atl Subpolar gyre
% from multiple atmospheric data sets
%
% Inputs:
%       
%       exfVar: 'tauu' or 'precip' etc ...
%               for now only atm vars ... no flux
%       
%       regionVar: 'acc' or 'atl.subPolar' .. 
%                  same convention as regionFld
%       
%       plotType: '2DAvg' (mean over record) 
%                 '<month>' e.g. 'jan' 'march' ...
%                 '<year>' e.g. 1992 ... gives mean of year
%              
% -----------------------------------------------------------------


%% Set up dirs
plotDir=sprintf('figures/exf/region-comp');
if ~exist(plotDir,'dir'), mkdir(plotDir); end;

saveName = sprintf('%s/%s_%s_%s',plotDir,exfVar,strrep(regionVar,'.','-'),plotType);

%% Set the vars
modelVars={'eccov4r2','eccov4r3','eig','jra55','jra55-gpcp'};
atmVars={'short-atm','short-atm','long-atm','short-atm','short-atm'};

%% Get index for exf variable
adjField=set_adjField( 'atm' );

%% Set up vars
nModels=length(modelVars);
nRecs=0;
fld_out = cell(nModels,1);
tMo = cell(nModels,1);
tYrs=tMo;
lbl=cell(nModels,1);


%% Get region mask
msks=createRegionMasks;
if strcmp(regionVar,'greenland')
  msk=msks.atl.subPolar+msks.arc;
else
  eval(sprintf('msk=msks.%s;',regionVar));
end
msk=convert2gcmfaces(msk);
msk(msk==0)=NaN;

%% Compute average
for i=1:nModels

  loadFile = sprintf('mat/exf/%s/%s/exf_%s.mat',modelVars{nRecs+1},atmVars{nRecs+1},exfVar);
  if exist(loadFile,'file')
    nRecs=nRecs+1;
    fprintf('Loading file: %s ...\n',loadFile);
    load(loadFile,'exf_fld')
  else
    fprintf('Need to prep %s - %s files, skipping ...\n',modelVars{nRecs},atmVars{nRecs});
  end

  Nmo = size(exf_fld.f1,3);
  
  if ~isempty(strfind(atmVars{i},'short'))
    yr0=1992;
  else
    yr0=1979;
  end

  tMo{nRecs} = yr0 + [1:Nmo]./12;
  tYrs{nRecs} = yr0:(yr0+Nmo/12-1);
  
  exf_fld=convert2gcmfaces(exf_fld).*repmat(msk,[1 1 Nmo]);
  
  if strcmp(plotType,'2DAvg') 

    % Average over full record
    fld_out{nRecs} = convert2gcmfaces(nanmean(exf_fld,3));

  elseif isempty(str2num(plotType))
  
    % Climatology
    ind=parseMonth(plotType); 
    allInd = ind:12:length(tMo{nRecs});
    fld_out{nRecs} = convert2gcmfaces(nanmean(exf_fld(:,:,allInd),3));

  else

    % Annual average
    pp=str2num(plotType);
    ind = pp == floor(tMo{nRecs});
    fld_out{nRecs} = convert2gcmfaces(nanmean(exf_fld(:,:,ind),3));

  end
 
end
msk=convert2gcmfaces(msk);

%% Load the error
errorFile = sprintf('mat/eccov4r2/ctrls/error/%s.mat',exfVar);
if ~exist(errorFile,'file')
  [ctrlError_mean,ctrlError_var] = loadCtrlError(exfVar);
else
  load(errorFile,'ctrlError_mean','ctrlError_var');
end

%% Do the plotting
figureH;
[ha,pos] = tight_subplot(3,3,[.005 .02],[.06 .02],[.05 .05]);
for i=1:nRecs
 
  % --- Change here for different regions
  if strcmp(regionVar,'greenland')
    mapOpt=7.1;
  else
    mapOpt=7;
  end
  axes(ha(i)),m_map_atl(fld_out{i},mapOpt);
  xlabel(sprintf('%s: %s %s',modelVars{i},plotType,exfVar))
  if strcmp(exfVar,'precip')
    caxis([0 10]*10^-8);
  end
end
axes(ha(7)),m_map_atl(log10(ctrlError_mean).*msk,mapOpt);
  xlabel(sprintf('Log_{10}( eccov4r2 %s mean bias )',exfVar))
axes(ha(8)),m_map_atl(log10(ctrlError_var).*msk,mapOpt);
  xlabel(sprintf('Log_{10}( eccov4r2 %s error variance )',exfVar))
 
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,saveName,'pdf');

end

function [ind] = parseMonth(plotType)

moStr = {'jan','feb','mar','apri','jun','jul','aug','sept','oct','nov','dec'};
ind=0;

for i=1:length(moStr)
  if ~isempty(strfind(plotType,moStr{i}))
    ind=i;
  end
end

if ind==0
  error('Couldn''t read month string, see subroutine parseMonth for available strings ...')
end

end
