function [amoc] = calc_amoc()
%
% Script to load ECCOv4r3 UVELMASS/VVELMASS files and compute AMOC
%
% Requires gcmfaces routines: grid_load, read_nctiles, v4_basin, mk3D, calc_overturn
%
% Output: amoc - 1D vector of doubles
%                amoc at desired latitude
%
% -----------------------------------------------------------------

% --- 0. Directories, filenames ...
lat=16;
loadDir = '../../results/eccov4r3/download/';
saveDir = 'mat/eccov4r3/state/';

if ~exist(saveDir,'dir'), mkdir(saveDir); end;

% --- 1. Load up grid
global mygrid;
Ny = length(mygrid.LATS_MASKS);
Nr = length(mygrid.RF);
Nt = 288;

% --- 2. Generate masks for atlantic
amocBasin = v4_basin('atlExt');
amocBasin = mk3D(amocBasin,mygrid.mskC);

% flip around to get an atlantic mask and volume weighting
atlBasin = flipdim(permute(amocBasin.f1,[2 1 3]),1);
atlBasin = cat(2,amocBasin.f5,atlBasin);

atlMskS = flipdim(permute(mygrid.mskS.f1,[2 1 3]),1);
atlMskS = cat(2,mygrid.mskS.f5,atlMskS);

[dyg,dxg] = calc_UEVNfromUXVY(-mygrid.DYG,mygrid.DXG);
dxg = mk3D(dxg,mygrid.mskS);
drf = mk3D(mygrid.DRF,mygrid.mskS);

atlDxg = flipdim(permute(dxg.f1,[2 1 3]),1);
atlDxg = cat(2,dxg.f5,atlDxg);

atlDrf = flipdim(permute(drf.f1,[2 1 3]),1);
atlDrf = cat(2,drf.f5,atlDrf);

% --- 3. Read in velocities and compute streamfunction

% Create UVEL/VVEL in Atlantic
VVELMASS = [];

% Note: 
% atlOvStf is a 3D array contains streamfunction at
%  i: each latitude
%  j: each depth
%  k: across time
atlOvStf = zeros(Ny,Nr,Nt);

%for n=1:Nt
%
%  uvel = read_nctiles([loadDir 'UVELMASS/UVELMASS'],'UVELMASS',n);
%  vvel = read_nctiles([loadDir 'VVELMASS/VVELMASS'],'VVELMASS',n);
%
%  % Compute overturning 
%  atlOvStf(:,:,n) = calc_overturn(uvel.*amocBasin.*mygrid.mskW,vvel.*amocBasin.*mygrid.mskS);
%
%  % Flip them around for saving
%  [uvel,vvel] = calc_UEVNfromUXVY(uvel,vvel);
%
%  % Stitch f1 and f5 together to make a matrix resembling atlantic
%  vvel_new_f1 = flipdim(permute(vvel.f1,[2 1 3]),1);
%  vvel_new = cat(2,vvel.f5,vvel_new_f1);
%  VVELMASS = cat(4,VVELMASS,vvel_new);
%
%end
%
%save([saveDir 'atlVVel.mat'],'VVELMASS','atlOvStf','-v7.3');

load([saveDir 'atlVVel.mat'],'VVELMASS','atlOvStf');
fprintf('Streamfunction computed ...\n');


% --- 4. At last, compute the AMOC at desired latitude
latInd = mygrid.LATS;
iy = find(latInd==lat);
kmax = zeros(1,Nt);
amoc = zeros(1,Nt);
depthmax = zeros(1,Nt);

for n = 1:Nt
  [~,kmax(n)] = max(squeeze(atlOvStf(iy,:,n)));
  amoc(n) = atlOvStf(iy,kmax(n),n);
  depthmax(n) = abs(mygrid.RF(kmax(n)));
end

% --- 5. For simplicity, save latitude masks that pertain to flipped velocities
mskW = convert2gcmfaces( repmat(convert2gcmfaces(0*mygrid.LATS_MASKS(1).mskWedge), [1 1 Ny]));
mskS = convert2gcmfaces( repmat(convert2gcmfaces(0*mygrid.LATS_MASKS(1).mskSedge), [1 1 Ny]));

for j = 1:Ny
  [mskW(:,:,j),mskS(:,:,j)] = calc_UEVNfromUXVY(mygrid.LATS_MASKS(j).mskWedge,mygrid.LATS_MASKS(j).mskSedge);
end

atlLatMsk = flipdim(permute(mskS.f1,[2 1 3]),1);
atlLatMsk = cat(2,mskS.f5,atlLatMsk);

% --- 6. Save everything necessary to do this computation 
t=[1:Nt]./12 + 1992;
save([saveDir 'eccov4r3_amoc.mat'],'atlOvStf','amoc','t',...
          'VVELMASS','atlBasin','atlMskS','atlLatMsk','latInd','atlDrf','atlDxg','-v7.3');
end
