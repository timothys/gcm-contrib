function [rec_avg,cond_avg_p,cond_avg_n,top_index] = calcStackRec(regionRec,tMem,samoc_adjFields,flux_ind,avgSelect,separator,doNormalize)

numSeparate=5;
Nt = 240;

% --- Get full reconstruction
fullRec=zeros(1,Nt);
for i=flux_ind(1):flux_ind(end)
  fullRec=fullRec+samoc_adjFields{i}(tMem,:);
end
  

% separate reconstruction by group
[all_avg] = grabSamocGroups(separator,tMem,flux_ind,regionRec);
all_avgp=all_avg; all_avgn=all_avg;
all_avgp(all_avg<0)=0; all_avgn(all_avg>=0)=0;

% Get avgs
[rec_avg,region_avg_p,region_avg_n,region_avg] = avgFields(all_avg,fullRec,avgSelect,20);
 
% Separate to regions
[cond_avg,top_index,other_index] = grabTopRegions(region_avg,numSeparate);
cond_avg_p=cond_avg; cond_avg_p(cond_avg<0)=0;
cond_avg_n=cond_avg; cond_avg_n(cond_avg>=0)=0;

%% Get normalizing factor
%if doNormalize==1
%  fwdClim=calcClimatology(samoc_for_mean,20);
%  fwdClimAmp=max(fwdClim)-min(fwdClim);
%
%  rec_clim=rec_clim ./ fwdClimAmp;
%  cond_clim_p=cond_clim_p ./ fwdClimAmp;
%  cond_clim_n=cond_clim_n ./ fwdClimAmp;
%elseif doNormalize==2
%  fwdMean=14.25; %[sv]
%
%  rec_clim=rec_clim ./ fwdMean;
%  cond_clim_p=cond_clim_p ./ fwdMean;
%  cond_clim_n=cond_clim_n ./ fwdMean;
%end
end

% ---------------------------------------------------------------------

function [rec_avg,region_avg_p,region_avg_n,region_avg] = avgFields(regionFld,fullFld,avgType,Nyrs)

if strcmp(avgType,'clim')
  rec_avg = calcClimatology(fullFld,Nyrs);
  region_avg = calcClimatology(regionFld,Nyrs);
elseif strcmp(avgType,'annual')
  rec_avg = calcAnnualMean(fullFld,Nyrs);
  region_avg = calcAnnualMean(regionFld,Nyrs);
elseif strcmp(avgType,'anomaly')
  rec_avg = fullFld - repmat(calcClimatology(fullFld,Nyrs,1),[1 Nyrs]);
  region_avg = regionFld - repmat(calcClimatology(regionFld,Nyrs,0),[1 Nyrs]);
else
  error('Option not recognized, please specify ''clim'' for climatology or ''annual'' for annual averages')
  return;
end 

region_avg_p = region_avg; region_avg_n = region_avg;
region_avg_p(region_avg<0)=0;
region_avg_n(region_avg>=0)=0;

end
