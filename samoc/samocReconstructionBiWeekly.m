function [ samoc_adjFields ] = samocReconstructionBiWeekly(modelVar,atmVar,lat,varargin)
% Want to reconstruct AMOC at 34S from adjoint sensitivities
%
%   modelVar: e.g. 'eccov4r2', decides which forcing to use in reconstruction
%	atmVar: ['long' or 'short'] - ['atm' or 'flux'] - bwk
%           select to reconstruct with atmospheric state or 
%		    computed fluxes. 
%	lat: denotes latitude for computing reconstruction
%
%	varargin:
%     sensFrac: fraction of final adxx_<> record to use
%               must be a double
%
%	  regionMsk: mask for partitioning hflux reconstruction
%		gcmfaces type
%
%	  baroInd: must be an integer! Denotes removing tf-n -> tf from reconst.
%	 	assign with n=int8(n);
%
%	  dirString: name for specifying save location of experiment, beyond monthly/flux/atm
%		e.g. monthly_"nobaro"_flux denotes no barotropic response ... no quotes though
%
% -------------------------------------------------------------------------

%% --- Load global grid variable
mygrid = establish_mygrid('llc90');


%% Sift through varargs
dirString='full';
recENSOPerturb=0;
for n=1:length(varargin)
	if isa(varargin{n},'gcmfaces'), 
        regionMsk=varargin{n};
        fprintf('Found region msk in var argument %d \n',n);
	elseif isa(varargin{n},'integer'), 
        baroInd=varargin{n};
        fprintf('Found barotropic index in var argument %d \n Index is: %d\n',n,baroInd);
	elseif isa(varargin{n},'char'), 
        dirString=varargin{n};
        fprintf('Found dirString index in var argument %d \n dirString is: %s\n',n,dirString);
    elseif isa(varargin{n},'double'),
        sensFrac=varargin{n};
        error('sensFrac does not make sense for biweekly output');
	else 
        fprintf('** Don''t recognize the %dth varargin, type help samocReconstructionByMonth for options and how to assign\n',n);
	     return;
	end
end

% --- atmVar must have bwk within the string, otherwise use ByMonth script
if isempty(strfind(atmVar,'bwk'))
    error('Can''t find ''bwk'' within atmVar. Either adjust or use ByMonth script');
end

% --- Recognize reconstruction for perturbation experiment with single variable
if (~isempty(strfind(modelVar,'hk92')) || ...
    ~isempty(strfind(modelVar,'xx92')) || ...
    ~isempty(strfind(modelVar,'hk97')) || ...
    ~isempty(strfind(modelVar,'xx97')) )
    recENSOPerturb = 1;
    fprintf('Recognized modelVar = %s, reconstructing from perturbation experiment\n',modelVar);
end

% --- Set field names for each forcing variable
if recENSOPerturb == 0
    [adjField] = set_adjField( atmVar );
else
    adjField={'tauu'};
end


if ~isempty(strfind(modelVar,'gpcp'))
    error('Need to prep gpcp for biweekly output')
end
Nt = 479;
Nadj = length(adjField);

%% Set up dirs
[saveDir]=samoc_dirs('reconstruct-mat',modelVar,atmVar,dirString);

%% Make sure sensitivity files are post processed to mat files
for m = 1:Nmo
    if lat==-34, 
        runStr=sprintf('%s.480bwk',monthStr{m});
    else 
        error('Not ready for other latitudes');
    end;
    if ~exist(sprintf('mat/%s/adj_hflux.mat',runStr))
	    error('Need to postprocess adxx files, exiting ...\n');
    end
end
% -------------------------------------------------------------------------

%% 2. Prep for reconstruction
%% Now go through forcing files and sensitivities
samoc_rec = zeros(Nt,Nt);
samoc_adjFields = cell(Nadj,1);

% -------------------------------------------------------------------------

%% 3. Perform reconstruction
for i = 1:Nadj
    %% Initialize fields for saving
    fld_rec=zeros(Nt,Nt);

    samoc_adjFields{i} = zeros(Nt,Nt);

    % --- Find force file
    if recENSOPerturb == 0
        forceDir=sprintf('mat/exf/%s/%s',modelVar,atmVar);
        forceFile = sprintf('%s/exf_%s.mat',forceDir,adjField{i});
    else
        forceDir = sprintf('../enso-samba/mat/%s/monthly/%s',modelVar,atmVar);
        if ~isempty(strfind(modelVar,'.diff'))
            fprintf('Computing reconstruction with perturbed-ref\n');
            forceDir = erase(forceDir,'.diff');
            forceFile = sprintf('%s/%s_diff.mat',forceDir,adjField{i});
        else
            fprintf('Computing reconstruction with full perturbed forcing, not diff from reference\n');
            forceFile = sprintf('%s/exf_%s.mat',forceDir,adjField{i});
        end
    end

    if strcmp(dirString,'hf') 
        error( 'Need to retool forcing for high frequency ' )
        forceFile = sprintf('%s../hf/exf_%s.mat',saveDir,adjField{i});
    end

    %% Load Forcing
    if exist(forceFile,'file')
        load(forceFile,'exf_fld');
    else
        fprintf('Preparing file: %s\n',forceFile);
        exf_fld=prepForcingFile(modelVar,atmVar,adjField{i},mygrid);
    end

    for m = 1:recPerYr
        %% Load sensitivity
        adjFile = sprintf('mat/%s.240mo/adj_%s.mat',monthStr{m},adjField{i});
	    if strcmp(dirString,'hf') 
            adjFile = sprintf('mat/%s.480bwk/adj_%s.mat',monthStr{m},adjField{i});
        end
        
        if exist(adjFile,'file')

            load(adjFile,'adxx');
            
            %% Determine relevant time period for sensitivity file
            %% Note: This is different from Nt because the start date is 
            %%       the same no matter when the objective function is
            %%       evaluated.
            %%       Practically, means that reconstructions are only
            %%       reliable for (Nyrs-1) years. 
            %%       Consider a 5 year simulation. Compute Jan. & Dec. 
            %%       objective functions. We have monthly sensitivities going back
            %%       48 and 48+11=59 months respectively. This means a 50+ month 
            %%       reconstruction is not well defined in the former case
            %%       so for consistency this is eliminated.
            %%       NOTE here: this is valid 50 and beyond, not 49 because in this case
            %%       there is a portion of the sensitivity which is computed for the 
            %%       month during objective function evaluation which is incorporated
            %%       in the reconstruction.
            Ntad = Nt + m - recPerYr;
            lv=m:recPerYr:Nt;
            Nyrs=length(lv);
            samoc_tmp=zeros(Ntad,Nyrs);

            %% Prep forcing files
            Nexf=size(exf_fld.f1,3);
            exf_tmp = exf_fld - repmat(mean(exf_fld,3),[1 1 Nexf]);

            % nExcess is num records for which Nexf > Nt (e.g. using EIG start in 1979)
            % Grab only exf variables up until where objective function is evaluated 
            % given by nExcess+Ntad
            nExcess=Nexf-Nt;
            exf_tmp=exf_tmp(:,:,1:(nExcess+Ntad));

            % Now for convolution, want to pad exf_tmp with zeros prior to where we have 
            % data. nAppend gives how many zeros to pad with
            % if Nt=Nexf, then the appending is Ntad-1 long. Otherwise it's less.
            nAppend=Ntad-nExcess;
            exf_tmp = cat(3,0*exf_tmp(:,:,1:nAppend-1),exf_tmp); % now 3rd dim is 2*Ntad-1 long

            %% Optional: if sensFrac exists, add on to last bit of adxx
            if exist('sensFrac','var')
	            adxx(:,:,Ntad)=adxx(:,:,Ntad)+sensFrac(m)*adxx(:,:,Ntad+1);
            end
            adxx=adxx(:,:,1:Ntad);

	        %% Optional: remove barotropic component
	        if strcmp(adjField{i},'hflux') && exist('baroInd','var')
		        baroInd=double(baroInd);
		        adxx(:,:,Ntad-baroInd:Ntad) = 0*adxx(:,:,Ntad-baroInd:Ntad);
	        end

	        %% Optional: grab subdomain for reconstruction
	        if exist('regionMsk','var')
		        adxx=adxx.*repmat(regionMsk,[1 1 Ntad]);
	        end
            
            % Perform reconstruction for particular forcing
            exf_tmp = convert2gcmfaces(exf_tmp);
            adxx = convert2gcmfaces(adxx);
            for n = 1:Nyrs

                samoc_tmp(:,n) = squeeze(nansum(nansum(...
                                         exf_tmp(:,:,lv(n)+[0:Ntad-1]).*adxx,1),2));

                % Flip direction for sum going backward in time
                samoc_tmp(:,n) = flipdim(samoc_tmp(:,n),1);
            end

            % Keep track of sensitivity to particular field
            samoc_adjFields{i}(1:Ntad,lv) = cumsum(samoc_tmp(1:Ntad,:),1);
	
        else
            fprintf('File: %s doesn''t exist, skipping ...\n',adjFile);
        end
    end %for 1:Nmo

    % Now add to full reconstruction
    samoc_rec = samoc_rec + samoc_adjFields{i}; 
    fld_rec = samoc_adjFields{i};
    fprintf('* Done with %s sensitivity ... \n',adjField{i});

    save(sprintf('%s/%s.mat',saveDir, adjField{i}),'fld_rec');
end %for 1:Nadj

save(sprintf('%s/fullReconstruction.mat',saveDir),'samoc_adjFields','samoc_rec');
end
