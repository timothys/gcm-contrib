function[regionFld] = calcRegionAvg( modelVar, atmVar, varargin )
% Look for trends in the forcing data and adxx by region for attribution

exfOrAdxx='exf';
for n=1:length(varargin)
  if isa(varargin{n},'char')
    if strcmp(varargin{n},'exf') || strcmp(varargin{n},'adxx')
      exfOrAdxx=varargin{n};
    end
  else
    fprintf('Don''t recognize varargin{%d}, skipping ...\n',n);
  end
end

msks=createRegionMasks; 
regionFld = msks;
mskNames=fieldnames(msks);

if strcmp(exfOrAdxx,'exf')
  saveDir=sprintf('mat/exf/%s/%s',modelVar,atmVar);
  if ~exist(saveDir,'dir') 
    fprintf('Need to prep forcing files, skipping %s - %s ...\n',modelVar,atmVar); 
    regionFld=[];
    return;
  end
  fldNames=set_adjField( atmVar );
    
else 
  saveDir='mat/dec.240mo';
  fldFile='../../results/samoc/dec.240mo/adxx_*';
  fldNames={'tauu' 'tauv' 'hflux' 'sflux', ...
             'atemp' 'aqh' 'lwdown' 'swdown', ... 
             'precip' 'runoff'};
end

Nfld=length(fldNames);
regionFld = msks;

% --- Loop through all
for n=1:Nfld

  % Load forcing
  if strcmp(exfOrAdxx,'exf')
    load(sprintf('%s/exf_%s.mat',saveDir,fldNames{n}),'exf_fld');
    fld=exf_fld;
    clear exf_fld;
    Nt=size(fld.f1,3);
    fld=convert2gcmfaces(fld);
    fld=fld-repmat(mean(fld,3), [1 1 Nt]);
  else
    load(sprintf('%s/adj_%s.mat',saveDir,fldNames{n}),'adxx');
    fld=adxx; 
    clear adxx;
    Nt=size(fld.f1,3);
  end

  % Get region mean from fld
  for i=1:length(mskNames)
  
    subNames = fieldnames(getfield(msks,mskNames{i}));
    if strcmp(subNames{1},'nFaces')
      
      tmpMsk=convert2gcmfaces(getfield(msks,mskNames{i}));

      tmp = fld.*repmat(tmpMsk,[1 1 Nt]);
      tmp(isnan(tmp))=0;
      tmp = squeeze(sum(sum(tmp,1),2));
      tmp = tmp / nansum(tmpMsk(:));

      % Make a row vector
      tmp=tmp';
  
      % This is to make structure match regionRec
      if ~iscell(getfield(regionFld,mskNames{i}))
        eval(sprintf('regionFld.%s=cell(Nfld,1);',mskNames{i}));
      end
        
      eval(sprintf('regionFld.%s{%d}=tmp;',mskNames{i},n));
      fprintf('-- Done with %s --\n',mskNames{i});
  
    else
      for j=1:length(subNames)
  
        tmpMsk=convert2gcmfaces(getfield(getfield(msks,mskNames{i}),subNames{j}));

        tmp = fld.*repmat(tmpMsk,[1 1 Nt]);
        tmp(isnan(tmp))=0;
        tmp = squeeze(sum(sum(tmp,1),2));
        tmp = tmp / nansum(tmpMsk(:));

        % Make a row vector
        tmp=tmp';
  
        % This is to make structure match regionRec
        if ~iscell(getfield(getfield(regionFld,mskNames{i}),subNames{j}))
          eval(sprintf('regionFld.%s.%s=cell(Nfld,1);',mskNames{i},subNames{j}));
        end
          
        eval(sprintf('regionFld.%s.%s{%d}=tmp;',mskNames{i},subNames{j},n));

        fprintf('-- Done with %s %s --\n',mskNames{i},subNames{j});
      end
    end
  end
end

% Save data structure containing regional external forcing
if strcmp(exfOrAdxx,'exf')
  save(sprintf('%s/region_exf.mat',saveDir),'msks','regionFld');
else
  save(sprintf('%s/region_adxx.mat',saveDir),'msks','regionFld');
end
end
