function [] = plotHfluxDivergence(modelVar,atmVar)
% Plot for SAMOC paper to describe divergence behavior


matfile = sprintf('mat/reconstruct.34S/%s/%s/full/fullReconstruction.mat',modelVar,atmVar);
figfile = sprintf('figures/reconstruct.34S/%s/%s/full/hflux_divergence',modelVar,atmVar);
load(matfile,'samoc_adjFields');

varInd=5; %swdown
tMem=[6, 12, 5*12, 10*12, 15*12, 19*12];

t=1992:2011;
ann=calcAnnualMean(samoc_adjFields{varInd},20);

cc=cbrewer2('Dark2',length(tMem));
fh=figureH; 
fh.Position(3:4)=[0.75 0.88];
%set(gca,'colororder',cc,'nextplot','replacechildren');
ph=plot(t,ann(tMem,:));
ylabel('Sv')
hl=legend('$\tau_{mem} = 6$ months',...
       '$\tau_{mem} = 1$ year',...
       '$\tau_{mem} = 5$ years',...
       '$\tau_{mem} = 10$ years',...
       '$\tau_{mem} = 15$ years',...
       '$\tau_{mem} = 19$ years','Location','Best');
set(ph,{'color'},num2cell(cc,2))
xlim([1992 2012])
grid on
hl.Position=[0.1413    0.7279    0.2125    0.2342];
fs =22;
hl.FontSize=fs;
set(gca,'fontsize',fs);
keyboard
savePlot(figfile,'pdf');
end
