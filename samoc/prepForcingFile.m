function [exf_fld] = prepForcingFile(modelVar,atmVar,adjField,mygrid)
% Want to prepare forcing files for quick reload in reconstruction 
%
% Inputs:
%       
%       modelVar = eccov4r2, eccov4r3, eig, jra55, jra55-gpcp
%       
%       atmVar = short/long - atm or flux
%                e.g. short-atm  or flux
%
%       adjField = e.g. atemp 
%               or for many {'atemp','aqh' }
%       mygrid
%
% Output
%
%       exf_fld : monthly avg exf field over length of record specified by model, atmVar
%
% ------------------------------------------------------------------------------------


% recID matches forcing variable to its position in data.diagnostics
% since I never seemed to output in the same order ... 
% Look for like this:
%   recID(1) = tauu; recID(2) = tauv; ...  
%   tauu, tauv, atemp, aqh, lwdown, swdown, precip, runoff, evap, hflux, sflux
    
if strcmp(modelVar,'eccov4r2')
    if ~isempty(strfind(atmVar,'long'))
        error('Need to run calcLongEccoForcing for this one ...')
    elseif ~isempty(strfind(atmVar,'bwk'))
        rdStr='../../results/samoc/dec.480bwk/diags/exf*';
        recID = [1 2 5 6 7 8 10 11 9 3 4];
    else
        rdStr='../../results/samoc/ref/diags/exf*';
        recID = [1 2 5 6 7 8 10 11 9 3 4];
    end
elseif strcmp(modelVar,'eccov4r3')
  if ~isempty(strfind(atmVar,'long'))
    error('Need to run calcLongEccoForcing for this one ...')
  else
    rdStr='../../results/eccov4r3/ref/diags/exf*';
    recID = [1 2 5 6 7 8 10 11 9 3 4];
  end
elseif strcmp(modelVar,'eig')
  if ~isempty(strfind(atmVar,'long'))
    rdStr='../../results/samoc/era-interim-long/diags/exf*';
    recID = [1 2 3 4 5 6 7 8 0 0 0];
  else
    rdStr='../../results/samoc/era-interim-short/diags/exf*';
    recID = [1 2 3 4 5 6 7 8 0 0 0];
  end
elseif strcmp(modelVar,'jra55')
  if ~isempty(strfind(atmVar,'long'))
    rdStr='../../results/jra55/long-global-llc90/diags/exf*';
    recID = [1 2 5 6 7 8 10 11 0 0 0];
  else
    rdStr='../../results/jra55/short-global-llc90/diags/exf*';
    recID = [1 2 5 6 7 8 10 11 0 0 0];
  end
elseif strcmp(modelVar,'jra55-gpcp')
  if ~isempty(strfind(atmVar,'long'))
    error('No long record')
  else
    rdStr='../../results/jra55-gpcp/short-global-llc90/diags/exf*';
    recID = [1 2 5 6 7 8 10 11 0 0 0];
  end
else
  error(sprintf('modelVar: %s not recognized ... ', modelVar))
end

% ------------------------------------------------------------------------------------

fprintf('Looking for forcing in dir: %s \n',rdStr);

if iscell(adjField)
  for i=1:length(adjField)
     
    [~,~,saveDir]=samoc_dirs('exf-mat',modelVar,atmVar);
    saveFile=sprintf('%s/exf_%s.mat',saveDir,adjField{i});
    if ~exist(saveFile,'file')
      exf_fld=loadForcingFile(saveFile,rdStr,recID);
      fprintf('Saving file: %s \n ',saveFile);
      save(saveFile,'exf_fld');
    else
      fprintf('Found file: %s , skipping ...\n',saveFile);
    end
  end

else

  saveFile=sprintf('mat/exf/%s/%s/exf_%s.mat',modelVar,atmVar,adjField);
  exf_fld=loadForcingFile(saveFile,rdStr,recID);
  fprintf('Saving file: %s \n ',saveFile);
  save(saveFile,'exf_fld');

end

end % end main function

% ------------------------------------------------------------------------------------

function [exf_fld] = loadForcingFile(saveFile,rdStr,recID)

%% Load forcing files
if ~isempty(strfind(saveFile,'tauu')) || ~isempty(strfind(saveFile,'tauv'))

  exf_taux = rdmds2gcmfaces(rdStr,NaN,'rec',recID(1));
  exf_tauy = rdmds2gcmfaces(rdStr,NaN,'rec',recID(2));

  Nt=size(exf_taux.f1,3);
  
  for n=1:Nt
    [exf_taux(:,:,n),exf_tauy(:,:,n)]=calc_UEVNfromUXVY(exf_taux(:,:,n),exf_tauy(:,:,n));
  end
  
  if ~isempty(strfind(saveFile,'tauu'))
    exf_fld=exf_taux;
    save(saveFile,'exf_fld');

    % Kill 2 birds, save tauy as well
    exf_fld=exf_tauy;
    saveFile=strrep(saveFile,'tauu','tauv');
    save(saveFile,'exf_fld');

    % Switch back to taux for saving and since this function
    % outputs exf_fld to reconstruction algorithm 
    exf_fld=exf_taux;
    saveFile=strrep(saveFile,'tauv','tauu');
  else
    exf_fld=exf_tauy;
    save(saveFile,'exf_fld');

    % Save taux as well
    exf_fld=exf_taux;
    saveFile=strrep(saveFile,'tauv','tauu');
    save(saveFile,'exf_fld');

    % Switch back to tauy
    exf_fld=exf_tauy;
    saveFile=strrep(saveFile,'tauu','tauv');
  end

elseif ~isempty(strfind(saveFile,'atemp')) 

  exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(3));
    
elseif ~isempty(strfind(saveFile,'aqh')) 

  exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(4));

elseif ~isempty(strfind(saveFile,'lwdown')) 

  exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(5));
    
elseif ~isempty(strfind(saveFile,'swdown')) 

  exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(6));
    
elseif ~isempty(strfind(saveFile,'precip')) 

  exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(7));
    
elseif ~isempty(strfind(saveFile,'runoff')) 
 
  exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(8));

elseif ~isempty(strfind(saveFile,'evap')) 

  if recID(9)==0
    error('No evap diagnostic with this model, exiting ...')
  else
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(9));
  end

elseif ~isempty(strfind(saveFile,'hflux'))
  % Grab EXFqnet diagnostic
  % This is net cooling of the ocean (>0 => decrease T)
  if recID(10)==0
    error('No hflux diagnostic with this model, exiting...')
  else        
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(10));
  end

elseif ~isempty(strfind(saveFile,'sflux'))
  % Grab EXFempmr diagnostic
  % This is a volumetric salt flux
  if recID(11)==0
    error('No sflux diagnostic with this model, exiting...')
  else        
    exf_fld = rdmds2gcmfaces(rdStr,NaN,'rec',recID(11));
  end
end
end
