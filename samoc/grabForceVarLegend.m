function [legendStr] = grabForceVarLegend(forceVar)

if strcmp(forceVar,'tauu')
  legendStr='Zonal Wind Stress';
elseif strcmp(forceVar,'tauv')
  legendStr='Meridional Wind Stress';
elseif strcmp(forceVar,'wind')
  legendStr='Wind Stress';
elseif strcmp(forceVar,'hflux')
  legendStr='Heat Flux';
elseif strcmp(forceVar,'sflux') || strcmp(forceVar,'fwf')
  legendStr='Fresh Water Flux';
elseif strcmp(forceVar,'buoyancy')
  legendStr='Buoyancy';
elseif strcmp(forceVar,'all')
  legendStr='All Atm. Fluxes';
elseif strcmp(forceVar,'aqh') || strcmp(forceVar,'humidity')
  legendStr='Humidity';
elseif strcmp(forceVar,'atemp')
  legendStr='2m Air Temperature';
elseif strcmp(forceVar,'swdn') || strcmp(forceVar,'swdown')
  legendStr='Shortwave Radiation';
elseif strcmp(forceVar,'lwdn') || strcmp(forceVar,'lwdown')
  legendStr='Longwave Radiation';
elseif ~isempty(strfind(forceVar,'rad'))
  legendStr='Net Radiation';
elseif ~isempty(strfind(forceVar,'warm'))
  legendStr='2m Air Temp. + Net Radiation';
elseif ~isempty(strfind(forceVar,'texas'))
  legendStr='Air Temp. + Radiation + Aqh';
elseif strcmp(forceVar,'evap')
  error('No evaporation in adxx fields ...')
elseif strcmp(forceVar,'preci') || strcmp(forceVar,'precip')
  legendStr='Precipitation';
elseif strcmp(forceVar,'roff') || strcmp(forceVar,'runoff')
  legendStr='Runoff';
else 
  error('forceVar option not recognized, exiting ... ')
  return;
end

end
