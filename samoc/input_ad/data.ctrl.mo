# *********************
# ECCO control variables
# *********************
 &ctrl_nml
 doSinglePrecTapelev=.TRUE.,
#to start from given xx*00.data files
  doinitxx = .FALSE.,
  doMainUnpack = .FALSE.,
#to start from given ecco_ctrl... files
# doinitxx = .FALSE.,
 &
#
# *********************
# names for ctrl_pack/unpack
# *********************
 &ctrl_packnames
 &
#
# *********************
# names for CTRL_GENARR2D,3D CTRL_GENTIM2D
# *********************
 &CTRL_NML_GENARR
#
 xx_genarr3d_weight(1) = 'wt_ones.data',
 xx_genarr3d_file(1)='xx_kapgm',
 mult_genarr3d(1) = 0.,
#
 xx_genarr3d_weight(2) = 'wt_ones.data',
 xx_genarr3d_file(2)='xx_kapredi',
 mult_genarr3d(2) = 0.,
#
 xx_genarr3d_weight(3) = 'wt_ones.data',
 xx_genarr3d_file(3)='xx_diffkr',
 mult_genarr3d(3) = 0.,
#
 xx_gentim2d_weight(1) = 'wt_ones.data',
 xx_gentim2d_file(1) = 'xx_tauu',
 xx_gentim2d_startdate1(1) = 19920101,
 xx_gentim2d_startdate2(1) = 60000,
 xx_gentim2d_period(1) = 2419200.0,
 mult_gentim2d(1) = 0.,
#
 xx_gentim2d_weight(2) = 'wt_ones.data',
 xx_gentim2d_file(2) = 'xx_tauv',
 xx_gentim2d_startdate1(2) = 19920101,
 xx_gentim2d_startdate2(2) = 60000,
 xx_gentim2d_period(2) = 2419200.0,
 mult_gentim2d(2) = 0.,
#
 xx_gentim2d_weight(3) = 'wt_ones.data',
 xx_gentim2d_file(3) = 'xx_aqh',
 xx_gentim2d_startdate1(3) = 19920101,
 xx_gentim2d_startdate2(3) = 60000,
 xx_gentim2d_period(3) = 2419200.0,
 mult_gentim2d(3) = 0.,
#
 xx_gentim2d_weight(4) = 'wt_ones.data',
 xx_gentim2d_file(4) = 'xx_atemp',
 xx_gentim2d_startdate1(4) = 19920101,
 xx_gentim2d_startdate2(4) = 60000,
 xx_gentim2d_period(4) = 2419200.0,
 mult_gentim2d(4) = 0.,
#
 xx_gentim2d_weight(5) = 'wt_ones.data',
 xx_gentim2d_file(5) = 'xx_swdown',
 xx_gentim2d_startdate1(5) = 19920101,
 xx_gentim2d_startdate2(5) = 60000,
 xx_gentim2d_period(5) = 2419200.0,
 mult_gentim2d(5) = 0.,
#
 xx_gentim2d_weight(6) = 'wt_ones.data',
 xx_gentim2d_file(6) = 'xx_lwdown',
 xx_gentim2d_startdate1(6) = 19920101,
 xx_gentim2d_startdate2(6) = 60000,
 xx_gentim2d_period(6) = 2419200.0,
 mult_gentim2d(6) = 0.,
#
 xx_gentim2d_weight(7) = 'wt_ones.data',
 xx_gentim2d_file(7) = 'xx_precip',
 xx_gentim2d_startdate1(7) = 19920101,
 xx_gentim2d_startdate2(7) = 60000,
 xx_gentim2d_period(7) = 2419200.0,
 mult_gentim2d(7) = 0.,
#
 xx_gentim2d_weight(8) = 'wt_ones.data',
 xx_gentim2d_file(8) = 'xx_runoff',
 xx_gentim2d_startdate1(8) = 19920101,
 xx_gentim2d_startdate2(8) = 60000,
 xx_gentim2d_period(8) = 2419200.0,
 mult_gentim2d(8) = 0.,
#
 &
