function [] = createHfluxPerturbation(mygrid)
% Want to create control vector for perturbation experiment. 
% Perturb the area around Greenland with a 10 W/m^2 cooling/heating. 
% 
%   dhf = perturbation scalar, denotes cooling or heating and magnitude per area box
% ----------------------------------------------------------------------------------

dhf = -10; %[W/m^2]

ctrlDir = 'control-vectors/'; if ~exist(ctrlDir,'dir'), createSensControlVectors(mygrid); end
saveDir = 'hf-perturbation/'; if ~exist(saveDir,'dir'), mkdir(saveDir); end;
prefix=[ctrlDir 'xx_'];
suffix='.0000000012.data';

% Load hflux from aqh, doesn't matter b/c all zeros
hflux=read_bin([prefix 'aqh' suffix]);

% Mask the area around Greenland and add perturbation for e/ mo over a yr
msks=createRegionMasks(mygrid);

for n=1:12
	hflux(:,:,n) = dhf.*msks.north;
end

% Make the directory with perturbation and this script
hflux=convert2gcmfaces(hflux);
hflux(isnan(hflux))=0;
write2file([saveDir 'xx_hflux' suffix],hflux);   % hflux=10W/m^2
%write2file([saveDir 'xx_atemp' suffix],hflux/5); % atemp = 2 degC (?)
copyfile('createHfluxPerturbation.m',saveDir);
tar('hf-perturbation.tar.gz',saveDir);

end
