function [amoc, depthmax, kmax, ovStf] = calcAMOC(slat, velType, loadDir, resultsDir,diagsName)
% Compute AMOC at some latitude 
% Use output from calcAtlOverturn, which computes the overturning stream function
% from [U,V]VELMASS diagnostic output.
%
%  Inputs: 
% 	
%       slat : latitude for computing AMOC 
%       velType : eulerian (default), bolus, residual velocity fields
%                 mht returns atlantic meridional heat transport
%       loadDir (optional) : where to load atlOverturn.mat from
%                            default: mat/eccov4r2/state/
%       resultsDir (optional) : where to pull UVELMASS, VVELMASS from
%                               default: ../../results/eccov4r2/ref/diags
%       diagsName (optional) : name of diagnostics file where [U,V]VELMASS recs exist
%                               default: trsp_3d_set1
%
%  Outputs:
%
%	amoc : [1x240] vector with AMOC at [lat] using velocity fields specified
%	depthMax : same size, giving max depth at which samoc was computed [NaN if mht selected]
%	kmax : gives depth level corresponding to depthmax [NaN if mht selected]
%       ovStf : overturning stream function [NaN if mht selected]
%
% ---------------------------------------------------------------------------------

%% --- mygrid global variable
mygrid=establish_mygrid('llc90');
if ~exist('slat','var'), slat=-34; fprintf('Defining latitude at 34S\n');end
if nargin<2, velType='eulerian'; end
if nargin<3, loadDir = 'mat/eccov4r2/state/'; end
if nargin<4, saveDir = '../../results/eccov4r2/ref/diags/'; end;
if nargin<5, diagsName = 'trsp_3d_set1'; end;

matfile = sprintf('%satlOverturn.mat',loadDir);

%% Load or compute overturning streamfunction
if ~exist(matfile,'file')
    [atlOvStf] = calcAtlOverturn(loadDir,resultsDir,diagsName);
else
    load(matfile,'atlBolusOv','atlOvStf','atlResOv','atlTrVolZon','atlMht');
end

%% Grab samoc latitude
iy = find(mygrid.LATS==slat);
if strcmp(velType,'eulerian')
    ovStf = squeeze(atlOvStf(iy,:,:));
elseif strcmp(velType,'bolus')
    ovStf=squeeze(atlBolusOv(iy,:,:));
elseif strcmp(velType,'residual')
    ovStf = squeeze(atlResOv(iy,:,:));
elseif strcmp(velType,'trVolZon')
    ovStf = squeeze(atlTrVolZon(iy,:,:));
elseif strcmp(velType,'mht')
    amoc = squeeze(atlMht(iy,:));
    depthmax=NaN;
    kmax=NaN;
    ovStf=NaN;
else
    fprintf('ERROR: Unrecognized velocity option in computing samoc\n')
end

%% AMOC is integrated (monthly Eulerian mean) transport at maximized depth
if ~strcmp(velType,'mht')
  Nt = size(atlOvStf,3);
  kmax = zeros(1,Nt);
  amoc = zeros(1,Nt);
  depthmax = zeros(1,Nt);
  for n = 1:Nt
      [~,kmax(n)] = max(ovStf(:,n));
      amoc(n) = ovStf(kmax(n),n);
      depthmax(n) = abs(mygrid.RF(kmax(n)));
  end
end
end
