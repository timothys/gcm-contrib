function [saveDir,plotDir,exfSaveDir,exfPlotDir] = samoc_dirs(dirType,modelVar,atmVar,varargin)
% Establish directory tree for SAMOC computations
%
% Inputs: 
%
%       dirType: 'reconstruct/exf-mat/plot' ... specifies where to go
%               e.g. 'reconstruct-mat'
%       
%       modelVar: 'eccov4r2', 'eccov4r3', 'eig', 'jra55', 'jra55-gpcp' 
%       atmVar: 'short/long-atm' or 'flux'
% 
%   Optional:
%
%       dirString: 'full' (default) .. could be 'acc' for reconstruct, or 'tauu' ... 
%
% ---------------------------------------------------------------------------------------------

dirString = 'full';
for n=1:length(varargin)
  if isa(varargin{n},'char')
    dirString=varargin{n};
  else
    error(sprintf('Argument %d / %d not recognized ...',n,length(varargin)))
  end
end

% For now, assuming 34S
%if lat < 0, dirName = sprintf('%dS',abs(lat)); else dirName=sprintf('%dN',lat);end;

% Reconstruct-mat
  mainSaveDir=sprintf('mat/reconstruct.34S');
  modelSaveDir=sprintf('%s/%s',mainSaveDir,modelVar);
  atmSaveDir = sprintf('%s/%s',modelSaveDir,atmVar);
  saveDir = sprintf('%s/%s',atmSaveDir,dirString);

% Reconstruct-plot
  mainPlotDir=sprintf('figures/reconstruct.34S');
  modelPlotDir=sprintf('%s/%s',mainPlotDir,modelVar);
  atmPlotDir = sprintf('%s/%s',modelPlotDir,atmVar);
  plotDir = sprintf('%s/%s',atmPlotDir,dirString);

% Exf-mat
  mainExfDir=sprintf('mat/exf');
  modelExfDir=sprintf('%s/%s',mainExfDir,modelVar);
  atmExfDir=sprintf('%s/%s',modelExfDir,atmVar);
  exfSaveDir=sprintf('%s/%s',atmExfDir,dirString);

% exf-plot
  mainExfPlotDir=sprintf('figures/exf');
  modelExfPlotDir=sprintf('%s/%s',mainExfPlotDir,modelVar);
  atmExfPlotDir=sprintf('%s/%s',modelExfPlotDir,atmVar);
  exfPlotDir=sprintf('%s/%s',atmExfPlotDir,dirString);

% --- Mat directories
if strcmp(dirType,'reconstruct-mat')

  if ~exist(mainSaveDir,'dir'), mkdir(mainSaveDir); end;
  if ~exist(modelSaveDir,'dir'), mkdir(modelSaveDir); end;
  if ~exist(atmSaveDir,'dir'), mkdir(atmSaveDir); end;
  if ~exist(saveDir,'dir'), mkdir(saveDir);end;
  
elseif strcmp(dirType,'reconstruct-plot')

  % --- Figure directories
  if ~exist(mainPlotDir,'dir'), mkdir(mainPlotDir); end;
  if ~exist(modelPlotDir,'dir'), mkdir(modelPlotDir); end;
  if ~exist(atmPlotDir,'dir'), mkdir(atmPlotDir); end;
  if ~exist(plotDir,'dir'), mkdir(plotDir);end;

elseif strcmp(dirType,'exf-mat')

  % --- Exf directories
  if ~exist(mainExfDir,'dir'), mkdir(mainExfDir); end;
  if ~exist(modelExfDir,'dir'), mkdir(modelExfDir); end;
  if ~exist(atmExfDir,'dir'), mkdir(atmExfDir); end;
  if strcmp(dirString,'full')
    exfSaveDir=atmExfDir;
  else
    if ~exist(exfSaveDir,'dir'), mkdir(exfSaveDir);end;
  end
  
elseif strcmp(dirType,'exf-plot')

  % --- Exf plot directories
  if ~exist(mainExfPlotDir,'dir'), mkdir(mainExfPlotDir); end;
  if ~exist(modelExfPlotDir,'dir'), mkdir(modelExfPlotDir); end;
  if ~exist(atmExfPlotDir,'dir'), mkdir(atmExfPlotDir); end;
  if ~exist(exfPlotDir,'dir'), mkdir(exfPlotDir);end;

else
  error(sprintf('dirType %s not recognized ...', dirType));
end

end
