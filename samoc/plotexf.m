function [] = plotexf(mygrid)
%% Plot exf diagnostics
% -------------------------------------------------------------------------


dataDir = '../../results/samoc/ref/diags/';
mainMatDir  = 'mat/eccov4r2/';
mainPlotDir = sprintf('figures/eccov4r2/'); 
matDir = [mainMatDir 'exf/'];
plotDir = [mainPlotDir 'exf/'];

if ~exist(mainMatDir,'dir'), mkdir(mainMatDir); end;
if ~exist(mainPlotDir,'dir'), mkdir(mainPlotDir); end;
if ~exist(matDir,'dir'), mkdir(matDir); end;
if ~exist(plotDir,'dir'), mkdir(plotDir); end;

exfField={'EXFtaux' 'EXFtauy' 'EXFqnet' 'EXFempmr' 'EXFatemp' 'EXFaqh' 'EXFlwdn' 'EXFswdn' 'EXFevap' 'EXFpreci' 'EXFroff'};
Nexf = length(exfField);
cunits = {'N/m^2','N/m^2','W/m^2','m/s',...
          'K','kg/kg','W/m^2','W/m^2','m/s','m/s','m/s'};
caxLim = {[-.15,.15],[-.1,.1],[-500,500],[-1e-7,1e-7],...
          [-15,15],[0,0.02],[-50,50],[-250,250],[-1e-8,1e-8],[-1e-7,1e-7],[-1e-7,1e-7]};

%% First prep wind stress
matfile = [matDir 'EXFtaux.mat'];
if ~exist(matfile,'file')
  taux=rdmds2gcmfaces([dataDir 'exf*'],NaN,'rec',1);
  tauy=rdmds2gcmfaces([dataDir 'exf*'],NaN,'rec',2);
  
  for n=1:240
    [taux(:,:,n),tauy(:,:,n)] = calc_UEVNfromUXVY(taux(:,:,n),tauy(:,:,n));
  end

  xx_fld=taux;
  save(matfile,'xx_fld');

  matfile = [matDir 'EXFtauy.mat'];
  xx_fld=tauy;
  save(matfile,'xx_fld');

  clear taux tauy;
end
      
%% Plot and save at various time steps
for i = 1:Nexf

  matfile = [matDir exfField{i} '.mat'];
  
  % Make sure mat file is made
  if ~exist(matfile,'file')
    xx_fld=rdmds2gcmfaces([dataDir 'exf*'],NaN,'rec',i);
    save(matfile,'xx_fld');
  else
    load(matfile,'xx_fld');
  end

  xx_mean=nanmean(xx_fld,3);
  xx_fld=xx_fld-repmat(xx_mean,[1 1 240]);

  figFile = sprintf('%s%s_mean',plotDir,exfField{i});
  
  %% Save mean field
  if ~exist([figFile '.png'],'file')

    caxLim = {[-.25,.25],[-.2,.2],[-500,500],[-1e-7,1e-7],...
          [-10,30],[0,0.02],[0, 450],[0,300],[0,1e-7],[0,1.5e-7],[0,1e-7]};

    if ~strcmp(exfField{i},'EXFatemp')
      clbl=cunits{i};
    else
      clbl='degC';
      xx_mean=xx_mean-273.15;
    end

    if ( ~isempty(strfind(exfField{i},'tau')) || strcmp(exfField{i},'EXFqnet') || strcmp(exfField{i},'EXFempmr') )
      colorScheme='redblue';
    else
      colorScheme='jet';
    end

    strs = struct(...
        'xlbl',sprintf('%s mean',exfField{i}), ...
        'time','',...
        'clbl',clbl,...
        'vidName',figFile);
    opts = struct('saveVideo',1,...
                  'caxLim',caxLim{i},...
                  'mmapOpt',2.1,...
                  'colorScheme',colorScheme,...
                  'noaxLims',1,...
                  'figType','square',...
                  'multiPanelGlobe',1);
    plotVideo(xx_mean,strs,opts,mygrid);
  end

  vidFile = sprintf('%s%s_clim',plotDir,exfField{i});

  if ~exist([vidFile '.mp4'],'file')

    xx_clim = calcClimatology(xx_fld,20);

    %% Climatology video
    caxLim = {[-.15,.15],[-.1,.1],[-250,250],[-1e-7,1e-7],...
          [-15,15],[-4e-3,4e-3],[-50,50],[-250,250],[-1e-8,1e-8],[-1e-7,1e-7],[-1e-7,1e-7]};
    strs = struct(...
        'xlbl',sprintf('%s climatology',exfField{i}), ...
        'time','months',...
        'clbl',cunits{i},...
        'vidName',vidFile);
    opts = struct('saveVideo',1,...
                  'caxLim',caxLim{i},...
                  'mmapOpt',2.1,...
                  'colorScheme','redblue',...
                  'noaxLims',1,...
                  'figType','square',...
                  'multiPanelGlobe',1);
    plotVideo(xx_clim,strs,opts,mygrid);

  end

  vidFile=strrep(vidFile,'clim','annual');
  if ~exist([vidFile '.mp4'],'file')

    xx_annual = calcAnnualMean(xx_fld,20);

    %% Annual average
    caxLim = {[-.1,.1],[-.1,.1],[-100,100],[-5e-8,5e-8],...
          [-2,2],[-1e-3,1e-3],[-20,20],[-30,30],[-1e-8,1e-8],[-5e-8,5e-8],[-1e-10,1e-10]};
    strs = struct(...
        'xlbl',sprintf('%s annual avg',exfField{i}), ...
        'time','years',...
        'clbl',cunits{i},...
        'vidName',vidFile);
    opts = struct('saveVideo',1,...
                  'caxLim',caxLim{i},...
                  'mmapOpt',2.1,...
                  'colorScheme','redblue',...
                  'noaxLims',1,...
                  'figType','square',...
                  'multiPanelGlobe',1);
    plotVideo(xx_annual,strs,opts,mygrid);

  end
end
end
