function [cc] = grabSamocGroupColors(nColors)
% Get nice color schemes for samoc paper
% Uses 'Paired' qualitative color scheme, the darker of the schemes with 12 colors
%
% Input:
%
%       nColors: num. colors to grab
%
% Output:
%
%       cc: the lovely color map
%
% --------------------------------------------------------------------------------


cc=zeros(nColors,3);
cc0=cbrewer2('Paired',12);
cc(1:12,:)=cc0;

if nColors==16

  % Give Pac Subpolar and ACC<60S same color (they don't collide)
  cc(16,:)=[0,51,102]/255;%cc(2,:);

  % Give Pac South an interesting turquoise
  cc(13,:)=[141,211,199]/255;

  % Make Ind. South a faint green
  cc(15,:)=cc(3,:);%[0,153,76]/255;%[179,250,105]/255;

  % Make Pac N. subtropics forest (?) green
  cc(3,:)=[0,102,51]/255;

  % Make Atl South a burnt red
  cc(14,:)=[160,0,0]/255;%cc(3,:);
  
elseif nColors==13
  error('haven''t worked this case of colors out yet');
end
  
end
