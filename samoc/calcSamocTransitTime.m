function [] = calcSamocTransitTime()
% NOTE: This should not be used yet, this is working with a premature idea of what
%       the "mean transit time" is with time varying controls. In actuality it is more
%       complicated because the integral needs to be over memory time rather than run time
%       which means we need dJ(x,y,s_mem) for s_mem \in (0,19yrs) which will take A LOT
%       of runs.
%
%
%
%
% Compute time weighted response as in (Heimbach, 2011)
% However, more complicated here with time varying controls
% Given objective function J(t), controls u(x,y,t), sensitivity dJ/du(x,y,t_lag)
% "Transit time" is:
%
%       T(x,y) = int_{0}^{max(tau_mem)} ( s_mem|dJ(x,y,s_mem)|ds_mem ) 
%                / 
%                int_{0}^{max(tau_mem)} ( dJ(x,y,s_mem) ds)
%   
%  where
%
%       dJ(x,y,t) = int_{t-tau_mem}^{t}( dJ/du(x,y,s-t) u(x,y,s) ds
%
%  and tau_mem is memory time. This is computed by samocReconstruction2D.m
%
% --------------------------------------------------------------------------------


% --- Load global grid variable
mygrid = establish_mygrid('llc90');

% --- Required inputs
modelVar = 'eccov4r2';
atmVar = 'long-atm';
lat = -34; 
tau_mem = 229; % 19 yrs plus one month

% --- Want contribution from final record 
sensFrac = calcSamocSensFrac();

% --- Compute dJ(x,y,t) as above
%     output is a cell structure with reconstruction from
%     {tauu,tauv,aqh,atemp,swdown,lwdown,precip,runoff}
samoc_adjFields = samocReconstruction2D(modelVar,atmVar,lat,tau_mem,


% --- Compute transit time
Nadj = length(samoc_adjFields);
transit_time = cell(Nadj,1);

for i=1:Nadj
  transit_time{i} = convert2gcmfaces(squeeze(nansum

