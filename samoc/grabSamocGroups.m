function [stackedReconstruction, stackedMasks] = grabSamocGroups(varargin) 
% Script for stacking regionally restricted reconstructions into one matrix
%
% Inputs:
%       
%       separator: string indicating specific sections to grab
%               'ek' : separates local band around 34S from SSub Trop Atl
%               'south' : separates southern ocean into pacific, atl, ind, and south of 60S
%               'none' (default) : does neither
%       tMem (int32): lead time for reconstruction
%               default = 229 months
%       flux_ind: index of fluxes to grab 1=tau_x 2=tau_y 3=hflux 4=sflux/fwf
%               default = 1
%       regionRec: structure containing regional reconstructions
%               no default
%       msks : structure containing gcmfaces objects masking off geographical regions 
%               no default      
%       mygrid
%
% Output:
%
%       stackedReconstruction: Matrix of reconstructions regions go by rows, reconstructed record in columns
%       stacked Masks: 3D gcmfaces object with various regions listed in 3rd dim.
%
% -----------------------------------------------------------------------------


for n=1:length(varargin)
  if isa(varargin{n},'int32'), tMem=varargin{n};
  elseif isa(varargin{n},'char'), separator=varargin{n};
  elseif isa(varargin{n},'double'), flux_ind=varargin{n};
  elseif isfield(varargin{n},'acc')
    if isa(varargin{n}.acc,'gcmfaces') 
      msks=varargin{n};
    else
      regionRec=varargin{n};
    end
  elseif isfield(varargin{n},'XC'),mygrid=varargin{n};
  end
end

if ~exist('separator','var')
  separator='none';
end

% --- If grab reconstruction
if exist('regionRec','var')
  if ~exist('tMem','var')
    tMem=229;
    fprintf('No lead time given, assuming tMem=%d\n',tMem);
  end
  if ~exist('flux_ind','var')
    flux_ind=1;
    fprintf('No flux index given, assuming flux_ind=%d\n',flux_ind);
  end

  Nt=size(regionRec.acc{1}(tMem,:),2);
  stackedReconstruction=zeros(12,Nt);
  if strfind(separator,'ek')
    stackedReconstruction=cat(1,stackedReconstruction,zeros(1,Nt));
  end
  if strfind(separator,'south')
    stackedReconstruction=cat(1,stackedReconstruction,zeros(3,Nt));
  end

  % --- Grab reconstruction by region
  for i=flux_ind(1):flux_ind(end)
    tmp=[regionRec.arc{i}(tMem,:)+regionRec.bar{i}(tMem,:);...
      regionRec.pac.subPolar{i}(tMem,:);...
      regionRec.pac.NStrop{i}(tMem,:); ...
      regionRec.pac.Ntrop{i}(tMem,:) + regionRec.pac.Strop{i}(tMem,:); ... 
      regionRec.pac.SStrop{i}(tMem,:); ...
      regionRec.atl.subPolar{i}(tMem,:);...
      regionRec.atl.NStrop{i}(tMem,:); ... 
      regionRec.atl.Ntrop{i}(tMem,:) + regionRec.atl.Strop{i}(tMem,:)];

    if strfind(separator,'ek')
      tmp=cat(1,tmp,...
      [regionRec.atl.SStrop{i}(tMem,:); ...
      regionRec.atl.ek{i}(tMem,:)]);
    else
      tmp=cat(1,tmp,...
        [regionRec.atl.SStrop{i}(tMem,:)+ ...
        regionRec.atl.ek{i}(tMem,:)]);
    end

    tmp=cat(1,tmp,...
      [regionRec.ind.NStrop{i}(tMem,:)+ regionRec.ind.Ntrop{i}(tMem,:) + regionRec.ind.Strop{i}(tMem,:); ... 
      regionRec.ind.SStrop{i}(tMem,:)]);

    if strfind(separator,'south')
      tmp=cat(1,tmp,...
        [regionRec.pac.south{i}(tMem,:); regionRec.atl.south{i}(tMem,:);  regionRec.ind.south{i}(tMem,:); ...
        regionRec.acc{i}(tMem,:)]); 
    else
      tmp=cat(1,tmp,...
        [regionRec.pac.south{i}(tMem,:)+ regionRec.atl.south{i}(tMem,:)+  regionRec.ind.south{i}(tMem,:)+ ...
        regionRec.acc{i}(tMem,:)]); 
    end

    stackedReconstruction=stackedReconstruction+tmp;

  end
else
  stackedReconstruction=0;
end

% -----------------------------------------------------------------------------

if exist('msks','var') 

  % --- Grab masks by region and stack on 3rd dim
  stackedMasks= cat(3,...
    convert2gcmfaces(msks.arc+msks.bar),...
    convert2gcmfaces(msks.pac.subPolar),...
    convert2gcmfaces(msks.pac.NStrop), ...
    convert2gcmfaces(msks.pac.Ntrop + msks.pac.Strop), ... 
    convert2gcmfaces(msks.pac.SStrop), ...
    convert2gcmfaces(msks.atl.subPolar),...
    convert2gcmfaces(msks.atl.NStrop),...
    convert2gcmfaces(msks.atl.Ntrop + msks.atl.Strop)); 
  
  if strfind(separator,'ek')
    stackedMasks=cat(3,stackedMasks,...
      convert2gcmfaces(msks.atl.SStrop),...
      convert2gcmfaces(msks.atl.ek));
  else
    stackedMasks=cat(3,stackedMasks,...
      convert2gcmfaces(msks.atl.SStrop + msks.atl.ek));
  end
  
  stackedMasks=cat(3,stackedMasks,...
    convert2gcmfaces(msks.ind.NStrop+ msks.ind.Ntrop + msks.ind.Strop), ... 
    convert2gcmfaces(msks.ind.SStrop));
  
  if strfind(separator,'south')
    stackedMasks=cat(3,stackedMasks,...
      convert2gcmfaces(msks.pac.south),...
      convert2gcmfaces(msks.atl.south),...
      convert2gcmfaces(msks.ind.south),...
      convert2gcmfaces(msks.acc));
  else
    stackedMasks=cat(3,stackedMasks,...
      convert2gcmfaces(msks.pac.south +...
                       msks.atl.south +...
                       msks.ind.south +...
                       msks.acc));
  end
  
  stackedMasks=convert2gcmfaces(stackedMasks);
else
  msks=0;
end

end
