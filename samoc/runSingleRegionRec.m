function[] = runSingleRegionRec(modelVar,atmVar,regionMask, regionMaskName )
% Reconstruction of only one region
% Input the region mask and name for reconstruction 

% --- Adding last bit of sensitivity
sensFrac=calcSamocSensFrac();

matFile = sprintf('mat/reconstruct.34S/%s/%s/%s/fullReconstruction.mat',modelVar,atmVar,regionMaskName);
if ~exist(matFile,'file')
    tmp = samocReconstructionByMonth(modelVar,atmVar,-34,regionMask,regionMaskName,sensFrac);
else
    load(matFile,'samoc_adjFields');
    tmp = samoc_adjFields;
    clear samoc_adjFields;
end

% --- Load region rec and add to it
region_rec_file = sprintf('mat/reconstruct.34S/%s/%s/region_rec.mat',modelVar,atmVar);

if exist(region_rec_file)
    load(region_rec_file,'regionRec')
else
    msks = createRegionMasks('ekman');
    regionRec = msks;
end

regionRec = setfield(regionRec,regionMaskName,tmp);

% Save data structure containing regional reconstruction
new_region_rec_file = sprintf('mat/reconstruct.34S/%s/%s/%s/region_rec.mat',modelVar,atmVar,regionMaskName);
save(new_region_rec_file,'regionMask','regionMaskName','regionRec');
end
