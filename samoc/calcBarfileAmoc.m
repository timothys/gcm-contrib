function [amoc] = calcBarfileAmoc(results_dir,avgPeriod)
% Making this quickly first, compute AMOC from the file e.g.
%
%   m_trVol_month.*.data
%
% where "month" can change depending on inputs
%
% Input:
%
%   results_dir - location of m_* file
%
%   avgPeriod - 'month', 'day', 'step' ...
%
% Output:
%
%   amoc - in [Sv], a vector [1 x Nrecs] 
%           where Nrecs is num. recs in m_trVol file
%
% ----------------------------------------------------

barfile = sprintf('%s/m_trVol_%s',results_dir,avgPeriod);

% Get volumetric transport per grid cell
trVol = rdmds2gcmfaces(barfile,12); % [m^3/s]

% From masking already done in m_ file
% compute zonal integratl
zonSumTrVol = squeeze(nansum(nansum(convert2gcmfaces(trVol),1),2)); %[m^3/s]

% Integrate vertically and multiply by -1 b/c integrating from depth
cumSumTrVol = -flipdim(cumsum(flipdim(zonSumTrVol,1)),1); %[m^3/s]

% Convert to Sverdrups
cumSumTrVol = 10^-6 * cumSumTrVol;

amoc = max(cumSumTrVol);

end
