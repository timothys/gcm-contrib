% Plot AMOC properties for Atlantic basin

%% Set up dirs
mygrid = establish_mygrid('llc90');
read_dir = '../../release2/nctiles_monthly/';
fig_dir = 'figures/'; if ~exist(fig_dir,'dir'), mkdir(fig_dir), end;
mat_dir = 'mat/'; if ~exist(mat_dir,'dir'), mkdir(mat_dir), end;

Ny = length(mygrid.LATS_MASKS);
Nr = length(mygrid.RF);
Nt = 240;

%% Load file
if ~exist(sprintf('%satlOverturn.mat',mat_dir),'file')
    [atlOvStf,atlBolusOv,atlResOv,atlTrVolZon]=calcAtlOverturn;
else
    load(sprintf('%satlOverturn.mat',mat_dir));
end

%% Compute AMOC 
atlKmax = zeros(Ny,Nt);
atlAmoc = zeros(Ny,Nt);
atlDepthmax = zeros(Ny,Nt);
for iy = 1:Ny
    for n = 1:Nt
        [~,atlKmax(iy,n)] = max(squeeze(atlOvStf(iy,:,n)));
        atlAmoc(iy,n) = atlOvStf(iy,atlKmax(iy,n),n);
        atlDepthmax(iy,n) = abs(mygrid.RF(atlKmax(iy,n)));
    end
end

%% Plotting
X=mygrid.LATS*ones(1,length(mygrid.RF)); Y=ones(length(mygrid.LATS),1)*(mygrid.RF(1:end)');

figureL; 
xlbl = 'Latitude';
ylbl = 'Depth (m)';
title0=sprintf('1992-2011 Mean\nAtlantic Meridional Overturning Stream Function');
doubleDepthPlot({X,Y},mean(atlOvStf,3),{xlbl,ylbl,title0,'pcolor'});
colorbar

figureL;
title0=sprintf('1992-2011 Mean\nAtlantic Meridional Bolus Overturning Stream Function');
doubleDepthPlot({X,Y},mean(atlBolusOv,3),{xlbl,ylbl,title0,'pcolor'});
colorbar

figureL;
title0=sprintf('1992-2011 Mean\nAtlantic Meridional Residual Overturning Stream Function');
doubleDepthPlot({X,Y},mean(atlResOv,3),{xlbl,ylbl,title0,'pcolor'});
colorbar

figureL;
title0=sprintf('1992-2011 Mean\nAtlantic Meridional Volume Transport at Depth');
doubleDepthPlot({X,Y},mean(atlTrVolZon,3),{xlbl,ylbl,title0,'pcolor'});
colorbar

figureL
latVect = [mygrid.LATS(1):mygrid.LATS(end)]';
grayCol = [192 192 192]/255;
subplot(2,1,1),plot(latVect,atlAmoc,'Color',grayCol)
        hold on
        plot(latVect,mean(atlAmoc,2),'k')
        plot(latVect,(mean(atlAmoc,2)+std(atlAmoc,0,2)),'k--')
        plot(latVect,(mean(atlAmoc,2)-std(atlAmoc,0,2)),'k--')
        hold off
    xlim([-35 70])
    ylim([0 30])
    xlabel('Month (1992-2011)')
    ylabel('AMOC (Sv)')
    title('AMOC Characteristics from ECCOv4r2 Fields')
subplot(2,1,2),plot(latVect,atlDepthmax,'Color',grayCol)
        hold on
        plot(latVect,median(atlDepthmax,2),'k')
        plot(latVect,(median(atlDepthmax,2)+std(atlDepthmax,0,2)),'k--')
        plot(latVect,(median(atlDepthmax,2)-std(atlDepthmax,0,2)),'k--')
        hold off
    xlim([-35 70])
    ylim([0 1500])
    xlabel('Month (1992-2011)')
    ylabel('z_{max} (m)')
    set(gca,'ydir','reverse')
