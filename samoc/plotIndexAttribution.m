function [] = plotIndexAttribution(indSelect,varargin)
% Script for plotting correlations between regional contributions to SAMOC and
% indices like ENSO, IPO, SAM, AMV ...
%
% Input: 
%
%       indSelect : select index to correlate ... 
%               enso - enso34 index
%               ipo - interdecadal pacific oscillation
%               amo - antlantic meridional oscillation
%               all - all of the above
%
% Optional inputs:
%
%       avgSelect : 'annual' (default) or 'anomaly' for 
%                    annual avg or anomaly from climatology
%       plotGlobe : 'false' (default) or 'globe'
%                    plots globe showing reconstruction area
%
% -----------------------------------------------------------------------------

% Parameters
modelVar='eccov4r2';
atmVar='long-atm';
Nt = 240;
tMem=229;
t=[1:Nt]./12 + 1992;
tYrs=1992:2011;
Nyrs=length(tYrs);
avgSelect = 'annual';
plotGlobe='false';

for n=1:length(varargin)
    if strcmp(varargin{n},'annual')||strcmp(varargin{n},'anomaly')
        avgSelect=varargin{n};
    elseif ~isempty(strfind(varargin{n},'globe'))
        plotGlobe='globe';
    else
        error(sprintf('variable arg. number %d not recognized ...',n));
    end

end


% Load the data
load(sprintf('mat/reconstruct.34S/%s/%s/region_rec.mat',modelVar,atmVar));
dataDir = 'index-data/';
matDir = 'mat/index-data/';
if ~exist(matDir,'dir'), error('Can''t find mat dir mat/index-data'); end;
plotDir = 'figures/index-data/';
if ~exist(plotDir,'dir'), mkdir(plotDir); end;

%% --- ENSO and IPO Plot
if ~isempty(strfind(indSelect,'EnsoAndIpo')) || ~isempty(strfind(indSelect,'all'))
    matfile = '../enso-samba/mat/reynolds/sst_enso_34.mat';
    if exist(matfile,'file')
        load(matfile,'enso_obs');
    else
        error('Need to prep enso data: %s ...\n',matfile);
    end

    % Set up ENSO stuff
    dJ=regionRec.pac.Ntrop{1}(tMem,:) + regionRec.pac.Strop{1}(tMem,:);
    dJ=dJ+regionRec.pac.NStrop{1}(tMem,:) + regionRec.pac.SStrop{1}(tMem,:);
    dJ=dJ+regionRec.pac.Strop{7}(tMem,:) + regionRec.pac.Strop{7}(tMem,:);
    %dJ=dJ+regionRec.pac.NStrop{2}(tMem,:) + regionRec.pac.NStrop{2}(tMem,:);

    if strcmp(avgSelect,'annual')
        dJ=calcAnnualMean(dJ,20);
        enso_obs=calcAnnualMean(enso_obs,20);
    else
        clim = calcClimatology(dJ,Nyrs);
        dJ=dJ - repmat(clim,[1 Nyrs]);
    end

    pp_enso=polyfit(enso_obs,dJ,1);
    ensoFit=polyval(pp_enso,enso_obs);
    r_squared_enso=corrcoef(enso_obs,dJ);

    % Set up IPO stuff
    matfile = [matDir 'ipo_unfiltered.mat'];
    if exist(matfile,'file')
      load(matfile,'ipoInd');
    else
      fprintf('Need to prep ipo data\n');
      return;
    end
    ind92=1465;
    tauLag=0;
    ipoInd=ipoInd(ind92-tauLag:ind92-tauLag+Nt-1);

    if strcmp(avgSelect,'annual')
      ipoInd=calcAnnualMean(ipoInd,20);
    end  
    
    pp_ipo=polyfit(ipoInd,dJ,1);
    ipoFit=polyval(pp_ipo,ipoInd);
    r_squared_ipo=corrcoef(ipoInd,dJ);

    % Put it all together
    ylbls = {'ENSO Index','IPO Index'};
    legendStr = {'$\delta J_{\tau_x,Pac}$','ENSO 3.4 Index','IPO Index'};
    savename=sprintf('%senso_and_ipo_%s',plotDir,avgSelect);

    indices={enso_obs,ipoInd};
    fits = {ensoFit,ipoFit};
    r_squared ={r_squared_enso,r_squared_ipo};
    
    if strcmp(avgSelect,'annual')
        ax=doubleIndexPlotter(tYrs, dJ, indices, fits, r_squared, ylbls, legendStr, savename );
    else
        [ax1,ax2]=doubleIndexPlotter(t, dJ, indices, fits, r_squared, ylbls, legendStr, savename );
        % Nice ENSO axis
        set(ax1(1),'ylim',[-1 1],'ytick',[-1:.5:1])
        set(ax1(2),'ylim',[-2.25 2.75],'ytick',[-2.25:1.25:2.75])
        % Nice IPO axis
        set(ax2(1),'ylim',[-1 1],'ytick',[-1:.5:1])
        set(ax2(2),'ylim',[-2 2.1],'ytick',[-2:1:2])
    end

end

%% --- ENSO Plot
if ~isempty(strfind(indSelect,'enso')) || ~isempty(strfind(indSelect,'all'))
    matfile = '../enso-samba/mat/reynolds/sst_enso_34.mat';
    if exist(matfile,'file')
        load(matfile,'enso_obs');
    else
        error('Need to prep enso data: %s ...\n',matfile);
    end
  
  dJ_enso=regionRec.pac.Ntrop{1}(tMem,:) + regionRec.pac.Strop{1}(tMem,:);
  %dJ_enso=dJ_enso+regionRec.pac.NStrop{1}(tMem,:) + regionRec.pac.SStrop{1}(tMem,:);

    if strcmp(avgSelect,'annual')
        dJ_enso=calcAnnualMean(dJ_enso,20);
        enso_obs=calcAnnualMean(enso_obs,20);
    else
        clim = calcClimatology(dJ_enso,Nyrs);
        dJ_enso=dJ_enso - repmat(clim,[1 Nyrs]);
    end

    pp=polyfit(enso_obs,dJ_enso,1);
    ensoFit=polyval(pp,enso_obs);
    r_squared=corrcoef(enso_obs,dJ_enso);
  
    ylbl = sprintf('ENSO 3.4');
    legendStr = {'$\delta J_{\tau_x, Trop Pac}$','ENSO 3.4 Index'};
    savename=sprintf('%senso_tMem_%dmo_%s',plotDir,tMem,avgSelect);
    
    if strcmp(avgSelect,'annual')

        ax=indexPlotter(tYrs, dJ_enso, enso_obs, ensoFit, r_squared, ylbl, legendStr, savename );
    else
        ax=indexPlotter(t, dJ_enso, enso_obs, ensoFit, r_squared, ylbl, legendStr, savename );
        set(ax(1),'ylim',[-1 1],'ytick',[-1:.5:1])
        set(ax(2),'ylim',[-2.25 2.75],'ytick',[-2.25:1.25:2.75])
    end

    savePlot(savename,'pdf')



  keyboard
end

% -----------------------------------------------------------------------------

%% --- IPO 
if ~isempty(strfind(indSelect,'ipo')) || ~isempty(strfind(indSelect,'all'))
  matfile = [matDir 'ipo_unfiltered.mat'];
  if exist(matfile,'file')
    load(matfile,'ipoInd','ind92');
  else
    fprintf('Need to prep ipo data\n');
    return;
  end
  
  %dJ_ipo=regionRec.pac.Ntrop{1}(tMem,:);
  %dJ_ipo=dJ_ipo+regionRec.pac.Strop{1}(tMem,:);
  %dJ_ipo=dJ_ipo+regionRec.pac.NStrop{1}(tMem,:);
  %dJ_ipo=dJ_ipo+regionRec.pac.SStrop{1}(tMem,:);
  %%%dJ_ipo=dJ_ipo+regionRec.pac.south{1}(tMem,:);

  % Trying for remote connections a la Lopez 2016
  dJ_ipo = regionRec.atl.ek{1}(tMem,:);
  %dJ_ipo = dJ_ipo + regionRec.atl.SStrop{1}(tMem,:);
  
  %ind92=1657; this is loaded with the file
  tauLag=0;
  ipoInd=ipoInd(ind92-tauLag:ind92-tauLag+Nt-1);

  filterWindow=0;

  if strcmp(avgSelect,'annual')
    dJ_ipo=calcAnnualMean(dJ_ipo,20);
    ipoInd=calcAnnualMean(ipoInd,20);
  else
    clim=calcClimatology(dJ_ipo,Nyrs);
    dJ_ipo = dJ_ipo - repmat(clim,[1 Nyrs]);
  end  

  % do filtering
  if filterWindow>0
    w = gausswin(filterWindow);
    w = w/sum(w);
    dJ_ipo = filter(w,1,dJ_ipo);
    ipoInd = filter(w,1,ipoInd);
  end

  pp=polyfit(ipoInd,dJ_ipo,1);
  ipoFit=polyval(pp,ipoInd);
  r_squared=corrcoef(ipoInd,dJ_ipo);
  
  ylbl = 'IPO Index';
  legendStr = {'$\delta J_{\tau_x, Pac \pm 40^{\circ}}$','IPO Index'};
  savename=sprintf('%sipo_tMem_%dmo',plotDir,tMem);
  
  if strcmp(avgSelect,'annual')
    indexPlotter(tYrs, dJ_ipo, ipoInd, ipoFit, r_squared, ylbl, legendStr, savename );
  else
    ax=indexPlotter(t, dJ_ipo, ipoInd, ipoFit, r_squared, ylbl, legendStr, savename );
    %set(ax(1),'ylim',[-1 1],'ytick',[-1:.5:1]);
    set(ax(2),'ylim',[-2 2],'ytick',[-2:1:2]);
  end
keyboard
end

% -----------------------------------------------------------------------------

%% --- AMO 
if ~isempty(strfind(indSelect,'amo')) || ~isempty(strfind(indSelect,'all'))
  matfile = [matDir 'amo_smoothed.mat'];
  if exist(matfile,'file')
    load(matfile,'amoInd');
  else
    fprintf('Need to prep amo data\n');
    return;
  end
  
  dJ_amo=regionRec.atl.subPolar{7}(tMem,:);
  dJ_amo=-dJ_amo; 
  
  ind92=44*12+1;
  tauLag=0;
  amoInd=amoInd(ind92-tauLag:ind92-tauLag+Nt-1);

  dJ_amo=calcAnnualMean(dJ_amo,20);
  amoInd=calcAnnualMean(amoInd,20);
  
  pp=polyfit(amoInd,dJ_amo,1);
  amoFit=polyval(pp,amoInd);
  r_squared=corrcoef(amoInd,dJ_amo);
  
  ylbl = 'AMO Index';
  legendStr = {'-$\delta J_{Precip, Atl Subpolar}$','AMO Index'};
  savename=sprintf('%samo_tMem_%dmo',plotDir,tMem);
  
  indexPlotter(tYrs, dJ_amo, amoInd, amoFit, r_squared, ylbl, legendStr, savename );
end

% -----------------------------------------------------------------------------

%% --- SAM 
if ~isempty(strfind(indSelect,'sam')) || ~isempty(strfind(indSelect,'all'))
  matfile = [matDir 'sam_index.mat'];
  if exist(matfile,'file')
    load(matfile,'samInd');
  else
    fprintf('Need to prep sam data\n');
    return;
  end
  
  dJ_sam=0*regionRec.atl.south{1}(tMem,:);
  %atmInd = [1,2,
  for i=1:2
    %dJ_sam=dJ_sam+regionRec.atl.south{i}(tMem,:);
    dJ_sam=dJ_sam+regionRec.pac.south{i}(tMem,:);
    dJ_sam=dJ_sam+regionRec.ind.south{i}(tMem,:);
    dJ_sam=dJ_sam+regionRec.acc{i}(tMem,:);
  end
  %dJ_sam=dJ_sam+regionRec.pac.SStrop{1}(tMem,:)+regionRec.atl.SStrop{1}(tMem,:)+regionRec.ind.SStrop{1}(tMem,:);
  %dJ_sam=dJ_sam+regionRec.atl.ek{1}(tMem,:);

  % --- Filter parameters
  filterWindow=[0,9];
  tauLag=0;

  ind92=35*12+1;
  Nt=240;
  samInd=samInd(ind92-tauLag:ind92-tauLag+Nt-1);
  samInd={samInd,samInd};
  dJ_sam={dJ_sam,dJ_sam};

  pp=cell(length(filterWindow));
  samFit=cell(length(filterWindow));
  r_squared=cell(length(filterWindow));

  for n=1:length(filterWindow)
    % Compute anomaly
    if strcmp(avgSelect,'annual')
        dJ_sam{n}=calcAnnualMean(dJ_sam{n},20);
        samInd{n}=calcAnnualMean(samInd{n},20);
    else
        clim = calcClimatology(dJ_sam{n},Nyrs);
        dJ_sam{n}=dJ_sam{n} - repmat(clim,[1 Nyrs]);
    end
    % do filtering
    if filterWindow(n)>0
      w = gausswin(filterWindow(n));
      w = w/sum(w);
      dJ_sam{n} = filter(w,1,dJ_sam{n});
      samInd{n} = filter(w,1,samInd{n});
    end

      pp{n}=polyfit(samInd{n},dJ_sam{n},1);
      samFit{n}=polyval(pp{n},samInd{n});
      r_squared{n}=corrcoef(samInd{n},dJ_sam{n});
  end
  

  

  ylbl = {'SAM Index','SAM Index'};

  
  if length(filterWindow)==1
    savename=sprintf('%ssam_%dmo_w%d',plotDir,tMem,filterWindow(1));
    legendStr = {sprintf('$\delta J_{x}$, w=%d',filterWindow(1)),sprintf('SAM Index, w=%d',filterWindow(1))};
    if strcmp(avgSelect,'annual')
      indexPlotter(tYrs, dJ_sam{1}, samInd{1}, samFit{1}, r_squared{1}, ylbl{1}, legendStr, savename );
    else
      indexPlotter(t, dJ_sam{1}, samInd{1}, samFit{1}, r_squared{1}, ylbl{1}, legendStr, savename );
    end
  elseif length(filterWindow)==2

    savename=sprintf('%ssam_w%d_w%d',plotDir,tMem,filterWindow(1),filterWindow(2));
    legendStr = {sprintf('%s, w=%d','$\delta J_{x}$',filterWindow(1)),sprintf('SAM Index, w=%d',filterWindow(1)),...
                 sprintf('%s, w=%d','$\delta J_{x}$',filterWindow(2)),sprintf('SAM Index, w=%d',filterWindow(2))};
    if strcmp(avgSelect,'annual')
      doubleIndexPlotter(tYrs, dJ_sam, samInd, samFit, r_squared, ylbl, legendStr, savename );
    else
      doubleIndexPlotter(t, dJ_sam, samInd, samFit, r_squared, ylbl, legendStr, savename );
    end
  end
end

% -----------------------------------------------------------------------------

%% --- MJO 
if ~isempty(strfind(indSelect,'mjo')) || ~isempty(strfind(indSelect,'all'))
  matfile = [matDir 'mjo_monthly.mat'];
  if exist(matfile,'file')
    load(matfile,'mjo_pc1','mjo_pc2','mjo_pc12','ind92');
  else
    fprintf('Need to prep sam data\n');
    return;
  end
  
  dJ_mjo=0*regionRec.atl.south{1}(tMem,:);
  %atmInd = [1,2,
  for i=1:2
    dJ_mjo=dJ_mjo+regionRec.pac.Ntrop{i}(tMem,:);
    dJ_mjo=dJ_mjo+regionRec.pac.Strop{i}(tMem,:);
    %dJ_mjo=dJ_mjo+regionRec.ind.south{i}(tMem,:);
    %dJ_mjo=dJ_mjo+regionRec.acc{i}(tMem,:);
  end
  %dJ_mjo=dJ_mjo+regionRec.pac.SStrop{1}(tMem,:)+regionRec.atl.SStrop{1}(tMem,:)+regionRec.ind.SStrop{1}(tMem,:);
  %dJ_mjo=dJ_mjo+regionRec.atl.ek{1}(tMem,:);

  % --- Filter parameters
  filterWindow=[0,0];
  tauLag=0;

  Nt=240;

  % Compute anomaly?
  Nyrs_mjo=length(mjo_pc1)/12;
  mjo_pc1 = mjo_pc1 - repmat(calcClimatology(mjo_pc1,Nyrs_mjo),[1 Nyrs_mjo]);
  mjo_pc2 = mjo_pc2 - repmat(calcClimatology(mjo_pc2,Nyrs_mjo),[1 Nyrs_mjo]);
  mjo_pc12 = mjo_pc12 - repmat(calcClimatology(mjo_pc12,Nyrs_mjo),[1 Nyrs_mjo]);

  mjo_pc1=mjo_pc1(ind92-tauLag:ind92-tauLag+Nt-1);
  mjo_pc2=mjo_pc2(ind92-tauLag:ind92-tauLag+Nt-1);
  mjo_pc12=mjo_pc12(ind92-tauLag:ind92-tauLag+Nt-1);
  mjoInd={mjo_pc1,mjo_pc2};
  dJ_mjo={dJ_mjo,dJ_mjo};

  pp=cell(length(filterWindow));
  mjoFit=cell(length(filterWindow));
  r_squared=cell(length(filterWindow));

  for n=1:length(filterWindow)
    % Compute anomaly
    if strcmp(avgSelect,'annual')
        dJ_mjo{n}=calcAnnualMean(dJ_mjo{n},20);
        mjoInd{n}=calcAnnualMean(mjoInd{n},20);
    else
        clim = calcClimatology(dJ_mjo{n},Nyrs);
        dJ_mjo{n}=dJ_mjo{n} - repmat(clim,[1 Nyrs]);
    end
    % do filtering
    if filterWindow(n)>0
      w = gausswin(filterWindow(n));
      w = w/sum(w);
      dJ_mjo{n} = filter(w,1,dJ_mjo{n});
      mjoInd{n} = filter(w,1,mjoInd{n});
    end

      pp{n}=polyfit(mjoInd{n},dJ_mjo{n},1);
      mjoFit{n}=polyval(pp{n},mjoInd{n});
      r_squared{n}=corrcoef(mjoInd{n},dJ_mjo{n});
  end
  

  

  ylbl = {'MJO Index','MJO Index'};

  
  if length(filterWindow)==1
    savename=sprintf('%smjo_w%d',plotDir,filterWindow(1));
    legendStr = {sprintf('$\delta J_{x}$, w=%d',filterWindow(1)),sprintf('MJO Index, w=%d',filterWindow(1))};
    if strcmp(avgSelect,'annual')
      indexPlotter(tYrs, dJ_mjo{1}, mjoInd{1}, mjoFit{1}, r_squared{1}, ylbl{1}, legendStr, savename );
    else
      indexPlotter(t, dJ_mjo{1}, mjoInd{1}, mjoFit{1}, r_squared{1}, ylbl{1}, legendStr, savename );
    end
  elseif length(filterWindow)==2

    savename=sprintf('%smjo_w%d_w%d',plotDir,filterWindow(1),filterWindow(2));
    legendStr = {sprintf('%s, w=%d','$\delta J_{x}$',filterWindow(1)),sprintf('MJO Index, w=%d',filterWindow(1)),...
                 sprintf('%s, w=%d','$\delta J_{x}$',filterWindow(2)),sprintf('MJO Index, w=%d',filterWindow(2))};
    if strcmp(avgSelect,'annual')
      doubleIndexPlotter(tYrs, dJ_mjo, mjoInd, mjoFit, r_squared, ylbl, legendStr, savename );
    else
      doubleIndexPlotter(t, dJ_mjo, mjoInd, mjoFit, r_squared, ylbl, legendStr, savename );
    end
  end
end
% -----------------------------------------------------------------------------

end

% -----------------------------------------------------------------------------

function [ax,h1,h2] = indexPlotter( t, dJ, ind, indTrend, r_squared, ylbl, legendStr, savename )

cmap=cbrewer2('Dark2',2);
figureW;
subplot(3,2,1:4),
[ax,h1,h2]=plotyy(t,dJ,t,ind);

  set(gca,'xtick',[1992:2:2012])
  xlim([1992 2012])

set(ax(1),'YColor',[0 0 0]);
set(get(ax(1),'ylabel'),'string','$\delta J$ [Sv]','color',cmap(1,:),'fontsize',22)%,'rotation',0)
 % set(ax(1),'ytick',[-1:.2:1]);


 % set(ax(2),'ytick',[-5:1:5]);
  set(ax(2),'YColor',[0 0 0]);
set(get(ax(2),'ylabel'),'string',ylbl,'color',cmap(2,:),'fontsize',22)%,'rotation',0)

hl=legend(legendStr,'Location','Best');
hl.FontSize=22;

set(h1,{'color'},num2cell(cmap(1,:),2));
set(h2,{'color'},num2cell(cmap(2,:),2));


set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])

grid on
subplot(3,2,5),
plot(ind,dJ,'.',ind,indTrend,'k-')

xlabel(legendStr{2})
ylabel(legendStr{1})%,'rotation',0)
legend('Data',sprintf('Linear trend, $r^2 = %1.2f$',r_squared(1,2)),'Location','Best');

subplot(3,2,6)
% --- Make a globe plot with tropical pacific
%figName=sprintf('%senso_globe',plotDir);
tropPacInd = 4; % see grabSamocGroups.m
separator='southek';
tauuInd = 1;
msks=createRegionMasks('ekman');
[~,ensoSection]=grabSamocGroups(separator,tauuInd,msks);
fullLegend=grabSamocGroupLegend(separator);
cmap=grabSamocGroupColors(length(fullLegend));
plotSelectSamocMasks(tropPacInd,separator,[],cmap(tropPacInd,:));


%saveas(gcf,savename,'pdf');

end

function [ax1,ax2,h11,h12,h21,h22] = doubleIndexPlotter( t, dJ, ind, indTrend, r_squared, ylbl, legendStr, savename )

if iscell(dJ)
    if length(dJ)==2
        dJ1 = dJ{1};
        dJ2 = dJ{2};
    else
        error('Unsure of dJ size in doubleIndexPlotter');
    end
else
    dJ1=dJ;
    dJ2=dJ;
end

if length(legendStr)==3
    legendStr={legendStr{1},legenStr{2},legendStr{1},legendStr{3}};
end

cmap=cbrewer2('Dark2',3);
figureH;
subplot(3,2,1:2),
[ax1,h11,h12]=plotyy(t,dJ1,t,ind{1});

  set(gca,'xtick',[1992:2:2012])
  xlim([1992 2012])

set(get(ax1(1),'ylabel'),'string','Sv')
 % set(ax(1),'ytick',[-1:.2:1]);
  set(ax1(1),'YColor',[0 0 0]);

set(get(ax1(2),'ylabel'),'string',ylbl{1})
 % set(ax(2),'ytick',[-5:1:5]);
  set(ax1(2),'YColor',[0 0 0]);

legend({legendStr{1},legendStr{2}},'Location','Best')
grid on
set(h11,{'color'},num2cell(cmap(1,:),2));
set(h12,{'color'},num2cell(cmap(2,:),2));


subplot(3,2,3:4),
[ax2,h21,h22]=plotyy(t,dJ2,t,ind{2});

  set(gca,'xtick',[1992:2:2012])
  xlim([1992 2012])

set(get(ax2(1),'ylabel'),'string','Sv')
 % set(ax(1),'ytick',[-1:.2:1]);
  set(ax2(1),'YColor',[0 0 0]);

set(get(ax2(2),'ylabel'),'string',ylbl{2})
 % set(ax(2),'ytick',[-5:1:5]);
  set(ax2(2),'YColor',[0 0 0]);

legend({legendStr{3},legendStr{4}},'Location','Best')

grid on
set(h21,{'color'},num2cell(cmap(3,:),2));
set(h22,{'color'},num2cell(cmap(2,:),2));

subplot(3,2,5),
ph=plot(ind{1},dJ1,'.');
set(ph,{'color'},num2cell(cmap(1,:),2));
hold on;
plot(ind{2},indTrend{2},'k');
hold off;

xlabel(legendStr{2})
ylabel(legendStr{1})
legend('Data',sprintf('Linear trend, $r^2 = %1.2f$',r_squared{1}(1,2)),'Location','Best');

subplot(3,2,6),
ph=plot(ind{2},dJ2,'.');
set(ph,{'color'},num2cell(cmap(3,:),2));
hold on;
plot(ind{2},indTrend{2},'k');
hold off;

xlabel(legendStr{4})
ylabel(legendStr{3})
legend('Data',sprintf('Linear trend, $r^2 = %1.2f$',r_squared{2}(1,2)),'Location','Best');

%saveas(gcf,savename,'pdf');

end
