function [ctrlError_mean, ctrlError_var] = loadCtrlError( fldName ) 
%
% Load "posterior" (?) error on ctrl vector. 
% This is inverse of weights in: 
%
%       /workspace/error_weight/ctrl_weight/* 
%
% Scale by cell area. I.e.: 
% 
%       error = 1 / (weight per grid cell)
%
% Inputs:
%
%       fldName: 'tauu','tauv','aqh','atemp','lwdown','swdown','precip'
%                'kapgm','diffkr','kapredi'
%                can be a cell structure with a bunch
% 
% Outputs:
%
%       Note: For now, only one field returned. So if cell structure
%             input, only returns last field in structure. 
%
%       ctrlError_mean: mean bias in error
%       ctrlError_var: variance
%
%----------------------------------------------------------------------

global mygrid;

if iscell(fldName)
  Nfld=length(fldName);
else
  Nfld=1;
  fldName={fldName};
end

dA = mygrid.DXG(:,:,1).*mygrid.DYG(:,:,1);

for i=1:Nfld

  meanFile=sprintf('../../error_weight/ctrl_weight/r2.w%s_mean.data',fldName{i});
  varFile=sprintf('../../error_weight/ctrl_weight/r2.w%s_var_tot.data',fldName{i});

  saveDir='mat/eccov4r2/ctrls/error';
  saveFile = sprintf('%s/%s.mat',saveDir,fldName{i});
  if ~exist(saveDir,'dir'), mkdir(saveDir); end;
  

  if strcmp(fldName{i},'precip')
    meanFile=strrep(meanFile,'mean','mean_20150807');
    varFile=strrep(varFile,'var_tot','var_nonseason_20150807');
  end

  ctrlError_mean = read_bin(meanFile); 
  ctrlError_var = read_bin(varFile); 

  % Invert for error and scale by area
  ctrlError_mean = 1 ./ (ctrlError_mean./dA);
  ctrlError_var = 1 ./ (ctrlError_var./dA);

  % Save to matfile
  save(saveFile,'ctrlError_mean','ctrlError_var');

end
end
