function [] = grabAdjFiles( mygrid )
% Re loading mat files from adxx_<>
% Re plotting the december adj files
% ----------------------------------

moStr = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
adjField = {'tauu','tauv','hflux','sflux','aqh','atemp','swdown','lwdown','precip','runoff'};

for i=1:length(moStr)
	runStr = sprintf('%s.240mo/',moStr{i});
	resultsDir = sprintf('../../results/samoc/%s',runStr);

	if strcmp(moStr{i},'dec')
		plotadx(runStr,resultsDir,mygrid);
	else
		postProcess_adxx(runStr,resultsDir,adjField,mygrid,10^-6);
	end

	fprintf('Done with %s ...\n',moStr{i})
end
end
