function [] = plotHfluxPerturb(mygrid)

expName = '';
expNameSave = strrep(expName,'.','_');
%resultsDir = '/scratch/tsmith/results/hflux/';
resultsDir = '/workspace/results/hflux/';
resultsP10 = [resultsDir expName 'p1.20yr/diags/'];
if ~exist(resultsP10,'dir'), pcheck=0; else pcheck=1; end;
resultsM10 = [resultsDir expName 'm1.20yr/diags/'];
resultsRef = [resultsDir 'ref/diags/'];
matDir = 'mat/hf-perturb/';
plotDir = 'figures/hf-perturb/';
if ~exist(matDir,'dir'), mkdir(matDir); end
if ~exist(plotDir,'dir'), mkdir(plotDir); end
fName = 'external_forcing';
trspName = 'trsp';

%% External forcing
% 'oceTAUX ' 'oceTAUY ' 'oceQnet ' 'oceFWflx' 'EXFtaux ' 'EXFtauy ' 'EXFqnet ' 'EXFempmr' 
% 'EXFatemp' 'EXFaqh  ' 'EXFlwdn ' 'EXFswdn ' 'EXFevap ' 'EXFpreci' 'EXFroff '

%% trsp: 
%  UVELMASS, VVELMASS, WVELMASS
 
%% ----------------------------------------------------------------------------------------
%% 1. Check perturbation
%% Load hflux from exp and ecco 
if pcheck, p1iters = grabAllIters( resultsP10,fName); end;
m1iters = grabAllIters( resultsM10,fName); 
hflux_diff_m1 = zeros(240,1);
hflux_diff_p1 = zeros(240,1);

for n = 19:-1:0
  if pcheck, hflux_p1 = rdmds2gcmfaces([resultsP10 fName],p1iters(n*12+1:n*12+12),'rec',7);end
  hflux_m1 = rdmds2gcmfaces([resultsM10 fName],m1iters(n*12+1:n*12+12),'rec',7);
  hflux_ref = rdmds2gcmfaces([resultsRef fName],m1iters(n*12+1:n*12+12),'rec',7);

  hflux_diff_m1(n*12+1:n*12+12) = squeeze(nansum(nansum(convert2gcmfaces(hflux_m1-hflux_ref),1),2));
  if pcheck, hflux_diff_p1(n*12+1:n*12+12) = squeeze(nansum(nansum(convert2gcmfaces(hflux_p1-hflux_ref),1),2)); end;
end

%% ----------------------------------------------------------------------------------------
%% 2.1 Compute perturbed AMOC
%% Load and compute AMOC from uvel/vvel
matStr = sprintf('%samoc_%s',matDir,expNameSave);
if pcheck, [amoc_p1] = loadPerturbedAmoc([matStr 'p1_20yr.mat'],[resultsP10 trspName],mygrid); end;
[amoc_m1] = loadPerturbedAmoc([matStr 'm1_20yr.mat'],[resultsM10 trspName],mygrid);
[amoc_ref] = loadPerturbedAmoc(['mat/hf-perturb/amoc_ref.mat'],[resultsRef trspName],mygrid);
Nt = size(amoc_m1,2);
if pcheck, dJ_p1 = amoc_p1 - repmat(mean(amoc_p1,2),[1 Nt]);end;
dJ_m1 = amoc_m1 - repmat(mean(amoc_m1,2),[1 Nt]);
dJ_ref = amoc_ref - repmat(mean(amoc_ref,2),[1 Nt]);

%%% 2.2 Load ECCO values
%lat=-34;
%[samoc] = calcAMOC(lat,'eulerian',mygrid);
%lat=26;
%[rapid] = calcAMOC(lat,'eulerian',mygrid);
%
%dJ_samoc = samoc - mean(samoc);
%dJ_rapid = rapid - mean(rapid); 

%% 2.3 Compute difference perturb-ecco
iyS = find(mygrid.LATS==-34);
iyN = find(mygrid.LATS==26);
if pcheck, diff_p1 = dJ_p1(iyS,:) - dJ_ref(iyS,:);end;
diff_m1 = dJ_m1(iyS,:) - dJ_ref(iyS,:);
if pcheck, diff_rapid_p1 = dJ_p1(iyN,:) - dJ_ref(iyN,:);end;
diff_rapid_m1 = dJ_m1(iyN,:) - dJ_ref(iyN,:);

%% ----------------------------------------------------------------------------------------
%% 3.1 Compute reconstructed AMOC 
matStr = sprintf('%samoc_rec_%s',matDir,expNameSave);
if pcheck, [dJ_rec_p1] = loadReconstructedAmoc([matStr 'p1_20yr.mat'],[resultsP10 fName],mygrid);end;
[dJ_rec_m1] = loadReconstructedAmoc([matStr 'm1_20yr.mat'],[resultsM10 fName],mygrid);
[dJ_rec_ref] = loadReconstructedAmoc(['mat/hf-perturb/amoc_rec_ref.mat'],[resultsRef fName],mygrid);

%% 3.2 Pick a guess at "optimal" soln.. still working on this
ind = 228;

%  %% 3.3 Load reconstructed amoc without variation
%  load('mat/reconstruct.34S/monthly_flux/fullReconstruction.mat','samoc_adjFields');
%  samoc_rec=samoc_adjFields{3};

if pcheck, diff_rec_p1 = dJ_rec_p1(ind,:) - dJ_rec_ref(ind,:);end;
diff_rec_m1 = dJ_rec_m1(ind,:) - dJ_rec_ref(ind,:);

%% 3.4 Remove seasonality from reconstruction error ...?
freq=(0:Nt-1)'/240 * 12; % Freq in 1/yr
if pcheck, f_rec_err_p1 = seasonFilter(dJ_rec_p1-dJ_rec_ref,freq);end;
f_rec_err_m1 = seasonFilter(dJ_rec_m1-dJ_rec_ref,freq);


%% ----------------------------------------------------------------------------------------
%% 4. Plot Convective adjustment parameter & mixed layer
tind=120;
%convadj_m1 = rdmds2gcmfaces([resultsM10 'mxl_snap'],NaN,'rec',2);
%convadj_p1 = rdmds2gcmfaces([resultsP10 'mxl_snap'],NaN,'rec',2);
%convadj_ref = rdmds2gcmfaces([resultsRef 'mxl_snap'],NaN,'rec',2);

mxl_m1 = rdmds2gcmfaces([resultsM10 'mxl'],NaN,'rec',1);
mxl_p1 = rdmds2gcmfaces([resultsP10 'mxl'],NaN,'rec',1);
mxl_ref = rdmds2gcmfaces([resultsRef 'mxl'],NaN,'rec',1);

%% ----------------------------------------------------------------------------------------
%% 5. Plot Heat Flux perturbation
plotName=sprintf('%shflux_%s_m1',plotDir,expNameSave);
if ~exist([plotName '.pdf'],'file')
figureW;
for nn=1:1
m_map_atl(hflux_m1(:,:,nn)-hflux_ref(:,:,nn),5);
xlabel(sprintf('qnet_{hot}-qnet_{ECCO}\nJanuary 1992'));
pause(1)
end
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,plotName,'pdf')

if pcheck,
figureW;
for nn=1:1
m_map_atl(hflux_p1(:,:,nn)-hflux_ref(:,:,nn),5);
xlabel(sprintf('qnet_{cool}-qnet_{ECCO}\nJanuary 1992'));
pause(1)
end
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
plotName=sprintf('%shflux_%s_p1',plotDir,expNameSave);
saveas(gcf,plotName,'pdf')
end
end

plotName=sprintf('%shflux_diff_%s',plotDir,expNameSave);
if ~exist([plotName '.pdf'],'file')
figureW;
plot(1:240,hflux_diff_m1,'r') 
if pcheck, hold on; plot(1:240,hflux_diff_p1,'b'); hold off; end; 
xlabel('Months')
ylabel(sprintf('%s_t (Q_p - Q_{ECCO})_t','\Sigma'))
legend('Q_{hot}','Q_{cool}')

set(gca,'xtick',[12:12:240])
xlim([0 240])
grid on

set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,plotName,'pdf')
end

%% ----------------------------------------------------------------------------------------
%% 6. Plot perturbed AMOC at select latitudes
figureW;
t=[1:Nt]./12;
plot(t,diff_m1,'r-',t,diff_rec_m1,'r--')
hold on; 
if pcheck, plot(t,diff_p1,'b-',t,diff_rec_p1,'b--'),end;
hold off
xlabel('Years')
ylabel('J_{perturb} - J_{ref} (Sv)')
legend('Ocean Heat = 1 W/m^2','Heat Reconstruction',...
       'Ocean Cool = 1 W/m^2','Cool Reconstruction',...
	'Location','Best')
set(gca,'xtick',[1:20])
xlim([0 20])
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
plotName=sprintf('%samocDiff_%s',plotDir,expNameSave);
saveas(gcf,plotName,'pdf')


%% ----------------------------------------------------------------------------------------
%% 7. Plot mixed layer  
fprintf('** About to plot mixed layer video, dbquit to cancel ...\n')
keyboard
plotName=sprintf('%smxl_%s_m1',plotDir,expNameSave);
 strs=struct('xlbl','Mixed Layer Depth (Hot-Ref)',...
	'time','months',...
	'clbl','m',...
	'vidName',plotName)
  opts=struct('logFld',0,'caxLim',[-10,10],...
	'saveVideo',1,...
	'figType','standard',...
	'mmapOpt',7,...
	'tLims',[1 240])
  exfOpt=1;
  plotAdjVideo(mxl_m1-mxl_ref,strs,opts,mygrid,exfOpt);

strs.xlbl='Mixed Layer Depth (Cool-Ref)';
strs.vidName=sprintf('%smxl_%s_p1',plotDir,expNameSave);
plotAdjVideo(mxl_p1-mxl_ref,strs,opts,mygrid,exfOpt);

%figureW;
%m_map_atl(mxl_m1(:,:,nn)-mxl_ref(:,:,nn),5);
%xlabel(sprintf('convadj_{cool}-convadj_{ECCO}'));
%lim=max(max(abs(mxl_m1(:,:,nn)-mxl_ref(:,:,nn))));
%caxis([-30 30])
%colormap(redblue)
%pause(.1)
%end
%set(gcf,'paperorientation','landscape')
%set(gcf,'paperunits','normalized')
%set(gcf,'paperposition',[0 0 1 1])
%plotName=sprintf('%sconvadj_%s_m1',plotDir,expNameSave);
%saveas(gcf,plotName,'pdf')

keyboard

end

function [amocExp] = loadPerturbedAmoc(matFile,diagFile,mygrid)

%% Grab all the iteration numbers from the directory
searchStr = sprintf('dir -1 %s*.meta',diagFile);
[status,list] = system(searchStr); 
res=textscan( list, '%s', 'delimiter', '\n' );
fileList = res{1};
Nt = length(fileList);

if ~exist(matFile,'file')
    % Compute for p1
    amocExp=zeros(length(mygrid.LATS),Nt);
    for n = 1:Nt
	% Iteration is the 42-51st digit
	iter = str2num(fileList{n}(end-14:end-5));
        uvel=rdmds2gcmfaces(diagFile,iter,'rec',1);
        vvel=rdmds2gcmfaces(diagFile,iter,'rec',2);
        amocExp(:,n) =calcPerturbedAmoc(uvel,vvel,mygrid);
    end
    save(matFile,'amocExp')
    fprintf('File %s saved ... \n',matFile);
else
    load(matFile,'amocExp');
    fprintf('File %s loaded ... \n',matFile);
end
end

function [filtered_signal ] = seasonFilter(sig, freq)

N = length(freq);
f_sig = fft(sig,N)/N;

%% Only look at half since symmetric for real data
% freq = freq(1:N/2);
% f_sig(2:end) = 2*f_sig(2:end);

f_sig(freq== 1) = 0;
f_sig(freq== 2) = 0; 
f_sig(freq== 11) = 0;
f_sig(freq== 10) = 0; 


filtered_signal = ifft(f_sig,N);

end
