function [stackedReconstruction] = grabSamocGroupsAllMem(regionRec,varargin) 
% Script for stacking regionally restricted reconstructions into one matrix
%
% Inputs:
%
%       regionRec: structure containing regional reconstructions
%               no default
%
% Optional (but recommended) Inputs:
%       
%       separator: string indicating specific sections to grab
%               'ek' : separates local band around 34S from SSub Trop Atl
%               'south' : separates southern ocean into pacific, atl, ind, and south of 60S
%               'southek'(default): does both
%               'none': does neither
%       flux_ind: index of fluxes to grab 1=tau_x 2=tau_y 3=hflux 4=sflux/fwf
%               default = 1
%
% Output:
%
%       stackedReconstruction: Matrix of reconstructions regions go by rows, reconstructed record in columns
%
% -----------------------------------------------------------------------------

separator='southek';
flux_ind=1;

for n=1:length(varargin)
  if isa(varargin{n},'char'), separator=varargin{n};
  elseif isa(varargin{n},'double'), flux_ind=varargin{n};
  end
end

Nt=size(regionRec.acc{1},2);
tau_mem_max = 229; % Max memory
stackedReconstruction=zeros(tau_mem_max,Nt,12);
if strfind(separator,'ek')
  stackedReconstruction=cat(3,stackedReconstruction,zeros(tau_mem_max,Nt,1));
end
if strfind(separator,'south')
  stackedReconstruction=cat(3,stackedReconstruction,zeros(tau_mem_max,Nt,3));
end

% --- Grab reconstruction by region
for i=flux_ind(1):flux_ind(end)
  tmp=cat(3,...
    regionRec.arc{i}(1:tau_mem_max,:)+regionRec.bar{i}(1:tau_mem_max,:),...
    regionRec.pac.subPolar{i}(1:tau_mem_max,:),...
    regionRec.pac.NStrop{i}(1:tau_mem_max,:), ...
    regionRec.pac.Ntrop{i}(1:tau_mem_max,:) + regionRec.pac.Strop{i}(1:tau_mem_max,:), ... 
    regionRec.pac.SStrop{i}(1:tau_mem_max,:), ...
    regionRec.atl.subPolar{i}(1:tau_mem_max,:),...
    regionRec.atl.NStrop{i}(1:tau_mem_max,:), ... 
    regionRec.atl.Ntrop{i}(1:tau_mem_max,:) + regionRec.atl.Strop{i}(1:tau_mem_max,:));

  if strfind(separator,'ek')
    tmp=cat(3,tmp,...
    regionRec.atl.SStrop{i}(1:tau_mem_max,:), ...
    regionRec.atl.ek{i}(1:tau_mem_max,:));
  else
    tmp=cat(3,tmp,...
      regionRec.atl.SStrop{i}(1:tau_mem_max,:)+ ...
      regionRec.atl.ek{i}(1:tau_mem_max,:));
  end

  tmp=cat(3,tmp,...
    regionRec.ind.NStrop{i}(1:tau_mem_max,:)+ ...
    regionRec.ind.Ntrop{i}(1:tau_mem_max,:) + ...
    regionRec.ind.Strop{i}(1:tau_mem_max,:), ... 
    regionRec.ind.SStrop{i}(1:tau_mem_max,:));

  if strfind(separator,'south')
    tmp=cat(3,tmp,...
      regionRec.pac.south{i}(1:tau_mem_max,:), ...
      regionRec.atl.south{i}(1:tau_mem_max,:),...  
      regionRec.ind.south{i}(1:tau_mem_max,:), ...
      regionRec.acc{i}(1:tau_mem_max,:)); 
  else
    tmp=cat(3,tmp,...
      regionRec.pac.south{i}(1:tau_mem_max,:)+ ... 
      regionRec.atl.south{i}(1:tau_mem_max,:)+  ...
      regionRec.ind.south{i}(1:tau_mem_max,:)+ ...
      regionRec.acc{i}(1:tau_mem_max,:)); 
  end

  stackedReconstruction=stackedReconstruction+tmp;

end

% --- Swap groups and tau_mem indices
stackedReconstruction = permute(stackedReconstruction,[3 2 1]);
end
