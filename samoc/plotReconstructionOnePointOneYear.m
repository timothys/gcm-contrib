function [] = plotReconstructionOnePointOneYear()
% This is to visualize reconstruction. 
% For one point in the spatial domain:
%   dJdx = sensitivity at that point for 1 year lead time
%   dx   = forcing anomaly at that point for one year, say 1992
%   dJ   = sum(dJdx .* dx) SAMOC response due to 
%           -> the forcing variable corresponding to dx
%           -> forcing at that spatial location
%           -> during that time period
%
% Here look at zonal wind stress because why not.
% ---------------------------------------------------------------


modelVar = 'eccov4r2';
atmVar = 'short-atm';

% --- Setup dirs
adxx_file = 'mat/dec.240mo/adj_tauu';
exf_file = sprintf('mat/exf/%s/%s/exf_tauu.mat',modelVar,atmVar);
figDir = 'figures/prez/';
if ~exist(figDir,'dir'), mkdir(figDir); end;

load(adxx_file,'adxx');
load(exf_file,'exf_fld');
mygrid = establish_mygrid('llc90');


% --- Grab point and show point in the domain
% looking at face 1, which is most of atlantic
i=20; j=160;


% --- Grab sensitivity for 1 year of lead time
Nmo = size(adxx.f1,3);
tau_mem = 13; % memory in months
dJdx = squeeze(adxx.f1(i,j,Nmo-tau_mem+1:Nmo));

% --- Grab forcing for first 13 months, 1992-jan 1993
dx = squeeze(exf_fld.f1(i,j,1:tau_mem));
dx = dx - nanmean(dx);

% --- Reconstruct January 1993 samoc from this single point and this forcing
dJ = nansum(dJdx.*dx);

% --- Plot the sensitivity and dx
fs = 22;
tMo = [0:tau_mem-1]./12+1992;
cmap = cbrewer2('Dark2',2);
figureW;
[ax,h1,h2]=plotyy(tMo,dx,tMo,dJdx);
set(ax(1),'xtick',tMo,...
'xticklabel',{'Jan','Feb','March','April','May','June','July','Aug','Sept','Oct','Nov','Dec','Jan'})
xlabel('1992')
set(gca,'position',[0.1406    0.1100    0.7165    0.8150])
set(gca,'position',[0.140600000000000   0.110000000000000   0.693105357142857   0.815000000000000])


set(ax(1),'ycolor',[0 0 0]);
set(get(ax(1),'ylabel'),'string',sprintf('%s\n%s','$\tau_x$','[N/m$^2$]'),...
                        'rotation',0,'color',cmap(1,:),'fontsize',fs)


set(ax(2),'ycolor',[0 0 0]);
set(get(ax(2),'ylabel'),'string',sprintf('%s\n%s','$\frac{\partial J}{\partial \tau_x}$', '[Sv/(N/m$^2$)]'),...
                        'rotation',0,'color',cmap(2,:),'fontsize',fs)

set(h1,{'color'},num2cell(cmap(1,:),2));
set(h2,{'color'},num2cell(cmap(2,:),2));
hl=legend('$\delta \tau_x$(x,y) in 1992','$\frac{\partial J}{\partial \tau_x}$(x,y), $\tau_{mem}$ = 1 yr','Location','Best');
hl.FontSize=fs;
hl.Position=[0.522539828477587   0.839653797229131   0.293635521616255   0.135859072208404];
hl.Position=[ 0.258030899906158   0.845903797229131   0.293635521616255   0.135859072208404];
p1=get(get(ax(1),'ylabel'),'position');
p1(1)=1.991882314389307e+03;
p1(2)= 0.002314592198471;
p1(3)=-1;
set(get(ax(1),'ylabel'),'fontsize',fs);
set(get(ax(1),'ylabel'),'position',p1);
p2=get(get(ax(2),'ylabel'),'position');
p2(2)=1.3*p1(2);
p2(1)=1.000025*p2(1);
set(get(ax(2),'ylabel'),'fontsize',fs);
set(get(ax(2),'ylabel'),'position',p2);
grid on
keyboard

savePlot([figDir 'single_point_reconstruction_dJdx_dx'],'pdf',[1 1 1]);


% --- Plot a map showing that single point
fld0 = 0*mygrid.XC.f1;

figure; 
m_proj('mollweide','lat',[-40 40],'lon',[-80 30]);
xtic=[-60:30:30];
ytic=[-40:20:40];

%[xx,yy,z]=convert2pcol(mygrid.XC.f1,mygrid.YC.f1,fld0);
[x,y]=m_ll2xy(mygrid.XC.f1,mygrid.YC.f1);
pcolor(x,y,fld0);
shading interp;
m_coast('patch',[1 1 1]*.7,'edgecolor','none');
m_grid('xtick',xtic,'ytick',ytic,'fonts',fs);
hold on;
plot(x(i,j),y(i,j),'k*','markersize',25);
colormap(redblue)

savePlot([figDir 'single_point_reconstruction_map'],'png',[1 1 1]);


end
