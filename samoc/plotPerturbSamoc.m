function [] = plotPerturbSamoc()
% Show samoc as a result of perturbed wind field during near Indonesian thruflow
%
% Input: perturbStr : indicate which perturbation to look at
%           'pert.barotropic'  (default)
%
% --------------------------------------------------------------------
expStr = 'barotropic.mo';
refStr = 'ref';
perturbStr='pert';
freqStr = 'daily';
%for n=1:length(varargin)
%    if iscell(varargin{n})
%        perturbStr=varargin{n};
%    end
%end

fig_dir = sprintf('figures/%s',expStr);
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;

% Set up load and save dirs
ref_results_dir = sprintf('../../results/samoc/%s/%s/diags/',expStr,refStr);
ref_mat_dir = sprintf('mat/%s/',expStr);
if ~exist(ref_mat_dir),mkdir(ref_mat_dir); end;
ref_mat_dir = sprintf('mat/%s/%s/',expStr,refStr);
if ~exist(ref_mat_dir),mkdir(ref_mat_dir); end;
ref_mat_dir = sprintf('mat/%s/%s/%s/',expStr,refStr,freqStr);
if ~exist(ref_mat_dir),mkdir(ref_mat_dir); end;

pert_results_dir = sprintf('../../results/samoc/%s/%s/diags/',expStr,perturbStr);
pert_mat_dir = sprintf('mat/%s/',expStr);
if ~exist(pert_mat_dir),mkdir(pert_mat_dir); end;
pert_mat_dir = sprintf('mat/%s/%s/',expStr,perturbStr);
if ~exist(pert_mat_dir),mkdir(pert_mat_dir); end;
pert_mat_dir = sprintf('mat/%s/%s/%s/',expStr,perturbStr,freqStr);
if ~exist(pert_mat_dir),mkdir(pert_mat_dir); end;

diagsName=sprintf('trsp_%s',freqStr);

% Do the calcs    
[ref_amoc] = calcAMOC(-34,'eulerian',ref_mat_dir,ref_results_dir,diagsName);
[per_amoc] = calcAMOC(-34,'eulerian',pert_mat_dir,pert_results_dir,diagsName);

% Compute timing based on frequency string
Nmo = 5;            % Num months in simulation
Nbwks = 9;          % Num. of biweeks in simulation
Ndays = 121;        % Num. days in simulation
Nt=length(ref_amoc);% Num. records for particular file loaded

t_mo   = [1:Nmo];
t_bwks = [1:Nbwks]*2; % weeks
t_days = [1:Ndays]/7; % in weeks

% Load the perturbation
xx_tauu = rdmds2gcmfaces('tauu-perturbation/xx_tauu',12);
xx_tauu = -xx_tauu(:,:,2);
global mygrid
tau_ref = rdmds2gcmfaces(sprintf('%s/exf_%s',ref_results_dir,freqStr),NaN,'rec',1);
tau_pert = rdmds2gcmfaces(sprintf('%s/exf_%s',pert_results_dir,freqStr),NaN,'rec',1);
dtau = tau_pert - tau_ref;
dtau(isnan(dtau))=0;
dtau_avg = 0*ones(1,size(tau_ref.f1,3));

% Set variables based on frequency of diagnostics 
if strcmp(freqStr,'monthly')
    assert(Nt==Nmo,sprintf('Num. records in %s != %d months hard coded\n',pert_mat_dir,Nmo));
    t=t_mo;
    plotStyle = '.-';
    selectVect = 2;
    Nxy = nansum(dtau(:,:,2)~=0);
elseif strcmp(freqStr,'biweekly')
    assert(Nt==Nbwks,sprintf('Num. records in %s != %d biweeks hard coded\n',pert_mat_dir,Nbwks));
    t=t_bwks;
    plotStyle = '.-';
    selectVect = 3:4;
    Nxy = nansum(dtau(:,:,3)~=0);
elseif strcmp(freqStr,'daily')
    assert(Nt==Ndays,sprintf('Num. records in %s != %d days hard coded\n',pert_mat_dir,Ndays));
    t=t_days;
    t_bwks = [0:Nbwks]*2;
    plotStyle = '-';
    selectVect = 28:56;
    Nxy = nansum(dtau(:,:,30)~=0);
end

% Compute average perturbation
for n=1:length(dtau_avg)
    dtau_avg(n) = nansum(dtau(:,:,n))/Nxy;
end

% --- Plotting
% Set fontsize
fs = 22;

% Plot perturbed tauu field during 2nd month
cmap = flipdim(cbrewer2('RdBu',5),1);
%cmax = nanmax(abs(xx_tauu));
cmax = 0.125; % Manual, for 0.1 N/m^2 perturbation and clean cbar
cticks= [-0.1:0.05:0.1];
figureW;
m_map_atl(xx_tauu,8,{'myCmap',cmap},{'fontSize',fs});
    hc=niceColorbar;
    hc.FontSize=fs;
    ylabel(hc,'N m$^{-2}$','rotation',0,...
        'units','normalized','position',[3.85 .538 0]);
    caxis([-cmax cmax]);
    hc.Ticks = cticks;
    savePlot(sprintf('%s/dtau_month',fig_dir));

% Plot diff
figureW; 
cmap = cbrewer2('Dark2',2);
set(gca,'colororder',cmap,'nextplot','replacechildren')

plot(t,dtau_avg,plotStyle,t,per_amoc-ref_amoc,plotStyle,'markersize',30)
    hl=legend('$\delta\tau_x$ [N m$^{-2}$]','$\delta$SAMOC [Sv]');
    hl.FontSize = fs;
    set(gca,'fontsize',fs)
    if strcmp(freqStr,'monthly')
        xlabel('Months')
    else
        xlabel('Weeks');
        set(gca,'xtick',t_bwks,'xticklabel',num2cell(t_bwks));
    end
    grid on;
    savePlot(sprintf('%s/diff_samoc_taux_%s',fig_dir,freqStr),'pdf');

% Plot diff as days since perturbation applied
t_bwks_p = t_bwks(1:end-2);
if strcmp(freqStr,'monthly')
    tp = t-1;
    per_amoc_p = per_amoc;
    ref_amoc_p = ref_amoc;
elseif strcmp(freqStr,'biweekly')
    tp = t(1:end-2);
    per_amoc_p = per_amoc(3:end);
    ref_amoc_p = ref_amoc(3:end);
elseif strcmp(freqStr,'daily')
    if ~isempty(strfind(expStr,'mo'))
        t_bwks_p = t_bwks;
        tp = t;
        per_amoc_p = per_amoc;
        ref_amoc_p = ref_amoc;
    elseif ~isempty(strfind(expStr,'bwk'))
        tp = t(1:end-28);
        per_amoc_p = per_amoc(29:end);
        ref_amoc_p = ref_amoc(29:end);
    end
end
figureW;
%set(gca,'colororder',cmap,'nextplot','replacechildren')
plot(tp,per_amoc_p-ref_amoc_p,'k');%,plotStyle,'markersize',30);
    set(gca,'fontsize',fs);
    ylabel('SAMOC$_{pert}$ - SAMOC$_{ref}$ (Sv)')
    if strcmp(freqStr,'monthly')
        xlabel('Months since start of perturbation')
    else
        xlabel('Weeks since start of perturbation');
        set(gca,'xtick',t_bwks_p,'xticklabel',num2cell(t_bwks_p));
    end
    grid on;
    savePlot(sprintf('%s/diff_samoc_%s',fig_dir,freqStr),'pdf');


keyboard
end
