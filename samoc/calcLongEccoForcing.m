function [] = calcLongEccoForcing()


longRawForcingDir='mat/exf/eig/long-atm';
shortRawForcingDir = 'mat/exf/eig/short-atm';
shortEccoForcingDir= 'mat/exf/eccov4r2/short-atm';
longEccoForcingDir='mat/exf/eccov4r2/long-atm';
ctrlDir = 'mat/eccov4r2/ctrls/'; 
climDir = 'mat/eccov4r2/ctrls/clim/'; 

if ~exist(longEccoForcingDir,'dir'), mkdir(longEccoForcingDir); end;
if ~exist(ctrlDir,'dir'), mkdir(ctrlDir); end;
if ~exist(climDir,'dir'), mkdir(climDir); end;

adjField = set_adjField( 'atm' );

if ~exist([ctrlDir 'xx_runoff.mat'],'file') && ~exist([climDir 'xx_runoff.mat'])
  calcEccoCtrls_monthly(shortRawForcingDir,shortEccoForcingDir,ctrlDir,climDir,adjField)
end

calcCtrlClimatology(longRawForcingDir,shortEccoForcingDir,climDir,longEccoForcingDir,adjField)


end


function [] = calcCtrlClimatology(longRawForcingDir,shortEccoForcingDir,climDir,longEccoForcingDir,adjField)

Nadj=length(adjField);

for i=1:Nadj

    exf_fld_str = sprintf('exf_%s',adjField{i});
    load(sprintf('%s/exf_%s.mat',longRawForcingDir,adjField{i}),'exf_fld'); 
        rawForcing = exf_fld; 
    load(sprintf('%s/exf_%s.mat',shortEccoForcingDir,adjField{i}),'exf_fld'); 
        shortEccoForcing = exf_fld; 
    load(sprintf('%s/xx_%s.mat',climDir,adjField{i}),'xx_clim');;
    exf_fld=0*rawForcing;
    Nt=size(exf_fld.f1,3);
    nProj = 13*12;

    exf_fld(:,:,1:nProj) = rawForcing(:,:,1:nProj) + repmat(xx_clim,[1 1 13]);
    exf_fld(:,:,nProj+1:Nt) = shortEccoForcing;

    matfile = sprintf('%s/exf_%s.mat',longEccoForcingDir,adjField{i});
    save(matfile,'exf_fld');
    fprintf('Saved file: %s \n',matfile);

end
end

% -----------------------------------------------------------------------------

function [] = calcEccoCtrls_monthly(rawForcingDir,eccoForcingDir,ctrlDir,climDir,adjField)

Nadj=length(adjField);

for i=1:Nadj
  
  saveStr=sprintf('xx_%s.mat',adjField{i});
  load([rawForcingDir saveStr]); rawForcing=xx_fld; 
  load([eccoForcingDir saveStr]); eccoForcing=xx_fld;
  clear xx_fld;

  xx_fld=eccoForcing-rawForcing;

  save([ctrlDir saveStr],'xx_fld');
  fprintf('Saved file: %s \n',[ctrlDir saveStr]);

  % Calc climatology
  
  xx_clim=calcClimatology(xx_fld,20);
  save([climDir saveStr],'xx_clim');
  fprintf('Saved file: %s \n',[climDir saveStr]);

end


end
