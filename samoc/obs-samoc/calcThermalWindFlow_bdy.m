function [ trsp_geo ] = calcThermalWindFlow_bdy()
% Compute geostrophic interior component of SAMOC based on boundary density profiles 
%
%   Thermal wind states: 
%
%       dv_g/dz = - g/rho_0/f drho/dx 
%
%       v_g(z,t) = v_ref(t) + v_rel(z,t)
%
%           v_rel(z,t) = - g/rho_0/f int_{z_ref}^z( drho / dx(z,t) )
%
%   To compare to (Dong et al. 2015), take reference level at 1000m
%   Interpolate to find reference velocity at exactly 1000m
%
%   Here we flip the order of integration and first integrate zonally to get
%   The zonal geostrophic transport per depth level 
%       in units [m^2/s]
%
%       trsp_per_depth_rel(z,t) = -g/rho_0/f int_{z_ref}^z( rho_e(z,t) - rho_w(z,t) )dz
%
%   So the geostrophic component of the amoc is
%
%       trsp_geo_rel = int_{z_ref}^surf( trsp_per_depth_rel dz )
%
%   So the above calculations are implemented as:
%
%       trsp_geo = trsp_geo_rel + trsp_geo_ref
%
%   Note that sea surface height contribution is not included, as shown in 
%   Sinha et al., 2018, for Absolute Geostrophic transport.
%
%  Outputs: 
%
%       trsp_geo : [1 x Nt] vector of geostrophic transport
% -------------------------------------------------------------------------

mygrid=establish_mygrid('llc90');
read_dir = '../../../release2/nctiles_monthly/';
fig_dir = 'figures/'; if ~exist(fig_dir,'dir'), mkdir(fig_dir), end;
mat_dir = ['mat/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir), end;

Nr = length(mygrid.RC);
Nt = 240; 
vel_ref = zeros(1,Nt);              % [m/s]
vel_rel = zeros(Nr,Nt);             % [m/s]
rho_e = zeros(Nr,Nt);               % [kg/m^3]
rho_w = zeros(Nr,Nt);               % [kg/m^3]
trsp_per_depth_rel = zeros(Nr,Nt);  % [Sv/m]
trsp_per_depth_ref=zeros(1,Nt);     % [Sv/m]
trsp_geo_rel = zeros(1,Nt);         % [Sv]
trsp_geo_ref = zeros(1,Nt);         % [Sv]
trsp_geo = zeros(1,Nt);             % [Sv]


% Compute masks between E/W boundary points
% this gives latitude band from E/W points 
[mskC, mskS, mskW, mskCE, mskCW] = calcZonalMasks();

% Constant parameters
z_ref = -1000; % reference level 1000m
k_ref1= find(mygrid.RC>z_ref,1,'last');
k_ref2 = k_ref1+1; % 
% interp fraction btwn cell centers for reference velocity
dk = (z_ref-mygrid.RC(k_ref1))/(mygrid.RC(k_ref2)-mygrid.RC(k_ref1));  
% interp fraction between last cell face and 1000m ... 
% basically, how much of next cell to include in accumulated transport
% because when accumulating and multiply by dRF, already grab transport in k_ref1
dkf = (z_ref-mygrid.RF(k_ref1+1))/(mygrid.RF(k_ref2+1)-mygrid.RF(k_ref1+1));
dz = mygrid.DRC(k_ref2)*dk; 
f = 2*7.27e-5*sind(-34);  % [1/s]
rho_0 = 1029; %[kg/m^3] 
g = 9.81; %[m/s^2]
Lx= nansum(abs(mskW(:,:,1)).*mygrid.DYG + abs(mskS(:,:,1)).*mygrid.DXG); %[m]
dxg = mk3D(mygrid.DXG,mskC); % [m]
dyg = mk3D(mygrid.DYG,mskC); % [m]



% Check: Compute bottom point for each boundary
%    if bottom is smaller than reference level, have problems
k_bottom=0;
for k=1:Nr
    if nansum(nansum(mskCE(:,:,k)))~=0 && nansum(nansum(mskCW(:,:,k)))~=0
        k_bottom=k_bottom+1;
    end
end
if k_ref1>=k_bottom 
    error('Check E/W points, reference depth is below bathymetry level i.e. k_ref1<k_bottom'); 
end

% Convert to array for speed
mskCE=convert2array(mskCE); mskCE(isnan(mskCE))=0;
mskCW=convert2array(mskCW); mskCW(isnan(mskCW))=0;
mskC=convert2array(mskC);   mskC(isnan(mskC))=0;
mskW=convert2array(mskW);   mskW(isnan(mskW))=0;
mskS=convert2array(mskS);   mskS(isnan(mskS))=0;
dxg=convert2array(dxg);     dxg(isnan(dxg))=0;
dyg=convert2array(dyg);     dyg(isnan(dyg))=0;


for n = 1:Nt

    % Read in monthly values from file
    rho = read_nctiles([read_dir 'RHOAnoma/RHOAnoma'],'RHOAnoma',n);
    uvel = read_nctiles([read_dir 'UVELMASS/UVELMASS'],'UVELMASS',n);
    vvel = read_nctiles([read_dir 'VVELMASS/VVELMASS'],'VVELMASS',n);

    % Mask out values and convert to array for speed 
    rho = convert2array(rho).*mskC;
    uvel = convert2array(uvel);
    vvel = convert2array(vvel);
    rho(isnan(rho))=0;  
    uvel(isnan(uvel))=0; vvel(isnan(vvel))=0;

    % Compute reference velocity: only computing average over length 
    uvel_interp = uvel(:,:,k_ref1) + dk*(uvel(:,:,k_ref2)-uvel(:,:,k_ref1));
    vvel_interp = vvel(:,:,k_ref1) + dk*(vvel(:,:,k_ref2)-vvel(:,:,k_ref1));
    mskS_interp = mskS(:,:,k_ref1) + dk*(mskS(:,:,k_ref2)-mskS(:,:,k_ref1));
    mskW_interp = mskW(:,:,k_ref1) + dk*(mskW(:,:,k_ref2)-mskW(:,:,k_ref1));
    trsp_per_depth_ref(n) =nansum(nansum(vvel_interp.*mskS_interp.*dxg(:,:,k_ref1),1),2) + ...
                           nansum(nansum(uvel_interp.*mskW_interp.*dyg(:,:,k_ref1),1),2); %[m^2/s]
    trsp_per_depth_ref(n) = 10^-6*trsp_per_depth_ref(n); % [Sv/m]
    trsp_geo_ref(n) = trsp_per_depth_ref(n)*(nansum(mygrid.DRF(1:k_ref1))+dkf*mygrid.DRF(k_ref2)); %[Sv]
    vel_ref(n)      = 10^6 * trsp_per_depth_ref(n) / Lx; %[m/s]

    % Get densities and SSH at boundaries 
    rho_e(1:k_bottom,n) = rho(logical(mskCE(:,:,1:k_bottom)));
    rho_w(1:k_bottom,n) = rho(logical(mskCW(:,:,1:k_bottom)));

    % Compute transport in sverdrups
    trsp_per_depth_rel(:,n) = -10^-6 * g/f/rho_0 * (rho_e(:,n) - rho_w(:,n)); %[Sv / m] 
    % Don't zero below k_ref2 for comparison to Dong etal, fig1
    % Just ensure to
    %   1. make k_ref2 appropriate fraction for 1000m interpolation
    %   2. only sum from k_ref2 up
    trsp_per_depth_rel(k_ref2,n) = dkf*trsp_per_depth_rel(k_ref2,n);
    trsp_per_depth_rel(1:k_ref2,n) = flipdim(cumsum(flipdim(...
                                trsp_per_depth_rel(1:k_ref2,n).*mygrid.DRF(1:k_ref2),...
                                1),1),1);

    % Compute average velocity to compare to (Dong et al, 2014)
    vel_rel(:,n) = 10^6 * trsp_per_depth_rel(:,n) / Lx;

    % Compute northward geostrophic transport
    % accumulate to amoc depth 
    trsp_geo_rel(n) = sum(trsp_per_depth_rel(1:k_ref2,n).*mygrid.DRF(1:k_ref2)); % [Sv]

    % Finally, geostrophic transport is relative+reference
    trsp_geo(n) = trsp_geo_rel(n)+trsp_geo_ref(n); % [Sv]

    if mod(n,12)==0; 
        fprintf('## Thermal wind flow: %d / %d \n',n,Nt); 
    end
end

% Save it all 
save([mat_dir 'thermalWindFlow_bdy.mat'],'trsp_per_depth_rel','trsp_per_depth_ref',...
                             'trsp_geo_rel','trsp_geo_ref','trsp_geo',...
                             'vel_ref','vel_rel','rho_e','rho_w','Lx');
end
