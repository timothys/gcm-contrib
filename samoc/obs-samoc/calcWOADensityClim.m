function [lat,lon,z,rho_anom,theta_mean,drhodz] = calcWOADensityClim()
% Compute WOA temperature, salinity, density, and geostrophic trsp fields 
% with gridded temperature and salinity WOA data from 
% https://www.nodc.noaa.gov/OC5/woa13/
%
% --------------------------------------------------------------------

mat_dir = 'mat'; if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_dir = sprintf('%s/woa-data',mat_dir); if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_file = sprintf('%s/theta_salt_clim.mat',mat_dir);

if ~exist(mat_file,'file')
    [lat,lon,z,pressure,theta_clim,salt_clim,rho_clim] = calcWOAClimatologies(mat_file);
    fprintf('Saving %s ...\n',mat_file);
    save(mat_file,'lat','lon','z','pressure','theta_clim','salt_clim','rho_clim')
else
    fprintf('Loading %s ...\n',mat_file);
    load(mat_file,'lat','lon','z','pressure','theta_clim','salt_clim','rho_clim');
end

% --- Compute mean and anomaly
[theta_mean, theta_anom] = calcMeanAndAnom(theta_clim);
[salt_mean, salt_anom] = calcMeanAndAnom(salt_clim);
[rho_mean, rho_anom] = calcMeanAndAnom(rho_clim);

drhodz = calc_minus_drhodz(z,rho_clim);

end

% --------------------------------------------------------------------

function [drhodz] = calc_minus_drhodz(z,rho)
% Compute negative vertical density gradient 

Nlat = size(rho,1);
Nlon = size(rho,2);
Nz = size(rho,3);
Nt = size(rho,4);
drhodz = zeros(Nlat,Nlon,Nz-1,Nt);


for n=1:Nt
    for k=1:Nz-1
        dz = z(k)-z(k+1);
        drho = squeeze(rho(:,:,k,n) - rho(:,:,k+1,n));
        drhodz(:,:,k,n) = drho ./ dz;
    end
end

drhodz = -drhodz;

end

% --------------------------------------------------------------------

function [rho] = calcDensity(theta,salt,pressure)
% Compute density using MITgcm/utils/matlab/densjmd95.m
% Local function that works on climatology ... 
% Assume inputs are a climatology
% Assume (from WOA FAQ section):
%

%   theta as in-situ temperature in units degC
%   salinity as practical salinity scale 78
%   pressure in dBar (computed from GSW)
%

    rho = zeros(size(theta));
    
    Nt = size(theta,4);
    Nz = size(theta,3);
    Nlat=size(theta,2);
    Nlon=size(theta,1);

    for k = 1:Nz
        for n = 1:Nt
            tt = squeeze(   theta(:,:,k,n));
            ss = squeeze(    salt(:,:,k,n));
            pp = squeeze(pressure(:,:,k));
            rho(:,:,k,n) = densjmd95(ss,tt,pp);
        end
        fprintf(' --- Computing rho: %d / %d ---\n',k,Nz);
    end

end

% --------------------------------------------------------------------

function [lat,lon,z,pressure,theta,salt,rho] = calcWOAClimatologies(mat_file)

[lat,lon,z,theta] = loadWOAData('woa-data/woa13_decav_t','mn01v2.csv');
[~,~,~,salt] = loadWOAData('woa-data/woa13_decav_s','mn01v2.csv');

% Ok fuck you matlab, csvread places missing values with 0's ... 
% assume that BOTH theta and salt = 0 means a missing value
missing_val = salt == 0;
theta(missing_val) = NaN;
salt(missing_val) = NaN;

% Compute pressure ...
pressure = zeros(length(lon),length(lat),length(z));
for i=1:length(lat)
    for j=1:length(z)
        tmp = gsw_p_from_z(z(j),lat(i));
        pressure(:,i,j) = repmat(tmp,[length(lon) 1 1]);
    end
end
rho = calcDensity(theta,salt,pressure);

end

% --------------------------------------------------------------------

function [lat,lon,z,gridded_data] = loadWOAData(file_prefix,file_suffix)

% Taken from the meta file 
depth = [0,5,10,15,20,25,30,35,...
         40,45,50,55,60,65,70,75,...
         80,85,90,95,100,125,150,175,...
         200,225,250,275,300,325,350,...
         375,400,425,450,475,500,550,...
         600,650,700,750,800,850,900,...
         950,1000,1050,1100,1150,1200,...
         1250,1300,1350,1400,1450,1500]';

z = -depth;
Nz = length(z);

% Assume ...
lon = [-179.5:1:179.5]';
Nlon = length(lon);

% Chop off some years which have higher latitudes so I don't have to
% use cell structures to represent this shit
lat = [-75.5:1:87.5]';
Nlat = length(lat);

gridded_data = zeros(Nlon,Nlat,Nz,12);

% From looking at the files ...
row_offset = 2;
column_offset = 0;

for n = 1:12


    % File prefix and suffix are just fitting into this gross, long csv format
    filename = sprintf('%s%02d%s',file_prefix,n,file_suffix);
    fprintf('Reading csv file: %s ...\n',filename);

    data_mat = csvread(filename,row_offset,column_offset);
    lat_list = data_mat(:,1);
    lon_list = data_mat(:,2);

    [mylat,row_ind] = unique(lat_list);
    row_ind = [row_ind; length(lat_list)+1];

    Nlatlocal = length(mylat);

    % After some testing, assume that longitude spans -179.5 to 179.5, 1 pt per degree
    % Loop through and find latitude bands which I'm not chopping off ...
    for i = 1:Nlatlocal

        % Find index where input latitude is inside the lat vector
        lat_ind = find(lat == mylat(i));

        % if it exists, put each point by longitude into the grid
        if ~isempty(lat_ind)

            % Grab rows in matrix for this particular latitude band
            beg = row_ind(i);
            ed = row_ind(i+1)-1;

            for j = beg:ed

                % For each row, identify longitude and put the data in grid form
                lon_ind = find(lon == lon_list(j));

                tmp = data_mat(j,3:end);
                gridded_data(lon_ind,lat_ind,:,n) = reshape(tmp,[1 1 Nz 1]);
            end
        end
    end

end


end

% --------------------------------------------------------------------

function [fld_mean,fld_anom] = calcMeanAndAnom(fld)
% Assumes time is on 4th dimension...

sz = size(fld);
fld_mean = nanmean(fld,length(sz));
fld_anom = fld - fld_mean;

end


% --------------------------------------------------------------------
