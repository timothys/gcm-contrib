% Compute AMOC at 34S from observer's perspective (using computed
% quantities), as in Meinen et al. These components are: 
%   1. Boundary flow East of 17.5E and West of 51.5W (the moorings)
%       Note: with ECCOv4 resolution, not represented well. 
%       Only West portion returns anything, and it's an order of magnitude
%       smaller than computed in the paper. 
%   2. Geostrophic interior.
%   3. Ekman flow
% -------------------------------------------------------------------------

establish_mygrid;
read_dir = '../../release2/nctiles_monthly/';
fig_dir = 'figures/'; if ~exist(fig_dir,'dir'), mkdir(fig_dir), end;
mat_dir = 'mat/'; if ~exist(mat_dir,'dir'), mkdir(mat_dir), end;

Nr = length(mygrid.RF);
Nt = 240; 

%% Boundary flow component
loadFile = [mat_dir 'bdyFlow.mat'];
if ~exist(loadFile,'file')
    [trVolZon_bdyE, trVolZon_bdyW] = calcBdyFlow(mygrid); 
else
    load(loadFile,'trVolZon_bdyE','trVolZon_bdyW');
end

trVolZon_bdy = trVolZon_bdyE+trVolZon_bdyW;

%% Geostrophic interior 
loadFile = [mat_dir 'geoFlow.mat'];
if ~exist(loadFile,'file')
    [trVolZon_geo]=calcGeoFlow(mygrid);
else
    load(loadFile,'trVolZon_geo');
end

%% Reference flow to geostrophic (bottom pressure) component
loadFile = [mat_dir 'refFlow.mat'];
if ~exist(loadFile,'file')
    [trVolZon_ref]=calcRefFlow(mygrid);
else
    load(loadFile,'trVolZon_ref');
end

%% Ekman component 
loadFile = [mat_dir 'ekFlow.mat'];
if ~exist(loadFile,'file')
    [trVolZon_ek]=calcEkFlow(mygrid);
else
    load(loadFile,'trVolZon_ek');
end

%% All together now
trVolZon = trVolZon_bdy + trVolZon_geo; 
trVolZon(1,:) = trVolZon(1,:) + trVolZon_ek; 
trVolZon(Nr-1,:) = trVolZon(Nr-1,:) - trVolZon_ref;

ovStf = -flipdim(cumsum(flipdim(trVolZon,1),1),1);
ovStf_bdy = -flipdim(cumsum(flipdim(trVolZon_bdy,1),1),1); 
ovStf_geo = -flipdim(cumsum(flipdim(trVolZon_geo,1),1),1); 

%% AMOC is integrated (monthly Eulerian mean) transport at maximized depth
kmax = zeros(1,Nt);
amoc = zeros(1,Nt);
depthmax = zeros(1,Nt);
for n = 1:Nt
    [~,kmax(n)] = max(ovStf(:,n));
    amoc(n) = ovStf(kmax(n),n);
    depthmax(n) = abs(mygrid.RF(kmax(n)));
end

%% Plot for SAMOC
slat = -34;
grayCol = [100 100 100]/255;

ylbl = 'Depth (m)';
xlbl = 'Zonally Integrated Transport (Sv)';
titleStr = sprintf('Zonally Integrated Transport at %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
depthStatsPlot(mygrid.RF,trVolZon,{xlbl,ylbl,titleStr})

titleStr = sprintf('Boundary flow component of ovStf %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
depthStatsPlot(mygrid.RF,ovStf_bdy,{xlbl,ylbl,titleStr})

titleStr = sprintf('Geostrophic flow component of ovStf %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
depthStatsPlot(mygrid.RF,ovStf_geo,{xlbl,ylbl,titleStr})

xlbl = 'Month (1992-2011)';
ylbl = 'Zonally integrated transport Sv (m)';
titleStr = sprintf('Reference flow component of ovStf %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
timeStatsPlot(1:Nt,trVolZon_ref,{xlbl,ylbl,titleStr})

titleStr = sprintf('Ekman flow component of ovStf %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
timeStatsPlot(1:Nt,trVolZon_ek,{xlbl,ylbl,titleStr})

xlbl = 'Stream Function (Sv)';
ylbl = 'Depth (m)';
titleStr = sprintf('Meridional Overturning Stream Function at %d\nfrom 1992-2011 ECCOv4r2 Fields',slat);
figureL
depthStatsPlot(mygrid.RF,ovStf,{xlbl,ylbl,titleStr})
    

xlbl = 'Month (1992-2011)';
ylbl1 = 'AMOC (Sv)';
ylbl2 = 'z_{max} (m)';
titleStr = 'AMOC Characteristics from ECCOv4r2 Fields'; 
figureL
timeStatsPlot(1:Nt, [amoc;depthmax], {xlbl,ylbl1,ylbl2,titleStr},2,'median');