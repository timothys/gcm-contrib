function [trVolZon_ek] = calcEkFlow() 
% Compute Ekman flow at 34S for comparison to Obs
%
%  Output: 
%
%       trsp_ek : [1 x Nt] zonally integrated volume transport
%                     northward at 34S due to Ekman flow. 
%
% -------------------------------------------------------------------------

mygrid=establish_mygrid('llc90');
read_dir = '../../../release2/nctiles_monthly/';
fig_dir = 'figures/'; if ~exist(fig_dir,'dir'), mkdir(fig_dir), end;
mat_dir = ['mat/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir), end;

Nt = 240;
trsp_ek = zeros(1,Nt);      % [Sv]

% Compute masks between E/W boundary points
% this gives latitude band from E/W points 
[~, mskS, mskW] = calcZonalMasks();
mskS = mskS(:,:,1);
mskW = mskW(:,:,1);

% Convert for speed
mskS = convert2array(mskS);
mskW = convert2array(mskW);

% Constant parameters
f = 2*7.27e-5*sind(-34);    % [1/s]
rho_0 = 1029;               %[kg/m^3] 
dxg = convert2array(mygrid.DXG);
dyg = convert2array(mygrid.DYG);


% Load and compute
taux = convert2array(read_nctiles([read_dir 'oceTAUX/oceTAUX'], 'oceTAUX'));
tauy = convert2array(read_nctiles([read_dir 'oceTAUY/oceTAUY'], 'oceTAUY'));

% use X-vector on gcmfaces 1, where u/v in right direction ... use south mask
% use y-vector on gcmfaces 5, where u/v flipped variables and sign, use west mask
trsp_ek = nansum(nansum( taux.*repmat(mskS.*dyg,[1 1 Nt]),1),2) + ...
          nansum(nansum(-tauy.*repmat(mskW.*dxg,[1 1 Nt]),1),2);

% Reshape, prefer time dimension always on last index
trsp_ek = reshape(trsp_ek,[1 Nt]);

trsp_ek = -10^-6 / f / rho_0 * trsp_ek;
    
save([mat_dir 'ekFlow.mat'],'trsp_ek');
end
