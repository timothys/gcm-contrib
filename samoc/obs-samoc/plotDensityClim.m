function [] = plotDensityClim()
% Make ECCO -vs- Argo density seasonal cycle comparison 
% Recreate plots from Dong et al., 2014 showing
%
%   1. meridional geostrophic velocity seasonal cycle at 34S
%   2. boundary density seasonal cycle rho_e, rho_w, rho_e-rho_w
%   3. 1000m mean temperature
%   4. september average drho/dz vs depth at 34S
% 
% Compute ECCO fields, see calcECCODensityClim.m
% Compute Argo fields, see calcArgoDensityClim.m
% Compute WOA fields,  see calcWOADensityClim.m
%
% Note: The ECCO fields are given "plot ready" here whereas Argo and WOA
%       data need some selections (e.g. the boundary points are grabbed here)
%       This is because the ECCO data are harder to work with, and this script
%       would get huge ... 
%
% -----------------------------------------------------------------


% --- 0.0 Load the data 
mygrid = establish_mygrid('llc90');
[lat,lon,argo_z,argo_msk,argo_geo_trsp,argo_geo_vel,argo_rho_anom,argo_theta_mean,argo_drhodz,argo_rho_mean] = calcArgoDensityClim();
[latw,lonw,woa_z,woa_rho_anom,woa_theta_mean,woa_drhodz] = calcWOADensityClim();
%[latR,latX,ecco_vel_geo,ecco_rho_e,ecco_rho_w,ecco_drhodz,ecco_theta_mean_1000,ecco_uvel_mean_1000,ecco_vvel_mean_1000] = calcECCODensityClim();
[latR,latX,ecco_vel_geo,ecco_rho_e,ecco_rho_w,ecco_drhodz,ecco_theta_mean_1000,ecco_uvel_mean_1000,ecco_vvel_mean_1000] = calcECCODensityClim_argo_years();

% --- 0.1 Figure settings
fs = 22;
cmap1=redblue(22);
cmap_redblue = cmap1([[1:10],[13:22]],:);

% --- 0.2 geostrophic seasonal cycle
figureW;
plot(1:12,argo_geo_trsp);
%savePlot('figures/shenfu-comp/geo_trsp_argo','pdf');
%close


% --- 1. meridional geostrophic transport
[argo_months,argo_depth,argo_rho_e,argo_rho_w] = grabArgoBdyDensities(argo_z,argo_rho_anom);

% ECCO 
figureL;
pcolor(1:12,latR,ecco_vel_geo)
fig1settings(fs,cmap_redblue);
%savePlot('figures/shenfu-comp/vel_clim_ecco','png','nocrop','-r300');
%close

% Argo
figureL;
pcolor(argo_months,argo_depth,argo_geo_vel);
fig1settings(fs,cmap_redblue);
%savePlot('figures/shenfu-comp/vel_clim_argo','png','nocrop','-r300');
%close

% --- 2. boundary density seasonal cycle

% ECCO 
figureL;
h1 = subplot(1,3,1);
    pcolor(1:12,latR,ecco_rho_w);
    ylabel('Depth (m)')
    fig2subSettings(fs,cmap_redblue,h1,1);   
    
h2 = subplot(1,3,2);
    pcolor(1:12,latR,ecco_rho_e);
    fig2subSettings(fs,cmap_redblue,h2,0);   

h3 = subplot(1,3,3);
    pcolor(1:12,latR,ecco_rho_e-ecco_rho_w);
    fig2subSettings(fs,cmap_redblue,h3,0);   

fig2settings(fs,cmap_redblue);
%savePlot('figures/shenfu-comp/density_clim_ecco','png','nocrop','-r300');
%close


% Argo
figureL;
h1 = subplot(1,3,1);
    pcolor(argo_months,argo_depth,argo_rho_w);
    ylabel('Depth (m)')
    fig2subSettings(fs,cmap_redblue,h1,1);   
    
h2 = subplot(1,3,2);
    pcolor(argo_months,argo_depth,argo_rho_e);
    fig2subSettings(fs,cmap_redblue,h2,0);   

h3 = subplot(1,3,3);
    pcolor(argo_months,argo_depth,argo_rho_e-argo_rho_w);
    fig2subSettings(fs,cmap_redblue,h3,0);   

fig2settings(fs,cmap_redblue);
%savePlot('figures/shenfu-comp/density_clim_argo','png','nocrop','-r300');
%close

% WOA
[woa_months,woa_depth,woa_rho_e,woa_rho_w] = grabWOABdyDensities(woa_z,woa_rho_anom);

figureL;
h1 = subplot(1,3,1);
    pcolor(woa_months,woa_depth,woa_rho_w);
    ylabel('Depth (m)')
    fig2subSettings(fs,cmap_redblue,h1,1);   
    
h2 = subplot(1,3,2);
    pcolor(woa_months,woa_depth,woa_rho_e);
    fig2subSettings(fs,cmap_redblue,h2,0);   

h3 = subplot(1,3,3);
    pcolor(woa_months,woa_depth,woa_rho_e-woa_rho_w);
    fig2subSettings(fs,cmap_redblue,h3,0);   

fig2settings(fs,cmap_redblue);
%savePlot('figures/shenfu-comp/density_clim_woa','png','nocrop','-r300');
%close

% --- 3. 1000m mean temperature plot
% Assume a depth level 1000m at 1000dBar, need to come back to this
%cmap = cbrewer2('YlOrRd',11);
cmap = shenfuColormap;

% Mollweide, as in paper
map_projection = 5.2;
% Mercator, as in Dong et al.
%map_projection = 5.3;
showCbar=1;

% ECCO
figureL;
m_map_atl(ecco_theta_mean_1000,...
          map_projection,...
          {'fontSize',fs},...
          {'doHold',1},...
          {'myCmap',cmap},...
          {'doCbar',showCbar})
m_map_gcmfaces_uv(ecco_uvel_mean_1000,ecco_vvel_mean_1000,...
                  map_projection,5,3,fs);

fig3settings(fs);            
%savePlot('figures/shenfu-comp/theta_1000m_ecco','png','nocrop','-r300');
%close

% Argo
[argo_temp_at_depth] = grabArgoTemperature(argo_z,argo_msk,argo_theta_mean);


figureL; 
m_map_nc(lat,lon,argo_temp_at_depth,...
            map_projection,...
            {'myCmap',cmap},...
            {'fontSize',fs},...
          {'doCbar',showCbar});
fig3settings(fs);
%savePlot('figures/shenfu-comp/theta_1000m_argo','png','nocrop','-r300');
%close

% WOA
[woa_temp_at_depth] = grabWOATemperature(woa_z,woa_theta_mean);
figureL; 
m_map_nc(latw,lonw,woa_temp_at_depth,...
            map_projection,...
            {'myCmap',cmap},...
            {'fontSize',fs},...
          {'doCbar',showCbar});

fig3settings(fs);
%savePlot('figures/shenfu-comp/theta_1000m_woa','png','nocrop','-r300');
%close


% --- 4. September drho/dz 

% ECCO
figureL;
pcolor(latX,mygrid.RF(2:end-1),ecco_drhodz{9});
fig4settings(fs);
%savePlot('figures/shenfu-comp/drhodz_sept_ecco','png','nocrop','-r300');
%close

% Argo
[argo_lon,argo_depth,argo_drhodz_sept] = grabArgoDensityShear(lon,argo_z,argo_msk,argo_drhodz);


figureL;
pcolor(argo_lon,argo_depth,argo_drhodz_sept);
fig4settings(fs);
%savePlot('figures/shenfu-comp/drhodz_sept_argo','png','nocrop','-r300');
%close

% WOA
[woa_lon,woa_depth,woa_drhodz_sept] = grabWOADensityShear(lonw,woa_z,woa_drhodz);

figureL;
pcolor(woa_lon,woa_depth,woa_drhodz_sept);
fig4settings(fs);
%savePlot('figures/shenfu-comp/drhodz_sept_woa','png','nocrop','-r300');
%close


end

% -----------------------------------------------------------------

function [mo_map,samoc_depth,rho_e,rho_w] = grabArgoBdyDensities(z,rho_anom)

    % indices for east and west boundaries at 
    [lat_ind,ind_e,ind_w] = calcZonalMasks_argo();

    
    rho_e = squeeze(nanmean(rho_anom(ind_e,lat_ind,:,:),1));
    rho_w = squeeze(nanmean(rho_anom(ind_w,lat_ind,:,:),1));
    samoc_depth = squeeze(z(1,lat_ind,:));
    samoc_depth = repmat(samoc_depth,[1 12]);
    mo_map = repmat(1:12, [size(samoc_depth,1) 1]);

end

% -----------------------------------------------------------------

function [mo_map,samoc_depth,rho_e,rho_w] = grabWOABdyDensities(z,rho_anom);

    % indices for east and west boundaries at 
    [lat_ind,ind_e,ind_w] = calcZonalMasks_woa();
    
    rho_e = squeeze(rho_anom(ind_e,lat_ind,:,:));
    rho_w = squeeze(rho_anom(ind_w,lat_ind,:,:));
    samoc_depth = repmat(z,[1 12]);
    mo_map = repmat(1:12, [size(samoc_depth,1) 1]);

end

% -----------------------------------------------------------------

function [temp_at_depth] = grabArgoTemperature(z,msk,theta_mean)

p_level = 44;

temp_at_depth = squeeze(msk(:,:,p_level).*theta_mean(:,:,p_level));

end

% -----------------------------------------------------------------

function [temp_at_depth] = grabWOATemperature(z,theta_mean)

z_lev = 47;

temp_at_depth = squeeze(theta_mean(:,:,z_lev));


end

% -----------------------------------------------------------------

function [lon_band,samoc_depth,drhodz_band_sept] = grabArgoDensityShear(lon,z,msk,drhodz)

% grab 34S lat band
lat_ind = calcZonalMasks_argo();
lon_band = repmat(lon,[1 size(z,3)-1]);

msk_band = squeeze(msk(:,lat_ind,:));
msk_band = msk_band(:,2:end);

% grab september density shear at 34s
drhodz_band = squeeze(drhodz(:,lat_ind,:,:));
drhodz_band_sept = squeeze(drhodz_band(:,:,9));
drhodz_band_sept = drhodz_band_sept .* msk_band;

samoc_depth = squeeze(z(:,lat_ind,:));
samoc_depth = (samoc_depth(:,1:end-1) + samoc_depth(:,2:end))/2;

end

% -----------------------------------------------------------------

function [lon_band,samoc_depth,drhodz_band_sept] = grabWOADensityShear(lon,z,drhodz)

% grab 34S lat band
lat_ind = calcZonalMasks_argo();
lon_band = repmat(lon,[1 length(z)-1]);

% grab september density shear at 34s
drhodz_band = squeeze(drhodz(:,lat_ind,:,:));
drhodz_band_sept = squeeze(drhodz_band(:,:,9));

zz = (z(1:end-1) + z(2:end))/2;
samoc_depth = repmat(zz',[length(lon) 1]);

end

% -----------------------------------------------------------------

function [] = fig1settings(fontSize,cmap)

    shading interp
    set(gca,'fontsize',fontSize)
    xlbl_clim; xlim([1 12])
    set(get(gca,'xaxis'),'tickdirection','out');
    set(get(gca,'yaxis'),'tickdirection','out');
    ylim([-1200 0])
    ylabel('Depth (m)')
    set(gca,'ytick',[-1200:200:0],'yticklabel',num2cell([-1200:200:0]));
    caxis([-.001 .001])
    colormap(cmap)
    hc=niceColorbar;
    set(hc,'location','southoutside')

        %'units','normalized','position',[3.33 0.529 0],...
        %'rotation',0)
    hh=gca;
    hh.Units='normalized';
    hh.Position(2) = 0.2;    
    hh.Position(4) = 0.77;
    set(hc,'units','normalized','position',[0.132 0.08 0.777 0.04],...
                'ticks',[-0.001:0.0002:0.001],'fontsize',fontSize)
    set(get(hc,'label'),'string','m s$^{-1}$','fontsize',fontSize,...
                    'units','normalized','position',[0.5 -1.1 0.0]);
end

% -----------------------------------------------------------------

function [] = fig2subSettings(fontSize,cmap,ph,doYLabel)

    colormap(cmap);
    shading(ph,'interp')
    set(gca,'fontsize',fontSize)
    xlbl_clim_half;
    set(get(gca,'xaxis'),'tickdirection','out');
    set(get(gca,'yaxis'),'tickdirection','out');
    
    set(gca,'ytick',[-1200:200:0]);
    if doYLabel==1
        set(gca,'yticklabel',num2cell([-1200:200:0]));
    else
        set(gca,'yticklabel',[]);
    end
    ylim([-1200 0])
    cmax = 0.1;
    caxis([-cmax cmax])
    colormap(cmap)
    ph.Units='normalized';
    ph.Position(2)=0.2;
    %ph.Position(3)=0.24;
    ph.Position(4)=0.77;
    ph.XAxis.FontSize=fontSize;
    ph.XAxis.TickLabelRotation=45;

end

% -----------------------------------------------------------------

function [] = fig2settings(fontSize,cmap)

hc=niceColorbar;%
colormap(cmap); %
set(hc,'location','southoutside')
set(hc,'units','normalized','position',[0.132 0.08 0.777 0.04],...
            'ticks',[-0.1:0.02:0.1],'fontsize',fontSize)
set(get(hc,'label'),'string','kg m$^{-3}$','fontsize',fontSize,...
                    'units','normalized','position',[0.5 -1.1 0.0])

end

% -----------------------------------------------------------------

function [] = fig3settings(fontSize)

    hh=gca;
    hh.DataAspectRatio = [1 0.39 1];
    hh.Position(1) = 0.15;
    hh.Position(2) = 0.2;
    hh.Position(3) = 0.7723;
    hh.Position(4) = 0.77;
    hc=niceColorbar;
    caxis([3 5.5])
    set(hc,'location','southoutside')
    set(hc,'units','normalized','position',[0.132 0.08 0.777 0.04],...
            'ticks',[3:.5:5.5],'fontsize',fontSize)
    set(get(hc,'label'),'string','$^\circ$C','fontsize',fontSize,...
                    'units','normalized','position',[0.5 -1.1 0.0])

end

% -----------------------------------------------------------------

function [] = fig4settings(fontSize)

    shading interp
    xlim([-52 19]);
    ylim([-1000 0]);
    ylabel('Depth (m)')
    set(get(gca,'xaxis'),'tickdirection','out');
    set(get(gca,'yaxis'),'tickdirection','out');
    set(gca,'fontsize',fontSize,...
        'ytick',[-1000:200:0],'yticklabel',num2cell([-1000:200:0]'),...
        'xtick',[-50:10:20],'xticklabel',...
        {'50$^{\circ}$W','40$^{\circ}$W','30$^{\circ}$W','20$^{\circ}$W','10$^{\circ}$W','0$^{\circ}$E','10$^{\circ}$E'});

    colormap(shenfuColormap); % Match (Dong et al, 2014)
    hc=niceColorbar;
    caxis([.0045 .012])
    set(hc,'fontsize',fontSize,'location','southoutside');
    set(gca,'color',[1 1 1]*.7); % set NaN color to gray
    hh=gca;
    hh.Units='normalized';
    hh.Position(2) = 0.2;    
    hh.Position(4) = 0.77;
    set(hc,'units','normalized','position',[0.132 0.08 0.777 0.04],...
                'ticks',[-.005:.001:.012],'fontsize',fontSize)
    set(get(hc,'label'),'string','kg m$^{-4}$','fontsize',fontSize,...
                    'units','normalized','position',[0.5 -1.1 0.0]);
end
