function [] = compShenfuAmoc( obsType, climOrAnnual, varargin )
%
% Compare SAMOC from ECCOv4r2 and observations from Shenfu Dong
%
% Inuts: 
%       
%       obsType : 'xbt', 'ssha', argo'
%       climOrAnnual : 'clim', 'annual'
%
% Optional inputs: 
%
%       amocType : 'full' (default) , 'ekman','geo'
%
% ------------------------------------------------------------------------
amocType='full';
for n=1:length(varargin)
    if ischar(varargin{n})
        if ~isempty(strfind(varargin{n},'ek'))
            amocType='ek';
        else
            amocType=varargin{n};
        end
    end
end

if ~iscell(obsType)
  obsType={obsType};
end

modelVars = 'eccov4r2';
exfVars = 'short-atm';
cd ..
[~,flux_ind] = set_adjField( 'atm', climOrAnnual );
cd obs-samoc

% Load ECCOv4r2 AMOC
modelAmoc = loadModelAmoc( modelVars, exfVars, flux_ind, climOrAnnual, amocType );

% Load Shenfu 
[obsAmoc,obsType_found] = loadObsAmoc( climOrAnnual, obsType, amocType );

% Plot em up
plotSAMOCS( modelAmoc, obsAmoc, obsType_found , climOrAnnual, amocType )

end % end compShenfuAmoc

% ------------------------------------------------------------------------

function [] = plotSAMOCS( modelAmoc, obsAmoc, obsType, climOrAnnual, amocType )

obsAmoc_cell=struct2cell(obsAmoc);
Nobs = length(obsAmoc_cell);

% --- Directories and savename
mainSaveDir = 'figures';
saveDir = sprintf('%s/shenfu-comp',mainSaveDir);
if ~exist(mainSaveDir,'dir'), mkdir(mainSaveDir); end;
if ~exist(saveDir,'dir'), mkdir(saveDir); end;

savename = sprintf('%s/',saveDir);
savename = [savename climOrAnnual '_'];

for n=1:Nobs
    if ~isempty(obsAmoc_cell{n})
        savename=[ savename obsType{n} '_'];
    end
end
savename = [savename amocType];

% --- Grab time vector
if size(modelAmoc,2) == 12
  t=1:12;
end

% --- Cbrewer colormap
cmap = cbrewer2('Dark2',1+Nobs);

% --- Make plot
figureW; 
ax=plot(t,modelAmoc);
set(ax,{'color'},num2cell(cmap(1,:),2));
hold on

for n=1:Nobs
  if ~isempty(obsAmoc_cell{n})
    ax=plot(t,obsAmoc_cell{n});
    set(ax,{'color'},num2cell(cmap(n+1,:),2));
  end
end

fs = 30;
set(gca,'fontsize',fs)

% --- Handle y axis
ylabel('Sv')
set(gca,'ylim',[-5 5],'ytick',[-5:5],'yticklabel',num2cell([-5:5]));
ga = gca;

ga.YLabel.Rotation=0;
ga.YLabel.FontSize=fs;
ga.XLabel.FontSize=fs;
ga.YLabel.Units='normalized';
ga.YLabel.Position = [-0.064 0.46 0];

% --- Legend stuff
legendLbl=cell(1,Nobs+1);
legendLbl{1} = 'ECCOv4r2';
k=2;
for i=1:Nobs
    if ~isempty(obsAmoc_cell{i})
        if strcmp(obsType{i},'argo')
            if strcmp(amocType,'geo')
                legendLbl{k}='Argo';
            elseif strcmp(amocType,'ek')
                legendLbl{k}='SCOW';
            elseif strcmp(amocType,'full')
                legendLbl{k}='Argo+SCOW';
            end
        end
        if strcmp(obsType{i},'ssha')
            if strcmp(amocType,'geo')
                legendLbl{k}='AVISO Altimetry';
            elseif strcmp(amocType,'ek')
                legendLbl{k}='NCEP Reanalysis 2';
            elseif strcmp(amocType,'full')
                legendLbl{k}='AVISO+NCEP2';
            end
        end
        if strcmp(obsType{i},'xbt')
            if strcmp(amocType,'geo')
                legendLbl{k}='AX18 XBT Line';
            elseif strcmp(amocType,'ek')
                legendLbl{k}='NCEP Reanalysis';
            elseif strcmp(amocType,'full')
                legendLbl{k}='AX18+NCEP';
            end

        end
        k=k+1;
    end
end

% --- Legend position per plot
if strcmp(amocType,'ek')
    hl=legend(legendLbl,'location','south','fontsize',fs);
elseif strcmp(amocType,'geo')
    hl=legend(legendLbl,'location','southwest','fontsize',fs);
elseif strcmp(amocType,'full')
    hl=legend(legendLbl,'location','south','fontsize',fs);
    hl.Position(1)=0.35;
end
if size(modelAmoc,2)==12
  xlbl_clim;
end

grid on

savePlot(savename,'pdf');

keyboard

end %end plotSAMOCS


% ------------------------------------------------------------------------

function [obsAmoc,obsType_found] = loadObsAmoc( climOrAnnual, obsType, amocType )

Nobs = length(obsType);
obsAmoc = struct('argo',[],'ssha',[],'xbt',[]);
obsAmoc_cell = cell(Nobs);
obsType_found = {'','',''};

% Parse clim/annual
for n=1:Nobs
    if strcmp(climOrAnnual,'clim')
        obsAmoc_cell{n} = zeros(1,12);
    elseif strcmp(climOrAnnual,'annual')
        error('Need to assign num years ...');
    else
        error('Don''t recognize climOrAnnual option ');
    end
end

% Parse obs type
for n=1:Nobs

    if strcmp(obsType{n},'ssha')
        if strcmp(climOrAnnual,'clim')

            matfile = '../mat/shenfu-data/moc_monthly_altimeter';
            load(matfile,'moc_ekm_mon','moc_geo_mon','moc_mon');

            if strcmp(amocType,'full')
                sshaAmoc = moc_mon'-mean(moc_mon);
            elseif strcmp(amocType,'geo')
                sshaAmoc = moc_geo_mon' - mean(moc_geo_mon);
            elseif strcmp(amocType,'ekman') || strcmp(amocType,'ek')
                sshaAmoc = moc_ekm_mon' - mean(moc_ekm_mon);
            end
            %ssha=struct('amoc', sshaAmoc, 'ek', sshaAmoc_ekm, 'geo', sshaAmoc_geo);
        end

        % Put in containers
        obsAmoc.ssha = sshaAmoc;
        obsType_found{2} = 'ssha';

    elseif strcmp(obsType{n},'argo')
        if strcmp(climOrAnnual,'clim')

            matfile = '../mat/shenfu-data/moc_argo_annual';
            load(matfile,'moc_annual','mocg_annual','moce_annual');

            if strcmp(amocType,'full')
                argoAmoc = moc_annual-mean(moc_annual);
            elseif strcmp(amocType,'geo')
                argoAmoc = (mocg_annual - mean(mocg_annual))*10^-6;
            elseif strcmp(amocType,'ekman') || strcmp(amocType,'ek')
                argoAmoc = (moce_annual - mean(moce_annual))*10^-6;
            end
            %argo=struct('amoc', argoAmoc, 'ek', argoAmoc_ekm, 'geo', argoAmoc_geo);
        end  

        % Put in containers
        obsAmoc.argo = argoAmoc;
        obsType_found{1} = 'argo';

    elseif strcmp(obsType{n},'xbt')
        if strcmp(climOrAnnual,'clim')
            matfile = '../mat/shenfu-data/moc_ax18_annual';
            load(matfile,'moc_annual','moc_geo_annual','moc_ekm_annual');

             
            day_per_month = [0 31 28 31 30 31 30 31 31 30 31 30 31];
            day_in_year = cumsum(day_per_month);

            moc_annual_mo = zeros(1,12);
            moc_ek_annual_mo = zeros(1,12);
            moc_geo_annual_mo = zeros(1,12);

            for i=1:12
                st=day_in_year(i)+1;
                ed=day_in_year(i+1);

                moc_annual_mo(i) = mean(moc_annual(st:ed));
                moc_ek_annual_mo(i)=mean(moc_ekm_annual(st:ed));
                moc_geo_annual_mo(i)=mean(moc_geo_annual(st:ed));
            end

            if strcmp(amocType,'full')
                xbtAmoc = moc_annual_mo-mean(moc_annual_mo);
            elseif strcmp(amocType,'geo')
                xbtAmoc = (moc_geo_annual_mo - mean(moc_geo_annual_mo));
            elseif strcmp(amocType,'ekman') || strcmp(amocType,'ek')
                xbtAmoc = (moc_ek_annual_mo - mean(moc_ek_annual_mo));
            end
            %xbt=struct('amoc', xbtAmoc, 'ek', xbtAmoc_ekm, 'geo', xbtAmoc_geo);

        elseif strcmp(climOrAnnual,'annual')
            error('need to look closer at these data ... :|')
        end  

        % Put in containers
        obsAmoc.xbt = xbtAmoc;
        obsType_found{3} = 'xbt';
    end
end

% Make it a structure


for n=1:Nobs
  if exist('sshaAmoc','var')

  end
  if exist('argoAmoc','var')

  end
  if exist('xbtAmoc','var')

  end
end


end % end loadModelAmoc

% ------------------------------------------------------------------------

function [modelAmoc] = loadModelAmoc( modelVars, exfVars, flux_ind, climOrAnnual, amocType );

if strcmp(amocType,'full')

    loadDir = 'mat/eccov4r2/state/';
    cd ..
    [modelAmoc] = calcAMOC(-34,'eulerian',loadDir);
    cd obs-samoc

elseif strcmp(amocType,'geo')

    %load('mat/geoFlow.mat','trsp_geo');
    %load('mat/thermalWindFlow.mat','trsp_geo');
    load('mat/thermalWindFlow_bdy.mat','trsp_geo');
    modelAmoc = trsp_geo;


elseif strcmp(amocType,'ekman') || strcmp(amocType,'ek')

    load('mat/ekFlow.mat','trsp_ek');
    modelAmoc = trsp_ek;

end

% Remove time mean
modelAmoc = modelAmoc - nanmean(modelAmoc);

%% Now compute proper averaging
if ~isempty(strfind(climOrAnnual,'clim'))
  modelAmoc=calcClimatology(modelAmoc,size(modelAmoc,2)/12);
elseif ~isempty(strfind(climOrAnnual,'annual')) 
  modelAmoc=calcAnnualMean(modelAmoc,size(modelAmoc,2)/12);
end
end %end loadModelAmoc
