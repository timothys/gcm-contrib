function [theta_mean_1000] = calcThetaDuringArgoYears()
% Compute time mean temperature during the same time period as Argo

mygrid = establish_mygrid('llc90');

theta_dir = '../../../ecco/release2/nctiles_monthly/THETA/';
filename = sprintf('%s/THETA',theta_dir);

mat_file = 'mat/theta_argo_years.mat';

if ~exist(mat_file,'file')

    % The time indices: 2004 on
    time_ind = 145:240;
    Nyrs=length(time_ind)/12;
    
    % Depth levels to 1000m
    z_ref = -1000;
    k_ref1 = find(mygrid.RC>z_ref,1,'last');
    k_ref2 = k_ref1+1;
    
    dk = (z_ref - mygrid.RC(k_ref1))/(mygrid.RC(k_ref2)-mygrid.RC(k_ref1));
    dz = mygrid.DRC(k_ref2)*dk;
    
    % Read the file, only for these indices
    theta = read_nctiles(filename,'THETA',time_ind,[k_ref1,k_ref2]);

    % Time average
    theta_mean = nanmean(theta,4);
    
    % Interpolate between these two indices
    theta_mean_1000 = theta_mean(:,:,1) + dk*(theta_mean(:,:,2)-theta_mean(:,:,1));


    % Does not matter if we take climatology first 
    %theta_clim_first = calcClimatology(theta,Nyrs,0);
    %theta_mean_2 = nanmean(theta_clim_first,4);
    %theta_mean_2 = theta_mean_2(:,:,1) + dk*(theta_mean_2(:,:,2)-theta_mean_2(:,:,1));

    % Also does not matter if we interpolate first
    %theta_interp_first = squeeze(theta(:,:,1,:)+dk*(theta(:,:,2,:)-theta(:,:,1,:)));
    %theta_mean_3 = nanmean(theta_interp_first,3);

    % Reading only the k_ref and time_ind parts of nc files works!
    %theta_full = read_nctiles(filename,'THETA');
    %theta_mean_full = nanmean(theta_full(:,:,:,time_ind),4);
    %theta_mean_4 = theta_mean_full(:,:,k_ref1) + dk*(theta_mean_full(:,:,k_ref2)-theta_mean_full(:,:,k_ref1));

    save(mat_file,'theta_mean_1000');

else
    load(mat_file,'theta_mean_1000');
end

end
