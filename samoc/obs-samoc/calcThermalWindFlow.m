function [ trsp_geo ] = calcThermalWindFlow()
% Compute geostrophic interior component of SAMOC from thermal wind relation 
%
%   Thermal wind states: 
%       where \phi= p/rho_0
%
%       dv_g/dz = - g/rho_0/f drho/dx 
%
%       v_g(z) = v(ref) + v_rel 
%
%           v_rel = - g/rho_0/f int_{z_ref}^z( drho / dx )
%
%   To compare to (Dong et al. 2015), take reference level at 1000m
%   Interpolate to find reference velocity at exactly 1000m
%
%   The zonal geostrophic transport per depth level 
%       in units [m^2/s]
%
%       trsp_per_depth_rel = int_W^E ( v_rel dx ) 
%
%   So the geostrophic component of the amoc is
%
%       trsp_geo_rel = int_{z_ref}^surf( trsp_per_depth_rel dz )
%
%   So the above calculations are implemented as:
%
%       trsp_geo = trsp_geo_rel + trsp_geo_ref
%
%  Outputs: 
%
%       trsp_geo : [1 x Nt] vector of geostrophic transport
% -------------------------------------------------------------------------

mygrid=establish_mygrid('llc90');
read_dir = '../../../release2/nctiles_monthly/';
fig_dir = 'figures/'; if ~exist(fig_dir,'dir'), mkdir(fig_dir), end;
mat_dir = ['mat/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir), end;

Nr = length(mygrid.RC);
Nt = 240; 
vel_ref = zeros(1,Nt);              % [m/s]
vel_rel = zeros(Nr,Nt);             % [m/s]
rho_e = zeros(Nr,Nt);               % [kg/m^3]
rho_w = zeros(Nr,Nt);               % [kg/m^3]
trsp_per_depth_rel = zeros(Nr,Nt);  % [Sv/m]
trsp_per_depth_ref=zeros(1,Nt);     % [Sv/m]
trsp_geo_rel = zeros(1,Nt);         % [Sv]
trsp_geo_ref = zeros(1,Nt);         % [Sv]
trsp_geo = zeros(1,Nt);             % [Sv]

% Compute masks between E/W boundary points
% this gives latitude band from E/W points 
[mskC, mskS, mskW, mskCE, mskCW] = calcZonalMasks();

% Constant parameters
z_ref = -1000; % reference level 1000m
k_ref1= find(mygrid.RC>z_ref,1,'last');
k_ref2 = k_ref1+1; % 
% interp fraction btwn cell centers for reference velocity
dk = (z_ref-mygrid.RC(k_ref1))/(mygrid.RC(k_ref2)-mygrid.RC(k_ref1));  
% interp fraction between last cell face and 1000m ... 
% basically, how much of next cell to include in accumulated transport
% because when accumulating and multiply by dRF, already grab transport in k_ref1
dkf = (z_ref-mygrid.RF(k_ref1+1))/(mygrid.RF(k_ref2+1)-mygrid.RF(k_ref1+1));
dz = mygrid.DRC(k_ref2)*dk; 
f = 2*7.27e-5*sind(-34);  % [1/s]
rho_0 = 1029; %[kg/m^3] 
g = 9.81; %[m/s^2]
Lx= nansum(abs(mskW(:,:,1)).*mygrid.DYG + abs(mskS(:,:,1)).*mygrid.DXG); %[m]
dxg = mk3D(mygrid.DXG,mskC); % [m]
dyg = mk3D(mygrid.DYG,mskC); % [m]

% Check: Compute bottom point for each boundary
%    if bottom is smaller than reference level, have problems
k_bottom=0;
for k=1:Nr
    if nansum(nansum(mskCE(:,:,k)))~=0 && nansum(nansum(mskCW(:,:,k)))~=0
        k_bottom=k_bottom+1;
    end
end
if k_ref1>=k_bottom 
    error('Check E/W points, reference depth is below bathymetry level i.e. k_ref1<k_bottom'); 
end

dxc = convert2array(mk3D(mygrid.DXC,mskC)); dxc(isnan(dxc))=0;
dyc = convert2array(mk3D(mygrid.DYC,mskC)); dyc(isnan(dyc))=0;

% Convert to array for speed
mskCE=convert2array(mskCE); mskCE(isnan(mskCE))=0;
mskCW=convert2array(mskCW); mskCW(isnan(mskCW))=0;
mskC=convert2array(mskC);   mskC(isnan(mskC))=0;
mskW=convert2array(mskW);   mskW(isnan(mskW))=0;
mskS=convert2array(mskS);   mskS(isnan(mskS))=0;
dxg=convert2array(dxg);     dxg(isnan(dxg))=0;
dyg=convert2array(dyg);     dyg(isnan(dyg))=0;


for n = 1:Nt

    % Read in monthly values from file
    rho = read_nctiles([read_dir 'RHOAnoma/RHOAnoma'],'RHOAnoma',n);
    uvel = read_nctiles([read_dir 'UVELMASS/UVELMASS'],'UVELMASS',n);
    vvel = read_nctiles([read_dir 'VVELMASS/VVELMASS'],'VVELMASS',n);

    % Mask out values and convert to array for speed 
    % omit rho, since need to take gradient, via gcmfaces function
    uvel = convert2array(uvel); uvel(isnan(uvel))=0;
    vvel = convert2array(vvel); vvel(isnan(vvel))=0;

    % Compute reference velocity: only computing average over length 
    uvel_interp = uvel(:,:,k_ref1) + dk*(uvel(:,:,k_ref2)-uvel(:,:,k_ref1));
    vvel_interp = vvel(:,:,k_ref1) + dk*(vvel(:,:,k_ref2)-vvel(:,:,k_ref1));
    mskS_interp = mskS(:,:,k_ref1) + dk*(mskS(:,:,k_ref2)-mskS(:,:,k_ref1));
    mskW_interp = mskW(:,:,k_ref1) + dk*(mskW(:,:,k_ref2)-mskW(:,:,k_ref1));
    trsp_per_depth_ref(n) =nansum(nansum(vvel_interp.*mskS_interp.*dxg(:,:,k_ref1),1),2) + ...
                           nansum(nansum(uvel_interp.*mskW_interp.*dyg(:,:,k_ref1),1),2); %[m^2/s]
    trsp_per_depth_ref(n) = 10^-6*trsp_per_depth_ref(n); % [Sv/m]
    trsp_geo_ref(n) = trsp_per_depth_ref(n)*(nansum(mygrid.DRF(1:k_ref1))+dkf*mygrid.DRF(k_ref2)); %[Sv]
    vel_ref(n)      = 10^6 * trsp_per_depth_ref(n) / Lx; %[m/s]

    % Get zonal density derivative and accumulate from ref depth to surface 
    drho_dx_delz = 0*mskC;
    drho_dy_delz = 0*mskC;
    for k=1:k_ref2
        [tmpx,tmpy] = calc_T_grad( rho(:,:,k), 0 );
        drho_dx_delz(:,:,k) = convert2array(tmpx).*abs(mskS(:,:,k)).*mygrid.DRF(k); % [kg/m^3]
        drho_dy_delz(:,:,k) = convert2array(tmpy).*abs(mskW(:,:,k)).*mygrid.DRF(k); % [kg/m^3]
    end

    % Zero below k_ref2, and make partial cell to get 1000m reference level
    %drho_dx(:,:,k_ref2+1:Nr) = zeros(size(drho_dx(:,:,k_ref2+1:Nr)));
    drho_dx_delz(:,:,k_ref2) = dkf*drho_dx_delz(:,:,k_ref2); 
    drho_dy_delz(:,:,k_ref2) = dkf*drho_dy_delz(:,:,k_ref2);
    tmp_trsp_x = flipdim(cumsum(flipdim(drho_dx_delz,3),3),3);
    tmp_trsp_y = flipdim(cumsum(flipdim(drho_dy_delz,3),3),3);

    % Integrate zonally
    % Note: derivative lives on velocity (W/S) points
    trsp_per_depth_rel(:,n) = -10^-6 * g/f/rho_0 * ...
                            squeeze(nansum(nansum(...
                            tmp_trsp_y.*abs(mskW).*dyc + tmp_trsp_x.*abs(mskS).*dxc ,1),2));

    % Compute northward geostrophic transport
    % accumulate to amoc depth 
    trsp_geo_rel(n) = sum(trsp_per_depth_rel(1:k_ref2,n).*mygrid.DRF(1:k_ref2)); % [Sv]

    % Compute average velocity to compare to (Dong et al, 2014)
    vel_rel(:,n) = 10^6 * trsp_per_depth_rel(:,n) / Lx;

    % Finally, geostrophic transport is relative+reference
    trsp_geo(n) = trsp_geo_rel(n)+trsp_geo_ref(n); % [Sv]

    if mod(n,10)==0; fprintf('## Thermal wind flow: %d / %d \n',n,Nt); end
end

% Save it all 
save([mat_dir 'thermalWindFlow.mat'],'trsp_per_depth_rel','trsp_per_depth_ref',...
                             'trsp_geo_rel','trsp_geo_ref','trsp_geo',...
                             'vel_ref','vel_rel','Lx');
% --- Verification Plots
% Just to make sure things are right ... 
% Plot the zonal density gradient and -g/f integral( drho/dx ) at 34S
% Do this for the last time step
mk = ones(1,50);

[drhodx_depth,~,latX,~,latR] = grabAtlLatBand_uv(-34,...
                            drho_dx_delz,drho_dy_delz,abs(mskS(:,:,1)),abs(mskW(:,:,1)),mk);
rel_trsp_per_depth = -g/f/rho_0 * 10^-6 * flipdim(cumsum(flipdim(drhodx_depth,1),1),1);

figureW;
contourf(latX,latR,drhodx_depth)
    title('$\frac{\partial \rho}{\partial z}\delta z$ at 34S, avg over Dec. 2011')
    niceColorbar;
    savePlot('figures/drhodx_samoc','pdf');

figureW;
contourf(latX,latR,rel_trsp_per_depth)
    title('- 10$^{-6}$ g/f/$\rho_0$ $\int_{z_0}^z \frac{\partial \rho}{\partial z}dz$, avg over Dec. 2011')
    niceColorbar;
    savePlot('figures/relGeoTrspVsDepth_samoc','pdf');
end
