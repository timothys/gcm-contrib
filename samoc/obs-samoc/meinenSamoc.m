function [] = meinenSamoc(  )
% Compute AMOC based on data from Meinen et al ... 
% 

%% Load temperature data
sites={'A','B','C','D'};
datTypes = {'Temperature','Salinity'};
% This will be cell array of data structures with each field
TS = cell(length(sites),length(datTypes));

for i = 1:length(sites)
    for j = 1:length(datTypes)
        
        fname = sprintf('time-series_data/SAM_IES_%s_%s_rows.asc.data',sites{i},datTypes{j});
       TS{i,j} = pullTSData( fname );
    end
end   

%% Load Velocity data
vel = pullVelData( 'time-series_data/SAM_vel_data.txt.data' );

%% Load Travel Time  data
trav = pullTravelTimeData( 'time-series_data/SAM_PIES_data.txt.data' );

keyboard


end

function [travDat] = pullTravelTimeData( fname )
fid = fopen(fname,'r');
d=fscanf(fid,'%f',[11,2429]);
fclose(fid);

yr=d(1,:);
mo=d(2,:);
day=d(3,:);
tau = cell(4,1);
p = cell(4,1);
for i =1:4
  tau{i}=d(4+(i-1)*2,:);
  p{i}=d(5+(i-1)*2,:);
end

travDat = struct('yr',yr,'mo',mo,'day',day,'tau',tau,'p',p);
end

function [velDat] = pullVelData( fname )

fid = fopen(fname,'r');
d=fscanf(fid,'%f',[5,321]);
fclose(fid);

yr=d(1,:);
mo=d(2,:);
day=d(3,:);
u=d(4,:);
v=d(5,:);

velDat = struct('yr',yr,'mo',mo,'day',day,'u',u,'v',v);
end


function [expDat] = pullTSData( fname )

%datePattern = '%d %d %d %d ';
%profPattern0 = '%f ';
%profPattern='';
%for n=1:168-31
%    profPattern = horzcat(profPattern,profPattern0);
%end

fid = fopen(fname,'r');
d = fscanf(fid, '%f',[137,inf]);
fclose(fid);

% Somehow, d is the data transposed ...
% Organized: pressure, p: col. vector
%	     time: row vector
%	     experiments: vertically by p, in columns by time
p = d(5:end,1);
yr = d(1,2:end);
mo = d(2,2:end);
day = d(3,2:end);
hr = d(4,2:end);
ts = d(5:end,2:end);

expDat = struct('p',p,'yr',yr,'mo',mo,'day',day,'hr',hr,'ts',ts);

end
