function [mskC,mskS,mskW,mskCE,mskCW,mskS_bdyE,mskW_bdyW,latMskC] = calcZonalMasks()
% Want a consistent method for computing the masks between 
% East and Western boundaries
%
% Note: all masks are for the interior, between lon(1) and lon(2) 
%       EXCEPT FOR mskW_bdyW and mskS_bdyE, which grab velocity 
%       west and east of lon(1) and lon(2), respectively.
%       Here: since the Western bdy is on face 5, only need western mask
%       "       " Eastern bdy is on face 1, only need southern mask        
% 
% ------------------------------------------------------

mygrid = establish_mygrid('llc90');

% Atlantic basin and prep fields
atlBasin = v4_basin('atlExt');

% Prep mask for E/W points
lon = [16.5 -50.5];
%lon = [10.1, -45];
%lon = [3 -45];

% Grab latitude masks, between lon boundaries
iy = find(mygrid.LATS==-34);
xCondE = abs(mygrid.XC-lon(1))<.5;
xCondW = abs(mygrid.XC-lon(2))<.5;
xCondW_u= abs(mygrid.XG-lon(2))<.5; % u velocity on cell face

latMskC = mygrid.LATS_MASKS(iy).mskCedge .* atlBasin;
latMskS = mygrid.LATS_MASKS(iy).mskSedge .* atlBasin;
latMskW = mygrid.LATS_MASKS(iy).mskWedge .* atlBasin;

% Get corresponding actual value on grid, different for u velocity vs C & V points
%   on western boundary, grabbing velocity points (for ref. velocity) on gcmface 5 
%   which has u/v velocities flipped, so need to grab the actual point where the u velocity acts
%   on that face
%   min/max is to get inner most point if 2 points are selected via xCond
xc=convert2gcmfaces(mygrid.XC);
xg=convert2gcmfaces(mygrid.XG);

lonE    = min(xc(logical(convert2gcmfaces(xCondE.*latMskC))));
lonW    = max(xc(logical(convert2gcmfaces(xCondW.*latMskC))));
lonW_u  = max(xg(logical(convert2gcmfaces(abs(xCondW_u.*latMskW)))));

% Check:
% Make sure only grab one point at boundaries:
%   a scalar is a 1x1 matrix in matlab ...
if length(size(lonE))>2, error('Logical statement grabbed >1 point on Eastern bdy ...\n'); end;
if length(size(lonW))>2, error('Logical statement grabbed >1 point on Western bdy ...\n'); end;
if length(size(lonW_u))>2, error('Logical statement grabbed >1 point on Western bdy ...\n'); end;

% --- Interior masks
mskC = latMskC .* (mygrid.XC<=lonE) .* (mygrid.XC>=lonW);
mskS = latMskS .* (mygrid.XC<=lonE) .* (mygrid.XC>=lonW);
mskW = latMskW .* (mygrid.XG<=lonE) .* (mygrid.XG>=lonW_u);

% tracer points
mskCE = mskC.*xCondE;    mskCW=mskC.*xCondW;
mskC = mk3D(mskC,mygrid.mskC).*mygrid.mskC;
mskCE = mk3D(mskCE,mskC).*mskC;  mskCW=mk3D(mskCW,mskC).*mskC;

% u velocity points
mskW = mk3D(mskW,mygrid.mskW).*mygrid.mskW;
 
% v velocity points
mskS = mk3D(mskS,mygrid.mskS).*mygrid.mskS;


% --- Boundary flow masks
mskS_bdyE = latMskS .* (mygrid.XC>lonE);
mskW_bdyW = latMskW  .* (mygrid.XG<lonW_u);

% u velocity points
mskW_bdyW=mk3D(mskW_bdyW,mygrid.mskW).*mygrid.mskW;
 
% v velocity points
mskS_bdyE = mk3D(mskS_bdyE,mygrid.mskS).*mygrid.mskS;
end
