function [] = plotEccoObsSamoc()
% Grab Ekman, geostrophic, and boundary flows 
% to compute samoc as if from observing system

% First load the ECCOv4r2 velocity diagnosed amoc
loadDir = 'mat/eccov4r2/state/';
cd ..
[ecco_amoc] = calcAMOC(-34,'eulerian',loadDir);
cd obs-samoc

load('mat/ekFlow.mat','trsp_ek');
load('mat/geoFlow.mat','trsp_geo','trsp_geo_sur','trsp_geo_hyd');
load('mat/thermalWindFlow_bdy.mat','trsp_geo','trsp_geo_rel','trsp_geo_ref');
%load('mat/thermalWindFlow.mat','trsp_geo','trsp_geo_rel','trsp_geo_ref');
load('mat/bdyFlow.mat','trsp_bdy','trsp_bdyE','trsp_bdyW');

% Add it all together
amoc = trsp_ek+trsp_geo+trsp_bdy;

% Print the mean value
ecco_mean   = nanmean(ecco_amoc);
amoc_mean   = nanmean(amoc);
ek_mean     = nanmean(trsp_ek);
geo_mean    = nanmean(trsp_geo);
rel_mean    = nanmean(trsp_geo_rel);
ref_mean    = nanmean(trsp_geo_ref);
sur_mean    = nanmean(trsp_geo_sur);
hyd_mean    = nanmean(trsp_geo_hyd);
bdw_mean    = nanmean(trsp_bdyW);
bde_mean    = nanmean(trsp_bdyE);

%bdy_mean     = nanmean(trsp_bdy);
fprintf(' --- ECCO SAMOC mean: %1.2f\n',ecco_mean);
fprintf(' --- Obs SAMOC mean: %1.2f\n',amoc_mean);
fprintf(' --- Ekman SAMOC mean: %1.2f\n',ek_mean);
fprintf(' --- Total geostrophic SAMOC mean: %1.2f\n',geo_mean);
fprintf(' --- Surface geostrophic SAMOC mean: %1.2f\n',sur_mean);
fprintf(' --- Hydrostatic geostrophic SAMOC mean: %1.2f\n',hyd_mean);
fprintf(' --- Relative geostrophic SAMOC mean: %1.2f\n',rel_mean);
fprintf(' --- Reference geostrophic SAMOC mean: %1.2f\n',ref_mean);
fprintf(' --- Western boundary SAMOC mean: %1.2f\n',bdw_mean);
fprintf(' --- Eastern boundary SAMOC mean: %1.2f\n',bde_mean);
fprintf('\n');

% Compute climatologies...
Nyrs=20;
ecco_clim = calcClimatology(ecco_amoc-ecco_mean,Nyrs);
amoc_clim = calcClimatology(amoc-amoc_mean,Nyrs);
ek_clim = calcClimatology(trsp_ek-ek_mean,Nyrs);
geo_clim = calcClimatology(trsp_geo-geo_mean,Nyrs);
sur_clim = calcClimatology(trsp_geo_sur-sur_mean,Nyrs);
hyd_clim = calcClimatology(trsp_geo_hyd-hyd_mean,Nyrs);
rel_clim = calcClimatology(trsp_geo_rel-rel_mean,Nyrs);
ref_clim = calcClimatology(trsp_geo_ref-ref_mean,Nyrs);
bdw_clim = calcClimatology(trsp_bdyW-bdw_mean,Nyrs);
bde_clim = calcClimatology(trsp_bdyE-bde_mean,Nyrs);

clim_plot = [ecco_clim; amoc_clim; ek_clim; geo_clim; ...
             bdw_clim; bde_clim];
legendStr={'u/v ECCOv4r2','ECCO Obs. total','Ekman','Geostrophic',...
            'West Bdy','East Bdy'};

% Plot the climatologies
cmap = cbrewer2('Dark2',size(clim_plot,1));
t = 1:12;
fs = 22;

figureW; 
ax = plot(t, clim_plot);
for n=1:length(ax)
    set(ax(n),{'color'},num2cell(cmap(n,:),2));
end
xlbl_clim;
ylabel('Sv');
grid on


legend(legendStr,'location','eastoutside','fontsize',fs);

saveWidePlot('figures/samoc_ecco_clim.pdf','pdf');
keyboard
end
