function [latR,latX,vel_geo_clim,rho_east,rho_west,drho_dz,theta_mean_1000,uvel_mean_1000,vvel_mean_1000] = calcECCODensityClim(  )
% Want to show climatology of density at East and West bdy
% of SAMOC domain.

mygrid=establish_mygrid('llc90');

% Climatology directories
rho_dir='../../../release2_climatology/nctiles_climatology/RHOAnoma/';
theta_dir='../../../release2_climatology/nctiles_climatology/THETA/';
uvel_dir='../../../release2_climatology/nctiles_climatology/UVELMASS/';
vvel_dir='../../../release2_climatology/nctiles_climatology/VVELMASS/';

% Load up geostrophic velocities diagnosed from eccov4r2
load('mat/thermalWindFlow_bdy.mat','vel_rel','vel_ref');

mat_file = 'mat/drhodz.mat';

fig_dir='figures/shenfu-comp/';
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;

%% Depth mask
Nr=length(mygrid.RC);

%% 2D masks at 34S consistent with geostrophic computation
[mskC,~,~,mskCE,mskCW,~,~,latMskC] = calcZonalMasks;
k_bottom = 0;
for k = 1:Nr
    if nansum(nansum(mskCE(:,:,k)))~=0 && nansum(nansum(mskCW(:,:,k)))~=0
        k_bottom=k_bottom+1;
    end
end

% Stuff for interpolating to exactly 1000m for theta mean
z_ref = -1000; % reference level 1000m
k_ref1= find(mygrid.RC>z_ref,1,'last');
k_ref2 = k_ref1+1; % 
% interp fraction btwn cell centers for reference velocity
dk = (z_ref-mygrid.RC(k_ref1))/(mygrid.RC(k_ref2)-mygrid.RC(k_ref1));  
% interp fraction between last cell face and 1000m ... 
% basically, how much of next cell to include in accumulated transport
% because when accumulating and multiply by dRF, already grab transport in k_ref1
dkf = (z_ref-mygrid.RF(k_ref1+1))/(mygrid.RF(k_ref2+1)-mygrid.RF(k_ref1+1));
dz = mygrid.DRC(k_ref2)*dk; 


if ~exist(mat_file,'file')

    rho_clim=read_nctiles([rho_dir 'RHOAnoma']);
    theta_clim=read_nctiles([theta_dir 'THETA']);
    uvel_clim=read_nctiles([uvel_dir 'UVELMASS']);
    vvel_clim=read_nctiles([vvel_dir 'VVELMASS']);

    samoc_rho=cell(12,1);
    drho_dz=cell(12,1);
    rho_west = zeros(Nr,12);
    rho_east = zeros(Nr,12);
    
    %% First, compute density gradient. 
    %  1. keep temporal mean inside of each point so we don't have
    %     density gradient which flips signs 
    %  2. Compute negative of gradient to compare to (Dong et al,2014)
    %     negative vertical gradient => stability.
    for i=1:12
      [samoc_rho{i},latX,latR] = grabAtlLatBand_c(-34,rho_clim(:,:,:,i));
      Nx=size(samoc_rho{i},2);
      drho_dz{i} = samoc_rho{i}(1:end-1,:)-samoc_rho{i}(2:end,:);
      drho_dz{i} = drho_dz{i}./repmat(mygrid.DRC(2:end),[1 Nx]);
      drho_dz{i} = -drho_dz{i};
    end
    
    %% Compute the temporal mean
    theta_mean = nanmean(theta_clim,4);
    rho_mean = nanmean(rho_clim,4);
    uvel_mean = nanmean(uvel_clim,4);
    vvel_mean = nanmean(vvel_clim,4);

    % Interpolate theta_mean for theta at 1000m 
    theta_mean_1000 = theta_mean(:,:,k_ref1) + dk*(theta_mean(:,:,k_ref2)-theta_mean(:,:,k_ref1));
    uvel_mean_1000 = uvel_mean(:,:,k_ref1) + dk*(uvel_mean(:,:,k_ref2)-uvel_mean(:,:,k_ref1));
    vvel_mean_1000 = vvel_mean(:,:,k_ref1) + dk*(vvel_mean(:,:,k_ref2)-vvel_mean(:,:,k_ref1));
    
    %% Compute seasonal variability about the temporal mean 
    %  at boundaries for density
    rho_clim = rho_clim - repmat(rho_mean,[1 1 1 12]);
    rho_clim = convert2array(rho_clim); 
    mskCW = convert2array(mskCW); mskCW(isnan(mskCW))=0;
    mskCE = convert2array(mskCE); mskCE(isnan(mskCE))=0;
    for i=1:12
        rho_tmp = rho_clim(:,:,:,i);
        rho_west(1:k_bottom,i) = rho_tmp(logical(mskCW(:,:,1:k_bottom)));
        rho_east(1:k_bottom,i) = rho_tmp(logical(mskCE(:,:,1:k_bottom)));
    end
    
    %% Compute seasonal cycle of 
    %  zonal avg of geo velocity
    vel_geo = vel_rel + repmat(vel_ref,[Nr 1]);
    vel_geo_mean = nanmean(vel_geo,2);
    vel_geo_clim = calcClimatology(vel_geo - repmat(vel_geo_mean,[1 size(vel_geo,2)]),20);
    save(mat_file,'samoc_rho','drho_dz','latX','latR','Nx',...
                  'theta_mean','theta_mean_1000',...
                  'uvel_mean','uvel_mean_1000',...
                  'vvel_mean','vvel_mean_1000',...
                  'rho_west','rho_east',...
                  'vel_geo','vel_geo_mean','vel_geo_clim');
else
    load(mat_file,'samoc_rho','drho_dz','latX','latR','Nx',...
                  'theta_mean','theta_mean_1000',...
                  'uvel_mean','uvel_mean_1000',...
                  'vvel_mean','vvel_mean_1000',...
                  'rho_west','rho_east',...
                  'vel_geo','vel_geo_mean','vel_geo_clim');
end

end
