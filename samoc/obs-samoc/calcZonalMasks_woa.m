function [lat_ind,ind_e,ind_w]= calcZonalMasks_argo 
% Consistently define east/west boundaries and -34S for argo data
% Damn this is much easier than for MITgcm fields ...

ind_w = 130; % -50.5 degrees longitude
ind_e = 198; % 17.5 degrees longitude
lat_ind = 42; %-34.5 degrees latitude
end
