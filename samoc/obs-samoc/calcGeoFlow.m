function [ trsp_geo ] = calcGeoFlow()
% Compute geostrophic interior component of SAMOC to compare to Argo inferred 
% geostrophic transport in Dong et al. (2015) 
%
%   Geostrophy dictates: 
%       where \phi= p/rho_0
%
%       v_g = 1/rho_0 1/f dp/dx = 1/f d \phi/dx
%
%   The zonal geostrophic transport per depth level 
%       in units [m^2/s]
%
%       trsp_per_depth = int_W^E ( v_g dx ) = 1/f (\phi_E - \phi_W)
%
%   So the geostrophic component of the amoc is
%
%       trsp_geo = int_z*^surf( trsp_per_depth dz )
%
%   where z* is the depth level amoc is computed at
%
%  Outputs: 
%
%       trsp_geo : zonally integrated volume transport [Sv] at each
%                      depth level (1:Nrf) and time step (1992-2011,
%                      monthly)
% -------------------------------------------------------------------------

mygrid=establish_mygrid('llc90');
read_dir = '../../../release2/nctiles_monthly/';
fig_dir = 'figures/'; if ~exist(fig_dir,'dir'), mkdir(fig_dir), end;
mat_dir = ['mat/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir), end;

Nr = length(mygrid.RC);
Nt = 240; 
phi_tot_e = zeros(Nr,Nt);           % [m^2/s^2]
phi_tot_w = zeros(Nr,Nt);           % [m^2/s^2]
trsp_per_depth_hyd = zeros(Nr,Nt);  % [Sv/m]
trsp_per_depth_sur = zeros(Nr,Nt);  % [Sv/m]
trsp_per_depth = zeros(Nr,Nt);      % [Sv/m]
trsp_geo_hyd = zeros(1,Nt);         % [Sv]
trsp_geo_sur = zeros(1,Nt);         % [Sv]
trsp_geo = zeros(1,Nt);             % [Sv]

% Compute masks between E/W boundary points
% this gives latitude band from E/W points 
[mskC, mskS, mskW, mskCE, mskCW] = calcZonalMasks();

% Constant parameters
z_ref = -1000; % reference level 1000m
k_ref1= find(mygrid.RC>z_ref,1,'last');
k_ref2 = k_ref1+1; % 
% interp fraction btwn cell centers for reference velocity
dk = (z_ref-mygrid.RC(k_ref1))/(mygrid.RC(k_ref2)-mygrid.RC(k_ref1));  
% interp fraction between last cell face and 1000m ... 
% basically, how much of next cell to include in accumulated transport
% because when accumulating and multiply by dRF, already grab transport in k_ref1
dkf = (z_ref-mygrid.RF(k_ref1+1))/(mygrid.RF(k_ref2+1)-mygrid.RF(k_ref1+1));
dz = mygrid.DRC(k_ref2)*dk; 
f = 2*7.27e-5*sind(-34);  % [1/s]
rhoc = 1029; %[kg/m^3] 
g = 9.81; %[m/s^2]
Lx= nansum(abs(mskW(:,:,1)).*mygrid.DYG + abs(mskS(:,:,1)).*mygrid.DXG); %[m]
dxg = mk3D(mygrid.DXG,mskC); % [m]
dyg = mk3D(mygrid.DYG,mskC); % [m]

% Check: Compute bottom point for each boundary
%    if bottom is smaller than reference level, have problems
k_bottom=0;
for k=1:Nr
    if nansum(nansum(mskCE(:,:,k)))~=0 && nansum(nansum(mskCW(:,:,k)))~=0
        k_bottom=k_bottom+1;
    end
end
if k_ref1>=k_bottom 
    error('Check E/W points, reference depth is below bathymetry level i.e. k_ref1<k_bottom'); 
end

% Convert to array for speed
mskCE=convert2array(mskCE); mskCE(isnan(mskCE))=0;
mskCW=convert2array(mskCW); mskCW(isnan(mskCW))=0;
mskC=convert2array(mskC);   mskC(isnan(mskC))=0;
mskW=convert2array(mskW);   mskW(isnan(mskW))=0;
mskS=convert2array(mskS);   mskS(isnan(mskS))=0;
dxg=convert2array(dxg);     dxg(isnan(dxg))=0;
dyg=convert2array(dyg);     dyg(isnan(dyg))=0;

for n = 1:Nt

    % Read in monthly values from file
    phihyd = read_nctiles([read_dir 'PHIHYD/PHIHYD'],'PHIHYD',n);
    eta = read_nctiles([read_dir 'ETAN/ETAN'],'ETAN',n);

    % Mask out values and convert to array for speed 
    phihyd = convert2array(phihyd).*mskC;
    eta = convert2array(eta).*mskC(:,:,1); 
    phihyd(isnan(phihyd))=0; eta(isnan(eta))=0; 

    % Get hydrostatic geopotential anomaly 
    phi_he = phihyd(logical(mskCE(:,:,1:k_bottom))); % [m^2/s^2]  
    phi_hw = phihyd(logical(mskCW(:,:,1:k_bottom))); % [m^2/s^2]
    
    % Compute surface geopotential anomaly 
    phi_se = g*eta(logical(mskCE(:,:,1))); % [m^2/s^2]
    phi_sw = g*eta(logical(mskCW(:,:,1))); % [m^2/s^2]

    % Add pressure together
    phi_tot_e(1:k_bottom,n) = phi_he + phi_se; % [m^2/s^2]
    phi_tot_w(1:k_bottom,n) = phi_hw + phi_sw; % [m^2/s^2]

    % Compute transport in sverdrups
    trsp_per_depth_hyd(1:k_bottom,n) = 10^-6 * 1/f * (phi_he - phi_hw);
    trsp_per_depth_sur(:,n) = 10^-6 * 1/f * (repmat(phi_se,[Nr 1]) - repmat(phi_sw,[Nr 1]));
    trsp_per_depth(:,n) = 10^-6 * 1/f * (phi_tot_e(:,n) - phi_tot_w(:,n)); %[Sv / m] 

    % Compute northward geostrophic transport
    % accumulate to amoc depth 
    trsp_geo_hyd(n) = sum(trsp_per_depth_hyd(1:k_ref1,n).*mygrid.DRF(1:k_ref1)); % [Sv]
    trsp_geo_sur(n) = sum(trsp_per_depth_sur(1:k_ref1,n).*mygrid.DRF(1:k_ref1)); % [Sv]
    trsp_geo(n)     = sum(trsp_per_depth(1:k_ref1,n).*mygrid.DRF(1:k_ref1)); % [Sv]

    % add on fraction of cell to get interpolant at 1000m
    trsp_geo_hyd(n) = trsp_geo_hyd(n) + ...
                      dkf*trsp_per_depth_hyd(k_ref2,n).*mygrid.DRF(k_ref2);
    trsp_geo_sur(n) = trsp_geo_sur(n) + ...
                      dkf*trsp_per_depth_sur(k_ref2,n).*mygrid.DRF(k_ref2);
    trsp_geo(n) =     trsp_geo(n) + ...
                      dkf*trsp_per_depth(k_ref2,n).*mygrid.DRF(k_ref2);

    if mod(n,10)==0; fprintf('## Geostrophic flow: %d / %d \n',n,Nt); end
end

% Save it all 
save([mat_dir 'geoFlow.mat'],'trsp_geo','phi_tot_e','phi_tot_w',...
                             'trsp_geo_hyd','trsp_geo_sur');
end
