#! /usr/bin/python

# Goal is to 
#  - read in AX18 data 
#  - grab T,S,pressure,depth,latitude, longitude data
#  - extend to full water column as in Thacker
#  - compute density 
#  - compute uncertainty in observations(?)
#  - compute meridional overturning circulation
#  - save to file

import os 
import math

def readXBT( fname ):
	if not os.path.isfile(fname): return

	fil = open( fname , 'r' )
	
	allDat=fil.readlines()
	head=allDat.[0:12]
	dat=allDat[12:]

	# Grab the header info
	cast = float(head[2][0:4])
	lat = float(head[2][5:12])
	lon = float(head[2][13:20])
	mo  = int(head[2][31:32])
	day = int(head[2][34:35])
	yr  = int(head[2][37:38])

	# Now fill in the data
	N = len(dat)
	p = zeros(N)
	T = zeros(N)
	Th= zeros(N)
	S = zeros(N)
	ht= zeros(N)
	d = zeros(N)

	for i in range(0,N):
		p[i] = float(dat[i][0:8])
		T[i] = float(dat[i][9:17])
		Th[i]= float(dat[i][18:26])
		S[i] = float(dat[i][27:35])
		ht[i]= float(dat[i][36:44])
		d[i] = float(dat[i][45:53])


		

	return 
