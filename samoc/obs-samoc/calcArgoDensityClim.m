function [lat,lon,z,msk,trsp_geo,vel_rel,rho_anom,theta_mean,drhodz,rho_mean] = calcArgoDensityClim()
% Compute Argo temperature, salinity, density, and geostrophic trsp fields 
% with gridded temperature and salinity Argo data from Roemmich and Gilson, 2009
% http://sio-argo.ucsd.edu/RG_Climatology.html, in argo-data/
%
% --------------------------------------------------------------------

% --- Load argo theta and salt
mat_dir = 'mat/argo-data'; if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_file = sprintf('%s/theta_and_salt_clim.mat',mat_dir);

if ~exist(mat_file,'file'),
    [lat,lon,pressure,msk,theta_clim,salt_clim,rho_clim] = calcArgoClimatologies(mat_file);
else
    fprintf('Loading %s ...\n',mat_file);
    load(mat_file,'lat','lon','pressure','msk',...
                  'theta_clim','salt_clim','rho_clim');
end

% --- Compute depth from pressure at 34S and make z a lat/lon map
z = calcDepth(lat,lon,pressure);


% --- Compute mean and anomaly
[theta_mean, theta_anom] = calcMeanAndAnom(theta_clim);
[salt_mean, salt_anom] = calcMeanAndAnom(salt_clim);
[rho_mean, rho_anom] = calcMeanAndAnom(rho_clim);
rho_mean = rho_mean - 1029;

% --- Compute density shear
drhodz = calc_minus_drhodz(z,rho_clim);

% --- Compute geostrophic transport
[trsp_geo,trsp_geo_rel,trsp_geo_ref,vel_rel] = calcGeoTrsp(z,rho_clim);
[vel_rel_mean,vel_rel_anom] = calcMeanAndAnom(vel_rel);
vel_rel=vel_rel_anom;



end

% --------------------------------------------------------------------

function [trsp_geo,trsp_geo_rel,trsp_geo_ref,vel_rel] = calcGeoTrsp(z,rho)

mat_file = 'mat/argo-data/geo_trsp.mat';
if ~exist(mat_file,'file')

    % fixed parameters
    f = 2*7.27e-5*sind(-34);  % [1/s]
    rho_0 = 1029; %[kg/m^3] 
    g = 9.81; %[m/s^2]
    % for now, just go to fixed reference, worry about interpolation later
    p_ind = 44;
    Lx = 6.18e6; % [m] taken from ECCO based computation

    % Grab boundary densities
    rho = rho - rho_0;
    [lat_ind,ind_e,ind_w] = calcZonalMasks_argo();
    rho_e = squeeze(rho(ind_e,lat_ind,:,:));
    rho_w = squeeze(rho(ind_w,lat_ind,:,:));
    
    % Compute vertical spacing
    zf = zeros(length(squeeze(z(1,lat_ind,:)))+1,1);
    zf(1) = 0;
    zf(2:end-1) = squeeze(z(1,lat_ind,1:end-1)+z(1,lat_ind,2:end))/2;
    
    dz = zf(2:end) - zf(1:end-1);
    dz(end) = dz(end-1); % this doesn't matter for my calc ... get back to it later
    
    
    trsp_per_depth_rel = -10^-6 * g/f/rho_0 * (rho_e - rho_w); %[Sv/m]
    trsp_per_depth_rel(1:p_ind,:) = flipdim(cumsum(flipdim( ...
                        trsp_per_depth_rel(1:p_ind,:) .* ...
                        repmat(dz(1:p_ind), [1 size(rho_e,2)]),...
                        1),1),1);
    
    vel_rel = 10^6 * trsp_per_depth_rel / Lx; % [m/s]
    vel_ref = 0*vel_rel; % need to get this from parking depth
    
    trsp_geo_rel = sum(trsp_per_depth_rel(1:p_ind,:).*repmat(dz(1:p_ind), [1 size(rho_e,2)]),1); %[Sv]
    
    % Need to finish this ...
    trsp_geo_ref = 0*trsp_geo_rel;

    % Compute geostrophic transport
    trsp_geo = trsp_geo_rel + trsp_geo_ref;
    
    % save it up
    save(mat_file,'trsp_geo','trsp_geo_rel','trsp_geo_ref',...
                    'Lx','vel_rel','vel_ref','trsp_per_depth_rel');
    fprintf('Saved %s ...\n',mat_file);

else
    fprintf('Loading %s ...\n',mat_file);
    load(mat_file,'trsp_geo','trsp_geo_rel','trsp_geo_ref','vel_rel');
end


end

% --------------------------------------------------------------------

function [drhodz] = calc_minus_drhodz(z,rho)
% Compute negative vertical density gradient 

Nlat = size(rho,1);
Nlon = size(rho,2);
Nz = size(rho,3);
Nt = size(rho,4);
drhodz = zeros(Nlat,Nlon,Nz-1,Nt);


for n=1:Nt
    for k=1:Nz-1
        dz = z(:,:,k)-z(:,:,k+1);
        drho = squeeze(rho(:,:,k,n) - rho(:,:,k+1,n));
        drhodz(:,:,k,n) = drho ./ dz;
    end
end

drhodz = -drhodz;

end

% --------------------------------------------------------------------

function [rho] = calcDensity(theta,salt,pressure)
% Compute density using MITgcm/utils/matlab/densjmd95.m
% Local function that works on climatology ... 
% Assume inputs are a climatology
% Assume (units in Argo netcdf format):
%
%   theta as in-situ temperature in units degC
%   salinity as practical salinity scale 78
%   pressure in dBar
%
% 1. convert to conservative temperature and absolute salinity
% 2. compute in-situ density

%mat_file = 'mat/argo-data/rho_clim_from_argo_and_jmd95.mat';
%if ~exist(mat_file,'file')

    rho = zeros(size(theta));
    
    Np = length(pressure);
    Nt = size(theta,4);
    Nlat=size(theta,2);
    Nlon=size(theta,1);

    for p = 1:Np
        for n = 1:Nt
            tt = squeeze(theta(:,:,p,n));
            ss = squeeze( salt(:,:,p,n));
            pp = repmat(pressure(p),[Nlon Nlat]);
            rho(:,:,p,n) = densjmd95(ss,tt,pp);
        end
        fprintf(' --- Computing rho: %d / %d ---\n',p,Np);
    end

    %rho_clim = 1029 + 1029*( ...
    %            -alpha_t*(theta_clim - t_0) + ...
    %             beta_s *(salt_clim  - s_0));
    %rho_clim = densjmd95(salt_clim,theta_clim,pressure);
    %rho_clim = densmdjwf(salt_clim,theta_clim,pressure);
    
    %save(mat_file,'rho_clim');
    %fprintf('Saved %s ...\n',mat_file);
%else
%    fprintf('Loading %s ...\n',mat_file);
%    load(mat_file,'rho_clim');
%end
    
end

% --------------------------------------------------------------------

function [z] = calcDepth(lat,lon,pressure)

z = zeros(1,length(lat),length(pressure));

for l = 1:length(lat)
    z(1,l,:) = gsw_z_from_p(pressure,lat(l));
end
z = repmat(z,[length(lon) 1 1]);

end

% --------------------------------------------------------------------

function [fld_mean,fld_anom] = calcMeanAndAnom(fld)
% Assumes time is on 4th dimension...

sz = size(fld);
fld_mean = nanmean(fld,length(sz));
fld_anom = fld - fld_mean;

end


% --------------------------------------------------------------------

function [lat,lon,pressure,msk,theta_clim,salt_clim,rho_clim] = calcArgoClimatologies(mat_file)

[lat,lon,pressure,msk] = loadArgoMetaData;
[theta,salt] = loadArgoThetaAndSalt;


% shift longitude to be in -180->+180 for nice computations ...
% this is not required for anything I'm using anymore, but is more intuitive ...
lon0 = 180;
lon_shift = max(round(lon)) - lon0; 
lon(lon>lon0) = lon(lon>lon0) - 360;
lon = circshift(lon,lon_shift);
theta = circshift(theta,[lon_shift 0 0 0]);
salt = circshift(salt,[lon_shift 0 0 0]);
msk = circshift(msk,[lon_shift 0 0 0]);

% Compute density
rho = calcDensity(theta,salt,pressure);

% Compute climatologies
Nmo = size(theta,4);
Nyrs = Nmo/12;
theta_clim = calcClimatology(theta,Nyrs);
salt_clim = calcClimatology(salt,Nyrs);
rho_clim = calcClimatology(rho,Nyrs);


save(mat_file,'lat','lon','pressure','msk','theta_clim','salt_clim','rho_clim');
fprintf('Saved %s ...\n',mat_file);

end

% --------------------------------------------------------------------

function [theta,salt] = loadArgoThetaAndSalt()


% --- Load data and compute 2004-2011

% WARNING: theta_anom and salt_anom are 3.8 GB each, mean vals are 24 MB  
theta_mean = ncread('argo-data/RG_ArgoClim_Temperature_2017.nc','ARGO_TEMPERATURE_MEAN');
theta_anom = ncread('argo-data/RG_ArgoClim_Temperature_2017.nc','ARGO_TEMPERATURE_ANOMALY');

salt_mean = ncread('argo-data/RG_ArgoClim_Salinity_2017.nc','ARGO_SALINITY_MEAN');
salt_anom = ncread('argo-data/RG_ArgoClim_Salinity_2017.nc','ARGO_SALINITY_ANOMALY');

% anom data are monthly. Remove 2012-2016, keeping overlapping with ECCOv4r2
% Now anom fields are 2.3 GB
end_ind = (2013-2004)*12;
theta_anom = theta_anom(:,:,:,1:end_ind);
salt_anom = salt_anom(:,:,:,1:end_ind);

% compute full field as mean+anom
theta = theta_mean + theta_anom;
salt = salt_mean + salt_anom;

end

% --------------------------------------------------------------------

function [lat,lon,pressure,msk] = loadArgoMetaData()

lat = ncread('argo-data/RG_ArgoClim_Salinity_2017.nc','LATITUDE');
lon = ncread('argo-data/RG_ArgoClim_Salinity_2017.nc','LONGITUDE');
pressure = ncread('argo-data/RG_ArgoClim_Salinity_2017.nc','PRESSURE');

% Mask for bathymetry, 1 where data are, 0 blocked by bathy. make 0's->NaN's for plotting
msk = ncread('argo-data/RG_ArgoClim_Salinity_2017.nc','BATHYMETRY_MASK');
msk(msk==0)=NaN;

end


