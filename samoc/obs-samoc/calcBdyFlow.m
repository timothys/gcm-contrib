function [trsp_bdyE, trsp_bdyW] = calcBdyFlow()
% Compute observers perspective (compare Meinen et al 2013) of AMOC at 34S
% Here: computing the boundary flow component. 
% outputs
%   trsp_bdyE, trsp_bdyW : [ Nrf x Nt ] array
%                                  zonally integrated volume transport per
%                                  depth over time 
%
% -------------------------------------------------------------------------

mygrid = establish_mygrid('llc90');

%% Set up mask for SAMOC
read_dir = '../../../release2/nctiles_monthly/';
fig_dir = 'figures/'; if ~exist(fig_dir,'dir'), mkdir(fig_dir), end;
mat_dir = ['mat/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir), end;

Nr = length(mygrid.RC);
Nt = 240;
trsp_per_depth_bdyE = zeros(Nr,Nt);
trsp_per_depth_bdyW = zeros(Nr,Nt);
trsp_bdyE = zeros(1,Nt);
trsp_bdyW = zeros(1,Nt);
trsp_bdy  = zeros(1,Nt);

% Compute masks between E/W boundary points
% this gives latitude band from E/W points 
[~, ~, ~, ~, ~, mskS_bdyE, mskW_bdyW] = calcZonalMasks();

% Constant parameters
z_ref = -1000; % reference level 1000m
k_ref1= find(mygrid.RC>z_ref,1,'last');
k_ref2 = k_ref1+1; % 
% interp fraction btwn cell centers for reference velocity
dk = (z_ref-mygrid.RC(k_ref1))/(mygrid.RC(k_ref2)-mygrid.RC(k_ref1));  
% interp fraction between last cell face and 1000m ... 
% basically, how much of next cell to include in accumulated transport
% because when accumulating and multiply by dRF, already grab transport in k_ref1
dkf = (z_ref-mygrid.RF(k_ref1+1))/(mygrid.RF(k_ref2+1)-mygrid.RF(k_ref1+1));
dz = mygrid.DRC(k_ref2)*dk; 

dxg = mk3D(mygrid.DXG,mygrid.mskC); % [m]
dyg = mk3D(mygrid.DYG,mygrid.mskC); % [m]

% Convert to array for speed
mskS_bdyE=convert2array(mskS_bdyE); mskS_bdyE(isnan(mskS_bdyE))=0;
mskW_bdyW=convert2array(mskW_bdyW); mskW_bdyW(isnan(mskW_bdyW))=0;
dxg=convert2array(dxg);     dxg(isnan(dxg))=0;
dyg=convert2array(dyg);     dyg(isnan(dyg))=0;

for n = 1:Nt
    % Load u/v
    uvel = read_nctiles([read_dir 'UVELMASS/UVELMASS'], 'UVELMASS', n);
    vvel = read_nctiles([read_dir 'VVELMASS/VVELMASS'], 'VVELMASS', n);

    uvel = convert2array(uvel); uvel(isnan(uvel))=0;
    vvel = convert2array(vvel); vvel(isnan(vvel))=0;

    % Western boundary is given fully by uvel & mskW, since this is on face 5
    trsp_per_depth_bdyW(:,n) = 10^-6 * squeeze(nansum(nansum(mskW_bdyW.*uvel.*dyg,1),2)); % [Sv/m]

    % Eastern boundary is given fully by vvel & mskS, since this is on face 1
    trsp_per_depth_bdyE(:,n) = 10^-6 * squeeze(nansum(nansum(mskS_bdyE.*vvel.*dxg,1),2)); % [Sv/m]

    % Accumulate in the vertical and sum together
    trsp_bdyE(n) = nansum(trsp_per_depth_bdyE(:,n),1);
    trsp_bdyW(n) = nansum(trsp_per_depth_bdyW(:,n),1);

    trsp_bdy(n)  = trsp_bdyE(n)+trsp_bdyW(n);

    if mod(n,10)==0; fprintf('## Boundary flow: %d / %d \n',n,Nt); end
end

save([mat_dir 'bdyFlow.mat'],'trsp_bdyE','trsp_bdyW','trsp_bdy',...
                             'trsp_per_depth_bdyE','trsp_per_depth_bdyW');
end
