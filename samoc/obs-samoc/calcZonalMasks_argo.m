function [lat_ind,ind_e,ind_w]= calcZonalMasks_argo 
% Consistently define east/west boundaries and -34S for argo data
% Damn this is much easier than for MITgcm fields ...

ind_w = 130; % -50.5 degrees longitude
ind_e = 197; % 16.5 degrees longitude
lat_ind = 31; % -34degrees latitude

end
