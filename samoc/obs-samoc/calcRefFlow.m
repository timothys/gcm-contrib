function [ trVolZon_ref, v_ref ] = calcRefFlow()
% Compute reference for geostrophic component of SAMOC as in Meinen et al. 
%
%  Outputs: 
%
%       trVolZon_ref : zonally integrated volume transport [Sv] at each
%                      depth level (1:Nrf) and time step (1992-2011,
%                      monthly)
% -------------------------------------------------------------------------

mygrid = establish_mygrid('llc90');
read_dir = '../../release2/nctiles_monthly/';
fig_dir = 'figures/'; if ~exist(fig_dir,'dir'), mkdir(fig_dir), end;
mat_dir = ['mat/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir), end;

Nt = 240; 
trVolZon_ref = zeros(1,Nt);

%% Get dynamic height at each time step
% Atlantic basin and prep fields
amocBasin = v4_basin('atlExt');
amocBasin(isnan(amocBasin))=0;

% Prep mask for E/W points
lon = [17.5, -51.5];
iy = find(mygrid.LATS==-34);
mskC = mygrid.LATS_MASKS(iy).mskCedge;
xCondE = mygrid.XC==lon(1);
xCondW = mygrid.XC==lon(2);
mskC = mskC.*(xCondE - xCondW);

% Coriolis parameter
f = 2*7.27e-5*sind(-34);  % [1/s]

for n = 1:Nt
    % Read in monthly eta values from file
    phibot = read_nctiles([read_dir 'PHIBOT/PHIBOT'],'PHIBOT',n);
    phibot = phibot.*mygrid.mskC(:,:,1).*amocBasin;
    
    % Extract east and west points & take difference
    phibot(isnan(phibot))=0;
    phibot = convert2array(phibot.*mskC);
    trVolZon_ref(n) = squeeze(nansum(nansum(phibot,1),2));
    trVolZon_ref(n) = trVolZon_ref(n)*mygrid.DRF(end);
         
    if mod(n,10)==0; fprintf('## Reference flow: %d / %d \n',n,Nt); end
end

% Compute transport in Sv
trVolZon_ref = trVolZon_ref * 10^-6 * f^-1; %[Sv/m]


% Compute geostrophic velocity relative to no flow
% Compute longitudinal distance between E/W
dx = nansum(nansum(convert2array(mygrid.DXC(:,:,1).*mskC)));
v_ref = trVolZon_ref/mygrid.DRF(end)/dx; 

% Save it all 
save([mat_dir 'refFlow.mat'],'trVolZon_ref','v_ref');
end
