function [] = plotExpVar(modelVar,atmVar,varargin)
%
% Plot reconstructions with variance explained metric:
%
%  eta^2 = 1 - var(fwd - rec) / var(fwd)
%
% Inputs:
%
%       modelVar: 'eccov4r2' or 'eig' or eccov4r3  ....
%       atmVar: 'atm', 'long-atm-raw' etc, or 'flux' (default)
%
%
% Optional input:
%
%       condenseAtmVars: 0 (default) or 1 - condense atm vars to a few
%                        2 - condense interannual and clim to just wind and fwf
%
%       plotSelect:
%         all (default) - all reconstructions
%         full - only full 20 year reconstruction
%         clim - only climatology
%         annual - only annual averages
%         anomaly - monthly anomaly from climatology
%
%       plotStyle: 'paper' (default), 'prez'
%
% --------------------------------------------------------------------

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');


[adjField,condenseAtmVars,plotSelect,fullFile,regionFile,plotDir,plotStyle] = inputAndDirs(modelVar,atmVar,varargin);
load(fullFile);
load(regionFile);
ek=regionRec.atl.ek{1};

monthStr = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
Nt = 240;
Nmem=229;
Nmo = length(monthStr);
t=[1:Nt]./12+1992;
tMem=[1:Nmem]./12;
eps=.0001;

Nadj = length(adjField)+1;
% -------------------------------------------------------------------------


% --- Set fwd and reconstruction vars
fwd=samoc_for_mean;
rec=cell(Nadj,1);

if ~isempty(strfind(atmVar,'flux'))
  % Wind, fwf, hflux
  lv=[1 2 4 3];
else
  lv=[1 2 7 8 5 6 4 3];
end

rec{1} = regionRec.atl.ek{1}; 
rec{2} = samoc_adjFields{1};
for i=2:Nadj-1
  rec{i+1}=rec{i}+samoc_adjFields{lv(i)};
end

% --- Compute full explained variance and correlation
if ~isempty(strfind(plotSelect,'full')) || ~isempty(strfind(plotSelect,'all'))

  corr=cell(Nadj,1);
  ev=cell(Nadj,1);
  absErr=cell(Nadj,1);

  for i=1:Nadj
    [corr{i}, ev{i},absErr{i}] = calcExpVar(fwd,rec{i},Nmem);
    %if i>1, absErr{i} = absErr{i}./(absErr{1}+eps);end
  end 
end

% --- Compute climatology
if ~isempty(strfind(plotSelect,'clim')) || ~isempty(strfind(plotSelect,'all'))

  rec_clim=cell(Nadj,1);
  corr_clim=cell(Nadj,1);
  ev_clim=cell(Nadj,1);
  absErr_clim=cell(Nadj,1);

  fwd_clim=calcClimatology(fwd,20);

  for i=1:Nadj
    rec_clim{i} = calcClimatology(rec{i},20);
    [corr_clim{i}, ev_clim{i},absErr_clim{i}] = calcExpVar(fwd_clim,rec_clim{i},Nmem);
    %if i>1, absErr_clim{i} = absErr_clim{i}./(absErr_clim{1}+eps);end
  end 
end

% --- Compute annual mean
if ~isempty(strfind(plotSelect,'annual')) || ~isempty(strfind(plotSelect,'all'))

  rec_annual=cell(Nadj,1);
  corr_annual=cell(Nadj,1);
  ev_annual=cell(Nadj,1);
  absErr_annual=cell(Nadj,1);

  fwd_annual=calcAnnualMean(fwd,20);

  for i=1:Nadj
    rec_annual{i} = calcAnnualMean(rec{i},20);
    [corr_annual{i}, ev_annual{i},absErr_annual{i}] = calcExpVar(fwd_annual,rec_annual{i},Nmem);
    %if i>1, absErr_annual{i} = absErr_annual{i}./(absErr_annual{1}+eps);end
  end 
end

% --- Compute anomaly to climatology
if ~isempty(strfind(plotSelect,'anomaly')) || ~isempty(strfind(plotSelect,'all'))

  rec_anomaly=cell(Nadj,1);
  corr_anomaly=cell(Nadj,1);
  ev_anomaly=cell(Nadj,1);
  absErr_anomaly=cell(Nadj,1);

  fwd_clim = calcClimatology(fwd,20);
  fwd_anomaly = fwd - repmat(fwd_clim,[1 20]);

  % Apply filter
  win_width=13;
  w = gausswin(win_width);
  w = w/sum(w);
  fwd_anomaly = filter(w,1,fwd_anomaly);
  %fwd_anomaly = movmean(fwd_anomaly,win_width);

  for i=1:Nadj
    rec_clim = calcClimatology(rec{i},20);
    rec_anomaly{i} = rec{i} - repmat(rec_clim,[1 20]);

    % Apply filter
    for j=1:size(rec_anomaly{i},1)
        rec_anomaly{i}(j,:) = filter(w,1,rec_anomaly{i}(j,:));
        %rec_anomaly{i}(j,:) = movmean(rec_anomaly{i}(j,:),win_width);
    end

    [corr_anomaly{i}, ev_anomaly{i},absErr_anomaly{i}] = calcExpVar(fwd_anomaly,rec_anomaly{i},Nmem);
  end 
end

% --- Plots
if ~isempty(strfind(plotSelect,'full')) || ~isempty(strfind(plotSelect,'all'))
  if condenseAtmVars~=0
    showVect=[1 2 3 4 5];
    rec2=cell(length(showVect),1);
    ev2=rec2; absErr2=rec2;
    for i=1:length(showVect)
      rec2{i}=rec{showVect(i)};
      ev2{i}=ev{showVect(i)};
      absErr2{i}=absErr{showVect(i)};
    end
    expVarPlotter(t,fwd,rec2,...
                  tMem,ev2,...
                  sprintf('%s/exp_var_full',plotDir),condenseAtmVars,plotStyle);
    %absErrPlotter(t,absErr2,...
    %              tMem,...
    %              sprintf('%s/abs_err_full',plotDir),condenseAtmVars);
  else
    expVarPlotter(t,fwd,rec,...
                  tMem,ev,...
                  sprintf('%s/exp_var_full',plotDir),condenseAtmVars,plotStyle);
    %absErrPlotter(t,absErr,...
    %              tMem,...
    %              sprintf('%s/abs_err_full',plotDir),condenseAtmVars);
  end
end
if ~isempty(strfind(plotSelect,'clim')) || ~isempty(strfind(plotSelect,'all'))

    if condenseAtmVars~=0
        if condenseAtmVars==1
            showVect=[1 3 5 8 9];
        elseif condenseAtmVars==2
            showVect=[1 2 3 4 5];
        end
    rec2=cell(length(showVect),1);
    ev2=rec2; absErr2=rec2;
    for i=1:length(showVect)
      rec2{i}=rec_clim{showVect(i)};
      ev2{i}=ev_clim{showVect(i)};
      absErr2{i}=absErr_clim{showVect(i)};
    end
    expVarPlotter(1:12,fwd_clim,rec2,...
                  tMem,ev2,...
                  sprintf('%s/exp_var_clim',plotDir),condenseAtmVars,plotStyle);
    %absErrPlotter(1:12,absErr2,...
    %              tMem,...
    %              sprintf('%s/abs_err_clim',plotDir),condenseAtmVars);
  else
    expVarPlotter(1:12,fwd_clim,rec_clim,...
                  tMem,ev_clim,...
                  sprintf('%s/exp_var_clim',plotDir),condenseAtmVars,plotStyle);
    %absErrPlotter(1:12,absErr_clim,...
    %              tMem,...
    %              sprintf('%s/abs_err_clim',plotDir),condenseAtmVars);
  end
  
end
if ~isempty(strfind(plotSelect,'annual')) || ~isempty(strfind(plotSelect,'all'))

  if condenseAtmVars~=0
    
    showVect=[1:4];
    rec2=cell(length(showVect),1);
    ev2=rec2; absErr2=rec2;
    for i=1:length(showVect)
      rec2{i}=rec_annual{showVect(i)};
      ev2{i}=ev_annual{showVect(i)};
      absErr2{i}=absErr_annual{showVect(i)};
    end
    expVarPlotter(1992:2011,fwd_annual,rec2,...
                  tMem,ev2,...
                  sprintf('%s/exp_var_annual',plotDir),condenseAtmVars,plotStyle);
    %absErrPlotter(1992:2011,absErr2,...
    %              tMem,...
    %              sprintf('%s/abs_err_annual',plotDir),condenseAtmVars);
  else
    expVarPlotter(1992:2011,fwd_annual,rec_annual,...
                  tMem,ev_annual,...
                  sprintf('%s/exp_var_annual',plotDir),condenseAtmVars,plotStyle);
    %absErrPlotter(1992:2011,absErr_annual,...
    %              tMem,...
    %              sprintf('%s/abs_err_annual',plotDir),condenseAtmVars);
  end
end
if ~isempty(strfind(plotSelect,'anomaly')) || ~isempty(strfind(plotSelect,'all'))

  if condenseAtmVars~=0
    
    showVect=[1:4];
    rec2=cell(length(showVect),1);
    ev2=rec2; absErr2=rec2;
    for i=1:length(showVect)
      rec2{i}=rec_anomaly{showVect(i)};
      ev2{i}=ev_anomaly{showVect(i)};
      absErr2{i}=absErr_anomaly{showVect(i)};
    end
    expVarPlotter(t,fwd_anomaly,rec2,...
                  tMem,ev2,...
                  sprintf('%s/exp_var_anomaly',plotDir),condenseAtmVars,plotStyle);
    %absErrPlotter(1992:2011,absErr2,...
    %              tMem,...
    %              sprintf('%s/abs_err_annual',plotDir),condenseAtmVars);
  else
    expVarPlotter(t,fwd_anomaly,rec_anomaly,...
                  tMem,ev_anomaly,...
                  sprintf('%s/exp_var_anomaly',plotDir),condenseAtmVars,plotStyle);
    %absErrPlotter(1992:2011,absErr_annual,...
    %              tMem,...
    %              sprintf('%s/abs_err_annual',plotDir),condenseAtmVars);
  end
end
end

% -------------------------------------------------------------------------

function [] = expVarPlotter(t,fwd,rec,tMem,ev,saveName,condenseAtmVars,plotStyle)

% --- set the color map to something custom
Nadj=length(rec);
%cc=lines(Nadj); %cc(3,:)=cc(4,:); cc(4,:)=cc(5,:); 
cc=cbrewer2('Set1',Nadj);
%cc=[0 0 0; cc];
%axes('colororder',cc,'nextplot','replacechildren');
fs=20;


tMemTop=19*12;
figureW;

if strcmp(plotStyle,'paper')
    subplot(2,1,1)
else
    subplot(12,3,1:18)
end
set(gca,'colororder',cc,'nextplot','replacechildren');
plot(t,fwd,'k','linewidth',6); hold on


% --- First plot reconstruction
for i=1:Nadj
  plot(t,rec{i}(tMemTop,:))
end
%set(gca,'colororder',cc(1:3));
ylabel('$\delta J$ (Sv)')
if length(t)>20
    xlim([1992 2012])
    set(gca,'ylim',[-8 12],'ytick',[-5:5:10]);
elseif length(t)>12
    xlim([1992 2011])
    set(gca,'ylim',[-1.5 1.5],'ytick',[-1.5:0.5:1.5]);
else
    xlim([1 12])
    set(gca,'xtick',[1:12],...
        'xticklabel',{'Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'});
    set(gca,'ylim',[-4 6],'ytick',[-4:2:6]);
end
hold off
grid on
set(gca,'fontsize',fs);

% --- Now plot variance explained
if strcmp(plotStyle,'paper')
    subplot(2,1,2)
else
    subplot(12,3,[22:23,25:26,28:29,31:32,34:35])
end
set(gca,'colororder',cc,'nextplot','replacechildren');
hold on
plot(tMem,ev{1},'k') % Extra entry for legend
for i=1:Nadj
  plot(tMem,ev{i})
end
hold off
%colormap(cc);
xlim([0 19])
set(gca,'xtick',1:2:19)
xlabel('Memory in Years, $\tau_{mem}$')
ylabel('Variance Explained, $\eta^2$')
grid on
set(gca,'fontsize',fs);

ek_lbl='$\tau_x,\, 30-40^{\circ}$S';
taux_lbl='$\tau_x$';

if length(t)>20
    set(gca,'ylim',[.75 0.95],'ytick',[.75:.05:0.95]);
elseif length(t)>12
    set(gca,'ylim',[.5 0.9],'ytick',[.5:.1:0.9]);
else
    set(gca,'ylim',[.7 1],'ytick',[.7:.05:1]);
end
 
% --- And finally, handle the legend
if length(ev)==5 && condenseAtmVars==0
  hl=legend('ECCOv4r2',ek_lbl,taux_lbl,'Wind','Wind + FWF','Wind + HF','Location','EastOutside');
elseif condenseAtmVars~=0 && ~isempty(strfind(saveName,'annual')) 
  hl=legend('ECCOv4r2',ek_lbl,taux_lbl,'Wind','Wind+Precip','Location','EastOutside');
elseif condenseAtmVars==1 && ~isempty(strfind(saveName,'clim'))
  hl=legend('ECCOv4r2',ek_lbl,'Wind','Wind+Freshwater','Wind+Freshwater+Thermal','All','Location','EastOutside');
elseif condenseAtmVars==2 && ~isempty(strfind(saveName,'clim'))
  hl=legend('ECCOv4r2',ek_lbl,taux_lbl,'Wind','Wind+Precip','Wind+Precip+Runoff','Location','EastOutside');
elseif condenseAtmVars~=0 && ~isempty(strfind(saveName,'full'))
  hl=legend('ECCOv4r2',ek_lbl,taux_lbl,'Wind','Wind+Precip','Wind+Precip+Runoff','Location','EastOutside');
else
  hl=legend('ECCOv4r2',ek_lbl,taux_lbl,'Wind','Wind+Precip','Wind+FW','Wind+FW+SW','Wind+FW+Rad','Wind+FW+Rad+Atemp','All','Location','EastOutside');
end

% --- Put legend in empty subplot
if ~strcmp(plotStyle,'paper')
    sh=subplot(12,3,[24,27,30]);
    sp=get(sh,'position');
    delete(sh);
    set(hl,'position',sp+[0 -.04 0 0]);
end
set(hl,'FontSize',fs);

if ~strcmp(plotStyle,'paper')
    sh=subplot(12,3,[33,36]);
    %delete(sh);
    set(sh,'visible','off')
    text(-.22,-.02,'$\eta^2(\tau_{mem})$=1-$\frac{var(\delta J_{Fwd} - \delta J_{Rec}(\tau_{mem}))}{var(\delta J_{Fwd})}$',...
                'fontsize',fs)
end

if ~strcmp(plotStyle,'paper')
    saveName=[saveName '_prez'];
end
    savePlot(saveName,'pdf');

end

% -------------------------------------------------------------------------

function [] = absErrPlotter(t,absErr,tMem,saveName,condenseAtmVars)

% --- set the color map to something custom
Nadj=length(absErr);
cc=lines(Nadj); 
%cc=cat(1,[0 0 0], cc);

tMemTop=19*12;
figureW;

subplot(2,1,1)
hold on
set(gca,'colororder',cc); %,'nextplot','replacechildren');
for i=1:Nadj
  plot(t,absErr{i}(tMemTop,:))
end
hold off

ylabel('$\delta J$ (Sv)')
if length(t)>20
  xlim([1992 2012])
elseif length(t)>12
  xlim([1992 2011])
else
  xlim([1 12])
  set(gca,'xtick',[1:12],...
      'xticklabel',{'Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'});
end
hold off
grid on

subplot(2,1,2)
hold on
set(gca,'colororder',cc); %,'nextplot','replacechildren');
for i=1:Nadj
  sae=sum(absErr{i}.^2,2);
  plot(tMem,sae)
end
hold off
xlim([1 19])
%ylim([0 4])
xlabel('Memory in Years')
ylabel('$||\delta J_{Fwd} - \delta J_{Rec}||_2$')

ek_lbl='$\tau_x,\, 30-40^{\circ}$S';
taux_lbl='$\tau_x$';
 
if length(absErr)==5
  hl=legend(ek_lbl,taux_lbl,'Wind','Wind + FWF','Wind + FWF + HF','Location','EastOutside');
elseif condenseAtmVars && isempty(strfind(saveName,'clim')) 
  hl=legend(ek_lbl,taux_lbl,'Wind','Wind+Precip','Location','EastOutside');
elseif condenseAtmVars && ~isempty(strfind(saveName,'clim'))
  hl=legend(ek_lbl,'Wind','Wind+Freshwater','Wind+Freshwater+Thermal','All','Location','EastOutside');
else
  hl=legend(ek_lbl,taux_lbl,'Wind','Wind+Precip','Wind+FW','Wind+FW+SW','Wind+FW+Rad','Wind+FW+Rad+Atemp','All','Location','EastOutside');
end
  
set(hl,'FontSize',18);
grid on

savePlot(saveName);
end

% -------------------------------------------------------------------------

function [corr,expVar,absErr] = calcExpVar(fwd,rec,Nrecs)

corr = zeros(1,Nrecs);
expVar= zeros(1,Nrecs);
absErr= zeros(Nrecs,size(rec,2));

for i=1:Nrecs
  tmp=corrcoef(fwd,rec(i,:));
  corr(i) = tmp(1,2);

  absErr(i,:) = fwd-rec(i,:);

  expVar(i) = 1-var(absErr(i,:))/var(fwd);
  %expVar(i) = var(rec(i,:))/var(fwd);
  %expVar(i) = (var(fwd-rec(i,:)) - immse(fwd,rec(i,:)))/(var(fwd) + immse(fwd,rec(i,:)));

end

end

% -------------------------------------------------------------------------

function [adjField,condenseAtmVars,plotSelect,fullFile,regionFile,plotDir,plotStyle] = inputAndDirs(modelVar,atmVar,vv)

condenseAtmVars=0;
plotSelect='all';
plotStyle='paper';
for n=1:length(vv)
    if isa(vv{n},'char')
        if ~isempty(strfind(vv{n},'paper')) || ~isempty(strfind(vv{n},'prez')) || ~isempty(strfind(vv{n},'presentation'))
            plotStyle = vv{n};
        else
            plotSelect=vv{n};
        end
    else
        condenseAtmVars=vv{n};
    end
end

%% 0. Get directories etc ready
%% Step 0: Pre processing
adjField = set_adjField( atmVar );

%% Set up dirs
[saveDir,plotDir]=samoc_dirs('reconstruct-plot',modelVar,atmVar);

%% Load results
fullFile = sprintf('%s/../full/fullReconstruction.mat',saveDir);
regionFile=sprintf('%s/../region_rec.mat',saveDir);
end
