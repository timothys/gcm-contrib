function [] = verifyIndoSens()
% Verify linearity of sensitivity in Indonesian passage with fwd sens experiment
% That is:
%
%
% 1. Grab tauu perturbation generated in createTauuPerturbation.m (N/m^2)
% 2. Compute avg sens in perturbation box 
%       dJ/dx 
% 3. Compute avg 1 mo response in box: dJ/dx * delTauu
% 4. Verify with amoc computed from barfiles
%
% Results should correspond. 
%
% ----------------------------------------------------------------------

expStr = 'barotropic.mo';
refStr = 'ref';
perturbStr = 'pert';
freqStr = 'daily';

% --- Load grid and sensitivities
mygrid = establish_mygrid('llc90');
adj_file = 'mat/feb.240mo/adj_tauu.mat';
load(adj_file,'adxx');

% --- Load perturbation
dtau = rdmds2gcmfaces('tauu-perturbation/xx_tauu',12);
dtau = -dtau(:,:,2); % flip sign a la exf package
pert_xy = dtau~=0;
Nxy = nansum(pert_xy);
dtau_scalar = nansum(dtau)/Nxy;

% --- Compute average sensitivity per grid cell, normalized by area (Sv/N)
% Add in fraction of sensitivity during objective function evaluation
ad_feb_ind = 230; %Feb, corresponding to Ntad in samocReconstructionByMonth.m
adj = adxx(:,:,ad_feb_ind)+adxx(:,:,ad_feb_ind+1);
avg_adj = nansum(adj.*pert_xy)/Nxy;
fprintf('Average sensitivity in perturbation box, 1mo before SAMOC eval: %1.2e Sv/(N/m^2)\n',avg_adj);

% --- Compute samoc response
avg_samoc = avg_adj * dtau_scalar * Nxy;
samoc = nansum(nansum(adj.*dtau));

% --- Load perturb and reference amoc
ref_results_dir = sprintf('/workspace/results/samoc/%s/%s/',expStr,refStr);
pert_results_dir = sprintf('/workspacek/results/samoc/%s/%s/',expStr,perturbStr);
[ref_amoc] = calcBarfileAmoc(ref_results_dir,'month');
[per_amoc] = calcBarfileAmoc(pert_results_dir,'month');
diff_samoc = per_amoc-ref_amoc;

% Grab perturbed samoc during 2nd month, 1 month after start of perturbation
obj_ind = 2; 
samoc_response = diff_samoc(obj_ind);

% --- Compute relative error
absErr = abs(samoc_response - samoc);
relErr = absErr / samoc_response;

fprintf('Avg. forcing during Feb. = %1.2e N/m^2\n',dtau_scalar);
fprintf('Avg response from sens: Avg sensitivity * total wind force = %1.2e Sv \n',avg_samoc);
fprintf('Total response from sens: sum( sensitivity * wind pert) = %1.2e Sv \n',samoc);

fprintf(' --- \n');

fprintf('Response from perturbation: SAMOC_p - SAMOC_r = %1.2e Sv \n',samoc_response);

fprintf('Relative error: (%1.2e - %1.2e)/(%1.2e) = %1.2e %% \n',samoc_response,samoc,samoc_response,relErr*100);

keyboard

end
