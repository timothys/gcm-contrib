function [] = plotTauvClim()

modelVar = 'eccov4r2';
atmVar = 'short-atm';

mygrid=establish_mygrid('llc90');

matFile = sprintf('mat/exf/%s/%s/exf_tauv.mat',modelVar,atmVar);
load(matFile,'exf_fld');

plotDir = 'figures/exf';
if ~exist(plotDir,'dir'), mkdir(plotDir); end;
plotDir=sprintf('%s/%s',plotDir, modelVar);
if ~exist(plotDir,'dir'), mkdir(plotDir); end;
plotDir=sprintf('%s/%s',plotDir, atmVar);
if ~exist(plotDir,'dir'), mkdir(plotDir); end;


% --- compute climatology and 3mo avg
Nmo = size(exf_fld.f1,3);
Nyrs = Nmo/12;
exf_fld=convert2gcmfaces(exf_fld);
exf_fld=exf_fld - repmat(nanmean(exf_fld,3),[1 1 Nmo]);
exf_fld=convert2gcmfaces(exf_fld);
tauv_clim = calcClimatology(exf_fld,Nyrs);
tauv_3mo = 0*tauv_clim(:,:,1:4);
for i=1:4
    tauv_3mo(:,:,i) = nanmean(tauv_clim(:,:,(i-1)*3+1:i*3),3);
end


% --- Plot up 3mo avg
fs=22;
lblStr = {'DJF','MAM','JJA','SON'};
ncolors=7;
cmap=flipdim(cbrewer2('BrBG',ncolors),1);
cmax=nanmax(abs(tauv_3mo));

timeDir = 1;
clbl = 'N/m$^2$';

opts = struct(...
        'saveVideo',1,...
        'logFld',1,...
        'mmapOpt',5,...
        'cmap',cmap,...
        'colorbarFlag',1);

figureW;
for i=1:4
    xlbl = sprintf('%s Climatology\n%s Avg.','$\delta\tau_y$',lblStr{i});
    figName = sprintf('%s/tauv_%s_clim',plotDir,lblStr{i});
    strs = struct('xlbl',xlbl,'clbl',clbl,'vidName',figName);
    plotVideo(tauv_3mo(:,:,i),strs,opts,mygrid,1);

    %m_map_atl(tauv_3mo(:,:,i),5);
    %caxis([-cmax cmax]);
    %colormap(cmap);
    %hl=niceColorbar;
    %ylabel(hl,clbl,'rotation',0,'fontsize',fs);
    %set(get(hl,'ylabel'),'position',[3.5989    0.0124         0]);
    %xlabel(xlbl,'fontsize',fs);
    %savePlot(figName,'pdf');
end

% --- Plot up clim 
lblStr = {'Jan','Feb','March','April','May','June','July','Aug','Sept','Oct','Nov','Dec'};
cmax=nanmax(abs(tauv_clim));
figureW;
for i=1:12

    xlbl = sprintf('%s Climatology\n%s Avg.','$\delta\tau_y$',lblStr{i});
    figName = sprintf('%s/tauv_%s_clim',plotDir,lblStr{i});
    strs = struct('xlbl',xlbl,'clbl',clbl,'vidName',figName);
    plotVideo(tauv_clim(:,:,i),strs,opts,mygrid,1);

    %m_map_atl(tauv_clim(:,:,i),5);
    %caxis([-cmax cmax]);
    %colormap(cmap);
    %hl=niceColorbar;
    %ylabel(hl,clim,'rotation',0,'fontsize',fs);
    %set(get(hl,'ylabel'),'position',[3.5989    0.0124         0]);
    %xlabel(clim,'fontsize',fs);
    %savePlot(figName,'pdf');
end

end
