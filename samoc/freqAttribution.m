function [] = freqAttribution( ) 

%% Prep dirs & load reconstruction file
dirs = establish_samocDirs;
saveDir = [dirs.mat 'reconstruct.34S/'];
plotSaveDir=[dirs.figs 'reconstruct.34S/'];
reconstructFile = sprintf('mat/reconstruct.34S/fullReconstruction.mat');
load(reconstructFile);
% adjFields={}
Nt=240;

adjField = {'tauu','tauv','aqh','atemp','swdown','lwdown','precip','runoff','hflux','sflux'};
Nadj = length(adjField);
freq = 1/12:1/12:20;
dJ_freq = fft(samoc_for_mean,Nt);
dJ_freq_adj = zeros(Nadj,Nt);

for i=1:Nadj
    adjFile = sprintf('%s%s_reconstruction.mat',saveDir,adjField{i});
    load(adjFile,'samoc_rec_adj','xx_fld','cumsamoc_adj','samoc_for_mean');
    dJ_freq_adj(i,:) = fft(samoc_rec_adj(1,:),Nt);
    
    figure
    plot(1:Nt,abs(dJ_freq_adj(i,:)))
    xlabel(' 1/yr ')
    ylabel('Amplitude')
    legend(sprintf('%s',adjField{i}));
end




end