function [] = plotSAMOCIntegralTimescale()
% More comments when I'm back from my bike ride...
%
% ------------------------------------------------------------

modelVar    = 'eccov4r2';
atmVar      = 'long-atm';
lat         = -34;
tau_mem     = 228;
Nyrs        = 20;

% --- Initial setup
mygrid = establish_mygrid('llc90');

fig_dir = sprintf('figures/reconstruct.34S/%s/%s/timescale_2D',modelVar,atmVar);
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;

% --- Load/calc 3D reconstruction dJ(x,y,t)
samoc2d = loadSAMOC2D(modelVar,atmVar,lat,tau_mem);


% --- Load/calc 3D correlation function R_jj(x,y,\tau)
% --- Correlation function for timescale didn't really work
%[Rjj,lags] = loadSAMOCCorrelationFunction(modelVar,atmVar,samoc2d);

% --- Calc average time scale from spatiotemporal reconstruction 
[avgTime] = loadSAMOCTimescale(modelVar,atmVar,lat,tau_mem);

% --- Calc average time scale simply from sensitivities
[avgTime_sens] = loadSAMOCSensitivityTimescale(atmVar,tau_mem);


% --- Plot up some results
Ncolors = 7;
cmap = cbrewer2('YlGnBu',Ncolors);
cax = [0 14];
ticks = [1:2:13];

adjFldName = set_adjField(atmVar);
for i = 1:length(adjFldName)
    % --- Plots from reconstruction
    figureW; 
    eval(sprintf('m_map_atl(avgTime.%s,5,{''myCmap'',cmap},{''myCaxis'',cax})',adjFldName{i}))
    xlabel(sprintf('%s\n%s',...
        adjFldName{i},...
        '$T(x,y) = \int_{t_0}^{t_f} \delta \tilde{J}(x,y,t) \, dt / \int_{t_0}^{t_f} \delta \hat{J}(x,y,t) \, dt$'));
    hc = niceColorbar;
    hc.Ticks=ticks;
    savePlot(sprintf('%s/%s_timescale',fig_dir,adjFldName{i}),'png');

    % --- Plots from sensitivity
    figureW; 
    eval(sprintf('m_map_atl(avgTime_sens.%s,5,{''myCmap'',cmap},{''myCaxis'',cax})',adjFldName{i}))
    xlabel(sprintf('%s\n%s',...
        adjFldName{i},...
        '$T(x,y) = \int_{0}^{\tau_{max}} s|\partial J/\partial u| \,ds/ \int_{0}^{\tau_{max}} |\partial J / \partial u | \, ds$'));
    hc = niceColorbar;
    hc.Ticks=ticks;
    savePlot(sprintf('%s/%s_sens_timescale',fig_dir,adjFldName{i}),'png');
end

end

% ------------------------------------------------------------

function [samoc2d] = loadSAMOC2D(modelVar,atmVar,lat,tau_mem)

% --- Load the reconstruction
matfile = sprintf('mat/reconstruct.34S/%s/%s/full_2D/fullReconstruction.mat',...
                    modelVar,atmVar);
if ~exist(matfile,'file')
    sensFrac = calcSamocSensFrac();
    samoc_adjFields = samocReconstructionByMonth_2D(modelVar,atmVar,lat,tau_mem,sensFrac);
else
    load(matfile,'samoc_adjFields');
end

% --- Make a more intuitive structure with names associated with the field
adjFldName = set_adjField(atmVar);
samoc2d = struct();
for i = 1:length(adjFldName)
    eval(sprintf('samoc2d.%s = samoc_adjFields{i};',adjFldName{i}));
end

end

% ------------------------------------------------------------

function [Rjj,lags] = loadSAMOCCorrelationFunction(modelVar,atmVar,samoc2d)

matfile = sprintf('mat/reconstruct.34S/%s/%s/full_2D/correlation_function.mat',...
                    modelVar,atmVar);

% --- Create structure with same fields as samoc2d containing correlation function
adjFldName = set_adjField(atmVar);
Rjj = struct();
if ~exist(matfile,'file')

    % First compute anomaly from seasonal cycle
    samoc_anom2d = struct();
    adjFldName = set_adjField(atmVar);
    for i =1:length(adjFldName)
        eval(sprintf('samoc_anom2d.%s = calcClimAnomaly(samoc2d.%s,Nyrs);',...
            adjFldName{i},adjFldName{i}));
    end

    for i = 1:length(adjFldName)
        eval(sprintf('[Rjj.%s,lags] = calcTemporalCorrelationFunction(samoc2d.%s,1);',...
            adjFldName{i},adjFldName{i}));
        save(matfile,'Rjj','lags');
    end
else
    load(matfile,'Rjj','lags')
end

end

% ------------------------------------------------------------

function [avgTime] = loadSAMOCTimescale(modelVar,atmVar,lat,tau_mem)

matfile = sprintf('mat/reconstruct.34S/%s/%s/timescale_2D/fullReconstruction.mat',...
                    modelVar,atmVar);
if ~exist(matfile,'file')
    sensFrac = calcSamocSensFrac();
    samoc_adjFields = samocReconstructionByMonth_2D(...
                        modelVar,atmVar,lat,tau_mem,sensFrac,'timescale');
else
    load(matfile,'samoc_adjFields');
end

% --- Make a more intuitive structure with names associated with the field
adjFldName = set_adjField(atmVar);
avgTime = struct();
for i = 1:length(adjFldName)

    % --- Convert timescale to years
    tmp = samoc_adjFields{i} / 12;

    % --- Save in container
    eval(sprintf('avgTime.%s = tmp;',adjFldName{i}));
end

end

% ------------------------------------------------------------

function [avgTime] = loadSAMOCSensitivityTimescale(atmVar,tau_mem)

global mygrid;

% --- Create time vector for weighted average
s = [1:tau_mem] + 0.5;
s_mat = repmat(reshape(s,[1 1 tau_mem]),[mygrid.faces2gcmSize 1]);

% --- Set up 
adjFldName = set_adjField(atmVar);
avgTime = struct();

for i = 1:length(adjFldName)

    % --- Load sensitivity file
    matfile = sprintf('mat/dec.240mo/adj_%s.mat',adjFldName{i});
    load(matfile,'adxx');

    % --- Compute ratio
    adxx = convert2gcmfaces(adxx);
    adxx = flipdim(adxx,3);
    adxx = adxx(:,:,1:tau_mem);
    
    numer = nansum(abs(s_mat .* adxx),3);
    denom = nansum(abs(adxx),3);

    tmp = numer ./ denom; 
    tmp = convert2gcmfaces(tmp);

    % --- Convert timescale to years
    tmp = tmp / 12;

    % --- Save to container
    eval(sprintf('avgTime.%s = tmp;',adjFldName{i}));
end

end
