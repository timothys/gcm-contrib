function [curvy,nContours] = plotFhContours(varargin)
% Plot f/H contours for ECCO's llc90 grid
% This is for you, Reviewer #4 
%
% optional:
%
%   doPlot - 1=default, yes show the plot
%            0= don't plot
%
% ------------------------------------------

doPlot = 1;
for n=1:length(varargin)
    doPlot = varargin{n};
end

mygrid = establish_mygrid('llc90');

% Compute cell centered f
Omega = 7.27e-5;                    % Earth's rot. rate, 1/s
f = 2 * Omega * sind(mygrid.YC);    % Coriolis param,    1/s 

H = mygrid.Depth;

curvy = log10(abs(f ./ H));

nContours = 40;

if doPlot==1
    figureW;
    m_map_atl({'contour',curvy,nContours,'k'},8);
end

end
