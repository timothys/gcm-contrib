function [] = plotOvStf()
% Plot the overturning streamfunction at 34S to visualize the AMOC
%
% ----------------------------------------------


mygrid = establish_mygrid('llc90');

% --- Load up SAMOC
slat = -34; 
[amoc, depthmax, kmax, ovStf] = calcAMOC(slat,'eulerian');
%[amocBolus, dmBolus, kmBolus, ovStfBolus] = calcAMOC(slat,'bolus');
%[amocRes, dmRes, kmRes, ovStfRes] = calcAMOC(slat,'residual');
%[~,~,~,trVolZon] = calcAMOC(slat,'trVolZon');


grayCol = [192 192 192]./255;
fs = 24;

figureL;
ph=plot(ovStf,mygrid.RF,'color',grayCol);
hold on;
plot(mean(ovStf,2),mygrid.RF,'k');
ylabel('Depth (m)','fontsize',fs);
xlabel('$\psi_{MOC}$ (Sv)','fontsize',fs);
grid on;
savePlot('figures/prez/ovStf_34S','pdf','portrait');
end
