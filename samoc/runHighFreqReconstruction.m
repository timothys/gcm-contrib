function [] = runHighFreqReconstruction( mygrid )
%
% Want to use the high frequency adj and forcing from recent experiment
% to reconstruct AMOC and see if this eliminates errors in the barotropic response
% 
% --------------------------------------------------------------------------------

%% 0. Set up dirs
runStr = 'dec.480bwk/';
resultsDir = ['../../results/samoc/' runStr];
diagsDir = [resultsDir 'diags/'];

adjMatDir = ['mat/' runStr];
xxMatDir = 'mat/reconstruct.34S/hf/';


%% 1. Post process adxx hflux file

adjField=set_adjField('flux');
postProcess_adxx(runStr,resultsDir,adjField,mygrid,10^-6);

%% 2. Prepare hflux forcing file
%'EXFtaux ' 'EXFtauy ' 'EXFqnet ' 'EXFempmr' 'EXFatemp' 'EXFaqh  ' 'EXFlwdn ' 'EXFswdn ' 'EXFevap ' 'EXFpreci' 'EXFroff '
xx_taux=rdmds2gcmfaces([diagsDir,'exf'],NaN,'rec',1);
xx_tauy=rdmds2gcmfaces([diagsDir,'exf'],NaN,'rec',2);
for n=1:size(xx_taux.f1,3)
  [xx_taux(:,:,n),xx_tauy(:,:,n)]=calc_UEVNfromUXVY(xx_taux(:,:,n),xx_tauy(:,:,n));
end
xx_fld=xx_taux; save([xxMatDir 'xx_tauu'],'xx_fld');
xx_fld=xx_tauy; save([xxMatDir 'xx_tauv'],'xx_fld');
clear xx_taux xx_tauy;

xx_fld = rdmds2gcmfaces([diagsDir, 'exf'],NaN,'rec',3);
save([xxMatDir 'xx_hflux'],'xx_fld');

xx_fld = rdmds2gcmfaces([diagsDir, 'exf'],NaN,'rec',4);
save([xxMatDir 'xx_sflux'],'xx_fld');

clear xx_fld;

%% 3. Run reconstruction 
samocReconstructionByMonth('flux',-34,mygrid,'hf');

%% 4. Do plotting
plotSamocReconstructionByMonth('flux',-34,mygrid,'hf');


end
