function [] = calcEkmanSens()
% Compute correspondence of sensitivity at one month lag to Ekman theory
% That is:
%
% 1. Compute avg  dJ/dx in box from lat_1-lat_2, normalized by area (Sv/N)
%    including band between these latitudes grabs accumulated Ekman response 
%    over the month
% 2. Compute total force from wind stress over that area: tau_x* area (N)
% 3. Multiple to get SAMOC response
%
% 4. Compute V_ek = - tau_x/rho_0/f 
%    for tau_x = 1 N/m^2 across 34S
%    this gives integral( vel * dz ) over ekman layer thickness for fluid parcel.
% 3. Compute transport across latitude band 
%    T_ek = V_ek * Lx(lat)
%    and convert to sverdrups
% 
% Results should correspond. 
%
% ----------------------------------------------------------------------


% --- Load grid and sensitivities
mygrid = establish_mygrid('llc90');
adj_file = 'mat/dec.240mo/adj_tauu.mat';
load(adj_file,'adxx');

% --- Compute sensitivity in box
msks = createRegionMasks('ekman');

ekmanBandMask_c = msks.atl.full & mygrid.YC<-32 & mygrid.YC>-36;
ekmanBandMask = msks.atl.full & mygrid.YG<-32 & mygrid.YG>-36;

% --- Compute zonal extent of 34S
%   Not clear how to do this, safest seems to be Gael's masks
%   But these are all relatively equal
%msk34_1 = msks.atl.full & mygrid.YC <-33.6 & mygrid.YC>-34.5;
%Lx_1 = nansum(mygrid.DXC.*msk34)
%latInd =56; 
%msk34_2 =(abs(mygrid.LATS_MASKS(latInd).mskWedge)+abs(mygrid.LATS_MASKS(latInd).mskSedge)).*msks.atl.full;
%Lx2= nansum((abs(mygrid.LATS_MASKS(latInd).mskWedge).*mygrid.DYG + ...
%             abs(mygrid.LATS_MASKS(latInd).mskSedge).*mygrid.DXC ).*msks.atl.full)
latInd =56; 
msk34 = mygrid.LATS_MASKS(latInd).mskCedge.*msks.atl.full;
msk34W= mygrid.LATS_MASKS(latInd).mskWedge.*msks.atl.full;
msk34S= mygrid.LATS_MASKS(latInd).mskSedge.*msks.atl.full;
Lx = nansum(abs(msk34W).*mygrid.DYG + abs(msk34S).*mygrid.DXG);


% --- Compute meridional extent of ekman band
msk0lon = mygrid.XC < 0.4 & mygrid.XC >-0.5;
Ly = nansum(msk0lon.*mygrid.DYG.*ekmanBandMask);


% --- Compute average sensitivity per grid cell, normalized by area (Sv/N)
% Add in fraction of sensitivity during objective function evaluation
sensFrac = calcSamocSensFrac;
Nmo = size(adxx.f1,3);
adj = (sensFrac(12)*adxx(:,:,Nmo)+adxx(:,:,Nmo-1)).* ...
                     ekmanBandMask;     % Sv / (N/m^2)
area = (mygrid.DXG.*mygrid.DYG);        % m^2
adj_norm = adj./area;                   % Sv / N

avg_adj_norm=nansum(adj_norm)/nansum(ekmanBandMask); % Sv / N

% --- Compute total response of SAMOC
Ntau = 50;
tau_x0 = logspace(-5,3,Ntau);    % N/m^2
tau_x= 0.01;                     % N/m^2

wind_force   =tau_x * nansum(area.*ekmanBandMask);
wind_force_1 =tau_x * Lx *Ly;
%wind_force_2=tau_x * Lx2*Ly;
%wind_force_3=tau_x * Lx3*Ly;

%SAMOC  = avg_adj_norm * wind_force; 
SAMOC= avg_adj_norm * wind_force; % about 1% error between these ...
%SAMOC_2= avg_adj_norm * wind_force_2;
%SAMOC_3= avg_adj_norm * wind_force_3;


% --- Compute from ekman theory
rho_0 = 1030;   % kg/m^3
f     =-8.13e-5;% 1/s @ 34S

V_ek = - tau_x / rho_0 / f;


T_ek = V_ek * Lx *10^-6; 
%T_ek_2 = V_ek * Lx2 *10^-6; 
%T_ek_3 = V_ek * Lx3 *10^-6; 

%figure; loglog(T_ek,SAMOC,T_ek,T_ek);
%figure; 
%loglog(tau_x, abs(T_ek-SAMOC)./abs(T_ek))
%xlabel('$\delta \tau$')
%ylabel('Relative Error')
%title(sprintf('Relative error between \n %s','$\bar{\frac{dJ}{dF}}\delta F$ and T$_{Ek}$'))


% --- Ok now try without computing total force
SAMOC_2=zeros(size(tau_x));
for i=1:length(SAMOC_2)
    SAMOC_2(i) = nansum(tau_x(i)*adj);
end



%figure; 
%loglog(tau_x, abs(T_ek-SAMOC_2)./abs(T_ek))
%xlabel('$\delta \tau$')
%ylabel('Relative Error')
%title(sprintf('Relative error between \n %s','$\sum_{i}\delta\tau_x\Big(\frac{dJ}{d\tau_x}\Big)_i$ and  T$_{Ek}$'))
%
%% --- Now compute for a single grid cell
Nek = nansum(ekmanBandMask);
Nx = nansum(abs(msk34S)+abs(msk34W));%nansum(msk34);
avg_adj = nansum(adj)/Nek;

SAMOC_gridCell = avg_adj*tau_x;

T_ek_gridCell = T_ek / Nx;
%
%
%figure; 
%loglog(tau_x, abs(T_ek_gridCell-SAMOC_gridCell)./abs(T_ek_gridCell))
%xlabel('$\delta \tau$')
%ylabel('Relative Error')
%title(sprintf('Relative error between \n %s','$\delta J$/$N_{Ek}$ and T$_{Ek}$/N$_x$'))
%
%% --- Use all points over N latitude bands
%
Nlat = nansum(msk0lon.*ekmanBandMask);
T_ek_gridCell_2 = T_ek / Nx / Nlat;
%
%figure; 
%loglog(tau_x, abs(T_ek_gridCell_2-SAMOC_gridCell)./abs(T_ek_gridCell_2))
%xlabel('$\delta \tau$')
%ylabel('Relative Error')
%title(sprintf('Relative error between \n %s','$\delta J$/$N_{Ek}$ and T$_{Ek}$/(N$_x$*N$_{lat}$)'))
%
%% --- Or normalize SAMOC by Nx, so that SAMOC_lat is Sv per deg. longitude,
%%    Since working with 1x1 degree model, each grid cell is 1 deg longitude in x
%%    T_ek_gridCell = T_ek_lon
SAMOC_lon = nansum(adj)*tau_x / Nx;
T_ek_lon = T_ek_gridCell;
%
%figure; 
%loglog(tau_x, abs(T_ek_lon-SAMOC_lon)./abs(T_ek_lon))
%xlabel('$\delta \tau$')
%ylabel('Relative Error')
%title(sprintf('Relative error between \n %s \n both giving trsp per deg. longitude','$\delta J$/$N_{x}$ and T$_{Ek}$/N$_x$'))
keyboard

end
