function [] = createTauuPerturbation(mygrid)
% Want to create control vector for perturbation experiment. 
% In response to Reviewer #4, show pathways from Indonesian passages to SAMOC 
% 
%   dtau = perturbation scalar
% ----------------------------------------------------------------------------------

mygrid = establish_mygrid('llc90');

% Use negative for positive perturbation
%   because the exf package is so intuitive :( 
dtau = -0.1; %[N/m^2]

saveDir = 'tauu-perturbation/'; if ~exist(saveDir,'dir'), mkdir(saveDir); end;
pert_file=[saveDir 'xx_tauu.0000000012.data'];

% Create the spatial mask for perturbation
yExtent = (mygrid.YG < -10) .* (mygrid.YG > -15); 
xExtent = (mygrid.XG < 120) .* (mygrid.XG > 100); 
pert_xy = xExtent .* yExtent .*mygrid.mskW(:,:,1);

% Perturbing the second month 
% Note: can only be used when ctrl_get_gen.F has been modified
%       because 1st two records are set to zero for wind stress there
Nt = 12; % leave plenty of records
pert = 0*mygrid.mskW(:,:,1);
pert = convert2gcmfaces(pert);
pert = repmat(pert,[1 1 Nt]);

pert(:,:,2) = dtau * convert2gcmfaces(pert_xy);
pert(isnan(pert))=0;

% Make the directory with perturbation and this script
write2file(pert_file,pert);   
write2meta(pert_file,size(pert));   
copyfile('createTauuPerturbation.m',saveDir);
tar('tauu-perturbation.tar.gz',saveDir);
end
