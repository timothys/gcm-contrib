function [] = enso_mask()
% Mask for testing sign of hflux sensitivity

mygrid = establish_mygrid('llc90');

%% Create directory for masks
if ~exist('masks/','dir'), mkdir('masks'),end

%% 2D tracer field mask
% Masking a specific part of the pacific basin
%pacMsk=v4_basin('pac');
%yCond=mygrid.YC<5 & mygrid.YC>-5;
%xCond = mygrid.XC < -120 & mygrid.XC > -170;
msks=createRegionMasks('ekman');

mskC = msks.globe;
mskC = convert2gcmfaces(mskC);


%% Depth level mask
mskK=[ones(1,5), zeros(1,45)];
    
%% Temporal mask
%mskT=ones(1,300); 
%mskT=zeros(1,240); mskT(240)=1;
mskT=[zeros(1,11), 1];

%% Write all to file
write2file('masks/hflux_test_maskC',mskC);
write2file('masks/hflux_test_maskK',mskK);
write2file('masks/hflux_test_maskT',mskT);

%% For reproducibility and such ... 
%  copy this file as well so we know what made the masks
copyfile('hflux_mask.m','masks/');

% Zip it up for sending to a supercomputer 
tar('masks.tar.gz','masks/');
end
