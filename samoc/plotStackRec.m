function [] = stackRecPlot( forceVar, varargin )
% Make stacked bar plot showing contribution from various regions  
%
% Inputs:
%       
%       forceVar : e.g. tauu, tauv, hflux ... 
%           For original, single stacked reconstruction plot input one variable
%           For updated, stacked stack plot (condensing tons of figs to one)
%               input as cell structure e.g. {'tauu','tauv','precip'}
%
% Optional inputs
%       
%       modelVar : e.g. eccov4r2
%       atmVar : 'long-atm' (default)
%       tMem: Memory time
%       varSelect: climatology 'clim' or annual averaging 'annual' or (default) 'both'
%       separator: override separator
%                  'ek' separate 30-40S from SSubtrop Atl
%                  'south' separate southern ocean 40S-60S
%                  'none' (default)
%       doNormalize: normalize the output by some factor
%                  'normAmp' - normalize by fwd clim/annual peak - trough
%                  'normMean'- normalize by mean of SAMOC signal
%                  'normNone' - default
%
% ------------------------------------------------------------------------------------

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');

%% --- Parse inputs 
% First set variable argument defaults
modelVar='eccov4r2';
varSelect='both';
separator='none';
atmVar='long-atm';
doNormalize=0;
tMem=int32(19*12);

% Now check user input
for n=1:length(varargin)
  if isa(varargin{n},'char')
    if strcmp(varargin{n},'clim') || strcmp(varargin{n},'annual') || strcmp(varargin{n},'anomaly')  
      varSelect=varargin{n};
    elseif ~isempty(strfind(varargin{n},'atm')) || ~isempty(strfind(varargin{n},'flux'))
      atmVar=varargin{n};
    elseif ~isempty(strfind(varargin{n},'norm'))
      if ~isempty(strfind(varargin{n},'Amp'))
        doNormalize=1;
      else
        doNormalize=2;
      end
    elseif ~isempty(strfind(varargin{n},'ecco')) || ~isempty(strfind(varargin{n},'eig')) || ~isempty(strfind(varargin{n},'jra'))
      modelVar=varargin{n};
    else
      separator=varargin{n};
    end
  elseif isa(varargin{n},'double') 
    tMem=int32(varargin{n});
  else
    fprintf('Didn''t recognize varargin at position: %d ...\n',n);
  end
end

% --- Setup directories and file names
fullrecfile=sprintf('mat/reconstruct.34S/%s/%s/full/fullReconstruction.mat',modelVar,atmVar);
matfile=sprintf('mat/reconstruct.34S/%s/%s/region_rec.mat',modelVar,atmVar);
load(matfile,'regionRec','msks');
load(fullrecfile,'samoc_adjFields','samoc_for_mean');
  
[~,mainPlotDir] = samoc_dirs('reconstruct-plot',modelVar,atmVar,'region-rec');


% --- Set some final parameters
mskNames = fieldnames(msks);
Nt=240;
t=[1992+1/12:1/12:2012];

% --- Adding flexibility for multiStackPlot
if ~iscell(forceVar)
  forceVar={forceVar};
end
% god this is messy but fuck it I'm so done with this paper
Natm=length(forceVar);
% Not making this ready for 'both', just 'annual' or 'clim'
if Natm>1 && strcmp(varSelect,'both'), error('multiStackPlot not compatible with doing both climatology and annual averaging, do either separately...'); end;
flux_ind=cell(Natm,1);
separatorCell=cell(Natm,1);
group_index=cell(Natm,1);
rec_avg=cell(Natm,1);
rec_avg_condensed_p=cell(Natm,1);
rec_avg_condensed_n=cell(Natm,1);

for i=1:Natm
  [~,flux_ind{i}] = set_adjField(atmVar,forceVar{i});
  plotDir=sprintf('%s/%s',mainPlotDir,forceVar{i}); if ~exist(plotDir,'dir'),mkdir(plotDir);end;
  
  if strcmp(forceVar{i},'hflux') || strcmp(forceVar{i},'buoyancy') || strcmp(forceVar{i},'aqh') || ~isempty(strfind(forceVar{i},'down')) || strcmp(forceVar{i},'warm') || strcmp(forceVar{i},'texas') || strcmp(forceVar{i},'rad') || strcmp(forceVar{i},'atemp')
  
  %  for i=flux_ind
  %    %samoc_adjFields{i}=samoc_adjFields{i}-regionRec.atl.subPolar{i}-regionRec.acc{i};
  %    regionRec.atl.subPolar{i}=1*regionRec.atl.subPolar{i};
  %    regionRec.acc{i}=1*regionRec.acc{i};
  %  end
  end
  
  if strcmp(varSelect,'annual') || strcmp(varSelect,'both')
    separatorCell{i}=separator;
    [rec_avg{i},rec_avg_condensed_p{i},rec_avg_condensed_n{i},group_index{i}] = calcStackRec(...
    regionRec,tMem,samoc_adjFields,flux_ind{i},'annual',separatorCell{i},doNormalize);
  
    % do plotting
   % makeSingleStackPlot(1992:2011,rec_avg{i},rec_avg_condensed_p{i}',rec_avg_condensed_n{i}',...
   % sprintf('%s/%s_annual_condensed',plotDir,forceVar{i}),forceVar{i},group_index{i},separatorCell{i});
  end


  if strcmp(varSelect,'anomaly') 
    separatorCell{i}=separator;
    [rec_avg{i},rec_avg_condensed_p{i},rec_avg_condensed_n{i},group_index{i}] = calcStackRec(...
    regionRec,tMem,samoc_adjFields,flux_ind{i},'anomaly',separatorCell{i},doNormalize);
  
    % do plotting
   % makeSingleStackPlot(1992:2011,rec_avg{i},rec_avg_condensed_p{i}',rec_avg_condensed_n{i}',...
   % sprintf('%s/%s_annual_condensed',plotDir,forceVar{i}),forceVar{i},group_index{i},separatorCell{i});
  end
  
  if strcmp(varSelect,'clim') || strcmp(varSelect,'both')
  
    % --- Pull out Ekman component in zonal wind stress climatology
    if ~isempty(find(flux_ind{i}==1))
      separatorCell{i}=[separator 'ek'];
    else
      separatorCell{i}=separator;
    end
    [rec_avg{i},rec_avg_condensed_p{i},rec_avg_condensed_n{i},group_index{i}] = calcStackRec(...
    regionRec,tMem,samoc_adjFields,flux_ind{i},'clim',separatorCell{i},doNormalize);
  
    % Do plotting
   % makeSingleStackPlot(1:12,rec_avg{i},rec_avg_condensed_p{i}',rec_avg_condensed_n{i}',...
   % sprintf('%s/%s_clim_condensed',plotDir,forceVar{i}),forceVar{i},group_index{i},separatorCell{i});
  end
end

% --- Do plotting here, with multiStackPlot (which makes singles)
if strcmp(varSelect,'clim')
  t=1:12;
elseif strcmp(varSelect,'annual')
  t=1992:2011;
elseif strcmp(varSelect,'anomaly')
  t=[1/12:1/12:20]+1992;
else
  error('Did not recognize averaging option. Clim, annual, or anomaly options available.')
end
plotDir=sprintf('%s/%s',mainPlotDir,'multiStack'); if ~exist(plotDir,'dir'),mkdir(plotDir);end;
filename=sprintf('%s/%s',plotDir,varSelect);
makeMultiStackPlot(t,rec_avg,rec_avg_condensed_p,rec_avg_condensed_n,...
                   forceVar,group_index,separatorCell,...
                   filename);

end

% ---------------------------------------------------------------------
function [] = makeSingleStackPlot(t,rec,pos_data,neg_data,savename,forceVar,ind,separator)

global mygrid;

fullLegend = grabSamocGroupLegend(separator);

Nregion=size(pos_data,2);
legendStr=cell(Nregion+1,1);

% --- First legend entry for full reconstruction
legendStr{1} = grabForceVarLegend(forceVar);

% --- fill legend string
if nargin<7 && Nregion==length(fullLegend);
  for i=1:length(fullLegend)
    legendStr{i+1}=fullLegend{i};
  end
else
  for i=1:length(ind)
    legendStr{i+1}=fullLegend{ind(i)};
  end
  legendStr{end}='Other';
end

% --- Set colorbar
%cc=parula(Nregion);
cc=grabSamocGroupColors(length(fullLegend));
cc=cc(ind,:);
cc=[cc; 1 1 1]; % add white for ''other''
grayCol=[0 0 0];%[96,96,96]/255;

% --- Plot data on top two rows of 3x2 grid
figureH;
if exist('ind','var')
  ax1=subplot(3,2,1:4);
  colormap(ax1,cc)
end
hold on
% --- First plot total rec
%     Do this here so it's the first legend entry
plot(t,rec,'.-','markersize',28,'Color',grayCol);

% --- Plot positive stacked bars
bar(t,pos_data,'stacked')
% --- Add negative stacked bars
if exist('ind','var')
  ax2=subplot(3,2,1:4);
  colormap(ax2,cc);
end
bar(t,neg_data,'stacked')
  ylabel('Sv')
  if length(t)>12
    xlim([1991 2012])
  else
    xlbl_clim;
  end
  grid on
%colormap(cc);
%colormap(cc(1:Nregion,:))

%  --- Re-plot total rec so it sits in front of stacked bars
plot(t,rec,'r.-','markersize',28,'Color',grayCol);%'markeredgecolor',cc(2,:),'markerfacecolor',cc(2,:))
hold off

% --- Create legend based on positive data
lh=legend(legendStr,'fontsize',18);
if exist('ind','var')
  % Set colormap to not be white
  colormap(ax1,cc);
 
  % Move legend to empty subplot
  sh=subplot(3,2,5);
  sp=get(sh,'position');
  set(lh,'position',sp);
  delete(sh);
else
  legend(legendStr,'Location','EastOutside','fontsize',18)
end

% --- Add map showing regional contributions
if exist('ind','var')
  plotSelectSamocMasks(ind,separator,[],cc)
end

savePlot(savename);

end

function [] = makeMultiStackPlot(t,rec,pos_data,neg_data,forceVar,ind,separator,filename)

global mygrid;

% --- get cell structure length
Natm = length(rec);
fullLegend=cell(Natm,1);
Nregion=size(pos_data{1},1);
legendStr=cell(Natm,1);


% --- Make all the legend entries
for i=1:Natm
  fullLegend{i} = grabSamocGroupLegend(separator{i});

  % --- First legend entry for full reconstruction
  legendStr{i}{1} = grabForceVarLegend(forceVar{i});
  if ~isempty(strfind(legendStr{i}{1},'Air Temp'))
          legendStr{i}{1}=['Air Temp. + Rad.'];
  end

  % --- fill legend string
  if Nregion==length(fullLegend{i});
    for n=1:length(fullLegend{i})
      legendStr{i}{n+1}=cell2mat(fullLegend{i});
    end
  else
    for n=1:length(ind{i})
      legendStr{i}{n+1}=cell2mat(fullLegend{i}(ind{i}(n)));

    end
    legendStr{i}{Nregion+1}='Other';
  end
end


% --- Plot data on Natm x Nx grid
Nx=4;
cc=cell(Natm,1);
ax=cell(Natm,1);
%figureH;
for i=1:Natm

  % --- Set colorbar 
  cc{i}=grabSamocGroupColors(length(fullLegend{i}));
  cc{i}=cc{i}(ind{i},:);
  cc{i}=[cc{i}; 1 1 1]; % add white for ''other''
  grayCol=[0 0 0];%[96,96,96]/255;

  % --- Make subplot, grab axis handle
  figureWhalf;
  ax{i}=subplot(1,Nx,1:Nx-1); %subplot(Natm,Nx,(i-1)*Nx+1:i*Nx-1);
  colormap(ax{i},cc{i})
  hold on
  % --- First plot total rec
  %     Do this here so it's the first legend entry
  plot(t,rec{i},'.-','markersize',28,'Color',grayCol);

  % --- Plot positive stacked bars
  bh=bar(t,pos_data{i}','stacked');

  % If version is after 2016, have to change colors manually
  versionStr = version;
  if str2double(versionStr(end-5:end-2))>2016
    for k=1:size(cc{i},1)
        bh(k).FaceColor=cc{i}(k,:);
        bh(k).EdgeColor='k';
    end
  end

  % --- Add negative stacked bars
  bh=bar(t,neg_data{i}','stacked');
  if str2double(versionStr(end-5:end-2))>2016
    for k=1:size(cc{i},1)
        bh(k).FaceColor=cc{i}(k,:);
        bh(k).EdgeColor='k';
    end
  end
    ylabel('Sv')
    if length(t)>12
      xlim([1991 2012])
    else
      xlbl_clim;
    end
  grid on

  %  --- Re-plot total rec so it sits in front of stacked bars
  plot(t,rec{i},'.-','markersize',28,'Color',grayCol);
  hold off

  % --- Manual adjustments for the paper ... 
  if strcmp(forceVar{i},'tauu') && length(t)==12
      ylim([-3 5])
      set(gca,'ytick',[-3:5])
      fprintf(' --- Forcing ylimit and tick for tauu, clim plot --- \n');
  elseif strcmp(forceVar{i},'tauv') && length(t)==12
      ylim([-.8 .8])
      set(gca,'ytick',[-.8:.2:.8])
      fprintf(' --- Forcing ylimit and tick for tauv, clim plot --- \n');
  end

  % --- Create legend based on positive data
  lh=legend(legendStr{i},'fontsize',16,'Location','best');
  % Set colormap to not be white
  %colormap(ax{i},cc{i});
  
  % Move legend to empty subplot
  sh=subplot(1,Nx,Nx);%subplot(Natm,Nx,i*Nx);
  sp=get(sh,'position');
  set(lh,'position',sp);
  delete(sh);

  %set(gcf,'paperorientation','landscape');
  tmpfile=sprintf('%s_%s_multiStack',filename,forceVar{i});
  saveas(gcf,tmpfile,'png');
  fprintf('Saved plot: %s\n',tmpfile);
end

allInd=unique(cell2mat(ind));
allInd=allInd(:);
cc=grabSamocGroupColors(length(fullLegend{1}));
cc=cc(allInd,:);
tmpfile=sprintf('%s_globe_multiStack',filename);
plotSelectSamocMasks(allInd,separator{1},tmpfile,cc)


%% --- Add map showing regional contributions
%if exist('ind','var')
%  plotSelectSamocMasks(ind,separator,[],cc)
%end


end
