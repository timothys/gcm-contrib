function [] = plotPerturbEta()
% Plot 6 hourly avg Eta over 4 months, showing rapid fwd propagation
% of perturbation from Indonesian passages
% 
% ------------------------------------------------------------------

expStr = 'barotropic.mo';
refStr = 'ref';
pertStr= 'pert';
freqStr= 'daily';

mygrid=establish_mygrid('llc90');

ref_results_dir = sprintf('../../results/samoc/%s/%s/diags/',expStr,refStr);
per_results_dir = sprintf('../../results/samoc/%s/%s/diags/',expStr,pertStr);

% Grab iterations for time stamp in plot
eta_file = sprintf('eta_%s',freqStr);
iters = grabAllIters(ref_results_dir,eta_file);

st = 1;
ed = length(iters);
dates = ts2dte(iters,3600,1992,1,1,13,1);
dates = dates(st:ed,:);
dates = mat2cell(dates,ones(1,size(dates,1)),size(dates,2));
for n=1:length(dates)
    dates{n} = num2str(dates{n});
end

% Load eta from each
% 'ETAN   ','MXLDEPTH'
eta_ref = rdmds2gcmfaces([ref_results_dir eta_file],iters(st:ed),'rec',1);
eta_per = rdmds2gcmfaces([per_results_dir eta_file],iters(st:ed),'rec',1);

eta_diff = eta_per-eta_ref;

% Make some flicks
fig_dir = sprintf('figures/%s',expStr);
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;


plotEtaFigs(eta_diff,dates,fig_dir,freqStr)
%saveEtaMovie(eta_diff,dates,sprintf('%s/eta_diff_%s',fig_dir,freqStr));

keyboard
end

% ------------------------------------------------------------------

function [] = plotEtaFigs(fld,timeStamp,fig_dir,freqStr)
% fld = 3D gcmfaces object, 2D field, 3rd dim gives time records
% timeStamp = cell aray of time stamps for each record of field

global mygrid 


% Select time stamps to plot
if ~isempty(strfind(fig_dir,'bwk'))

    selectVect = [32,45];
    t0 = 28;

    if strcmp(freqStr,'6hr')
        selectVect = selectVect*4;
        t0 = t0*4;
    end
elseif ~isempty(strfind(fig_dir,'mo'))

    selectVect = [28,35];
    t0=0;

else
    error('Not sure which experiment to grab, \nchoose either \n    ''barotropic.bwk'' or ''barotropic.mo''');

end



% Set colormap
ncolors = 7;
cmap = cbrewer2('BrBG',ncolors);
cmap = flipdim(cmap,1);

for n=1:length(selectVect)

    if strcmp(freqStr,'daily')

        filename = sprintf('%s/eta_diff_%02d_days',fig_dir,selectVect(n)-t0);

        xlbl = sprintf('%s\n%d days since perturbation',...
                    '$\eta_{pert}$ - $\eta_{ref}$',selectVect(n)-t0);

    elseif strcmp(freqStr,'6hr')

        filename = sprintf('%s/eta_diff_%03d_hrs',fig_dir,(selectVect(n)-t0)*6);

        xlbl = sprintf('%s\n%d hrs since perturbation',...
                    '$\eta_{pert}$ - $\eta_{ref}$',(selectVect(n)-t0)*6);
    end

    % --- Removing some stuff for publishable figure
    xlbl = '';

    % Strings
    strs = struct( ...
            'xlbl',xlbl,...
            'time','',...
            'clbl','m',...
            'vidName',filename);
    
    % Other options
    timeDir = 1;
    opts = struct( ...
            'saveVideo',1,...
            'logFld',1,...
            'mmapOpt',8,...
            'fontSize',22,...
            'cmap',cmap);
    
    % Grab f/h contours
    [fhContours,nContours] = plotFhContours(0);
    
    plotVideo({fld(:,:,selectVect(n)),fhContours,nContours},...
                strs,opts,mygrid,timeDir);
end

end

% ------------------------------------------------------------------

function [] = saveEtaMovie(fld,timeStamp,filename)

global mygrid
% Set colormap
ncolors = 7;
cmap = cbrewer2('BrBG',ncolors);
cmap = flipdim(cmap,1);

% Set up labels
if ~isempty(strfind(filename,'daily'))
    timeStr = 'days';
elseif ~isempty(strfind(filename,'6hr'))
    timeStr = '6hrs';
else
    timeStr = '';
end
strs = struct( ...
        'xlbl','$\eta_{pert}$ - $\eta_{ref}$',...
        'time',timeStr,...
        'clbl','[m]',...
        'vidName',filename);

% Other options
timeDir = 1;
opts = struct( ...
        'saveVideo',1,...
        'logFld',1,...
        'mmapOpt',8,...
        'cmap',cmap);

% Grab f/h contours
[fhContours,nContours] = plotFhContours(0);


plotVideo({fld,fhContours,nContours},strs,opts,mygrid,timeDir);
        
end
