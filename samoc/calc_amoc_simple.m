function [amoc] = calc_amoc_simple()
%
% Script to compute AMOC given meridional velocity
%
% Modify: loadDir - directory where meridional velocity is saved
%         lat - integer of latitude band 
%
% Output: amoc - 1D vector of doubles
%                amoc at desired latitude from 1992-2015
%
% -----------------------------------------------------------------

% --- 0. Set latitude, load velocity, grid weighting, etc ...
lat = 16;
loadDir = 'mat/eccov4r3/state/';

load([loadDir 'eccov4r3_amoc.mat'],'VVELMASS',...
        'atlBasin','atlMskS','atlLatMsk','latInd','atlDrf','atlDxg'); 

% --- 1. Do volume weighting, and grab only the atlantic
Nr = size(VVELMASS,3);
Nt = size(VVELMASS,4);
for n=1:Nt
  VVELMASS(:,:,:,n) = VVELMASS(:,:,:,n).*atlDxg.*atlDrf.*atlBasin;
end

% --- 2. Compute streamfunction at each latitude
% Note: 
% atlOvStf in this case is a 2D array containing streamfunction at latitude across
%  i: depth
%  j: time
atlOvStf = zeros(Nr+1,Nt);

% grab our favorite latitude band
iy = find(lat == latInd);

% compute zonally integrated transport and integrate vertically:
tmp = VVELMASS .* repmat(atlLatMsk(:,:,iy),[1 1 Nr Nt]);
trsp = squeeze(nansum(nansum(tmp,2),1));
atlOvStf(1:Nr,:)=flipdim(cumsum(flipdim(trsp,1),1),1); 

% convert to Sv and change sign:
atlOvStf=-1e-6*atlOvStf;
atlOvStf(end,:)=0;

% --- 3. Compute AMOC
kmax = zeros(1,Nt);
amoc = zeros(1,Nt);
for n = 1:Nt
  [~,kmax(n)] = max(squeeze(atlOvStf(:,n)));
  amoc(n) = atlOvStf(kmax(n),n);
end
end
