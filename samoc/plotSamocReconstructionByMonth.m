function [] = plotSamocReconstructionByMonth(modelVar,atmVar,lat,varargin)
% plot reconstruction of AMOC at 34S from adjoint sensitivities
%
%       modelVar: e.g. 'eccov4r2'
%	atmVar: 'flux' or 'long-atm' etc. select to reconstruct with atmospheric state or 
%		 computed fluxes. 
%	lat: denotes latitude for computing reconstruction
%
%	varargin:
%
%	  dirString: name for specifying save location of experiment, beyond monthly/flux/atm
%		e.g. monthly_"nobaro"_flux denotes no barotropic response ... no quotes though
%
% -------------------------------------------------------------------------

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');

%% 0. Get directories etc ready
adjField = set_adjField( atmVar );

monthStr = {'jan','feb','march','april','may','june','july','aug','sept','oct','nov','dec'};
Nt = 240;
Nmem=229;
Nadj = length(adjField);
Nmo = length(monthStr);
t=[1:Nt]./12 + 1992;
tMem=[1:Nmem]./12;

%% Sift through varargs
dirString='full';
for n=1:length(varargin)
	if isa(varargin{n},'char'), dirString=varargin{n};
	else fprintf('** Don''t recognize the %dth varargin, type help samocReconstructionByMonth for options and how to assign\n Except only pay attention to dirString...\n',n);
	     return;
	end

end

%% Set up dirs
[saveDir,plotDir] = samoc_dirs('reconstruct-plot',modelVar,atmVar,dirString);

%% Load results
fullFile = sprintf('%s/fullReconstruction.mat',saveDir);
load(fullFile);

%% This is just a guess on optimal memory time
ind = 228; 
% -------------------------------------------------------------------------

% 1. Plot reconstruction for each variable individually 
for i = 1:Nadj
     
    % Plot reconstruction from particular variable
    figureW;
    plot(t,samoc_adjFields{i}([12,60,120,180,229],:));
      xlabel('Years')
      xlim([1992 2012])
      ylabel('\deltaJ(t)')
  %	ylim([-9,9])
      legend('\tau_{mem}=1 yr','\tau_{mem}=5 yrs','\tau_{mem}=10 yrs',...
  	   '\tau_{mem}=15 yr','\tau_{mem}=20 yrs','Location','Best')
    title(sprintf('Variability attribution to %s',adjField{i}))
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])

    grid on
    saveas(gcf,sprintf('%s/%s_reconstruct',plotDir,adjField{i}),'pdf')
end
% -------------------------------------------------------------------------

%% 2. Full reconstruction
samoc_rec(samoc_rec==0)=NaN;
 
figureW;
windRec = samoc_adjFields{1}+samoc_adjFields{2};
mhfRec = windRec + samoc_adjFields{4};
fullRec= mhfRec+samoc_adjFields{3};
plot(t,samoc_for_mean,t,samoc_adjFields{1}(ind,:),t,windRec(ind,:),t,mhfRec(ind,:),t,fullRec(ind,:));
    xlabel('Years')
    xlim([1992 2012])
    ylabel('\deltaJ(t)')
    legend('Forward Model','Zonal Wind Only','Wind Stress Reconstruction','Wind + FWF Reconstruction','Full','Location','Best')
    grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%s/full_reconstruct_all',plotDir),'pdf')

figureW;
plot(t,samoc_for_mean,'color',[.2 .2 .2])
hold on
plot(t,mhfRec(ind,:),'color',[0 .5 .2]);
hold off
    xlabel('Years')
    xlim([1992 2012])
    ylabel('\deltaJ(t)')
    legend('\deltaJ_{Fwd}(t)','\deltaJ_{Rec}(t)','Location','Best')
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%s/full_reconstruct',plotDir),'pdf')
% -------------------------------------------------------------------------

%% 3. Error in reconstructions
normErr = zeros(Nmem,1);
normErr_tauu = zeros(Nmem,1);
normErr_wind = zeros(Nmem,1);
normErr_mhf = zeros(Nmem,1);
for n=1:Nmem
    normErr(n) = norm(samoc_for_mean - fullRec(n,:));
    normErr_tauu(n) = norm(samoc_for_mean - samoc_adjFields{1}(n,:));
    normErr_wind(n) = norm(samoc_for_mean - windRec(n,:));
    normErr_mhf(n) = norm(samoc_for_mean - mhfRec(n,:));
end

figureW;
plot(tMem,normErr_tauu,tMem,normErr_wind,tMem,normErr_mhf,tMem,normErr)
ylabel('||\deltaJ(t)_{Fwd} - \deltaJ(t)_{R}||_2')
xlabel('Memory in Years')
legend('\tau_x','\tau_x + \tau_y','\tau_x + \tau_y + FWF','All')
set(gca,'xtick',[1:20])
xlim([0 20])
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%s/norm_error',plotDir),'pdf')

% compute 1 sided FT of windRec error
zonalErr = samoc_for_mean-samoc_adjFields{1}(ind,:);
windErr = samoc_for_mean-windRec(ind,:);
mhfErr =  samoc_for_mean-mhfRec(ind,:);
fullErr = samoc_for_mean-fullRec(ind,:);

freq = (0:Nt/2-1)'/240 * 12; % Frequency in 1/yr
fftauu = fft(zonalErr,Nt);
ffw = fft(windErr,Nt);
ffmhf = fft(mhfErr,Nt);
fffull = fft(fullErr,Nt);
freq_tauuErr = fftauu(1:Nt/2)/Nt;
freq_tauuErr(2:end) = 2*freq_tauuErr(2:end);
freq_windErr = ffw(1:Nt/2)/Nt;
freq_windErr(2:end) = 2*freq_windErr(2:end);
freq_mhfErr = ffmhf(1:Nt/2)/Nt;
freq_mhfErr(2:end) = 2*freq_mhfErr(2:end);
freq_fullErr = fffull(1:Nt/2)/Nt;
freq_fullErr(2:end) = 2*freq_fullErr(2:end);

% Remove higher frequencies 
freq_windErr_low = ffw;
freq_windErr_low(41:Nt-39)=zeros(size(freq_windErr_low(41:Nt-39)));
freq_windErr_low(21) = 0;
freq_windErr_low(Nt-19)=0;
windErr_low = ifft(freq_windErr_low);


figureW;
semilogx(freq,abs(freq_tauuErr),freq,abs(freq_windErr),freq,abs(freq_mhfErr),freq,abs(freq_fullErr));
xlabel('Frequency [1/yr]')
ylabel('Amplitude');
legend('Zonal Wind','All Wind','Wind + FWF','full')
%title('Frequency domain of wind reconstruction error')
grid on
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%s/freq_err',plotDir),'pdf');

% Plot explained variance and correlation
full_corr = zeros(1,Nmem);
mhf_corr = zeros(1,Nmem);
wind_corr = zeros(1,Nmem);
zonal_corr= zeros(1,Nmem);
full_ev = zeros(1,Nmem);
mhf_ev = zeros(1,Nmem);
wind_ev = zeros(1,Nmem);
zonal_ev= zeros(1,Nmem);

for i=1:Nmem
  tmp=corrcoef(samoc_for_mean,fullRec(i,:));
  full_corr(i) = tmp(1,2);
  tmp=corrcoef(samoc_for_mean,windRec(i,:));
  wind_corr(i) = tmp(1,2);
  tmp=corrcoef(samoc_for_mean,samoc_adjFields{1}(i,:));
  zonal_corr(i) = tmp(1,2);
  tmp=corrcoef(samoc_for_mean,mhfRec(i,:));
  mhf_corr(i) = tmp(1,2);

  full_ev(i) = 1-var(samoc_for_mean-fullRec(i,:))/var(samoc_for_mean);
  mhf_ev(i) = 1-var(samoc_for_mean-mhfRec(i,:))/var(samoc_for_mean);
  wind_ev(i) = 1-var(samoc_for_mean-windRec(i,:))/var(samoc_for_mean);
  zonal_ev(i) = 1-var(samoc_for_mean-samoc_adjFields{1}(i,:))/var(samoc_for_mean);
end
figureW;
plot(tMem,full_corr,tMem,mhf_corr,tMem,wind_corr,tMem,zonal_corr)
hold on 
plot(tMem,full_ev,'--',tMem,mhf_corr,'--',tMem,wind_ev,'--',tMem,zonal_ev,'--')
xlabel('Memory in Years')
ylabel('Correlation / Explained variance')
legend('Correlation: Full','Correlation: \tau_x + \tau_y + FWF','Correlation: \tau_x + \tau_y','Correlation: \tau_x',...
       'Exp. Var: Full','Exp. Var: \tau_x + \tau_y + FWF','Exp. Var: \tau_x + \tau_y','Exp. Var: \tau_x','Location','Best')
set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,sprintf('%s/exp_var',plotDir),'pdf');


% Plot wind reconstruction error and low pass filtered version
%figureW;
%plot(t,windErr,t,real(windErr_low))
%xlabel('Years')
%ylabel('\deltaJ(t) - \deltaJ(t)_{Wind}')
%title('Forward model - wind reconstruction')
%legend('Full error','Low pass & season filtered')
%set(gca,'xtick',[1:20])
%xlim([0 20])
%grid on
%set(gcf,'paperorientation','landscape')
%set(gcf,'paperunits','normalized')
%set(gcf,'paperposition',[0 0 1 1])
%saveas(gcf,sprintf('%sfwd_minus_wind',plotDir),'pdf');

end
