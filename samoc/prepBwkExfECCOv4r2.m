function [] = prepBwkExfECCOv4r2()
% Driver script to prepare biweekly forcing files for reconstruction


modelVar    = 'eccov4r2';
atmVar      = 'short-atm-bwk';
adjField    = set_adjField('atm');
mygrid      = establish_mygrid('llc90');

prepForcingFile(modelVar,atmVar,adjField,mygrid);
end
