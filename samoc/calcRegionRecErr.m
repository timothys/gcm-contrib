function [relErr,absErr,topRegionIndex] = calcRegionRecErr(forceVar,varargin)
% Calculate error showing how regional reconstructions improve with
%   increasing lead time. This operates on the interannual or climatology
%   reconstructions.
%
%   For example if considering tropPac_interannual
% 
%     relErr = sum_t abs(tropPac_interannual(t,tau_i) - tropPac_interannual(t,tau=19yrs))
%
%     absErr = relErr / sum_t abs(tropPac_interannual(t,tau=19yrs))
%
%   Where t is time (in this case years), tau_i is the varying lead time, tau=19yrs is
%   the "full" reconstruction, considered to be "correct". 
%
% Required input:
%
%   forceVar = atm forcing variable to consider
%
% Optional inputs: 
%
%   modelVar = e.g. 'eccov4r2' (default)
%   atmVar = e.g. 'flux' or 'long-atm'(default) or 'short-atm'
%   separator = how to grab groups, separate out ekman, southern ocean basins?
%              'southek'(default) 
%   varSelect = 'clim' or 'annual'
%   numTopRegions = 5 (default) ... number of top contributing regions 
%   
% Outputs:
%
%   absErr = [Ngroups x tauMemMax] matrix
%           Ngroups = number of regional groups to consider
%           tauMemMax = 229 months
%
%   relErr = same with absolute error
%   
% --------------------------------------------------------------------------------------- 

% --- Set defaults
modelVar = 'eccov4r2';
atmVar = 'long-atm';
separator = 'southek';
varSelect = 'clim';
numTopRegions = 5;

% --- Now check user input
for n=1:length(varargin)
  if isa(varargin{n},'char')
    if strcmp(varargin{n},'clim') || strcmp(varargin{n},'annual')  
      varSelect=varargin{n};
    elseif ~isempty(strfind(varargin{n},'atm')) || ~isempty(strfind(varargin{n},'flux'))
      atmVar=varargin{n};
    elseif ~isempty(strfind(varargin{n},'ecco')) || ~isempty(strfind(varargin{n},'eig')) || ~isempty(strfind(varargin{n},'jra'))
      modelVar=varargin{n};
    else
      separator=varargin{n};
    end
  elseif isa(varargin{n},'double')
    numTopRegions = varargin{n};
  else
    fprintf('Didn''t recognize varargin at position: %d ...\n',n);
  end
end

% --- Set index of forcing variable to grab
[~,flux_ind] = set_adjField(atmVar,forceVar);

% --- Load regional reconstruction
matfile = sprintf('mat/reconstruct.34S/%s/%s/region_rec.mat',modelVar,atmVar);
load(matfile,'regionRec');

% --- Grab region reconstruction groups similar to stackRecPlot
%     Except here this is a matrix of size 
%       [Ngroups, Nt, tauMemMax]
stackedRec = grabSamocGroupsAllMem(regionRec,separator,flux_ind);

% --- Set sizes
%     Note: tauMemMax hard coded to 229 in grabSamocGroupsAllMem.m
Ngroups = size(stackedRec,1);
Nt = size(stackedRec,2);
tauMemMax = size(stackedRec,3);
Nmo = 12;
Nyrs = Nt/Nmo;

% --- Grab clim or annual avg
% For this operation, easier with permuted stackedRec...
stackedRec = permute(stackedRec, [3 2 1]);
if strcmp(varSelect,'clim')

  rec_avg = zeros(tauMemMax,Nmo,Ngroups);

  for g = 1:Ngroups
    rec_avg(:,:,g) = calcClimatology(stackedRec(:,:,g),Nyrs);
  end
else

  rec_avg = zeros(tauMemMax,Nyrs,Ngroups);

  for g = 1:Ngroups
    rec_avg(:,:,g) = calcAnnualMean(stackedRec(:,:,g),Nyrs);
  end
end

% Permute back before computing top contributors
% making rec_avg [Ngroups x Nt x tauMemMax]
rec_avg = permute(rec_avg,[3 2 1]);

% --- Now grab top contributing regions
%     Note: grab top regions based on final reconstruction
[~, ind_top, ind_etc] = grabTopRegions(rec_avg(:,:,tauMemMax),numTopRegions);
rec_avg_top = zeros(numTopRegions+2,size(rec_avg,2),tauMemMax);

for i = 1:numTopRegions
  rec_avg_top(i,:,:) = rec_avg(ind_top(i),:,:);
end

for i = 1:length(ind_etc)
  rec_avg_top(numTopRegions+1,:,:) = rec_avg_top(numTopRegions+1,:,:) +  ...
                                     rec_avg(ind_etc(i),:,:);
end
rec_avg_top(end,:,:) = sum(rec_avg,1);

% --- At last, compute error
absErr = zeros(size(rec_avg_top));
relErr = relErr;

for g = 1:numTopRegions+2
  absErr(g,:,:) = abs(rec_avg_top(g,:,:) - repmat(rec_avg_top(g,:,tauMemMax),[1 1 tauMemMax]));
  relErr(g,:,:) = absErr(g,:,:) ./ abs(repmat(rec_avg_top(g,:,tauMemMax),[1 1 tauMemMax]));
end

% --- Take sum
absErr = squeeze(sum(relErr,2));
relErr = squeeze(sum(absErr,2));

% --- More explanatory output name
topRegionIndex = ind_top;
end
