function [] = plotPrecipRec()
% Show the precipitation reconstruction from each forcing dataset 
%
% -------------------------------------------------------------

%% --- mygrid global variable
mygrid = establish_mygrid('llc90');

% Save this stuff
savename = 'figures/reconstruct.34S/precip_rec';
savename_annual = 'figures/reconstruct.34S/precip_rec_annual';

% Each dataset
Nyrs_gpcp=15;
Nyrs_ecco=20;
Nyrs_era=20;

t_gpcp=[1/12:1/12:Nyrs_gpcp]+1992;
t_ecco=[1/12:1/12:Nyrs_ecco]+1992;
t_era =[1/12:1/12:Nyrs_era ]+1992;

t_gpcp_yrs=[1:Nyrs_gpcp]+1992-1;
t_ecco_yrs=[1:Nyrs_ecco]+1992-1;
t_era_yrs =[1:Nyrs_era ]+1992-1;


recDir = 'mat/reconstruct.34S/';
suffix = 'atl/subPolar/precip.mat';
load([recDir 'jra55-gpcp/short-atm/' suffix],'fld_rec');
gpcp=fld_rec;
load([recDir 'eig/long-atm/' suffix],'fld_rec');
era=fld_rec;
load([recDir 'eccov4r2/long-atm/' suffix],'fld_rec');
ecco=fld_rec;

% Compute annual averages
gpcp_annual=calcAnnualMean(gpcp,Nyrs_gpcp);
era_annual =calcAnnualMean(era,Nyrs_era);
ecco_annual=calcAnnualMean(ecco,Nyrs_ecco);


% First, take a look at annually averaged precip field
plotRegionExf('precip','atl.subPolar','annAvg');

% Grab reconstruction index. Want 1 less year than sim time.
ecco_ind=(Nyrs_ecco-1)*12;
era_ind =(Nyrs_era -1)*12;
gpcp_ind=(Nyrs_gpcp-1)*12;

% Now look at reconstructions
figureW;
plot(t_ecco,ecco(ecco_ind,:),t_era,era(era_ind,:),t_gpcp,gpcp(gpcp_ind,:))
ylabel('$\delta$ J N. Atl. Subpolar Precip')
legend('ECCOv4r2','ERA','GPCP');
grid on;
savePlot(savename,'pdf');


figureW;
plot(t_ecco_yrs,ecco_annual(ecco_ind,:),...
     t_era_yrs,era_annual(era_ind,:), ...
     t_gpcp_yrs,gpcp_annual(gpcp_ind,:))
ylabel('$\delta$ J N. Atl. Subpolar Precip')
legend('ECCOv4r2','ERA','GPCP');
grid on;
savePlot(savename_annual,'pdf');

end
