function [] = plotBigSensFig(varargin)
% Make the samoc sensitivity movies with cbrewer2
%
% Optional Inputs
%       
%       runStr : string denoting run type 
%               'dec.240mo' (default)
%
%       atmVar : atm vars or flux vars
%               'flux' (default) or 'atm'
%
%       areaWeighted: normalize sensitivities by area or not
%               'noNormByArea' (default) or 'normByArea'
%
% Colorbar notes
%
%       tauu: 
%           non-area-normalized:
%               +/- 10^-1 Sv / (N/m^2) 
%
%           area-normalized:
%               +/- 10^-11 Sv / N 
%
%       tauv: 
%           non-area-normalized:
%               +/- 10^-1 Sv / (N/m^2) 
%
%           area-normalized:
%               +/- 10^-11 Sv / N 
%
%       oceHeat: 
%           non-area-normalized:
%               +/- 10^6 Sv / (W/m^2)
%
%           area-normalized:
%               +/- 10^-16 Sv / W
%               from tf-1mo -> tf-6mo there are 18, 52, 48, 36, 22, 6 grid cells where 
%               floor(log10(abs(adxx))) = -15
%               They are close to 1-2 * 10^-15 and saturating these points does not remove features
%               (shows path of heat flux sensitivity moving through Drake Passage, still visible
%               with slightly saturated colorbar)
%
%       fwf: 
%           non-area-normalized:
%               +/- 10^9 Sv / (Sv/m^2)
%               Max amplitude is 10116 * 10^6 Sv / (Sv/m^2) and occurs at 1 point on the globe
%               at time step tf - 3 months ONLY. Removing this point by setting colorbar
%               to +/- 10^9 is no problem ...
%
%           area-normalized:
%               +/- 10^0 Sv / Sv  
%
% ----------------------------------
runStr = 'dec.240mo';
atmVar = 'flux';
areaWeighted='noNormByArea';
for n=1:length(varargin)
    if ischar(varargin{n})
        if ~isempty(strfind(varargin{n},'atm')) || ~isempty(strfind(varargin{n},'flux'))
            atmVar = varargin{n};
        elseif ~isempty(strfind(lower(varargin{n}),'area'))
            areaWeighted=varargin{n};
        else
            runStr = varargin{n};
        end
    end
end

% --- Load grid and setup data
mygrid=establish_mygrid('llc90');
matDir = sprintf('mat/%s',runStr);
if ~exist(matDir,'dir'), error(sprintf('Cannot find matDir: %s, exiting ...',matDir)); end;
figDir = sprintf('figures/%s',runStr);
if ~exist(figDir,'dir'), mkdir(figDir); end;

% --- Set the fields to plot
adjField = set_adjField(atmVar);
Nadj = length(adjField);

if ~isempty(strfind(atmVar,'atm'))
    if strcmp(areaWeighted,'normByArea')
        cunits = {sprintf('Sv/N'),sprintf('Sv/N'),sprintf('Sv/\n[kg/kg m^2]'),...
              sprintf('Sv/\n[K m$^2$]'),sprintf('Sv/W'), sprintf('Sv/W'),
              sprintf('Sv/\n[m^3/s]'),sprintf('Sv/\n[m^3/s]')};       
    else
        cunits = {sprintf('Sv/\n[N/m$^2$]'),sprintf('Sv/\n[N/$m^2$]'),sprintf('Sv/\n[kg/kg]'),...
              sprintf('Sv/\nK'),sprintf('Sv/\n[W/m$^2$]'), sprintf('Sv/\n[W/m$^2$]'),
              sprintf('Sv/\n[m/s]'),sprintf('Sv/\n[m/s]')};       
    end
else
    if strcmp(areaWeighted,'normByArea')
        cunits = {sprintf('%s','Sv N$^{-1}$'),...
                  sprintf('%s','Sv N$^{-1}$'),...
                  sprintf('%s','Sv W$^{-1}$'),...
                  sprintf('%s','Sv Sv$^{-1}$')};
                  %sprintf('%s','Sv (m$^3$ s$^{-1}$)$^{-1}$')};
    else    
        cunits = {sprintf('%s','Sv (N m$^{-2}$)$^{-1}$'),...
                  sprintf('%s','Sv (N m$^{-2}$)$^{-1}$'),...
                  sprintf('%s','Sv (W m$^{-2}$)$^{-1}$'),...
                  sprintf('%s','Sv (Sv m$^{-2}$)$^{-1}$')};
                  %sprintf('%s','Sv (m s$^{-1}$)$^{-1}$')};
    end
    adjField{3}='oceHeat';
    adjField{4}='fwf';
end


% Only looking at fluxes
for i=1:Nadj

    if ~isempty(strfind(adjField{i},'tau'))
      % tf - 1mo, 3mo, 1yr
      ind = [240 238 229];  
    else
      % tf - 1mo, 1.5yr, 10yr
      ind = [240 223 121];
    end

    matFile=sprintf('%s/adj_%s',matDir,adjField{i});
    load(matFile,'adxx');

    % --- Do the area weighting if desired
    if strcmp(areaWeighted,'normByArea')

        fprintf('normalizing by area ...\n');

        if ~isempty(strfind(adjField{i},'tauu'))
            adxx = adxx ./ mk3D(mygrid.RAW,adxx);
        elseif ~isempty(strfind(adjField{i},'tauv'))
            adxx = adxx ./ mk3D(mygrid.RAS,adxx);
        else
            adxx = adxx ./ mk3D(mygrid.RAC,adxx);
        end
    end

    % --- Set FWF to be Sv / Sv units
    if strcmp(adjField{i},'fwf')
        adxx = adxx.*10^6;
        if strcmp(areaWeighted,'normByArea')
            fprintf(' --- Factoring FWF sensitivity by %s to be Sv/Sv --- \n','10$^6$');
        else
            fprintf(' --- Factoring FWF sensitivity by %s to be Sv/(Sv/m^2) --- \n','10$^6$');
        end
    end
       
 
    for n = 1:length(ind)


        % --- Only do colorbar on bottom plot (vertical stack)
        if n~=3 % || i~=4
          colorbarFlag=0;
        else
          colorbarFlag=2;
        end

        figName=sprintf('%s/adj_%s_%dmo',figDir,adjField{i},ind(n));
        if strcmp(areaWeighted,'normByArea')

            figName = [figName '_normedByArea'];
        end


        % --- Colorbrewer2 colormap
        ncolors=7;
        cmap = cbrewer2('RdBu',ncolors);
        cmap = flipdim(cmap,1);

        % --- Ridiculous options structures...
        timeDir = -1;
        strs = struct(...
            'clbl',cunits{i},...
            'vidName',figName);
        opts = struct(...
            'saveVideo',1,...
            'logFld',1,...
            'mmapOpt',5,...
            'cmap',cmap,...
            'colorbarFlag',colorbarFlag,...
            'fontSize',19);

        % --- Saturate colorbar, removing ~10 points from plots ...
        if strcmp(adjField{i},'oceHeat') && strcmp(areaWeighted,'normByArea')
            c_lim=10^-16;
            opts.c_lim=deal(c_lim);
        elseif strcmp(adjField{i},'fwf') 
            if strcmp(areaWeighted,'normByArea')
                c_lim=10^-1;
                opts.c_lim=deal(c_lim);
            else
                c_lim=10^9;
                opts.c_lim=deal(c_lim);
            end
        end


        if n==1 
            % Let plotVideo compute caxMax and c_lim for the first map,
            [c_lim]=plotVideo(adxx(:,:,ind(n)),strs,opts,mygrid,timeDir);
        else
            % Then use these values for the others
            opts.c_lim=deal(c_lim);
            plotVideo(adxx(:,:,ind(n)),strs,opts,mygrid,timeDir);
        end
    end
end
end
