function [] = plotPartitionTest( mygrid )
% Plot regional results from reconstruction test

matfile = 'mat/reconstruct.34S/flux_region_rec.mat';
matfile2= 'mat/reconstruct.34S/basin_rec.mat';
mainPlotDir = 'figures/reconstruct.34S/region-rec/';
if ~exist(mainPlotDir,'dir'), mkdir(mainPlotDir); end;

load(matfile,'regionRec','msks');
if ~exist(matfile2,'file'), calcPartitionTest(mygrid); end;
load(matfile2)
adjField = set_adjField( 'flux' );

mskNames = fieldnames(msks);
Nt=240;
%tMem = [1,2,3,4,6,12,24,60,120,180,229];
tMem=[3,12,24,36,48,60,72,84,229];
t=[1992+1/12:1/12:2012];

for i=4:length(adjField)

  plotDir=[mainPlotDir adjField{i} '/'];
  if ~exist(plotDir,'dir'),mkdir(plotDir);end;

  %% plot all basins, without Ekman
  if 0
  ab=cell(length(tMem));
  for k=length(tMem):length(tMem)
    figureW;ab{k}=gcf;hold on;
    for j=1:length(mskNames)-1
      
      if strcmp(mskNames{j},'arc')
        basinRec{i,j}(tMem(k),:)=basinRec{i,j}(tMem(k),:)+regionRec.bar{i}(tMem(k),:);
      end;
      if ~strcmp(mskNames{j},'bar')
        plot(t,basinRec{i,j}(tMem(k),:))
      end
    end
    plot(t,allButEkRec{i}(tMem(k),:))
    figure(ab{k})
    title(sprintf('All Basins, %s_{mem}=%d','\tau',tMem(k)))
    xlabel('Years')
    ylabel('Sv')
    legend('ACC','Arctic','Indian','Pacific','Atlantic','All But Ek','Location','Best')
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on
    saveas(gcf,sprintf('%sbasinReconstruct_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------
    
  %% Plot Tropics
  if 0
  tf=cell(length(tMem));
  for k=length(tMem):length(tMem)
    figureW;tf{k}=gcf;hold on;
    plot(t,tropicRec{i}(tMem(k),:),...
         t,regionRec.pac.Ntrop{i}(tMem(k),:)+regionRec.pac.Strop{i}(tMem(k),:),...
         t,regionRec.atl.Ntrop{i}(tMem(k),:)+regionRec.atl.Strop{i}(tMem(k),:),...
         t,regionRec.ind.Ntrop{i}(tMem(k),:)+regionRec.ind.Strop{i}(tMem(k),:))
    title(sprintf('Tropics, %s_{mem}=%d','\tau',tMem(k)))
    xlabel('Years')
    ylabel('Sv')
    legend('All Tropics','Pacific','Atlantic','Indian')
   % legend('All Tropics','Pacific N','Pacific S','Atlantic N',...
   %        'Atlantic S','Indian N','Indian S')
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on

    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%stropicsReconstruct_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------
    
  %% ENSO Plot
  %load('mat/enso_mei.mat','ensoInd');
  %ensoInd = reshape(ensoInd(:,2:end)',[1 Nt]);
  if 0
  if strcmp(adjField{i},'tauu') || strcmp(adjField{i},'sflux')
  load('mat/enso_34.mat','ensoInd');
  en=cell(length(tMem));
  for k=length(tMem):length(tMem)
    figureW;en{k}=gcf;hold on;
    %dJ_enso=regionRec.pac.enso34{i}(tMem(k),:);
    dJ_enso=regionRec.pac.Ntrop{i}(tMem(k),:) + regionRec.pac.Strop{i}(tMem(k),:);

    ind92=145;
    if strcmp(adjField{i},'sflux'), tauLag= 1*12;
    else tauLag= 0*12; end;
    ensoInd=ensoInd(ind92-tauLag:ind92-tauLag+Nt-1);

    pp=polyfit(ensoInd,dJ_enso,1);
    ensoFit=polyval(pp,ensoInd);
    r_squared=corrcoef(ensoInd,dJ_enso);

    subplot(2,1,1),
    [ax,h1,h2]=plotyy(...
        t,dJ_enso,...
        t,ensoInd);
    xlabel('Years')
    set(get(ax(1),'ylabel'),'string','Sv')
    set(ax(1),'ytick',[-1:.2:1]);
    set(ax(1),'YColor',[0 0 0]);
    set(get(ax(2),'ylabel'),'string','ENSO Index')
    set(ax(2),'ytick',[-5:1:5]);
    set(ax(2),'YColor',[0 0 0]);
    legend('\deltaJ_{\tau_u, Tropical Pacific}','ENSO 3.4 Index','Location','Best')
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on
    subplot(2,1,2),
    plot(ensoInd,dJ_enso,'.',...
         ensoInd,ensoFit,'k--')
    xlabel('ENSO 3.4 Index')
    ylabel('\deltaJ_{\tau_u, Tropical Pacific}')
    legend('Data',sprintf('Linear trend, r^2 = %1.2f',r_squared(1,2)),'Location','Best');
    saveas(gcf,sprintf('%senso_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
  end
% ------------------------------------------------------------------
    
  %% Plot North Subtropics, without ekman
  if 0
  stf=cell(length(tMem));
  for k=length(tMem):length(tMem)

    nstrop = regionRec.pac.NStrop{i}(tMem(k),:) + ...
             regionRec.atl.NStrop{i}(tMem(k),:) + ...
             regionRec.ind.NStrop{i}(tMem(k),:);

    figureW;stf{k}=gcf;hold on;
    plot(t,nstrop,...
         t,regionRec.pac.NStrop{i}(tMem(k),:),...
         t,regionRec.atl.NStrop{i}(tMem(k),:),...
         t,regionRec.ind.NStrop{i}(tMem(k),:))
    figure(stf{k})
    title(sprintf('North Sub Tropics, %s_{mem}=%d','\tau',tMem(k)))
    xlabel('Years')
    ylabel('Sv')
    legend('All North Sub Tropics','Pacific','Atlantic','Indian');
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%snorthSubTropicsReconstruct_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------


  %% Plot South Subtropics, without ekman
  if 0
  stf=cell(length(tMem));
  for k=length(tMem):length(tMem)
    if strcmp(adjField{i},'tauu')
        sstrop=regionRec.atl.SStrop{i}(tMem(k),:);
    else
        sstrop=regionRec.atl.SStrop{i}(tMem(k),:)+regionRec.atl.ek{i}(tMem(k),:);
    end

    totsstrop = regionRec.pac.SStrop{i}(tMem(k),:) + ...
                sstrop + ...
             regionRec.ind.SStrop{i}(tMem(k),:);

    figureW;stf{k}=gcf;hold on;
    plot(t,totsstrop,...
         t,regionRec.pac.SStrop{i}(tMem(k),:),...
         t,sstrop,...
         t,regionRec.ind.SStrop{i}(tMem(k),:))
    figure(stf{k})
    title(sprintf('South Sub Tropics, %s_{mem}=%d','\tau',tMem(k)))
    xlabel('Years')
    ylabel('Sv')
    legend('All South Sub Tropics','Pacific','Atlantic','Indian');
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%ssouthSubTropicsReconstruct_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------

  %% Plot both subtropical gyres 
  if 0
  stf=cell(length(tMem));
  for k=length(tMem):length(tMem)

    figureW;stf{k}=gcf;hold on;
    plot(t,nstrop,t,sstrop,t,nstrop+sstrop)
    figure(stf{k})
    title(sprintf('Total Sub Tropics, %s_{mem}=%d','\tau',tMem(k)))
    xlabel('Years')
    ylabel('Sv')
    legend('North Sub Tropics','South Sub tropics','All subtropics');
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%stotSubTropicsReconstruct_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------

  %% Plot Southern Ocean, 
  if 1
  so=cell(length(tMem));
  for k=1:length(tMem)
    figureW;so{k}=gcf;hold on;

    freq = (0:Nt/2-1)'/240 * 12; % Frequency in 1/yr
    ff_south = fft(southRec{i}(tMem(k),:)+regionRec.acc{i}(tMem(k),:),Nt);
    ff_south=oneSidedFFT(ff_south);


    subplot(2,1,1),
    plot(t,southRec{i}(tMem(k),:)+regionRec.acc{i}(tMem(k),:),...
         t,regionRec.acc{i}(tMem(k),:),...
         t,regionRec.pac.south{i}(tMem(k),:),...
         t,regionRec.atl.south{i}(tMem(k),:),...
         t,regionRec.ind.south{i}(tMem(k),:))
    title(sprintf('Southern Ocean, %s_{mem}=%d','\tau',tMem(k)))
    xlabel('Years')
    ylabel('Sv')
    legend('All Southern','ACC','Pacific','Atlantic','Indian','Location','BestOutside')
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on

    subplot(2,1,2),
    semilogx(freq,abs(ff_south))
    grid on
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%ssouthReconstruct_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------

  %% Plot subpolar , 
  if 0
  so=cell(length(tMem));
  for k=length(tMem):length(tMem)
    figureW;so{k}=gcf;hold on;

    freq = (0:Nt/2-1)'/240 * 12; % Frequency in 1/yr
    ff_sp = fft(regionRec.atl.subPolar{i}(tMem(k),:),Nt);
    ff_sp=oneSidedFFT(ff_sp);

    subplot(2,1,1)
    plot(t,regionRec.pac.subPolar{i}(tMem(k),:),...
         t,regionRec.atl.subPolar{i}(tMem(k),:),...
         t,regionRec.arc{i}(tMem(k),:));
    title(sprintf('N. Subpolar + Arctic, %s_{mem}=%d','\tau',tMem(k)))
    xlabel('Years')
    ylabel('Sv')
    legend('Pacific','Atlantic','Arctic')
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on

    subplot(2,1,2),
    semilogx(freq,abs(ff_sp))
    grid on

    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%ssubpolarReconstruct_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------
  %% Plot SAM Index, 
  if 0
  load('mat/sam_index.mat','samInd');
  samInd=reshape(samInd(:,2:end)',[1 240]);
  sa=cell(length(tMem));
  for k=length(tMem):length(tMem)
    figureW;sa{k}=gcf;hold on;

    % Linear fit
    pp=polyfit(samInd,southRec{i}(tMem(k),:),1);
    samFit=polyval(pp,samInd);
    r_squared=corrcoef(samInd,southRec{i}(tMem(k),:));

    subplot(2,1,1),
    [ax,h1,h2]=plotyy(t,southRec{i}(tMem(k),:),...
         t,samInd);
    figure(sa{k})
    xlabel('Years')
    set(get(ax(1),'ylabel'),'string','Sv')
    set(ax(1),'ytick',[-1.2:.2:1.2]);
    set(ax(1),'ylim',[-1.2 1.2]);
    set(ax(1),'YColor',[0 0 0]);
    set(get(ax(2),'ylabel'),'string','SAM Index')
    set(ax(2),'ytick',[-6:1:6]);
    set(ax(2),'ylim',[-6 6]);
    set(ax(2),'YColor',[0 0 0]);
    legend('All Southern','SAM Index')
    set(gca,'xtick',[1992:2:2012])
    xlim([1992 2012])
    grid on

    subplot(2,1,2),
    plot(samInd,southRec{i}(tMem(k),:),'.',...
         samInd,samFit)
    xlabel('SAM Index')
    ylabel('\deltaJ_{\tau_u, Southern Ocean}')
    legend('Data',sprintf('Linear trend, r^2 = %1.2f',r_squared(1,2)),'Location','Best');
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%ssam_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------

  %% Plot NAO Index, 
  if strcmp(adjField{i},'tauu')
  fullFreq=(0:Nt-1)'/240 * 12;
  freq = (0:Nt/2-1)'/240 * 12; % Frequency in 1/yr
  load('mat/nao_index_ncar.mat','naoInd');
  for k=length(tMem):length(tMem)

    ind92=265;
    tauLag= 0*12;
    naoInd=naoInd(ind92-tauLag:ind92-tauLag+Nt-1);
    dJ_rec=regionRec.atl.subPolar{i}(tMem(k),:);

    %% Compute low pass filter
    cutoffFreq = 4;
   % dJ_rec=lowPassFilter(fullFreq,dJ_rec,cutoffFreq);
   % naoInd=lowPassFilter(fullFreq,naoInd,cutoffFreq);

    %% Try moving mean
    mm=3;
    aa=1;
    bb=(1/mm)*ones(1,mm);
   % dJ_rec=filter(bb,aa,dJ_rec);
   % naoInd=filter(bb,aa,naoInd);

    %% Compute linear fit
    pp=polyfit(naoInd,dJ_rec,1);
    naoFit=polyval(pp,naoInd);
    r_squared=corrcoef(naoInd,dJ_rec);

    figureW;nao{k}=gcf;hold on;
    subplot(2,1,1),
    [ax,h1,h2]=plotyy(t,dJ_rec,...
         t,naoInd);
    figure(nao{k})
    set(get(ax(1),'ylabel'),'string','Sv')
    %set(ax(1),'ytick',[-1.2:.2:1.2]);
    %set(ax(1),'ylim',[-1.2 1.2]);
    set(ax(1),'YColor',[0 0 0]);
    set(get(ax(2),'ylabel'),'string','NAO Index')
    %set(ax(2),'ytick',[-6:1:6]);
    %set(ax(2),'ylim',[-6 6]);
    set(ax(2),'YColor',[0 0 0]);
    legend('\deltaJ_{\tau_u, N. Atl. Subpolar}','NAO Index')

    grid on

    subplot(2,1,2),
    plot(naoInd,dJ_rec,'.',...
         naoInd,naoFit)
    xlabel('NAO Index')
    ylabel('\deltaJ_{\tau_u, N. Atl. Subpolar}')
    legend('Data',sprintf('Linear trend, r^2 = %1.2f',r_squared(1,2)),'Location','Best');

    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])
    saveas(gcf,sprintf('%snao_tMem_%dmo',plotDir,tMem(k)),'pdf');
  end
  end
% ------------------------------------------------------------------

  %% Plot All but Ekman for tauu
  freq = (0:Nt/2-1)'/240 * 12; % Frequency in 1/yr
  abe=cell(length(tMem));
  if strcmp(adjField{i},'tauu')
    for k=length(tMem):length(tMem)

    ffabe=fft(allButEkRec{i}(tMem(k),:),Nt);
    freq_abe=oneSidedFFT(ffabe);
    p0=max(abs(freq_abe))+.1;
    db_amp = 10*log10(abs(freq_abe)/p0);

    figureW;abe{k}=gcf;hold on;
    subplot(2,1,1),plot(t,allButEkRec{i}(tMem(k),:))
      xlabel('Years')
      ylabel('Sv')
      legend(sprintf('%sJ_{%s}(t)-Ekman','\delta',adjField{i}))
      set(gca,'xtick',[1992:2:2012])
      xlim([1992 2012])
      grid on
    subplot(2,1,2),semilogx(freq,abs(freq_abe))
      xlabel('Year^{-1}')
      ylabel('Amplitude (dB)')
      grid on
    set(abe{k},'paperorientation','landscape')
    set(abe{k},'paperunits','normalized')
    set(abe{k},'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%sallButEkReconstruct_tMem_%dmo',plotDir,tMem(k)),'pdf');
    end
  end


  if 0 %strcmp(adjField{i},'tauu')
    load('mat/reconstruct.34S/monthly_flux/fullReconstruction.mat','samoc_for_mean');

    ind=229;
    eps=samoc_for_mean - regionRec.atl.ek{i}(ind,:);
    freq_eps=fft(eps,Nt);
    freq_eqs=oneSidedFFT(freq_eps);

    figureW;
    subplot(2,1,1),plot(t,eps)
      xlabel('Years')
      ylabel('Sv')
      legend(sprintf('%sJ_{Fwd}(t)-%sJ_{Ekman}','\delta','\delta'))
      set(gca,'xtick',[1992:2:2012])
      xlim([1992 2012])
      grid on
    subplot(2,1,2),semilogx(freq,abs(freq_eps))
      xlabel('Year^{-1}')
      ylabel('Amplitude')
      grid on
    set(abe{k},'paperorientation','landscape')
    set(abe{k},'paperunits','normalized')
    set(abe{k},'paperposition',[0 0 1 1])

    saveas(gcf,sprintf('%sfwd_minus_ek',plotDir),'pdf')
  end

  keyboard
end
