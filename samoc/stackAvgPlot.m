function [] = stackExfPlot( forceVar, modelVar, atmVar, mygrid, varargin )
% Make similar plot to reconstruction but with external forcing or adxx
%
% Inputs:
%       
%       forceVar : e.g. tauu, tauv, hflux ... 
%       modelVar : 'eccov4r2', ...
%       atmVar : 'long/short-atm' or 'flux'
%       mygrid
%
% Optional inputs
%       
%       varSelect: climatology 'clim' or annual averaging 'annual' or (default) 'both'
%       separator: split up southern ocean or ekman area 
%       exfOrAdxx: choose exf or adxx fields
%
% ------------------------------------------------------------------------------------

%% --- Parse inputs 
% First set variable argument defaults
varSelect='both';
separator='none';
exfOrAdxx='exf';

% Now check user input
for n=1:length(varargin)
  if isa(varargin{n},'char')
    if strcmp(varargin{n},'clim') || strcmp(varargin{n},'annual')  
      varSelect=varargin{n};
    elseif strcmp(varargin{n},'exf') || strcmp(varargin{n},'adxx')
      exfOrAdxx=varargin{n};
    else
      separator=varargin{n};
    end
  else 
    fprintf('Didn''t recognize varargin at position: %d ...\n',n);
  end
end


if strcmp(exfOrAdxx,'exf')
  [~,~,saveDir,plotDir] = samoc_dirs('exf-plot',modelVar,atmVar,forceVar);
  matfile=sprintf('%s/../region_exf.mat',saveDir);
else
  matfile = 'mat/dec.240mo/region_adxx.mat';
  mainPlotDir = 'figures/dec.240mo/';
end
load(matfile,'regionFld','msks');

mskNames = fieldnames(msks);
Nt=length(regionFld.globe{1});
t=[1992+1/12:1/12:2012];
tMem=int32(1);
numSeparate=8;

% --- Based on forceVar, create vars and loop limits
if strcmp(forceVar,'tauu') || strcmp(forceVar,'taux')
  flux_ind=1;
elseif strcmp(forceVar,'tauv') || strcmp(forceVar,'tauy')
  flux_ind=2;
elseif strcmp(forceVar,'wind')
  flux_ind=1:2;
elseif strcmp(forceVar,'hflux') || strcmp(forceVar,'qnet')
  flux_ind=3;
elseif strcmp(forceVar,'sflux') || strcmp(forceVar,'empmr')
  flux_ind=4;
elseif strcmp(forceVar,'fwf')
  flux_ind=4;
  fprintf('\n\n ** Note: this is actually the salt flux, i.e. empmr ** \n\n\n');
elseif strcmp(forceVar,'buoyancy')
  flux_ind=3:4;
elseif strcmp(forceVar,'atemp')
  flux_ind=5;
elseif strcmp(forceVar,'aqh')
  flux_ind=6;
elseif strcmp(forceVar,'lwdn') || strcmp(forceVar,'lwdown')
  flux_ind=7;
elseif strcmp(forceVar,'swdn') || strcmp(forceVar,'swdown')
  flux_ind=8;
elseif strcmp(forceVar,'evap')
  if strcmp(exfOrAdxx,'exf')
    flux_ind=9;
  else
    error('No evaporation in adxx fields ...')
  end
elseif strcmp(forceVar,'preci') || strcmp(forceVar,'precip')
  if strcmp(exfOrAdxx,'exf')
    flux_ind=10;
  else
    flux_ind=9;
  end
elseif strcmp(forceVar,'roff') || strcmp(forceVar,'runoff')
  if strcmp(exfOrAdxx,'exf')
    flux_ind=11;
  else
    flux_ind=10;
  end
else 
  error('forceVar option not recognized, exiting ... ')
  return;
end


% --- Group regional attribution
if find(flux_ind,1)
  separator_clim=[separator 'ek'];
else
  separator_clim=[separator 'none'];
end
all_ek = grabSamocGroups(separator_clim,tMem,flux_ind,regionFld);
all_ekp=all_ek; all_ekn=all_ek;
all_ekp(all_ek<0)=0; all_ekn(all_ek>=0)=0;

all = grabSamocGroups(separator,tMem,flux_ind,regionFld);
allp=all; alln=all;
allp(all<0)=0; alln(all>=0)=0;

fullFld=zeros(1,Nt);
for i=flux_ind(1):flux_ind(end)
  fullFld=fullFld+regionFld.globe{i}(tMem,:);
end

if strcmp(exfOrAdxx,'exf')
  % --- Compute Averages
  [fld_clim,region_clim_p,region_clim_n,region_clim] = avgFields(all_ek,fullFld,'clim',20);
  [fld_annual,region_annual_p,region_annual_n,region_annual] = avgFields(all,fullFld,'annual',20);
  
  % --- Compute top contributors
  [cond_clim,indS_clim,indEtc_clim] = grabTopRegions(region_clim,numSeparate);
  cond_clim_p=cond_clim; cond_clim_p(cond_clim<0)=0;
  cond_clim_n=cond_clim; cond_clim_n(cond_clim>=0)=0;
  [cond_annual,indS_annual,indEtc_annual] = grabTopRegions(region_annual,numSeparate);
  cond_annual_p=cond_annual; cond_annual_p(cond_annual<0)=0;
  cond_annual_n=cond_annual; cond_annual_n(cond_annual>=0)=0;

  % --- Plots
  if strfind(varSelect,'clim')
    makeStackPlot(1:12,fld_clim,cond_clim_p',cond_clim_n',sprintf('%s/clim_condensed.pdf',plotDir),forceVar,indS_clim,separator_clim,mygrid);
  elseif strfind(varSelect,'annual')
    makeStackPlot(1992:2011,fld_annual,cond_annual_p',cond_annual_n',sprintf('%s/annual_condensed.pdf',plotDir),forceVar,indS_annual,separator,mygrid);
  elseif strfind(varSelect,'both')
    makeStackPlot(1:12,fld_clim,cond_clim_p',cond_clim_n',sprintf('%s/clim_condensed.pdf',plotDir),forceVar,indS_clim,separator_clim,mygrid);
    makeStackPlot(1992:2011,fld_annual,cond_annual_p',cond_annual_n',sprintf('%s/annual_condensed.pdf',plotDir),forceVar,indS_annual,separator,mygrid);
  else
    fprintf('Didn''t recognize varSelect: %s \n',varSelect);
  end
else
  
  % --- Grab top contributors
  [cond_all,indS_all,indEtc_all] = grabTopRegions(all,numSeparate);
  cond_all_p=cond_all; cond_all_p(cond_all<0)=0;
  cond_all_n=cond_all; cond_all_n(cond_all>=0)=0;
 
  % --- Plot
  makeStackPlot(1:Nt,fullFld,cond_all_p',cond_all_n',sprintf('%s/all_condensed.pdf',plotDir),forceVar,indS_all,separator,mygrid);
end

end

% ---------------------------------------------------------------------
function [] = makeStackPlot(t,rec,pos_data,neg_data,savename,forceVar,ind,separator,mygrid)

fullLegend={'Arctic','Pacific Subpolar',...
            'Pacific N. Subtropics','Pacific Tropics','Pacific S. Subtropics',...
            'Atlantic Subpolar','Atlantic N. Subtropics','Atlantic Tropics'}; ...

if strfind(separator,'ek')
  fullLegend=cat(2,fullLegend,{'Atlantic S. Subtropics','30-40^{\circ}S'});
else
  fullLegend=cat(2,fullLegend,{'Atlantic S. Subtropics'});
end

fullLegend=cat(2,fullLegend,...
           {'Indian Tropics','Indian S. Subtropics'});

if strfind(separator,'south')
  fullLegend=cat(2,fullLegend,{'Pacific South','Atlantic South','Indian South','ACC (<60S)'});
else
  fullLegend=cat(2,fullLegend,{'Southern Ocean'});
end

Nregion=size(pos_data,2);
legendStr=cell(Nregion+1,1);

% --- First legend entry for full reconstruction
if strcmp(forceVar,'tauu') || strcmp(forceVar,'taux')
  legendStr{1}='Zonal Wind Stress';
elseif strcmp(forceVar,'tauv') || strcmp(forceVar,'tauy')
  legendStr{1}='Meridional Wind Stress';
elseif strcmp(forceVar,'wind')
  legendStr{1}='Wind Stress';
elseif strcmp(forceVar,'hflux') || strcmp(forceVar,'qnet')
  legendStr{1}='Heat Flux';
elseif strcmp(forceVar,'sflux') || strcmp(forceVar,'fwf')
  legendStr{1}='EmPmR';
elseif strcmp(forceVar,'buoyancy')
  legendStr{1}='Buoyancy';
elseif strcmp(forceVar,'all')
  legendStr{1}='All Atm. Fluxes';
elseif strcmp(forceVar,'atemp')
  legendStr{1}='Air Temperature';
elseif strcmp(forceVar,'aqh')
  legendStr{1}='Air Humidity';
elseif strcmp(forceVar,'lwdn') || strcmp(forceVar,'lwdown')
  legendStr{1}='Long Wave Down                            ';
elseif strcmp(forceVar,'swdn') || strcmp(forceVar,'swdown')
  legendStr{1}='Short Wave Down';
elseif strcmp(forceVar,'evap')
  legendStr{1}='Evaporation';
elseif strcmp(forceVar,'preci') || strcmp(forceVar,'precip')
  legendStr{1}='Precipitation';
elseif strcmp(forceVar,'roff') || strcmp(forceVar,'runoff')
  legendStr{1}='Runoff';
else 
  error('forceVar option not recognized, exiting ... ')
  return;
end

% --- fill legend string
if nargin<7 && Nregion==length(fullLegend);
  for i=1:length(fullLegend)
    legendStr{i+1}=fullLegend{i};
  end
else
  for i=1:length(ind)
    legendStr{i+1}=fullLegend{ind(i)};
  end
  legendStr{end}='Other';
end

% --- Set colorbar
cc=parula(Nregion);

figureH;
if exist('ind','var')
  ax1=subplot(3,2,1:4);
end
hold on
plot(t,rec,'r.-','markersize',28);
bar(t,pos_data,'stacked')
lh=legend(legendStr,'fontsize',18);

if exist('ind','var')
  % Set colormap to not be white
  colormap(ax1,cc);
 
  % Move legend to empty subplot
  sh=subplot(3,2,5);
  sp=get(sh,'position');
  set(lh,'position',sp);
  delete(sh);
else
  legend(legendStr,'Location','EastOutside','fontsize',18)
end

if exist('ind','var')
  ax2=subplot(3,2,1:4);
  colormap(ax2,cc);
end
bar(t,neg_data,'stacked')
  ylabel('Sv')
  if length(t)==20
    xlim([1991 2012])
  elseif length(t)==12
    xlim([0.5 12.5])
    set(gca,'xtick',[1:12],...
        'xticklabel',{'Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'});
  elseif length(t)==241
    xlim([227.5 241.5])
  end
  grid on
colormap(cc(1:Nregion,:))
plot(t,rec,'r.-','markersize',28);%'markeredgecolor',cc(2,:),'markerfacecolor',cc(2,:))

if exist('ind','var')
  plotSelectSamocMasks(ind,separator)
end

set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,savename,'pdf')

end

% ---------------------------------------------------------------------

function [rec_avg,region_avg_p,region_avg_n,region_avg] = avgFields(regionFld,fullFld,avgType,Nyrs)

if strcmp(avgType,'clim')
  rec_avg = calcClimatology(fullFld,Nyrs);
  region_avg = calcClimatology(regionFld,Nyrs);
elseif strcmp(avgType,'annual')
  rec_avg = calcAnnualMean(fullFld,Nyrs);
  region_avg = calcAnnualMean(regionFld,Nyrs);
else
  error('Option not recognized, please specify ''clim'' for climatology or ''annual'' for annual averages')
  return;
end 

region_avg_p = region_avg; region_avg_n = region_avg;
region_avg_p(region_avg<0)=0;
region_avg_n(region_avg>=0)=0;


end

function [condensed_list, indSeparate, indEtc] = grabTopRegions(regionFld,numSeparate)
% grab numSeparate regions from full regionFld matrix

% --- Grab sum of squares along 'time' dimension (columns) and sort
sosFld = sum(abs(regionFld),2);
%sosFld = max(abs(regionFld),[],2);

[sorted_sosFld, ind] = sort(sosFld);

% want max to min,
ind = ind(end:-1:1);

condensed_list = zeros(numSeparate+1,size(regionFld,2));

for i=1:numSeparate
  condensed_list(i,:) = regionFld(ind(i),:);
end

for i=numSeparate+1:size(regionFld,1)
  condensed_list(numSeparate+1,:) = condensed_list(numSeparate+1,:)+regionFld(ind(i),:);
end

indSeparate=ind(1:numSeparate);
indEtc = ind(numSeparate+1:end);
end
