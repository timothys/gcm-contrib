function [] = plotSamocReconstruction2D(tau_mem,modelVar,atmVar,lat)
% plot reconstruction of AMOC at 34S from adjoint sensitivities
%
%       tau_mem: memory in reconstruction
%       modelVar: e.g. 'eccov4r2'
%	atmVar: 'flux' or 'long-atm' etc. select to reconstruct with atmospheric state or 
%		 computed fluxes. 
%	lat: denotes latitude for computing reconstruction
%
% -------------------------------------------------------------------------

%% --- Load global grid variable
mygrid = establish_mygrid('llc90');

adjField = set_adjField( atmVar );

% --- 0. Set up directories     
[loadDir,plotDir] = samoc_dirs('reconstruct-plot',modelVar,atmVar,sprintf('2D_tau_%dmo',tau_mem));
load(sprintf('%s/fullReconstruction.mat',loadDir),'samoc_adjFields');

% --- Compute climatology
cscale=[.001, .001, .0001, .0001];
clim = cell(4,1);
for i=1:4
  clim{i} = calcClimatology(samoc_adjFields{i},20);
  manyGlobePlotter(clim{i},3,4,sprintf('%s/%s_clim.pdf',plotDir,adjField{i}),cscale(i));
end

keyboard
close all;
clear clim;

% --- Compute annual avg
annual = cell(4,1);
for i=1:4
  annual{i} = calcAnnualMean(samoc_adjFields{i},20);
  manyGlobePlotter(annual{i},4,5,sprintf('%s/%s_annual.pdf',plotDir,adjField{i}),cscale(i));
end
end

function [] = manyGlobePlotter(fld,numRows,numCols,saveName,cscale)

Nt=size(fld.f1,3);

months={'Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'};
yrs=1992:2011;

figureH;
[ha,pos]=tight_subplot(numRows,numCols,[.02 .02],[.02 .02],[.02 .02]);
for i=1:Nt
%subplot(numRows,numCols,i)
axes(ha(i));
m_map_atl(fld(:,:,i),5)
colormap(redblue)
caxis([-cscale, cscale])

if Nt==12
  xlabel(sprintf('%s Clim.',months{i}));
else
  xlabel(sprintf('%d Avg.',yrs(i)))
end
end

set(gcf,'paperorientation','landscape')
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0 1 1])
saveas(gcf,saveName,'pdf');
end
