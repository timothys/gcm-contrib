function [] = calcPartitionTest( mygrid )
% Compute basinwide partitions

matfile = 'mat/reconstruct.34S/region_rec.mat';
load(matfile,'regionRec','msks');
adjFields = set_adjField('flux');

mskNames = fieldnames(msks);
Nt=240;
leggy = cell(0,0); % append this baby as we go
allDat = cell(0,0);

basinRec = cell(length(adjFields),length(mskNames));
tropicRec = cell(length(adjFields),1);
nTropicRec = cell(length(adjFields),1);
sTropicRec = cell(length(adjFields),1);
subTropicRec = cell(length(adjFields),1);
nSubTropicRec = cell(length(adjFields),1);
sSubTropicRec = cell(length(adjFields),1);
southRec = cell(length(adjFields),1);
allButEkRec = cell(length(adjFields),1);

% ------------------------------------------------------------------

for i=1:length(adjFields)

  n=1;
  tropicRec{i}=zeros(Nt,Nt);
  subTropicRec{i}=zeros(Nt,Nt);
  southRec{i}=zeros(Nt,Nt);
  allButEkRec{i} = zeros(Nt,Nt);
  nTropicRec{i}=tropicRec{i};
  sTropicRec{i}=tropicRec{i};
  nSubTropicRec{i}=tropicRec{i};
  sSubTropicRec{i}=tropicRec{i};

  for j=1:length(mskNames)
  
    basinRec{i,j} = zeros(Nt,Nt);

    subNames = fieldnames(getfield(msks,mskNames{j}));

    if strcmp(subNames{1},'nFaces')

      allDat{n} = getfield(regionRec,mskNames{j});
      leggy{n}=mskNames{j};
      basinRec{i,j} = allDat{n}{i};
      allButEkRec{i} = allButEkRec{i}+allDat{n}{i};

      n=n+1;

    else

      for k = 1:length(subNames)

        allDat{n} = getfield(getfield(regionRec,mskNames{j}),subNames{k});
        leggy{n}=[mskNames{j} ' ' subNames{k}];
        if ~strcmp(subNames{k},'ek')
          basinRec{i,j}=basinRec{i,j}+allDat{n}{i};
          allButEkRec{i}=allButEkRec{i}+allDat{n}{i};
        end

        if strcmp(subNames{k},'Ntrop')
          nTropicRec{i}=nTropicRec{i}+allDat{n}{i};
          tropicRec{i}=tropicRec{i}+allDat{n}{i};
        elseif strcmp(subNames{k},'Strop')
          sTropicRec{i}=sTropicRec{i}+allDat{n}{i};
          tropicRec{i}=tropicRec{i}+allDat{n}{i};
        elseif strcmp(subNames{k},'NStrop') 
          nSubTropicRec{i}=nSubTropicRec{i}+allDat{n}{i};
          subTropicRec{i}=subTropicRec{i}+allDat{n}{i};
        elseif strcmp(subNames{k},'SStrop') 
          sSubTropicRec{i}=sSubTropicRec{i}+allDat{n}{i};
          subTropicRec{i}=subTropicRec{i}+allDat{n}{i};
        elseif strcmp(subNames{k},'south')
          southRec{i} = southRec{i}+allDat{n}{i};
        end

        n=n+1;
      end
    end
  end
end
% ------------------------------------------------------------------

save('mat/reconstruct.34S/basin_rec.mat','basinRec','nTropicRec','nTropicRec','nSubTropicRec',...
        'sSubTropicRec','southRec','allButEkRec','tropicRec','subTropicRec')
end
