function [] = plotNonlin( kLev )
% 
% Plotting script for nonlinear advection and ggl90
% terms in ECCOv4r2 reference run. 
%
% Purpose is to look for why errors build up in hflux reconstruction
% only in the ACC and ''north box'' region around Greenland. 
%
% Input: kLev = level to integrate 3d fields to
% 
% ------------------------------------------------------------------

%% 0. Preliminaries
if ~exist('kLev','var'), kLev=15; end;
mygrid = establish_mygrid('llc90');

diagsDir = '../../results/hflux/ref/diags/';
saveDir = 'mat/nonlin/';
plotDir = 'figures/nonlin/';

if ~exist(saveDir,'dir'), mkdir(saveDir); end;
if ~exist(plotDir,'dir'), mkdir(plotDir); end;

fileList = {'mxl','adv','ggl_2d','ggl_3d'};
fldNames = {'MXLDEPTH','CONVADJ','' '' '' ''; ...
	    'ADVx_TH' 'ADVy_TH' 'ADVr_TH' ''  '' ''; ...
	    'GGL90flx' 'GGL90tau', '' '' '' '';...
	    'GGL90TKE' 'GGL90Lmx' 'GGL90Prl' 'GGL90ArU' 'GGL90ArV' 'GGL90Kr'};
% ------------------------------------------------------------------


%% 1. Load results and save to mat files
for i=1:length(fileList)

  iters = grabAllIters( diagsDir, fileList{i} );
  fprintf('Found %d files titled %s ...\n',length(iters),fileList{i});

  for j=1:size(fldNames,2)
	if strcmp(fileList{i},'adv') || ~isempty(strfind(fileList{i},'3d'))
	  saveFile=sprintf('%s%s_k%d.mat',saveDir,fldNames{i,j},kLev);
	else
	  saveFile=[saveDir fldNames{i,j} '.mat'];
	end
	if ~strcmp(fldNames{i,j},'') && ~exist(saveFile,'file')
	  writeMatFile(saveFile,fldNames{i,j},fileList{i},diagsDir,iters,j,kLev,mygrid);
	end %if fldname != ''
  end % for all flds in file

  fprintf('Done with file: %s ...\n',fileList{i});
end % Loop thru files
% ------------------------------------------------------------------

%% 2. Load mat files and do plotting
for i=1:length(fileList)
  for j=1:size(fldNames,2)

	if ~strcmp(fldNames{i,j},'') 
	  if strcmp(fileList{i},'adv') || ~isempty(strfind(fileList{i},'3d'))
	    mfile=sprintf('%s%s_k%d.mat',saveDir,fldNames{i,j},kLev);
	    plotName=sprintf('%s%s_k%d.mat',plotDir, fldNames{i,j},kLev);
	  else
	    mfile=[saveDir fldNames{i,j} '.mat'];
  	    plotName=[plotDir fldNames{i,j}];
	  end

	  load(mfile,'fld_clim');
	  strs=struct(...
	  	'xlbl',strrep(fldNames{i,j},'_',' '),...
	  	'time','months',...
	  	'vidName',plotName);
	  opts=struct('saveVideo',1);

	  plotVideo(fld_clim,strs,opts,mygrid);
	end
  end
end
end
% ------------------------------------------------------------------
% ------------------------------------------------------------------

function [] = writeMatFile( fileName, fldName, loadFile, loadDir, iters, recInd, kInt, mygrid)

Nmo = 240;
Nyrs= 20;

% Initialize fields for saving
fld=repmat(0*mygrid.XC, [1 1 Nmo]);
fld_clim = fld(:,:,1:12);

for n=1:length(iters)

  % Read 1 file at a time b/c I have 0 RAM
  if ~isempty(strfind(fileName,'ADVx')) || ~isempty(strfind(fileName,'ADVy'))
    tmpx=rdmds2gcmfaces([loadDir loadFile],iters(n),'rec',1);
    tmpy=rdmds2gcmfaces([loadDir loadFile],iters(n),'rec',2);
    [tmpx,tmpy]= calc_UEVNfromUXVY(tmpx,tmpy);
    if ~isempty(strfind(fileName,'ADVx')), tmpFld=tmpx; 
    else tmpFld=tmpy; 
    end
  else
    tmpFld = rdmds2gcmfaces([loadDir loadFile],iters(n),'rec',recInd);
  end

  % Compute index for climatology
  climInd = mod(n,12);
  if climInd==0, climInd=12; end;

  if length(size(tmpFld.f1))<3
  % Time series of 2D field, just read it in and save

      fld(:,:,n) = tmpFld;
      fld_clim(:,:,climInd) = fld_clim(:,:,climInd) + tmpFld/Nyrs;

  elseif length(size(tmpFld.f1))==3
  % Time series of 3D field, integrate past mixed layer and add

      if isempty(strfind(fileName,'_k'))
	fileName = strrep(fileName,'.mat',sprintf('_k%d.mat',kInt));
      end
      if ~exist('DRF','var'), DRF=mk3D(mygrid.DRF,tmpFld); end;	

      if strcmp(fldName,'GGL90Prl') || strcmp(fldName,'GGL90Lmx')
	% Just grab Prandtl number and mixing length scale at level, don't integrate
        fld(:,:,n) = tmpFld(:,:,kInt);
        fld_clim(:,:,climInd) = fld_clim(:,:,climInd) + tmpFld(:,:,kInt)/Nyrs;
      else
        % Vertical integration
	drfsum = convert2gcmfaces(repmat(sum(convert2gcmfaces(DRF(:,:,1:kInt)),3),[1 1 kInt]));
	tmpFld=tmpFld(:,:,1:kInt).*DRF(:,:,1:kInt) ./ drfsum;
        tmpFld=convert2gcmfaces(tmpFld);
        fld(:,:,n) = convert2gcmfaces(nansum(tmpFld,3));
        fld_clim(:,:,climInd) = fld_clim(:,:,climInd) + fld(:,:,n)/Nyrs;
      end

  else

      fprintf('Unrecognized Size! \n File: %s, Fld: %s ... \n',loadFile,fldName)
      for k=1:length(size(tmpFld.f1))
        fprintf('Dim %d length: %d\n',k,size(tmpFld.f1,k));
      end
      fprintf('Skipping this field ...\n');

  end %Size check
end % for 1:length(iters)

fprintf('Done reading field: %s ...\n',fldName);

% Save to mat file
save(fileName,'fld','fld_clim');
fprintf('File: %s written ...\n',fileName);

end
