function [] = compareRecWithCtrls(varargin)
% Load reconstruction with and without added ctrl vector, compare

plotSelect='all';
forceVar='allVars';
doNormalize=0;
for n=1:length(varargin)
  if isa(varargin{n},'char')
    if strcmp(varargin{n},'clim') || strcmp(varargin{n},'annual') || strcmp(varargin{n},'full') ||strcmp(varargin{n},'all')  
      plotSelect=varargin{n};
    elseif ~isempty(strfind(varargin{n},'doNormalize'))
      if ~isempty(strfind(varargin{n},'Mean')) || ~isempty(strfind(varargin{n},'mean'))
        doNormalize=3;
      else
        doNormalize=1;
      end
    else
      forceVar=varargin{n};
    end
  else
    error('Don''t recognize varargin...');
  end
end

if doNormalize==1 && strcmp(plotSelect,'annual')
  doNormalize=2;
end

longEccoDir = 'mat/reconstruct.34S/monthly_long-atm-ecco/';
shortEccoDir = 'mat/reconstruct.34S/monthly_atm/';
longRawDir = 'mat/reconstruct.34S/monthly_long-atm-raw/';
shortRawDir = 'mat/reconstruct.34S/monthly_short-atm-raw/';

load([longEccoDir 'fullReconstruction.mat']);
longEccoRec = samoc_adjFields;

load([shortEccoDir 'fullReconstruction.mat']);
shortEccoRec = samoc_adjFields;

load([longRawDir 'fullReconstruction.mat']);
longRawRec = samoc_adjFields;

load([shortRawDir 'fullReconstruction.mat']);
shortRawRec = samoc_adjFields;

clear samoc_adjFields;

adjField = set_adjField('atm');
Nadj=length(adjField);

% --- Grab variable name

if strcmp(forceVar,'tauu')
  flux_ind=1;
elseif strcmp(forceVar,'tauv')
  flux_ind=2;
elseif strcmp(forceVar,'wind')
  flux_ind=1:2;
elseif strcmp(forceVar,'hflux')
  error('No long record with flux reconstruction')
elseif strcmp(forceVar,'sflux') || strcmp(forceVar,'fwf')
  error('No long record with flux reconstruction')
elseif strcmp(forceVar,'buoyancy')
  error('No long record with flux reconstruction')
elseif strcmp(forceVar,'aqh')
  flux_ind=3;
elseif strcmp(forceVar,'atemp')
  flux_ind=4;
elseif strcmp(forceVar,'swdn') || strcmp(forceVar,'swdown')
  flux_ind=5;
elseif strcmp(forceVar,'lwdn') || strcmp(forceVar,'lwdown')
  flux_ind=6;
elseif strcmp(forceVar,'evap')
  error('No evaporation in adxx fields ...')
elseif strcmp(forceVar,'preci') || strcmp(forceVar,'precip')
  flux_ind=7;
elseif strcmp(forceVar,'roff') || strcmp(forceVar,'runoff')
  flux_ind=8;
elseif strcmp(forceVar,'allVars')
  flux_ind=1:8;
else 
  error('forceVar option not recognized, exiting ... ')
  return;
end

% sml_raw = short minus long, raw EIG reconstructions
% sml_ecco = short minus long, ecco reconstructions
% emr_long = ecco minus raw EIG long 
% emr_short = ecco minus raw short

[lms_raw_clim,lms_raw_annual,lms_raw_full] = diffComp(longRawRec,shortRawRec,flux_ind,doNormalize);
[lms_ecco_clim,lms_ecco_annual,lms_ecco_full] = diffComp(longEccoRec,shortEccoRec,flux_ind,doNormalize);
[emr_long_clim,emr_long_annual,emr_long_full] = diffComp(longEccoRec,longRawRec,flux_ind,doNormalize);
[emr_short_clim,emr_short_annual,emr_short_full] = diffComp(shortEccoRec,shortRawRec,flux_ind,doNormalize);

for i=flux_ind
  if strcmp(plotSelect,'clim') || strcmp(plotSelect,'all')
    diffPlotter(1:12,lms_raw_clim{i},lms_ecco_clim{i},emr_long_clim{i},emr_short_clim{i},adjField{i});
  end
  if strcmp(plotSelect,'annual') || strcmp(plotSelect,'all')
    diffPlotter(1992:2011,lms_raw_annual{i},lms_ecco_annual{i},emr_long_annual{i},emr_short_annual{i},adjField{i});
  end
  if strcmp(plotSelect,'full') || strcmp(plotSelect,'all')
    t=[1:240] ./ 12 + 1992;
    diffPlotter(t,lms_raw_full{i},lms_ecco_full{i},emr_long_full{i},emr_short_full{i},adjField{i});
  end
end

end 

% ---------------------------------------------------------

function [] = diffPlotter(t,fld1,fld2,fld3,fld4,fldName)

tMem=229;

figureW;
plot(t,fld1(tMem,:),t,fld2(tMem,:),t,fld3(tMem,:),t,fld4(tMem,:))
ylabel(fldName)
legend('Long - Short, raw','Long - Short, ECCO','ECCO - raw, long','ECCO - raw, short')

  if length(t)>12
    xlim([1991 2012])
  else
    xlim([0.5 12.5])
    set(gca,'xtick',[1:12],...
        'xticklabel',{'Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'});
  end
end

% ---------------------------------------------------------

function [diff_clim,diff_annual,diff_full] = diffComp(rec1,rec2,flux_ind,doNormalize)

Nadj=length(rec1);

absDiff=cell(Nadj,1);
relDiff=cell(Nadj,1);
ctrl_clim=cell(Nadj,1);
ctrl_annual=cell(Nadj,1);
raw_clim=cell(Nadj,1);
raw_annual=cell(Nadj,1);
diff_clim=cell(Nadj,1);
diff_annual=cell(Nadj,1);

% Get normalizing factor
if doNormalize==1

  load('mat/reconstruct.34S/monthly_atm/fullReconstruction.mat','samoc_for_mean');
  fwdClim=calcClimatology(samoc_for_mean,20);
  normFact=max(fwdClim)-min(fwdClim);
elseif doNormalize==2

  load('mat/reconstruct.34S/monthly_atm/fullReconstruction.mat','samoc_for_mean');
  fwdAnnual=calcAnnualMean(samoc_for_mean,20);
  normFact=max(fwdAnnual)-min(fwdAnnual);
elseif doNormalize==3
  normFact=14.25; %[sv]
else 
  normFact=1;
end

for i=flux_ind

  absDiff{i}=rec1{i}-rec2{i};
  relDiff{i}=absDiff{i}./rec1{i};

  clim1{i} = calcClimatology(rec1{i},20);
  annual1{i} = calcAnnualMean(rec1{i},20);
  clim2{i} = calcClimatology(rec2{i},20);
  annual2{i} = calcAnnualMean(rec2{i},20);

  diff_clim{i} = (clim1{i}-clim2{i}); % ./ raw_clim{i};
  diff_annual{i} = (annual1{i}-annual2{i}); % ./ raw_annual{i};
  diff_full{i}=absDiff{i};
  
  diff_clim{i}=diff_clim{i} ./ normFact;
  diff_annual{i}=diff_annual{i} ./ normFact;
  diff_full{i}=diff_full{i} ./ normFact;
  
end

end
