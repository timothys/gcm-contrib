function [] = avgDiagnostics( adjFields )
% Goal is to read diagnostics at 15.25day output and avg together
% Inputs: 
%
%	adjFields: Either
%		(a) String with field to average
%		(b) Cell array with elements specifying multiple fields
%
% Output
%
%	saves xx_fld in specified save directory for each field
% ---------------------------------------------------------------------


%% 0. Preliminaries
if ~exist('adjFields','var'), error('Need adjFields (e.g. hflux) specified, exiting ...\n');end;
mygrid=establish_mygrid('llc90');

if ~iscell(adjFields), adjFields={adjFields};end;
Nadj=length(adjFields);

% Set up directories
diagsDir = '../../results/samoc/dec.240mo.hf/diags/';
mainSaveDir = 'mat/reconstruct.34S/';
saveDir = [mainSaveDir 'hf/'];

if ~exist(saveDir,'dir'), mkdir(saveDir); end;

% Set up flds based on diagnostic list in exf files
% see diagsDir/exf.*.meta
fldList = {'tauu','tauv','hflux','sflux','atemp','aqh','lwdown','swdown','evap','precip','runoff'};
% ---------------------------------------------------------------------



%% 1. Loop through and do averaging
for i=1:Nadj

	% Grab recnumber
	for k=1:length(fldList) 
	  if strcmp(adjFields{i},fldList{k}), recNum=k;end;
	end;
	
	% Load, avg, & save
	% With wind stress, assume both components requested, compute and save both
	if recNum==1 
	  tauu=rdmds2gcmfaces(loadStr,NaN,'rec',1);
	  tauv=rdmds2gcmfaces(loadStr,NaN,'rec',2);
	  N = size(tauu.f1,3);
	  for n=1:N
	    [tauu(:,:,n),tauv(:,:,n)]=calc_UEVNfromUXVY(tauu(:,:,n),tauv(:,:,n));
	  end
	  
	  [xx_fld]=doAveraging(tauu,mygrid);
	  saveStr=sprintf('%sxx_tauu.mat',saveDir);
	  save(saveStr,'xx_fld');
	  fprintf('wrote file: %s ...\n',saveStr);

	  [xx_fld]=doAveraging(tauv,mygrid);	
	  saveStr=sprintf('%sxx_tauv.mat',saveDir);
	  save(saveStr,'xx_fld');
	  fprintf('wrote file: saved ...\n',saveStr);

	  clear tauu tauv;
	
	elseif recNum==2
	  fprintf('tauv already done or need to have tauu in options ...\n');

	else
	  xx_fld_in = rdmds2gcmfaces([diagsDir 'exf*'],NaN,'rec',recNum);
	  xx_fld=doAveraging(xx_fld_in,mygrid);
	  saveStr = sprintf('%sxx_%s.mat',saveDir,adjFields{i});
	  save(saveStr,'xx_fld');
	  fprintf('wrote file: %s ...\n',saveStr);
	end
end
end

function [xx_fld] = doAveraging( fldIn, mygrid )

Nrec = size(fldIn.f1,3);
	
if mod(Nrec,2)>0
	Nt = (Nrec+1)/2;
	xx_fld = .5*fldIn(:,:,[1:2:Nrec-1]) + .5*fldIn(:,:,[2:2:Nrec-1]);
	xx_fld = cat(3,xx_fld,fldIn(:,:,Nrec));

	if size(xx_fld.f1,3)~=Nt, fprintf('Output not right number of records ...\n'); keyboard; end;
else
	Nt = Nrec/2;	
	xx_fld = .5*fldIn(:,:,[1:2:Nrec]) + .5*fldIn(:,:,[2:2:Nrec]);
	if size(xx_fld.f1,3)~=Nt, fprintf('Output not right number of records ...\n'); keyboard; end;
end
end
