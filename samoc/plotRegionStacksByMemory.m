function [] = plotRegionStacksByMemory( mygrid )
% Want stackRecPlot, but varying the memory time like Hovmoeller plots in Pillar, 2016
% this generates a lot of plots. 
%
% ------------------------------------------------------------------------------

modelVar='eccov4r2';
atmVar = 'long-atm';
[~,figDir] = samoc_dirs('reconstruct-plot',modelVar,atmVar,'region-stacks-memory');

adjField = {'tauu','tauv','aqh','atemp','swdown','lwdown','precip','runoff','rad','warm','texas','buoyancy'};
Nadj=length(adjField);

% ------------------------------------------------------------------------------

tMem = [1 3 6 12 36 60 9*12 15*12 19*12];

for i=1:Nadj

  saveDir = sprintf('%s/%s',figDir,adjField{i});
  if ~exist(saveDir,'dir'), mkdir(saveDir); end;

  for t=1:length(tMem)

    % Climatology
    saveStr = sprintf('%s/clim_%dmo',saveDir,tMem(t));
    if ~exist([saveStr '.pdf'],'file')
      stackRecPlot(adjField{i},mygrid,atmVars,'southek','clim',tMem(t));
      set(gcf,'paperorientation','landscape')
      set(gcf,'paperunits','normalized')
      set(gcf,'paperposition',[0 0 1 1])
      saveas(gca,saveStr,'pdf');
    end
  end

  close all

  for t=1:length(tMem)

    % Interannual
    saveStr = sprintf('%s/annual_%dmo',saveDir,tMem(t));
    if ~exist([saveStr '.pdf'],'file')
      stackRecPlot(adjField{i},mygrid,atmVars,'southek','annual',tMem(t));
      set(gcf,'paperorientation','landscape')
      set(gcf,'paperunits','normalized')
      set(gcf,'paperposition',[0 0 1 1])
      saveas(gca,saveStr,'pdf');
    end

  end
 
  close all

end    
