function [] = plotRegionExf( exfVar, regionVar, plotType)
% Plot precipitation average over N Atl Subpolar gyre
% from multiple atmospheric data sets
%
% Inputs:
%       
%       exfVar: 'tauu' or 'precip' etc ...
%               for now only atm vars ... no flux
%       
%       regionVar: 'acc' or 'atl.subPolar' .. 
%                  same convention as regionFld
%       
%       plotType: 'annAvg' 'clim' 
%
%              
% -----------------------------------------------------------------


%% Set up dirs
plotDir=sprintf('figures/exf/region-comp');
if ~exist(plotDir,'dir'), mkdir(plotDir); end;

saveName = sprintf('%s/%s_%s_%s',plotDir,exfVar,strrep(regionVar,'.','-'),plotType);

%% Set the vars
modelVars={'eccov4r2','eccov4r3','eig','jra55','jra55-gpcp'};
atmVars={'short-atm','short-atm','long-atm','short-atm','short-atm'};

%% Get index for exf variable
adjField=set_adjField( 'atm' );
for i=1:length(adjField)
  if strcmp(adjField{i},exfVar)
    exfInd=i;
  end
end

%% Set up vars
nModels=length(modelVars);
nRecs=0;
fld=cell(nModels,1);
fld_out = fld;
tMo = cell(nModels,1);
tYrs=tMo;
lbl=cell(nModels,1);

%% Compute annual average
for i=1:nModels

  % Fill regionFld appropriately
  regionFld=loadRegionFld(modelVars{i},atmVars{i});

  % Make sure region fld is done for this set
  if ~isempty(regionFld)
    nRecs=nRecs+1;
    
    eval(sprintf('fld{nRecs} = regionFld.%s{%d};',regionVar,exfInd));

    if ~isempty(strfind(atmVars{i},'short'))
      tMo{nRecs} = 1992 + [1:length(fld{nRecs})]./12;
      tYrs{nRecs} = 1992:(1992+length(fld{nRecs})/12-1);
    else
      tMo{nRecs} = 1979 + [1:length(fld{nRecs})]./12;
      tYrs{nRecs} = 1979:(1979+length(fld{nRecs})/12-1);
    end
    lbl{nRecs} = modelVars{i};
      
    if strcmp(plotType,'annAvg')
      fld_out{nRecs} = calcAnnualMean(fld{nRecs},length(tYrs{nRecs}));
    elseif strcmp(plotType,'clim')
      fld_out{nRecs} = calcClimatology(fld{nRecs},length(tYrs{nRecs}));
    end 
  end

end

%% Do the plotting
figureW;
for i=1:nRecs
  if ~isempty(strfind(saveName,'clim')) 
    plot(1:12,fld_out{i})
  else
    plot(tYrs{i},fld_out{i})
  end
  hold on;
end
colormap(lines)
ylabel(exfVar)
legend(lbl{1:nRecs},'location','best');
if ~isempty(strfind(saveName,'clim')) 
  xlim([0.5 12.5])
  set(gca,'xtick',[1:12],...
      'xticklabel',{'Jan','Feb','Mar','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'});
end
grid on
 
savePlot(saveName,'pdf');
end

% -----------------------------------------------------------------

function [regionFld] = loadRegionFld( modelVar, atmVar )

matfile = sprintf('mat/exf/%s/%s/region_exf.mat',modelVar,atmVar);

if exist(matfile,'file')
  load(matfile, 'regionFld')
else
  regionFld=calcRegionAvg(modelVar,atmVar,'exf');
end

end
