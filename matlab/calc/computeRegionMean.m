function [ fldMean ] = computeRegionMean( fld, msk, mygrid)
% Given a field and mask, want to compute a spatial mean over that region.

%% Preliminaries
if isa(fld,'gcmfaces')
    fld = convert2gcmfaces(fld);
end;

if isa(msk,'gcmfaces')
    msk = convert2gcmfaces(msk);
end

sz = size(fld);
if length(sz > 2), Nt = sz(3); 
else Nt = 1; 
end;

if length(size(msk))<2 || length(size(msk))>3
  error('Need a 2D or 3D mask, exiting...\n');
elseif length(size(msk))==2
  mskT=repmat(msk,[1 1 Nt]);
elseif length(size(msk))==3 && size(msk,3)~=Nt
  error('Mask third dimension needs to match field\n');
end

%% Get mean
tmp1 = squeeze(nansum(nansum(fld.*mskT,1),2));
tmp2 = squeeze(nansum(nansum(msk,1),2));
fldMean = [tmp1/tmp2]';
end
