function [ msks ] = createRegionMasks( varargin )
% Masks various global regions
% For extra project specific masks, optional input:
%
%       'ekman' - mask local ekman for samoc
%       'cze' - mask for czeschel oscillatory adj
%       'enso34' - mask for attributing enso to wind in SAMOC
%       'sstrop30'/'sstrop40' - mask for removing tip of Africa from ind SS trop
%       'ind/pacMXL' - large mix layer in pac / ind ocean
%       
%
%
% ----------------------------------------------------------------------

extraMasks={};
for i=1:length(varargin)
  if iscell(varargin{i})
    for n=1:length(varargin{i})
      extraMasks{n}=varargin{i}{n};
    end
  else
    extraMasks{i} = varargin{i};
  end
end

global mygrid;

%% ACC: Everything south of 60S.
yCond = mygrid.YC <= -60;
acc= mygrid.mskC(:,:,1).*yCond;
acc=acc.*mygrid.mskC(:,:,1);
acc(isnan(acc))=0;
globe=acc;

%% Arctic
arc= v4_basin('arct'); %mygrid.mskC(:,:,1).*yCond;
arc=arc.*mygrid.mskC(:,:,1);
arc(isnan(arc))=0;
globe=globe+arc;

%% Indian Ocean
ind= v4_basin('indExt'); 
ind=ind.*(mygrid.mskC(:,:,1)-acc);
ind(isnan(ind))=0;

%% Pacific
pac = v4_basin('pacExt');
pac = pac.*(mygrid.mskC(:,:,1) - acc);
pac(isnan(pac))=0;

%% Atlantic
atl = v4_basin('atlExt');
atl = atl.*(mygrid.mskC(:,:,1) - acc);
atl(isnan(atl))=0;

%% Subpolar gyres:
yCond = mygrid.YC > 45 & mygrid.YC <= 80;
pacSubPolar = mygrid.mskC(:,:,1).*yCond.*pac;
pacSubPolar=pacSubPolar.*(mygrid.mskC(:,:,1) - arc);
pacSubPolar(isnan(pacSubPolar))=0;
globe=globe+pacSubPolar;

atlSubPolar = mygrid.mskC(:,:,1).*yCond.*atl;
atlSubPolar=atlSubPolar.*(mygrid.mskC(:,:,1) - arc);
atlSubPolar(isnan(atlSubPolar))=0;
globe=globe+atlSubPolar;

%% Subtropical gyres:
yCond = mygrid.YC > 15 & mygrid.YC <= 45;
pacNStrop = mygrid.mskC(:,:,1).*yCond.*pac;
pacNStrop(isnan(pacNStrop))=0;
globe=globe+pacNStrop;

atlNStrop = mygrid.mskC(:,:,1).*yCond.*atl;
atlNStrop(isnan(atlNStrop))=0;
globe=globe+atlNStrop;

indNStrop = mygrid.mskC(:,:,1).*yCond.*ind;
indNStrop(isnan(indNStrop))=0;
globe=globe+indNStrop;

%% North tropical
yCond = mygrid.YC > 0 & mygrid.YC <= 15;
pacNtrop = mygrid.mskC(:,:,1).*yCond.*pac;
pacNtrop(isnan(pacNtrop))=0;
globe=globe+pacNtrop;

atlNtrop = mygrid.mskC(:,:,1).*yCond.*atl;
atlNtrop(isnan(atlNtrop))=0;
globe=globe+atlNtrop;

indNtrop = mygrid.mskC(:,:,1).*yCond.*ind;
indNtrop(isnan(indNtrop))=0;
globe=globe+indNtrop;

%% South tropical
yCond = mygrid.YC > -15 & mygrid.YC <= 0;
pacStrop = mygrid.mskC(:,:,1).*yCond.*pac;
pacStrop(isnan(pacStrop))=0;
globe=globe+pacStrop;

atlStrop = mygrid.mskC(:,:,1).*yCond.*atl;
atlStrop(isnan(atlStrop))=0;
globe=globe+atlStrop;

indStrop = mygrid.mskC(:,:,1).*yCond.*ind;
indStrop(isnan(indStrop))=0;
globe=globe+indStrop;


%% South Sub tropical
yCond = mygrid.YC > -40 & mygrid.YC <= -15;
pacSStrop = mygrid.mskC(:,:,1).*yCond.*pac;
pacSStrop(isnan(pacSStrop))=0;
globe=globe+pacSStrop;

indSStrop = mygrid.mskC(:,:,1).*yCond.*ind;
indSStrop(isnan(indSStrop))=0;
globe=globe+indSStrop;

atlSStrop = mygrid.mskC(:,:,1).*yCond.*atl;
atlSStrop(isnan(atlSStrop))=0;
globe=globe+atlSStrop;

%% Southern ocean
yCond = mygrid.YC > -60 & mygrid.YC <= -40;
pacSouth = mygrid.mskC(:,:,1).*yCond.*pac;
pacSouth(isnan(pacSouth))=0;
globe=globe+pacSouth;

atlSouth = mygrid.mskC(:,:,1).*yCond.*atl;
atlSouth(isnan(atlSouth))=0;
globe=globe+atlSouth;

indSouth = mygrid.mskC(:,:,1).*yCond.*ind;
indSouth(isnan(indSouth))=0;
globe=globe+indSouth;


%% Add Barents Sea
barents=v4_basin('barents');
barents=barents.*mygrid.mskC(:,:,1);
barents(isnan(barents))=0;
globe=globe+barents;


pp = struct('subPolar',pacSubPolar,'NStrop',pacNStrop,'Ntrop',pacNtrop,'Strop',pacStrop,'SStrop',pacSStrop,'south',pacSouth,'full',pac);
aa = struct('subPolar',atlSubPolar,'NStrop',atlNStrop,'Ntrop',atlNtrop,'Strop',atlStrop,'SStrop',atlSStrop,'south',atlSouth,'full',atl);
ii = struct('NStrop',indNStrop,'Ntrop',indNtrop,'Strop',indStrop,'SStrop',indSStrop,'south',indSouth,'full',ind);

msks = struct('acc',acc,'arc',arc,'bar',barents,'ind',ii,'pac',pp,'atl',aa,'globe',globe);

if ~isempty(extraMasks)
  for n=1:length(extraMasks)
    if ~isempty(strfind(extraMasks{n},'ek'))
      %% Local Ekman
      yCond = mygrid.YC > -40 & mygrid.YC <= -30;
      ek = mygrid.mskC(:,:,1).*yCond.*atl;
      ek(isnan(ek))=0;

      msks.atl.ek=deal(ek);
      msks.atl.SStrop=msks.atl.SStrop-msks.atl.ek; %% Don't want overlap for samoc attribution

    elseif strcmp(extraMasks{n},'cze')
      %% Czeschel et al's north box
      yCond = mygrid.YC >= 45 & mygrid.YC <= 70;
      xCond = mygrid.XC >= -90 & mygrid.XC <= 10;
      cze = mygrid.mskC(:,:,1).*yCond.*xCond.*atl;
      cze=cze.*(mygrid.mskC(:,:,1));
      cze(isnan(cze))=0;

      msks.cze=deal(cze);

    elseif strcmp(extraMasks{n},'sstrop30')
      xCond = mygrid.XC > 30;
      indSStrop30=mygrid.mskC(:,:,1).*xCond.*yCond.*ind;
      indSStrop30(isnan(indSStrop30))=0;

      msks.SStrop30=deal(indSStrop30);

    elseif strcmp(extraMasks{n},'sstrop40')
      xCond = mygrid.XC > 40;
      indSStrop40=mygrid.mskC(:,:,1).*xCond.*yCond.*ind;
      indSStrop40(isnan(indSStrop40))=0;

      msks.SStrop40=deal(indSStrop40);

    elseif strcmp(extraMasks{n},'indMXL')
      %% IndMXL: where MXL Depth is high in Southern Hemisphere
      yCond = mygrid.YC<-35 & mygrid.YC>-50;
      indMXL = mygrid.mskC(:,:,1).*yCond.*ind;
      indMXL(isnan(indMXL))=0;

      msks.indMxl=deal(indMXL);

    elseif strcmp(extraMasks{n},'pacMXL')
      %% PacMXL: where MXL Depth is high in Southern Hemisphere
      yCond = mygrid.YC<-40 & mygrid.YC>-60;
      pacMXL = mygrid.mskC(:,:,1).*yCond.*pac;
      pacMXL(isnan(pacMXL))=0;

      msks.pacMxl=deal(pacMXL);

    elseif strcmp(extraMasks{n},'westTropPac')
        %% Western and central side of tropical pacific
        yCond = mygrid.YC<15 & mygrid.YC>-15;
        xCond = mygrid.XC>100;
        westTropPac = mygrid.mskC(:,:,1).*yCond.*xCond.*pac;
        westTropPac(isnan(westTropPac))=0;

        msks.westTropPac = deal(westTropPac);

    elseif strcmp(extraMasks{n},'westCentTropPac')
        %% Western and central side of tropical pacific
        yCond = mygrid.YC<15 & mygrid.YC>-15;
        xCond = mygrid.XC>100 | mygrid.XC<-140;
        westCentTropPac = mygrid.mskC(:,:,1).*yCond.*xCond.*pac;
        westCentTropPac(isnan(westCentTropPac))=0;

        msks.westCentTropPac = deal(westCentTropPac);

    elseif strcmp(extraMasks{n},'tropPac15')

        %% Just +/-15 in Pac
        yCond = mygrid.YC<15 & mygrid.YC>-15;
        tropPac15 = mygrid.mskC(:,:,1).*yCond.*pac;
        tropPac15(isnan(tropPac15))=0;

        msks.tropPac15 = deal(tropPac15);


    elseif strcmp(extraMasks{n},'tropPac10')

        %% Just +/-10 in Pac
        yCond = mygrid.YC<10 & mygrid.YC>-10;
        tropPac10 = mygrid.mskC(:,:,1).*yCond.*pac;
        tropPac10(isnan(tropPac10))=0;

        msks.tropPac10 = deal(tropPac10);

    elseif strcmp(extraMasks{n},'tropPac5')

        %% Just +/-5 in Pac
        yCond = mygrid.YC<5 & mygrid.YC>-5;
        tropPac5 = mygrid.mskC(:,:,1).*yCond.*pac;
        tropPac5(isnan(tropPac5))=0;

        msks.tropPac5 = deal(tropPac5);

    elseif strcmp(extraMasks{n},'tropPac2_5')

        %% Just +/-2.5 in Pac
        yCond = mygrid.YC<2.5 & mygrid.YC>-2.5;
        tropPac2_5 = mygrid.mskC(:,:,1).*yCond.*pac;
        tropPac2_5(isnan(tropPac2_5))=0;

        msks.tropPac2_5 = deal(tropPac2_5);

    elseif strcmp(extraMasks{n},'tropPac20')

        %% Just +/- 20 in Pac
        yCond = mygrid.YC<20 & mygrid.YC>-20;
        tropPac20 = mygrid.mskC(:,:,1).*yCond.*pac;
        tropPac20(isnan(tropPac20))=0;

        msks.tropPac20 = deal(tropPac20);

    elseif strcmp(extraMasks{n},'tropPac30')

        %% Just +/- 30 in Pac
        yCond = mygrid.YC<30 & mygrid.YC>-30;
        tropPac30 = mygrid.mskC(:,:,1).*yCond.*pac;
        tropPac30(isnan(tropPac30))=0;

        msks.tropPac30 = deal(tropPac30);

    elseif strcmp(extraMasks{n},'enso34')
        %%% ENSO 3.4 mask
        enso34 = calcENSOMasks('enso34');
        enso34 = mygrid.mskC(:,:,1).*enso34;

        msks.enso34=deal(enso34);

    elseif strcmp(extraMasks{n},'enso12')
        %%% ENSO 1+2 mask
        enso12 = calcENSOMasks('enso12');
        enso12 = mygrid.mskC(:,:,1).*enso12;

        msks.enso12=deal(enso12);

    elseif strcmp(extraMasks{n},'enso3')
        %%% ENSO 3 mask
        enso3 = calcENSOMasks('enso3');
        enso3 = mygrid.mskC(:,:,1).*enso3;

        msks.enso3=deal(enso3);

    elseif strcmp(extraMasks{n},'enso4')
        %%% ENSO 4 mask
        enso4 = calcENSOMasks('enso4');
        enso4 = mygrid.mskC(:,:,1).*enso4;

        msks.enso4=deal(enso4);

    elseif strcmp(extraMasks{n},'ensoModoki')

        %% See Ashok et al, 2007, El Nino Modoki and its teleconnections
        %% ENSO Modoki = center avg - 1/2(East + West)

        ensoModoki = calcENSOMasks('ensoModoki');
        ensoModoki = mygrid.mskC(:,:,1).*ensoModoki;

        msks.ensoModoki = deal(ensoModoki);

    else
      fprintf('Did not recognize masks %s, skipping ...\n',extraMasks{n});
    end
  end
end





end
