function [clim] = calcClimatology(fld,Nyrs,varargin)
% Simple script to compute climatology from a field
% assumes fld is monthly
% 
% Inputs
%
%       fld: [1,2,3,4]D, or [3,4]D gcmfaces
%            time is on last dimension
%
%       Nyrs: number of years for record
%
%  (optional)
%       
%       removeAnnualAvg: 1/0, 1 (default) = remove annual avg
%
% Output
%
%       Same as input dims, time replaced with 12 months
% ---------------------------------------------------------

% Default, do remove annual average
removeAnnualAvg=1;
for n=1:length(varargin)
    removeAnnualAvg=varargin{n};
end

if ~isa(fld,'gcmfaces')

    % --- work on array data
    sz = size(fld);
    clim=zeros([sz(1:end-1) 12]);
    Nt=sz(end);
    if Nyrs*12 ~= Nt
        fprintf('Error: Nyrs * 12 != length of field, input the correct number of years for field\n');
        keyboard
    end
  
    if removeAnnualAvg==1

        % --- First, remove temporal mean
        time_mean = nanmean(fld,length(sz));
        if length(sz) == 2
            for n = 1:sz(2)
                fld(:,n) = fld(:,n) - time_mean;
            end
        elseif length(sz) == 3
            for n = 1:sz(3)
                fld(:,:,n) = fld(:,:,n) - time_mean;
            end
        elseif length(sz) == 4
            for n = 1:sz(4)
                fld(:,:,:,n) = fld(:,:,:,n) - time_mean;
            end
        end

        % --- Next, remove annual averages and add temporal mean back
        annual=calcAnnualMean(fld,Nyrs);

        % Here: yy indexes the year
        if length(sz)==2
            for n = 1:sz(2)
                yy = floor(mod((n-1)/12,12))+1;
                fld(:,n) = fld(:,n) - annual(:,yy);
                fld(:,n) = fld(:,n) + time_mean;
            end
        elseif length(sz)==3
            for n = 1:sz(3)
                yy = floor(mod((n-1)/12,12))+1;
                fld(:,:,n) = fld(:,:,n) - annual(:,:,yy);
                fld(:,:,n) = fld(:,:,n) + time_mean;
            end
        elseif length(sz)==4
            for n = 1:sz(4)
                yy = floor(mod((n-1)/12,12))+1;
                fld(:,:,:,n) = fld(:,:,:,n) - annual(:,:,:,yy);
                fld(:,:,:,n) = fld(:,:,:,n) + time_mean;
            end
        else
            error('calcClimatology not general to >4D ...');
        end
    end
  
    % --- Now compute the average for each month
    if length(sz)==2
        for i=1:12
            clim(:,i) = nanmean(fld(:,i:12:Nt),length(sz));
        end
    elseif length(sz)==3
        for i=1:12
          clim(:,:,i) = nanmean(fld(:,:,i:12:Nt),length(sz));
        end
    elseif length(sz)==4
        for i=1:12
          clim(:,:,:,i) = nanmean(fld(:,:,:,i:12:Nt),length(sz));
        end
    else
        error('calcClimatology not general to >4D ...');
    end

else

% --- work on gcmfaces data

    if length(size(fld.f1))==3
        Nt = size(fld.f1,3);
    else
        Nt = size(fld.f1,4);
    end

    if Nyrs*12 ~= Nt
        fprintf('Error: Nyrs * 12 != length of field, input the correct number of years for field\n');
        keyboard
    end



    if removeAnnualAvg==1

        % --- Remove annual avg
        annual=calcAnnualMean(fld,Nyrs);
        annual=convert2array(annual);
        fld=convert2array(fld);

          if length(size(fld))==3
        
            for i=1:Nyrs
                beg=(i-1)*12+1;
                ed=i*12;
                fld(:,:,beg:ed)=fld(:,:,beg:ed)-repmat(annual(:,:,i),[1 1 12]); 
            end

          else

              for i=1:Nyrs
                  beg=(i-1)*12+1;
                  ed=i*12;
                  fld(:,:,:,beg:ed)=fld(:,:,:,beg:ed)-repmat(annual(:,:,:,i),[1 1 1 12]); 
              end
          end
      else
          fld=convert2array(fld);
    end

    % --- Now take clim
    if length(size(fld))==3
 
        clim=repmat(0*fld(:,:,1),[1 1 12]);

        for i=1:12
            clim(:,:,i) = mean(fld(:,:,i:12:Nt),3);
        end

    else

        clim=repmat(0*fld(:,:,:,1),[1 1 1 12]);

        for i=1:12
            clim(:,:,:,i) = mean(fld(:,:,:,i:12:Nt),4);
        end
    end

    % convert back to gcmfaces
    clim=convert2array(clim);
end
