function [merMean, lon] = calcMeridionalMean( fldIn, lon_bounds, mskC, method )
% Compute mean along meridional line for 1degree longitude increments at points: 
%
%       lon \in (lon_bounds(1),lon_bounds(2))
% 
% NOTE: This assumes the field lives on tracer points of C grid
% Also, don't do this for the Arctic :( sorry
%   This is b/c I'm using convert2array, where Arctic is attached to Atlantic,
%   and doesn't connect to pacific. 
% 
%
% Input: 
%
%       fldIn: [n1 x n2 x n3 x n4] gcmfaces object
%
%       lon_bounds: longitudinal bounds, assuming lon is between -180, 180
%                   e.g. [-20 20] computes average between 20W and 20E
%                   However, if "more positive" value is first, assume
%                   section spans the dateline
%                   e.g. [160 -140] ... this gets converted to [-200 -140]
%
%       mskC: 2D mask to enforce averaging over a specific region
%
%       method: 1 or 2, following gcmfaces_calc/calc_mermean_T... 
%               1 => use hFacC*DRF
%               2 => use mskC
%              
% Output: 
%       
%       merMean: [longitude x n3 x n4] matrix with meridional mean
%                for each entry in 3rd,4th dimensions
%       
%       lon: [longitude ] vector with longitude coordinates
%                  1 per degree between lon_bounds(1):lon_bounds(2)
%
% -------------------------------------------------------------


% Make sure to set mygrid variable
global mygrid
if isempty(mygrid)
    error('Need to set global mygrid variable');
end

% Deal with defaults and assert inputs
if nansum(lon_bounds < -180) + nansum(lon_bounds>180) > 0
    error('lon_bounds must be inside (-180,180)');
end
% Deal with longitude values which span across dateline 
if lon_bounds(1)>lon_bounds(2)
    lon_bounds(1) = lon_bounds(1)-360;
end
if nargin<3
    fprintf('mskC not provided, using mskC(:,:,1) \n');
    mskC = mygrid.mskC(:,:,1);
end
if nargin<4
    fprintf('method not provided, defaulting to method 2 \n');
    method = 2;
end

% Use array format
% In this format: dim1 is latitude, dim2 is longitude
% warning: The arctic is not attached to Pacific correctly :(
fldIn=convert2array(fldIn);
n1=size(fldIn,1); 
n2=size(fldIn,2);
n3=size(fldIn,3); 
n4=max(size(fldIn,4),1);
if length(size(fldIn))>3
    error('Not ready to accept depth varying fields yet...\n'); 
end;

% Make mskC 3D if necessary
if n3 > 1 && n3 == length(mygrid.RC)
    fprintf('Extending mskC to depth levels in 3rd dimension \n');
    mskC = mk3D(mskC,mygrid.mskC);
end

% Setup longitude vector
lon = lon_bounds(1):lon_bounds(2);
Nlon = length(lon);

% Set weighting: rac and hFacC/mskC according to method
rac=squeeze(repmat(convert2array(mygrid.RAC), [1 1 n3 n4]));
if abs(method)==1;

  if n3==length(mygrid.RC);
      fprintf('n3==length(mygrid.RC), so assuming 3rd dimension is depth.\n');
      hFacC=convert2array(mygrid.hFacC.*mskC);
      hFacC=squeeze(repmat(hFacC,[1 1 1 n4]));
      DRF=squeeze(repmat(reshape(mygrid.DRF,[1 1 n3 1]),[n1 n2 1 n4]));
  else;
      fprintf('n3!=length(mygrid.RC), so assuming 3rd dim is time records for 2D field. \n');
      hFacC=convert2array(mygrid.hFacC(:,:,1).*mskC);
      hFacC=squeeze(repmat(hFacC,[1 1 n3 n4]));
      DRF=squeeze(repmat(mygrid.DRF(1),[n1 n2 n3 n4]));
  end;
  weight=rac.*hFacC.*DRF;
else;
  if n3==length(mygrid.RC);
      fprintf('n3==length(mygrid.RC), so assuming 3rd dimension is depth.\n');
      mskC=convert2array(mygrid.mskC.*mskC);
      mskC=squeeze(repmat(mskC,[1 1 1 n4]));
  else;
      fprintf('n3!=length(mygrid.RC), so assuming 3rd dim is time records for 2D field. \n');
      mskC=convert2array(mygrid.mskC(:,:,1).*mskC);
      mskC=squeeze(repmat(mskC,[1 1 n3 n4]));
  end;
  weight=rac.*mskC;
end;

% Initialize output array
xc = convert2array(mygrid.XC);
xc = repmat(xc,[1 1 n3]);
merMean = zeros(Nlon,n3);

% Note: if dx = 0.5, get stripes across face 5 :(
dx = 0.55;

% Compute mean along meridional line
for j = 1:Nlon
    if lon(j)<-180
        xc_msk = xc<lon(j)+360 +dx & xc>=lon(j)+360-dx;
    elseif lon(j) == -180
        xc_msk = xc<lon(j)+dx | xc>=lon(j)+360-dx;
    else
        xc_msk = xc<lon(j)+dx & xc>=lon(j)-dx;
    end

    numer = squeeze(nansum(nansum(fldIn.*weight.*xc_msk,1),2));
    denom = squeeze(nansum(nansum(weight.*xc_msk,1),2));
    merMean(j,:) = numer' ./ denom';

    if mod(j,10)==0
        fprintf('Computed %d / %d meridional avgs ... \n',j,Nlon);
    end
end

end
