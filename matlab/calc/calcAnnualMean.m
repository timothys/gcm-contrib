function [annual] = calcAnnualMean(fld,Nyrs)
% Simple script for computing annual mean of monthly field values
% 
% Inputs
%
%       fld: [1,2,3,4]D, or [3,4]D gcmfaces
%         if 1D, must be row vector
%            time is on last dimension
%
%       Nyrs: number of years for record
%
% Output
%
%       Same as input dims, time replaced with Nyrs
% ---------------------------------------------------------

if ~isa(fld,'gcmfaces')
  
    % --- Work on array data
    sz = size(fld);
    annual = zeros([sz(1:end-1),Nyrs]);
    Nt=sz(end);
    
    if Nyrs*12 ~= Nt
        fprintf('Error: Nyrs*12 != length of field, input correct number of years for field\n');
        keyboard
    end
  
    if length(sz)==2
        for i=1:Nyrs
            annual(:,i) = nanmean(fld(:,((i-1)*12+1):(i*12)),length(sz));
        end
    elseif length(sz)==3
        for i=1:Nyrs
            annual(:,:,i) = nanmean(fld(:,:,((i-1)*12+1):(i*12)),length(sz));
        end
    elseif length(sz)==4
        for i=1:Nyrs
            annual(:,:,:,i) = nanmean(fld(:,:,:,((i-1)*12+1):(i*12)),length(sz));
        end
    else
        error('calcAnnualMean not general to >4D ...');
    end

else

    % --- work on gcmfaces data
    if length(size(fld.f1))==3
        Nt = size(fld.f1,3);
    else
        Nt = size(fld.f1,4);
    end

    if Nyrs*12 ~= Nt
        fprintf('Error: Nyrs * 12 != length of field, input the correct number of years for field\n');
        keyboard
    end

    fld=convert2array(fld);

    if length(size(fld))==3
 
        annual=repmat(0*fld(:,:,1),[1 1 Nyrs]);

        for i=1:Nyrs
              annual(:,:,i) = mean(fld(:,:,(i-1)*12+1:(i*12)),3);
        end

    else

        annual=repmat(0*fld(:,:,:,1),[1 1 1 Nyrs]);

        for i=1:Nyrs
            annual(:,:,:,i) = mean(fld(:,:,:,(i-1)*12+1:(i*12)),4);
        end

    end

    % convert back to gcmfaces
    annual=convert2array(annual);
end
end
