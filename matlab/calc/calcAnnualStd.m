function [annual] = calcAnnualStd(fld,Nyrs)
% Simple script for computing standard deviation of monthly field values annually
% 
% Inputs
%
%       fld: 1D, 2D, or gcmfaces
%         if 1D, must be row vector
%            2D, time on 2nd dim
%            gcmfaces, time on last dim
%
%       Nyrs: number of years for record
%
% Output
%
%       Same as input dims, time replaced with Nyrs
% ---------------------------------------------------------

if ~isa(fld,'gcmfaces')
  
  % --- Work on array data

  annual = zeros(size(fld,1),Nyrs);
  Nt=size(fld,2);
  
  if Nyrs*12 ~= Nt
    fprintf('Error: Nyrs*12 != length of field, input correct number of years for field');
    keyboard
  end
  
  for i=1:Nyrs
    annual(:,i) = std(fld(:,((i-1)*12+1):(i*12)),[],2);
  end

else

  % --- work on gcmfaces data

  if length(size(fld.f1))==3
    Nt = size(fld.f1,3);
  else
    Nt = size(fld.f1,4);
  end

  if Nyrs*12 ~= Nt
    fprintf('Error: Nyrs * 12 != length of field, input the correct number of years for field\n');
    keyboard
  end

  fld=convert2array(fld);

  if length(size(fld))==3
 
    annual=repmat(0*fld(:,:,1),[1 1 Nyrs]);

    for i=1:Nyrs
      annual(:,:,i) = std(fld(:,:,(i-1)*12+1:(i*12)),[],3);
    end

  else

    annual=repmat(0*fld(:,:,:,1),[1 1 1 Nyrs]);

    for i=1:Nyrs
      annual(:,:,:,i) = std(fld(:,:,:,(i-1)*12+1:(i*12)),[],4);
    end

  end

  % convert back to gcmfaces
  annual=convert2array(annual);
end
end
