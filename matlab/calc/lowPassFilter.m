function [lowPassSig,ff_sig] = lowPassFilter( freq, sig, cutoffFreq )
% 
% Return a low pass filtered version of the signal, removing frequencie above cutoffFreq
%
% Input:
%
%   freq : vector of frequency values 
%   sig : vector containing the signal in time domain 
%   cutoffFreq : frequency to set all above to zero
% 
% Output:
%
%   lowPassSig : filtered version of original signal in time domain
%   ff_sig : filtered response in frequency domain
%
% --------------------------------------------------------------------------------------

Nt = length(sig); 
ff_sig = fft(sig,Nt);

ind=freq>cutoffFreq;
ff_sig(ind) = zeros(1,sum(ind));
lowPassSig=ifft(ff_sig);

lowPassSig=abs(lowPassSig);
end
