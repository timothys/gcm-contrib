function [frac] = calcSensFrac(ctrlTimeSteps,filename,objFuncTimeSteps)
% Script for computing the additional fraction of sensitivity 
% corresponding to the very end of the simulation.
%
% Inputs: 
%       
%       ctrlTimeSteps: "expected" number of adxx_<> fields
%                      e.g. 240 for 20yrs, monthly objective function
%
%       filename: location of STDOUT.0000
%
%       objFuncTimeSteps: (optional) time steps to objective function evaluation
%                         if different from the end of the simulation
%       
% Output:
%       
%       frac: fraction of final month forcing to include in reconstruction from 
%             last sensitivity record
%
% ---------------------------------------------------------------------------------

if nargin<3
  objFuncTimeSteps=0;
end


[modelStartYr,modelStartMo,modelStartDay,modelStartHr,modelStepPeriod,modelTimeSteps] = grepForModelDates(filename);
[ctrlStartYr,ctrlStartMo,ctrlStartDay,ctrlStartHr,ctrlPeriod] = grepForCtrlDates(filename);

if objFuncTimeSteps~=0 && objFuncTimeSteps<modelTimeSteps
  modelTimeSteps=objFuncTimeSteps;
end

% --- From nTimeSteps, get end date
modelEndDate = ts2dte(modelTimeSteps,modelStepPeriod,modelStartYr,modelStartMo,modelStartDay,modelStartHr,0);
ctrlEndYr = str2num(ts2dte(ctrlTimeSteps,ctrlPeriod,ctrlStartYr,ctrlStartMo,ctrlStartDay,ctrlStartHr,'yyyy'));
ctrlEndMo = str2num(ts2dte(ctrlTimeSteps,ctrlPeriod,ctrlStartYr,ctrlStartMo,ctrlStartDay,ctrlStartHr,'mm'));
ctrlEndDay = str2num(ts2dte(ctrlTimeSteps,ctrlPeriod,ctrlStartYr,ctrlStartMo,ctrlStartDay,ctrlStartHr,'dd'));
ctrlEndHr = str2num(ts2dte(ctrlTimeSteps,ctrlPeriod,ctrlStartYr,ctrlStartMo,ctrlStartDay,ctrlStartHr,'hh'));

% --- Compute fraction as 
%      modelEndDate - ModelTimeAt(xx_*(N-1)) -
frac = dte2ts(modelEndDate,ctrlPeriod,ctrlEndYr,ctrlEndMo,ctrlEndDay,ctrlEndHr);

end

% ---------------------------------------------------------------------------------

function [modelStartYr,modelStartMo,modelStartDay,modelStartHr,modelStepPeriod,modelTimeSteps] = grepForModelDates(filename)
% Grab the model start and end dates


strSearch = 'modelstartdate YY';
cmd=sprintf('grep -ri -A 1 "%s" %s',strSearch,filename);
[status,cmdout]=system(cmd);

modelStartDate1 = cmdout(end-8:end-1);
modelStartYr=str2num(modelStartDate1(1:4));
modelStartMo=str2num(modelStartDate1(5:6));
modelStartDay=str2num(modelStartDate1(7:8));


% --- Get the start date
strSearch = 'modelstartdate HH';
cmd=sprintf('grep -ri -A 1 "%s" %s',strSearch,filename);
[status,cmdout]=system(cmd);

modelStartHr = str2num(cmdout(end-6:end-5));

% --- Find the time step size
strSearch = 'modelstep';
cmd=sprintf('grep -ri -A 1 "%s" %s',strSearch,filename);
[status,cmdout]=system(cmd);

ind=strfind(cmdout,')');
modelStepPeriod = str2num(cmdout(ind(end)+1:end));

% --- Find the number of time steps for end date
strSearch='ntimesteps';
cmd=sprintf('grep -ri -A 1 "%s" %s',strSearch,filename);
[status,cmdout]=system(cmd);

ind=strfind(cmdout,')');
modelTimeSteps = str2num(cmdout(ind(end)+1:end));

end

% ---------------------------------------------------------------------------------

function [ctrlStartYr,ctrlStartMo,ctrlStartDay,ctrlStartHr,ctrlPeriod] = grepForCtrlDates(filename)

strSearch = 'xx_gentim2d_startdate1';

% --- Find the start date of controls
cmd=sprintf('grep -ri %s %s',strSearch,filename);
[status,cmdout]=system(cmd);

ctrlStartYr=str2num(cmdout(end-9:end-6));
ctrlStartMo=str2num(cmdout(end-5:end-4));
ctrlStartDay=str2num(cmdout(end-3:end-2));

strSearch = 'xx_gentim2d_startdate2';

cmd=sprintf('grep -ri %s %s',strSearch,filename);
[status,cmdout]=system(cmd);

ctrlStartHr=str2num(cmdout(end-7:end-6));

strSearch = 'xx_gentim2d_period';

% --- Find the control period "step size"
cmd=sprintf('grep -ri %s %s',strSearch,filename);
[status,cmdout]=system(cmd);

ind=strfind(cmdout,'=');

ctrlPeriod=str2num(cmdout(ind(end)+1:end-2));


end
