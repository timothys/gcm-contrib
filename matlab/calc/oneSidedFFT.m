function [ff_sig] = oneSidedFFT( sig )
%
% Return one sided fft of signal in frequency domain
%
% Input: 
%    
%    sig : vector containing the signal in time domain
%
% Output
%
%    freq_sig : filtered version of original signal in frequency domain
%
% -------------------------------------------------------------------------------------

Nt=length(sig);
ff_sig=fft(sig,Nt);
ff_sig=ff_sig(1:Nt/2)/Nt;
ff_sig(2:end)=2*ff_sig(2:end);
end
