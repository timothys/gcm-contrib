function [R_uu,lags] = calcTemporalCorrelationFunction(u,coeff)
% For the stationary function u(x,t)
% Compute the temporal correlation function
%
%   R_uu( t1,t2 ) = < u(x,t1) u(x,t2) >
%
% or
%
%   R_uu( tau ) = < u(x,t) u(x,t+tau) >
%
% where tau = t1-t2 (Note: by definition R_uu(tau) = R_uu(-tau)
%
% This is equivalent to MATLAB's function xcorr, with the 'unbiased' flag
% (in the case where u is a vector) 
%       R_uu = xcorr(u,'unbiased');
%
% Inputs:
%
%   u:      time series of a stationary field 
%           [1,2,3,4]D, or [3,4]D gcmfaces
%           time is on last dimension
%
% Optional Inputs:  
%   coeff:  default = 0
%           Set to 1 to produce correlation normalize by
%                      R_uu(0) = <u(x,0)^2>
%           producing the correlation coefficient
%
% Output:
%
%   R_uu:   correlation function
%           same dimension as input u with
%           time separation tau on last dimension
%   
%   lags:   time lags associated with the correlation 
%           function
%
% --------------------------------------------------

% --- Optional inputs
if nargin<2
    coeff = 0;
end

% --- Determine if gcmfaces object
isgcmfaces = 0;
if isa(u,'gcmfaces')
    isgcmfaces = 1;
    u = convert2array(u);
end

% --- Sizes etc.
sz = size(u);
Nt = sz(end);
col = repmat(':,',[1 length(sz)-1]);

% --- Compute temporal mean for each time separation tau
%       max Nt time separation given by time series length
lags = 0:Nt-1;
u_auto = cat(length(sz),u,NaN*u);
R_uu = 0*u;

for tau = 1:Nt

    % For 3D
    %tmp = u.*u_auto(:,:,tau + [0:Nt-1]);
    %R_uu(:,:,tau) = nanmean(tmp,length(sz));

    % More generally... 
    eval(sprintf('tmp = u.*u_auto(%stau + [0:Nt-1]);',col));
    eval(sprintf('R_uu(%stau) = nanmean(tmp,length(sz));',col));
end

% --- Normalize by R_uu(0)
if coeff == 1
    eval(sprintf('R_uu = R_uu./repmat(R_uu(%s1),[ones(1,length(sz)-1) Nt]);',col));
end


% --- Return correct form
if isgcmfaces == 1
    R_uu = convert2array(R_uu);
end
end
