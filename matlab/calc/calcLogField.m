function[ladxx,cticks,cticklbls] = calcLogField(adxx, nfact, mygrid, binLims, nBins)
%% Get log of adjoint field for nice plotting
% Inputs: 
%       adxx : gcmfaces object containing (e.g.) sensitivity field
% (opt) mygrid : ...
%
% Output:
%       ladxx : log10 of adxx, where sign(ladxx) = sign(adxx)
% -------------------------------------------------------------------------

if isa(adxx,'gcmfaces'), adxx=convert2gcmfaces(adxx); end
if nargin < 2, fprintf('Need a factor to scale field!\n'); end;
if nargin < 3, establish_mygrid; end
if nargin < 4, binLims=[1 2];end;
if nargin < 5, nBins = 32; end;

%% adxx is not gcmfaces type for this bit
adxx=adxx*nfact;        
sn = sign(adxx);
pos_adxx=adxx;
pos_adxx(adxx<=0)=1/8;
neg_adxx=adxx;
neg_adxx(adxx>=0)=1/8;
neg_adxx=abs(neg_adxx);


% Bin positive and negative parts
binBuckets=linspace(binLims(1),binLims(2),nBins);

% Loop through bins and assin pos/neg adxx appropriately
for i=1:nBins
  if i==1
    pos_adxx(log10(abs(adxx)) < binBuckets(i) & adxx >= 0) = 1/8;
    neg_adxx(log10(abs(adxx)) < binBuckets(i) & adxx <= 0) = 1/8;
  elseif i==nBins
    pos_adxx(log10(abs(adxx)) >= binBuckets(i) & adxx>=0) = binBuckets(i);
    neg_adxx(log10(abs(adxx)) >= binBuckets(i) & adxx<=0) = binBuckets(i);
  else
    ind = log10(abs(adxx)) >= binBuckets(i) & log10(abs(adxx)) < binBuckets(i+1) & adxx>=0;
    pos_adxx(ind) = (10^binBuckets(i) + 10^binBuckets(i+1))/2; 
    ind = log10(abs(adxx)) >= binBuckets(i) & log10(abs(adxx)) < binBuckets(i+1) & adxx<=0;
    neg_adxx(ind) = (10^binBuckets(i) + 10^binBuckets(i+1))/2; 
  end
end


adxx= pos_adxx + neg_adxx;
adxx = adxx.*sn;
adxx=convert2gcmfaces(adxx);


ladxx=log10(pos_adxx)+log10(neg_adxx);
ladxx(ladxx==log10(1/8)| ladxx==2*log10(1/8))=0;
ladxx = ladxx.*sn;
ladxx=convert2gcmfaces(ladxx);

cticks = [-binBuckets(end:-1:1) 0 binBuckets];
cticklbls=[-10.^binBuckets(end:-1:1)/nfact 0 10.^binBuckets/nfact];
end
