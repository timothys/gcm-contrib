function [anom] = calcClimatology(fld,Nyrs,varargin)
% Compute anomaly to a seasonal cycle assuming 
% that fld contains monthly averages along temporal dimension
% 
% Inputs
%
%       fld: [1,2,3,4]D, or [3,4]D gcmfaces
%            time is on last dimension
%
%       Nyrs: number of years for record
%
%  (optional)
%       
%       removeAnnualAvg: 1/0, 1 (default) = remove annual avg
%
% Output
%
%       Same as input dims, time replaced with 12 months
% ---------------------------------------------------------

% Default, do remove annual average
removeAnnualAvg=1;
for n=1:length(varargin)
    removeAnnualAvg=varargin{n};
end

% --- First, compute seasonal cycle
clim = calcClimatology(fld,Nyrs,removeAnnualAvg);

% --- work on array data
isgcmfaces=0;
if isa(fld,'gcmfaces')
    isgcmfaces=1;
    fld  = convert2array(fld);
    clim = convert2array(clim);
end

sz = size(fld);
Nt=sz(end);
if Nyrs*12 ~= Nt
    fprintf('Error: Nyrs * 12 != length of field, input the correct number of years for field\n');
    keyboard
end
  
% --- Repeat climatology for Nyrs
if length(sz)==2
    clim_ext = repmat(clim,[1 Nyrs]);
elseif length(sz)==3
    clim_ext = repmat(clim,[1 1 Nyrs]);
elseif length(sz)==4
    clim_ext = repmat(clim,[1 1 1 Nyrs]);
else
    error('calcClimatology not general to >4D ...');
end

% --- Remove temporal mean
time_mean = nanmean(fld,length(sz));
if length(sz) == 2
    fld = fld - repmat(time_mean,[1 Nt]);
elseif length(sz) == 3
    fld = fld - repmat(time_mean,[1 1 Nt]);
elseif length(sz) == 4
    fld = fld - repmat(time_mean,[1 1 1 Nt]);
end

% --- Now compute anomaly
anom = fld - clim_ext;

% --- If necessary, convert back to gcmfaces
if isgcmfaces==1
    anom = convert2array(anom);
end

end
