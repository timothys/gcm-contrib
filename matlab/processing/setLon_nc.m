function [lon,fld] = setLon_nc(lon,fld,lon_west,lon_east,doPad)
% Set longitude to vary lon_west -> lon_east, 
% default is (180W,180E), where +/- 180 are defined at 
% the international date line
% Shift fld appropriately
%
% Inputs:
%
%   lon: vector giving longitude values
%   fld: 2D field with dims [lat lon]
%
% Optional inputs:
%
%   lon_west: default=-180 = 180W
%             sets "West-most" longitude
%   lon_east: default=180 = 180E
%             sets "East-most" longitude
%   doPad: default = 0 (false)
%          in the case where (lon_west,lon_east) spans
%          more than 360 degrees, enter 1 to repeat data 
%          on either side. E.g. for map projection larger than 360
%
% Outputs:
%
%   lon: column vector varying -180->180
%   fld: 2D field matching longitude layout
%
% ----------------------------------------------------

% Set default longitude bounds and default don't pad for data
% spanning >360 degrees
if nargin<5, doPad = 0; end;
if nargin<4, lon_east = 180; end;
if nargin<3, lon_west =-180; end;

% Make sure longitude is a column vector
if size(lon,2) > size(lon,1)
    lon = lon';
end
if size(lon,2) ~= 1
    error('lon input: longitude must be passed as column vector'); 
end

% Make sure fld is [lat,lon, ...]
if size(fld,1) == length(lon)
    lsz = length(size(fld));
    if lsz > 2
        fld = permute(fld, [2 1 [3:lsz]]);
    else
        fld = permute(fld, [2 1]);
    end
end

% Grab current longitude bounds
lon_max = max(lon);
lon_min = min(lon);

% Check if padding data to span > 360degrees (e.g. for plot
if doPad == 1 && abs(lon_east-lon_west)>360
    doPad = 1;
end

if lon_west < lon_min

    % Shift eastmost points to be "more west" than lon_min
    dLon = abs(lon_min - lon_west);

    % Shift max and min
    lon_max = lon_max - dLon;
    lon_min = lon_min - dLon;

    % Create a mask denoting points in this band, necessary for sub 1degree data
    lon_mask = lon > lon_max & lon <= lon_max+dLon;
    Nlon = sum(lon_mask);

    % Shift the actual longitude values
    lon(lon>lon_max) = lon(lon>lon_max) - 360;

    % Shift the position of values in their vectors
    lon = circshift(lon,Nlon);
    fld = circshift(fld,[0 Nlon]);
    
    if doPad==1
        % Now pad data if desired
        dLon_pad = abs(lon_east - lon_max);
        lon_mask_pad = lon > lon_west & lon <=lon_west + dLon_pad;
        Nx_pad = sum(lon_mask_pad);

        fld = cat(2,fld,fld(:,1:Nx_pad));
        lon = [lon;lon(1:Nx_pad)+360];
    end

elseif lon_east > lon_max

    % Shift westmost points to be "more east" than lon_max
    dLon = abs(lon_max - lon_east)

    % Shift max and min
    lon_max = lon_max + dLon;
    lon_min = lon_min + dLon;

    % Create a mask denoting points in this band, necessary for sub 1degree data
    lon_mask = lon < lon_min & lon >= lon_min-dLon;
    Nlon = sum(lon_mask)

    % Shift the actual longitude values
	lon(lon<lon_min)=lon(lon<lon_min)+360;

    % Shift the position of values in their vectors
	lon = circshift(lon,-Nlon);
	fld = circshift(fld,[0 -Nlon]);

    if doPad==1
        % Now pad data if desired
        dLon_pad = abs(lon_west - lon_min);
        lon_mask_pad = lon < lon_east & lon >=lon_east - dLon_pad;
        Nx_pad = sum(lon_mask_pad);

        fld = cat(2,fld(:,1:Nx_pad),fld);
        lon = [lon(1:Nx_pad)-360;lon];
    end
end

end
