function [mygrid] = establish_mygrid(grid_name)
% Load mygrid variable 
%
% Input:
%
%   grid_name: string indicating which grid. Options are:
%       'llc90', 'pig_08', 'pig_32', 'ase'    
% 
% Sets global variable mygrid
%
% ----------------------------------------------------------------

global mygrid;

if isempty(mygrid)

    !hostname > hostname.txt
    !echo ~ > home.txt
    hh=char(textread('hostname.txt','%s'));
    ho=char(textread('home.txt','%s'));
    delete('hostname.txt');
    delete('home.txt');
  
    % If grid directory not specified
    % default to ECCO llc90 grid
    if ismac
        gridDir = '/Users/tim/work/grids/';
    elseif ~isempty(strfind(hh,'ekman'))
    	gridDir='/workspace/grids/';
    elseif ~isempty(strfind(hh,'sverdrup')) || ~isempty(strfind(hh,'cluster'))
        error('Need to set this up on sverdrup');
    elseif ~isempty(strfind(ho,'03754'))
        gridDir='/work/03754/tsmith/grids/';
    else
        error('Machine not recognized in establish_mygrid :( ...\n');
    end
  
    if strcmp(grid_name,'llc90')
        nFaces=5;
        fileFormat='compact';
    else
        nFaces=1;
        fileFormat='straight';
    end
    
  
    grid_load([gridDir grid_name '/'],nFaces,fileFormat);
    gcmfaces_global;

    % llc90: Want latitude/line masks for transport quantities
    % this was commented from recent gcmfaces version
    if strcmp(grid_name,'llc90')
        if ~isfield(mygrid,'mygrid.LATS_MASKS');
            gcmfaces_lines_zonal;
            mygrid.LATS=[mygrid.LATS_MASKS.lat]';
        end;
        if ~isfield(mygrid,'LINES_MASKS');
            [lonPairs,latPairs,names]=gcmfaces_lines_pairs;
            gcmfaces_lines_transp(lonPairs,latPairs,names);
        end;
    end

    % For gcmfaces objects with only one face, remove face object structure
    if nFaces==1
        tmpCell = struct2cell(mygrid);

        for i = 1:length(tmpCell)
            if isa(tmpCell{i},'gcmfaces')
                tmpCell{i}=tmpCell{i}.f1;
            end
        end

        mygrid = cell2struct(tmpCell,fieldnames(mygrid),1); 
    end

end
end
