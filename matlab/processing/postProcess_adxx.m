function[] = postProcess_adxx(runStr, adjLoadDir, adjField, mygrid, nFact, klev, Xinterp,adjDump )
% Inputs: 
%
%       runStr : string for particular run 
%               e.g. 'dec.240mo/'
%	adjLoadDir : location of adxx_*.data files 
%               e.g. '../../results/samoc/dec.240mo/'
%       adjFields : tauu, tauv, aqh, precip ... 
%               e.g. 'hflux'
%       mygrid : yup
%
% Optional Inputs
%
%       nFact : e.g. 10^-6 for -> Sv
%       klev : level for 3d terms
%	Xinterp : for interpolating output, typically not used
%	adjDump : 0 => used ecco/ctrl pkg for adxx_<> output
%		  1 => used good old fashioned dump output
%
% Output:
%
%       adxx : the field as gcmfaces object after post processing
% -------------------------------------------------------------------------

%% 1. Preliminaries
if ~strcmp(adjLoadDir(end),'/'), adjLoadDir=[adjLoadDir '/']; end;
saveDir = ['mat/' runStr];
if ~exist(saveDir,'dir'), mkdir(saveDir); end;
if ~exist('mygrid','var'), establish_mygrid; end;
if ~isa(adjField,'cell'),adjField={adjField}; end;
Nadj = length(adjField);

%% Optional stuff
if ~exist('klev','var'), klev=1; end;
if ~exist('Xinterp','var')
    Xinterp = {0,0};
end
if ~exist('adjDump','var'), adjDump=0; end;
% -------------------------------------------------------------------------

for i = 1:Nadj
    %% First check if it exists
    adjFile = sprintf('%sadj_%s.mat',saveDir,adjField{i});
    resultFile = sprintf('%sadxx_%s.0000000012.data',adjLoadDir,adjField{i});
    
    if ~exist(adjFile,'file') && ~isempty(dir(resultFile))
        %% Load from file
        if ~adjDump
            fprintf('Reading file: %s ...\n',resultFile);
            adxx = read_bin(resultFile);
        else
            if strcmp(adjField{i},'salt') || strcmp(adjField{i},'theta')
                adxx = rdmds2gcmfaces([adjLoadDir 'ADJustress'],NaN);
                len_adxx = size(adxx.f1,3);
                for n = 1:len_adxx
                    tmp = rdmds2gcmfaces([adjLoadDir 'ADJ' adjField{i}],n);
                    adxx(:,:,n) = tmp(:,:,klev);
                end
            elseif strcmp(adjField{i},'tauu') || strcmp(adjField{i},'tauv')
                if ~exist('adxxU','var')
                    adxxU = rdmds2gcmfaces([adjLoadDir 'ADJustress'],NaN);
                    adxxV = rdmds2gcmfaces([adjLoadDir 'ADJvstress'],NaN);
                    len_adxx = size(adxxU.f1,3);
                    adxxU=adxxU.*repmat(mygrid.mskW(:,:,1),[1 1 len_adxx]);
                    adxxV=adxxV.*repmat(mygrid.mskS(:,:,1),[1 1 len_adxx]);
                    for n = 1:len_adxx
                        [adxxU(:,:,n),adxxV(:,:,n)] = calc_UEVNfromUXVY(adxxU(:,:,n),adxxV(:,:,n));
                    end
                    adxx = adxxU;
                else
                    adxx = adxxV;
                end
            else
                adxx = rdmds2gcmfaces([adjLoadDir 'ADJ' adjField{i}],NaN);
            end
        end
        

%         %% Normalize by area or volume
%         dxg = mk3D(mygrid.DXG,adxx);
%         dyg = mk3D(mygrid.DYG,adxx);
%         if strcmp(adjField{i},'salt') || strcmp(adjField{i},'theta')
%             if adjDump
%                 drf = mygrid.DRF(klev);
%             else
%                 drf = mk3D(mygrid.DRF,adxx);
%             end
%             adxx = adxx./dxg./dyg ./ drf;
%         else
%             adxx = adxx./dxg./dyg;
%         end        

        %% Factor 
        adxx = adxx*nFact;
        if strcmp(adjField{i},'tauu') || strcmp(adjField{i},'tauv')
            adxx = -1*adxx;
        end
    
        %% Interpolate along 3rd dim
        if Xinterp{1}(1)==0 || strcmp(adjField{i},'salt') || strcmp(adjField{i},'theta')
            fprintf('** adxx post process: no interpolation\n');
        else
            adxx = gcmfaces_interp_1d(3, Xinterp{1}, adxx, Xinterp{2});
        end
        
        %% Save as mat file
        save(adjFile,'adxx')

	%% Print to screen 
	fprintf('## Post Processing: %s written. ##\n',adjFile);
    end
end
end
