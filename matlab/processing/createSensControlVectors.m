function [] = createSensControlVectors()
% Purpose is to make control vectors for sensitivity run full of zeros

mygrid = establish_mygrid('llc90');
ctrlDir = 'control-vectors/'; if ~exist(ctrlDir,'dir'), mkdir(ctrlDir), end
ctrlVects={'tauu','tauv','aqh','atemp','swdown','lwdown','precip','runoff','salt','theta'};
prefix = [ctrlDir 'xx_'];
suffix = '.0000000012.data';

if nargin<1, establish_mygrid; end;

%% Make one for each time step over 400 time steps
Nt = 700;
len_xx = length(ctrlVects);

%% First, wind stress
% Across west faces faces
xx = 0*mygrid.mskW(:,:,1);
xx=convert2gcmfaces(xx);
xx(isnan(xx))=0;
xx=repmat(xx,1,1,Nt);

write2file([prefix ctrlVects{1} suffix],xx);
write2meta([prefix ctrlVects{1} suffix],size(xx));

% Across south faces
xx = 0*mygrid.mskS(:,:,1);
xx=convert2gcmfaces(xx);
xx(isnan(xx))=0;
xx=repmat(xx,1,1,Nt);

write2file([prefix ctrlVects{2} suffix],xx);
write2meta([prefix ctrlVects{2} suffix],size(xx));

% Cell centers
xx=0*mygrid.mskC(:,:,1);
xx=convert2gcmfaces(xx);
xx(isnan(xx))=0;
xx=repmat(xx,1,1,Nt);

for i = 3:len_xx-2
    write2file([prefix ctrlVects{i} suffix], xx);
    write2meta([prefix ctrlVects{i} suffix], size(xx));
end

%% Now, initial salinity / temp.
% static in time
xx = 0*mygrid.mskC;
xx=convert2gcmfaces(xx);
xx(isnan(xx))=0;

for i = len_xx-1:len_xx
    write2file([prefix ctrlVects{i} suffix], xx);
    write2meta([prefix ctrlVects{i} suffix], size(xx));
end

%% Tar & zip for sending around 
tar('control-vectors.tar.gz',ctrlDir);
end
