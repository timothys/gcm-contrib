function [iters] = grabAllIters( searchDir, fileName )
%
% MITgcm output (e.g. from diagnostics) is listed with iteration stamps
% This script grabs all iteration numbers in 'searchDir'.
% 
% Input:
%
% 	searchDir = directory to find output files, with iter numbers
% 	fileName = base of file name 
%
% Output:
%
%   Iters = vector of iterations for files
%
%	Ex. 
%	searchDir = '/workspace/results/samoc/hf-perturb/diags/'
%	fileName = 'state_2d_set1'
%
%	in directory: state_2d_set1.0000000732.(meta/data)
%		      state_2d_set1.0000001428.(meta/data)
%
%	iters = [732, 1428];
% ------------------------------------------------------------

%% Grab all the iteration numbers from the directory
if ~strcmp(searchDir(end),'/')
	searchDir = [searchDir '/'];
end

list = dir(sprintf('%s*.meta',[searchDir fileName]));
fileList = cell(length(list),1);
for n=1:length(list)
    fileList{n}=list(n).name;
end
Nt = length(fileList);

iters=zeros(1,Nt);

for n = 1:Nt
	% Iteration is the 42-51st digit
	iters(n) = str2num(fileList{n}(end-14:end-5));
end
end
