function [] = plotAdjMean(loadDir,saveDir,mygrid,deseasonFlag,saveFig)
% Plot mean of adjoint sensitivity across globe
% 
%   Inputs: 
%       loadDir : where to load data from
%       saveDir : where to save figure
%       mygrid : 
%       deseasonFlag : 1 = take out seasonal signal, 0 = don't
%       saveFig : 
% -------------------------------------------------------------------------

if nargin<4, deseasonFlag=0; end
if nargin<5, saveFig=0; end

adjField = {'tauu','tauv','aqh','atemp','swdown','lwdown','precip','runoff','hflux','sflux'};
Nadj = length(adjField);
msks=createRegionMasks;

for i = 1:Nadj
  adjFile = sprintf('%s/adj_%s.mat',loadDir,adjField{i});	
  if exist(adjFile,'file')
    if deseasonFlag
      figFile = sprintf('%s/adjMean_%s_deseasoned',saveDir,adjField{i});
    else
      figFile = sprintf('%s/adjMean_%s',saveDir,adjField{i});
    end
    load(adjFile);
    Nt = size(adxx.f1,3);
    adxx = adxx(:,:,1:Nt);
    
    % Global mean
    adjMean = computeRegionMean( adxx, mygrid.mskC(:,:,1), mygrid);
    if deseasonFlag, adjMean=removeSeasonality(adjMean); end
    
    % ACC
    adjMeanAcc = computeRegionMean( adxx, msks.acc, mygrid );
    if deseasonFlag, adjMeanAcc=removeSeasonality(adjMeanAcc); end
    
    % Arctic
    adjMeanArc = computeRegionMean(adxx,msks.arc,mygrid);
    if deseasonFlag, adjMeanArc=removeSeasonality(adjMeanArc); end
    
    % North box
    adjMeanNorth = computeRegionMean(adxx,msks.atl.subPolar,mygrid);
    if deseasonFlag, adjMeanNorth=removeSeasonality(adjMeanNorth); end
    
    % Indian Ocean
    adjMeanInd = computeRegionMean(adxx,msks.ind.full,mygrid);
    if deseasonFlag, adjMeanInd=removeSeasonality(adjMeanInd); end
    
    % Atlantic from 60S to 40N
    adjMeanAtl = computeRegionMean(adxx,msks.atl.full-msks.atl.subPolar,mygrid);
    if deseasonFlag, adjMeanAtl=removeSeasonality(adjMeanAtl); end
    
    % Pacific from 60S
    adjMeanPac = computeRegionMean(adxx,msks.pac.full,mygrid);
    if deseasonFlag, adjMeanPac=removeSeasonality(adjMeanPac); end

    %% Plot it up 
    figureW;
%     if deseasonFlag, Nt=Nt-1; end
    t = [1:Nt] ./ 12;
    plot(t, adjMean,t,adjMeanArc, t, adjMeanNorth, t, adjMeanAtl, ...
         t, adjMeanAcc, t, adjMeanInd, t, adjMeanPac)
    legend('Full Mean','Arctic','NASP','Atlantic 60S-45N','<60S',...
        'Indian >60S','Pacific >60S','location','best')
    xlabel('Years')
    if strcmp(adjField{i},'tauu') || strcmp(adjField{i},'tauv')
        xlim([Nt-36 Nt]./12)
    else
        xlim([0 20])
    end
    ylabel('Spatial Mean ( dJ/du(t) )')
    title(sprintf('Mean of %s sens.',adjField{i}))
    set(gcf,'paperorientation','landscape')
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])
    
    if saveFig, saveas(gcf,figFile,'pdf'); end
    close;
  end
end
