function [] = savePlot(savename,varargin);
% Save a figure to file
%
% Input:
%
%   savename: filename to store figure under
%
% Optional input:
%
%   doCrop: 'doCrop' (default), or 'noCrop' (crops out extra space)
%   backgroundColor: 'transparent' (default), or 1x3 RGB array
%   exportOpt: 'export_fig' (default), 'saveas'
%   fileFormat: 'png' (default), 'pdf','eps' ...
%   layout: 'landscape' (default), 'portrait'
%
% -----------------------------------------------

fileFormat='png';
layout='landscape';
exportOpt='export_fig';
backgroundColor='transparent';
doCrop='doCrop';
resolution=[];
for n=1:length(varargin)
    if ~isempty(strfind(varargin{n},'landscape')) || ~isempty(strfind(varargin{n},'portrait'))
        layout=varargin{n};
    elseif ~isempty(strfind(varargin{n},'export')) || ~isempty(strfind(varargin{n},'saveas'))
        exportOpt=varargin{n};
    elseif ~isempty(strfind(varargin{n},'transparent')) || ( size(varargin{n},2)==3 && isa(varargin{n},'double'))
        backgroundColor=varargin{n};
    elseif ~isempty(strfind(lower(varargin{n}),'crop'))
        doCrop=varargin{n};
    elseif ~isempty(strfind(lower(varargin{n}),'-r'))
        resolution=varargin{n};
    else
        fileFormat=varargin{n};
    end
end

% --- Set default resolution based on file format
if isempty(resolution)
    if strcmp(fileFormat,'png') 
        resolution = '-r300';
        fprintf('Setting default resolution for bitmap to 300ppi\n');
    elseif strcmp(fileFormat,'pdf') 
        resolution = '-r900';
        fprintf('Setting default resolution for vector to 900ppi\n');
    else
        error('Need to add other file types for resolution default...\n')
    end
end





if strcmp(exportOpt,'export_fig')
    if ~strcmp(fileFormat(1),'-')
        fileFormat = ['-' fileFormat];
    end
    if size(backgroundColor,2)==3
        set(gcf,'color',backgroundColor)
    else
        if ~strcmp(backgroundColor(1),'-')
            backgroundColor = ['-' backgroundColor];
        end
    end
    if ~strcmp(doCrop(1),'-')
        doCrop = ['-' doCrop];
    end

    if isempty(strfind(lower(doCrop),'nocrop')) && size(backgroundColor,2)==3
        export_fig(savename,fileFormat,resolution);
    elseif ~isempty(strfind(lower(doCrop),'nocrop')) && size(backgroundColor,2)==3
        export_fig(savename,fileFormat,doCrop,resolution);
    elseif isempty(strfind(lower(doCrop),'nocrop')) && strcmp(backgroundColor,'transparent')
        export_fig(savename,fileFormat,backgroundColor,resolution);
    else
        export_fig(savename,fileFormat,backgroundColor,doCrop,resolution);
    end

elseif strcmp(exportOpt,'saveas')
    set(gcf,'paperorientation',layout)
    set(gcf,'paperunits','normalized')
    set(gcf,'paperposition',[0 0 1 1])
    saveas(gcf,savename,fileFormat)
end
fprintf('Saved plot: %s ...\n',savename);

end
