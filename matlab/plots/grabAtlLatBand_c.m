function[latC,latX,latR] = grabAtlLatBand_c(lat, fld, varargin)
% Produces a 2d field of values along a specified latitude 
%  Grabs the entire latitude band. To mask interior points, 
%  give optional mskC or mskK arguments
%
% Restriction: can only work in the Atlantic 
% or can only involve gcmfaces 1 & 5
%
% Note: this ignores NaNs,  keeps zeros
%
% Inputs: 
%
%   lat : type=double, scalar
%           latitude to grab zonal/depth profile
%
%   fld : type=gcmfaces, 3D field in space
%            contains values desired for midsection returned field
%
% Optional Inputs:
%
%   mskC/k : type=gcmfaces, 2D field in space
%           Default: all ones in interior
%           Used to mask interior points
%           Created from mygrid, see ../../samoc/create_masks.m for example
%
% Outputs: 
%   latC : type=2d matrix of fld across lat band
%            dims: [Nr, Nx] Nx: longitudinal basin width, Nr: depth
%
%   latX : type=vector of longitudinal points where fld points live
%            dims: [1, Nx] Nx: longitudinal basin width
%   
%   latR : type=vector of depth points where fld points live
%            dims: [Nr, 1] Nr: Nr: depth
%-----------------------------------------------------------------------

% Get Atlantic and latitude masks first
mygrid = establish_mygrid('llc90');
atlBasin = v4_basin('atlExt');
iy = find(mygrid.LATS==lat);
latMskC = mygrid.LATS_MASKS(iy).mskCedge.*atlBasin;

[fld,mskC,mskK] = processInputs(fld,latMskC,varargin);

% Remove all non Atlantic points, and points not on lat band
latMskC(latMskC==0)=NaN;
latIndC = find(~isnan(convert2array(latMskC)));

Nx = length(latIndC);
Nr = size(fld.f1,3);
if Nr ~= length(mskK), error('length(mskK) != fld Nr, exiting'); return; end;

% Make containers
latC = zeros(Nr,Nx);
latX = zeros(1,Nx);
NxFaceC = zeros(5,1);

%% First for gcmface 5
for iFace = 5:-4:1
    
    tmpC = latMskC{iFace};
    latIndC = find(~isnan(tmpC));

    NxFaceC(iFace)=length(latIndC);

    if iFace == 5
        bigInd = 1;
    else
        bigInd = 1+NxFaceC(5);
    end

    [li,lj] = ind2sub(size(tmpC),latIndC);

    for i = 1:length(li)
        for k=1:Nr
            latC(k,bigInd) = fld{iFace}(li(i),lj(i),k).*mskC{iFace}(li(i),lj(i))*mskK(k);
        end
        latX(bigInd) = mygrid.XC{iFace}(li(i),lj(i));
        bigInd = bigInd + 1;
    end

end%end iFace=5->1

% Fill latR 
if Nr == 50
    latR = mygrid.RC;
elseif NR == 51
    latR = mygrid.RF;
else
    fprintf(' Not sure how to fill latR ... \n');
    latR = [];
end
end

%-----------------------------------------------------------------------

function [fld,mskC,mskK] = processInputs(fld,latMskC,varg)
% Deal with optional inputs 

global mygrid;
Nr = length(mygrid.RC);
mskC = latMskC;
mskK = ones(Nr,1);
for n = 1:length(varg)
    if isa(varg{n},'gcmfaces') || size(varg{n},1)==360 || size(varg{n},1)==90
        mskC = varg{n};
    else
        mskK = varg{n};
    end
end

% Deal with types
if ~isa(fld,'gcmfaces')
    if size(fld,1)~=360
        % Not an array
        fld = convert2gcmfaces(fld);
    else
        fld = convert2array(fld);
    end
end

if ~isa(mskC,'gcmfaces')
    if size(mskC,1)~=360
        % Not an array
        mskC = convert2gcmfaces(mskC);
    else
        mskC = convert2array(mskC);
    end
end
end
