function []=m_map_nc(lat,lon,fld,varargin);
%object:    gcmfaces front end to m_map
%           Mimic with Netcdf data
%
% NEED TO MAKE THIS NICER ... but moving on for now... 
% as written, has to use longitude 0->360. 
% only tested projection is 4.5, since that's what I need
%       
%
%
%inputs:    
%           lat - latitude from netcdf file
%           lon - longitude from netcdf
%           fld is the 2D field to be mapped, or a cell (see below).
%
% Otherwise, see m_map_atl or m_map_gcmfaces

%check that m_map is in the path
aa=which('m_proj'); if isempty(aa); error('this function requires m_map that is missing'); end;

%get optional parameters
if nargin>1; proj=varargin{1}; else; proj=1.4; end;
if iscell(proj);
    error('not yet implemented');
else;
    choicePlot=proj;
end;;
%determine the type of plot
if iscell(fld); myPlot=fld{1}; else; myPlot='pcolor'; fld={'pcolor',fld}; end;
%set more optional paramaters to default values
myCaxis=[]; myTitle=''; myShading='interp'; myCmap='jet';
do_m_coast=1; doHold=0; doCbar=1; doLabl=1; doFit=0;
fontSize=14;
%set more optional paramaters to user defined values
for ii=2:length(varargin);
    if ~iscell(varargin{ii});
        warning('inputCheck:m_map_gcmfaces_1',...
            ['As of june 2011, m_map_gcmfaces expects \n'...
            '         its optional parameters as cell arrays. \n'...
            '         Argument no. ' num2str(ii+1) ' was ignored \n'...
            '         Type ''help m_map_gcmfaces'' for details.']);
    elseif ~ischar(varargin{ii}{1});
        warning('inputCheck:m_map_gcmfaces_2',...
            ['As of june 2011, m_map_gcmfaces expects \n'...
            '         its optional parameters cell arrays \n'...
            '         to start with character string. \n'...
            '         Argument no. ' num2str(ii+1) ' was ignored \n'...
            '         Type ''help m_map_gcmfaces'' for details.']);
    else;
        if strcmp(varargin{ii}{1},'myCaxis')|...
                strcmp(varargin{ii}{1},'myCmap')|...
                strcmp(varargin{ii}{1},'myShading')|...
                strcmp(varargin{ii}{1},'myTitle')|...
                strcmp(varargin{ii}{1},'doHold')|...
                strcmp(varargin{ii}{1},'doCbar')|...
                strcmp(varargin{ii}{1},'doLabl')|...
                strcmp(varargin{ii}{1},'doFit')|...
                strcmp(varargin{ii}{1},'do_m_coast')|...
                strcmp(varargin{ii}{1},'fontSize');
            eval([varargin{ii}{1} '=varargin{ii}{2};']);
        else;
            warning('inputCheck:m_map_gcmfaces_3',...
                ['unknown option ''' varargin{ii}{1} ''' was ignored']);
        end;
    end;
end;

%make parameter inferences
if length(myCaxis)==0;
    plotCBAR=0;
elseif length(myCaxis)==2;
    plotCBAR=1;
else;
    plotCBAR=2;
end;
%
if choicePlot==0&~doHold;
    clf;
elseif ~doHold;
    cla;
else;
    hold on;
end;

%re-group param:
param.plotCBAR=plotCBAR;
param.myCmap=myCmap;
param.myCaxis=myCaxis;
param.do_m_coast=do_m_coast;
param.doHold=doHold;
param.doCbar=doCbar;
param.doLabl=doLabl;
param.doFit=doFit;
param.myPlot=myPlot;
param.fontSize=fontSize;

%do the plotting:
if (choicePlot~=0&choicePlot~=1&choicePlot~=2&choicePlot~=3);
    do_my_plot(fld,lat,lon,param,choicePlot,myShading);
end;%if choicePlot==0|choicePlot==1;

if choicePlot==0; subplot(2,1,1); end;
if choicePlot==0|choicePlot==1;
       do_my_plot(fld,lat,lon,param,1,myShading);
end;%if choicePlot==0|choicePlot==1;

if choicePlot==0; subplot(2,2,3); end;
if choicePlot==0|choicePlot==2;
    do_my_plot(fld,lat,lon,param,2,myShading);
end;%if choicePlot==0|choicePlot==1;

if choicePlot==0; subplot(2,2,4); end;
if choicePlot==0|choicePlot==3;
    do_my_plot(fld,lat,lon,param,3,myShading);
end;%if choicePlot==0|choicePlot==1;

if plotCBAR==2&strcmp(myPlot,'pcolor')&doCbar;
    cbar=gcmfaces_cmap_cbar(myCaxis,{'myCmap',myCmap});
    if choicePlot==0;
        set(cbar,'Position',[0.88 0.15 0.02 0.75]);
    elseif choicePlot==-1;
        set(cbar,'Position',[0.88 0.35 0.02 0.3]);
    elseif choicePlot==1;
        set(cbar,'Position',[0.88 0.34 0.02 0.35]);
    elseif choicePlot==1.2;
        set(cbar,'Position',[0.88 0.34 0.02 0.35]);
    else;
        set(cbar,'Position',[0.88 0.3 0.02 0.4]);
    end;
end;

if doFit;
    if doLabl&~doCbar; 
        set(gca,'LooseInset',[0.05 0.02 0.03 0.03]);
    else; 
        set(gca,'LooseInset',[0.01 0 0.03 0.03]);
    end;
    tmp1=get(gca,'PlotBoxAspectRatio');
    set(gcf,'PaperPosition',[0 4 8 8/tmp1(1)]);
end;

function []=do_my_plot(fld,lat,lon,param,proj,shad);

%default m_grid params:
xloc='bottom'; xtic=[-180:60:180]; xticlab=1;
yloc='left';   ytic=[-60:30:60];   yticlab=1;
% the rightmost bound of the map
% important for projections which span > 360
lon_right=0;

%choice of projection:
if proj==-1;
    %%m_proj('Miller Cylindrical','lat',[-90 90]);
    %m_proj('Equidistant cylindrical','lat',[-90 90]);
    %m_proj('mollweide','lon',[-180 180],'lat',[-80 80]);
    m_proj('mollweide','lon',[-180 180],'lat',[-88 88]);
    myConv='pcol'; xticlab=0; yticlab=0;
elseif proj==1;
    m_proj('Mercator','lat',[-70 70]);
    myConv='pcol';
elseif proj==1.1;
    m_proj('Mercator','lat',[-70 70],'lon',[0 360]+20);
    myConv='pcol';
    xtic=[-360:60:360]; ytic=[-60:30:60];
elseif proj==1.2;
    m_proj('Equidistant cylindrical','lat',[-90 90],'lon',[0 360]+20);
    myConv='pcol';
    xtic=[-360:60:360]; ytic=[-90:30:90];
elseif proj==1.3;
    m_proj('Equidistant cylindrical','lat',[15 30],'lon',[190 210]);
    myConv='pcol';
    xtic=[190:5:220]; ytic=[15:3:30];
elseif proj==1.4;
    m_proj('Equidistant cylindrical','lat',[-90 90],'lon',[240 380]+20);
    myConv='pcol';
    xtic=[140:40:360]; ytic=[-90:30:90];
elseif proj==2;
    m_proj('Stereographic','lon',0,'lat',90,'rad',40);
    myConv='convert2arctic';
    yloc='bottom'; ytic=[50:10:90];

elseif proj==2.1;
    m_proj('Stereographic','lon',0,'lat',90,'rad',60);
    myConv='convert2arctic';
    yloc='bottom'; ytic=[30:10:90];
elseif proj==3;
    m_proj('Stereographic','lon',0,'lat',-90,'rad',40);
    myConv='convert2southern';
    xloc='top'; ytic=[-90:10:-50];
elseif proj==3.1;
    m_proj('Stereographic','lon',0,'lat',-90,'rad',60);
    myConv='convert2southern';
    xloc='top'; ytic=[-90:10:-30];

    % North Atlantic
elseif proj==4.1;
    m_proj('mollweide','lat',[25 75],'lon',[-100 30]);
    myConv='pcol';
    xtic=[-100:20:30]; ytic=[30:10:70];

    % Tropical Atlantic
elseif proj==4.2;
    m_proj('mollweide','lat',[-30 30],'lon',[-65 20]);
    myConv='pcol';
    xtic=[-60:10:20]; ytic=[-30:10:30];

    % South Atlantic
elseif proj==4.3;
    m_proj('mollweide','lat',[-75 -25],'lon',[-75 25]);
    myConv='pcol';
    xtic=[-70:20:20]; ytic=[-70:10:-30]; xloc='top';

    % North Pacific
elseif proj==4.4;
    m_proj('mollweide','lat',[25 75],'lon',[-240 -110]);
    myConv='pcol';
    xtic=[-240:20:-120]; ytic=[30:10:60]; 

    % Equatorial Pacific
elseif proj==4.5;
    lon_left = -239.5;
    lon_right = -70.5;
    m_proj('mollweide','lat',[-30 30],'lon',[lon_left lon_right]);
    myConv='pcol';
    xtic=[(floor(lon_left)+20):20:ceil(lon_right)]; ytic=[-80:10:80]; xloc='bottom';

    % South Pacific
elseif proj==4.6;
    m_proj('mollweide','lat',[-75 -25],'lon',[-215 -60]);
    myConv='pcol';
    xtic=[-210:20:-70]; ytic=[-70:10:-30]; xloc='top'; 

    % Indian
elseif proj==4.7;
    m_proj('mollweide','lat',[-30 30],'lon',[15 155]);
    myConv='pcol';
    xtic=[10:20:160]; ytic=[-30:10:30]; xloc='top'; 

    % South Indian
elseif proj==4.8;
    m_proj('mollweide','lat',[-75 -25],'lon',[15 155]);
    myConv='pcol';
    xtic=[10:20:160]; ytic=[-70:10:-30]; xloc='top';

    % Broad South Atlantic
elseif proj==4.9
    m_proj('mollweide','lat',[-85 35],'lon',[-105 65]);
    myConv='pcol';
    xtic=[-100:40:60]; ytic=[-80:20:30]; xloc='top';

    % Full Atlantic
elseif proj==4.95
    m_proj('mollweide','lat',[-75 75],'lon',[-105 45]);
    myConv='pcol';
    xtic=[-100:40:40]; ytic=[-80:20:70]; xloc='top';

% --- SAMOC 
elseif proj == 5
    m_proj('mollweide','lat',[-80 80],'lon',[-220 160]);
    myConv='pcol';
    xtic=[-200:40:160]; ytic=[[-80:20:-40],-34, [-20:20:80]]; xloc='bottom';
    xticlab=0;
elseif proj == 5.1
    m_proj('mollweide','lat',[-80 80],'lon',[-220 160]);
    myConv='pcol';
    xtic=[-200:40:160]; ytic=[[-80:20:-40],-34, [-20:20:80]]; xloc='bottom';
    xticlab=0;
    yticlab=0;
elseif proj == 5.2
    lon_left = -60;
    lon_right = 30;
    lat_lower = -50;
    lat_upper = -20;
    m_proj('mollweide','lat',[lat_lower lat_upper],'lon',[lon_left lon_right]);
    myConv='pcol';
    xtic=[lon_left:20:lon_right]; 
    ytic=[[lat_lower:5:-40],-34, [-30:5:lat_upper]]; 
    xloc='bottom';
elseif proj == 5.3
    lon_left = -60;
    lon_right = 30;
    lat_lower = -50;
    lat_upper = -20;
    m_proj('Mercator','lat',[lat_lower lat_upper],'lon',[lon_left lon_right]);
    myConv='pcol';
    xtic=[-40:20:30]; ytic=[[-50:5:-20]]; xloc='bottom';

% --- AMOC @ 25N 
elseif proj == 6
    m_proj('mollweide','lat',[-80 80],'lon',[-235 115]);
    myConv='pcol';
    xtic=[-200:40:110]; ytic=[[-80:20:20], 26, [40:20:80]]; xloc='bottom';
    xticlab=0;
elseif proj == 6.1
    m_proj('mollweide','lat',[-80 80],'lon',[-235 115]);
    myConv='pcol';
    xtic=[-200:40:110]; ytic=[[-80:20:20], 26, [40:20:80]]; xloc='bottom';
    xticlab=0;
    yticlab=0;

% --- Focus on N. Atl. Subpolar

elseif proj==7;
    m_proj('mollweide','lat',[35 85],'lon',[-100 30]);
    myConv='pcol';
    xtic=[-100:20:30]; ytic=[40:10:70];

elseif proj == 7.1
    m_proj('Stereographic','lon',-30,'lat',90,'rad',40);
    myConv='convert2arctic';
    yloc='bottom'; ytic=[50:10:90];

% --- Globe without latitude band picked out
elseif proj == 8
    lon_left = -220;
    lon_right = 160;
    m_proj('mollweide','lat',[-80 80],'lon',[lon_left lon_right]);
    myConv='pcol';
    xtic=[(lon_left+20):40:lon_right]; ytic=[-80:20:80]; xloc='bottom';
    xticlab=0;
elseif proj ==8.1
    lon_left = -210;
    lon_right = 150;
    m_proj('mollweide','lat',[-80 80],'lon',[lon_left lon_right]);
    myConv='pcol';
    xtic=[(lon_left+10):40:(lon_right-10)]; ytic=[-80:20:80]; xloc='bottom';
    xticlab=0;
else;
    error('undefined projection');
end;

if ~param.doLabl; xticlab=0; yticlab=0;end; %omit labels
ff=param.fontSize;
m_grid_opt=['''XaxisLocation'',xloc,''YaxisLocation'',yloc,''xtick'',xtic,''ytick'',ytic,''fonts'',ff'];
if xticlab==0; m_grid_opt=[m_grid_opt ',''xticklabel'',[]']; end;
if yticlab==0; m_grid_opt=[m_grid_opt ',''yticklabel'',[]']; end;

if strcmp(param.myPlot,'pcolor')|strcmp(param.myPlot,'contour');

    % Ensure working with column vectors
    if size(lat,2)>size(lat,1)
        lat=lat';
    end
    if size(lon,2)>size(lon,1)
        lon=lon';
    end

    % Now make sure they are vectors, not matrices
    if (size(lat,2) ~= 1) || (size(lon,2) ~= 1) 
        error('(lat, lon) must be provided as vectors');
    end

    % Catch projections which haven't been updated with proper bounds
    if ~exist('lon_left','var')
        error('Need to define bounds lon_left, lon_right for this projection.\n See projetion 8 for example...\n')
    end

    % Account for bounds of plot being outside (-180,180) range 
    % Note that if plot exceeds both, data will be shifted to range
    % (lon_left, lon_max), where lon_max is the "new" shifted max longitude. 
    % Then, data will be repeated (padded) on this side.
    z=fld{2};
    if (lon_right - lon_left) > 360
        doPad = 1;
    else
        doPad = 0;
    end
    [lon,z] = setLon_nc(lon,z,lon_left,lon_right,doPad);

    % Make lat/lon grid
    [xx,yy]=meshgrid(lon,lat);
    [x,y]=m_ll2xy(xx,yy);

    if strcmp(param.myPlot,'pcolor');
        if sum(~isnan(x(:)))>0; pcolor(x,y,z); eval(['shading ' shad ';']); end;
        if param.plotCBAR==0;
            colormap(param.myCmap); if param.doCbar; colorbar; end;
        elseif param.plotCBAR==1;
            caxis(param.myCaxis); colormap(param.myCmap); if param.doCbar; colorbar; end;
        else;
            cbar=gcmfaces_cmap_cbar(param.myCaxis,{'myCmap',param.myCmap}); delete(cbar);
        end;
        if param.do_m_coast; m_coast('patch',[1 1 1]*.7,'edgecolor','none'); end;
        eval(['m_grid(' m_grid_opt ');']);
    elseif strcmp(param.myPlot,'contour');
        if ~param.doHold;
            if param.do_m_coast; m_coast('patch',[1 1 1]*.7,'edgecolor','none'); end;
            eval(['m_grid(' m_grid_opt ');']);
        end;
        if length(fld)==2; fld{3}='k'; end;
        hold on; contour(x,y,z,fld{3:end});
    end;
elseif strcmp(param.myPlot,'plot');
    if ~param.doHold;
        if param.do_m_coast; m_coast('patch',[1 1 1]*.7,'edgecolor','none'); end;
        eval(['m_grid(' m_grid_opt ');']);
    end;
    [x,y]=m_ll2xy(fld{2},fld{3});
    hold on; plot(x,y,fld{4:end});
elseif strcmp(param.myPlot,'text');
    if ~param.doHold;
        if param.do_m_coast; m_coast('patch',[1 1 1]*.7,'edgecolor','none'); end;
        eval(['m_grid(' m_grid_opt ');']);
    end;
    [x,y]=m_ll2xy(fld{2},fld{3});
    if length(fld)>4; cc=fld{5}; else; cc='k'; end;
    hold on; hold on; text(x,y,fld{4},'Color',cc,'FontSize',16,'FontWeight','bold');
end;

set(gca,'fontsize',12);
%add tag spec. to map & proj generated with this routine
set(gca,'Tag',['gfMap' num2str(proj)]);

