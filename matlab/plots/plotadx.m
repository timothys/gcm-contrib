function [] = plotadx(runStr,resultsDir,mmapOpt,adjDump,timeDir,makeVidStills)
%% Plot adjoint sensitivities as a movie
%
%   Inputs: 
%       runStr : describes save dir e.g. dec.240mo, 12mo, 
%       resultsDir : location of results directory
%       mmapOpt : m_map_atl option (default 5, good for samoc)
%       adjDump : if created via adjDump (default 0)
%       timeDir : time direction for sensitivity plot (default -1)
%       makeVidStills : make snapshot backups for videos (default 0)
% -------------------------------------------------------------------------

%% 1. Parse through input 
if ~exist('runStr','var')
    fprintf('ERROR: need to define runStr (e.g. dec.240mo/\n');
    return
end
if ~exist('resultsDir','var') && ~ismac
    fprintf('ERROR: need to define resultsDir, i.e. location of adxx*.data ...\n');
    return
elseif ~exist('resultsDir','var') && ismac
    fprintf('Can''t find results but assuming they''re not on this machine ... \n');
end
if ~strcmp(runStr(end),'/'), runStr=[runStr '/']; end;
if ~strcmp(resultsDir(end),'/'), resultsDir=[resultsDir '/']; end;
if ~exist('mmapOpt','var'), mmapOpt=5; end;
if ~exist('adjDump','var'), adjDump=0; end;
if ~exist('timeDir','var'), timeDir=-1; end;
if ~exist('makeVidStills','var'), makeVidStills=0; end;

global mygrid;
if isempty(mygrid)
  if ismac
    grid_dir='~/work/grids/llc90/';
  else
    fprintf('Assuming this is on Ekman ...\n');
    grid_dir='/workspace/grids/llc90/';
  end
  establish_mygrid(grid_dir,5,'compact');
end
% -------------------------------------------------------------------------

%% 2. Post processing and preparing fields 
% Omitting theta and salinity right now
adjField = {'tauu','tauv','oceHeat','fwf','aqh','atemp','swdown','lwdown','precip','runoff'};
if ~isempty(strfind(runStr,'366day'))
    caxLim = [7,-2, 13, 4, 17, 18, 18, 9, 9];
elseif ~isempty(strfind(runStr,'mo')) 
  if ~isempty(strfind(runStr,'ad5day'))
	caxLim = [2.4, 2.5, 7.5, -2, 2, 6, 7, 7, -2, -2, 4, 4];
  else
	caxLim = [2, 2, 7, -2.5, 2, 6, 7, 7, -2, -2, 4, 4];
  end
elseif ~isempty(strfind(runStr,'bwk')) 
	caxLim = [2, 2, 7, -2, 2, 6, 7, 7, -2, -2, 4, 4];
elseif ~isempty(strfind(runStr,'five-day'))
    caxLim = [4,-2, 13, 13, 13, 17, 18, 18, 8, 8, 14, 14];
end

cunits = {sprintf('Sv/\n[N/m^2]'),sprintf('Sv/\n[N/m^2]'),sprintf('Sv/\n[W/m^2]'),...
          sprintf('Sv/\nm/s'),sprintf('Sv/\n[kg/kg]'),sprintf('Sv/K'),...
          sprintf('Sv/\n[W/m^2]'),sprintf('Sv/\n[W/m^2]'),...
          sprintf('Sv/\n[m/s]'),sprintf('Sv/\n[m/s]')};       

if ~isempty(strfind(runStr,'240mo'))
        tLims = {[205 241], [205 241], [1 241],  [1 241], [1 241], [1 241], [1 241], [1 241], [1 241],[1 241]};
elseif ~isempty(strfind(runStr,'480bwk'))
        tLims={[408 480], [408 480], [1 480], [1 480],  [1 480], [1 480], [1 480], [1 480], [1 480], [1 480], [1 480]};
elseif ~isempty(strfind(runStr,'ad5day')) && timeDir==1
        tLims={[32 74], [32 74], [1 74], [1 74],  [1 74], [1 74], [1 74], [1 74], [1 74], [1 74], [1 74]};
end
Nadj = length(adjField);
klev = 5; 

%% Pull data to mat files
if ~ismac
postProcess_adxx(runStr, resultsDir, adjField, mygrid, 10^-6);
end
% -------------------------------------------------------------------------

%% 3. Prepare directories
matDir = ['mat/' runStr];
figDir = ['figures/' runStr];

if ~exist(matDir,'dir') 
	fprintf('Can''t find mats, something wrong with post processing ...\n');
	return;
end
if ~exist(figDir,'dir'), mkdir(figDir); end;
% -------------------------------------------------------------------------


%% 3. Plot and save at various time steps
for i = 1:Nadj
    adjFile = sprintf('%sadj_%s.mat',matDir,adjField{i});
    if exist(adjFile,'file')
    load(adjFile);
    
    if ~adjDump && (strcmp(adjField{i},'salt') || strcmp(adjField{i},'theta'))
        %% Figure of field at klev for 3d constant fields
        figFile = sprintf('%sadj_%s',figDir,adjField{i});


        
        strs = struct(...
            'xlbl',sprintf('J() = time mean of final month AMOC\nSensitivity to %s, 50m depth',adjField{i}),...
            'clbl',cunits{i},...
            'figFile',figFile);
        opts = struct(...
            'logFld',0,...
            'caxLim',caxLim(i),...
            'saveFig',1,...
            'mmapOpt',mmapOpt,...
            'figType','wide');
        plotAdjField(adxx(:,:,klev),strs,opts,mygrid);
    else
        %% Video for time varying controls
        if ~isempty(strfind(runStr,'366day'))
            adxx = gcmfaces_interp_1d(3,[.5:365.5]/366,adxx,[.5:51.5]/52);
        end
        if ~isempty(strfind(runStr,'mo'))
		if ~isempty(strfind(runStr,'ad5day')), tt='5days';
		else, tt='months';
		end
        elseif ~isempty(strfind(runStr,'flux')), tt='months';
        elseif ~isempty(strfind(runStr,'366day')),tt='days';
        elseif ~isempty(strfind(runStr,'five-day')),tt='hrs';
        elseif ~isempty(strfind(runStr,'bwk')),tt='2 weeks';
        end

        if timeDir ==1
          vidName=sprintf('%sadj_%s_fwd',figDir,adjField{i});
        else    
         vidName=sprintf('%sadj_%s',figDir,adjField{i});
        end

        if strcmp(adjField{i},'tauu')
          xlbl='dJ/d\tau_x';
        elseif strcmp(adjField{i},'tauv')
          xlbl='dJ/d\tau_y';
        elseif strcmp(adjField{i},'hflux')
          xlbl=sprintf('dJ/dQ_{Net}, Q_{Net}>0 Cools Ocean');
        elseif strcmp(adjField{i},'oceHeat')
          xlbl=sprintf('dJ/dQ_{Net}, Q_{Net}>0 Heats Ocean');
        elseif strcmp(adjField{i},'sflux')
          xlbl=sprintf('dJ/dS');
        elseif strcmp(adjField{i},'fwf')
          xlbl=sprintf('dJ/dFWF');
        else
          xlbl=sprintf('J() = time mean of final month AMOC\nSensitivity to %s ',adjField{i});
        end

        strs = struct(...
            'xlbl',xlbl, ...
            'time',tt,...
            'clbl',cunits{i},...
            'vidName',vidName);
        opts = struct(...
            'caxLim',caxLim(i),...
            'saveVideo',1,...
            'mmapOpt',mmapOpt);
	if exist('tLims','var'), opts.tLims=deal(tLims{i}); end;
        if ismac, opts.fileFormat=deal('MPEG-4');
                  checkVid= [vidName '.mp4'];
        else,     opts.fileFormat=deal('Motion JPEG 2000');
                  checkVid= [vidName '.avi'];
        end

        if ~exist(checkVid,'file')
          plotVideo(adxx,strs,opts,mygrid,timeDir);
        end

        if makeVidStills
          if ~isempty(strfind(runStr,'ad5day'))
            ind=[73 69 64 60 50 30];
          end
          opts.spdim=deal([3 2]);
          opts.plotInd = deal(ind);
          opts.saveFig = deal(1);
          plotManyMaps(adxx,strs,opts,mygrid,timeDir);
        end

    end
    else
        fprintf('* Skipping %s file ... \n',adjField{i})
    end
end
end
