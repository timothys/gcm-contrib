function [] = xlbl_clim_half(ax)
% Creates an xlabel with labelled months on x axis
% Sets every other month Feb, Apr, ..., Dec
%
% Optional Parameters
% -------------------
%
%   ax  ::  axis object for figure
%           gca() (default)
%
% -------------------------------------------------

if nargin<1
    ax = gca;
end

xlim([1 12])
set(ax,...
    'xtick',[2:2:12],...
    'xticklabel',{'Feb','Apr','Jun','Aug','Oct','Dec'});
end
