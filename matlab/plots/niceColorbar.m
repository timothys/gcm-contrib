function [hc] = niceColorbar(hc)
% Function to manually adjust interpreter and other annoying matlab
% details that ONLY apply to the colorbar :(
%
% Optional input:
%
%       hc: colorbar handle if already generated
%
% -----------------------------------------------------------------

if ~exist('hc','var')
  hc=colorbar;
end

hc.Units='normalized';
hc.Label.Units='normalized';
hc.Label.Interpreter = 'latex';
hc.TickLabelInterpreter='latex';
hc.FontSize = 16;
hc.Label.FontSize=20;
hc.Label.Rotation=0;
hc.Label.Position = [3.33 0.533 0];

end
