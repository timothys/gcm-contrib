function [fh] = figureL()
% Makes a wide figure, returns figure handle

fh=figure; 
fh.Units='Normalized';
fh.Position=[0.0 0.0 0.5 1.0];
end
