function [fh] = figureW()
% Makes a wide figure, returns figure handle

fh=figure; 
fh.Units='Normalized';
fh.Position=[0.0 0.0 0.7 0.6];
end

