function[c_lim ] = plotVideo( fld, varargin )
% Make a video through time of mapped field
% Inputs: 
%
%       fld : 3d gcmfaces fld, 3rd dim assumed to be in time
%               OR
%               cell structure with the following fields:
%               fld{1} - the (potentially time evolving) field to be plotted        
%               fld{2} - the (for now) time constant contour to plot underneath
%               fld{3} - number of contour lines to show (default = 20)
%
%       strs: data type with fields: 
%           xlbl : x axis label
%           time : unit of time, appended onto xlbl as t-i (time) 
%                  can also be cell structure of time stamp values
%           clbl : colorbar label 
%           vidName : name of video object
%       opts: data type with fields: 
%	        tLims : limits of time vector
%           logFld : plot log of field (default 0)
%           caxLim: colorbar scale
%           saveVideo: default = 0
%           fileFormat: 'MPEG-4'= .mp4 (default) 
%                       'Motion JPEG AVI'
%	        mmapOpt : for m_map_atl arg 2
%           figType: long or wide(default) or square
%           caxLimsMax: Default=0, set to 1 to make caxis limits 
%                       +/- max(abs(fld))
%           multiPanelGlobe: top map of globe, bottom Arctic + Antarctic
%           colorbarFlag = 1 (default) - vertical to the right colorbar           
%                          0 - no colorbar
%                          2 - horizontal colorbar underneath
%           cmap: colormap in rgb matrix, default is redblue
%           fontSize: 14 (default)
%
%       vidDirection: 1 = fwd (default), -1 = backward
% -------------------------------------------------------------------------

%% Check for cell structure field, where first item is field to be plotted
%%  second item is the contour to plot underneath
%%  third item is number of contours
if iscell(fld)
    if length(fld)>2
        nContours = fld{3};
    else
        nContours = 20;
    end
    fld_contour = fld{2};
    fld = fld{1};
end

%% 0. Sanity check on the field being plotted
% After struggling with making log binned plots with correct color bars,
fprintf('Make sure colorbar spans range of max(abs( fld ))= %1.3e \n',...
        nanmax(abs(fld)));

% -------------------------------------------------------------------------
%% 1. Sift through options
for n=1:length(varargin)
    if isstruct(varargin{n})
	    if isfield(varargin{n},'XC'), mygrid=varargin{n};
	    elseif isfield(varargin{n},'vidName') | isfield(varargin{n},'xlbl'), strs=varargin{n};
	    elseif isfield(varargin{n},'saveVideo'), opts=varargin{n};
	    else fprintf('ERROR: plotVideo can''t classify input %d ...\n',n+1);
        end
    else
	    if isa(varargin{n},'double'), vidDirection=varargin{n};
	    else fprintf('ERROR: plotVideo can''t classify input %d ...\n',n+1);
        end
	end;
end


if ~exist('mygrid','var'), establish_mygrid; end;
if ~isa(fld,'gcmfaces'), convert2gcmfaces(fld); end

if ~exist('strs','var')    
    xlbl='';
    time='';
    clbl='';
    vidName = '';
else
    if isfield(strs,'xlbl'), xlbl=strs.xlbl; else xlbl=''; end;
    if isfield(strs,'clbl'), clbl=strs.clbl; else clbl='';end;
    if isfield(strs,'time'), time=strs.time; else time='';end;
    if isfield(strs,'vidName'), vidName=strs.vidName; else vidName='myVid';end;
end

if ~exist('opts','var')
    if length(size(fld.f1))==3, tLims=[1 length(fld.f1,3)];
    elseif length(size(fld.f1))==4, tLims=[1 length(fld.f1,4)]; end; 
    caxLim=0;
    logFld = 0;
    saveVideo = 0; 
    mmapOpt = 5;
    figType = 'wide';
    caxLimsMax=0;
    multiPanelGlobe=0;
    colorbarFlag=1;
    cmap=redblue;
    fontSize=14;
    if ismac 
      fileFormat='MPEG-4'; 
    else
      fileFormat='Motion JPEG AVI';
    end
else
    if isfield(opts,'tLims')
      tLims = opts.tLims; 
    else 
      if length(size(fld.f1))==3
        tLims=[1 size(fld.f1,3)];
      elseif length(size(fld.f1))==4 
        tLims=[1 size(fld.f1,4)];
      else
        tLims=[1 1];
      end 
    end
    if isfield(opts,'logFld'), logFld = opts.logFld; else logFld=0; end;
    if isfield(opts,'caxLim'), caxLim = opts.caxLim; else caxLim=0; end;
    if isfield(opts,'saveVideo'), saveVideo = opts.saveVideo; else saveVideo=0; end;
    if isfield(opts,'mmapOpt'), mmapOpt = opts.mmapOpt; else mmapOpt=5;end;
    if isfield(opts,'figType'), figType = opts.figType; else figType='wide'; end;
    if isfield(opts,'multiPanelGlobe'), multiPanelGlobe = opts.multiPanelGlobe; else multiPanelGlobe=0; end;
    if isfield(opts,'colorbarFlag'), colorbarFlag = opts.colorbarFlag; else colorbarFlag=1; end;

    if isfield(opts,'caxLimsMax'), caxLimsMax = opts.caxLimsMax; 
    else
        if caxLim ~=0 
          caxLimsMax=0; 
        end
    end
    if isfield(opts,'fileFormat'), fileFormat = opts.fileFormat; 
    else 
        if ismac 
          fileFormat='MPEG-4'; 
        else
          fileFormat='Motion JPEG AVI';
        end
    end
    if isfield(opts,'cmap'), cmap=opts.cmap; else cmap=redblue; end;
    if isfield(opts,'c_lim'), c_lim=opts.c_lim; else c_lim=[]; end;
    if isfield(opts,'fontSize'), fontSize=opts.fontSize; else fontSize=14; end;
       
end

if ~exist('vidDirection','var')
    vidDirection=1;
end

isFig=tLims(1)==tLims(2); if isFig && tLims(1)~=1, error('isFig flag and tLims conflict ...'); end;
if vidDirection==-1, tLims = [tLims(2) tLims(1)]; end;

% -------------------------------------------------------------------------
    
% -------------------------------------------------------------------------
%% 3. Prep video object
if saveVideo && ~isFig
    vidObj = VideoWriter(vidName,fileFormat); 
    set(vidObj,'FrameRate',2)
    open(vidObj);
end
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
%% 4. Open up a figure
if strcmp(figType,'wide')
    figureW;
elseif strcmp(figType,'long')
    figureL;
elseif strcmp(figType,'square')
    figureH;
else
    figure;
end
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
%% 5. Bin for nice plots
% [binFld, colbarticks, colbarlbl, Ntick, cmap] = binForPlotting(fld,caxLim,mygrid);

logFactor = 10^20;
if length(caxLim)>1 && ~logFld
  binFld=fld;
  caxMax=round(nanmax(abs(binFld)));
elseif logFld && isempty(c_lim)

    [binFld,shift_log_data] = binByLog(fld,logFactor,size(cmap,1));

    %%% caxMax is just a position on colorbar based on log10(fld)
    %%% e.g. if data span +/- 0.1, then 
    %%% (with a standard of 7 colors on the bar, 3 for + 3 for -, 1 for 0)
    %%% a value binFld(i,j)=3 => bigo( log10(fld(i,j))) ) = 0.1
    %%% so caxMax will be ~3.5 for 7 colors on the colorbar and 3 orders of 
    %%% magnitude (1 for each color)
    %%caxMax=nanmax(abs(binFld(:)));

    % Note: caxMax is always ncolors/2 
    ncolors = size(cmap,1);
    caxMax = ncolors/2;
    c_lim = 10.^(shift_log_data+round(caxMax)-1) ./ logFactor;

elseif logFld && ~isempty(c_lim)
    [binFld,shift_log_data] = binByLog(fld,logFactor,size(cmap,1));

    %caxMax_tmp=nanmax(abs(binFld(:)));
    ncolors = size(cmap,1);
    caxMax = ncolors/2;
    c_lim_tmp = 10.^(shift_log_data+round(caxMax)-1) ./ logFactor;
    %fprintf('Warning: Overriding caxMax= %1.2f with %1.2f \n',caxMax_tmp,caxMax);
    fprintf('Warning: Overriding c_lim= %1.2e with %1.2e \n',c_lim_tmp,c_lim);
    %fprintf('  note: should only do this with an odd number of colors on colorbar \n');
    %fprintf('        see note on c_lim_diff in plotVideo.m for details\n');

    % If these data have a different max than c_lim,
    % shift them
    % e.g. if data span +/- 0.01 but c_lim=0.1,
    % shift binFld by one order of magnitude (i.e. subtract by 1)
    c_lim_diff = log10(c_lim)-log10(c_lim_tmp);
    if c_lim_diff < 0
        fprintf('WARNING: saturating colorbar removes ... \n');
        numTot = mygrid.faces2gcmSize(1)*mygrid.faces2gcmSize(2);
        for tt=1:size(fld.f1,3)
            for k=1:abs(c_lim_diff)
                numClip = nansum(nansum(...
                        floor(log10(abs(convert2gcmfaces(fld(:,:,tt)))))==log10(c_lim)+k));
            end

            fprintf('    %d / %d points at time step %d \n',...
                        numClip,numTot, tt);
        end
        fprintf(' --- PROCEED AT YOUR OWN RISK ---\n');
    end

    % cbarZero is the range denoting "zeroed" values
    % when shifting, don't want to push any of these values 
    % into the next bin over
    if c_lim_diff > 0
        for k = 1:c_lim_diff
            % values between -cbarZero, +cbarZero will get moved to the
            % 0 bin, and not represented on log scale
            cbarZero = k-0.5;
            binFld(binFld>= 0.5) = binFld(binFld>= 0.5) - 1;
            binFld(binFld<=-0.5) = binFld(binFld<=-0.5) + 1;
        end
    elseif c_lim_diff < 0
        binFld(binFld>0)=binFld(binFld>0)+1;
        binFld(binFld<0)=binFld(binFld<0)-1;
    end
else
  binFld = fld*10^caxLim(1); 
  caxMax=nanmax(abs(binFld));
end
% -------------------------------------------------------------------------


% -------------------------------------------------------------------------
%% 6. Play with time to get label right
if strfind(time,'5')
  time='Days';
  tfact=5;
else
  tfact=1;
end


% -------------------------------------------------------------------------
%% 6. Do the plotting 
c=gcf();
for n=tLims(1):vidDirection:tLims(2)
        
   figure(c),
   if ~multiPanelGlobe

        % --- Hold and add contour underneath fld
        if exist('fld_contour','var')
            m_map_atl({'contour',fld_contour,nContours,'k'},mmapOpt,{'doHold',1},{'doLabl',0});
            m_map_atl(binFld(:,:,n),mmapOpt,{'doHold',1},{'myCmap',cmap},{'fontSize',fontSize});
            hold off
        else
            m_map_atl(binFld(:,:,n),mmapOpt,{'myCmap',cmap},{'fontSize',fontSize});
        end

        % --- Set up colorbar 
        hc=niceColorbar;
        hc.Label.String = clbl;
        hc.FontSize = fontSize;
        hc.Label.FontSize = fontSize;

        if colorbarFlag==2
            % Horizontal
            set(hc,'location','southoutside');
            hc.Label.Position = [0.5 -2 0];
            hc.FontSize=22;
        else
            % yaxis colorbar
            pp = [3.33 0.533 0];
            if fontSize == 22
                pp = [3.33 0.54 0];
            elseif fontSize~=14
                fprintf('no guarantee on colorbar label being in the right spot...\n');
                fprintf('Good values known for fontSize=14 or 22...\n');
            end
            set(get(hc,'label'),'rotation',0,...
                'units','normalized','position',pp);
        end

        
    % --- Multi panel globe
    else
        h1=subplot(2,2,3);
            m_map_atl(binFld(:,:,n),2);
            hc1=colorbar;
        h2=subplot(2,2,4);
            m_map_atl(binFld(:,:,n),3);
            hc2=colorbar;
        h3=subplot(2,2,1:2);
            m_map_atl(binFld(:,:,n),8);
            hc3=colorbar;
        bigh={h1, h2, h3};
        bighc={hc1,hc2,hc3};
    end

    % --- Set the xaxis
    if ~strcmp(xlbl,'')

        % --- Forward video
        if vidDirection>0 && ~isFig

            % label for each time step
            if length(strs) > 1
                if ~strcmp(time,'')
                    xlabel(sprintf('%s %s',strs(n).xlbl,strs(n).time));
                else
                    xlabel(sprintf('%s',strs(n).xlbl))
                end
            % otherwise, regular relative time label
            else
                xlabel([xlbl sprintf('\nt = %d %s',n*tfact,time)]);
            end

        % --- Adjoint video
        elseif vidDirection<0 && ~isFig
            xlabel([xlbl sprintf(', %s = %d %s','$\tau_{mem}$',tfact*(tLims(1)-n),time)]);
        % --- Figure
        elseif isFig
            xlabel(xlbl);
        end
    end

    % --- Specific colorbar options
    % --- Log bins
    if logFld

        % Center caxis ticks
        ncolors = size(cmap,1);
        caxis([-ncolors/2 ncolors/2])
        nn=floor(ncolors/2)-1;

        % Specify the ticks and their labels
        ticksVect = linspace(-caxMax, caxMax,ncolors*2+1);
        ticksVect = ticksVect(2:2:end);
        hc.Ticks=ticksVect;
        tickLblNums=[-c_lim*10.^[0:-1:-nn],0,c_lim*10.^[-nn:0]];
        tickLbls=cell(length(tickLblNums),1);
        for kk=1:length(tickLblNums)
            if tickLblNums(kk)<0
                tickLbls{kk}=sprintf('-10$^{%d}$',int64(log10(abs(tickLblNums(kk)))));
            elseif tickLblNums(kk)>0
                tickLbls{kk}=sprintf(' 10$^{%d}$',int64(log10(abs(tickLblNums(kk)))));
            else
                tickLbls{kk}=' 0';
            end
        end
        hc.TickLabels = tickLbls;

    % --- Specified colorbar axis limits for yaxis colorbar    
    elseif length(caxLim)>1 && colorbarFlag~=2

        if multiPanelGlobe
            for i=1:3
                set(bigh{i},'clim',[caxLim(1) caxLim(2)]);
                set(get(bighc{i},'label'),'string',sprintf(' %s',clbl),...
                    'rotation',0,'position',[2.3 .15 0]);
            end
        else       
            caxis([caxLim(1) caxLim(2)]);
        end

    % --- Get caxis limits from maximum
    elseif caxLimsMax==1

        caxMax=nanmax(abs(binFld(:)));
        caxis([-caxMax caxMax]);

    elseif ~logFld 
        fprintf('This is some deprecated code ...\n') 
        fprintf('  clean up the caxLim(1) stuff while you''re using this...\n'); 
        caxis([-1 1]);
        if mmapOpt==5
            hc.Label.String = sprintf(' $10^{%0.0f}$ %s',-caxLim(1),clbl);
        else 
            hc.Label.String = sprintf(' $10^{-%0.0f}$ %s',-caxLim(1),clbl);
        end
    end %end caxLim>1

    % --- Remove the colorbar
    if colorbarFlag==0
        hc.Visible = 'off';
        hc.Label.Visible = 'off';
    end

    % --- Save the file
    if saveVideo && ~isFig
        currFrame=getframe(c);
        writeVideo(vidObj,currFrame);
    elseif saveVideo && isFig
        savePlot(vidName,'png');
    elseif ~saveVideo
        pause(0.2)
        keyboard
    end
end %end time stepping for loop
        
% --- Close up the video file and frame
if saveVideo && ~isFig
    close(vidObj);
    close;
end
end

function [bin_fld,shift_log_data] = binByLog(fld,fact,ncolors)
% min_bin = smallest bin, everything smaller than 10^{min_bin} goes in last bin
% fact is just a factor to make log10(fld) positive. 
% Set data to zero that won't be represented in colorbar based on number of colors

fld = fld.*fact;
fld_pos = log10(fld.*(fld>0));
fld_pos(isinf(fld_pos))=0;
fld_pos(isnan(fld_pos))=0;

fld_neg = log10(abs(fld.*(fld<=0)));
fld_neg(isinf(fld_neg))=0;
fld_neg(isnan(fld_neg))=0;

% Shift data so only showing (e.g. for ncolors=7) 3 orders of magnitude 
max_pos = nanmax(fld_pos(:));
max_neg = nanmax(fld_neg(:));
shift_log_data = floor(max([max_pos,max_neg])) - floor(ncolors/2);

fld_pos = fld_pos - shift_log_data;
fld_neg = fld_neg - shift_log_data;

% Now shift by 1/2 to account for odd num colorbar
% e.g. ncolors = 7, largest value is 3.5, the linear distance from zero 
%      on colorbar. Anything above that would be 4 orders of magnitude
% first colorbar is first order of magnitude, spanning binFld values 0.5-1.5
% second spans 1.5-2.5, etc
% Note: Need to work this out for even number of colors
if mod(ncolors,2)~=0
    fld_pos = fld_pos - 0.5;
    fld_neg = fld_neg - 0.5;
else
    error('Need to rework log data for odd number of colors')
end

% Now set everything smaller than O(1) to zero
fld_pos(fld_pos<0)=0;
fld_neg(fld_neg<0)=0;

bin_fld = fld_pos - fld_neg;

end
