function [] = xlbl_clim(ax)
% Creates an xlabel with labelled months on x axis
%
% Optional Parameters
% -------------------
%
%   ax  ::  axis object for figure
%           gca() (default)
%
% -------------------------------------------------

if nargin<1
    ax = gca;
end

set(ax, ...
    'xlim',[0.5 12.5],...
    'xtick',[1:12],...
    'xticklabel',{'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'});
end
