function [] = saveWidePlot(savename,varargin);
% Save a figure to file
%
% Input:
%
%   savename: filename to store figure under
%
% Optional input:
%
%   fileFormat: 'png' (default), 'pdf','eps' ...
%   layout: 'landscape' (default), 'portrait'
%
% -----------------------------------------------

fileFormat='png';
layout='landscape';
for n=1:length(varargin)
    if isa(varargin{n},'char')
        if ~isempty(strfind(varargin{n},'landscape')) || ~isempty(strfind(varargin{n},'portrait'))
            layout=varargin{n};
        else
            fileFormat=varargin{n};
        end
    end
end

set(gcf,'paperorientation',layout)
set(gcf,'paperunits','normalized')
set(gcf,'paperposition',[0 0.2 1 .5])
saveas(gcf,savename,fileFormat)
fprintf('Saved plot: %s ...\n',savename);

end
