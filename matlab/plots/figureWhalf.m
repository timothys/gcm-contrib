function [fh] = figureWhalf()
% Makes a wide figure, returns figure handle

fh=figure; 
fh.Units='Normalized';
fh.Position=[0. 0.2 0.7 0.4];
end
