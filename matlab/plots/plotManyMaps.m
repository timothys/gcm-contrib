function[ ] = plotManyMaps( fld, varargin )
% Make a plot of mapped field over given 3rd dim args
% Inputs: 
%
%       fld : 3d gcmfaces fld, 3rd dim assumed to be in time
%       strs: data type with fields: 
%           xlbl : x axis label
%           time : unit of time, appended onto xlbl as t-i (time) 
%           clbl : colorbar label 
%           vidName : name of video object
%       opts: data type with fields: 
%           plotInd : indices to grab for plotting
%           spdim : subplot dimensions
%           logFld : plot log of field (default 0)
%           caxLim: colorbar scale
%           saveFig: default = 0
%	    mmapOpt : for m_map_atl arg 2
%           figType: long, wide, or huge (default)
%       vidDirection: 1 = fwd (default), -1 = backward
% -------------------------------------------------------------------------


% -------------------------------------------------------------------------
%% 1. Sift through options
for n=1:length(varargin)
	if isfield(varargin{n},'XC'), mygrid=varargin{n};
	elseif isfield(varargin{n},'xlbl'), strs=varargin{n};
	elseif isfield(varargin{n},'saveFig'), opts=varargin{n};
	elseif isa(varargin{n},'double'), vidDirection=varargin{n};
	else fprintf('ERROR: plotVideo can''t classify input %d ...\n',n);
	end;
end


if ~exist('mygrid','var'), establish_mygrid; end;
if ~isa(fld,'gcmfaces'), convert2gcmfaces(fld); end

if ~exist('strs','var')    
    xlbl='';
    time='steps';
    clbl='';
    vidName = '';
else
    if isfield(strs,'xlbl'), xlbl=strs.xlbl; else xlbl=''; end;
    if isfield(strs,'clbl'), clbl=strs.clbl; else clbl='';end;
    if isfield(strs,'time'), time=strs.time; else time='steps';end;
    if isfield(strs,'vidName'), vidName=strs.vidName; else vidName='myVid';end;
end

if ~exist('opts','var')
    if length(size(fld.f1))==3, tLims=[1 length(fld.f1,3)];
    elseif length(size(fld.f1))==4, tLims=[1 length(fld.f1,4)]; end; 
    caxLim=0;
    logFld = 0;
    saveFig = 0; 
    mmapOpt = 5;
    figType = 'huge';
    spdim=[2 2];
    plotInd=[1 2 3 4];
else
    if isfield(opts,'tLims'), tLims = opts.tLims; else 
    	if length(size(fld.f1))==3, tLims=[1 size(fld.f1,3)];
    	elseif length(size(fld.f1))==4, tLims=[1 size(fld.f1,4)]; end; 
	end
    if isfield(opts,'logFld'), logFld = opts.logFld; else logFld=0; end;
    if isfield(opts,'caxLim'), caxLim = opts.caxLim; else caxLim=0; end;
    if isfield(opts,'saveFig'), saveFig = opts.saveFig; else saveFig=0; end;
    if isfield(opts,'mmapOpt'), mmapOpt = opts.mmapOpt; else mmapOpt=5;end;
    if isfield(opts,'figType'), figType = opts.figType; else figType='huge'; end;
    if isfield(opts,'spdim'), spdim = opts.spdim; else spdim=[2 2]; end;
    if isfield(opts,'plotInd'), plotInd = opts.plotInd; else plotInd=[1 2 3 4]; end;
end

if ~exist('vidDirection','var')
    vidDirection=1;
end
if vidDirection==-1, tLims = [tLims(2) tLims(1)]; end;
% -------------------------------------------------------------------------
    

% -------------------------------------------------------------------------
%% 2. Prepare ranges for logarithmic plotting
if logFld
    [fld] = calcLogField(fld,mygrid);
end;
% -------------------------------------------------------------------------
    
% -------------------------------------------------------------------------
%% 4. Open up a figure
if strcmp(figType,'huge')
    figureH;
elseif strcmp(figType,'wide')
    figureW;
elseif strcmp(figType,'long')
    figureL;
else
    figure;
end
% -------------------------------------------------------------------------

% ---------------------
%% Make night tight subplot
[ha,pos] = tight_subplot(spdim(1),spdim(2),[.01 .1],[.06 .02],[.02 .1]);

% -------------------------------------------------------------------------
%% 5. Bin for nice plots
% [binFld, colbarticks, colbarlbl, Ntick, cmap] = binForPlotting(fld,caxLim,mygrid);


if length(caxLim)>1
  binFld=fld;
  caxMax=round(nanmax(abs(binFld)));
else
  binFld = fld*10^caxLim(1); 
  caxMax=nanmax(abs(binFld));
end
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
%% 6. Play with time to get label right
if strfind(time,'5')
  time='Days';
  tfact=5;
else
  tfact=1;
end


% -------------------------------------------------------------------------
%% 6. Do the plotting 
c=gcf();
k=1;
for n=tLims(1):vidDirection:tLims(2)
        
   if k<=length(plotInd)
   if n==plotInd(k)
   figure(c),
   %subplot(spdim(1),spdim(2),k)
   axes(ha(k));
   k=k+1;
   m_map_atl(binFld(:,:,n),mmapOpt+.1)%,{'myCaxis',myCaxis});
   hc=colorbar;
   colormap(redblue);
   
   if vidDirection>0
     xlabel([xlbl sprintf('t = %d %s',n*tfact,time)],'position',[0 2 0])
   else
     xlabel([xlbl sprintf('t = t_f-%d %s',tfact*(tLims(1)-n),time)],'position',[-0.25 -1 0])
   end
   if length(caxLim)>1
     caxis([caxLim(1) caxLim(2)])
     ylabel(sprintf(' %s',clbl),'rotation',0,'position',[1.7 0 0]);
   elseif caxLim(1)==0 && ~logFld
     caxMax=nanmax(abs(binFld(:,:,n)));
     caxis([-caxMax caxMax]);
     ylabel(sprintf(' %s',clbl),'position',[1.7 0 0],'rotation',0);
   elseif caxLim(1)>0 && ~logFld
     caxis([-1 1]);
    % cc=get(gca,'clim');
    % caxis([-max(abs(cc)) max(abs(cc))])
     if mmapOpt==5
       ylabel(sprintf(' 10^{-%0.0f} %s',caxLim(1),clbl));
     else 
       ylabel(sprintf(' 10^{-%0.0f} %s',caxLim(1),clbl));
     end
   elseif caxLim(1)<0 && ~logFld
     caxis([-1 1]);
     if mmapOpt==5
       ylabel(sprintf(' 10^{-%0.0f} %s',-caxLim(1),clbl));
     else 
       ylabel(sprintf(' 10^{-%0.0} %s',-caxLim(1),clbl));
     end
   else
     ylabel(sprintf(' %s',clbl),'position',[1.7 0 0],'rotation',0);
   end % if length(caxLim) > 1

   if spdim(1)==3 && spdim(2) == 2
     set(get(gca,'ylabel'),'position',[3.05 -.35 0],'rotation',0,'fontsize',16);
   end


   end % if n==plotInd(k)
   end % if k<=length(plotInd)
end
        
if saveFig
  savePlot(vidName);
end
end
