function [fh] = figureH()
% Makes a screen size figure, returns figure handle

fh=figure; 
fh.Units='Normalized';
fh.Position=[0 0 1 1];

end
