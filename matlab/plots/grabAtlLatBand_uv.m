function[latU,latV,latXU,latXV,latR] = grabAtlLatBand_uv(lat,fldU, fldV, varargin)
% Produces a 2d field of values along a latitude mask
%  Grabs the entire latitude band. To mask interior points, 
%  give optional mskC or mskK arguments
%
% Note: this assumes fldU/V are NOT pre rotated, that is they follow 
%       the "interesting" and "fun to use" gcmfaces convention ...
% 
% Restriction: can only work in the Atlantic 
% or can only involve gcmfaces 1 & 5
%
% Inputs: 
%
%   lat : type=double, scalar
%           latitude to grab zonal/depth profile
%
%   fldU/fldV : type=gcmfaces, 3D field in space
%            contains values desired for midsection returned field
%
% Optional Inputs
% 
%   mskW/S/k : type=gcmfaces, 2D field in space
%           Default: all ones in interior
%           Used to mask interior points
%           Note: mskW has to come first!!!
%           Created from mygrid, see ../../samoc/create_masks.m for example
%
% Outputs: 
%
%   latU/V : type=2d matrices
%            dims: [Nr, Nx] Nx: longitudinal basin width, Nr: depth
%
%   latXU : type=vector of longitudinal points where latU points live
%            dims: [1, Nx] Nx: longitudinal basin width
%
%   latXV : type=vector of longitudinal points where latV points live
%            dims: [1, Nx] Nx: longitudinal basin width
%   
%   latR : type=vector of depth points where fld points live
%            dims: [Nr, 1] Nr: Nr: depth
%
%-----------------------------------------------------------------------

% Get Atlantic and latitude masks first
mygrid = establish_mygrid('llc90');
atlBasin = v4_basin('atlExt');
iy = find(mygrid.LATS==lat);
latMskW = mygrid.LATS_MASKS(iy).mskWedge.*atlBasin;
latMskS = mygrid.LATS_MASKS(iy).mskSedge.*atlBasin;

[fldU,fldV,mskW,mskS,mskK] = processInputs(fldU,fldV,latMskW,latMskS,varargin);

% Remove all non Atlantic points, and points not on lat band
latMskW(latMskW==0)=NaN;
latIndW = find(~isnan(convert2array(latMskW)));
latMskS(latMskS==0)=NaN;
latIndS = find(~isnan(convert2array(latMskS)));

Nx = length(latIndW)+length(latIndS);
Nr = size(fldU.f1,3);
if Nr ~= length(mskK), error('length(mskK) != fldU Nr, exiting'); return; end;

% Make containers
latV = zeros(Nr,Nx);
latU = zeros(Nr,Nx);
latXU = zeros(1,Nx);
latXV = zeros(1,Nx);
NxFaceW = zeros(5,1);
NxFaceS = zeros(5,1);

for iFace = 5:-4:1
    
    tmpW = latMskW{iFace};    tmpS = latMskS{iFace};
    latIndW = find(~isnan(tmpW));  latIndS = find(~isnan(tmpS));

    NxFaceW(iFace)=length(latIndW);
    NxFaceS(iFace)=length(latIndS);
      
    %% Get velocities going northward
    if iFace == 5

        bigInd = 1;
        [li,lj] = ind2sub(size(tmpW),latIndW);

        for i = 1:length(li)
            for k=1:Nr
                % Face 5: 
                %   velocities going northward
                latV(k,bigInd) = fldU{iFace}(li(i),lj(i),k).*mskW{iFace}(li(i),lj(i))*mskK(k);
                %   velocities going eastward
                latU(k,bigInd) = fldV{iFace}(li(i),lj(i),k).*mskS{iFace}(li(i),lj(i))*mskK(k);
            end
            latXU(bigInd) = mygrid.XG{iFace}(li(i),lj(i));
            latXV(bigInd) = mygrid.XC{iFace}(li(i),lj(i));
            bigInd = bigInd + 1;
        end


    else

        bigInd = 1+NxFaceW(5);
        [li,lj] = ind2sub(size(tmpS),latIndS);

        for i = 1:length(li)
            for k=1:Nr
                % Face 1: 
                %   velocities going northward
                latV(k,bigInd) = fldV{iFace}(li(i),lj(i),k).*mskS{iFace}(li(i),lj(i))*mskK(k);
                %   velocities going eastward
                latU(k,bigInd) = fldU{iFace}(li(i),lj(i),k).*mskW{iFace}(li(i),lj(i))*mskK(k);
            end
            latXU(bigInd) = mygrid.XG{iFace}(li(i),lj(i));
            latXV(bigInd) = mygrid.XC{iFace}(li(i),lj(i));
            bigInd = bigInd + 1;
        end
    end


end %end iFace=5->1

% Fill latR 
if Nr == 50
    latR = mygrid.RC;
elseif Nr == 51
    latR = mygrid.RF;
elseif Nr == 1
    latR = []; % latR does not apply to surface field
else
    fprintf(' Not sure how to fill latR ... \n');
    latR = [];
end
end

%-----------------------------------------------------------------------

function [fldU,fldV,mskW,mskS,mskK] = processInputs(fldU,fldV,latMskW,latMskS,varg)
% Deal with optional inputs 

global mygrid;
if isa(fldU,'gcmfaces')
    if length(size(fldU.f1))>2
        Nr = size(fldU.f1,3);
    else
        Nr = 1;
    end
else
    if length(size(fldU))>2
        Nr = size(fldU,3);
    else
        Nr = 1;
    end
end
mskW = [];
mskS = [];
mskK = ones(Nr,1);

if length(varg)<2
    mskW = latMskW;
    mskS = latMskS;
else
    fprintf(' *** WARNING: if providing mskW and mskS, mskW must come first, mskS right after it! *** \n ');
end

for n = 1:length(varg)
    if isa(varg{n},'gcmfaces') || size(varg{n},1)==360 || size(varg{n},1)==90 
        if isempty(mskW)
            tmpW = varg{n};
            tmpS = varg{n+1};

            if size(varg{n},1)==360
                tmpW = convert2array(tmpW);
                tmpS = convert2array(tmpS);
            elseif size(varg{n},1)==90
                tmpW = convert2gcmfaces(tmpW);
                tmpS = convert2gcmfaces(tmpS);
            end
            mskW = tmpW; 
            mskS = tmpS; 
        end
    else
        mskK = varg{n};
    end
end

% Deal with types
if ~isa(fldU,'gcmfaces')
    if size(fldU,1)~=360
        % Not an array
        fldU = convert2gcmfaces(fldU);
    else
        fldU = convert2array(fldU);
    end
end
if ~isa(fldV,'gcmfaces')
    if size(fldV,1)~=360
        % Not an array
        fldV = convert2gcmfaces(fldV);
    else
        fldV = convert2array(fldV);
    end
end
end
    
