!printenv HOSTNAME > hostname.txt
hh=textread('hostname.txt','%s');
!echo ~ > home.txt
ho=textread('home.txt','%s');
delete('hostname.txt');
delete('home.txt');

hh=char(hh);
ho=char(ho);

if ismac
  addpath(genpath('~/work/gcmfaces'));
  addpath(genpath('~/work/MITprof'));
  addpath(genpath('~/work/MITgcm_c66i+/utils/matlab/'))
  addpath(genpath('~/work/MITgcm_c66i+/utils/mexcdf/'))
  addpath(genpath('~/work/gcm-contrib/matlab'));
elseif ~isempty(strfind(hh,'ekman'))
  addpath(genpath('/workspace/gcmfaces'));
  addpath(genpath('/workspace/MITprof'));
  addpath(genpath('/workspace/MITgcm_c65x+/utils/matlab/'))
  addpath(genpath('/workspace/gcm-contrib/matlab/'))
elseif ~isempty(strfind(hh,'sverdrup')) || ~isempty(strfind(hh,'cluster'))
  addpath(genpath('~/gcmfaces'));
  addpath(genpath('~/MITprof'));
  addpath(genpath('~/MITgcm_c65x+/utils/matlab/'))
  addpath(genpath('~/gcm-contrib/matlab/'))
elseif ~isempty(strfind(ho,'03754'))
  fprintf('hello from TACC ... \n')
  addpath(genpath('/work/03754/tsmith/gcmfaces'));
  addpath(genpath('/work/03754/tsmith/MITgcm/utils/matlab/'))
  addpath(genpath('/work/03754/tsmith/gcm-contrib/matlab'))

elseif ~isempty(strfind(hh,'pfe'))
  addpath(genpath('~/gcmfaces'));
  addpath(genpath('~/MITprof'));
  addpath(genpath('~/MITgcm/utils/matlab/'))
  addpath(genpath('/nobackupp2/tasmit12/gcm-contrib/matlab'))

elseif ~isempty(strfind(hh,'eofe'))
  addpath(genpath('~/gcmfaces'));
  addpath(genpath('~/MITprof'));
  addpath(genpath('~/MITgcm/utils/matlab/'))
  addpath(genpath('~/gcm-contrib/matlab'))

elseif ~isempty(strfind(hh,'glacier'))

else
  fprintf('Not sure which machine you''re using, nothing added to path :( \n')
end
set(0,'DefaultFigurePosition', [25 550 750 600]);
set(0, 'DefaultAxesFontSize', 16);
%set(0, 'DefaultAxesFontName', 'Helvetica');
set(0, 'DefaultLineLineWidth', 2);
set(0, 'DefaultAxesLineWidth', 2);
set(0, 'DefaultPatchLineWidth', 2);
% set(0, 'DefaultAxesFontWeight','bold');
set(0, 'DefaultFigureColor',[1 1 1]);
set(groot,'DefaultTextInterpreter', 'latex')
set(groot,'DefaultLegendInterpreter', 'latex')
set(groot,'DefaultAxesTickLabelInterpreter', 'latex')

