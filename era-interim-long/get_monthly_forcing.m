clear all;
dirRoot='/nobackupp2/tasmit12/';
%dirRun=[dirRoot 'global_oce_llc90/run_BE2_debugxx_L07str_it0059_pk0000000001_wspee/diags/'];
dirRun=[dirRoot 'era-interim-long/run'];
dirOut=[dirRoot 'era-interim-long/monthly-forcing-1979-to-2011'];if(exist(dirOut)==0);mkdir(dirOut);end;

nx=90;ny=nx*13;
yrStart=1995;moStart=1;daStart=1;deltaT=3600;

dirlist=dir([dirRun 'diags*']);

%%loop through first to get total
for idir=1:length(dirlist);
  flist=dir([dirRun dirlist(idir).name '/exf*.data']);
  L(idir)=length(flist);
  fprintf('%s ',dirlist(idir).name);fprintf('%i ',L(idir));
end;
fprintf('\n');

fsave=[dirRun 'timestep.mat'];

%if(exist(fsave)==0);
  %Ltot=35060;
  Ltot=sum(L);
  cc=0;
  tt=zeros(Ltot,8);
  dirstr=char(Ltot,5);

  for idir=1:length(dirlist);
  
    flist=dir([dirRun dirlist(idir).name '/exf*.data']);
    if(length(flist)>0);
      fprintf('%s ',dirlist(idir).name);fprintf('%i ',cc);
      idot=find(flist(1).name=='.');idot=idot(1)+1:idot(end)-1;
      for k=1:length(flist);
        cc=cc+1;
        if(k==1 & idir==1);
            t0=datenum(yrStart,moStart,daStart);
        else;
          t0=t1;
        end;
        ts=str2num(flist(k).name(idot));
        tmp=datevec(ts2dte(ts,deltaT,yrStart,moStart,daStart));
        t1=datenum(tmp);
    
        t01=(t1+t0)./2;
  
        tmp=datevec(t01);
        tsp=dte2ts(tmp,deltaT,yrStart,moStart,daStart);
  
        tt(cc,1:6)=tmp(1:6); tt(cc,7)=tsp;tt(cc,8)=ts;
        dirstr(cc,1:5)=dirlist(idir).name;
        if(cc>1);if(tsp-tt(cc-1,7)~=6);error('missing file\n');end;end;
      end;
      fprintf('%i\n',cc);
    end;
  
  end;

  save(fsave,'tt','dirstr');
%else;
%  load(fsave,'tt','dirstr');
%end;

%yr=1992:1998;
yr=1995:2027;
%now create forcing files:
%fields(1:9,20) ='EXFpreci','EXFswdn ','EXFlwdn ',
%                'EXFatemp','EXFaqh  ','EXFtaux ','EXFtauy ',
%                'EXFtauE ','EXFtauN ','EXFwspee','EXFroff ',
varstr={'preci','swdn','lwdn','atemp','aqh','taux','tauy','tauE','tauN','wspee','roff'};

for iyr=1:length(yr);
  ii=find(tt(:,1)==yr(iyr));

%initialize
  if(length(ii)>0);
    clear FF
    for i=1:size(varstr,2);
      eval(['FF.' varstr{i} '=zeros(nx,ny,length(ii));']);
    end;

    for irec=1:length(ii);

      fIn=[dirRun dirstr(ii(irec),:) '/' 'exf_zflux_set2.' sprintf('%10.10i',tt(ii(irec),8))];
      a=rdmds(fIn);
      %a=readbin(fIn,[nx ny size(varstr,2)]);
      for i=1:size(varstr,2);
        eval(['FF.' varstr{i} '(:,:,irec)=a(:,:,i);']);
      end;
      if(mod(irec,100)==0);fprintf('%i ',irec);end;
    end
    fprintf('\n');

    for i=1:size(varstr,2);
      fOut=[dirOut 'it59_' varstr{i} '_' sprintf('%4.4i',yr(iyr))];
      eval(['tmp=FF.' varstr{i} ';']);
      writebin(fOut,tmp);
      fprintf('%s\n',fOut);
    end;

  end;

end;
