function [] = diag2ctrl()
%
% On Lonestar, want to take diagnostic output of EXF fields and 
% convert to control vector format
%
% -------------------------------------------------------------

runDir = '/scratch/tsmith/ext-ctrls-v4r2/run.raw2diags.92to11/';
runInitialDir='/scratch/tsmith/ext-ctrls-v4r2/run.raw2diags.initial/';

%% Grab iterations in results dir
resultsDir = [runDir 'diags/'];
resultsInitialDir = [runInitialDir 'diags/'];
exfName = 'exf';

% 'EXFtaux ' 'EXFtauy ' 'EXFatemp' 'EXFaqh  ' 
% 'EXFlwdn ' 'EXFswdn ' 'EXFpreci'
% --- 

%% Loop through diagnostic variables, then grab and save as control vectors
saveDir = '/scratch/tsmith/ext-ctrls-v4r2/era-interim-raw-1992-to-2011/';
if ~exist(saveDir), mkdir(saveDir); end

prefix = [saveDir 'xx_'];
suffix = '.0000000012.data';
fldNames = {'tau','atemp','aqh','lwdown','swdown','precip'};
recNum = [1 3:7];



for i = 1:length(fldNames)

  %% Special case for wind stress, need to flip UX,VY
  if ~isempty(strfind(fldNames{i},'tau'))

    %% for chunking out to reduce memory footprint
    %iters=grabAllIters(resultsDir,exfName);
    iters=3:6:175299;
    nIters=length(iters);
    chunkSize=1000;
    nChunk = ceil(nIters/chunkSize);

    tmpuSaveFile=sprintf('%stmpu_%d.mat',saveDir,0);
    tmpvSaveFile=sprintf('%stmpv_%d.mat',saveDir,0);
 
    if ~exist(tmpuSaveFile,'file')
      %% first do initial time slot
      tmpu0=rdmds2gcmfaces([resultsInitialDir exfName],3,'rec',recNum(i));
      tmpv0=rdmds2gcmfaces([resultsInitialDir exfName],3,'rec',recNum(i)+1);
      [tmpu0,tmpv0] = calc_UEVNfromUXVY(tmpu0,tmpv0);

      %% Convert to array
      tmpu0=convert2gcmfaces(tmpu0); tmpu0(isnan(tmpu0))=0;
      tmpv0=convert2gcmfaces(tmpv0); tmpv0(isnan(tmpv0))=0;

      save(tmpuSaveFile,'tmpu0'); 
      save(tmpvSaveFile,'tmpv0');    

      clear tmpu0 tmpv0;
    end

    %% Now do rest of the files
    %% Do swap UEVN ... 
    for j=1:nChunk

      tmpuSaveFile=sprintf('%stmpu_%d.mat',saveDir,j);
      tmpvSaveFile=sprintf('%stmpv_%d.mat',saveDir,j);

      if ~exist(tmpuSaveFile,'file')

        beg = (j-1)*chunkSize+1;
        ed = min(j*chunkSize,nIters);
        
        tmpu = rdmds2gcmfaces([resultsDir exfName],iters(beg:ed),'rec',recNum(i));
        tmpv = rdmds2gcmfaces([resultsDir exfName],iters(beg:ed),'rec',recNum(i)+1);
        
        Nt = size(tmpu.f1,3);
        for n=1:Nt
        	[tmpu(:,:,n),tmpv(:,:,n)] = calc_UEVNfromUXVY(tmpu(:,:,n),tmpv(:,:,n));
        end

        %% Convert to array
        tmpu=convert2gcmfaces(tmpu); tmpu(isnan(tmpu))=0;
        tmpv=convert2gcmfaces(tmpv); tmpv(isnan(tmpv))=0;
        
        save(tmpuSaveFile,'tmpu');
        save(tmpvSaveFile,'tmpv');

        clear tmpu tmpv;
        fprintf('Done with wind stress swapping %d/%d\n',j,nChunk);
      end
    end


    saveFile=[prefix 'tauu' suffix];
    if ~exist(saveFile,'file');

      saveu=[];

      %% Now load and concatenate to one big file
      %% Tauu 
      for j=0:nChunk
        
        tmpuSaveFile=sprintf('%stmpu_%d.mat',saveDir,j);
        load(tmpuSaveFile)

        if j==0
          saveu=tmpu0;
          clear tmpu0;
        else
          saveu=cat(3,saveu,tmpu);
        end
      end

      clear tmpu;

      %% Switch sign 
      saveu=-saveu;
        
      %% Save it all
      write2file(saveFile,saveu);
      write2meta(saveFile,size(saveu));
      fprintf('File %s written ... \n',[prefix 'tauu' suffix]);
      clear saveu;
    end


    saveFile=[prefix 'tauv' suffix];

    if ~exist(saveFile,'file')

      savev=[];

      %% Tauv 
      for j=0:nChunk
        
        tmpvSaveFile=sprintf('%stmpv_%d.mat',saveDir,j);
        load(tmpvSaveFile)

        if j==0
          savev=tmpv0;
          clear tmpv0;
        else
          savev=cat(3,savev,tmpv);
        end
      end

      clear tmpv;

      %% Switch sign 
      savev=-savev;
        
      %% Save it all
      write2file(saveFile,savev);
      write2meta(saveFile,size(savev));
      fprintf('File %s written ... \n',[prefix 'tauv' suffix]);
      clear savev;
    end

  %% Otherwise super straight forward
  else
  
    saveFile = [prefix fldNames{i} suffix];
    
    if ~exist(saveFile,'file')
      xx_tmp = rdmds2gcmfaces([resultsDir exfName],NaN,'rec',recNum(i));	
      xx_tmp0 = rdmds2gcmfaces([resultsInitialDir exfName],3,'rec',recNum(i));	
      
      %% Convert to array
      xx_tmp=convert2gcmfaces(xx_tmp);
      xx_tmp(isnan(xx_tmp))=0;

      xx_tmp0=convert2gcmfaces(xx_tmp0);
      xx_tmp0(isnan(xx_tmp0))=0;

      %% Add extra spot for time 0 
      xx_tmp = cat(3,xx_tmp0,xx_tmp);
      
      %% Switch sign 
      xx_tmp=-xx_tmp;
      
      write2file(saveFile,xx_tmp);
      write2meta(saveFile,size(xx_tmp));
      fprintf('File %s written ... \n',[prefix fldNames{i} suffix]);
      clear xx_tmp;
    end
  end
end
end
