function [ date ] = rec2date(n)
% Given a record number (of biweekly controls ...) compute "date" being 1->366
% Treat leap as: March 1 = 61 no matter what

mo = str2num(ts2dte(n,1209600,1979,1,3,6,5));
day= str2num(ts2dte(n,1209600,1979,1,3,6,7));

dayPerMonth = [0 31 29 31 30 31 30 31 31 30 31 30 31];
dayPerMonth = cumsum(dayPerMonth');

date = dayPerMonth(mo) + day;
end
