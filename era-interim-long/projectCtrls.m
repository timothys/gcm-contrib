function [ ] = projectCtrls(mygrid) 
% Goal is to compute ctrls for 1979-1991 based on an average of 1992-2011 values.
% 1. Grid/dirs etc. 
% 2. Set record lengths 
% ------------------------------------------------------------------

%% 1. Establish grid and directories
if ~exist('mygrid','var'), establish_mygrid; end;

ctrlDir = '/workspace/ctrls-r2/';
mainSaveDir = 'projected-ctrls/';

ctrlSaveDir = ['ctrls/'];
varSaveDir = ['var/'];

if ~exist(ctrlDir,'dir'), fprintf('Error: Can''t find controls ... exiting ...\n'); return; end;
if ~exist(mainSaveDir,'dir'), mkdir(mainSaveDir); end;
if ~exist(subDir,'dir'), mkdir(subDir); end;
if ~exist(ctrlSaveDir,'dir'), mkdir(ctrlSaveDir); end;
if ~exist(varSaveDir,'dir'), mkdir(varSaveDir); end;
% ------------------------------------------------------------------

%% 2. Set variables 
% Note: need to start ctrls at Jan3,1979 6am so that recSt correspnds to
%       Jan1,1992 @6am

nRecs_v4r2=523;         %ts2dte(522,1209600,1992,1,1,6,0)->Jan4,2012

% recSt & recEnd  give start and end of v4r2 controls
recSt = 340;            %ts2dte(339,1209600,1979,1,3,6,0)->Jan1,1992@6am
recEnd=recSt+nRecs_v4r2-1;
fldNames = {'tauu','tauv','atemp','lwdown','swdown','aqh','precip'};

Nproj = recSt-1;
xx_proj = repmat(0.0*mygrid.mskC(:,:,1),[1 1 Nproj]);
var_proj= xx_proj;
% ------------------------------------------------------------------

%% 3. Loop thru the v4r2 ctrls 2 at a time 
%     for each projected record that fits between these two, interpolate for avg 
for i=1:length(fldNames)

  xx_tmp = rdmds2gcmfaces([ctrlDir 'xx_' fldNames{i}],12);
  Nrec = size(xx_tmp.f1,3);
  recip = 1/24;
  projInd = zeros(1,Nrec);
  
  saveStr = [ctrlSaveDir 'xx_' fldNames{i} '.0000000012.data'];
  if ~exist(saveStr,'file')
    % First compute the mean
    for j=1:nRecs_v4r2
    
      % Compute interpolated record and find index for xx_proj
      [ xx, k ] = interpRecs( xx_tmp(:,:,j), xx_tmp(:,:,j+1), j, Nproj, 13);
      
      % Accumulate to kth record for the mean
      xx_proj(:,:,k) = xx_proj(:,:,k) + xx*recip;
    end
    
    % Now save xx_proj appended to control vector
    xx_tmp = cat(3, xx_proj,xx_tmp(:,:,1:Nrec-1));
    xx_tmp=convert2gcmfaces(xx_tmp); xx_tmp(isnan(xx_tmp))=0;
    write2file(saveStr,xx_tmp);
    write2meta(saveStr,size(xx_tmp));
    xx_tmp = convert2gcmfaces(xx_tmp);
    fprintf('Written climatology for %s ...\n',fldNames{i});

  else
    if ~exist([varSaveDir fldNames{i} '.data'])
      xx_tmp=rdmds2gcmfaces([ctrlSaveDir 'xx_' fldNames{i}],12);
      xx_proj=xx_tmp(:,:,1:Nproj);
      fprintf('Loaded climatology for %s ...\n',fldNames{i});
    end
  end

saveStr = [varSaveDir fldNames{i} '.data'];
if ~exist(saveStr,'file')
  % Now compute the variance
  for j=recSt:recEnd
  
  	% Compute interpolated record and find index for variance
  	[ xx, k ] = interpRecs( xx_tmp(:,:,j), xx_tmp(:,:,j+1), j);
  	var_proj(:,:,k) = var_proj(:,:,k) + ((xx-xx_proj(:,:,k)).^2)*recip;
  end
  
  % And save variance
  var_proj = convert2gcmfaces(var_proj); var_proj(isnan(var_proj))=0;
  write2file(saveStr,var_proj);
  write2meta(saveStr,size(var_proj));
  var_proj = convert2gcmfaces(var_proj);
  fprintf('Written variance for %s ...\n',fldNames{i});
end

end %for i=1:fldNames
end
% ------------------------------------------------------------------

function [ ctrls, projInd ] = interpRecs(xx0, xx1, j, Nproj, numYrs) 
% Interpolate between records xx0 & xx1 where xx0 sits at record j

% Records for projecting 
%Nproj = 652-627+1; 
projDate = rec2date([1:Nproj]);

% Dates for interpolation 
date0 = rec2date(j);
date1 = rec2date(j+1); 

foundRefDate=0;
k=0;

% Want to fill all projection years associated with these two ctrls
ctrls=convert2gcmfaces(0*xx0(:,:,1));
ctrls=convert2gcmfaces(repmat(ctrls,[1 1 numYrs]));
projInd=zeros(numYrs,:);

for n=1:numYrs

  % Find the reference date (i.e. the date associated with a particular 
  % element of xx_proj) that sits between records j & j+1
  while ~foundRefDate && k<Nproj
    k=k+1;
    lb = projDate(k)-14; 
    if lb <= 0 && date0>351, lb = 366+lb; end;
    
    ub = projDate(k)+14;
    if ub >= 366 && date1<15, ub = ub-366; end;
  
    foundRefDate = lb<=date0 && ub>=date1;
  end
  
  if k==Nproj && ~foundRefDate
  	fprintf('ERROR: Couldn''t find a bound between records j=%d & j=%d\n',j,j+1);
  	keyboard
  end
  
  if date0>351 && projDate(k)<15
    date0=date0-366;
  end 
  
  % Linearly interpolate between the two records 
  % to get controls at the particular day of xx_proj(k)
  alpha = (projDate(k) - date0)/14;
  xx(:,:,n) = (1-alpha)*xx0 + alpha*xx1;
  projInd(n) = k;

  % Once found, loop thru k a bit faster
  k=k+20;
end
end
