
-------------------------------------------------------------------------------------------------------------
ECCO Version 4: Third Release       [ECCO v4-r3] [ftp://ecco.jpl.nasa.gov/Version4/Release3/]
-------------------------------------------------------------------------------------------------------------

- This directory contains binary files needed to initialize MITgcm to run 'ECCO version 4, release 3':
  README                               This file
  *data*                               namelists

- Notes:
  These files are directly readable by the MITgcm when using the LLC90 setup to 
   re-run the ECCO v4 ocean state estimate (see eccov4.pdf link provided below).

- References:
  Forget, G., J.-M. Campin, P. Heimbach, C. N. Hill, R. M. Ponte, and C. Wunsch, 2015: 
   ECCO version 4: an integrated framework for non-linear inverse modeling and global ocean 
   state estimation. Geoscientific Model Development, 8, 3071-3104, doi:10.5194/gmd-8-3071-2015
  Forget, G., J.-M. Campin, P. Heimbach, C. N. Hill, R. M. Ponte, and C. Wunsch, 2016:
   ECCO version 4: Second Release, http://hdl.handle.net/1721.1/102062
 
- Software:
  The ECCO v4-r3 files were produced using the ’checkpoint65u’ versions of the general circulation 
   model (MITgcm and ECCO v4 settings) and Matlab analysis toolboxes (gcmfaces and MITprof).
  These software versions are available at http://mitgcm.org/download/other_checkpoints/
   and http://mit.ecco-group.org/opendap/ecco_for_las/version_4/checkpoints/contents.html

- Contact Us:
  ecco-support@mit.edu (please subscribe via http://mailman.mit.edu/mailman/listinfo/ecco-support)

-----------------------------
README file revision history:
-----------------------------

- README file creation                             [Ou Wang] [2017/06/07]

