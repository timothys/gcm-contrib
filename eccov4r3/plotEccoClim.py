# Script to plot a climatological month from ECCOv4r3
# specifically cell centered, scalar data from 
#   ftp://ecco.jpl.nasa.gov/Version4/Release3/nctiles_climatology
#
# Trying out the new ecco v4 python library ... 
#
#
import numpy as np
import xarray as xr
import ecco_v4_py as ecco
import matplotlib.pylab as pyl
import matplotlib.pyplot as plt
# Colorbrewer colors are native in matplotlib ...
#from palettable.colorbrewer.diverging import BrBG_9

grid_dir = '../../grids/llc90_nctiles/'
clim_dir = '../../release3_climatology/nctiles_climatology/THETA/'

var = 'GRID'
var_type = 'grid'
grid = ecco.load_all_tiles_from_netcdf(grid_dir,var,var_type)

var = 'THETA'
var_type = 'c' # as in, at cell centers
theta_clim = ecco.load_all_tiles_from_netcdf(clim_dir,var,var_type)

# Compute the anomaly
theta_clim_anom = theta_clim - theta_clim.mean('time')

# Merge the two together ...
v4 = xr.merge([theta_clim_anom,grid])
print(v4)

# Now make a plot ...
# First grab a nice colormap ...
pyl.figure(figsize=(12,6),dpi=90)
plt.set_cmap('BrBG')

theta_plot = v4.THETA.sel(time=2,k=1)
cmax = np.max(np.abs(theta_plot))

ecco.plot_tiles_proj(v4.XC,v4.YC,
                     theta_plot,
                     user_lon_0 = 115,
                     plot_type = 'pcolor',
                     show_colorbar=True,
                     cmap='BrBG',
                     cmin=-cmax,cmax=cmax,
                     projection_type = 'robin')
plt.show()

# This is not working in python scripts
# but "works" in jupyter notebook ...
# however still has incorrectly rotated faces :( 
### Now use plot_tiles
##pyl.figure(figsize=(12,6),dpi=90)
##
##theta_plot = v4.THETA.sel(time=1,k=1)
##cmax = np.max(np.abs(theta_plot))
##
### new optional argument:
### layout  -  either 'llc' or 'latlon'  default is 'llc
##ecco.plot_tiles(theta_plot, cbar=True, 
##                layout='latlon', tile_labels=False) 
