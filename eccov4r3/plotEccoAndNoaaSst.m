function [] = plotEccoAndNoaaSst()
% Plot ECCOv4r3 and NOAA SST fields to give a visual
% for why we do this whole data constraining thing
% 
%
% How about the Dec. 1997 anomaly, a recognizable SST anomaly for monster Nino
% --------------------------------------------------

mygrid = establish_mygrid('llc90');
ecco_dir = '../../ecco/release3/nctiles_monthly/THETA/';
obs_dir = '../../data/noaa_sst/'; % Contains SSTs
noaa_file = 'sst.mnmean.nc';
obs_ind92 = 122; % Grab monthly index corresponding to Jan. 1992

fig_dir = 'figures/'; 
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;

% --- load the stuff
[ecco_sst] = loadEccoSst(ecco_dir,mygrid);
[noaa_sst,lat,lon] = loadNoaaSst([obs_dir noaa_file],obs_ind92);
[ecco_anom] = calcAnomaly(ecco_sst);
[noaa_anom] = calcAnomaly(noaa_sst);
[ecco_clim] = calcClimatology(ecco_sst,size(ecco_sst.f1,3)/12);
[noaa_clim] = calcClimatology(noaa_sst,size(noaa_sst,3)/12);

% need to add back time mean to seasonal cycle
ecco_mean = convert2gcmfaces(nanmean(ecco_sst,3));
ecco_clim = ecco_clim + convert2gcmfaces(repmat(ecco_mean,[1 1 12]));
noaa_mean = nanmean(noaa_sst,3);
noaa_clim = noaa_clim + repmat(noaa_mean,[1 1 12]);

% --- Plot stuff
%plotAnomaly(ecco_anom,noaa_anom,lat,lon,fig_dir);
%plotClim(ecco_clim,noaa_clim,lat,lon,fig_dir);
plotMonth(ecco_clim,noaa_clim,lat,lon,fig_dir);

keyboard

end

% --------------------------------------------------

function [] = plotAnomaly(ecco_anom,noaa_anom,lat,lon,fig_dir)

% Dec. 1997
plot_ind = 6*12;
ecco_plot = ecco_anom(:,:,plot_ind);
noaa_plot = noaa_anom(:,:,plot_ind);
cmap = flipdim(cbrewer2('BrBG',11),1);
cmax = nanmax([nanmax(ecco_plot(:)), nanmax(noaa_plot(:))]);
cmin = -cmax;
cmax = 5.5; % after trial and error, this looks nice
cbar_ticks = -5:5;
fontSize = 22;

% --- Plot ECCO SST
figureW;
m_map_atl(ecco_plot,8,{'myCmap',cmap},{'fontSize',fontSize});
    hc=niceColorbar;
    hc.Label.String='$^\circ$C';
    %hc.Ticks = cbar_ticks;
    caxis([cmin cmax]);
    xlabel(sprintf('ECCOv4r3 SST, Dec. 1997'));
    set(gca,'FontSize',fontSize);

    savePlot([fig_dir 'ecco_sstAnom_dec97']);

% --- Plot NOAA SST
figureW;
m_map_nc(lat,lon,noaa_plot,8,{'myCmap',cmap},{'fontSize',fontSize});
    hc=niceColorbar;
    hc.Label.String='$^\circ$C';
    %hc.Ticks = cbar_ticks;
    caxis([cmin cmax]);
    xlabel(sprintf('NOAA OIv2 %s SST, Dec. 1997','$1^\circ\times1^\circ$'));
    set(gca,'FontSize',fontSize);

    savePlot([fig_dir 'noaaOIv2_sstAnom_dec97']);

end

% --------------------------------------------------

function [] = plotClim(ecco_clim,noaa_clim,lat,lon,fig_dir)

% Sept. Avg
plot_ind = 9;
ecco_plot = ecco_clim(:,:,plot_ind);
noaa_plot = noaa_clim(:,:,plot_ind);
cmap = flipdim(cbrewer2('YlGnBu',8),1);
%cmax = nanmax([nanmax(ecco_plot(:)), nanmax(noaa_plot(:))]);
%cmin = nanmin([nanmin(ecco_plot(:)), nanmin(noaa_plot(:))]);
cmin = -5; % after trial and error, this looks nice
cmax = 11;
cbar_ticks = cmin+1:2:cmax-1;
fontSize = 22;

% --- Plot ECCO SST
figureW;
m_map_atl(ecco_plot,8,{'myCmap',cmap},{'fontSize',fontSize});
    hc=niceColorbar;
    hc.Label.String='$^\circ$C';
    hc.Ticks = cbar_ticks;
    caxis([cmin cmax]);
    xlabel(sprintf('ECCOv4r3 SST, Sept. Avg'));
    set(gca,'FontSize',fontSize);

    savePlot([fig_dir 'ecco_sstClim_sept']);

% --- Plot NOAA SST
figureW;
m_map_nc(lat,lon,noaa_plot,8,{'myCmap',cmap},{'fontSize',fontSize});
    hc=niceColorbar;
    hc.Label.String='$^\circ$C';
    hc.Ticks = cbar_ticks;
    caxis([cmin cmax]);
    xlabel(sprintf('NOAA OIv2 SST, Sept. Avg'));
    set(gca,'FontSize',fontSize);

    savePlot([fig_dir 'noaaOIv2_sstClim_sept']);

    keyboard

end

% --------------------------------------------------

function [] = plotMonth(ecco_clim,noaa_clim,lat,lon,fig_dir)

% Sept. Avg
plot_ind = 9;
ecco_plot = ecco_clim(:,:,plot_ind);
noaa_plot = noaa_clim(:,:,plot_ind);
cmap = flipdim(cbrewer2('YlGnBu',8),1);
%cmax = nanmax([nanmax(ecco_plot(:)), nanmax(noaa_plot(:))]);
%cmin = nanmin([nanmin(ecco_plot(:)), nanmin(noaa_plot(:))]);
cmin = -2.5; % after trial and error, this looks nice
cmax = 37.5;
cbar_ticks = cmin+2.5:5:cmax-2.5;
fontSize = 22;

% --- Plot ECCO SST
figureW;
m_map_atl(ecco_plot,8,{'myCmap',cmap},{'fontSize',fontSize});
    hc=niceColorbar;
    hc.Label.String='$^\circ$C';
    hc.Ticks = cbar_ticks;
    caxis([cmin cmax]);
    xlabel(sprintf('ECCOv4r3 SST, Sept. Avg'));
    set(gca,'FontSize',fontSize);

    savePlot([fig_dir 'ecco_sstClim_sept']);

% --- Plot NOAA SST
figureW;
m_map_nc(lat,lon,noaa_plot,8,{'myCmap',cmap},{'fontSize',fontSize});
    hc=niceColorbar;
    hc.Label.String='$^\circ$C';
    hc.Ticks = cbar_ticks;
    caxis([cmin cmax]);
    xlabel(sprintf('NOAA OIv2 SST, Sept. Avg'));
    set(gca,'FontSize',fontSize);

    savePlot([fig_dir 'noaaOIv2_sstClim_sept']);

    keyboard

end

% --------------------------------------------------

function [sst] = loadEccoSst(read_dir,mygrid)

mat_dir = 'mat/'; if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_dir = [mat_dir 'state/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_file = [mat_dir 'sst_monthly.mat'];

if exist( mat_file , 'file' )
    load(mat_file,'sst');
else

    % Num. months for ECCOv4r3
    Nt = 288;

    % Make a container big enough for those records
    sst = 0*convert2gcmfaces(mygrid.XC);
    sst = convert2gcmfaces( repmat( sst, [1 1 Nt] ) );

    for n=1:Nt
        sst(:,:,n) = read_nctiles([read_dir 'THETA'],'THETA',n,1);
    end

    save(mat_file,'sst');

end
fprintf(' --- Loaded ECCOv4r3 SSTs ---\n');
end

% --------------------------------------------------

function [sst,lat,lon] = loadNoaaSst(read_file,ind92)

Nt = 288;

mat_dir = 'mat/'; if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_dir = [mat_dir 'noaa/']; if ~exist(mat_dir,'dir'), mkdir(mat_dir); end;
mat_file = [mat_dir 'sst_monthly.mat'];

if exist(mat_file,'file')
    load(mat_file,'lat','lon','sst')
else
    sst = ncread(read_file,'sst');
    sst = sst(:,:,ind92:ind92+Nt-1);
    lat = ncread(read_file,'lat');
    lon = ncread(read_file,'lon');

    save(mat_file,'lat','lon','sst')
end
fprintf(' --- Loaded NOAA OIv2 SSTs ---\n');

end

% --------------------------------------------------

function [sst_anom] = calcAnomaly(sst)

gcm_flag = logical(0);

if isa(sst,'gcmfaces')
    gcm_flag=logical(1);
    sst = convert2gcmfaces(sst);
end
Nmo = size(sst,3);
Nyrs = Nmo/12;
sst_mean = nanmean(sst,3);
sst = sst - repmat(sst_mean,[1 1 Nmo]);
sst_clim = calcClimatology(sst,Nyrs);
sst_anom = sst - repmat(sst_clim,[1 1 Nyrs]);

if gcm_flag
    sst_anom = convert2gcmfaces(sst_anom);
end

end
