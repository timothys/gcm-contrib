% --- Add the essential packages to your matlab path
addpath(genpath('/work/projects/aci/ECCO/community/matlab'));
addpath(genpath('/work/projects/aci/ECCO/community/ECCO/ECCOv4/Release3/MITgcm_c66g/utils/matlab'))

% --- Optional! Uncomment these for nice plots :)
%set(0,'DefaultFigurePosition', [25 550 750 600]);
%set(0, 'DefaultAxesFontSize', 16);
%set(0, 'DefaultLineLineWidth', 2);
%set(0, 'DefaultAxesLineWidth', 2);
%set(0, 'DefaultPatchLineWidth', 2);
%set(0, 'DefaultFigureColor',[1 1 1]);
%
%set(groot,'DefaultTextInterpreter', 'latex')
%set(groot,'DefaultLegendInterpreter', 'latex')
%set(groot,'DefaultAxesTickLabelInterpreter', 'latex')
