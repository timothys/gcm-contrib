function [] = time_gcmfaces()
% Test how long it takes to run some standard gcmfaces scripts
%
%   1. establish_mygrid
%   2. calc_UEVNfromUXVY
%   3. calc_overturn
%   4. convert2gcmfaces
%   5. calcClimatology (my own)
%
% ------------------------------------------------------------

% --- 1. Load the grid
tic 
establish_mygrid('llc90');
t_mygrid = toc;

% --- 2. calc_UEVNfromUXVY
% Load 1 month of data for testing
diags_dir = '../../jupyter-examples/data/eccov4r2';
trsp_file = sprintf('%s/trsp_3d_set1',diags_dir);
iter = 732;

xvel = rdmds2gcmfaces(trsp_file,iter,'rec',1);
yvel = rdmds2gcmfaces(trsp_file,iter,'rec',2);

% Do the X->U Y->V exchange
f = @() calc_UEVNfromUXVY(xvel,yvel);
t_exch = timeit(f,2);

% --- 3. calc_overturn
f = @() calc_overturn(xvel,yvel,1,{'dh','dz'});
t_overturn = timeit(f);

% --- 4. convert2gcmfaces
% Maybe one of the most used functions ever...
f = @() convert2gcmfaces(xvel);
t_convert = timeit(f);

% --- 5. Compute seasonal cycle
Nyrs = 25;
Nmo = Nyrs*12;

%% 3D field climatology
%xvel_mat = convert2gcmfaces(xvel);
%xvel_mat = repmat(xvel_mat,[1 1 1 Nmo]);
%xvel_multiYear = convert2gcmfaces(xvel_mat);
%
%f = @() calcClimatology(xvel_multiYear,Nyrs);
%t_clim_3d = timeit(f);

% 2D field climatology
xvel = xvel(:,:,1);
xvel_mat = convert2gcmfaces(xvel);
xvel_mat = repmat(xvel_mat,[1 1 Nmo]);
xvel_multiYear = convert2gcmfaces(xvel_mat);

f = @() calcClimatology(xvel_multiYear,Nyrs);
t_clim_2d = timeit(f);

% --- Print the results
fprintf(' --- Timing Results ---\n');
fprintf('Load mygrid: %1.2f\n',t_mygrid);
fprintf('Velocity exchange: %1.2f\n',t_exch);
fprintf('Compute overturning streamfunction: %1.2f\n',t_overturn);
fprintf('Convert gcmfaces object to matrix: %1.2f\n',t_convert);
%fprintf('Compute %d year climatology of 3D field: %1.2f\n',Nyrs,t_clim_3d);
fprintf('Compute %d year climatology of 2D field: %1.2f\n',Nyrs,t_clim_2d);

end
