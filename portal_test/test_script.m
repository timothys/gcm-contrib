function [] = test_script()
% A test script for MATLAB users on stampede2
%
% 1. Load the grid
% 2. Load 1 record of velocity fields
% 3. Perform U/V "rectification"
% 4. Make a plot
% 5. Use the Gibb's seawater toolbox
%
% -------------------------------------------

% --- 1. Load the grid
mygrid = establish_mygrid('llc90');
fprintf('1. Load the grid: Success!\n');

% --- 2. Load 1 record of velocity fields
r3_nc_dir = '/work/projects/aci/ECCO/community/ECCO/ECCOv4/Release3/nctiles_monthly';
xvel = read_nctiles(sprintf('%s/UVELMASS/UVELMASS',r3_nc_dir),'UVELMASS',1);
yvel = read_nctiles(sprintf('%s/VVELMASS/VVELMASS',r3_nc_dir),'VVELMASS',1);
fprintf('2. Load 1 record of netcdf files: Success!\n');

% --- 3. Rectify the UX->UE situation
[uvel, vvel] = calc_UEVNfromUXVY(xvel,yvel);
fprintf('3. Perform gcmfaces computation: Success!\n');

% --- 4. Make a big ol' plot, and for the love of god don't use the jet colormap
fh=figure;
fh.Units='Normalized';
fh.Position=[0.0, 0.0, 0.7, 0.6];
cmap = flipdim(cbrewer2('BrBG',11),1);
m_map_gcmfaces(uvel(:,:,1),-1,{'myCmap',cmap});
fprintf('4. Make a nice map: Success!\n');

% --- 5. Use the GSW
depth = -500; %[m]
lat = 26; 
p_rel = gsw_p_from_z(depth,lat);
fprintf('5. Use Gibbs Sea Water toolbox: Success! \n(PS: relative pressure at 500m depth, 26N is about %0.2f\n',p_rel);

end
