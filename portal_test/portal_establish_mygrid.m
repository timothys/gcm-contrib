function [mygrid] = establish_mygrid(grid_name)
% Load the global mygrid variable 
%
% Input:
%
%   grid_name: Options include
%       'llc90'    
%
% Returns:
%
%   mygrid: an object with a lot of grid info
%
% ----------------------------------------------------------------

global mygrid;

if isempty(mygrid)
  
    if strcmp(grid_name,'llc90')
        grid_dir = '/work/projects/aci/ECCO/community/grids/llc90/';
        nFaces=5;
        fileFormat='compact';
    else
        error('grid name not recognized');
    end
    
  
    grid_load(grid_dir,nFaces,fileFormat);
    gcmfaces_global;

    % llc90: Want latitude/line masks for transport quantities
    % this was commented from recent gcmfaces version
    if strcmp(grid_name,'llc90')
        if ~isfield(mygrid,'mygrid.LATS_MASKS');
            gcmfaces_lines_zonal;
            mygrid.LATS=[mygrid.LATS_MASKS.lat]';
        end;
        if ~isfield(mygrid,'LINES_MASKS');
            [lonPairs,latPairs,names]=gcmfaces_lines_pairs;
            gcmfaces_lines_transp(lonPairs,latPairs,names);
        end;
    end

end
end
