function [] = grid_scratch()
% Want to play around with ASE grid 



% Start with boundary_conditions_flt ... 
gridDir='../../ase_grid'; %'boundary_conditions_flt';
g=read_grid( gridDir );

bc = read_boundary_conditions( gridDir );

%gridDir_llc270='llc_reg_270_11/boundary_conditions_flt';
%g_270 = read_grid( gridDir );
%g270=load_grid([gridDir_llc270 '/']);
%dxc_270 = read_bin(sprintf('%s/DXC.bin',gridDir_llc270));


figureW;
contourf(g.lonc,g.latc,bc.topo);
grid on

keyboard
end

function [bc] = read_boundary_conditions( bcDir )


topo = read_binary_grid(sprintf('%s/topo_md2.bin',bcDir));
%icetopo = read_binary_grid(sprintf('%s/icetopo_md2.bin',bcDir));
%shelfice_transcoeff = read_binary_grid(sprintf('%s/SHELFICETransCoeffTFile_run2_mmm.bin',bcDir));


bc = struct('topo',topo); %, ...
           % 'icetopo',icetopo,...
           % 'shelfice_transcoeff',shelfice_transcoeff);

end

function [g] = read_grid( gridDir )


dxc = read_binary_grid(sprintf('%s/DXC.bin',gridDir));
dxf = read_binary_grid(sprintf('%s/DXF.bin',gridDir));
dxg = read_binary_grid(sprintf('%s/DXG.bin',gridDir));
dxv = read_binary_grid(sprintf('%s/DXV.bin',gridDir));

dyc = read_binary_grid(sprintf('%s/DYC.bin',gridDir));
dyf = read_binary_grid(sprintf('%s/DYF.bin',gridDir));
dyg = read_binary_grid(sprintf('%s/DYG.bin',gridDir));
dyu = read_binary_grid(sprintf('%s/DYU.bin',gridDir));

ra = read_binary_grid(sprintf('%s/RA.bin',gridDir));
ras = read_binary_grid(sprintf('%s/RAS.bin',gridDir));
raw = read_binary_grid(sprintf('%s/RAW.bin',gridDir));
raz = read_binary_grid(sprintf('%s/RAZ.bin',gridDir));

latc = read_binary_grid(sprintf('%s/LATC.bin',gridDir));
latg = read_binary_grid(sprintf('%s/LATG.bin',gridDir));
lonc = read_binary_grid(sprintf('%s/LONC.bin',gridDir));
long = read_binary_grid(sprintf('%s/LONG.bin',gridDir));



g = struct('dxc',dxc,'dxf',dxf,'dxg',dxg,'dxv',dxv,...
           'dyc',dyc,'dyf',dyf,'dyg',dyg,'dyu',dyu,...
           'ra',ra,'ras',ras,'raw',raw,'raz',raz,...
           'latc',latc,'latg',latg,'lonc',lonc,'long',long);
end

function [fld] = read_binary_grid( fname )

ieee='b';
accuracy='real*4';

% From code/SIZE.h
sNx = 26; sNy = 56;
nSx = 1;  nSy = 1;
nPx = 4;  nPy = 4;

nx=sNx*nSx*nPx;
ny=sNy*nSy*nPy;

fid=fopen(fname,'r',ieee);
fld=fread(fid,[nx ny],accuracy);
fclose(fid);
end
