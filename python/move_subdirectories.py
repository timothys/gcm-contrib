#! /bin/python

# Python script to move/rename many subdirectories  
# Example directory tree:
# 
#       dir_name*/dir_name2*/oldStr
#
# all subdirs with "oldStr" get moved to
# 
#       dir_name*/dir_name2*/newStr


import os 
import shutil

rootdir     = '.'
oldStr      = 'sens92'
newStr      = 'dec.92'

for subdir, dirs, files in os.walk(rootdir):
    for dd in dirs:
        if dd.find('sens92') != -1:
            oldpath = os.path.join('%s/%s' % (subdir,dd))
            newpath = os.path.join('%s/dec.92' % subdir)
            print('old %s ' % oldpath)
            print('new %s ' % newpath)
            shutil.move(oldpath,newpath)
