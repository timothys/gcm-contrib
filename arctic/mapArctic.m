function [] = mapArctic(fld)
% Quickly, want to use m_map to try mapping something in the Arctic
% Todo: make this flexible for arctic grids

modelStr = 'arctic18km';
mygrid = establish_mygrid(modelStr);



% --- copied almost directly from m_map_gcmfaces
m_proj('Stereographic','lon',0,'lat',90,'rad',35);
myConv='convert2arctic';
xloc='bottom'; xtic=[-180:60:180];  xticlab=1;
yloc='bottom'; ytic=[60:10:90];     yticlab=1; 

ff=14;
m_grid_opt=['''XaxisLocation'',xloc,''YaxisLocation'',yloc,''xtick'',xtic,''ytick'',ytic,''fonts'',ff'];
if xticlab==0; m_grid_opt=[m_grid_opt ',''xticklabel'',[]']; end;
if yticlab==0; m_grid_opt=[m_grid_opt ',''yticklabel'',[]']; end;

% --- Lat/lon to xy
[x,y] = m_ll2xy(mygrid.XC,mygrid.YC);

% --- Plot it up?
pcolor(x,y,fld)
shading interp

% --- Draw the coast lines
m_coast('patch',[1 1 1]*.7,'edgecolor','none');
eval(['m_grid(' m_grid_opt ');']);

end
