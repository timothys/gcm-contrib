function [] = plotTempDiff()
% Quick plot of arctic domain showing differences between two 
% "online" runs with
%   "run.mean" uses diffKr = mean
%   "run.sample"    diffKr = mean + random perturbation
%
% ----------------------------------------------------------

modelStr = 'arctic18km';
meanStr = 'mean';
sampleStr = 'sample.7mo';

% --- Load up the grid
mygrid = establish_mygrid(modelStr);

% --- Set up directories
mean_results_dir = sprintf('../../results/%s/%s/diags',modelStr,meanStr);
sample_results_dir = sprintf('../../results/%s/%s/diags',modelStr,sampleStr);

fig_dir = sprintf('figures');
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;
fig_dir = sprintf('%s/%s',fig_dir,modelStr);
if ~exist(fig_dir,'dir'), mkdir(fig_dir); end;


% --- Read in the data
% state_2d_set1.*.meta
%   'ETAN    ' 'SIarea  ' 'SIheff  ' 'SIhsnow ' 'DETADT2 ' 'PHIBOT  ' 
%   'sIceLoad' 'SIatmQnt' 'SIatmFW ' 'oceQnet ' 'oceFWflx' 'oceTAUX ' 
%   'oceTAUY ' 'SIuice  ' 'SIvice  ' 'MXLDEPTH'
%
% state_3d_set1.*.meta
%   'THETA   ' 'SALT    ' 
%
% trsp_3d_set1.*.meta
%   'UVELMASS' 'VVELMASS'   
%

state2dStr = 'state_2d_set1';
state3dStr = 'state_3d_set1';
trsp3dStr = 'trsp_3d_set1';

% Grab iteration numbers b/c run.sample ended early (after 7.5 months)
% only compare these iters for now
iters = grabAllIters(sample_results_dir,state3dStr);
theta0 = rdmds(sprintf('%s/%s',mean_results_dir,state3dStr),iters,'rec',1);
thetap = rdmds(sprintf('%s/%s',sample_results_dir,state3dStr),iters,'rec',1);

salt0 = rdmds(sprintf('%s/%s',mean_results_dir,state3dStr),iters,'rec',2);
saltp = rdmds(sprintf('%s/%s',sample_results_dir,state3dStr),iters,'rec',2);


% --- Compute some diagnostics
[maxThetaDiff, kMaxTheta] = nanmax( abs( thetap - theta0 ),[],3);
[maxSaltDiff, kMaxSalt] = nanmax( abs( saltp - salt0 ),[],3);
maxThetaDiff = squeeze(maxThetaDiff); kMaxTheta = squeeze(kMaxTheta);
maxSaltDiff = squeeze(maxSaltDiff); kMaxSalt = squeeze(kMaxSalt);

meanThetaDiff = squeeze(nanmean( abs( thetap - theta0 ),3));
meanSaltDiff = squeeze(nanmean( abs( saltp - salt0 ),3));


% --- Plot it up

Nt = length(iters);
t = [1:Nt]*2; % Weeks
i=Nt;

selectVect = [1,8,Nt]; % 2wks, 16 wks, 32 wks

cmap = flipdim(cbrewer2('YlOrBr'),1);
cmap2 = flipdim(cbrewer2('YlGnBu'),1);

for i=selectVect
figureW;
subplot(1,2,1)
mapArctic(log10(maxThetaDiff(:,:,i)));
    hc=niceColorbar;
    colormap(cmap);
    xlabel(sprintf('max %s at t = %d wks','$\log_{10}|\theta_{pert}-\theta_{0}|$',t(i)));

subplot(1,2,2)
mapArctic(kMaxTheta(:,:,i));
    colormap(cmap);
    hc=niceColorbar;
    xlabel('Depth level at max difference');

savePlot(sprintf('%s/max_diff_theta_%02dwks',fig_dir,t(i)),'png');

figureW;
subplot(1,2,1)
mapArctic(log10(maxSaltDiff(:,:,i)));
    colormap(cmap);
    hc=niceColorbar;
    xlabel(sprintf('max %s at t = %d wks','$log_{10}|S_{pert}-S_{0}|$',t(i)));

subplot(1,2,2)
mapArctic(kMaxSalt(:,:,i));
    colormap(cmap);
    hc=niceColorbar;
    xlabel('Depth level at max difference');

savePlot(sprintf('%s/max_diff_salt_%02dwks',fig_dir,t(i)),'png');
end


for i=selectVect
figureW;
subplot(1,2,1)
mapArctic(log10(maxThetaDiff(:,:,i)));
    colormap(cmap);
    hc=niceColorbar;
    xlabel(sprintf('%s at t = %d wks','$\max\log_{10}|\theta_{pert}-\theta_{0}|$',t(i)));

subplot(1,2,2)
mapArctic(log10(maxSaltDiff(:,:,i)));
    colormap(cmap);
    hc=niceColorbar;
    xlabel(sprintf('%s at t = %d wks','$\max\log_{10}|S_{pert}-S_{0}|$',t(i)));

savePlot(sprintf('%s/max_diff_%02dwks',fig_dir,t(i)),'png');
end

keyboard

end
