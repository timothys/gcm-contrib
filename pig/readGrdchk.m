function [position_data,fc_data,grad_data] = readGrdchk(filename)
%
% function to plot grdchk output.
%
% Step 1: after running grdchk do 
%
%   >> grep grad-res STDOUT.0000 > grad_res_file.txt
%
% Step 2: grad_res_file.txt is filename here
%
% ------------------------------------------------------

fid = fopen(filename,'r');
all_data = fscanf(fid,'%f');
fclose(fid);

Nsize=length(all_data);
Ndata=16; % Num of entries in the file for a single point
Npts=Nsize/Ndata;

% Within grdchk_output file, grab the following information
% 1. ind, i,j,k,bi,bj info  
% 2. fc_ref, fc+eps, fc-eps
% 3. fd_grad, adj_grad, 1-fd/adj

position_data    = zeros(Npts,6);
fc_data          = zeros(Npts,3);
grad_data        = zeros(Npts,3);

for i=1:Npts
    ind=(i-1)*Ndata+1;
    position_data(i,:) = all_data( ind   :ind+5 )';
    fc_data(i,:)       = all_data( ind+9 :ind+11)';
    grad_data(i,:)     = all_data( ind+13:ind+15)';
end


end
