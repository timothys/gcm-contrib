function [] = plotBathymetry(gridRes)
if nargin<1, gridRes=8; end;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));

% --- Read some files
bin_dir=sprintf('/workspace/ase-adjoint/pig/init_%02d/',gridRes);
%bin_dir='/workspace/MITgcm_c66l/mysetups/pig/input_ad_08_dan/';
bathy = read_bin([bin_dir 'bathymetry.pig.bin'],1,0,64);
shelftopo = read_bin([bin_dir 'shelftopo.pig.bin'],1,0,64);
pload = read_bin([bin_dir 'pload.pig.mdjwf'],1,0,64);

% --- 
clevs=20;
figure;
subplot(2,2,1),contourf(mygrid.XC,mygrid.YC,mygrid.Depth,clevs);
  title('Depth (m)'), colorbar
subplot(2,2,2),contourf(mygrid.XC,mygrid.YC,bathy,clevs);
  title('Bathymetry (m)'), colorbar
subplot(2,2,3),contourf(mygrid.XC,mygrid.YC,shelftopo,clevs);
  title('Shelf Topography (m)'), colorbar
subplot(2,2,4),contourf(mygrid.XC,mygrid.YC,pload);
  title('Pressure load (kpa?)'), colorbar

  keyboard
end
