function [] = plotDiags()


% --- Grab grid for this setup 
gridRes=32;
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% --- Grab diagnostics
diagsDir = '/workspace/results/pig/parameter-sensitivity/both_adgf/diags/';
eta   = rdmds([diagsDir 'state_2d'],NaN,'rec',1);
uvel  = rdmds([diagsDir 'state_3d'],NaN,'rec',1);
vvel  = rdmds([diagsDir 'state_3d'],NaN,'rec',2);
theta = rdmds([diagsDir 'state_3d'],NaN,'rec',4);
salt  = rdmds([diagsDir 'state_3d'],NaN,'rec',5);
fwflx = rdmds([diagsDir 'shelfice'],NaN,'rec',1);
htflx = rdmds([diagsDir 'shelfice'],NaN,'rec',2);
forcS = rdmds([diagsDir 'shelfice'],NaN,'rec',3);
forcT = rdmds([diagsDir 'shelfice'],NaN,'rec',4);
gammS = rdmds([diagsDir 'shelfice'],NaN,'rec',5);
gammT = rdmds([diagsDir 'shelfice'],NaN,'rec',6);
uStar = rdmds([diagsDir 'shelfice'],NaN,'rec',7);
uDrag = rdmds([diagsDir 'shelfice'],NaN,'rec',8);
vDrag = rdmds([diagsDir 'shelfice'],NaN,'rec',9);

shelfice = struct('fwflx',fwflx,'htflx',htflx,...
                  'forcS',forcS,'forcT',forcT,...
                  'gammS',gammS,'gammT',gammT,...
                  'uStar',uStar,...
                  'uDrag',uDrag,'vDrag',vDrag);

% Want only in data structure, don't want copies of vars
% want shelfice to be clear about what the var is
clear fwflx htflx forcS forcT gammS gammT uStar uDrag vDrag

if ~isempty(strfind(diagsDir,'spinup'))
  plotDomainMean(theta,'Temperature');
  plotDomainMean(salt,'Salt');
end


if isempty(strfind(diagsDir,'spinup'))
  zlev=10;
  plotContourMovie(log10(shelfice.gammT));
end

keyboard
end

function [] = plotDomainMean(fld,fldStr)

% --- Plot domain mean quantities from spinup
global mygrid;
Nr = size(fld,3);
Nt = size(fld,4);


% Scale by grid cell volume
drf = repmat( reshape( mygrid.DRF, [1 1 length(mygrid.DRF)]), size(mygrid.DXG));
fld = fld.*repmat(mygrid.DXG,[1 1 Nr Nt]).*...
                 repmat(mygrid.DYG,[1 1 Nr Nt]).*...
                 repmat(drf,[1 1 1 Nt]); 

global_fld = squeeze(nansum(nansum(nansum(fld,1),2),3));
totalVolume = nansum(nansum(nansum(repmat(mygrid.DXG, [1 1 Nr]).*...
                                   repmat(mygrid.DYG, [1 1 Nr]).*...
                                   drf)));

global_fld = global_fld/totalVolume;

% --- Meaningful time axis, 10 year spinup
diagnosticFreq = 1209600 / 3600 / 24 / 365 ; % [yrs]
t = [1:Nt]*diagnosticFreq;

figure;
plot(t,global_fld)
  title(sprintf('Domain Volume Avg %s vs Time',fldStr))
  xlabel('Years')
end

function [] = plotContourMovie(fld,zlev, uvel, vvel)

% Default: plot velocities as quiver plot unless not passed as args
plotVels=1;

global mygrid
if ~exist('zlev','var')
  zlev=10;
end

if nargin<3
  plotVels = 0;
end

if length(size(fld)) == 2

        % plot a still figure
        Nt = 1;
        is2D=1; is3D=0; is4D=0;

elseif length(size(fld)) == 3

        % plot 2D over time
        Nt = size(fld,3);
        is2D=0; is3D=1; is4D=0;
else
        % plot 3D at zlev over time
        Nt = size(fld,4);
        is2D=0; is3D=0; is4D=1;
end

figure;
for n=1:Nt

  if is2D 
    contourf(mygrid.XC,mygrid.YC,fld);
  elseif is3D
    contourf(mygrid.XC,mygrid.YC,fld(:,:,n));
  else
    contourf(mygrid.XC,mygrid.YC,fld(:,:,klev,n))
  end
    
  if plotVels
    hold on;
    quiver(mygrid.XC,mygrid.YC,uvel(:,:,zlev,n),vvel(:,:,zlev,n),'k');
    hold off;
  end
  colorbar
  pause(0.5)
end
end
