#!/bin/python

# Another script to test log10ctrl option
# Now we have the following data.ctrl file:
#
# ------------------------------------------
# &CTRL_NML
# doInitxx = .true.,
# doMainUnpack = .false.,
# doMainPack= .false.,
# &
# &CTRL_PACKNAMES
# &
# &CTRL_NML_GENARR
# xx_genarr2d_weight(1) = 'wt_ones.data',
# xx_genarr2d_file(1) = 'xx_shicoefft',
## xx_genarr2d_preproc(1,1) = 'noscaling',
# xx_genarr2d_preproc_c(1,1) = 'log10ctrl',
# xx_genarr2d_preproc_r(1,1) = -4.0,
# mult_genarr2d(1) = 0.,
# &
# ------------------------------------------
#
# Basically, we have doInitXX=.true. but we want to
# initialize with a nonzero prior value. Note that 
# with log10(ctrlvar), we don't necessarily want 
#
# fld = 10^0 
# 
# we want
#
# fld = 10^-4
#
# hence the option xx_genarr2d_preproc_r.
# 
# Test 1:
# Verify the diagnostic variable and xx_.effective for shicoefft is 10^-4
#
# Test 2: 
# Verify the sensitivity to the field and control parameter 
# coincide. That is
#
#  dJ/d(ctrl_param) = dJ/dfld * ln(10) * 10**xx_genarr2d_preproc_r
#
# Test 3:
# verify that if xx_genarr2d_preproc='noscaling' is uncommented and
# xx_genarr2d_preproc_c='log10ctrl', CTRL_CHECK will raise an error
# Completed elsewhere...
#
# ------------------------------------------------------------------

import numpy as np
from xmitgcm import open_mdsdataset
from pig_tools import diag_plot

run_dir = '/workspace/results/pig/test-log-ctrl/init_xx'
grid_dir = '/workspace/grids/pig_08'
ctrl = open_mdsdataset(data_dir=run_dir,
                     grid_dir=grid_dir,
                     iters=0)
diags=open_mdsdataset(data_dir=('%s/diags' % run_dir),
                      grid_dir=grid_dir,
                      prefix='state_2d')
adj=open_mdsdataset(data_dir=('%s/adj-diags' % run_dir),
                    grid_dir=grid_dir,
                    prefix='adjState_2d')

xc = ctrl.XC
yc = ctrl.YC
depth = ctrl.Depth

# Test 1:
# Plot xx_shicoefft.effective and diags/SHIgammT
# both should be constant 10^-4
diag_plot(xc,yc,ctrl['xx_shicoefft.effective'].isel(time=0),
          diags['SHIgammT'].isel(time=0),
          title1='xx$\_$shicoefft.effective',
          title2='SHIgammT from pkg/diags',
          depth=depth)

# Test 2:
# Verify sensitivities

dJdctrl = adj['ADJshict'].isel(time=0) * np.log(10) * np.power(10.,-4.)

diag_plot(xc,yc,ctrl['adxx_shicoefft'].isel(time=0),
          dJdctrl,
          title1='adxx$\_$shicoefft',
          title2='$\partial J/\partial \gamma_T \log(10) 10^{-4}$',
          depth=depth)

diag_plot(xc,yc,ctrl['adxx_shicoefft'].isel(time=0) - dJdctrl,
          title1='adxx$\_$shicoefft - $\partial J/\partial \gamma_T \log(10) 10^{-4}$',
          depth=depth)
