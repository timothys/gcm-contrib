#!/bin/python

from plotShiOptimUpdates import readCostFunction, readAllCostFunctions

filename = '/workspace/ase-adjoint/pig/generic/shicoeff_optim/run_ad_08.shiOptim.00/costfunction0000'

test = readCostFunction(filename)

#print(test)

tt = readAllCostFunctions('/workspace/ase-adjoint/pig/generic/shicoeff_optim/run_ad_08.shiOptim',20)

print(tt['fc'])
print(tt['xx_shicoefft'])
