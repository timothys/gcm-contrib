#!/workspace/anaconda3/bin python

# --- plotUpdateMasksDebug
# 
# script to debug the different depth formulations
# Reg: typical run
#
# Smooth or sm: with ALLOW_DEPTH_CONTROL + ALLOW_SMOOTH_MIN
#               uses approximated min() that is differentiable
#
# Rough or ns: with ALLOW_DEPTH_CONTROL + undef ALLOW_SMOOTH_MIN
#              should be the same as Reg.

import os
import sys
import numpy as np
import xarray as xr
import xgcm
from xmitgcm import open_mdsdataset
import matplotlib.pyplot as plt

def quickplot(x,y,fld1,fld2,z_level=1,filename=None):
    # --- If no vertical coordinate, just plot difference
    #     of 2D field for third plot
    cmap = 'YlGnBu'

    fld_diff = fld1 - fld2
    diff_cmap = 'BrBG'
    cmax = np.max(np.abs(fld_diff.values))
    cmin = -cmax 
    
    # --- If vertical coordinate exists, compute some of abs diff
    if 'Z' in fld1.coords:
        if np.size(fld_diff['Z']) > 1:
            fld_diff = np.abs(fld1 - fld2)
            fld_diff = fld_diff.sum('Z')
            fld1 = fld1.isel(Z=z_level)
            fld2 = fld2.isel(Z=z_level)
            diff_cmap = 'YlGnBu'
            cmax = np.max(fld_diff.values)
            cmin = 0.0
                                                                                    
                                                                                                
    fig = plt.figure(figsize=(15,6))
    plt.subplot(1,3,1)
    plt.pcolormesh(x,y,fld1,cmap=cmap)
    plt.colorbar()
             
    plt.subplot(1,3,2)
    plt.pcolormesh(x,y,fld2,cmap=cmap)
    plt.colorbar()
                             
    plt.subplot(1,3,3)
    plt.pcolormesh(x,y,fld_diff.values,cmap=diff_cmap,
                     vmin=cmin, vmax=cmax)
    plt.colorbar()

    if filename is not None:
        plt.savefig(filename,bbox_inches='tight',dpi=300)

    plt.show()

