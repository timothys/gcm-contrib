function [] = genPigCtrls(gridRes)
%
% Generate zero ctrl vectors for PIG domain
% sensitivity experiments. Only variable here is 
% domain resolution. 
%
% (optional) Input: 
%       
%       gridRes: scalar
%                represents grid resolution, options are
%                8 (default), 16, 32
% 
% ------------------------------------------------------


if nargin<1, gridRes=8; end;

prec=64;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));

% The list for now ...
list_ctrl_2d = {'etan','bottomdrag','depth'};
list_ctrl_3d = {'theta','salt','uvel','vvel','diffkr','kapredi','kapgm'};
list_ctrl_gentim2d = {'tauu','tauv'}; % can make more on the fly

% Make 2d and 3d ctrls
xx_genarr2d = 0*mygrid.mskC(:,:,1);
xx_genarr2d(isnan(xx_genarr2d))=0;

%% ---
%%% testing depth 
%xx_depth = 0*mygrid.mskC(:,:,1);
%xx_depth(1,10)= .01;
%
%xx_depth(isnan(xx_depth))=0;
%% ---

xx_genarr3d = 0*mygrid.mskC;
xx_genarr3d(isnan(xx_genarr3d))=0;

% Make gentim2d ctrls
Nt = 10; % still testing ...
xx_gentim2d = repmat(xx_genarr2d,[1 1 Nt]);

% Write all to file
saveDir = sprintf('zero-ctrls-pig-%02d',gridRes);
if ~exist(saveDir,'dir'), mkdir(saveDir); end;
prefix = [saveDir '/xx_'];
suffix = '.0000000000.data';
% genarr2d
for i=1:length(list_ctrl_2d)
  write2file([prefix list_ctrl_2d{i} suffix], xx_genarr2d,prec);
  write2meta([prefix list_ctrl_2d{i} suffix], size(xx_genarr2d),prec);
end
% genarr3d
for i=1:length(list_ctrl_3d)
  write2file([prefix list_ctrl_3d{i} suffix], xx_genarr3d,prec);
  write2meta([prefix list_ctrl_3d{i} suffix], size(xx_genarr3d),prec);
end
% gentim2d
for i=1:length(list_ctrl_gentim2d)
  write2file([prefix list_ctrl_gentim2d{i} suffix], xx_gentim2d,prec);
  write2meta([prefix list_ctrl_gentim2d{i} suffix], size(xx_gentim2d),prec);
end

%% ---
%write2file([prefix 'depth' suffix],xx_depth,prec);
%write2meta([prefix 'depth' suffix],size(xx_depth),prec);
%% ---

% Make a 1 weights field
wt_ones = mygrid.mskC;
wt_ones(isnan(wt_ones))=0;
wt_ones(:) = 1;

write2file([saveDir '/wt_ones.data'],wt_ones,prec);
write2meta([saveDir '/wt_ones.data'],size(wt_ones),prec);

% tar and zip
tar([saveDir '.tar.gz'],saveDir);
end
