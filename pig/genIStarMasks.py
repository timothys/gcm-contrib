#!/bin/python

# Script to specifically generate the iStar8/9 mooring masks
# From Davis et al., (2018) citing Webber et al., (2017), 2 moorings at 
# - Southern (iStar9): 75.059$^\circ$S 102.152$^\circ$W  
# - Northern (iStar8): 74.863$^\circ$S 102.104$^\circ$W
# which are iStar8 & iStar9 in Webber et al., (2017).
# 
# Collect measurements at roughly the depth levels **very rough, reading off a plot**:
# - 380m
# - 425m
# - 480m
# - 540m 
# - 610m
# - 700m
# 
# but need actual dataset to confirm. 
#**Also, the Southern mooring was pushed downslope because of a 
#  large calving event between 2009-2013**

import os 
import tarfile
import glob
import numpy as np
from xmitgcm import open_mdsdataset
import genMooringMasks as mm

# Set grid resolution
grid_res = 8

# Location of the moorings
lat8 = -75.059
lon8 = -102.152
lat9 = -74.863
lon9 = -102.104
mooring_depth = np.array((-380,-425,-480,-540,-610,-700))

# Create a grid only dataset
grid_dir = ('/workspace/grids/pig_%02d' % grid_res)
ds = open_mdsdataset(data_dir=grid_dir,grid_dir=grid_dir,iters=None)

# Create mask in lat/lon/depth space
iStar8 = mm.genMooringMask(ds,lat8,lon8,mooring_depth,mask_name='iStar8')
iStar9 = mm.genMooringMask(ds,lat9,lon9,mooring_depth,mask_name='iStar9')

# Create a temporal mask
temporal_mask = np.array((0,0,0,0,0,1,0))
temporal_mask_string = 'np.array((0,0,0,0,0,1,0))'

# Write out mask files
mask_dir = ('istar_masks_%02d' % grid_res)
if not os.path.isdir(mask_dir):
    os.makedirs(mask_dir)

mm.writeMaskFile(iStar8.values,('%s/iStar8_3d_mskC' % mask_dir),
    comments=iStar8.attrs['description'])
mm.writeMaskFile(temporal_mask,('%s/iStar8_3d_mskT' % mask_dir),
    comments=temporal_mask_string)

mm.writeMaskFile(iStar9.values,('%s/iStar9_3d_mskC' % mask_dir),
    comments=iStar9.attrs['description'])
mm.writeMaskFile(temporal_mask,('%s/iStar9_3d_mskT' % mask_dir),
    comments=temporal_mask_string)

# Tar the directory for sending to another machine
filelist = glob.glob(('%s/*' % mask_dir))
with tarfile.open( ('%s.tar.gz' % mask_dir), 'w:gz') as tar:
    for name in filelist:
        tar.add(name)
print('Created %s.tar.gz' % mask_dir)
