function [] = pig_mask(gridRes)
% All PIG binaries are double precision...
% ... except in the generic ecco/ctrl packages
% (optional) Input: 
%       
%       gridRes: scalar
%                represents grid resolution, options are
%                8 (default), 16, 32
% 
% ------------------------------------------------------
prec=64;

if nargin<1, gridRes=8; end;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));

if ~exist('masks/','dir'), mkdir('masks'),end

%% 2D masks  
mskC_2d = nanmax(mygrid.mskC,[],3);
mskC = mygrid.XC;
keyboard
%mskC(:)=1.0;
mskC(:)=0.0;
mskC(3,13,:)=1.0; %outgoing flow
mskC(3,17,:)=1.0; %middle
mskC(3,21,:)=1.0; %middle
mskC(3,25,:)=1.0; %incoming flow
%mskC=mygrid.mskC(:,:,1);
mskC(isnan(mskC))=0;

% transport masks
mskW=0*mygrid.mskW(:,:,1);
mskS=0*mygrid.mskS(:,:,1);
mskW=mygrid.XG==-101.75;
mskW(isnan(mskW))=0; mskS(isnan(mskS))=0;

%% Depth mask
NR=length(mygrid.DRF);
mskK = ones(1,NR); %[1 zeros(1,NR-1)];

%% Temporal mask
%mskT = [zeros(1,228) 0 0 0 0 0 0 0 0 0 0 0 1];
%mskT = [zeros(1,11) 1];
% This doesn't matter for observation data...
% Only for sensitivities
mskT = [0 0 1 0 0 0 0];
%mskT=[zeros(1,119) 1 0 0 0];


% Make a 1 weights field
wt_ones = mygrid.mskC;
wt_ones(isnan(wt_ones))=0;
wt_ones(:) = 1;

write2file(['masks/wt_ones.data'],wt_ones,prec);
write2meta(['masks/wt_ones.data'],size(wt_ones),prec);

%% Write to file
write2file('masks/pig_maskC',mskC,prec);
write2file('masks/pig_maskS',mskS,prec);
write2file('masks/pig_maskW',mskW,prec);
write2file('masks/pig_maskK',mskK,prec);
write2file('masks/pig_maskT',mskT,prec);

%% For reproducibility and such ... copy this file as well so we know what made the masks
copyfile('pig_mask.m','masks/');

% Zip it up for sending to a server
tar('masks.tar.gz','masks/');
keyboard
end
