function [] = plotDepthSensitivity(gridRes)
% (optional) Input: 
%       
%       gridRes: scalar
%                represents grid resolution, options are
%                8 (default), 16, 32
% 
% ------------------------------------------------------
prec=64;

if nargin<1, gridRes=8; end;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% --- Load up sensitivity
resultsDir='/workspace/ase-adjoint/pig/generic/run_ad_08/';
adxx_depth = rdmds([resultsDir 'adxx_depth']);
%adxx_eta = rdmds([resultsDir 'adxx_etan']);
%adxx_theta = rdmds([resultsDir 'adxx_theta']);
%adxx_salt = rdmds([resultsDir 'adxx_salt']);

% --- Factor for getting rid of adjoint noise
%boxmeanFactor=10^-7;
%adxx_depth=boxmeanFactor*adxx_depth;
%adxx_eta=boxmeanFactor*adxx_eta;
%adxx_theta=boxmeanFactor*adxx_theta;
%adxx_salt=boxmeanFactor*adxx_salt;

% --- Stuff for plotting
nContoursFill=20;
nContourLines=15;
mskC = nanmax(mygrid.mskC,[],3);

% --- Plot depth sensitivity
figure;
contourf(mygrid.XC,mygrid.YC,adxx_depth,nContoursFill);


hold on;
lineColor=cbrewer2('greys',10);
lineColor=lineColor(10,:);
contour(mygrid.XC,mygrid.YC,mygrid.Depth,nContourLines,...
'lineColor',lineColor,'LineWidth',1);
hold off;


cmax=nanmax(abs(adxx_depth(:)));
caxis([-cmax cmax]);
hc=niceColorbar;
cmap=cbrewer2('RdBu',nContoursFill);
cmap=flipdim(cmap,1);
colormap(cmap);
ylabel(hc,'$dJ/dr_{low}$')

%% --- Plot eta sensitivity
%figure;
%contourf(mygrid.XC,mygrid.YC,adxx_eta,nContoursFill);
%hold on;
%contour(mygrid.XC,mygrid.YC,mygrid.Depth,nContourLines,...
%        'lineColor',lineColor,'LineWidth',1);
%hold off;
%cmax=nanmax(abs(adxx_eta(:)));
%caxis([-cmax cmax]);
%hc=niceColorbar;
%colormap(cmap);
%ylabel(hc,'$dJ/d\eta$')
%
%% --- Plot theta sensitivity
%klev = 20;
%mskC=mygrid.mskC;
%mskC(mskC==0)=NaN;
%figure;
%contourf(mygrid.XC,mygrid.YC,adxx_theta(:,:,klev),nContoursFill);
%hold on;
%contour(mygrid.XC,mygrid.YC,mygrid.Depth,nContourLines,...
%        'lineColor',lineColor,'LineWidth',1);
%hold off;
%cmax=nanmax(abs(adxx_theta(:)));
%caxis([-cmax cmax]);
%hc=niceColorbar;
%colormap(cmap);
%ylabel(hc,'$dJ/d\theta$')
%
%% --- Plot salt sensitivity
%figure;
%contourf(mygrid.XC,mygrid.YC,adxx_salt(:,:,klev),nContoursFill);
%hold on;
%contour(mygrid.XC,mygrid.YC,mygrid.Depth,nContourLines,...
%        'lineColor',lineColor,'LineWidth',1);
%hold off;
%cmax=nanmax(abs(adxx_salt(:)));
%caxis([-cmax cmax]);
%hc=niceColorbar;
%colormap(cmap);
%ylabel(hc,'$dJ/dS$')

keyboard
end
