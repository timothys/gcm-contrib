function [] = plotBathyCtrlUpdates(optimCycle)
% In optimization, want to plot updated bathymetry from last iteration
% Assumes 1/8th degree grid resolution
% 
% Input:
%
%       optimCycle: 1,2,... Grab the ctrls associated with appropriate optim cycle
%
% --------------------------------------------------------------------
prec=64;
gridRes=8;

if nargin<1, error('Feed me the optimization cycle number :O \n'); end;

% --- Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% --- Grab the data
run_dir = '/workspace/ase-adjoint/pig/generic/run_ad_08.bathyPrior/';
results_dir = [run_dir ''];

shelftopo = read_bin([run_dir 'shelftopo.pig.bin'],1,0,64);
bathy_prior = read_bin([run_dir 'bathymetry_noRidgePrior.bin'],1,0,prec);
bathy_truth = read_bin([run_dir 'bathymetryAdj.pig.bin'],1,0,prec);

adxx = rdmds([results_dir 'adxx_depth'],optimCycle-1);
xx_eff = rdmds([results_dir 'xx_depth.effective'],optimCycle);
xx_noDimAdj = rdmds([results_dir 'xx_depth'],optimCycle);
xx_adj = xx_eff-bathy_prior;

prior_weights=read_bin([run_dir 'bathymetry_weights.bin'],1,0,prec);
prior_weights(prior_weights~=.0001)=0;

% get mask for missing ridge
ridge_msk = bathy_truth-bathy_prior ~= 0;

% colormap
nContoursFill=15;
cmap=cbrewer2('RdBu',nContoursFill);
cmap=flipdim(cmap,1);



% --- Plot the adjustments with units
figureH;
subplot(2,2,1),
contourf(mygrid.XC,mygrid.YC,xx_adj,nContoursFill);
hold on
contour(mygrid.XC,mygrid.YC,bathy_prior,'k');
maxAbsAdj=max(abs(xx_adj(:)));
caxis([-maxAbsAdj maxAbsAdj]);
hold off
hc=niceColorbar;
title('Adjustments (m)')
ylabel(hc,'$dr_{low}$');


% --- Plot sensitivity
subplot(2,2,2),
contourf(mygrid.XC,mygrid.YC,adxx.*prior_weights,nContoursFill);
hold on
contour(mygrid.XC,mygrid.YC,bathy_prior,'k');
maxAbsAdj=max(abs(adxx(:).*prior_weights(:)));
caxis([-maxAbsAdj maxAbsAdj]);
hold off
hc=niceColorbar;
title('Sensitivity (m/m)')
ylabel(hc,'$dJ/dr_{low}$')


% --- Plot both with ridge mask
subplot(2,2,3),
contourf(mygrid.XC,mygrid.YC,xx_adj.*ridge_msk,nContoursFill);
hold on
contour(mygrid.XC,mygrid.YC,bathy_prior,'k');
maxAbsAdj=max(abs(xx_adj(:).*ridge_msk(:)));
caxis([-maxAbsAdj maxAbsAdj]);
hold off
hc=niceColorbar;
ylabel(hc,'$dr_{low}$');

subplot(2,2,4),
contourf(mygrid.XC,mygrid.YC,adxx.*ridge_msk,nContoursFill);
hold on
contour(mygrid.XC,mygrid.YC,bathy_prior,'k');
maxAbsAdj=max(abs(adxx(:).*ridge_msk(:)));
caxis([-maxAbsAdj maxAbsAdj]);
hold off
hc=niceColorbar;
ylabel(hc,'$dJ/dr_{low}$')

colormap(cmap);

%% --- Plot (iceshelf topo - bathymetry updates)
%%     should be positive ...
%figure;
%contourf(mygrid.XC,mygrid.YC,shelftopo-xx_eff)
%hold on
%contour(mygrid.XC,mygrid.YC,bathy_prior,'k');
%maxAbsAdj=max(abs(shelftopo(:)-xx_eff(:)));
%caxis([-maxAbsAdj maxAbsAdj]);
%hold off
%hc=niceColorbar;
%ylabel(hc,'shelftopo - (r$_{low,0}+\delta r_{low}$)')
%colormap(cmap)

keyboard

end
