function [] = plotDiags(gridRes)
% (optional) Input: 
%       
%       gridRes: scalar
%                represents grid resolution, options are
%                8 (default), 16, 32
% 
% ------------------------------------------------------
prec=64;

if nargin<1, gridRes=8; end;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));

% --- Grab diagnostics
diagsDir = '/workspace/ase-adjoint/pig/generic/run_ad_08.bathyPrior/diags/';
%%eta = rdmds([diagsDir 'surfDiag'],NaN,'rec',1);
%%theta = rdmds([diagsDir 'dynDiag'],NaN,'rec',4);
%%salt = rdmds([diagsDir 'dynDiag'],NaN,'rec',5);
uvel = rdmds([diagsDir 'dynDiag'],NaN,'rec',1);
vvel = rdmds([diagsDir 'dynDiag'],NaN,'rec',2);
wvel = rdmds([diagsDir 'dynDiag'],NaN,'rec',3);
phivel=rdmds([diagsDir 'depth_stuff_3d'],NaN,'rec',1);
psivel=rdmds([diagsDir 'depth_stuff_3d'],NaN,'rec',2);
phihyd=rdmds([diagsDir 'depth_stuff_3d'],NaN,'rec',5);
ubotdrag=rdmds([diagsDir 'depth_stuff_3d'],NaN,'rec',6);
vbotdrag=rdmds([diagsDir 'depth_stuff_3d'],NaN,'rec',7);
usiddrag=rdmds([diagsDir 'depth_stuff_3d'],NaN,'rec',8);
vsiddrag=rdmds([diagsDir 'depth_stuff_3d'],NaN,'rec',9);

phi_surf=rdmds([diagsDir 'depth_stuff_2d'],NaN,'rec',1);
phibot=rdmds([diagsDir 'depth_stuff_2d'],NaN,'rec',2);

% --- Grab xx fld
%xx = read_bin([diagsDir '../xx_obcsw.0000000000.001.001.data'],1,0,64);
%  plotDomainMean(theta,'Temperature');
%  plotDomainMean(salt,'Salt');

% --- plot diags movie
zlev=20;
Nt =size(phivel,4);%size(theta,4);
%diagMovieDepthDive(theta,Nt);

%diagMovie(eta,zlev,Nt);
%diagMovie(theta,zlev,Nt);
%diagMovie(salt,zlev,Nt);
%diagMovie(uvel,zlev,Nt);
%diagMovie(vvel,zlev,Nt);

%diagMovieDepthDive(phivel,1);
%keyboard
%diagMovieDepthDive(psivel,1);
%keyboard
keyboard

diagMovieDepthDive(sqrt(ubotdrag.^2+vbotdrag.^2),1);
keyboard
diagMovieDepthDive(sqrt(usiddrag.^2+vsiddrag.^2),1);
keyboard
diagMovie(phi_surf,1,Nt);
keyboard
diagMovie(phibot,1,Nt);




keyboard
end

function [] = diagMovie(fld,zlev,Nt)

global mygrid;

mskC=mygrid.mskC;
mskC_2d = nanmax(mygrid.mskC,[],3);
mskC_2d(mskC_2d==0)=NaN;
mskC(mskC==0)=NaN;

figure;
for n=1:Nt
  %contourf(mygrid.XC,mygrid.YC,theta(:,:,zlev,n));
  if length(size(fld))<4
    contourf(mygrid.XC,mygrid.YC,fld(:,:,n).*mskC_2d);
  else
    contourf(mygrid.XC,mygrid.YC,fld(:,:,zlev,n).*mskC(:,:,zlev));
  end
  %hold on;
  %quiver(mygrid.XC,mygrid.YC,uvel(:,:,zlev,n),vvel(:,:,zlev,n),'k');
  %caxis([-1 1]);
  niceColorbar;
  pause(0.5)
  %hold off
end
end

function [] = diagMovieDepthDive(fld,n_time)
% Plot movie going down in depth at a point in time: n_time
global mygrid;

mskC=mygrid.mskC;
mskC_2d = nanmax(mygrid.mskC,[],3);
mskC_2d(mskC_2d==0)=NaN;
mskC(mskC==0)=NaN;
Nr = length(mygrid.DRF);
figure;
for k=1:Nr
  %contourf(mygrid.XC,mygrid.YC,theta(:,:,zlev,n));
  contourf(mygrid.XC,mygrid.YC,fld(:,:,k,n_time).*mskC(:,:,k));
  %hold on;
  %quiver(mygrid.XC,mygrid.YC,uvel(:,:,zlev,n),vvel(:,:,zlev,n),'k');
  %caxis([-1 1]);
  niceColorbar;
  pause(0.5)
  %hold off
end
end

function [fldDiff, maxFldDiff] = calcQuickDiff(fld1,fld2)

fldDiff = abs(fld1-fld2)./abs(fld2);

maxFldDiff=nanmax(fldDiff(:));
end

function [] = plotDomainMean(fld,fldStr)

% --- Plot domain mean quantities from spinup
global mygrid;
Nr = size(fld,3);
Nt = size(fld,4);


% Scale by grid cell volume
drf = repmat( reshape( mygrid.DRF, [1 1 length(mygrid.DRF)]), size(mygrid.DXG));
fld = fld.*repmat(mygrid.DXG,[1 1 Nr Nt]).*...
                 repmat(mygrid.DYG,[1 1 Nr Nt]).*...
                 repmat(drf,[1 1 1 Nt]); 

global_fld = squeeze(nansum(nansum(nansum(fld,1),2),3));
totalVolume = nansum(nansum(nansum(repmat(mygrid.DXG, [1 1 Nr]).*...
                                   repmat(mygrid.DYG, [1 1 Nr]).*...
                                   drf)));

global_fld = global_fld/totalVolume;

% --- Meaningful time axis, 10 year spinup
diagnosticFreq = 1209600 / 3600 / 24 / 365 ; % [yrs]
t = [1:Nt]*diagnosticFreq;

figure;
plot(t,global_fld)
  title(sprintf('Domain Volume Avg %s vs Time',fldStr))
  xlabel('Years')
end
