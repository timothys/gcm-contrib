#! /bin/bash

# Generate inputs for prior covariance matrix, computed by pkg/smooth
# 1. smooth2Dscales001.meta/data: 2 2D fields with
#     - 3 * dXC
#     - 3 * dYC
#     
# 2. smooth3DscalesH001.meta/data: 2 3D fields, aka 100 2D fields, where 
#     - 1-50 have 3*dXC
#     - 51-100 have 3*dYC
#     
# 3. smooth3DscalesZ001.meta/data: 1 3D field with
#     - 2 * dRC 

import os
import numpy as np
import xarray as xr
from xmitgcm import open_mdsdataset
import MITgcmutils as mut

from pig_tools import diag_plot


# Simple function to write mds files
def smooth_writer(fname,arr,comments=None):
    if comments is not None:
        mut.wrmds(fbase=fname,arr=arr,dataprec='float64',simulation=comments)
    else:
        mut.wrmds(fbase=fname,arr=arr,dataprec='float64')
    print('%s.meta/data files written ...' % fname)

# Set grid resolution
grid_res = 8

# Load grid
grid_dir = ('/workspace/grids/pig_%02d' % grid_res)
ds = open_mdsdataset(data_dir=grid_dir, 
                     grid_dir=grid_dir,
                     geometry='sphericalpolar',
                     iters=None)


# Make directory
smooth_dir = ('pig_prior_%02d' % grid_res)
if not os.path.isdir(smooth_dir):
    os.makedirs(smooth_dir)

# Create arrays with horizontal scales
horizontal_scales_2d = np.stack((3*ds.dxC.values,3*ds.dyC.values),axis=0)
horizontal_scales_3d = np.concatenate((np.tile(3*ds.dxC.values, [50,1,1]),
                                 np.tile(3*ds.dyC.values, [50,1,1])))

# Now vertical scales
vertical_scales = np.full_like(ds.hFacC.values,ds.drF.mean())

# write it up ...
smooth_writer(('%s/smooth2Dscales001' % smooth_dir), arr=horizontal_scales_2d, 
              comments='2 2D fields. 3*dxC, 3*dyC. Concatenated on 3rd dim.'
             )
smooth_writer(('%s/smooth3DscalesH001' % smooth_dir), arr=horizontal_scales_3d, 
              comments='2 3D fields. 3*dxC, 3*dyC. Both 3D fields, concatenated on 3rd dim.'
             )
smooth_writer(('%s/smooth3DscalesZ001' % smooth_dir), arr=vertical_scales, 
              comments='1 3D field. 3*drF. Constant across x,y dimensions.'
             )
