function [] = plotDiagDiff(gridRes)
% (optional) Input: 
%       
%       gridRes: scalar
%                represents grid resolution, options are
%                8 (default), 16, 32
% 
% ------------------------------------------------------
prec=64;

if nargin<1, gridRes=8; end;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% --- Grab diagnostics
resultsDir ='/workspace/results/pig/nlfs_32/rStar1_with-depth/'
diagsDir = [resultsDir 'diags/'];
%resultsDir = '/workspace/MITgcm_c66l/mysetups/pig/';
%resultsDir = '/workspace/ase-adjoint/pig/generic/';
%diagsDir = [resultsDir 'run_ad_08/diags/'];

iters=grabAllIters(diagsDir,'surfDiag');
eta = rdmds([diagsDir 'surfDiag'],NaN,'rec',1);
theta = rdmds([diagsDir 'dynDiag'],NaN,'rec',4);
salt = rdmds([diagsDir 'dynDiag'],NaN,'rec',5);
uvel = rdmds([diagsDir 'dynDiag'],NaN,'rec',1);
vvel = rdmds([diagsDir 'dynDiag'],NaN,'rec',2);

resultsDir = '/workspace/results/pig/nlfs_32/rStar1_no-depth/';
diagsDir = [resultsDir 'diags/'];
%resultsDir = '/workspace/MITgcm_c66l/mysetups/pig/';
%diagsDir = [resultsDir 'run_ad_08_noDepth/diags/'];
eta2 = rdmds([diagsDir 'surfDiag'],NaN,'rec',1);
theta2 = rdmds([diagsDir 'dynDiag'],NaN,'rec',4);
salt2 = rdmds([diagsDir 'dynDiag'],NaN,'rec',5);
uvel2 = rdmds([diagsDir 'dynDiag'],NaN,'rec',1);
vvel2 = rdmds([diagsDir 'dynDiag'],NaN,'rec',2);

% --- Grab xx fld
%xx = read_bin([diagsDir '../xx_obcsw.0000000000.001.001.data'],1,0,64);
Nt = size(theta2,4);

[etaDiff, maxEtaDiff] = calcQuickDiff(eta(:,:,1:Nt),eta2(:,:,1:Nt));
[thetaDiff, maxThetaDiff] = calcQuickDiff(theta(:,:,:,1:Nt),theta2(:,:,:,1:Nt));
[saltDiff, maxSaltDiff] = calcQuickDiff(salt(:,:,:,1:Nt),salt2(:,:,:,1:Nt));
[uvelDiff, maxUvelDiff] = calcQuickDiff(uvel(:,:,:,1:Nt),uvel2(:,:,:,1:Nt));
[vvelDiff, maxVvelDiff] = calcQuickDiff(vvel(:,:,:,1:Nt),vvel2(:,:,:,1:Nt));

maxEtaDiff
maxThetaDiff
maxSaltDiff
maxUvelDiff
maxVvelDiff

keyboard

% --- plot blowup movie
zlev = 20;

diagMovie(etaDiff,zlev,Nt);
diagMovie(thetaDiff,zlev,Nt);
diagMovie(uvelDiff,zlev,Nt);

keyboard
end


function [] = diagMovie(fld,zlev,Nt)

global mygrid;

figure;
for n=1:Nt
  %contourf(mygrid.XC,mygrid.YC,theta(:,:,zlev,n));
  if length(size(fld))<4
    contourf(mygrid.XC,mygrid.YC,fld(:,:,n));
  else
    contourf(mygrid.XC,mygrid.YC,fld(:,:,zlev,n));
  end
  %hold on;
  %quiver(mygrid.XC,mygrid.YC,uvel(:,:,zlev,n),vvel(:,:,zlev,n),'k');
  %caxis([-1 1]);
  niceColorbar;
  pause(0.5)
  %hold off
end
end

function [fldDiff, maxFldDiff] = calcQuickDiff(fld1,fld2)

fldDiff = abs(fld1-fld2)./abs(fld2);

maxFldDiff=nanmax(fldDiff(:));
end
