#!/bin/python

# Test the control variable log10(gamma) so that 
# e.g. SHIheatTransCoeff = prior + 10 ** xx_shicoefft

import os 
import numpy as np
from xmitgcm import open_mdsdataset
import MITgcmutils as mut

from pig_tools import diag_plot


# Place to put test vectors and results
test_dir = '/workspace/results/pig/test-log-ctrl/provide_xx_files'
if not os.path.isdir(test_dir):
    os.makedirs(test_dir)

# Open up a dataset with the grid etc.
grid_dir = '/workspace/grids/pig_08'
ds = open_mdsdataset(data_dir=grid_dir,
                     grid_dir=grid_dir,
                     iters=None
                    )

# Make an array with e.g. -4 (so SHIheatTransCoeff = 10^-4 
xx_shicoefft = ds['Depth'].copy().values
xx_shicoeffs = ds['Depth'].copy().values

# Make it vary with latitude, more negative at lower latitude 
for y in np.arange(len(ds.YC)):
    xx_shicoefft[y,:] = -4 - y/10
    xx_shicoeffs[y,:] = -7 + y/10


# Plot it up
diag_plot(ds.XC,ds.YC,xx_shicoefft,xx_shicoeffs,
        title1='xx\_shicoefft, log ctrls',
        title2='xx\_shicoeffs, log ctrls',
        depth=ds.Depth)

# Write to files
filename=('%s/xx_shicoefft') % test_dir
mut.wrmds(fbase=filename,
        arr=xx_shicoefft,
        itr=0,
        dataprec='float64'
     )
print('File %s.meta/data written ...' % filename)


filename=('%s/xx_shicoeffs') % test_dir
mut.wrmds(fbase=filename,
        arr=xx_shicoeffs,
        itr=0,
        dataprec='float64'
     )
print('File %s.meta/data written ...' % filename)

# If diagnostics directory exists, plot diagnosed field 
diags_dir = ('%s/diags' % test_dir)
if os.path.isdir(diags_dir):
    diags = open_mdsdataset(data_dir=diags_dir,grid_dir=grid_dir)
    diag_plot(ds.XC,ds.YC,diags['SHIgammT'],diags['SHIgammS'],
            title1='SHIgammT',title2='SHIgammS',depth=ds.Depth)

