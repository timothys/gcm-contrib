function [] = plotAdScratch()

resultsDir = '/workspace/ase-adjoint/pig/generic/run_ad_08/';
resultsDir2 ='/workspace/ase-adjoint/pig/no-ecco/run_ad_08/';

% Grab mygrid for this setup 
gridRes=8;
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% Load adxx file ...
adxx_depth = rdmds([resultsDir 'adxx_depth']);
adxx_theta = rdmds([resultsDir 'adxx_theta']);
adxx_salt = rdmds([resultsDir 'adxx_salt']);

%adxx_depth2 = rdmds([resultsDir2 'adxx_depth']);
%adxx_theta2 = rdmds([resultsDir2 'adxx_theta']);
%adxx_salt2 = rdmds([resultsDir2 'adxx_salt']);
suff='.0000000000.001.001.data';
adxx_depth2 = read_bin([resultsDir2 'adxx_depth' suff],1,0,64);
adxx_theta2 = read_bin([resultsDir2 'adxx_theta' suff],1,[],64);
adxx_salt2 =  read_bin([resultsDir2 'adxx_salt'  suff],1,[],64);

thetaDiff = adxx_theta-adxx_theta2;
saltDiff = adxx_salt - adxx_salt2;
depthDiff= adxx_depth - adxx_depth2;
%depthDiff(adxx_depth<(10^-18) & adxx_depth2<(10^-18))=0;
%depthDiff(depthDiff<(10^-16))=0;
%depthDiff(depthDiff==0)=NaN;

%adxx_theta2 = read_bin([resultsDir2 'adxx_theta.0000000000.001.001.data'],1,0,64);
%adxx_salt2 = read_bin([resultsDir2 'adxx_salt.0000000000.001.001.data'],1,0,64);
%adxx_etan = rdmds([resultsDir 'adxx_etan']);
%adxx_bottomdrag = rdmds([resultsDir 'adxx_bottomdrag']);
%adxx_uvel = rdmds([resultsDir 'adxx_uvel']);
%adxx_vvel = rdmds([resultsDir 'adxx_vvel']);
%adxx_obcsw = read_bin([resultsDir 'adxx_obcsw.0000000000.001.001.data'],1,0,64);

% Load diags
%shi_fwflx = rdmds([resultsDir 'diags/surfDiag'],NaN,'rec',7);

% Plot up weird adxx thing
t = 2;
t_unit='day';
klev=10;
px=3;
py=3;

figureH;
subplot(px,py,1)
  contourf(mygrid.XC,mygrid.YC,adxx_theta(:,:,klev),40)
  niceColorbar;
  ylabel('$dJ/d\theta_0$')
  title('Tim''s Setup')
subplot(px,py,2)
  contourf(mygrid.XC,mygrid.YC,adxx_theta2(:,:,klev),40)
  niceColorbar;
  title('Dan''s Setup')
subplot(px,py,3)
  relErr= abs(thetaDiff)./abs(adxx_theta);
  contourf(mygrid.XC,mygrid.YC,relErr(:,:,klev),40)
  niceColorbar;
  title('Relative Error')

subplot(px,py,4)
  contourf(mygrid.XC,mygrid.YC,adxx_salt(:,:,klev),40)
  niceColorbar;
  ylabel('$dJ/dS_0$')
subplot(px,py,5)
  contourf(mygrid.XC,mygrid.YC,adxx_salt2(:,:,klev),40)
  niceColorbar;
subplot(px,py,6)
  relErr= abs(saltDiff)./abs(adxx_salt);
  contourf(mygrid.XC,mygrid.YC,relErr(:,:,klev),40)
  niceColorbar;

subplot(px,py,7)
  contourf(mygrid.XC,mygrid.YC,adxx_depth,40)
  niceColorbar;
  ylabel('$dJ/dr_{low}$')
subplot(px,py,8)
  contourf(mygrid.XC,mygrid.YC,adxx_depth2,40)
  niceColorbar;
ax9= subplot(px,py,9);
  relErr= abs(depthDiff)./abs(adxx_depth2);
  contourf(mygrid.XC,mygrid.YC,relErr,31)
  niceColorbar;


%figureH;
%subplot(2,2,1),contourf(mygrid.XC,mygrid.YC,adxx_depth,40)
%  niceColorbar
%subplot(2,2,2),contourf(mygrid.XC,mygrid.YC,adxx_depth2,40)
%  niceColorbar
%subplot(2,2,3),contourf(mygrid.XC,mygrid.YC,adxx_depth-adxx_depth2,40)
%  niceColorbar
  

%figureH;
%ax1=subplot(px,py,1);
%  contourf(mygrid.XC,mygrid.YC,mygrid.Depth,40);
%  colormap(ax1,parula)
%  niceColorbar
%  title('Depth')
%  grid on
%ax2=subplot(px,py,2);
%  contourf(mygrid.XC,mygrid.YC,mygrid.hFacCsurf);
%  colormap(ax2,gray)
%  niceColorbar
%  title('hFacCsurf')
%  grid on
%ax3=subplot(px,py,3);
%  contourf(mygrid.XC,mygrid.YC,shi_fwflx(:,:,t))
%  colormap(ax3,parula)
%  niceColorbar
%  title(sprintf('Shelfice Freshwater Flux,\n t = %s %d',t_unit,t));
%  grid on
%
%ax4=subplot(px,py,4);
%  contourf(mygrid.XC,mygrid.YC,adxx_theta(:,:,klev),15);
%  colormap(ax4,redblue)
%  niceColorbar
%  caxis(max(max(abs(adxx_theta(:,:,klev))))*[-1 1]);
%  title(sprintf('adxx theta0, k=%d',klev))
%  grid on
%ax5=subplot(px,py,5);
%  contourf(mygrid.XC,mygrid.YC,adxx_salt(:,:,klev),15);
%  colormap(ax5,redblue)
%  niceColorbar
%  caxis(max(max(abs(adxx_salt(:,:,klev))))*[-1 1]);
%  title(sprintf('adxx salt0, k=%d',klev))
%  grid on
%ax6=subplot(px,py,6);
%  contourf(mygrid.XC,mygrid.YC,adxx_etan(:,:),15);
%  colormap(ax6,redblue);
%  niceColorbar
%  caxis(max(max(abs(adxx_etan(:,:))))*[-1 1]);
%  title(sprintf('adxx etan0'))
%  grid on
%
%ax7=subplot(px,py,7);
%  contourf(mygrid.XC,mygrid.YC,adxx_uvel(:,:,klev),15);
%  colormap(ax7,redblue);
%  niceColorbar
%  caxis(max(max(abs(adxx_uvel(:,:,klev))))*[-1 1]);
%  title(sprintf('adxx uvel0, k=%d',klev))
%  grid on
%ax8=subplot(px,py,8);
%  contourf(mygrid.XC,mygrid.YC,adxx_vvel(:,:,klev),15);
%  colormap(ax8,redblue);
%  niceColorbar
%  caxis(max(max(abs(adxx_vvel(:,:,klev))))*[-1 1]);
%  title(sprintf('adxx vvel, k=%d',klev))
%  grid on
%ax9=subplot(px,py,9);
%  contourf(mygrid.XC,mygrid.YC,adxx_bottomdrag(:,:),15);
%  colormap(ax9,redblue);
%  niceColorbar
%  caxis(max(max(abs(adxx_bottomdrag(:,:))))*[-1 1]);
%  title(sprintf('adxx bottomdrag'))
%  grid on
%
%saveas(gcf,'figures/adxx_all_but_depth','png')

keyboard

end
