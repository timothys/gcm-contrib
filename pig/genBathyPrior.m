function [] = genBathyPrior()
% Generate a ''prior'' for bathymetry in simple inversion experiment
% Remove some features from the PIG bathymetry and see if optimizer can
% discover them...
% 
% Assuming 1/8th degree grid resolution
%
% ------------------------------------------------------------------

prec=64;

% Grab mygrid for this setup 
gridRes=8;
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% --- Read in ''true'' bathymetry
init_dir = '/workspace/ase-adjoint/pig/init_08/';
prior_dir= '/workspace/results/pig/bathy-eta-inversion/bathy-prior/';
bathy_truth = readbin([init_dir 'bathymetryAdj.pig.bin'],size(mygrid.XC),1,'real*8');

% --- Remove the famous PIG ridge
bathy_prior=bathy_truth;

% Define outer ring
i=[10,11,11,11,12,13,14,15,16,17,17,17,17,17,16,15,14,13,13,12,11];
j=[10, 9, 8, 7, 7, 6, 7, 7, 7, 7, 8, 9,10,11,12,13,14,14,13,12,11];

ring=0*bathy_prior;
for n=1:length(i)
    ring(i(n),j(n))=1;
end    

% --- Use boundary and eyeballs to grab the ridge
ridge=0*ring;
ridge(13,7:12)=2*ones(1,6);
ridge(12:16,8:11)=2*ones(5,4);
ridge(14:15,12:13)=2*ones(2,2);
ridge(15,13)=0;
ridge(11,10)=2;

% --- layer this thing
interior=0*ridge;
ii=[13,13,14,15,15,15,14];
jj=[09,10,11,11,10,09,09];
for n=1:length(ii)
        interior(ii,jj)=1;
end

center=0*ridge;
center(14,10)=1;

remove_ridge=-(ring+ridge+interior+center);
removeFact=50;

bathy_prior=bathy_prior+removeFact*remove_ridge;

% --- Really making a guess here for uncertainties ...
% Assume 100m under the iceshelf, 20m in open ocean
% 0 uncertainty beyond domain boundary ... means 
% that dry cells can't open up for now

% Assume we know the extent of the oceanic cavity
% probably a strong assumption ... :(
mskC=bathy_prior~=0;

% Omitting this part...
%% cutting off diagonal of PIG cavity
%iii=7:16;jjj=9:18;
%Ni=size(bathy_prior,1);
%cavity_mask=0*bathy_prior;
%
%for n=1:jjj(1)
%        cavity_mask(iii(1):Ni,n) = ones(Ni-iii(1)+1,1);
%end
%
%for n=1:length(iii)
%        cavity_mask(iii(n):Ni,jjj(n)) = ones(Ni-iii(n)+1,1);
%end

% Set to 20m for all wet points 
prior_uncertainty = 20*mskC;

prior_uncertainty = prior_uncertainty + 80*isnan(mygrid.mskC(:,:,1)).*mskC;

% --- Convert uncertainty to weights for MITgcm ctrl package
prior_weight = prior_uncertainty.^-2;
prior_weight(isinf(prior_weight))=0;
%prior_weight=prior_weight.*mskC;

% --- Write to file
truth_file = [prior_dir 'bathymetry_truth.data'];
prior_file=[prior_dir 'bathymetry_noRidgePrior.data'];
uncertainty_file=[prior_dir 'bathymetry_uncertainty.data'];
weight_file=[prior_dir 'bathymetry_weights.data'];

write2file(truth_file,bathy_truth,prec);
write2meta(truth_file,size(bathy_truth),prec);

write2file(prior_file,bathy_prior,prec);
write2meta(prior_file,size(bathy_prior),prec);

write2file(uncertainty_file,prior_uncertainty,prec);
write2meta(uncertainty_file,size(prior_uncertainty),prec);

write2file(weight_file,prior_weight,prec);
write2meta(weight_file,size(prior_weight),prec);

fprintf('Wrote %s ...\n',truth_file);
fprintf('Wrote %s ...\n',prior_file);
fprintf('Wrote %s ...\n',uncertainty_file);
fprintf('Wrote %s ...\n',weight_file);

%% For reproducibility and such ... copy this file as well so we know what made the masks
copyfile('genBathyPrior.m',prior_dir);

fprintf('copied this matlab file to %s ...\n',prior_dir);
keyboard

end
