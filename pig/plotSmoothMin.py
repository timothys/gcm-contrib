#!/bin/python

# plotSmoothMin
#   Functions to visualize the impact of smoothing out the min/max functions
#   and if/else statements associated with setting hFac, rLow, rSurf 

import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy

def smooth_hFac(hFacIn,hFacMin=0.3,minThreshold=None):
    # Replace if statements for computing hFac with smooth version
    # inputs: 
    #   hFacIn:    input hFac value to threshold
    #   hFacMin:   minimum value for hFac

    hFac = np.zeros(np.shape(hFacIn))

    for i in range(np.size(hFacIn,axis=0)):

        # original code thresholds at 0 rather than 0.5*hFacMin 
        # note: when not zero, disrupt smoothness within [0,1]
        if minThreshold is not None:
            low_threshold = minThreshold
        else:
            low_threshold = 0

        # threshold tests for hFac
        if hFacIn[i] < low_threshold:
            hFac[i] = 0
        elif hFacIn[i] > 1:
            hFac[i] = 1
        else:
            hFac[i] = hFacIn[i] +  \
                hFacMin * ( np.exp( -hFacIn[i]/hFacMin ) - np.exp( -1.0/hFacMin ) )


    return hFac

# --------------------------------------------------------------------

def hFac_spline(x,hFacMin,epsilon):
    # Spline to make hFac thresholding a closer, smooth
    # approximation to pure if statements 
    # Spline is g(x) defined as:
    #
    #       g(x) := a(x-x0)^3 + b(x-x0) + c(x-x0) + d
    #
    # with x0 = hFacMin and 
    #   a = -1/epsilon^3
    #   b = 2/epsilon
    #   c = 0
    #   d = hFacMin
    #
    # so that f \in C^1([0.5,1]) 
    #
    # inputs:
    #   x:          input hFac value to threshold
    #   hFacMin:    minimum threshold for hFac
    #   epsilon:    region for smooth thresholding

    a = -1. / pow(epsilon,2)
    b =  2. / epsilon
    d = hFacMin

    del_x = x - hFacMin

    g = a * pow(del_x,3) + b * pow(del_x,2) + d

    return g

# --------------------------------------------------------------------

def smooth_hFac2(hFacIn,hFacMin=0.3,epsilon=0.1):
    # Compute smooth min with spline.
    # f(x) = { 0 ,      x < 0.5 hFacMin
    #         hFacMin,  0.5 hFacMin <= x <= hFacMin 
    #         g(x),     hFacMin <= x <= hFacMin+epsilon
    #          x ,      hFacMin+epsilon <= x <= 1
    #          1,       x > 1 }
    # 
    # where g(x) is the cubic spline defined in hFac_spline

    #
    # inputs: 
    #   hFacIn:         input hFac value to threshold
    #   hFacMin:        minimum value for hFac
    #   epsilon:        region for smooth thresholding

    hFac = np.zeros(np.shape(hFacIn))

    for i in range(np.size(hFacIn,axis=0)):

        # threshold tests for hFac
        if hFacIn[i] < 0.5*hFacMin:
            hFac[i] = 0
        elif hFacIn[i] <= hFacMin:
            hFac[i] = hFacMin
        elif hFacIn[i] <= hFacMin + epsilon:
            hFac[i] = hFac_spline(hFacIn[i],hFacMin,epsilon)
        elif hFacIn[i] <= 1:
            hFac[i] = hFacIn[i]
        else:
            hFac[i] = 1

    return hFac

# --------------------------------------------------------------------

def reg_hFac(hFacIn,hFacMin=0.3):
    # Standard if statements for computing hFac: 
    # inputs:
    #   hFacIn:    input hFac value to threshold
    #   hFacMin:   minimum value for hFac

    hFac = np.zeros(np.shape(hFacIn))

    for i in range(np.size(hFacIn,axis=0)):

        hFac[i] = min(max(hFacIn[i],0),1)

        if hFac[i] < hFacMin:
            if hFacIn[i] < 0.5 * hFacMin:
                hFac[i] = 0
            else:
                hFac[i] = hFacMin
        else:
            hFac[i] = hFac[i]

    return hFac

# --------------------------------------------------------------------

def smooth_abs(x,smoothAbsRange=0.0):
    # Smooth version of absolute value function
    # inputs:
    #   x: compute |x|
    #
    # optional inputs:
    #   smoothAbsRange: parameter to control smoothness
    #                   of function

    sf = 0.0
    rsf = 0.0
    result = np.zeros(np.shape(x))
    
    if smoothAbsRange <= 0:
        return result
    else:
        sf = 10.0 / smoothAbsRange
        rsf = 1.0 / sf

        for i in range(np.size(x,axis=0)):
            if x[i] >= smoothAbsRange:
                result[i] = x[i]
            elif x[i] <= -smoothAbsRange:
                result[i] = -x[i] 
            else:
                result[i] = np.log( 0.5 * (np.exp( x[i]*sf ) + np.exp( -x[i]*sf )))*rsf

        return result

# --------------------------------------------------------------------

def smooth_min(x,y,alpha=0.0):
    # Take min of x & y
    # inputs: 
    #    x,y: scalars or arrays to take min of

    return 0.5 * ( x+y - smooth_abs(x-y,smoothAbsRange=alpha) )

# --------------------------------------------------------------------

def smooth_max(x,y,alpha=0.0):

    return 0.5 * ( x+y + smooth_abs(x-y,smoothAbsRange=alpha) )


# --------------------------------------------------------------------

def smooth_min_2(x,y,alpha=-10.0):
    # Take min of x & y
    # inputs: 
    #    x,y: scalars or arrays to take min of

    z = np.zeros(np.shape(x),dtype=np.float64)

    for i in range(np.size(x,axis=0)):
        a1 = np.multiply(alpha,x[i])
        a2 = np.multiply(alpha,y[i])

        z[i] = x[i]*np.exp(a1) + y[i]*np.exp(a2)
        tmp = np.exp(a1)+np.exp(a2)
        z[i] = np.divide(z[i], tmp)

    return z

# --------------------------------------------------------------------

def smooth_max_2(x,y,alpha=-10.0):
    # Take max of x & y
    # inputs: 
    #    x,y: scalars or arrays to take min of

    z = np.zeros(np.shape(x),dtype=np.float64)

    for i in range(np.size(x,axis=0)):
        a1 = np.multiply(alpha,y[i])
        a2 = np.multiply(alpha,x[i])

        z[i] = x[i]*np.exp(a1) + y[i]*np.exp(a2)
        tmp = np.exp(a1)+np.exp(a2)
        z[i] = np.divide(z[i], tmp)

    return z

# --------------------------------------------------------------------

def real_min(x,y):

    z = np.zeros(np.shape(x))

    for i in range(len(x)):
        z[i] = min(x[i],y[i])

    return z

# --------------------------------------------------------------------

# plotting scripts

def plot_min(x,y,smin):
    n = np.arange(len(x))

    plt.plot(n,x,n,y)
    plt.plot(n,real_min(x,y))
    plt.plot(n,smin)
    plt.legend(('x','y','min(x,y)','smin(x,y)'))

# --------------------------------------------------------------------

