function [] = plotDepthDebug()


% --- Grab grid for this setup 
gridRes=8;
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% --- Load various r_low fields
results_dir = '/workspace/MITgcm_c66l/mysetups/pig/run_ad_08_tim/';

[r_low,ro_surf,hfacc] = loadResults(results_dir);
[hfacc_3d,hfacw_3d] = load3dResults(results_dir);


% --- Debug plots
plotBigDiff(r_low,'rLow');
plotBigDiff(ro_surf,'r0surf');
plotBigDiff(hfacc,'hFacC');

% --- Take 3D diff
calc3Ddiff(hfacc_3d,'hFacC');
calc3Ddiff(hfacw_3d,'hFacW');

keyboard

end
function [] = plotBigDiff(fld,fldStr)

% Fontsize for big title on each plot
fs=10;
global mygrid;

figureH;
subplot(3,3,1)
  contourf(mygrid.XC,mygrid.YC,fld.iniDepths)
  colorbar
  title(sprintf('%s ini depths',fldStr))
  set(gca,'fontsize',fs)
subplot(3,3,2)
  contourf(mygrid.XC,mygrid.YC,fld.iniMasksEtc-fld.iniDepths)
  colorbar
  title(sprintf('%s diff(iniMasksEtc,iniDepths)',fldStr))
  set(gca,'fontsize',fs)
subplot(3,3,3)
  contourf(mygrid.XC,mygrid.YC,fld.iniVariaStart-fld.iniMasksEtc)
  colorbar
  title(sprintf('%s diff(iniVariaStart,iniMasksEtc)',fldStr))
  set(gca,'fontsize',fs)
subplot(3,3,4)
  contourf(mygrid.XC,mygrid.YC,fld.iniDepths2-fld.iniMasksEtc)
  colorbar
  title(sprintf('%s diff(iniDepthsVaria,iniMasksEtc)',fldStr))
  set(gca,'fontsize',fs)
subplot(3,3,5)
  contourf(mygrid.XC,mygrid.YC,fld.ctrlDepthIni-fld.iniMasksEtc)
  colorbar
  title(sprintf('%s diff(ctrlDepthIni,iniMasksEtc)',fldStr))
  set(gca,'fontsize',fs)
subplot(3,3,6)
  contourf(mygrid.XC,mygrid.YC,fld.shelficeInitDepths-fld.iniMasksEtc)
  colorbar
  title(sprintf('%s diff(shelficeInitDepths,iniMasksEtc)',fldStr))
  set(gca,'fontsize',fs)
subplot(3,3,7)
  contourf(mygrid.XC,mygrid.YC,fld.updateMasksEtc-fld.iniMasksEtc)
  colorbar
  title(sprintf('%s diff(updateMasksEtc,iniMasksEtc)',fldStr))
  set(gca,'fontsize',fs)

end

function [] = calc3Ddiff(fld,fldStr)

fprintf('Total 3D diff(iniVariaStart, updateMasksEtc): \n \n')
fprintf('  %s: %d \n',fldStr,nansum(abs(fld.iniVariaStart(:) - fld.updateMasksEtc(:))))

end

function [r_low,ro_surf,hfacc] = loadResults(results_dir)


% --- And now, in calling order ...
r_low_iniDepths = rdmds([results_dir 'r_low_iniDepths_iniFixed']);
ro_surf_iniDepths = rdmds([results_dir 'ro_surf_iniDepths_iniFixed']);
hfacc_iniDepths = rdmds([results_dir 'hfacc_iniDepths_iniFixed']);

r_low_iniMasksEtc = rdmds([results_dir 'r_low_iniMasksEtc_iniFixed']);
ro_surf_iniMasksEtc = rdmds([results_dir 'ro_surf_iniMasksEtc_iniFixed']);
hfacc_iniMasksEtc = rdmds([results_dir 'hfacc_iniMasksEtc_iniFixed']);

r_low_iniVariaStart = rdmds([results_dir 'r_low_iniVariaStart']);
ro_surf_iniVariaStart = rdmds([results_dir 'ro_surf_iniVariaStart']);
hfacc_iniVariaStart = rdmds([results_dir 'hfacc_iniVariaStart']);


if ~isempty(strfind(results_dir,'noDepth'))
    r_low_iniDepths2=r_low_iniDepths;
    r_low_ctrlDepthIni=r_low_iniDepths;
    r_low_shelficeInitDepths=r_low_iniDepths;

    ro_surf_iniDepths2=ro_surf_iniDepths;
    ro_surf_ctrlDepthIni=ro_surf_iniDepths;
    ro_surf_shelficeInitDepths=ro_surf_iniDepths;

    hfacc_iniDepths2=hfacc_iniDepths;
    hfacc_ctrlDepthIni=hfacc_iniDepths;
    hfacc_shelficeInitDepths=hfacc_iniDepths;
else
    r_low_iniDepths2 = rdmds([results_dir 'r_low_iniDepths_iniVaria']);
    r_low_ctrlDepthIni = rdmds([results_dir 'r_low_ctrlDepthIni']);
    r_low_shelficeInitDepths = rdmds([results_dir 'r_low_shelficeInitDepths']);

    ro_surf_iniDepths2 = rdmds([results_dir 'ro_surf_iniDepths_iniVaria']);
    ro_surf_ctrlDepthIni = rdmds([results_dir 'ro_surf_ctrlDepthIni']);
    ro_surf_shelficeInitDepths = rdmds([results_dir 'ro_surf_shelficeInitDepths']);
    
    hfacc_iniDepths2 = rdmds([results_dir 'hfacc_iniDepths_iniVaria']);
    hfacc_ctrlDepthIni = rdmds([results_dir 'hfacc_ctrlDepthIni']);
    hfacc_shelficeInitDepths = rdmds([results_dir 'hfacc_shelficeInitDepths']);
end

r_low_updateMasksEtc = rdmds([results_dir 'r_low_updateMasksEtc']);
ro_surf_updateMasksEtc = rdmds([results_dir 'ro_surf_updateMasksEtc']);
hfacc_updateMasksEtc = rdmds([results_dir 'hfacc_updateMasksEtc']);

r_low = struct('iniDepths',r_low_iniDepths,...
               'iniMasksEtc',r_low_iniMasksEtc,...
               'iniVariaStart',r_low_iniVariaStart,...
               'iniDepths2',r_low_iniDepths2,...
               'ctrlDepthIni',r_low_ctrlDepthIni,...
               'shelficeInitDepths',r_low_shelficeInitDepths,...
               'updateMasksEtc',r_low_updateMasksEtc);

ro_surf = struct('iniDepths',ro_surf_iniDepths,...
               'iniMasksEtc',ro_surf_iniMasksEtc,...
               'iniVariaStart',ro_surf_iniVariaStart,...
               'iniDepths2',ro_surf_iniDepths2,...
               'ctrlDepthIni',ro_surf_ctrlDepthIni,...
               'shelficeInitDepths',ro_surf_shelficeInitDepths,...
               'updateMasksEtc',ro_surf_updateMasksEtc);

hfacc = struct('iniDepths',hfacc_iniDepths,...
               'iniMasksEtc',hfacc_iniMasksEtc,...
               'iniVariaStart',hfacc_iniVariaStart,...
               'iniDepths2',hfacc_iniDepths2,...
               'ctrlDepthIni',hfacc_ctrlDepthIni,...
               'shelficeInitDepths',hfacc_shelficeInitDepths,...
               'updateMasksEtc',hfacc_updateMasksEtc);

end

function [hfacc,hfacw] = load3dResults(results_dir)

hfacc_iniVariaStart = rdmds([results_dir 'hfacc_3d_iniVariaStart']);
hfacw_iniVariaStart = rdmds([results_dir 'hfacw_3d_iniVariaStart']);

hfacc_updateMasksEtc = rdmds([results_dir 'hfacc_3d_updateMasksEtc']);
hfacw_updateMasksEtc = rdmds([results_dir 'hfacw_3d_updateMasksEtc']);



hfacc = struct('iniVariaStart',hfacc_iniVariaStart,...
               'updateMasksEtc',hfacc_updateMasksEtc);
hfacw = struct('iniVariaStart',hfacw_iniVariaStart,...
               'updateMasksEtc',hfacw_updateMasksEtc);

end
