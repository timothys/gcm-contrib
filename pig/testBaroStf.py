#!/bin/python

# A test to see if script to compute barotropic streamfunction is correct
# Compare to vertical avg of PsiVel

import xgcm
from xmitgcm import open_mdsdataset

import pig_tools as pt

grid_dir = '/workspace/grids/pig_32'
ds = open_mdsdataset(data_dir='/workspace/results/pig/shicoeff-sensitivities/istar-ts-1mo-2014/diags',
                     grid_dir=grid_dir)

ds2 = open_mdsdataset(data_dir='/workspace/results/pig/shicoeff-sensitivities/istar-ts-1mo-2012/diags',
        grid_dir=grid_dir)

xg = xgcm.Grid(ds,periodic=False)

stf1 = pt.calc_baro_stf(xg,ds)
stf2 = pt.calc_baro_stf(xg,ds2)

pt.diag_plot(ds.XC,ds.YC,stf1.mean('time'),stf2.mean('time'),
        title1=('Max = %.2f Sv' % stf1.mean('time').max()),
        title2=('Max = %.2f Sv' % stf2.mean('time').max()),
        depth=ds.Depth,c_lim=(-.06,.06))

adj1 = open_mdsdataset(data_dir='/workspace/results/pig/shicoeff-sensitivities/istar-ts-1mo-2014',
            grid_dir=grid_dir)
adj2= open_mdsdataset(data_dir='/workspace/results/pig/shicoeff-sensitivities/istar-ts-1mo-2012',
            grid_dir=grid_dir)

pt.diag_plot(ds.XC,ds.YC,adj1['adxx_shicoefft'],adj2['adxx_shicoefft'],depth=ds.Depth)
