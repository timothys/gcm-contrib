#!/bin/python

from xmitgcm import open_mdsdataset

import pig_tools as pt

grid_dir = '/workspace/grids/pig_32'
ds = open_mdsdataset(data_dir='/workspace/results/pig/shicoeff-sensitivities/istar-ts-1mo-2014/diags',
                     grid_dir=grid_dir)

mxl = pt.calc_vel_at_mxl(ds)

k=10
pt.diag_plot(ds.XC,ds.YC,mxl[0,k,:,:])
