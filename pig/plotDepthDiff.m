function [] = plotDepthDebug()

gridRes=8;
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% --- Load various r_low fields
results_dir = '/workspace/MITgcm_c66l/mysetups/pig/run_ad_08_tim/';
results_dir2='/workspace/MITgcm_c66l/mysetups/pig/run_ad_08_tim_noDepth/';

[r_low,ro_surf,hfacc] = loadResults(results_dir);
[r_low2,ro_surf2,hfacc2] = loadResults(results_dir2);

r_lowDiff = struct('iniVariaStart',abs(r_low.iniVariaStart - r_low2.iniVariaStart)./...
                                    abs(r_low2.iniVariaStart),...
                   'updateMasksEtc',abs(r_low.updateMasksEtc - r_low2.updateMasksEtc)./...
                                    abs(r_low2.updateMasksEtc));
ro_surfDiff = struct('iniVariaStart',abs(ro_surf.iniVariaStart - ro_surf2.iniVariaStart)./...
                                    abs(ro_surf2.iniVariaStart),...
                   'updateMasksEtc',abs(ro_surf.updateMasksEtc - ro_surf2.updateMasksEtc)./...
                                    abs(ro_surf2.updateMasksEtc));
hfaccDiff = struct('iniVariaStart',abs(hfacc.iniVariaStart - hfacc2.iniVariaStart)./...
                                    abs(hfacc2.iniVariaStart),...
                   'updateMasksEtc',abs(hfacc.updateMasksEtc - hfacc2.updateMasksEtc)./...
                                    abs(hfacc2.updateMasksEtc));

figureH;
subplot(3,3,1)
  contourf(mygrid.XC,mygrid.YC,r_low.updateMasksEtc)
  colorbar
  title('rlow 0, depthctrl')
subplot(3,3,2)
  contourf(mygrid.XC,mygrid.YC,r_low2.updateMasksEtc)
  colorbar
  title('rlow 0, no depthctrl')
subplot(3,3,3)
  contourf(mygrid.XC,mygrid.YC,r_lowDiff.updateMasksEtc)
  colorbar
  title('rlow 0, diff')

subplot(3,3,4)
  contourf(mygrid.XC,mygrid.YC,ro_surf.updateMasksEtc)
  colorbar
  title('r0surf 0, depthctrl')
subplot(3,3,5)
  contourf(mygrid.XC,mygrid.YC,ro_surf2.updateMasksEtc)
  colorbar
  title('r0surf 0, no depthctrl')
subplot(3,3,6)
  contourf(mygrid.XC,mygrid.YC,ro_surfDiff.updateMasksEtc)
  colorbar
  title('r0surf 0, diff')

subplot(3,3,7)
  contourf(mygrid.XC,mygrid.YC,hfacc.updateMasksEtc)
  colorbar
  title('hfacc 0, depthctrl')
subplot(3,3,8)
  contourf(mygrid.XC,mygrid.YC,hfacc2.updateMasksEtc)
  colorbar
  title('hfacc 0, no depthctrl')
subplot(3,3,9)
  contourf(mygrid.XC,mygrid.YC,hfaccDiff.updateMasksEtc)
  colorbar
  title('hfacc 0, diff')

% --- plot it up
%figureH;
%subplot(2,1,1)
%  contourf(mygrid.XC,mygrid.YC,r_low.ini-r_low2.ini)
%  colorbar
%  title('r_low 0')
%
%%subplot(2,3,2)
%%  contourf(mygrid.XC,mygrid.YC,r_low.iniDepths-r_low2.iniDepths)
%%  colorbar
%%  title('r_low iniDepths')
%%
%%subplot(2,3,3)
%%  contourf(mygrid.XC,mygrid.YC,r_low.ctrlDepthIni-r_low2.ctrlDepthIni)
%%  colorbar
%%  title('r_low ctrlDepthIni')
%%
%%subplot(2,3,4)
%%  contourf(mygrid.XC,mygrid.YC,r_low.shelficeInitDepths-r_low2.shelficeInitDepths)
%%  colorbar
%%  title('r_low shelficeInitDepths')
%
%subplot(2,1,2)
%  contourf(mygrid.XC,mygrid.YC,r_low.updateMasksEtc-r_low2.updateMasksEtc)
%  colorbar
%  title('r_low updateMasksEtc')
%figureH;
%
%
%subplot(2,1,1)
%  contourf(mygrid.XC,mygrid.YC,ro_surf.ini-ro_surf2.ini)
%  colorbar
%  title('ro_surf 0')
%
%%subplot(2,3,2)
%%  contourf(mygrid.XC,mygrid.YC,ro_surf.iniDepths-ro_surf2.iniDepths)
%%  colorbar
%%  title('ro_surf iniDepths')
%%
%%subplot(2,3,3)
%%  contourf(mygrid.XC,mygrid.YC,ro_surf.ctrlDepthIni-ro_surf2.ctrlDepthIni)
%%  colorbar
%%  title('ro_surf ctrlDepthIni')
%%
%%subplot(2,3,4)
%%  contourf(mygrid.XC,mygrid.YC,ro_surf.shelficeInitDepths-ro_surf2.shelficeInitDepths)
%%  colorbar
%%  title('ro_surf shelficeInitDepths')
%
%subplot(2,1,2)
%  contourf(mygrid.XC,mygrid.YC,ro_surf.updateMasksEtc-ro_surf2.updateMasksEtc)
%  colorbar
%  title('ro_surf updateMasksEtc')
%
%
%figureH;
%subplot(2,1,1)
%  contourf(mygrid.XC,mygrid.YC,hfacc.ini-hfacc2.ini)
%  colorbar
%  title('hfacc 0')
%
%%subplot(2,3,2)
%%  contourf(mygrid.XC,mygrid.YC,hfacc.iniDepths-hfacc2.iniDepths)
%%  colorbar
%%  title('hfacc iniDepths')
%%
%%subplot(2,3,3)
%%  contourf(mygrid.XC,mygrid.YC,hfacc.ctrlDepthIni-hfacc2.ctrlDepthIni)
%%  colorbar
%%  title('hfacc ctrlDepthIni')
%%
%%subplot(2,3,4)
%%  contourf(mygrid.XC,mygrid.YC,hfacc.shelficeInitDepths-hfacc2.shelficeInitDepths)
%%  colorbar
%%  title('hfacc shelficeInitDepths')
%
%subplot(2,1,2)
%  contourf(mygrid.XC,mygrid.YC,hfacc.updateMasksEtc-hfacc2.updateMasksEtc)
%  colorbar
%  title('hfacc updateMasksEtc')
%
%
%%figure;
%%contourf(mygrid.XC,mygrid.YC,r_low.xx_ctrlDepthIni-r_low2.xx_ctrlDepthIni)
%%  colorbar
%%  title('xx\_r\_low ctrlDepthIni')
%
%
%keyboard

function [r_low,ro_surf,hfacc] = loadResults(results_dir)


% --- And now, in calling order ...
r_low_iniDepths = rdmds([results_dir 'r_low_iniDepths_iniFixed']);
ro_surf_iniDepths = rdmds([results_dir 'ro_surf_iniDepths_iniFixed']);
hfacc_iniDepths = rdmds([results_dir 'hfacc_iniDepths_iniFixed']);

r_low_iniMasksEtc = rdmds([results_dir 'r_low_iniMasksEtc_iniFixed']);
ro_surf_iniMasksEtc = rdmds([results_dir 'ro_surf_iniMasksEtc_iniFixed']);
hfacc_iniMasksEtc = rdmds([results_dir 'hfacc_iniMasksEtc_iniFixed']);

r_low_iniVariaStart = rdmds([results_dir 'r_low_iniVariaStart']);
ro_surf_iniVariaStart = rdmds([results_dir 'ro_surf_iniVariaStart']);
hfacc_iniVariaStart = rdmds([results_dir 'hfacc_iniVariaStart']);


if ~isempty(strfind(results_dir,'noDepth'))
    r_low_iniDepths2=r_low_iniDepths;
    r_low_ctrlDepthIni=r_low_iniDepths;
    r_low_shelficeInitDepths=r_low_iniDepths;

    ro_surf_iniDepths2=ro_surf_iniDepths;
    ro_surf_ctrlDepthIni=ro_surf_iniDepths;
    ro_surf_shelficeInitDepths=ro_surf_iniDepths;

    hfacc_iniDepths2=hfacc_iniDepths;
    hfacc_ctrlDepthIni=hfacc_iniDepths;
    hfacc_shelficeInitDepths=hfacc_iniDepths;
else
    r_low_iniDepths2 = rdmds([results_dir 'r_low_iniDepths_iniVaria']);
    r_low_ctrlDepthIni = rdmds([results_dir 'r_low_ctrlDepthIni']);
    r_low_shelficeInitDepths = rdmds([results_dir 'r_low_shelficeInitDepths']);

    ro_surf_iniDepths2 = rdmds([results_dir 'ro_surf_iniDepths_iniVaria']);
    ro_surf_ctrlDepthIni = rdmds([results_dir 'ro_surf_ctrlDepthIni']);
    ro_surf_shelficeInitDepths = rdmds([results_dir 'ro_surf_shelficeInitDepths']);
    
    hfacc_iniDepths2 = rdmds([results_dir 'hfacc_iniDepths_iniVaria']);
    hfacc_ctrlDepthIni = rdmds([results_dir 'hfacc_ctrlDepthIni']);
    hfacc_shelficeInitDepths = rdmds([results_dir 'hfacc_shelficeInitDepths']);
end

r_low_updateMasksEtc = rdmds([results_dir 'r_low_updateMasksEtc']);
ro_surf_updateMasksEtc = rdmds([results_dir 'ro_surf_updateMasksEtc']);
hfacc_updateMasksEtc = rdmds([results_dir 'hfacc_updateMasksEtc']);

r_low = struct('iniDepths',r_low_iniDepths,...
               'iniMasksEtc',r_low_iniMasksEtc,...
               'iniVariaStart',r_low_iniVariaStart,...
               'iniDepths2',r_low_iniDepths2,...
               'ctrlDepthIni',r_low_ctrlDepthIni,...
               'shelficeInitDepths',r_low_shelficeInitDepths,...
               'updateMasksEtc',r_low_updateMasksEtc);

ro_surf = struct('iniDepths',ro_surf_iniDepths,...
               'iniMasksEtc',ro_surf_iniMasksEtc,...
               'iniVariaStart',ro_surf_iniVariaStart,...
               'iniDepths2',ro_surf_iniDepths2,...
               'ctrlDepthIni',ro_surf_ctrlDepthIni,...
               'shelficeInitDepths',ro_surf_shelficeInitDepths,...
               'updateMasksEtc',ro_surf_updateMasksEtc);

hfacc = struct('iniDepths',hfacc_iniDepths,...
               'iniMasksEtc',hfacc_iniMasksEtc,...
               'iniVariaStart',hfacc_iniVariaStart,...
               'iniDepths2',hfacc_iniDepths2,...
               'ctrlDepthIni',hfacc_ctrlDepthIni,...
               'shelficeInitDepths',hfacc_shelficeInitDepths,...
               'updateMasksEtc',hfacc_updateMasksEtc);


end
end
