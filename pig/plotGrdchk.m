function [] = plotGrdchk(gridRes)
%
% Plot output from gradient check
%
% --------------------------------------------
prec=64;

if nargin<1, gridRes=8; end;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


filename = 'grdchk.depth.full.data';

[pos,fc,grad] = readGrdchk(filename);

Npts=size(pos,1);
i=pos(:,2);
j=pos(:,3);
fd_grad=grad(:,1);
adj_grad=grad(:,2);
rel_err=grad(:,3);

grdchk=NaN*mygrid.XC;
grdchk2=NaN*mygrid.XC;

for n=1:Npts
    grdchk(i(n),j(n)) = rel_err(n);
    if (fd_grad(n)>10^-7)
        grdchk2(i(n),j(n))=rel_err(n);
    end
end

log_grdchk=log10(grdchk);
log_grdchk(isinf(log_grdchk))=NaN;
log_grdchk=real(log_grdchk);

% Show points where log(grdchk)<-3
log_grdchk_threshold=log_grdchk.*(log_grdchk<-3);

% Ignore points where fd_grad<10^-7
log_grdchk2=log10(grdchk);
log_grdchk2(isinf(log_grdchk2))=NaN;
log_grdchk2=real(log_grdchk2);
log_grdchk_prec=log_grdchk2.*(log_grdchk2<-3);


% --- First get a plot of log10(grdchk)
cmap = cbrewer2('set1',9);

figure;
pcolor(mygrid.XC,mygrid.YC,log_grdchk);
colormap(cmap);
niceColorbar;
title('$\log_{10}$(1 - fd/adj)');

% --- Plot points which are above threshold value
figure;
pcolor(mygrid.XC,mygrid.YC,log_grdchk_threshold);
colormap(cmap);
title('$\log_{10}$(1 - fd/adj), threshold $10^{-3}$');
niceColorbar;

% --- Plot similar, but ignore points where gradient is below 10^-7
figure;
pcolor(mygrid.XC,mygrid.YC,log_grdchk_prec);
colormap(cmap);
title('$\log_{10}$(1 - fd/adj), threshold $10^{-3}$, remove FD$<=10^{-7}$');
niceColorbar;


keyboard

end
