function [] = genEtaObs(gridRes)
% Make fake sea surface height observations at ''mooring locations''
% Right now, want to see if inverting for bathymetry is possible...
% 
% This writes file: 
%   eta_mooring.obs - "mooring observations" of temp profile
%   eta_mooring.err - "uncertainties" of temp profiles
% 
%
% Inputs: gridRes = 8 (default), 16, 32
% 
% -------------------------------------------------------------------------------
prec=64;

if nargin<1, gridRes=8; end;

% Grab mygrid for this setup 
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));

%% Directories
saveDir = '/workspace/results/pig/bathy-eta-inversion/';

eta_truth_dir = [saveDir 'eta-truth-3yr-spinup/'];
eta_obs_dir = [saveDir 'eta-obs-3yr-spinup/'];
if ~exist(eta_obs_dir,'dir'), mkdir(eta_obs_dir); end;

%% Make a mask for the mooring locations  
Nr=length(mygrid.DRF);
mskC = 0*mygrid.XC;
mskC(3,13)=1.0; %outgoing flow
mskC(3,17)=1.0; %middle
mskC(3,21)=1.0; %middle
mskC(3,25)=1.0; %incoming flow


%% Read in "true" eta values
etaTruth= rdmds([eta_truth_dir 'surfDiag'],NaN,'rec',1);
mooringObs = etaTruth.*mskC; 
Nt = size(mooringObs,3);
%mskCT=mk3D(mskC,mooringObs); % only 1 month ...
%mooringObs = mooringObs.*mskCT;


% add noise
% set 0 to NaN to get correct mean
mooringObs(mooringObs==0)=NaN;
sigma = 0.05*nanmean(mooringObs(:));
mooringObs = mooringObs + sigma*mskC.*randn(size(mooringObs));
%for n=1:Nt
%  mooringObs(:,:,n) = mooringObs(:,:,n) + sigma*mskCT(:,:,n).*randn(size(squeeze(mooringObs(:,:,n))));
%end
% Can't seem to get time invariant weights to work
mooringError = sigma*mskC;

mooringObs(isnan(mooringObs))=0;
mooringError(isnan(mooringError))=0;

%% Write to file
% try extending record by a couple...
%mooringObs=cat(3,mooringObs,mooringObs(:,:,Nt-2:Nt));

%% Using time varying weights because fuck the ecco package
%mooringError=mooringObs;
%mooringError(mooringError~=0)=sigma^2;

meanFile = [eta_obs_dir 'eta_mooring.obs.data'];
errorFile= [eta_obs_dir 'eta_mooring.err.data'];

write2file(meanFile,mooringObs,prec);
write2meta(meanFile,size(mooringObs),prec);

write2file(errorFile,mooringError,prec);
write2meta(errorFile,size(mooringError),prec);

fprintf('Wrote file %s ... \n',meanFile);
fprintf('Wrote file %s ... \n',errorFile);

%% For reproducibility and such ... copy this file as well so we know what made the masks
copyfile('genEtaObs.m',eta_obs_dir);

fprintf('copied this matlab file to %s ...\n',eta_obs_dir);

keyboard
end
