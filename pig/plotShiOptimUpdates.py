#!/bin/python 

# A quick function to plot updates to SHIgammT, SHIgammS with sensitivities 
# for a given optimization iteration

import re
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import MITgcmutils as mut
from xmitgcm import open_mdsdataset
from pych import horizontal_map, calc_baro_stf
import xgcm

def plotShiOptimUpdates(run_dir_prefix,optimIter):
    """
    Given optim iter, plot adxx_shic(t/s), xx_shic(t/s), and xx_shic(t/s).effective
    """

    grid_dir = '/workspace/grids/pig_08'
    run_dir = ('%s.%02d' % (run_dir_prefix,optimIter))
    #run_dir = ('/workspace/ase-adjoint/pig/generic/shicoeff_optim/run_ad_08.shiOptim.%02d' % optimIter)
    diags_dir = ('%s/diags' % run_dir)
    adj_dir = ('%s/adj-diags' % run_dir)

    truth_dir = '/workspace/results/pig/shicoeff-optim/truth.ustar/diags'

    # --- Open the datasets
    optim = open_mdsdataset(data_dir=run_dir,
                            grid_dir=grid_dir,
                            ignore_unknown_vars=True,
                            iters=optimIter)

    diags = open_mdsdataset(data_dir=diags_dir,
                            grid_dir=grid_dir)

    #adj = open_mdsdataset(data_dir=adj_dir,
    #                      grid_dir=grid_dir)

    truth = open_mdsdataset(data_dir=truth_dir,grid_dir=grid_dir)

    if 'adxx_shicoeffs' not in optim.variables:
        optim['adxx_shicoeffs'] = np.NAN * optim['adxx_shicoefft']
        optim['xx_shicoeffs'] = np.NAN * optim['xx_shicoefft']
        optim['xx_shicoeffs.effective'] = np.NAN * optim['xx_shicoefft.effective']

    xc=optim.XC
    yc=optim.YC
    depth=optim.Depth

    # --- Sensitivities
    horizontal_map(xc,yc,
              optim['adxx_shicoefft'].isel(time=0),
              optim['adxx_shicoeffs'].isel(time=0),
              title1=('$\partial J/\partial u_T$, optim iter %d' % optimIter),
              title2=('$\partial J/\partial u_S$, optim iter %d' % optimIter),
              depth=depth)

    # --- Optimization updates
    if optimIter > 0:
        horizontal_map(xc,yc,
                  optim['xx_shicoefft'].isel(time=0),
                  optim['xx_shicoeffs'].isel(time=0),
                  title1=('$\delta u_T$, optim iter %d' % optimIter),
                  title2=('$\delta u_S$, optim iter %d' % optimIter),
                  depth=depth)

    # --- Smoothed updates + initial value = log_10(field)
    horizontal_map(xc,yc,
              optim['xx_shicoefft.effective'].isel(time=0),
              optim['xx_shicoeffs.effective'].isel(time=0),
              title1=('$\log_{10}(m_T) = \mathcal{A}u_T$, optim iter %d' % optimIter),
              title2=('$\log_{10}(m_S) = \mathcal{A}u_S$, optim iter %d' % optimIter),
              depth=depth, log_data=True)

    # --- Melt rate, compare to truth

    # gamma_T
    diags_shict = diags['SHIgammT'].isel(time=slice(-1,-3,-1)).mean('time')
    truth_shict = truth['SHIgammT'].isel(time=slice(-1,-3,-1)).mean('time')

    cmin = -6.2
    cmax = -5.2

    horizontal_map(xc,yc,diags_shict,truth_shict,
              title1=('SHI $\gamma_T$, optim iter %d' % optimIter),
              title2=('SHI $\gamma_T$, truth'),
              depth=depth,log_data=True,c_lim=(cmin,cmax))

    # gamma_S
    diags_shics = diags['SHIgammS'].isel(time=slice(-1,-3,-1)).mean('time')
    truth_shics = truth['SHIgammS'].isel(time=slice(-1,-3,-1)).mean('time')

    cmin = -7.8
    cmax = -6.6

    horizontal_map(xc,yc,diags_shics,truth_shics,
              title1=('SHI $\gamma_S$, optim iter %d' % optimIter),
              title2=('SHI $\gamma_S$, truth'),
              depth=depth,log_data=True,c_lim=(cmin,cmax))
    
    # meltrate
    diags_fwflx = diags['SHIfwFlx'].isel(time=slice(-1,-3,-1)).mean('time')
    truth_fwflx = truth['SHIfwFlx'].isel(time=slice(-1,-3,-1)).mean('time')

    cmax = np.max(np.abs(truth_fwflx.values))

    horizontal_map(xc,yc,diags_fwflx,truth_fwflx,
              title1=('SHI FWFLX, optim iter %d' % optimIter),
              title2=('SHI FWFLX, truth'),
              depth=depth,c_lim=(-cmax,cmax))

    return

def plotInitialConditionFit(run_dir_prefix,optimIter,t_ind=0,zlev=20):
    """
    Compare initial conditions of prior, optimIter, and truth.
    """

    grid_dir = '/workspace/grids/pig_08'
    optim_dir = ('%s.%02d/diags' % (run_dir_prefix,optimIter))
    prior_dir = ('%s.00/diags' % run_dir_prefix)
    truth_dir = '/workspace/results/pig/shicoeff-optim/truth.ustar/diags'

    # --- Warn myself to make sure I'm comparing to the right thing
    print(' Using truth associated with 1 month objective function, not last week ')
    print('   truth_dir = %s ' % truth_dir )

    # --- Open the datasets
    optim = open_mdsdataset(data_dir=optim_dir,
                            grid_dir=grid_dir)

    prior = open_mdsdataset(data_dir=prior_dir,
                            grid_dir=grid_dir)

    truth = open_mdsdataset(data_dir=truth_dir,
                            grid_dir=grid_dir)


    # --- Grab some common grid variables
    xc=optim.XC
    yc=optim.YC
    xg=optim.XG
    yg=optim.YG
    depth=optim.Depth


    # --- Generate grids for each
    optim_grid = xgcm.Grid(optim,periodic=False)
    prior_grid = xgcm.Grid(prior,periodic=False)
    truth_grid = xgcm.Grid(truth,periodic=False)

    # --- Calculate barotropic streamfunction
    optim_baro = calc_baro_stf(optim,optim_grid)
    prior_baro = calc_baro_stf(prior,prior_grid)
    truth_baro = calc_baro_stf(truth,truth_grid)

    # --- Mask out stuff
    maskC = optim.maskCtrlC.isel(Z=zlev)
    maskU = optim.maskCtrlU.isel(Z=0)

    # --- Loop over each field and plot
    for fldStr in ('THETA','SALT'):

        horizontal_map(xc,yc,
                prior[fldStr].isel(time=t_ind,Z=zlev),
                truth[fldStr].isel(time=t_ind,Z=zlev),
                title1=('Prior %s, t = %d * 5 days, k = %d' % 
                    (fldStr,t_ind,zlev)),
                title2=('Truth %s, t = %d * 5 days, k = %d' % 
                    (fldStr,t_ind,zlev)),
                mask1=maskC,
                mask2=maskC,
                depth=depth)

        horizontal_map(xc,yc,
                optim[fldStr].isel(time=t_ind,Z=zlev),
                truth[fldStr].isel(time=t_ind,Z=zlev),
                title1=('Optim iter %d %s, t = %d * 5 days, k = %d' % 
                    (optimIter,fldStr,t_ind,zlev)),
                title2=('Truth %s, t = %d * 5 days, k = %d' % 
                    (fldStr,t_ind,zlev)),
                mask1=maskC,
                mask2=maskC,
                depth=depth)

    # --- Barotropic streamfunction
    horizontal_map(xc,yc,
            prior_baro.isel(time=t_ind),
            truth_baro.isel(time=t_ind),
            title1=('Prior $\Psi$, t = %d * 5 days' % 
                (t_ind)),
            title2=('Truth $\Psi$, t = %d * 5 days' % 
                (t_ind)),
            mask1=maskU,
            mask2=maskU,
            depth=depth)
    horizontal_map(xc,yc,
            optim_baro.isel(time=t_ind),
            truth_baro.isel(time=t_ind),
            title1=('Optim iter %d $\Psi$, t = %d * 5 days' % 
                (optimIter,t_ind)),
            title2=('Truth $\Psi$, t = %d * 5 days' % 
                (t_ind)),
            mask1=maskU,
            mask2=maskU,
            depth=depth)

    return
    

def plotFailedUpdates(run_dir_prefix,optimIter):
    """
    Plot xx_shic(t/s), and xx_shic(t/s).effective for failed run to see what happened
    """

    grid_dir = '/workspace/grids/pig_08'
    run_dir = ('%s.%02d' % (run_dir_prefix,optimIter))

    # --- Open the datasets
    optim = open_mdsdataset(data_dir=run_dir,
                            grid_dir=grid_dir,
                            ignore_unknown_vars=True,
                            iters=optimIter)

    xc=optim.XC
    yc=optim.YC
    depth=optim.Depth

    # --- Optimization updates
    for vname in optim:
        if vname.startswith('xx_') and (not vname.endswith('effective')):
            if vname.find('shicoeff')>0:
                log_plot=True
            else:
                log_plot=False

            horizontal_map(xc,yc,
                  optim[vname].isel(time=0),
                  optim[('%s.effective' % vname)].isel(time=0),
                  title1=('$%s$ at optim iter %d' % (vname.replace('_','\_'),optimIter)),
                  title2=('$%s.effective$ at optim iter %d' % (vname.replace('_','\_'),optimIter)),
                  depth=depth,log_data=log_plot)

    return optim

def plotAvgMisfits(run_dir_prefix,optim_iter):
    """
    Plot error as a function of iteration number 
    optim_iter is the last iteration number to be considered
    """

    grid_dir = '/workspace/grids/pig_08'
    #main_dir = '/workspace/ase-adjoint/pig/generic/shicoeff_optim'

    g = open_mdsdataset(data_dir=grid_dir,grid_dir=grid_dir,iters=None)
    
    # --- Create containers
    nIters = optim_iter+1
    theta_misfits = np.zeros_like(np.tile(g.hFacC.copy().values,[nIters,1,1,1]))
    salt_misfits = np.zeros_like(np.tile(g.hFacC.copy().values,[nIters,1,1,1]))

    
    # --- Load misfts
    for n in np.arange(nIters):
        run_dir = ('%s.%02d' % (run_dir_prefix,n))
        theta_misfits[n,:,:,:] = mut.rdmds('%s/misfit_theta_obs' % run_dir)
        salt_misfits[n,:,:,:] = mut.rdmds('%s/misfit_salt_obs' % run_dir)

    theta_avg = np.nansum(np.nansum(np.nansum(np.abs(theta_misfits),axis=3),axis=2),axis=1)
    salt_avg = np.nansum(np.nansum(np.nansum(np.abs(salt_misfits),axis=3),axis=2),axis=1)

    plt.figure(figsize=(15,7))
    plt.loglog(np.arange(nIters),theta_avg)
    plt.loglog(np.arange(nIters),salt_avg)
    plt.legend(('Sum Theta misfits','Sum Salt misfits'))
    plt.show()

    return

def plotShiCostFunction(run_dir_prefix,optim_iter):

    nIters=optim_iter+1
    #main_dir = '/workspace/ase-adjoint/pig/generic/shicoeff_optim/run_ad_08.shiOptim'

    # Read in the costfunctions as a dictionary
    di = readAllCostFunctions(run_dir_prefix,optim_iter)

    # Make an empty legend string to append
    legendStr = []

    plt.figure(figsize=(15,7))
    for key,value in di.items():
        if np.nansum(np.abs(value)) > 0.0:
            plt.loglog(np.arange(nIters),value)
            legendStr.append('$%s$' % key.replace('_','\_'))

    plt.legend(legendStr)
    plt.show()

    return



def readAllCostFunctions(main_dir,optim_iter):
    """
    Read all costfunction files from optimization iteration 0 to optim_iter
    
    Parameters
    ----------
    main_dir    :: path to search for costfunction.#### files in the format
                   main_dir.NN/costfunction00NN, N = each optim iteration
    optim_iter  :: integer indicating final optim iter to find costfunction file

    Output
    ------
    di          :: dictionary containing cost function components determined
                   by each line in the costfunction file
                   e.g. 
                   di['fc'] = np.array(fc0,fc1,fc2,...)
                   di['f_obcsw'] = np.array(fow0,fow1,fow2,...)
    """

    n=0
    nIters = optim_iter+1

    # Read initial costfunction data to dataframe
    df = readCostFunction(('%s.%02d/costfunction%04d' % (main_dir,n,n)))

    # Make a dictionary with cost for each component
    di = {}
    for name,cost in df[['name','cost']].values:
        di[name] = np.array(cost)

    # Append cost from each iteration
    for n in np.arange(1,nIters):
        df = readCostFunction(('%s.%02d/costfunction%04d' % (main_dir,n,n)))
        for name,cost in df[['name','cost']].values:
            di[name] = np.append(di[name],cost)
    
    return di


def readCostFunction(file_path):
    """
    Read costfunction file for full cost function and each component

    Build a pandas dataset with the following components
    1. Keywords from first word on each line
    2. cost function value as first value after "="
    3. number of elements for the next number

    Parameters
    ----------
    file_path   ::  path to costfunction#### file to be read

    Output
    ------
    df          ::  Pandas dataframe with costfunction file contents:
                    df['name'] : contains each named line of file (e.g. 'fc')
                    df['cost'] : the value of cost from that component
                    df['size'] : number of elements accounted for in cost component 
    """

    rx = re.compile(r"^\s?(?P<keyname>\S+)\s+(?P<gen_info>\(\w+\s+\d+\))?\s?=\s+(?P<cost>\d+\.\d+E?D?\+?-?\d+)\s+(?P<num_cost_elements>\d+\.\d+E?D?\+?-?\d+)")

    # Create an empty container to be appended w/ each line
    costfunction_data = []

    with open(file_path,'r') as file_object:
        for line in file_object:
            match = rx.search(line)
            if match:
                single_match = {
                        'name'  :   match.group('keyname'),
                        'cost'  :   float(match.group('cost').replace("D","E")),
                        'size'  :   int(float(match.group('num_cost_elements').replace("D","E")))
                        }
                costfunction_data.append(single_match)

    # Convert to nice Pandas dataframe
    costfunction_data = pd.DataFrame(costfunction_data,columns=['name','size','cost'])

    # Index by name
    costfunction_data.set_index('name')

    return costfunction_data

