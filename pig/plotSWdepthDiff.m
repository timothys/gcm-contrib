function [] = plotSWdepthDiff()

% --- Grab grid for this setup 
gridRes=8;
mygrid=establish_mygrid(sprintf('pig_%02d',gridRes));


% --- Load various r_low fields
results_dir = '/workspace/MITgcm_c66l/mysetups/pig/run_ad_08_tim/reinit-correct/';
results_dir2='/workspace/MITgcm_c66l/mysetups/pig/run_ad_08_tim/reinit-wrong/';

[rLowW,rLowS,hFacW,hFacS] = loadResults(results_dir);
[rLowW2,rLowS2,hFacW2,hFacS2] = loadResults(results_dir2);

rLowWdiff = struct('reinit',abs(rLowW.reinit - rLowW2.reinit)./...
                                    abs(rLowW2.reinit),...
                   'update1',abs(rLowW.update1 - rLowW2.update1)./...
                                    abs(rLowW2.update1),...
                   'update2',abs(rLowW.update2 - rLowW2.update2)./...
                                    abs(rLowW.update2));

bigPlot(rLowWdiff.reinit,rLowWdiff.update1,rLowWdiff.update2);
end


function [ ] = bigPlot(fld1,fld2,fld3)

global mygrid;

figureH;
subplot(1,3,1)
  contourf(mygrid.XC,mygrid.YC,fld1)
  niceColorbar;
subplot(1,3,2)
  contourf(mygrid.XC,mygrid.YC,fld2)
  niceColorbar;
subplot(1,3,3)
  contourf(mygrid.XC,mygrid.YC,fld3)
  niceColorbar;
 
end


function [rLowW,rLowS,hFacW,hFacS] = loadResults(results_dir)


% --- And now, in calling order ...
rLowW_reinit = rdmds([results_dir 'rLowW_reinit_sw_masks']);
rLowS_reinit = rdmds([results_dir 'rLowS_reinit_sw_masks']);

rLowW_1 = rdmds([results_dir 'rLowW_update1']);
rLowS_1 = rdmds([results_dir 'rLowS_update1']);
hFacW = rdmds([results_dir 'hFacW_update1']);
hFacS = rdmds([results_dir 'hFacS_update1']);

rLowW_2 = rdmds([results_dir 'rLowW_update2']);
rLowS_2 = rdmds([results_dir 'rLowS_update2']);

rLowW = struct('reinit',rLowW_reinit,...
               'update1',rLowW_1,...
               'update2',rLowW_2);
rLowS = struct('reinit',rLowW_reinit,...
               'update1',rLowW_1,...
               'update2',rLowW_2);


end
