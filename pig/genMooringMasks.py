#!/bin/python

# General functions to make and write masks corresponding to mooring observation
# locations
#
# In the future, this could be moved to a general 'python tools' directory ... 

import os
from shutil import copyfile
import numpy as np
from xmitgcm import open_mdsdataset
import MITgcmutils as mut


def genMooringMask(grid,lat,lon,depth,mask_name=None):
    """
    Create 3D mask in lat/lon/depth space indicating location of mooring

    Inputs
    ------
    grid            :: xarray DataSet with grid information
                       NOTE: Needs XC, YC, Z, and maskCtrlC in DataSet

    lat/lon         :: float/double with decimal coordinates of mooring

    depth           :: depth levels at which mooring takes observations 
                       assumed to be < number of grid levels

    comments        :: mask_name to be used for meta data


    Output
    ------
    mooring_mask    :: xarray DataArray with 3D mask giving points where 
                       observations are taken.  
                       2D points are the grid cell centers nearest to mooring lat/lon
                       depth points are cell centers nearest to input depth levels
    """

    # Make sure that XC,YC,Z,maskCtrlC is in the DataSet
    flds = ('XC','YC','Z','maskCtrlC')
    for f in flds:
        if f not in grid.coords:
            raise AttributeError('field %s not present in grid' % f)

    # Find nearest cell centers to input lat/lon
    nearest_xc = grid.XC.sel(XC=lon,method='nearest')
    nearest_yc = grid.YC.sel(YC=lat,method='nearest')
    msk2d_cond = (grid.XC==nearest_xc) & (grid.YC==nearest_yc)

    # Create a container size of Z all false
    full_depth_cond = grid.Z != grid.Z

    # At each depth level, write True for points where single level matches input depth
    for k in np.arange(len(depth)):
        single_level_cond = grid.Z == grid.Z.sel(Z=depth[k],method='nearest')
        full_depth_cond = full_depth_cond | single_level_cond

    # For meta data, return nearest z levels
    # (can't do this above because when depth is a vector, it drops unmatching levels
    #  and the mask needs to have T/F at all depth levels)
    nearest_depth = grid.Z.sel(Z=depth,method='nearest')
    depth_string = ''.join( ('%dm ' % nearest_depth[i]) 
                             for i in np.arange(len(nearest_depth)) )

    # Make a 3D mask
    mooring_msk = grid.maskCtrlC.where(msk2d_cond & full_depth_cond,0)

    # Add to meta data via attributes
    if mask_name is not None:
        mooring_msk.attrs['standard_name'] = mask_name

    mask_name = mooring_msk.attrs['standard_name']

    mask_description = ('%s mask at\n' \
                        '(lat,lon) = (%.3f, %.3f) \n' \
                        'depth = %s' % 
                        (mask_name, nearest_yc, nearest_xc, depth_string))

    mooring_msk.attrs['long_name'] = mask_name
    mooring_msk.attrs['description']= mask_description

    return mooring_msk

def writeMaskFile(mask_array,filename,dataprec='float64',comments=None):
    """
    Write a mask file in *.meta/data format and binary 
    Simply copy the *.data format to binary

    Input 
    -----
        mask_array  :: array to be written

        filename    :: base filename for <filename>.(meta/data)

        dataprec    :: 'float64' (default) or 'float32' for double/single

        comments    :: any string describing how the file was generated 
    """

    mut.wrmds(fbase=filename,arr=mask_array,dataprec=dataprec,simulation=comments)
    print('%s.meta/data files written ...' % filename)

    copyfile(src=('%s.data' % filename),dst=filename)
    print('%s binary file written ...' % filename)
